<?php

use Illuminate\Database\Seeder;
use App\Models\MenuBookmark;
use App\Models\MenuPrices;
use App\Models\Cuisine;
use App\Models\Menu;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\CmsUser;
use App\Models\BuildYourOwnPizza;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Restaurant::truncate();
        MenuBookmark::truncate();
        MenuPrices::truncate();
        Menu::truncate();
        Cuisine::truncate();
        CmsUser::truncate();
        BuildYourOwnPizza::truncate();

        factory(Restaurant::class, 10)->create();
        factory(User::class, 10)->create();
        factory(Cuisine::class, 10)->create();
        factory(Menu::class, 10)->create();
        factory(MenuBookmark::class, 10)->create();
        factory(MenuPrices::class, 10)->create();
        factory(CmsUser::class, 1)->create();
        factory(BuildYourOwnPizza::class, 3)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
