<?php

    /*
    |--------------------------------------------------------------------------
    | Model Factories
    |--------------------------------------------------------------------------
    |
    | Here you may define all of your model factories. Model factories give
    | you a convenient way to create models for testing and seeding your
    | database. Just tell the factory how a default model should look.
    |
    */

    /*
        randomDigit // 7
        randomDigitNotNull // 5
        randomNumber($nbDigits = NULL) // 79907610
        randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL) // 48.8932
        numberBetween($min = 1000, $max = 9000) // 8567
        randomLetter // 'b'
        randomElements($array = array ('a','b','c'), $count = 1) // array('c')
        randomElement($array = array ('a','b','c')) // 'b'
        numerify($string = '###') // '609'
        lexify($string = '????') // 'wgts'
        bothify($string = '## ??') // '42 jz'
     */

    $factory->define(App\Models\User::class, function (Faker\Generator $faker) {
        $hash      = app()->make('hash');
        $firstName = $faker->firstName;
        $lastName  = $faker->lastName;

        return [
            'restaurant_id'    => $faker->numberBetween($min = 1, $max = 10),
            'username'         => strtolower($firstName . '_' . $lastName),
            'fname'            => $firstName,
            'lname'            => $lastName,
            'email'            => strtolower($firstName . '.' . $lastName) . '@gmail.com',
            'password'         => $hash->make('secret'),
            'mobile'           => $faker->phoneNumber,
            'phone'            => $faker->phoneNumber,
            'display_pic_url'  => $faker->imageUrl(),
            'billing_address'  => $faker->address,
            'shipping_address' => $faker->address,
            'status'           => $faker->boolean,
        ];
    });

    $factory->define(App\Models\Restaurant::class, function (Faker\Generator $faker) {
        return [
            'parent_restaurant_id' => $faker->numberBetween($min = 1, $max = 10),
            'rest_code'            => $faker->swiftBicNumber,//strtoupper(str_random()),
            'restaurant_name'      => $faker->name . ' Restaurant',
            'description'          => $faker->text,
            'lat'                  => $faker->latitude,
            'lng'                  => $faker->longitude,
            'address'              => $faker->address,
            'street'               => $faker->streetName,
            'zipcode'              => $faker->streetName,
            'mobile'               => $faker->phoneNumber,
            'phone'                => $faker->phoneNumber,
            'accept_cc'            => $faker->boolean,
            'accept_dc'            => $faker->boolean,
            'delivery'             => $faker->boolean,
            'takeout'              => $faker->boolean,
            'dinning'              => $faker->boolean,
            'reservation'          => $faker->boolean,
            'menu_available'       => $faker->boolean,
            'total_seats'          => $faker->randomNumber(),
            'facebook_url'         => $faker->url,
            'twitter_url'          => $faker->url,
            'gmail_url'            => $faker->url,
            'pinterest_url'        => $faker->url,
            'instagram_url'        => $faker->url,
            'yelp_url'             => $faker->url,
            'tripadvisor_url'      => $faker->url,
            'foursquare_url'       => $faker->url,
            'twitch_url'           => $faker->url,
            'youtube_url'          => $faker->url,
        ];
    });

    $factory->define(App\Models\Cuisine::class, function(Faker\Generator $faker) {
        return [
            'cuisine'       => $faker->colorName,
            'cuisine_type'  => $faker->country,
            'description'   => $faker->colorName,
            'image_name'    => $faker->image(),
            //'search_status' => $faker->boolean,
            'status'        => $faker->boolean,
            'priority'      => $faker->numberBetween(1,10),
        ];
    });

    $factory->define(App\Models\Menu::class, function (Faker\Generator $faker) {
        return [
            'restaurant_id' => $faker->numberBetween(1, 10),
            'item_name'     => $faker->name,
            'image_name'    => $faker->image(),
            'cuisine_id'    => $faker->numberBetween(1, 10),
            'status'        => $faker->boolean,
        ];
    });

    $factory->define(App\Models\MenuBookmark::class, function (Faker\Generator $faker) {
        return [
            'restaurant_id' => $faker->numberBetween($min = 1, $max = 10),
            'menu_id'       => $faker->numberBetween($min = 1, $max = 10),
            'user_id'       => $faker->numberBetween($min = 1, $max = 5),
        ];
    });

    $factory->define(App\Models\MenuPrices::class, function (Faker\Generator $faker) {
        return [
            'menu_id'           => $faker->numberBetween($min = 1, $max = 10),
            'price'             => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'price_description' => $faker->text,
        ];
    });

    $factory->define(App\Models\CmsUser::class, function (Faker\Generator $faker) {
        $hash = app()->make('hash');
        return [
            'name'      => 'Amit Malakar',
            'email'     => 'amalakar@bravvura.in',
                'password'  => $hash->make('amit!@#456'),
            'superuser' => 1,
            'status'    => 1,
        ];
    });

    $factory->define(App\Models\BuildYourOwnPizza::class, function (Faker\Generator $faker) {
        return [
            'restaurant_id'   => $faker->numberBetween($min = 1, $max = 5),
            'crust'           => '[{"name": "simple", "status": 1}, {"name": "extra thin", "status": "1"}, {"name": "extra thick", "status": "1"}]',
            'size'            => '[{"name": "7", "status": 1}, {"name": "9", "status": "1"}, {"name": "13", "status": "1"}]',
            'sauce'           => '[{"name": "tamarind", "status": 1}, {"name": "mustard", "status": "1"}, {"name": "tomato", "status": "1"}]',
            'cheese'          => '[{"name": "cheddar", "status": 1}, {"name": "mozzarella", "status": "1"}, {"name": "feta", "status": "1"}]',
            'topping_veg'     => '[{"name": "olives", "status": 1}, {"name": "capsicum", "status": "1"}, {"name": "basil", "status": "1"}]',
            'topping_non_veg' => '[{"name": "bacon", "status": 1}, {"name": "pepperoni", "status": "1"}, {"name": "chicken", "status": "1"}]',
            'quantity'        => '[{"name":"light","status":1},{"name":"regular","status":"1"},{"name":"extra","status":"1"}]',
            'side'            => '[{"name":"left","status":1},{"name":"right","status":"1"},{"name":"whole","status":"1"}]',
        ];
    });