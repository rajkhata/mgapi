<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('template_name')->unique();
            $table->text('content');
            $table->boolean('status')->default(0)->comment('0 - inactive, 1 - active');
            $table->integer('language_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');            
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('mail_templates');
    }
}
