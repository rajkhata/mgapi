<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('restaurant_id')->unsigned();
            $table->enum('review_for', ['1', '2', '3', '4']);
            $table->enum('on_time', ['0', '1', '2'])->default('0');
            $table->enum('fresh_prepared', ['0', '1', '2'])->default('0');
            $table->enum('as_specifications', ['0', '1', '2'])->default('0');
            $table->enum('temp_food', ['0', '1', '2'])->default('0');
            $table->enum('taste_test', ['0', '1', '2'])->default('0');
            $table->enum('services', ['0', '1', '2'])->default('0');
            $table->enum('noise_level', ['0', '1', '2'])->default('0');
            $table->double('rating_desc');
            $table->enum('order_again', ['0', '1', '2'])->default('0');
            $table->enum('come_back', ['0', '1', '2'])->default('0');
            $table->mediumText('review_desc');
            $table->boolean('status')->default('0');
            $table->bigInteger('approved_by');
            $table->timestamp('approved_date')->nullable();
            $table->tinyInteger('sentiment')->nullable(); //check
            $table->bigInteger('order_id')->unsigned();
            $table->tinyInteger('replied')->default('0');
            $table->mediumText('restaurant_response')->nullable();
            $table->tinyInteger('is_read')->default('0');
            $table->timestamp('owner_response_date');
            $table->boolean('cron_update')->default('0')->nullable();
            $table->timestamps();
        });
        Schema::table('menus', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_reviews');
    }
}
