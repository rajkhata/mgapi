<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuMealCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_meal_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('menu_id')->unsigned();
            $table->bigInteger('meal_category_id')->unsigned();
            $table->tinyInteger('priority')->default(0)->unsigned();
            $table->timestamps();
        });

        Schema::table('menu_meal_categories', function($table) {
            $table->foreign('menu_id')->references('id')->on('menus');
            $table->foreign('meal_category_id')->references('id')->on('meal_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_meal_categories');
    }
}
