<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id');
            $table->string('name');
            $table->string('slug');
            $table->string('image_svg')->nullable();
            $table->string('thumb_image_med_svg')->nullable();
            $table->string('image_png')->nullable();
            $table->string('thumb_image_med_png')->nullable();
            $table->boolean('status')->default(0);
            $table->tinyInteger('priority')->default(0)->unsigned();
            $table->timestamps();
        });
        Schema::table('menu_categories', function ($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_categories');
    }
}
