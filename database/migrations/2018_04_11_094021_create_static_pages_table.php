<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();
            $table->bigInteger('parent_id')->nullable()->unsigned();
            $table->string('page_type'); //config('constants.static_page_type'));
            $table->string('page_heading');
            $table->string('page_sub_heading')->nullable();
            $table->text('page_content');
            $table->tinyInteger('priority')->default(0)->unsigned();
            $table->json('image')->nullable()->comment('Multiple images json');
            $table->json('video')->nullable()->comment('Multiple videos json');
            $table->timestamps();
        });

        Schema::table('static_pages', function ($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('parent_id')->references('id')->on('static_pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pages');
    }
}
