<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();
            $table->string('username', 32)->nullable();
            $table->string('fname', 32);
            $table->string('lname', 32);
            $table->string('email', 64);
            $table->unique('email', 'users_email_unique');
            $table->string('password', 64);
            $table->string('mobile', 15);
            $table->string('phone', 15);
            $table->string('display_pic_url');
            $table->string('billing_address');
            $table->string('shipping_address');
            $table->boolean('status')->default(0);
            $table->string('stripe_customer_id');

            /*$table->integer('city_id');
            $table->enum('user_source', ['fb', 'tw', 'ws', 'gp']);
            $table->tinyInteger('accept_toc');
            $table->enum('newsletter_subscribtion', ['0','1']);
            $table->mediumInteger('points');
            $table->string('display_pic_url_normal');
            $table->string('display_pic_url_large');
            $table->mediumText('delivery_instructions');
            $table->mediumText('takeout_instructions');
            $table->tinyInteger('order_msg_status');
            $table->string('session_token');
            $table->string('access_token');
            $table->tinyInteger('image_status');
            $table->mediumText('tutorial');
            $table->tinyInteger('bp_status');
            $table->enum('registration_subscription', ['1','0']);
            $table->string('wallpaper');
            $table->string('referral_code', 30);
            $table->string('referral_ext', 50);
            $table->decimal('wallet_balance',10, 2);
            $table->string('token');
            $table->enum('	pointsreminder', ['0','1']);*/

            $table->timestamp('last_login');
            $table->timestamps();
        });

        Schema::table('users', function($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        /*Schema::table($this->table, function (Blueprint $tb) {
            $tb->dropForeign('venda_nfe_id_foreign');
            $tb->dropColumn('nfe_id');
        });*/
    }
}
