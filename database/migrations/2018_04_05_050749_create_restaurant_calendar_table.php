<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_calendar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();
            $table->enum('calendar_day', ['su','mo','tu','we','th','fr','sa']);
            $table->time('open');
            $table->time('close');
            $table->time('breakfast_start');
            $table->time('breakfast_end');
            $table->time('lunch_start');
            $table->time('lunch_end');
            $table->time('dinner_start');
            $table->time('dinner_end');
            $table->timestamps();
        });

        Schema::table('restaurant_calendar', function($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_calendar');
    }
}
