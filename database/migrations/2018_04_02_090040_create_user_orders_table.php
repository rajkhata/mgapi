<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('user_ip', 16);
            $table->decimal('lat', 10, 8)->nullable();
            $table->decimal('lng', 11, 8)->nullable();
            $table->string('phone')->nullable();

            $table->enum('order_type', ['delivery', 'takeout']);
            $table->boolean('pay_via_card')->default('1')->comment('1-card,0-non-card');
            $table->decimal('order_amount', 8, 2)->default(0.00);
            $table->decimal('tax', 8, 2)->default(0.00);
            $table->decimal('tip_amount', 8, 2)->default(0.00);
            $table->decimal('tip_percent', 8, 2)->default(0.00);
            $table->decimal('total_amount', 8, 2)->default(0.00);

            $table->decimal('delivery_charge', 8, 2)->default(0.00);
            $table->bigInteger('delivery_user_address_id')->nullable();
            $table->timestamp('delivery_time');
            $table->string('instructions')->nullable();

            $table->enum('status', ['placed', 'ordered', 'confirmed', 'delivered', 'arrived', 'cancelled', 'rejected', 'archived', 'test', 'edited'])->nullable();
            $table->bigInteger('card_id')->unsigned();
            $table->string('stripe_charge_id');
            $table->boolean('cod')->default(0);
            $table->timestamps();
            /*
            $table->string('stripe_token');
            $table->string('payment_receipt');
            */

            //$table->json('item')->nullable()->comment('pizza/item in json');
            //$table->bigInteger('menu_id')->unsigned();
        });

        Schema::table('user_orders', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('card_id')->references('id')->on('user_cards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_orders');
    }
}
