<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmsContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_content', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sms_name')->unique();
            $table->text('content');
            $table->boolean('status')->default(0)->comment('0 - inactive, 1 - active');
            $table->integer('language_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');            
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_content');
    }
}
