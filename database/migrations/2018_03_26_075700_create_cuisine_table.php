<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuisineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuisines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->unsigned();
            $table->string('cuisine', 100);
            $table->string('cuisine_type', 100);
            $table->string('description');
            $table->string('image_name');
            //$table->boolean('search_status')->default('0');
            $table->boolean('status')->default('1');
            $table->tinyInteger('priority')->default(0)->unsigned();
            $table->timestamps();
        });
        Schema::table('cuisines', function($table) {
            $table->foreign('parent_id')->references('id')->on('cuisines');//->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuisines');
    }
}
