<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            //$table->increments('id');
            $table->bigIncrements('id');
            $table->bigInteger('parent_restaurant_id')->unsigned();
            $table->string('rest_code', 20);
            $table->string('restaurant_name', 32);
            $table->text('description');
            $table->decimal('lat', 10, 8)->nullable();
            $table->decimal('lng', 11, 8)->nullable();
            $table->string('address', 255);
            $table->string('street', 200);
            $table->string('zipcode', 20);
            $table->string('mobile', 15);
            $table->string('phone', 200);

            $table->tinyInteger('accept_cc');
            $table->tinyInteger('accept_dc');
            $table->tinyInteger('delivery');
            $table->tinyInteger('takeout');
            $table->tinyInteger('dinning');
            $table->tinyInteger('reservation');
            $table->tinyInteger('menu_available');

            $table->addColumn('integer', 'total_seats', ['length' => 5]);

            $table->string('facebook_url', 255);
            $table->string('twitter_url', 255);
            $table->string('gmail_url', 255);
            $table->string('pinterest_url', 255);
            $table->string('instagram_url', 255);
            $table->string('yelp_url', 255);
            $table->string('tripadvisor_url', 255);
            $table->string('foursquare_url', 255);
            $table->string('twitch_url', 255);
            $table->string('youtube_url', 255);

            $table->timestamps();
        });

        Schema::table('restaurants', function($table) {
            $table->foreign('parent_restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
