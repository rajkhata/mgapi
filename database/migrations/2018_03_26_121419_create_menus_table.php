<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();
            $table->string('item_name');
            //$table->json('item_json')->nullable();
            $table->string('image_name');
            $table->bigInteger('cuisine_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::table('menus', function($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('cuisine_id')->references('id')->on('cuisines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
