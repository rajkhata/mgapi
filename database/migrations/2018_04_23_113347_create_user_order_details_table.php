<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('menu_id')->unsigned();
            $table->json('menu_json')->nullable();
            $table->string('special_instruction');
            $table->integer('quantity');
            $table->decimal('unit_price', 8, 2)->default(0.00);
            $table->decimal('total_menu_amount', 8, 2)->default(0.00);
            $table->boolean('status')->default(0)->comment('0 - cart, 1 - order placed');
            $table->timestamps();
        });

        Schema::table('user_order_details', function($table) {
            $table->foreign('order_id')->references('id')->on('user_orders');
            $table->foreign('menu_id')->references('id')->on('menus');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_order_details');
    }
}
