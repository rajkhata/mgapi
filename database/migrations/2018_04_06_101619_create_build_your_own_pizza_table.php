<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateBuildYourOwnPizza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('build_your_own_pizzas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('restaurant_id')->unsigned();

            $table->json('crust')->nullable()->comment('Extra thin / Simple / Extra thick');
            $table->json('size')->nullable();
            $table->json('sauce')->nullable()->comment('Multiple sauce options');
            $table->json('cheese')->nullable()->comment('Multiple cheese options for future');
            $table->json('topping_veg')->nullable()->comment('Multiple veg toppings');
            $table->json('topping_non_veg')->nullable()->comment('Multiple non-veg toppings');
            $table->json('bake_level')->nullable()->comment('Bake well / Bake light');
            $table->json('cut')->nullable()->comment('Pie cut / Square cut');
            $table->json('seasoning')->nullable()->comment('HRI Seasoning');

            $table->json('quantity')->nullable()->comment('Light / Regular / Extra');
            $table->json('side')->nullable()->comment('Left / Right / Whole');
            $table->timestamps();
        });
        Schema::table('build_your_own_pizzas', function ($table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('build_your_own_pizzas');
    }
}
