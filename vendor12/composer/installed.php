<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'ac5a9673eb0a8969398942d94db65468618634c6',
    'name' => 'laravel/lumen',
  ),
  'versions' => 
  array (
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'darkaonline/swagger-lume' => 
    array (
      'pretty_version' => '5.6.2',
      'version' => '5.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4fd9a49f0189ca5f9ef485c7170390a50ad28e66',
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce77a7ba1770462cd705a91a151b6c3746f9c6ad',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '65b2d8ee1f10915efb3b55597da3404f096acba2',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '60d379c243457e073cff02bc323a2a86cb355631',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53330f47520498c0ae1f61f7e2c90f55690c06a3',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'illuminate/auth' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d4aa427a01ecfcd9eeda1301a7602d6e39a1cdb',
    ),
    'illuminate/broadcasting' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '346c4f5552c0247e0e9f8bf2e637feb13520c061',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dba5798de392a3da8add4541df10661d5c6a0f4',
    ),
    'illuminate/cache' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '615759a96b212365aece1abc64cc3a98a7d730b0',
    ),
    'illuminate/config' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '61f4eb8145a1473577a9876471c92fa4de4718a7',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'de6b95229de443585f97953da4b02721465a2b2b',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '779b56b37396b888414622d5667d3bcc205964ab',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '66b653fd430bf06f59cef1112197d009bd02da84',
    ),
    'illuminate/database' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '33073fe81e705b3d5d5ef992e6b065b3a89b07f6',
    ),
    'illuminate/encryption' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd5ec7a711863fea409211bc6f04da8d1a05515e',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c702e65fe37458fece6ae87ce7906aaa614383d6',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b25940b428c9fd284feaad20e176ace8d780973b',
    ),
    'illuminate/hashing' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee3f00a4f12e562f7d2217b0889e3ba44c587a6d',
    ),
    'illuminate/http' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '552a7d2851c84adc1a4536c56ffc25d77a3b5f34',
    ),
    'illuminate/log' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e00aa10a73127f676939ddb5b615302025c2838',
    ),
    'illuminate/pagination' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8ff0633e8c20f16c1100192f71be0b13568f021',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '18bf3d2c621939f87e285b5af8c464a549c4df05',
    ),
    'illuminate/queue' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd304bf168765b773a0bb44556a09d1f475d9c12e',
    ),
    'illuminate/session' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '183f1c064bb47a94d72c98aacc24f3189e03c6e7',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2fce24254b8f60a2f92a3ab485799b372625a06',
    ),
    'illuminate/translation' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e4d8211d6717a1d55c11d4e06b3185c9f8a8d2a',
    ),
    'illuminate/validation' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f7da089264cf8cea97a1591ea9622dae1e500ec2',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '60fd8f12340417a4312e5d90961510333d4f1d46',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'kylekatarnls/update-helper' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '429be50660ed8a196e0798e5939760f168ec8ce9',
    ),
    'laravel/cashier' => 
    array (
      'pretty_version' => 'v7.2.2',
      'version' => '7.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd450aa31f1b5e9f9618088ebe9fa4c2d77962d5d',
    ),
    'laravel/lumen' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'ac5a9673eb0a8969398942d94db65468618634c6',
    ),
    'laravel/lumen-framework' => 
    array (
      'pretty_version' => 'v5.6.4',
      'version' => '5.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '377918ff2b73e02fbc091f75471a365f17bf0704',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.3.3',
      'version' => '3.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1123697f6a2ec29162b82f170dd4a491f524773',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '60fa2f67f6e4d3634bb4a45ff3171fa52215800d',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.25.5',
      'version' => '1.25.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '1817faadd1846cd08be9a49e905dc68823bc38c0',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'namshi/jose' => 
    array (
      'pretty_version' => '7.2.3',
      'version' => '7.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '89a24d7eb3040e285dd5925fcad992378b82bcff',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.25.3',
      'version' => '1.25.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad6afecd38ce2d7f7bd1b5d47ffd8e93ebbd3ed8',
    ),
    'nikic/fast-route' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'v0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.1.8',
      'version' => '6.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '917ab212fa00dc6eacbb26e8bc387ebe40993bc1',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ce87516be71aae9b956f81906aaf0338e0d8a2d',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '6.1.4',
      'version' => '6.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '7.5.20',
      'version' => '7.5.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9467db479d1b0487c99733bb1e7944d32deded2c',
    ),
    'predis/predis' => 
    array (
      'pretty_version' => 'v1.1.6',
      'version' => '1.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '9930e933c67446962997b05201c69c2319bf26de',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'pubnub/pubnub' => 
    array (
      'pretty_version' => '4.1.7',
      'version' => '4.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '18c60ceea0d00048060fcafe20f7a5c13455d158',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'rmccue/requests' => 
    array (
      'pretty_version' => 'v1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '87932f52ffad70504d93f04f15690cf16a089546',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.1',
      'version' => '8.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v5.9.2',
      'version' => '5.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '026191d12241a76c957884dff75e4f3721b0e77f',
    ),
    'swagger-api/swagger-ui' => 
    array (
      'pretty_version' => 'v3.37.0',
      'version' => '3.37.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f0b7e7b0412d89b20a3f7def8397b60ca2ee5041',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '20f73dd143a5815d475e0838ff867bce1eebd9d5',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c87adf3fc1cd0bf4758316a3a150d50a8f957ef4',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa56b4074d1ae755beb55617ddafe6f5d78f665',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '363cca01cabf98e4f1c447b14d0a68617f003613',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '4204f13d2d0b7ad09454f221bb2195fccdf1fe98',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '26f63b8d4e92f2eecd90f6791a563ebb001abe31',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41db680a15018f9c1d4b23516059633ce280ca33',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '827a00811ef699e809a201ceafac0b2b246bf38a',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '109b2a46e470a487ec8b0ffea4b0bb993aaf42ed',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5485a92c24d4bcfc2f3fc648744fb398482ff1b',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ba089a5b6366e453971d3aad5fe8e897b37f41',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b75acd829741c768bc8b1f84eb33265e7cc5117',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '727d1096295d807c309fb01a851577302394c897',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39d483bdf39be819deabf04ec872eb0b2410b531',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cede45fcdfabdd6043b3592e83678e42ec69e930',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ff431c517be11c78c48a39a66d37431e26a6bed',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e70aa8b064c5b72d3df2abd5ab1e90464ad009de',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '2f4b049fb80ca5e9874615a2a85dc2a502090f05',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.16',
      'version' => '4.4.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '73095716af79f610f3b6338b911357393fdd10ab',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e13f3fcefb1fcaaa5efb5403581406f4e840b9a',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.1.8',
      'version' => '5.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f284e032c3cefefb9943792132251b79a6127ca6',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'tymon/jwt-auth' => 
    array (
      'pretty_version' => '1.0.0-rc.2',
      'version' => '1.0.0.0-RC2',
      'aliases' => 
      array (
      ),
      'reference' => 'd5220f6a84cbb8300f6f2f0f20aa908d072b4e4b',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v2.6.6',
      'version' => '2.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1d57f62db3db00d9139078cbedf262280701479',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'zircote/swagger-php' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d172471e56433b5c7061006b9a766f262a3edfd',
    ),
  ),
);
