<?php

namespace Faker\Provider\nl_BE;

class Text extends \Faker\Provider\Text
{
    /**
     * The Project Gutenberg EBook of De legende en de heldhaftige, vroolijke en
     * roemrijke daden van Uilenspiegel en Lamme Goedzak in Vlaanderenland en elders, by Charles de Coster
     *
     * This eBook is for the use of anyone anywhere at no cost and with
     * almost no restrictions whatsoever.  You may copy it, give it away or
     * re-use it under the terms of the Project Gutenberg License included
     * with this eBook or online at www.gutenberg.org/license
     *
     *
     * Title: De legende en de heldhaftige, vroolijke en roemrijke daden
     * van Uilenspiegel en Lamme Goedzak in Vlaanderenland en elders
     *
     * Author: Charles de Coster
     *
     * Release Date: July 3, 2005 [EBook #11208]
     * [Last updated: March 14, 2015]
     *
     * Language: Dutch
     *
     * @see http://www.gutenberg.org/cache/epub/11208/pg11208.txt
     * @var string
     */
    protected static $baseText = <<<'EOT'
  De legende en de heldhaftige,
                    vroolijke en roemrijke daden van
                     Uilenspiegel en Lamme Goedzak
                      in Vlaanderenland en elders


                                  door

                           Charles de Coster



                     in het Vlaamsch vertaald door

                 Richard Delbecq   en   René de Clercq
                 (voor het proza)     (voor de liederen)

                               Derde druk
                     met 22 platen van Jules Gondry
                                  1919







KORTE LEVENSBESCHRIJVING VAN CHARLES DE COSTER

Bewerkt naar Ch. Potvin, Francis Nautet enz.


Charles-Theodore-Henri De Coster werd geboren te München, den 20n
Augustus 1827. Zijn vader was intendant van graaf Charles Mercy
d'Argenteau, aartsbisschop van Tyrus, die peter des kunstenaars was en
hem de markiezin Henriette de la Tour Dupin, vrouw van den Franschen
gezant te Turijn, tot meter gaf.

De kleine De Coster, een engeltje van een knaap, sleet dus zijne
eerste levensjaren in het paleis van den aartsbisschop, midden in
weelde, in bloemen, geliefkoosd door zijne ouders en zijnen peter. Zijn
eerste opvoeding was dus zeer aristocratisch en die indrukken blijven
gewoonlijk onuitwischbaar.

Doch weinig tijds nadien verandert dit alles. Zijne ouders verlaten
München en gaan naar Brussel, waar hun tweede kind ter wereld komt;
dan sterft zijn vader te Ieperen, bij zijn broeder, die daar geneesheer
was. Zijn moeder keert terug naar Brussel bij hare zuster en hare
kinderen.

Charles was reeds in eene kostschool te Etterbeek, waar "ik mij zal
moeten schikken naar den wil van een ander", zegt hij, "na zoolang
mijn zin te hebben gedaan". Als hij uit de kostschool komt, is het
om in het "Collège Saint-Michel" te treden, waar men een oogenblik
hoopte dat het kind, dat reeds de droomerijen boven de droge studiën
verkoos, zich aan het priesterschap zou wijden.

Eerst dacht hij in de balie te treden, doch een vriend deed hem
opmerken dat de rechten en de kunst moeilijk samengaan, en De Coster,
geholpen door machtige beschermers, aanvaardde eene bediening in de
"Société Générale".

In 't lot gevallen, stelde zijne moeder eenen plaatsvervanger, die
wegliep; na eenige dagen in het regiment, bij zijn kolonel, vertoefd
te hebben, "om den plaatsvervanger te vervangen", maakte de jonge
bediende op zijne beurt van de gelegenheid gebruik om zijne plaats
te ontloopen. "Het ambtenaarsleven bevalt mij in het geheel niet",
zegde hij. In de Bank voelde hij zich als een vreemdeling te midden
van de bureaucraten. Hij stikte in die atmosfeer en "overigens wilde
hij voor zich zelven werken". De letterkundige roeping verkreeg de
bovenhand en hij trad in 1850 in de Hoogeschool van Brussel, waar
hij het diploma van candidaat in de letteren behaalde.

Maar De Coster gaf aan de Hoogeschool noch zijn hart, noch zijnen
geest, noch zijne pen. Toen hij ze verliet, was hij noch doctor,
noch professor, noch dagbladschrijver, noch tooneeldichter. Maar hij
was kunstenaar, meer dan ooit.

Vervolgens wilde hij in de redactie van een dagblad treden, maar hij
aanbad het schoone boven alles en weigerde "een werktuig te maken
van zijne pen".

Dan begint een jammerlijk leven van voortdurenden tegenspoed en
onbegrepen arbeid. In 1856 weigert hij eene plaats bij een makelaar
in wijnen,--alles wat men hem aanbood.

Om het even, de jonge kunstenaar heeft wilskracht en, door al zijn
kommer heen, maakt hij eervol naam in de Fransche letterkunde. Buiten
en behalve menigvuldige gewaardeerde bijdragen in dagbladen en
tijdschriften, levert hij, in 1856, les Frères de la bonne trogne
(Brabantsche legende); in 1857, de Légendes flamandes et wallones,
die een ongemeenen bijval ontmoeten en door de Fransche pers vleiend
beoordeeld worden; in 1861, de Contes brabançons.

Zijn peter, de aartsbisschop, had hem sedert lang zijne bescherming
onttrokken, die hem zeker ware bijgebleven, hadde De Coster zijne
studiën in de Hoogeschool van Leuven willen doen. Hij had Brussel
verkozen, waar hij vrienden vond. Dat was eene keuze doen voor de
algeheele vrijheid des geestes. In 1863 wordt het petekind van den
aartsbisschop van Tyrus lid van de Vrije Gedachte van Brussel. Hij was
toen in den vollen bloei van zijn eersten bijval en gansch vervoerd
door zijne liefde voor het schoone.

Zijne liefde voor het volk, voor het wakkere Vlaamsche volk, stuwt
hem voorwaarts en houdt zijn machtig genie bezig. De schilder Dillens
zijn vriend, bezat in zijn werkhuis een verzameling oude Vlaamsche
boeken. De Coster en Dillens doen verscheidene reizen door Zeeland
en Vlaanderen: de "Legende van Uilenspiegel" was van dan af geboren
in De Coster's brein.

De Legende van Uilenspiegel en Lamme Goedzak, in de letterwereld
met ongeduld verwacht, verscheen in 1867 in een prachtige uitgave,
opgeluisterd met twee en dertig etsen van negentien talentvolle
kunstenaars.

Ziehier wat onder meer drie Fransche bladen zeiden van dat gewrocht:

La Liberté van 18 December 1868: "'t Is een heldendicht in proza,
waarin het bloed zoo rijkelijk vloeit als het bier. Men zou zeggen
een kermis rondom eenen brandstapel".

Le Constitutionnel, 9 December 1868, wijdde drie groote kolommen
aan Uilenspiegel, waarin de recensent het boek met Goethe's Faust
vergelijkt.

Le Corsaire: "'t Is een heldendicht in proza, 't is de verheerlijking
van den Vlaamschen geest".

Heel de Fransche pers deelde dit gevoelen en drukte hare bewondering
in de vleiendste artikelen uit.

Onze Busken Huët getuigde: "Hollanders noch Vlamingen bezitten een
werk over de XVIe eeuw in Vlaanderen, dat met het meesterwerk van De
Coster kan vergeleken worden".

Na Uilenspiegel verscheen nog: Voyage de noce (1872) en le Mariage
de Toulet (1879).

Edoch De Coster, die in het volle succes van de Légendes flamandes
zijne vriendin verloren had, zag zich op 29 Juli 1869, wanneer
Uilenspiegel zoo gunstig onthaald werd, nu nog zijne moeder ontrukken.

Die ramp schokte hem diep in zijn reeds droevig bestaan, want De Coster
leefde veelal in armoede, niettegenstaande zijn talent en de gunst
waarmede zijne werken ontvangen werden. Schrale schrijversrechten,
karige toelagen, luttel betaalde lessen moesten hem vrijwaren voor
ellende. Hij kloeg dan ook, steeds denzelfden strijd te moeten
herbeginnen. In 1870 schreef hij: "Hoewel ik veel gewerkt heb
uit lust en uit liefde, begrijp ik, sedert minder dan drie jaar,
de schrikverwekkende waarde van het geld en de noodwendigheid van
een arbeid, die, genoegzaam betaald, den mensch, met den welstand,
ook vrijheid en vreugde schenkt".

Maar daarom legde hij zijne fierheid niet af.

Toen eindelijk de regeering, een tiental jaren vóór zijnen dood, er
aan dacht de verstandelijke hulpmiddelen van den grooten schrijver
ten behoeve van het onderwijs aan te wenden, was het te laat. Hij
stak zoo diep in schulden, dat zijne benoeming geen anderen uitslag
opleverde dan eene opschudding te verwekken onder zijne schuldeischers,
die zijn traktement aansloegen en hunne prooi niet meer loslieten.

Toen hij stierf, op 7 Mei 1879, verkeerde hij in de diepste ellende.

                   *       *       *       *       *

Den 22n Juli 1894 werd door het gemeentebestuur van Eisene een
eenvoudig doch treffend gedenkteeken van den beeldhouwer Samuel ter
nagedachtenis van De Coster ingehuldigd.







DE LAATSTE OOGENBLIKKEN VAN CHARLES DE COSTER.


Charles De Coster stierf op 7 Mei 1879, te Elsene, in het huis, dat
den hoek uitmaakt van de Gewijde-Boomstraat, en toen gehuurd werd
door een fruitverkooper. Heel de woning van den grooten kunstenaar
bestond uit de twee kamers op de eerste verdieping: de grootste was
zijn werkkabinet, de andere zijne slaapkamer; daarin stonden een
ijzeren bed, een kleine tafel, een houten kast, eenige stoelen.

Hij had zich den dag te voren te bed gelegd: de pisvloed waaraan
hij leed, en diens noodlottige gezellin, de longtering, waren
plotseling verergerd. Charles De Coster nam zelden zijne toevlucht tot
geneesheeren; een zijner vrienden nochtans, M. Kirkpatrick, verschrikt
over den voortgang van de kwaal, had den heer dokter Vaucleroy,
geneesheer aan de Krijgsschool, ontboden. Toen deze kwam, vond hij
aan de sponde van den zieke eene oppasster, die De Coster in zijn
verheven en grenzenloos medelijden met de onterfden en ongelukkigen,
bij zich genomen had. Deze arme vrouw, die bij den zieltogende waakte,
was zelve het toonbeeld des doods; heel haar aangezicht was ingevreten
door zweren. De geneesheer ging heen zonder hoop den zieke te redden,
maar hij voorzag toch geen dreigenden dood: hij zou 's anderen daags
namiddags terugkomen.

's Anderen daags scheen De Coster zijn nakend einde niet bewust te
zijn, want hij vroeg noch naar zijnen schoonbroeder, noch naar zijne
zuster, die hij aanbad. Doch hij wilde zich omringen van vrienden,
als om zijn lichaam en zijn hart te verwarmen. Hij liet deze roepen,
die in de nabijheid woonden: zoo werden Félix Bouré, de beeldhouwer,
en later ik zelf geroepen. Bouré was ziek; hij verwittigde zijn
broeder, mede een vriend van De Coster: de heer Bouré vond in het
werkkabinet kapitein Mertens die, diep bedroefd, in de kamer van den
zieke niet dorst gaan. Deze betoonde een levendige erkentelijkheid
aan den heer Bouré, die zijn bed wat gemakkelijker schikte en hem te
drinken gaf. Toen ik en mijne vrouw op onze beurt kwamen, richtte De
Coster zich op in zijn bed en herkende mij heel goed. Kloekmoedig in
het aanschijn van den dood, had hij nog het gedacht om den heer Bouré
en mij aan elkander voor te stellen. De heer Bouré bevestigde mij dat
hij, toespeling makend op mijn beroep van advocaat, eenige Latijnsche
woorden mompelde. Maar zijn blik verduisterde, zijne ademhaling werd
hijgend; toen mijne vrouw hem naderde om zijn hoofdkussen te schikken
en zijn voorhoofd te verfrisschen, moest hij eene inspanning doen om
heur te herkennen: "Hoe, gij ook, mevrouw, ik dank u zeer!" Daarna
werd de ademhaling flauwer, een laatste naam, die zijner zuster,
kwam pijnlijk over zijne lippen: "Ca...ro...line". Het was zijn hart,
dat ontsnapte. Het was twee uren.


Hector Denis.







VOORREDE VAN DEN UIL [1]


Heeren kunstenaars, heeren uitgevers, heer dichter, ik heb u eenige
aanmerkingen te doen aangaande uwe eerste uitgave. Hoe! in dat
lijvige boek, in dien olifant dien gij met achttienen naar den roem
tracht te drijven, hebt gij het kleinste plaatsje niet gegund aan
den vogel van Minerva, den wijzen, omzichtigen uil! In Duitschland
en in dat Vlaanderen dat gij zoozeer bemint, reis ik gedurig op den
schouder van Uilenspiegel, die maar aldus genoemd wordt, omdat zijn
naam bediedt: Uil en Spiegel, wijsheid en komediespel. Die van Damme,
waar hij geboren werd, naar men zegt, spreken uit: Ulenspiegel, door
samentrekking en de gewoonte die zij hebben u in stee van Ui uit te
spreken. Dat is hunne zaak.

Gij hebt eene andere uitlegging uitgedacht: Ulen voor U lieden Spiegel,
de Spiegel van U, boeren en heeren, geregeerden en regeerders, de
spiegel van de dwaasheden, de belachelijkheden, de misdaden van een
tijdstip. Dat was vernuftig, maar onredelijk. Men moet nooit afbreken
met den slenter.

Misschien vondt gij het vreemd de wijsheid te verbeelden door een--naar
uwe meening--treurigen, belachelijken vogel, een gebrilden schoolvos,
een kermis-grappenmaker, een vriend der duisternis, dien men niet
hoort vliegen en die doodt zonder dat men hem hoort komen, evenals de
Dood. Nochtans gelijkt gij op mij, huichelaars die lacht met mij. In
menige uwer nachten stroomde het bloed onder de slagen der Moord,
die op vilten zolen liep, opdat men heur ook niet zoude hooren komen.

Brak, in uw aller geschiedenis, nooit geen bleeke dageraad aan, die met
zijn vale schemering de met lijken van mannen, vrouwen en kinderen
bedekte straatsteenen verlichtte? Waarvan leeft uwe Staatkunde,
sedert dat gij over de wereld regeert? Van worgen en moorden.

Ik, uil, de leelijke uil, ik dood om mij te spijzen, om mijne jongen
te spijzen, ik dood niet om te dooden. Verwijt gij mij de vogeltjes
op te peuzelen, dan kan ik u even goed de slachting verwijten die
gij aanricht onder alles wat leeft. Gij hebt boeken geschreven waarin
gij met verteedering spreekt over de lichtheid van de vogelen, over
hunne minnarijen, over hunne schoonheid, over de kunst waarmede zij hun
nestje bouwen, en over de angsten des moederschaps, vervolgens zegt gij
met welke saus men ze moet opdienen en in welke maand van het jaar zij
de vetste stoverij opleveren. Ik, ik maak geen boeken, God beware mij
daarvoor, anders schreef ik dat, als gij den vogel niet kunt opeten,
gij het nest opeet, uit vreeze dat gij een hap zoudt verliezen.

Wat u betreft, onbesuisde dichter, het was uw belang mij terug te
brengen in uw werk, waarvan ten minste twintig hoofdstukken mij
toebehooren [2] de andere laat ik u in onbetwisten eigendom. Men mag
toch wel het volstrekt meesterschap behouden over de domheden die
men laat drukken. Schreeuwende dichter, gij slaat links en rechts op
die welke gij de beulen des vaderlands heet, gij stelt Keizer Karel
en Philips II aan den schandpaal der geschiedenis; gij zijt geen
uil; gij zijt niet voorzichtig. Weet gij of er geen Keizer Karel
of geen Philips II op de wereld meer bestaan? Vreest gij niet dat
eene opmerkzame censuur uit den buik van uwen olifant toespelingen
op doorluchtige tijdgenooten vinde? Waarom laat gij dien Keizer en
dien Koning niet slapen in hun graf? Waarom moet gij al die majesteit
aanblaffen? Die het zweerd trekt, zal door het zweerd vergaan. Er zijn
menschen die het u nooit zullen vergeven, ik ook vergeef het u niet,
gij stoort mijne burgerlijke spijsvertering.

Wat beteekent die bestendige tegenstelling tusschen een verfoeiden
koning, wreedaardig van jongs af--daarom is het een mensch--en
dat Vlaamsche volk, dat gij ons wilt voorstellen als heldhaftig,
gulhartig, eerlijk en werkzaam? Wie zegt u dat die koning slecht en
dat volk goed was? Wijselijk zou ik u het tegenovergestelde kunnen
bewijzen. Uwe hoofdpersonages zijn dwazen of zotten, zonder er een
uit te zonderen: uw deugniet van Uilenspiegel neemt de wapenen op
voor de gewetensvrijheid; zijn vader Klaas sterft, laat zich levend
verbranden voor zijne godsdienstige overtuiging; zijne moeder, Soetkin,
kwijnt van verdriet en sterft ten gevolge van de foltering, om een
fortuin voor haren zoon te bewaren; uw Lamme Goedzak stapt recht door
het leven alsof het al was, goed en eerlijk op deze wereld te zijn;
uwe kleine Nele, die niet leelijk is, bemint in heel haar leven maar
een enkelen man.... Waar ziet men nog zulke dingen? Ik zou u beklagen,
zoo ge mij niet deedt lachen.

Nochtans moet ik bekennen dat naast die bespottelijke personages, er
wel eenige zijn die ik geerne onder mijne boezemvrienden zoude nemen:
uwe Spaansche huurlingen, uwe monniken die het gemeen verbranden,
uwe Gilline, spionneerster der Inquisitie, uw gierige vischverkooper,
aanklager en weerwolf, uw edelman die 's nachts duivel speelt om eene
onnoozele te verleiden, en vooral dien omzichtigen Philips II, die,
geld noodig hebbende, de heilige beelden in de kerken doet breken,
ten einde een opstand te beteugelen waarvan hij de wijze aanstoker
was. Minder kan men toch niet, als men geroepen is te erven van
degenen die men doodt.

Maar ik geloof dat al mijne woorden verloren moeite zijn. Gij weet
niet wat een uil is. Ik ga het u zeggen.

De uil is hij die in 't geniep, eerroof stookt onder de lieden
die hem hinderlijk zijn en die, als men hem vraagt of hij de
verantwoordelijkheid over zijne gezegden wil dragen, voorzichtig
antwoordt: Ik bevestig niets, Men heeft mij gezegd.... Hij weet wel
dat Men onvindbaar is.

Uil is hij die een eerlijk gezin binnendringt, zich aanstelt als
een trouwer, een meisje verleidt, geld ontleent, soms zijne schuld
betaalt en henengaat als er niets meer te nemen is.

Uil, de politieke man die een masker van vrijheid, van oprechtheid,
van menschenliefde opzet en die, op een gegeven oogenblik, zonder te
verwittigen, een man of eene natie zachtjes de keel toeworgt.

Uil, de koopman die zijnen wijn doopt, zijne eetwaren vervalscht,
een kwade maag brengt daar waar spijsverteering,--woede, daar waar
vroolijkheid was.

Uil, hij die behendig steelt, zonder dat men hem bij den kraag vatten
kan, valsch getuigt tegen de waarheid, de weduwe ten onder brengt,
de weeze stroopt, en zegepraalt in 't vet, lijk anderen zegepralen in
't bloed.

Uilin, zij die hare schoonheid verkoopt, de beste harten van
jongelieden vermorst, dat heeten: de jeugd vormen, en ze zonder eenen
cent, achterlaat in het slijk waarin zij hen sleepte.

Als ze ooit treurig gestemd is, zich ooit herinnert dat ze vrouw is,
moeder zoude kunnen zijn, dan verloochen ik heur. Als ze, dat bestaan
moede, in 't water springt, dan is zij eene zinnelooze, die niet
verdiende te leven.

Zie rondom u, domme schrijver, en tel, als gij kunt, de uilen van deze
wereld; bedenk of het voorzichtig is gelijk gij het doet, van Macht
en List, die koninginnen der uilen, aan te vallen. Kom tot inkeer,
zeg mea culpa en vraag op uwe knieën om vergiffenis.

Nochtans hebt gij mijne belangstelling gewonnen door uwe onbesuisdheid,
vol zelfvertrouwen; tegen mijne gekende gewoonten in, verwittig ik u
dan ook dat ik, op staanden voet, de grofheid en roekeloosheid van
uwen stijl ga aanklagen bij mijne neven in letterkunde, die eene
sterke pen, eene stoute tong en voortreffelijke brillen hebben, en
zeer voorzichtige en pedante lieden zijn, die uwen trant niet gewoon
zijn en hunne taal zoozeer kuischen, dat er ten lange laatste niets
zal van overblijven. [3]


Bubulus Bubb.







EERSTE BOEK.


I.

In meimaand, als de hagedoorn in bloei stond, werd te Damme, in
Vlaanderenland, Uilenspiegel, de zoon van Klaas geboren.

Terwijl Katelijne, de vroedvrouw, hem in warme doeken bakerde, bezag
ze zijn hoofd en riep ze blijde uit:

--Hij is met den helm geboren!

Maar weldra jammerend, met den vinger een zwart stipje op den schouder
van den boorling toonend:

--Laas! schreide zij, dat is het zwarte merk van den vinger des
duivels!

--Heer Satan is vandaag vroeg opgestaan, antwoordde Klaas, dat hij
alreeds den tijd vond om mijn zoon te teekenen?

--Satan sliep nog niet, zei Katelijne, want luister, nu eerst kraait
Kanteklaar de hennen wakker.

En zij gaf het kind over aan Klaas en ging naar buiten.

De dageraad verdreef nu het nachtelijk duister, de zwaluwen vlogen
kwetterend rakelings over de weide, en de zon kleurde vuurrood
de kimme.

Klaas deed het venster open en sprak tot Uilenspiegel:

--Kind met den helm, zie, daar is moeder de Zon, die Vlaanderenland
komt groeten. Bezie haar als uwe kijkers zullen open zijn; verkeert
gij later ooit in twijfel, weet gij niet wat te doen om goed te doen,
ga dan om raad bij de Zonne; zij is warm en helder: wees zoo goed
als zij warm, zoo eerlijk als zij helder is.

--Klaas, mijn man, zei Soetkin, ge spreekt tot een doove; kom en drink,
mijn jongen.

En de moeder stak den boorling hare schoone, blanke borsten toe.




II.

Terwijl Uilenspiegel zich laafde aan de levensbron, ontwaakten al de
vogelkens in 't veld.

Klaas, die mutsaards bond, bezag zijne vrouw, die Uilenspiegel de
borst gaf.

--Zeg eens, vrouw, sprak hij, hebt ge nog veel van die lekkere melk?

--De kruiken zijn vol, man, antwoordde zij, maar dat is niet voldoende
om mijn hert te verblijden.

--Gij spreekt zoo treurig en het is zoo vroeg nog in den morgen.

--Ik denk er aan, dat er geen oortje meer steekt in de tassche,
die daar aan den muur hangt.

Klaas nam de tassche van den wand; maar hij had goed schudden, er
rinkelde geen geld in. Hij was er onthutst over; doch hij wilde zijne
vrouw moed inspreken, en zei:

--Waarover bekommert gij U? Hebben wij in de schapraai den koek
niet liggen, dien Katelijne ons gisteren gaf? Zie ik daar geen groot
stuk vleesch, dat ten minste voor drie dagen goede melk aan 't kind
zal geven? Die zak boonen daar in den hoek, is die een voorteeken
van hongersnood? En dat kuipje boter bestaat toch niet in mijne
verbeelding? In mijne verbeelding ook niet, die appelen, welke,
met elven in 't gelid, op onzen zolder liggen? En de dikke tonne
schuimende Brugsche kuite, noodt zij ons niet, met haren vollen buik,
tot een gulle drinkpartij?

--Als 't kind gedoopt wordt, zei Soetkin, moeten er twee oortjes zijn
voor den pastoor en één gulden voor 't festijn.

Daarop kwam Katelijne het huis binnen met een grooten bundel kruiden
en zij sprak:

--Aan het kind bied ik de angelica, die den man voor ontucht behoedt
en de venkel, die Satan van hem verwijderd houdt....

--Hebt gij het kruideken niet, vroeg Klaas, dat guldens aantrekt?

--Neen, zegde zij.

--Dan ga ik zien of er iets in de vaart is te vinden.

Hij ging heen, met zijn hengel en zijn net, zeker dat hij niemand
ontmoeten zou, want het was nog een heel uur vóór oosterzon, wat in
Vlaanderen vijf uren zeggen wil.




III.

Klaas kwam aan de Brugsche vaart, niet verre van de zee. Hij schoof
het aas aan den haak, wierp de lijn uit en liet ook zijn net in 't
water zinken. Op den overkant der vaart lag een goedgekleede knaap
vast in slaap, op een bed van mosselen.

Op het gerucht, dat Klaas maakte, werd de jongen wakker; hij wilde
vluchten, meenende dat het een serjant der naburige gemeente was, die
kwam om hem te pakken en naar het Steen te brengen voor landlooperij.

Doch de schrik was verdwenen toen hij Klaas herkende, die hem toeriep:

--Wilt gij zes duiten verdienen? Ja?... Jaag dan de visch langs hier!

Op die woorden ging het knaapje, een kleine dikzak, het water in;
het trok er eenige lischbladeren, vatte ze tot een bundel samen en
joeg er mee de visch naar Klaas.

Toen de vangst gedaan was, trok Klaas net en lijn uit het water en
ging hij de sluis over naar het knaapje.

--Gij zijt het, zegde hij, die Lamme heet van uw doopnaam, en
Goedzak om den wille van uw zachtaardig karakter, en achter Onze
Lieve Vrouwekerk in de Reigerstraat woont? Hoe komt het dat gij,
zoo jong en zoo netgekleed, onder den blooten hemel slaapt?

--Laas! baas kooldrager, antwoordde het jongetje, ik heb thuis eene
zuster, die een jaar jonger is dan ik en mij troef geeft bij den
minsten twist. Maar op haren rug durf ik mijne weerwraak niet nemen,
want ik zou haar zeer doen, baas. Gisterenavond, onder het eten,
wischte ik met mijne vingers een teil uit, waarin ossenvleesch met
boonen geweest was, en zij wou er heur deel van hebben. Daar was niet
eens genoeg voor mij, baas. Als ze mij zag likkebaarden om den goeden
smaak der saus, werd ze razend en sloeg ze met de volle hand mij zóó in
't gezicht, dat ik heel bebloed het huis uitgeloopen ben.

Klaas vroeg hem wat zijn vader en zijne moeder zeiden, terwijl hij
zoo geslagen werd.

Lamme Goedzak antwoordde:

--Vader stompte mij op den eenen schouder en moeder klopte mij op den
anderen, roepende: "Verweer u, laffe Lamme". Maar ik wil geen meisje
slaan en daarom ben ik weggeloopen.

Eensklaps verbleekte Lamme en beefde hij als een riet.

En Klaas zag een lange vrouw afkomen, met een mager meisje naast zich,
dat er barsch uitzag.

--Ah! zuchtte Lamme, terwijl hij Klaas bij zijne hooze vastgreep, daar
komen moeder en zuster mij halen. Bescherm mij toch, baas kooldrager!

--Dáár, sprak Klaas, neem eerst die zes duiten voor uwe moeite en
heb geen vrees.

Toen de twee vrouwen Lamme zagen, liepen zij naar hem toe, en beiden
wilden hem slaan, de moeder omdat hij haar onrust aangedaan had en
de zuster uit gewoonte.

Lamme verschool zich achter Klaas en riep:

--Ik heb zes duiten verdiend, ik heb zes duiten verdiend, slaat
me niet!

Doch de moeder kuste haren jongen reeds, terwijl het meisje Lamme's
handen wilde openwringen, om hem zijn geld af te nemen. Maar Lamme
schreeuwde:

--'t Is 't mijne, ge zult het niet hebben.... 't Is 't mijne!

En hij balde de vuisten.

Toen trok Klaas de kleine meid geducht bij de ooren en sprak:

--Als het u nog voorvalt leed te doen aan uw broer, die goed en zacht
is als een lammeken, steek ik u in een donker kolenhok, en daar zal
ik u niet meer bij de ooren trekken, maar de roode duivel uit de hel;
hij zal u aan stukken scheuren met zijn groote klauwen en zijne tanden,
die op vorken gelijken.

Op die woorden dorst de meid Klaas niet meer te bezien, noch
heuren broeder te naderen; zij verborg zich achter de rokken heurer
moeder. Doch in de stad schreeuwde zij het overal uit:

--De kooldrager heeft mij geslagen; hij heeft een duivel in zijn
kelder.

Nochtans dorst zij Lamme niet meer slaan; maar als zij groot was,
deed ze hem haar werk doen. En de goede sul gehoorzaamde gewillig.

Onderweg had Klaas zijne vangst verkocht aan een pachter, een
lekkerbek, en thuis komende, zegde hij tot Soetkin:

--Zie, dat heb ik gevonden in den buik van vier snoeken, negen karpers
en in een volle ben paling.

En hij smeet twee gulden en een oortje op tafel.

--Man, waarom gaat gij niet alle dagen visschen? vroeg Soetkin.

Klaas antwoordde:

--Wel, omdat ik zelf niet geerne zou spartelen in de netten van de
stadsserjanten.




IV.

Te Damme werd Uilenspiegel's vader "Klaas de kooldrager"
geheeten. Klaas had zwart haar, schitterende oogen; zijn vel was van
de kleur zijner koopwaar, uitgenomen op Zon- en feestdagen, als er
veel zeep in de stulp was. Hij was klein, hoekig, sterk en blijgezind.

Als zijn werk gedaan was en hij met den valavond naar eene taveerne
van den Brugschen steenweg ging, om met kuite zijn keelgat te spoelen,
dat zwart was van koolstof, riepen al de vrouwen, die, op den dorpel
van heur deur den koelen avond genoten, hem vriendelijk toe:

--Goên avond en klaar bier, kooldrager!

--Goên avond en 'nen man die niet slaapt, antwoordde Klaas.

De meisjes die in troepjes van het veld kwamen, stelden zich vóór hem,
lieten hem niet door en vroegen hem:

--Wat geeft ge om er door te mogen: een scharlaken lint, een vergulden
gesp, fluweelen schoentjes of een gulden in ons beursje?

Maar Klaas nam er eene om haar middel en kuste heur wangen of heur
hals, al naarvolgens zijn mond het dichtst bij de donzige huid was,
en dan zegde hij:

--Vraagt, mijne hertjes, vraagt de rest aan uwe minnaars.

En schaterlachend gingen de joelende meisjes voort.

De kinderen herkenden Klaas aan zijn grove stem en aan zijn zwaren
stap. Zij liepen naar hem toe en zeiden:

--'n Avond, kooldrager!

--Van 's gelijken, mijne engelkens, zei Klaas; maar komt niet te dicht,
of 'k maak U zwart als moorkens.

De stoute kaboutermannekens kwamen toch nader; dan nam Klaas er een
bij zijn wambuis, streek zijn zwarte hand over 't gladde gezichtje
en liet hem zoo loopen, tot groote vreugd van de schaterende bende.

Soetkin, Klaas' wijf, was een brave, wakkere vrouw, die opstond met
de zon, en vlug en vlijtig was als een bij.

Zij en Klaas bebouwden getweeën hunnen akker en spanden zich als ossen
vóór den ploeg. Zwaar was het om hem voort te trekken, doch zwaarder
nog trok de egge, die met hare houten tanden den harden grond moest
scheuren. Toch deden zij het blij te moede, met een liedeken op
de lippen.

En de grond mocht nog zoo hard zijn en de zon hare heetste stralen
op hen neerschieten: zij konden water en bloed zweeten als zij de
egge trokken dat hunne knieën knikten--al hun lijden vergaten zij,
als zij even stil stonden en Soetkin heur zacht gelaat naar Klaas
keerde, want dan kuste Klaas den spiegel van die teedere ziele.




V.

Den vooravond had men van de pui van 't gemeentehuis uitgeroepen dat
Mevrouw, echtgenoote van keizer Karel, zwanger was en dat er gebeden
voor hare aanstaande verlossing moesten worden opgezegd.

Gansch huiverend kwam Katelijne bij Klaas binnen.

--Wat scheelt er? vroeg de kooldrager.

--Laas! sprak zij met hijgenden boezem. Dezen nacht zag ik spoken,
die menschen maaiden gelijk de hooiers het gras.--'k Zag meisjes
levend begraven! En de beul danste op de lijken!--De bloedsteen,
die sedert negen maanden zweette, is dezen nacht gebarsten.

--Erbarming, zuchtte Soetkin, erbarming, Heere God: wat duister
voorteeken voor Vlaanderenland!

--Ziet gij dat met uwe oogen of in droom? vroeg Klaas.

--Met mijne eigen oogen, sprak Katelijne.

Doodsbleek en schreiend sprak Katelijne toen:

--Twee kinderkens zijn geboren; het een, in Spanje, is de kleine
Philippus, het ander, in Vlaanderenland, is de zoon van Klaas,
die later Uilenspiegel zal heeten. Philippus wordt een beul, want
hij werd verwekt door Karel den Vijfde, den moordenaar van ons
land. Uilenspiegel wordt een meester in kwinkslagen en guitenstreken,
maar goedhertig zal hij zijn, want zijn vader is Klaas, de wakkere
arbeider, die in braafheid, eer en deugd zijn brood verdient. Keizer
Karel en koning Philippus zullen hun leven lang kwaad doen, door
oorlog en knevelarij en andere misdaden. Klaas, die heel de week werkt,
zal leven volgens recht en wet, bij zijn zuren arbeid zal hij lachen
in stee van weenen: hij zal het zinnebeeld van de goede Vlaamsche
werkers zijn. Uilenspiegel, immer jong en onsterfelijk, gaat de wereld
door, maar nergens zal hij een vaste woonplaats hebben. En hij zal
boer, edelman, schilder, beeldhouwer worden, alles zal hij te gelijk
zijn. Zoo zal hij dolen langs velden en wegen, het goede en het schoone
prijzen en lachen en spotten met alles wat dwaas en verkeerd is. Klaas
is uw moed, edel volk van Vlaanderen, en Soetkin uwe dappere moeder;
Uilenspiegel is uw geest; een lief en bevallig meisje, Uilenspiegel's
gezellin en onsterfelijk als hij, zal uw hert zijn, en Lamme Goedzak,
een dikke pens, uwe maag. En omhoog zullen de opeters van 't volk gaan,
en omlaag hunne slachtoffers; omhoog de roovende wespen, omlaag de
noeste bijen, en in den hemel zullen de wonden van Christus bloeden.

Toen Katelijne, de goede tooveres, dit gezegd had, viel zij in slaap.




VI.

Uilenspiegel werd ten doop gebracht, toen plotseling een hevige
regenbui viel, die hem gansch nat maakte. Zoo werd hij voor de eerste
maal gedoopt.

Als hij nu de kerk binnengebracht werd, kwam de kosterschoolmeester
aan peter en meter, vader en moeder zeggen, dat zij zich rond de
doopvont moesten scharen, hetgeen zij deden.

Maar boven de vont, was er in 't gewelf een gat, dat een metser gekapt
had om er eene lamp aan een vergulde sterre te hangen. De metser, die,
van boven, peter en meter stokstijf rond de toegedekte vont zag staan,
goot verraderlijk door het gat een emmer water, dat, tusschen hen, met
groot geplas op het deksel van de vont kletste. Doch Uilenspiegel kreeg
er het grootste deel van. En zoo werd hij voor de tweede maal gedoopt.

De deken kwam; zij deden hem hun beklag, maar hij zei hun van zich te
haasten, dat het een ongeluk was. Uilenspiegel ging te werk als een
bezetene, om den wille van het water, dat op hem gespat was. De deken
gaf hem het zout en het water en heette hem Thijlbert, wat zeggen wil:
"altijd ongedurig". En zoo werd hij voor de derde maal gedoopt.

Uit Onze Lieve Vrouwekerk ging men daar rechtover, in de Langestraat,
eene taveerne binnen, die voor uithangbord een rozenkrans had, met
eene pint in het midden. Zij dronken er zeventien pinten dobbele kuite
en nog meer. Want in Vlaanderen, als men nat is, droogt men zich met
een vuur van bier in den buik. Zoo werd Uilenspiegel voor de vierde
maal gedoopt.

Met het hoofd zwaarder dan 't lichaam, strompelden ze huiswaarts;
zoo kwamen ze aan een brugje over eenen poel; Katelijne, die
meter was, droeg het kind; zij struikelde en viel in de modder met
Uilenspiegel. Zoo werd hij voor de vijfde maal gedoopt.

Men trok hem uit den poel. In 't huis van Klaas werd hij met lauw
water gewasschen. Dit was zijn zesde doopsel.




VII.

Dien dag besloot Zijne Heilige Majesteit keizer Karel, groote feesten
te houden, om de geboorte van zijn zoon te vieren. Evenals Klaas,
besloot hij uit visschen te gaan, niet in de vaart, doch in de beurzen
en tasschen zijner onderdanen. Daaruit is het dat vorstelijke lijnen
gouden karolussen, gouden lammeren, rozenobels, dubloenen, zilveren
daelders en al die wonderbare visschen trekken, die, naar willekeur
van den visscher, veranderen in fluweelen kleederen en schitterende
edelgesteenten, in lekkeren wijn en smakelijke gerechten. Want de
rivieren, die 't rijkst zijn aan visch, zijn niet die, waarin het
meeste water is.

Nadat Zijne Heilige Majesteit zijn raad bijeengeroepen had, besloot
hij, dat de vangst volgenderwijze geschieden zou:

De genadige infant zou rond negen of tien uren ten doop gebracht
worden; ten blijke van groote vreugde, zouden de inwoners van
Valladolid heel den nacht, op eigen kosten, feesten en kermissen,
en ten bate der armen, hun geld op de Groote Markt strooien.

Op vijf punten zou eene fontein, tot aan den dageraad toe, goeden
wijn spuiten, die door de stad moest betaald worden. Op vijf andere
plaatsen zouden, op houten kramen, allerhande worsten, ossetongen en
pasteien uitgestald worden, mede ten laste van de stad.

Op eigen kosten zouden de lieden van Valladolid, op den doortocht van
den stoet, in grooten getale zegebogen oprichten, den Vrede, het Geluk,
den Overvloed, de Fortuin voorstellend, en allerhande zinnebeeldige
toespelingen op de gaven des hemels, waarmede zij onder de regeering
van Zijne Heilige Majesteit begunstigd waren.

Ten slotte en behalve deze bogen van pais, zouden er andere opgericht
worden, waarop, in helle kleuren, minder goedertieren kenteekenen
zouden prijken, zooals arenden, leeuwen, lansen, hellebaarden,
vlammende spiesen, kanonnen, falkonetten, slangen met wijden mond,
mitsgaders al ander oorlogstuig, om op zinnebeeldige wijze de macht
en de kracht van Zijne Heilige Majesteit voor te stellen.

En, voor het verlichten der kerk zou, als een blijk van de genade
Zijner Majesteit, aan het gilde der keersgieters toegestaan worden,
voor niet, over de twintig duizend waskeersen te leveren, waarvan de
onopgebrande einden naar 't kapittel zouden gaan.

Al de andere kosten zou de keizer zelf betalen, om aldus te toonen,
dat het Zijner Goedertierenheid behaagde, zijne volkeren niet te zeer
te belasten.

Als de gemeente die bevelen uitvoerde, kwamen jammerlijke tijdingen uit
Rome. Oranje, Alençon en Frundsberg, bevelhebbers van den keizer,
waren binnen de heilige stede gedrongen en hadden er kerken,
kapellen en huizen verwoest en geplunderd, niemand, priesters,
nonnen, moeders noch kinderen, sparend. Den Heiligen Vader hadden zij
gevangengenomen. De plundering duurde reeds een volle week; ridders en
landsknechten doolden door Rome, zwelgend en brassend, met de wapens
zwaaiend, op zoek naar de kardinalen, roepende en tierende, dat zij
hen allen derwijze verminken zouden, dat geen hunner ooit paus zou
worden. Enkelen hadden die bedreiging reeds ten uitvoer gebracht en
dweilden langs de straten met halssnoeren van acht-en-twintig of meer
bloedige bollen, groot als okkernoten. De wegen leken roode beken,
waarin de verminkte lijken der vermoorden lagen.

Onder het volk werd gezegd, dat de keizer, die geld noodig had, er
wilde visschen in het bloed van de priesters, en dat hij bekend met
het tractaat, den gevangen paus door zijne bevelhebbers opgelegd, hem
dwong afstand te doen van al de versterkte plaatsen zijner Staten,
400.000 dukaten te betalen en gevangen te blijven totdat aan die
voorwaarden voldaan was.

Nochtans was de droefheid van Zijne Majesteit zoo groot, dat hij al
de toebereidselen van vreugde, feesten en vermakelijkheden afzegde
en den heeren en edelvrouwen van zijn huis beval den rouw aan te nemen.

En de infant werd gedoopt in zijn witte doeken, ten teeken van
koninklijken rouw.

Dat alles aanschouwden de heeren en edelvrouwen als voorteekenen
van rampspoed.

Desniettemin toonde de voedster den infant aan de edelen en edelvrouwen
van het koninklijk huis, opdat zij hem, naar aloud gebruik, hunne
wenschen en giften zouden bieden.

Mevrouw de la Coena hing om zijn hals een zwarten steen tegen het
vergif, zoo rond en zoo groot als eene hazelnoot, in een gouden
ring gevat; Mevrouw de Chaussade bond aan een zijden draadje eene
schelp, wolfsmuil geheeten, hangende op zijne maag, voor de goede
spijsvertering; messire Van der Steen, uit Vlaanderen, bood hem een
Gentsche worst, vijf ellebogen lang en een halven dik, en wenschte
daarbij hoogstnederig aan Zijne Hoogheid, dat hij, alleen op den
reuk van de worst, dorst mocht krijgen naar Gentschen klauwaard,
daarbij voegende dat, al wie het bier eener stad lust, de brouwers
niet kan haten; messire jonker Jacob Christoffel van Castilië bad
Zijne Hoogheid den Infant een groenen jaspis aan zijn doorluchtige
voetjes te willen dragen, opdat hij goed zou kunnen loopen. Jan de
Paepe, de nar, die daar ook was, sprak toen:

--Messire, geef hem liever den horen van Jozua, op wiens geschal al de
steden, met alles wat er in was aan mannen, vrouwlieden en kinderen,
zich in beweging zetten en liepen. Want Zijne Hoogheid moet niet
leeren zelf te loopen, maar wel de anderen te doen loopen.

De bedrukte weduwe van Floris van Borsele, in leven heer van Veere
in Zeeland, schonk aan Zijne Hoogheid Philippus eenen steen die,
naar zij zegde, de eigenschap had de mannen verliefd en de vrouwen
ontroostbaar te maken.

Maar de infant schreide zonder ophouden.

Uilenspiegel schreide ook, maar Klaas stak hem een wisschen klater
met belletjes in de hand, deed hem op zijne hand dansen en sprak:
Klingelingeling, hadt gij maar altijd belletjes aan uw kaproen,
mijn zoon, want de gekken zijn meester van de wereld.

En Uilenspiegel lachte zijn vader toe.




VIII.

Klaas had een grooten zalm gevangen, die op een Zondag gegeten werd
door hem en ook door Soetkin, Katelijne en den kleinen Uilenspiegel;
doch Katelijne at niet meer dan een vogelken.

--Maar, zei Klaas tot haar, is Vlaanderens lucht tegenwoordig zoo
voedzaam, dat gij maar moet ademhalen om gespijsd te wezen als met
een teil vleesch? Wanneer zal men kunnen leven zonder eten? De regen
moest goede soep zijn, de hagelsteenen erwten en de sneeuw stoverije;
dat zou den armen pelgrims versterking geven.

Katelijne schudde zwijgend het hoofd.

--Maar, moet gij daar zoo jammerend zitten? zei Klaas. Wat scheelt
er aan?

Toen sprak Katelijne met eene stem, zacht als een ademtocht:

--De booze geest, de zwarte nacht valt neer.--Daar meldt hij zijne
komst, met het geschreeuw van den nachtuil.--Rillend aanroep ik--te
vergeefs--de Heilige Maagd.--Voor hem, muren noch hagen, deuren noch
vensters.--Licht als een geest, dringt hij overal binnen.--Krakende
ladder.--Hij is bij mij, op den zolder waar mijne legerstee staat.--Hij
grijpt mij in zijn koude armen, als marmer zoo hard.--IJskoud is zijn
gelaat, en zijn kussen vochtig als de sneeuw.--De stroohut schudt en
slingert als een schuitje op de woelige zee....

--Elken morgen, zei Klaas, moet gij ter misse gaan, opdat de Heer
Jezus U de kracht geve dat helsche spook te verjagen.

--Hij is zoo schoon! sprak zij.




IX.

Als Uilenspiegel gespeend was, groeide hij op lijk een boom.

Dan kuste zijn vader hem zoo dikwerf niet meer, maar voedde hem streng
op, opdat hij geen weekeling worden zou.

Als Uilenspiegel thuis kwam en kloeg, dat hij, bij een of anderen
twist, klop gekregen had, kreeg hij er nog klop bij van Klaas,
omdat hij de anderen niet geklopt had: en, aldus opgebracht, kreeg
Uilenspiegel den moed van een jongen leeuw.

Als Klaas er niet was, vroeg Uilenspiegel aan Soetkin een duit om te
spelen. Dan was Soetkin boos en sprak:

--Waarom moet ge gaan spelen? Blijf liever thuis, om mutsaards
te binden.

En als zij niets gaf, begon Uilenspiegel te blaten als een lam. Maar
Soetkin maakte dan veel leven met potten en pannen, om te gebaren
dat ze hem niet hoorde. Dan weende Uilenspiegel, en de zoete moeder
liet hare geveinsde hardheid af, kwam tot hem, streelde hem en vroeg:
"Hebt gij genoeg met een denier?" Nu, gij moet weten, dat een denier
zes duiten gold.

Zoo beminde zij hem te veel en, als Klaas er niet was, was Uilenspiegel
baas in huis.




X.

Op een morgen zag Soetkin haren man met gebogen hoofd in de keuken
staan, in gedachten verdiept.

--Wat scheelt er toch, man? vroeg zij. Ge ziet bleek, gij zijt kwaad
en verstrooid.

Met eene stem, als een hond die bromt, antwoordde Klaas:

--De wreede plakkaten des keizers gaan ze weer uithalen. Opnieuw gaat
de dood over Vlaanderenland heerschen. De aanbrengers krijgen de helft
van de have der slachtoffers, als de have de honderd karolusgulden
niet te boven gaat.

--Wij zijn arm, sprak zij.

--Arm, zeide hij,... niet arm genoeg. Er zijn lage zielen, gieren
en raven, die ons zouden aanklagen, zoowel om een zak kolen als om
een zak karolussen met Zijne Majesteit te deelen. Wat bezat het arme
Tanneken, de weduw van Sies den kleermaker, die ze te Heist levend
begroeven? Een Latijnschen bijbel, drie gouden florijnen en wat potten
van Engelsch tin, waarop eene buurvrouw loerde. Wantje Martens werd
eerst in 't water geworpen; haar lijf dreef boven, en daarin zag
men hekserij, weshalve zij als tooveres verbrand werd. Zij had wat
gebroken meubelen, zeven gouden karolussen in een lederen tassche,
en de aanklager vroeg er de helft van. Eilaas! nog tot morgen zou ik
aldus kunnen spreken: maar wat baat het, vrouw: in Vlaanderen is het
leven onhoudbaar om den wille van de plakkaten. Welhaast zal telken
nacht de kar van den Dood dof door de straten rijden en wij zullen
zijne beenderen hooren rammelen.

Soetkin sprak:

--Jaag me geen schrik aan, Klaas. De keizer is de vader van Vlaanderen
en Brabant; als dusdanig is hij braaf en grootmoedig, geduldig
en genadig.

--Daarbij zou hij te veel verliezen, antwoordde Klaas, want de
verbeurdverklaarde goederen komen hem bij erfenis toe.

Plotseling hoorde men de trompet en de cimbels van den
stadsuitroeper. Op dat geluid kwamen Klaas en Soetkin, die beurt om
beurt Uilenspiegel op den arm droegen, met de volksmenigte toegeloopen.

Zoo kwamen zij aan het schepenhuis. Voor de pui waren de herauten te
peerd, op bazuinen blazend en op cimbels slaande, de provoost met de
roede der justitie in de hand en de stadsprocureur, ook te peerd,
die eene ordonnantie des keizers in de hand hield en zich gereed
maakte ze aan vergaderde volksmenigte voor te lezen.

Klaas vernam, dat het andermaal aan allen in 't algemeen en aan elk in
't bijzonder verboden was, te drukken, te lezen, in bezit te hebben of
voor te staan, de boeken, schriften of leerstellingen van Martinus
Luther, van Joannes Wycliff, Joannes Huss, Marcilius van Padua,
Æcolampadius, Ulricus Zwinglius, Philippus Melanchton, Franciscus
Lambertus, Joannes Pomeranus, Otto Brunselsius, Justus Jonas, Joannes
Pupperis en Gorcianus, de Nieuwe Testamenten gedrukt door Adriaan
van Bergen, Christoffel van Roemonde en Joannes Zell, vol Luthersche
en andere heresiën, verworpen en veroordeeld door de Faculteit der
godgeleerdheid van de Universiteit van Leuven. Mitsgaders te maken of
te doen maken smadelijke konterfeitsels of afbeeldsels van God, van de
heilige Maagd Maria of van de santen; te breken, te scheuren of uit te
wisschen de beelden of konterfeitsels, vervaardigd tot verheerlijking
van en tot aandenken aan God en de Maagd Maria of de heiligen der kerk.

Verder zei het plakkaat, dat het aan niemand toegelaten was, tot welken
staat hij ook mocht behooren, zich te vermeten de Heilige Schrifture te
bespreken of over haar te twisten, zelfs niet op twijfelachtige punten,
tenzij door een godgeleerde van naam, erkend door eene Universiteit,
daartoe gemachtigd.

Onder andere straffen besliste Zijne Heilige Majesteit, dat
de verdachten nooit of nimmer een eerbaar ambt zouden kunnen
bekleeden. En zij, welke in hunne dolingen hervielen of bleven
volharden, zouden veroordeeld worden met een zacht of hard vuur,
in een strooien huis of gebonden aan een paal te worden verbrand,
al naar de sententie van den rechter. De anderen zouden omgebracht
worden door het zweerd als zij edelen of goede burgers waren, de
gemeene manslieden aan de galg geknoopt en de vrouwlieden levend
begraven. Om tot voorbeeld te strekken, zou hun hoofd op een paal
worden gestoken. Ten profijte van den keizer was er verbeurte hunner
goederen, overal waar verbeurdverklaring geschieden kon.

Zijne Heilige Majesteit schonk den aanbrengers de helft van al
hetgene de aflijvigen in eigendom bezeten hadden, zoo die have de
somme van honderd pond grooten, Vlaamsche munte, alles in 't alles,
niet te boven ging. En wat aanging het deel van den keizer, dit
zou hij aanwenden voor werken van godsvrucht en van bermhertigheid,
gelijk bij de plundering van Rome was geschied.

En treurig keerde Klaas naar huis, met Soetkin en Uilenspiegel.




XI.

Daar het een jaar van voorspoed geweest was, kocht Klaas voor zeven
florijnen een ezel en negen halsters boonen, en op een morgen besteeg
hij zijn beest. Uilenspiegel zat van achteren. Aldus gingen zij hun
oom en oudsten broeder, Judocus Klaas, bezoeken, die woonde omtrent
Meiborg, in de Duitsche landen.

Judocus, die in zijne jeugd eenvoudig en zacht van aard was geweest,
had door vele geleden onrechtveerdigheden haat tegen de menschen
opgevat en leefde in eenzaamheid.

Zijn vermaak was, twee zoogezeid trouwe vrienden met elkander te doen
vechten, en hij gaf drie oortjes aan hem, die zijn vriend het ergst
toegetakeld had.

Ook bracht hij geerne, in een warme kamer, in grooten getale,
twistzieke oude wijven bijeen en gaf haar geroosterd brood en
kruidenwijn.

Aan de vrouwen, die meer dan zestig jaar oud waren, stelde hij saaie
ter hand, die zij in een hoek moesten opbreien; daarbij beval hij haar
altijd aan, de nagels lang te laten groeien. En 't was wonderlijk
ze te hooren kuchen, babbelen, snappen en, met hare priemen onder
de oksels, te zamen den naam en de eer van den evennaaste te hooren
schenden en rooven.

Wanneer Judocus zag, dat zij goed in gang waren, smeet hij eenen
borstel in 't vuur, die door het schroeien der haren de lucht met
een geweldigen stank vervulde.

Dan begonnen de wijven al te gelijk te kijven en elkaar te beschuldigen
de oorzaak te zijn van den stank: en allen streden het af en vlogen
weldra elkander in 't haar; en dan wierp Judocus opnieuw borstels in
het vuur en paardenhaar op den vloer. Als het gevecht zoo verwoed en
de rook zoo dik werd, en het stof zoo hoog steeg, dat hij niets meer
zien kon, ging hij zijne twee in stadsserjanten verkleede knechts
halen, die de ouden als woedende ganzen met groote stokslagen uit de
kamer verdreven.

En toen Judocus het slagveld overzag, vond hij er lappen van rokken,
van kousen, van hemden en ook oude tanden.

En droefgeestig zei hij tot zich zelven:

--Mijn dag is verloren, niet eene van haar heeft hare tong
achtergelaten.




XII.

In het baljuwschap Meiborg ging Klaas door een smal boschje: de
ezel hapte hier en daar naar een distel; Uilenspiegel smeet zijne
kaproen naar de vlinders en ving ze weer op, zonder van den ezel te
komen. Klaas at eene snede brood en nam zich voor, die in de naaste
taveerne te begieten. Van verre hoorde hij een klokje kleppen en een
gedruisch als van vele menschen die altegader spreken.

--'t Is eene bedevaart, en de heeren pelgrims zijn zeker in grooten
getale. Houd u goed vast, mijn zoon, dat zij u niet van het grauwtje
stooten. Wij zullen zien. Komaan, ezeltje, wat gauwer, toe!

En de ezel draafde.

Zij verlieten den zoom van het bosch en daalden naar een groote vlakte,
ten Westen door eene rivier bespoeld. Aan den Oosterkant stond een
kleine kapel, den gevel versierd met een beeld der Lieve-Vrouwe, met
twee stieren aan heure voeten. Op de trappen van de kapel stonden een
heremiet--die giegelend, aan 't kleppen was--vijftig staffieren met
brandende keersen in de hand, spelers, klokluiders en trommelslagers,
klaroenblazers, pijpers, schalmei- en doedelzakspelers, alsmede een
hoop lustige gezellen, die bakken vol oudroest in de handen hielden,
doch voor het oogenblik allen stille zwegen.

Meer dan vijf duizend pelgrims, in gesloten gelederen, elk van zeven
man, met helmen op en stokken van groen hout in de hand, gingen hen
voorbij. Dan schaarden zij zich, telkens zeven, vóór de kapel. Zij
lieten hunne stokken zegenen en kregen elk eene keers uit de handen
der staffieren, in ruil waarvan zij den heremiet een halven florijn
betaalden.

En hunne processie was zoo lang, dat de keersen van de eersten
opgebrand waren, toen die van de laatsten nog hare volle lengte hadden.

Klaas, Uilenspiegel en de ezel verlustigden zich met aldus een groote
verscheidenheid breede, hooge, lange, puntige, fiere, ronde of slappe
buiken te zien voorbijgaan.

Al de pelgrims hadden helmen op. Er waren er die van Troje kwamen,
andere, die phrygische mutsen leken. Sommige pelgrims hoewel met bolle
wangen en dikke buiken, droegen helmen met uitgespreide vleugelen,
doch hadden geenerlei zin tot vliegen. Anderen waren gekapt met
zoogenaamde "salades", door de slakken onwaardig gekeurd omdat ze
niet groen genoeg waren.

Maar het meerendeel had helmen, zoo oud en verroest, dat ze uit den
tijd schenen te zijn van Gambrinus, koning van Vlaanderen en koning
van het bier, dewelke regeerde negenhonderd jaar vóór Christus en eene
pint op zijn hoofd droeg, uit vrees niet op tijd te kunnen drinken,
bij gebrek aan een beker.

Eensklaps begonnen klokken, pijpen, schalmeien, trommelen en het
oudroest te kleppen, te fluiten, te schallen, te slaan en te kletteren.

Het was het sein voor de pelgrims zich omme te keeren en bij groepen
van zeven zich nu tegenover elkaar te plaatsen. Als uitdaging stak elk
de brandende keers in het gelaat van zijn overman. Daardoor ontstond
groot genies en daarna regende het stokslagen.

Ze vochten en sloegen met handen en voeten, met hoofden, met alles. Er
waren er, die, gelijk de rammen, op hunne tegenstrevers vielen, met
den helm vooruit, die bij den eersten schok over hunne ooren schoot,
en als blinden terechtkwamen op zeven andere woedende pelgrims,
die hen verwelkomden, maar niet met zachtheid.

Anderen, schreeuwers en bloodaards, jammerden om de ontvangen slagen,
maar bij het prevelen hunner gebeden werden ze bliksemsnel door nieuwe
zeventallen overvallen en zonder genade omvergeloopen of omvergetrapt.

En de heremiet lachte.

Verderop zag men zeventallen, die als klissen aan elkaar hingen en
van boven naar beneden in het water rolden; maar zij bleven elkaar
toetakelen en ranselen, zonder dat het water hunne woede bekoelde.

En de heremiet lachte.

Zij, die boven gebleven waren, sloegen elkander de oogen blauw en de
tanden vaneen, rukten elkanders haren uit, en scheurden wambuizen en
hoozen aan stukken.

En de heremiet lachte en sprak:

--Dapper aan, vrienden: wie 't hardst slaat, bemint het meest. Aan de
kloekste vechters, de schoonste liefjes! Hier ziet Onze Lieve Vrouw
van Rindbisbels, wie man is!

En de pelgrims sloegen als op kaf.

Middelerwijl was Klaas den heremiet genaderd, terwijl Uilenspiegel
lachend en gierend op de slagen bleef toekijken.

--Eerwaarde vader, vroeg hij, welke misdaad hebben die arme sukkelaars
bedreven, om elkander zoo wreedelijk te mishandelen?

Doch zonder hem te aanhooren, riep de heremiet:

--Luieriken! gij verliest den moed. Als de vuisten moede zijn, zijn
de voeten het immers nog niet! Zijn er onder U, die beenen hebben om
te vluchten als hazen? Wat doet het vuur uit de steenen springen? Het
ijzer, dat er op slaat!

Op die woorden gingen die onnoozele pelgrims voort te vechten met
helmen, met handen en met voeten. 't Was een verwoede strijd, waarvan
Argus met zijn honderd oogen niets hadde gezien dan stofwolken en
hier en daar de punt van een helm.

Doch eensklaps begon de heremiet te kleppen. Pijpen, trommelen,
trompetten en schalmeien en het oudroest staakten hun gedruisch,
tot teeken van vrede.

De pelgrims brachten nu hunne gekwetsten bijeen. Er waren er, wier
tong, gezwollen van gramschap, uit den mond hing. Maar die ging van
zelve in hare verblijfplaats terug. Moeilijker was het om de helmen
af te trekken, die tot ver over de ooren zaten. Zij schudden den kop
en bleven hem schudden: de helmen waren vast gelijk groene pruimen
aan den boom.

Doch toen sprak de heremiet:

--Leest elkeen een ave en keert terug naar uw wijf. En binnen negen
maanden zullen evenveel kinderen meer in het baljuwschap zijn, als
heden 't gevecht dappere strijders telde.

En de heremiet zong het ave voor, en allen zongen het mee. En de
klok klepte.

De heremiet zegende hen in name van Onze Lieve Vrouwe van Rindbisbels
en sprak tot de pelgrims:

--Gaat in vrede!

En roepend en stompend en zingend, trokken zij naar Meiborg terug. Al
de vrouwen, oude en jonge, wachtten hen op den dorpel van de huizen,
waar zij binnenvlogen als soldeniers in een stormenderhand veroverde
stad.

De klokken van Meiborg luidden al te gader: de jongens floten, riepen,
speelden op den rommelpot.

Pinten en stoopen, bekers en glazen gingen lustig aan 't klinken en
rinkelen. En de wijn vloeide in de kelen als een stroom in de zee.

Terwijl de klokken luidden en de wind, bij vlagen, aan Klaas 't gezang
van mannen, vrouwlieden en kinderen bracht, vroeg hij opnieuw aan den
heremiet, welke hemelsche gratie die sukkelaars hoopten te verkrijgen,
na die hardhandige oefeningen.

Lachend antwoordde hem de heremiet:

--Op die kapel daar, ziet ge twee gekapte beelden, die twee stieren
voorstellen. Zij staan daar ter herinnering aan het mirakel van den
heiligen Martinus, die twee ossen in stieren veranderen deed, door
hen met de horens te doen vechten. Daarna streek hij meer dan een
uur keersvet over hunnen snuit, en sloeg er met den stok op.

Welnu, ik kende het mirakel. Ik vroeg Zijne Heiligheid om eene
vergunning, die ik duur betaalde en kwam mij vestigen in dit oord.

Toen preekte ik over het wonder en weldra kregen al de mannen, zoo
ouden als jongen, de zekerheid dat Onze Lieve Vrouwe hun genadig was
als ze goed gevochten hadden met de keers die de zalf, en den stok die
de kracht is. Hierheen is het, dat de vrouwen heuren man sturen. De
kinderen, die uit kracht van de bedevaart verwekt zijn, worden vlug
en wreedaardig, geweldig en roekeloos en, later, vrome soldaten.

Eenklaps vroeg de heremiet aan Klaas:

--Herkent gij mij?

--Ja, sprak Klaas, gij zijt mijn broeder Judocus.

--Gij zijt er, antwoordde de heremiet, maar wie is die bengel daar,
die leelijke gezichten naar mij trekt?

--'t Is uw neef, was 't antwoord van Klaas.

--Welk verschil maakt gij tusschen keizer Karel en mij?

--'t Is groot, sprak Klaas.

--'t Is klein, wedervoer Judocus: de keizer doodt de menschen en bij
mij krijgen ze klop, tot ons beider profijt en vermaak.

Dan bracht hij Klaas en Uilenspiegel naar zijne kluis, waar zij elf
dagen achtereen kermis vierden.




XIII.

Als Klaas afscheid nam van zijn broer, steeg hij op zijn ezel, met
Uilenspiegel achter zich. Op de Markt van Meiborg stonden velerhande
pelgrims en als zij hen zagen, ontstaken ze in woede en hieven de
stokken dreigend omhoog. En allen riepen "Schelm! Nietdeug!" om
den wille van Uilenspiegel, die zijne hooze losgemaakt en zijn hemd
opgetrokken had, en zijne achterkaken liet zien.

Klaas, ziende dat ze zijn zoon bedreigden, vroeg hem:

--Wat hebt gij gedaan, dat zij zoo kwaad op u zijn?

--Vadertjelief, antwoordde Uilenspiegel, ik zit op den ezel en zeg
tot niemand een woord, en toch schelden ze mij uit voor een nietdeug.

Toen deed Klaas hem langs voren zitten.

In die postuur stak Uilenspiegel de tong uit naar de pelgrims, en
roepend en tierend balden ze hunne vuisten en dreigden met hunne
stokken Klaas en den ezel.

Maar Klaas sloeg op zijn ezel om hunne woede te ontvlieden. Toen de
pelgrims hen met rust lieten, sprak Klaas tot zijn zoon:

--Gij zijt onder een zeer slecht gesternte geboren, want gij zit vóór
mij, doet niemand kwaad en toch willen ze u dooden!

Uilenspiegel hield zijn buik vast van 't lachen.

Terwijl Klaas door 't Land van Luik reed, hoorde hij zeggen, dat
die van Rivage hongersnood leden en dat ze gesteld waren onder
de jurisdictie van den officiaal, eene vierschaar van geestelijke
rechters. Zij maakten opstand om brood en om wereldlijke rechters
te bekomen. Eenigen werden onthoofd of gehangen, anderen uit het
land gebannen; dàt was de goedertierenheid van den zachtzinnigen
aartsbisschop, den hoogweerdigen Van de Marck.

Klaas zag onderwege de gebannenen, die de zoete vallei van Luik
ontvloden, en, aan de boomen, omtrent de stad, zag hij de lijken van
hen die gehangen waren, omdat zij de misdaad begaan hadden, honger
te hebben. En Klaas schreide over hunnen rampspoed.




XIV.

Toen Klaas op zijn ezel weer thuis kwam met een zak vol oortjes,
dien hij van zijn broeder gekregen had en ook met een schoonen beker
van Engelsch tin, was 't Zondag en weekdag kermis in de arme stulp;
alle dagen at men boonen met vleesch.

Menigmaal vulde Klaas den schoonen beker met schuimende dobbele kuite.

Uilenspiegel at voor drie; hij ging en kwam naar de borden en teilen
als eene musch op een graanzolder.

Eet gij het zoutvat niet mee? vroeg Klaas.

Uilenspiegel antwoordde:

--Wanneer, gelijk hier, het zoutvat gemaakt is van een uitgeholde
korst brood, moet men het soms opeten, anders komen er wormen in.

--Waarom, zegde Soetkin, veegt gij uwe vettige handen af aan uwe hooze?

--Aan mijne hooze? wel, om nooit met natte billen te loopen.

Daarop dronk Klaas een groote teug bier uit zijn tinnen beker.

Uilenspiegel vroeg hem:

--Waarom hebt gij zoo'n grooten beker en ik maar een klein kroezeken?

Klaas antwoordde:

--Omdat ik uw vader en de baas van het huis ben.

Doch Uilenspiegel hernam:

--Gij drinkt al veertig en ik nog maar negen jaar; gij hebt al genoeg
gedronken en mijne beurt is gekomen. Geef mij den beker en neem gij
het kroezeken.

--Zoon, sprak Klaas, men giet geen vat bier in een vaatje over
zonder morsen.

--Nu ga dan te werk met verstand en giet uwe kan in mijn tonne,
want mijn buik is grooter dan uw beker, antwoordde Uilenspiegel.

En lachend liet Klaas hem zijn beker ledigen. En zoo leerde
Uilenspiegel listig worden om bier te krijgen.




XV.

Onder haren gordel droeg Soetkin het kenmerk van een nieuwe
bevruchting; ook Katelijne was zwanger, maar zij dorst heur huis
niet verlaten.

Soetkin ging haar bezoeken.

--Ach! sprak zij jammerend, wat ga ik aanvangen met de ongelukkige
vrucht van mijn lichaam? Moet ik het wichtje versmachten? Ik zou
het besterven! Maar zoo ik een kind heb zonder getrouwd te zijn,
zullen de serjanten mij pakken. Ik zal, als een ontuchtige deerne,
twintig gulden moeten betalen, en op de groote markt gegeeseld worden.

Om haar te troosten, sprak Soetkin heur eenige zoete woorden
toe. Bezorgd en nadenkend keerde zij huiswaarts. Op een morgen sprak
zij tot Klaas:

--Zoudt ge mij slaan, Klaas, als ik u twee kindjes schonk in stee
van maar één?

--Dat weet ik niet, antwoordde Klaas.

--Maar, sprak Soetkin, als het tweede kindje niet uit mijn lichaam
kwam en, gelijk dat van Katelijne, verwekt was door een onbekende,
door den duivel misschien?

--De duivel, antwoordde Klaas, verwekt wel vuur en dood en rook,
maar geen kinderen. Het kind van Katelijne zal ik als het onze aanzien.

--Zoudt gij dat? vroeg zij.

--Gelijk ik u zeg, hernam Klaas.

Soetkin ging die goede mare aan Katelijne kondschappen en uiterst
gelukkig en opgetogen riep deze uit:

--De goede man heeft gesproken voor 't heil van mijn lichaam. God zal
hem zegenen, en ook de duivel, sprak zij huiverd, als 't een duivel
is, die U verwekte, arm schaapje, dat in mijn boezem leeft.

Soetkin bracht een zoon en Katelijne eene dochter ter wereld. Beiden
werden ten doop gebracht als zoon en dochter van Klaas. De knaap werd
Hans genoemd, maar bleef niet in leven; het meisje werd Nele geheeten
en groeide flink op.

Aan vier bekers dronk zij levenssap: aan de borsten van Soetkin en
aan die van Katelijne. En een zoete strijd ontstond tusschen de twee
vrouwen, om de kleine de borst te mogen geven. Maar tot haar groot
leed, moest Katelijne heure melk laten verdrogen, want men hadde heur
gevraagd van waar die kwam, zonder dat zij moeder was.

Als Nele gespeend was, nam Katelijne heure dochter bij zich en liet
haar niet eerder naar Soetkin gaan, dan nadat zij heur "moeder"
genoemd had.

En de buren zeiden, dat het schoon was van Katelijne, die have en
goed bezat, het kind op te voeden, want Soetkin en Klaas leefden
veelal in kommer en armoe.




XVI.

Op zekeren morgen was Uilenspiegel alleen thuis. Hij verdroot zich
geweldig, en nam een schoen van zijn vader, om er een schuitje van
te maken. De groote mast stond reeds vast in de zool en Uilenspiegel
ging een gat snijden in 't overleer, om den boegspriet te plaatsen,
toen hij over 't halfdeurken het hoofd van een ruiter en den kop van
een peerd zag.

--Is hier niemand? vroeg de ruiter.

--Ja, antwoordde Uilenspiegel, een mensch, een halve mensch en een
paardekop.

--Hoezoo? vroeg de ruiter.

Uilenspiegel sprak:

--Wel, ik zie hier een heelen mensch en die ben ik; verder zie ik
een halven mensch, te weten, uw hoofd en borst, en daarbij nog den
kop van uw peerd.

--Waar zijn uw vader en moeder? vroeg de man.

--Vader gaat van kwaad tot erger en moeder is bezig met ons in scha
of schande te brengen.

--Dat begrijp ik niet, sprak de ruiter.

Uilenspiegel hernam:

--Vader graaft de voren van zijn land dieper, om de jagers, die zijn
koren plat trappen, van kwaad in erger te doen vallen. Moeder is geld
gaan leenen: geeft zij te veel weer, dan is het ons scha en geeft ze
te weinig, dan is het ons schande.

Toen vroeg de man hem den weg.

--Daar, waar de eenden gaan, antwoordde Uilenspiegel.

De ruiter ging heen, doch als Uilenspiegel bezig was met van Klaas'
tweeden schoen eene galei te maken, kwam hij terug.

--Gij hebt mij bedrogen, sprak hij; daar waar de eenden zijn, is het
modder en veengrond, waarin zij ploeteren.

Uilenspiegel antwoordde:

--Ik zei u niet van te rijden waar zij ploeteren, doch daar waar
zij gaan.

--Wijs mij ten minste den weg, die naar Heist gaat, sprak toen de man.

--In Vlaanderen, zei Uilenspiegel, zijn 't de menschen die gaan,
en de wegen blijven liggen.




XVII.

Op zekeren dag sprak Soetkin tot Klaas:

--Man, ik heb den dood op het lijf. 't Is nu al drie dagen, dat Thijl
uit den huize is. Waar mag hij wel zijn?

Treurig antwoordde Klaas:

--Hij is waar de straathonden zijn, op den grooten weg, met nietdeugen
van zijne soort. God was vol wreedheid, toen hij ons zulk een zoon
gaf. Toen Thijl ter wereld kwam, zag ik in hem de vreugd van onzen
ouden dag, een werktuig te meer in ons huis; ik meende hem een
handwerk te leeren, maar 't boosaardige noodlot maakt hem tot een
schelm, een dagdief.

--Wees niet te gestreng, man, sprak Soetkin. Onze zoon is maar negen
jaar, hij is nog in den roes van de eerste jeugd. Moet hij, als de
boomen, niet eerst zijne hulsels afwerpen, alvorens zich te kunnen
tooien met zijne bladeren, die, voor den boom des volks, de eer en de
deugd zijn? 't Is een kleine guit, ik weet het, maar zijne slimheid zal
hem later te goede keeren, als hij ze tot een of ander goed ambacht
aanwendt, in stee van ze tot kwade parten te gebruiken. Hij steekt
geerne den draak met een ieder; maar later zal hij zijn plaats vinden
in een lustige broederschap. Hij lacht gedurig; maar de gezichten,
die zuur zien vóór hunne rijpheid, zijn een slecht voorteeken voor
later. Zoo hij loopt, is 't dat hij zulks noodig heeft om te groeien;
zoo hij niet werkt, is het dat hij nog niet begrijpt, dat werken een
plicht is en als hij somwijlen dag en nacht, een halve week uitblijft,
is het dat hij niet beseft hoeveel verdriet hij ons aandoet, want
hij heeft een goed hart en ziet ons geerne.

Klaas schudde het hoofd en antwoordde niet, en toen hij sliep,
lag Soetkin te weenen. En 's morgens, als zij dacht dat haar zoon
wellicht ergens aan den weg ziek lag, ging zij op den dorpel der
deure zien of hij niet afkwam; maar zij zag hem niet en zij zette
zich aan 't venster, om van daar naar de straat te kijken. En meer
dan eens bonsde heur het hert in de borst, als zij den lichten stap
van een kind hoorde; maar als de kleine voorbijging en zij zag dat
het Uilenspiegel niet was, weende zij weer, de arme moeder.

Doch Uilenspiegel was, met zijn deugnieten van kameraden, te Brugge
op de Zaterdagsmarkt.

Daar zag men leerzenmakers en schoenlappers in hunne kramen,
kleermakers met hoozen, wambuizen, bovenkerels; Antwerpsche
meezenvangers, die 's nachts met een uil ter vogelvangst gaan; daar
waren kooplui in wild, hondenvangers, verkoopers van kattevellen voor
handschoenen, borstlappen en kragen, en koopers uit alle standen,
poorters en poorteressen, knechten en dienstmaagden, broodmeesters,
botteliers, eierboeren en -boerinnen en men hoorde ze, ieder op zijn
wijs, vragen en bieden, de waren prijzen en afkeuren.

In een hoek van de markt was een schoone lijnwaden tente opgericht op
vier palen. Aan den ingang van die tente stond een boer uit het Land
van Aalst--met twee monniken naast zich om het geld te ontvangen--die
voor een oortje aan de nieuwsgierigen een stukje van het schouderblad
van de heilige Maria van Egypte liet zien. Met schorre, heesche stemme,
roemde hij de verdiensten der gelukzalige; in zijnen lofzang vergat
hij zelfs niet te zeggen hoe ze eens, bij gebreke aan geld, een jongen
veerman, die haar overgezet had, betaalde met schoone munt der nature,
om Gods gebod, omtrent het loon der werklieden, niet te overtreden.

En de twee monniken knikten om te bevestigen, dat de boer waarheid
sprak. Naast hen stond een groot, dik wijf, met een rood gezicht,
als Astarte zoo wulpsch, een oorverdoovend lawaai te maken op een
gebarsten doedelzak, terwijl een lieftallig meisje naast haar zong
als een nachtegaaltje, doch op haar lette niemand. Aan den ingang
van de tent wiegelde eene kuip, met de beide ooren aan twee staken
vastgemaakt. Als het wijf in hoogdravende woorden vertelde, dat het
eene kuip wijwater was, die van Rome kwam en de monniken weer knikten
om hare woorden te staven, verviel Uilenspiegel in diepe overpeinzing.

Aan een van de palen der tente stond een ezel gebonden, die meer hooi
dan haver kreeg: met hangenden kop zag hij naar de aarde, maar zonder
hoop er distels te zien opschieten.

--Jongens, riep Uilenspiegel, naar het wijf, de twee paters en 't
weemoedige grauwtje wijzend, vermits de meesters zoo goed zingen,
moet de ezel dansen. En hij liep naar een winkel, en kwam met zes
duiten peper. Toen hief hij den steert van den ezel op en wreef er
de peper onder.

Als het beest de peper gevoelde, keek het omme, om te zien van waar
die ongewone warmte kwam. Het meende, dat het den vuurduivel achter
de hielen had en wilde loopen om hem te ontvlieden; dan begon het
dier te balken en te stampen en uit al zijne kracht aan den paal te
trekken. Bij den eersten schok ging de kuip los, die tusschen de
twee haken hing, en al het wijwater kletste op de tent en op hen,
die er in waren. Weldra stortte de tent in en de aanwezigen, die naar
de geschiedenis van Maria van Egypte luisterden, lagen als onder een
natten mantel begraven. En Uilenspiegel en zijne vrienden hoorden van
onder het doek groote beroering en geweeklaag, want de geloovigen,
die binnen waren, beschuldigden malkander de kuip omvergetrokken
te hebben, en wit van woede brachten ze elkander vele vuistslagen
toe. Men zag het doek van de tente op en neer gaan, en telkens als
Uilenspiegel op het doek een ronden vorm zag uitkomen, stak hij er in
met eene speld. Dan hoorde hij grootere kreten en grootere uitdeeling
van vuistslagen onder de tente.

En hij had dolle pret en het meest toen hij den ezel zag wegloopen met
doek, met kuip en met palen achter zich aan, terwijl de baas van de
tent, zijn wijf en zijn kind zich vastklampten aan den sleep van den
ezel. Eindelijk kon het dier niet meer voort, het begon erbarmelijk
te balken en te stampen, en hield maar op om onder zijn steert te
zien of het vuur, dat er brandde, niet haast gebluscht was.

Maar de kwezelaars vochten voort en zonder aan hen te denken,
scharrelden de monniken het geld bijeen, dat uit de schaal gevallen
was.

Uilenspiegel hielp devotelijk mee, doch niet zonder profijt.




XVIII.

Terwijl de zoon van den kooldrager als een schalk opwies, bracht de
ziekelijke zoon van den grooten keizer zijne dagen in droefgeestigheid
door. Edelvrouwen en heeren zagen hem, door kamers en gangen van 't
paleis van Vallodolid, zijn armzalig lichaam op waggelende beentjes
voortsleepen, alsof hij moeite had de zwaarte te dragen van zijn
groot hoofd, met stekelig blond haar bedekt.

Steeds zocht hij de donkere gangen op en bleef er uren lang zitten, met
de beenen uitgestrekt. En als een dienstknecht er uit onachtzaamheid
op trapte, liet hij hem geeselen, en als hij hem hoorde huilen van
pijn, deed het hem goed, maar hij lachte niet.

's Anderen daags haalde hij elders in de gangen van het paleis
dezelfde streken uit. Edelvrouwen, heeren en schildknapen, die hem
voorbijgingen, deed hij struikelen, en als zij vielen en zich bezeerden
deed hem dat genoegen, maar hij lachte niet.

En zoo iemand hem aanraakte en niet viel, huilde hij alsof hij geslagen
was: en de ontsteltenis ziende, was hij blij, maar hij lachte niet.

Zijne Majesteit hierover verwittigd, beval, dat men geen acht op den
infant moest geven, zeggende, zoo hij niet wilde dat men hem trapte,
hij zijne voeten niet moest zetten waar eens anders beenen gingen.

Zulks mishaagde Philippus, doch hij zei niets; men zag hem niet meer,
tenzij in den tuin, wanneer hij, bij helderen zomer dag, zijn schraal
lichaampje in de zonne ging warmen.

En als keizer Karel, van den oorlog teruggekeerd, zijn zoon vol
somberheid zag zitten, sprak hij:

--Mijn zoon, hoe zeer verschilt gij van mij! Op uwen leeftijd klom ik
op de boomen achter de eekhoorntjes; met een koord liet ik mij langs
steile rotsen glijden om arendsnesten te ledigen. Ik kon er het leven
bij inschieten, maar mijn lichaam werd er des te sterker om. Op de
jacht vluchtten de wilde dieren, als ze mij zagen met mijn vuurroer.

--Ach! zuchtte de infant, 'k heb buikpijn, heer vader.

--Paxarete-wijn is een uitstekend geneesmiddel tegen de buikpijn,
sprak Karel.

--Dien wijn lust ik niet; 'k heb hoofdpijn, heer vader.

--Mijn zoon, zei Karel, gij moet loopen, springen, stoeien, zooals
de andere kinderen van uwen leeftijd doen.

--Mijne beenen zijn stijf, heer vader.

--Kan het anders? sprak Karel, gij maakt er houten beenen van. Wacht,
ik ga u vastbinden op een vurig peerd.

De infant weende.

--Bind mij niet vast, sprak hij, ik heb pijn aan de lenden, heer vader.

--Maar, vroeg Karel, hebt gij dan overal pijn?

--Het zou niets zijn, zoo men mij gerust liet, zegde de infant.

--Denkt gij soms, hernam de keizer ongeduldig, uw koninklijk leven als
de poëten in mijmering door te brengen? Laat hen hunne perkamenten
met inkt bemorsen, in stilte, eenzaamheid en bespiegeling; aan u
zoon van het zweerd, behoort het warme bloed, het oog van den arend,
de list van den vos, de kracht van een Hercules. Waarom maakt gij het
teeken des kruises? Een leeuwenwelp mag geen paternosterknauwer zijn!

--Het Angelus, heer vader, antwoordde de infant.




XIX.

Bloei- en Zomermaand waren dat jaar oprecht de maanden der
bloemen. Nooit zag men, in Vlaanderen, zulke welriekende hagedoornen,
in de hovingen zooveel rozen, jasmijn en kamperfoelie. Als de
Westenwind de geuren van dat bloemenland naar 't Oosten dreef, stak
iedereen, en meest nog in Antwerpen, verrukt den neus omhoog, zeggende:

--Riekt gij dien goeden wind, die uit Vlaanderen waait?

Onverpoosd waren de vlijtige bijen bezig met honig uit de bloemen te
zuigen, was te maken, het broedsel te verzorgen in de korven, te weinig
in aantal om al de zwermen te bergen. Heerlijke muziek van den arbeid
onder den blauwen hemel, die schitterend den rijken bodem overdekte!

Men maakte rieten, strooien, wisschen bijenkorven. Mandenmakers,
kuipers, stroovlechters arbeidden van den vroegen morgen. En de
kastenmakers konden bijlange het bestelde werk niet afdoen.

De zwermen bestonden uit dertig duizend werkbijen en twee duizend
zevenhonderd hommels. De honigraten waren zoo lekker en van zulke
zeldzame hoedanigheid, dat de deken van Damme er elf zond aan keizer
Karel, als dankzegging omdat hij, door zijne ordonnantiën de Heilige
Inquisitie weder ingevoerd had. Philippus at de honigraten op, maar
hij had er geen genot van.

Schooiers, bedelaars, rabauwen en heel die bende luiaards, die vadsig
langs de wegen slenteren en zich liever laten opknoopen dan zich aan
eenigerhande bezigheid over te leveren, kwamen van heinde en verre
aanloopen, verlekkerd door den honiggeur. En 's nachts zwierven zij
in groote menigte door velden en hoven.

Klaas had korven gemaakt om er bijenzwermen heen te lokken; eenige
waren gevuld, andere nog ledig. Klaas bleef heel den nacht waken om
op zijn goed te letten. Als hij moede was, zegde hij tot Uilenspiegel
zijne plaats in te nemen. Deze deed het gewillig.

Nu, op een nacht dat het koel was, kroop Uilenspiegel in een ledigen
korf, en gansch ineengedrongen, keek hij door de gaten die er van
boven in waren.

Op 't punt van insluimeren, hoorde hij de haag kraken en de stemmen
van twee manslieden, die hij voor dieven aanzag. Hij keek door een
der gaten van den bijenkorf en zag, dat de beide mannen lang haar
en een langen baard hadden, hoewel een lange baard te dien tijde een
teeken van adel was.

Zij gingen van korf tot korf, en zoo kwamen zij aan den zijnen en
hem optillende, spraken zij:

--Deze is de zwaarste; vervolgens staken zij er hunne stokken onder
en droegen hem mee.

Uilenspiegel vond het geenszins aangenaam, aldus in een bijenkorf
vervoerd te worden. De nacht was donker en de dieven spraken geen
woord. Alle vijftig stappen bleven zij staan om adem te scheppen en
zich vervolgens weder op weg te begeven. Die vóór ging gromde van
kwaadheid omdat de last zoo zwaar woog, en die van achteren, kermde
weemoedig. Want in de wereld zijn twee soorten luiaards: zij, die
kwaad zijn op den arbeid, en zij, die jammeren als er te werken valt.

Uilenspiegel, die niets te doen had, trok den dief, die vóór ging,
bij zijn haar, en den anderen bij zijnen baard, zoodat de grommer
den janker toeschreeuwde:

--Als gij niet ophoudt, aan mijn haar te trekken, geef ik u eene smete
op den kop, dat hij in uwe borstkas valt en gij door uwe ribben kunt
zien, als een dief door de traliën van het Steen.

--Ik deed het niet, vriend, jammerde de janker, gij zijt het die aan
mijnen baard trekt.

De grommer antwoordde:

--Ik zoek geen ongedierte in een schurftigen baard!

--Maat, sprak de janker, doe de korf niet zoo schommelen, mijne armen
houden het niet langer uit.

--Hewel, ik zal ze u van het lijf rukken. En hij trok zijnen riem over
zijn hoofd, zette den korf op den grond en sprong op zijn makker. En
zij vochten, de eene vloekend, de andere om genade smeekende.

Toen Uilenspiegel de slagen hoorde vallen, kroop hij uit den korf,
sleepte dien in een boschje, waar hij hem terugvinden kon, en keerde
toen huiswaarts.

En zoo is het, dat de slimmen voordeel halen uit twist en krakeel.




XX.

Als Uilenspiegel vijftien jaar oud was, bouwde hij te Damme, met
vier palen, eene kleine tent op, en riep dat een iegelijk er zijn
tegenwoordig en toekomstig gelaat kon afgebeeld zien, in een schoone
lijst van hooi.

Wanneer een opgeblazen rechtsgeleerde binnen kwam, zot van eigenwaan,
stak Uilenspiegel zijn hoofd door de lijst en bootste het gezicht
van een ouden aap na; dan sprak hij:

--Een ouden snuit kan rotten, maar geenszins bloeien; ben ik uw
spiegel niet, heer dokter in de rechten?

Als Uilenspiegel tot klant een oudgediende kreeg, liet hij, in stee
van zijn gezicht, in 't midden van de lijst een schotel vleesch en
brood zien, en sprak hij:

--De oorlog zal u tot gehakt maken; wat geeft gij mij om de
voorzegging, o snorrebaard, verzot op sakkers met wijden mond?

En als een oud heertje aan Uilenspiegel zijn poezelig wijfje liet
zien, verborg de snaak zijn gelaat nogmaals en toonde in de lijste een
boompje, aan welks takken messen, koffertjes, kammen en schrijfgerei
hingen, alles van hoorn vervaardigd, en zeide:

--Vanwaar komen die schoone snuisterijen, messire? is het niet van den
horenboom, die groeit in den boomgaard der oude manslieden? Wie zal
nog zeggen, dat horendragers van geenerlei nut zijn in de samenleving?

En Uilenspiegel toonde in de lijste, nevens den boom, zijn jeugdig
gezicht.

Als de ouderling hem hoorde, ontstak hij in hevige woede, doch zijn
vrouwtje paaide hem, en glimlachend vroeg zij aan Uilenspiegel:

--En mijn spiegel, wilt ge hem mij toonen?

--Kom nader, was 't antwoord.

Zij deed het. Toen kuste hij haar waar hij maar kon.

--Uw spiegel, sprak hij, is bloeiende jeugd in trotschheid gehuld.

En de schoone ging heen, en vergat niet hem een paar gulden te geven.

Aan een dikken monnik, die hem vroeg om zijn tegenwoordig en toekomstig
gezicht te zien, antwoordde Uilenspiegel:

--Gij zijt eene hespenkast, en een bierkelder zult gij ook zijn,
want zout noodt tot drinken, niet waar, dikzak? Geef mij een oortje,
omdat ik de waarheid zei.

--Mijn zoon, sprak de monnik, nooit dragen wij geld op ons.

--Dan is het, antwoordde Uilenspiegel, dat het geld u op zich draagt,
want mij is 't bekend, dat gij het steekt tusschen twee zolen, onder
den voet! Geef mij uw riemschoen.

Maar de monnik hernam:

--Mijn zoon, 't is het goed van het klooster. Maar als 't moet,
zal ik u toch twee oortjes geven voor uwe moeite.

De monnik gaf ze en Uilenspiegel nam ze minzaam aan.

Daarna toonde hij ook aan de lieden van Damme, van Brugge, van
Blankenberge, tot zelfs van Oostende, hunnen spiegel.

En in stee van te zeggen in Vlaamsche sprake: "Ik ben Ulieden spiegel",
zei hij kortweg: "Ik ben Ulen spiegel", gelijk thans nog gezegd wordt
in Oost- en West-Vlaanderen.

En zóó kwam hij aan zijn bijnaam Uilenspiegel.




XXI.

Grooter geworden slenterde hij geerne langs kermissen en
jaarmarkten. Zag hij ergens een hobo-, vedel- of doedelzakspeler,
dan liet hij zich voor een oortje leeren, hoe men uit die speeltuigen
welluidende tonen kan halen.

Zeer behendig werd hij in 't bespelen van den rommelpot, een speeltuig
gemaakt met een pot, eene blaas en een rietje, en wel als volgt:
over den pot spant men een natte blaas; een eind van het rietje
wordt met een touwtje gebonden in het middenste van de blaas en het
ander raakt den bodem van van den pot; vervolgens wordt de blaas tot
barstens toe om den pot gespannen. 's Anderen morgens, als de blaas
droog geworden is, kan men er op slaan als op een tamboerijn en zoo
men met het rietje wrijft, bromt het schooner dan de viool.

En Uilenspiegel ging met zijn rommelpot, die het geblaf van wachthonden
nabootste, aan de deur van de huizen kerstliederen zingen, in
gezelschap van kinderen, waarvan een, op Driekoningen, een blinkende
papieren ster droeg.

Als een meester-schilder te Damme kwam om de broeders van een of ander
gilde geknield op het doek te malen, bekroop Uilenspiegel de lust te
zien hoe hij werkte; daarom vroeg hij om zijne verf te mogen wrijven,
en als loon wilde hij slechts eene snee brood, drie duiten en eene
pint kuite aanveerden.

Terwijl hij de verf fijn wreef, ging hij de doenwijze zijns meesters
na. Als deze weg was, beproefde hij te schilderen, maar overal streek
hij scharlakenrood. Hij probeerde ook 't portret te maken van Klaas,
Soetkin, Katelijne en Nele, alsmede van pinten en stoopen. En Klaas
hem aan 't werk ziende, voorzeide, dat hij, zoo hij neerstig wou
zijn, florijnen bij tientallen zou kunnen verdienen met opschriften
te schilderen op de speelwagens in Vlaanderen en Zeeland.

Ook het houtsnijden en steenkappen leerde hij van een meester-metser,
terwijl deze, in het koor van Onze Lieve Vrouwekerk, eenen zetel
kwam maken, derwijze geschikt, dat de oude deken zitten kon zonder
dat iemand het merkte.

Uilenspiegel was het, die het eerst een messenhecht sneed, zooals de
Zeeuwen gebruiken. Hij maakte er een kunstig bewerkt snijwerk van,
met van binnen een doodshoofd en van boven een wakende hond. Hetgeen
zeggen wilde: het hecht getrouw tot aan den dood.

En alzoo begon zich de voorzegging van Katelijne te verwezenlijken,
want Uilenspiegel was nu tegelijk schilder, beeldhouwer, boer en ook
edelman: immers de Klaassen voeren, van vader tot zoon, drie zilveren
pinten in een veld van bruinbier.

Maar Uilenspiegel bleef in alles ongedurig en Klaas zei dat, als dat
spelletje zoo voortging, hij hem de stulp uit zou jagen.




XXII.

De keizer, van den oorlog teruggekeerd, vroeg waarom zijn zoon
Philippus hem niet was komen begroeten.

De aartsbisschop-leermeester van den infant antwoordde, dat hij niet
gewild had, dat hij slechts van boeken en eenzaamheid hield.

De keizer vroeg, wáár hij zich ophield.

De leermeester antwoordde, dat men hem overal zoeken moest, waar het
duister was. Zoo deden zij.

Als zij door menige zalen gegaan waren, kwamen zij eindelijk in een
somber verblijf, door een smal venster verlicht. En op den grond
stond een staak, waaraan een jong en lief aapje vastgemaakt lag,
een diertje dat Zijne Hoogheid uit Indië gekregen had om er mede te
spelen. Smeulende takkebossen lagen rondom en in het vertrek hing
een walm van verkoold haar.

Het diertje, levend verbrand, had zoo verschrikkelijk geleden, dat
zijn lichaampje niet geleek op dat van een wezen dat geleefd had,
maar op een stuk gewrongen en gerimpelden wortel. En op zijn mondje,
dat open was, als om genade te vragen, stond een bloedig schuim,
en zijn arm gezichtje was nat van zijne tranen.

--Wie heeft dat gedaan? vroeg de keizer.

De leermeester dorst niet antwoorden en beiden bleven sprakeloos,
droef en grammoedig staan.

Maar onverwacht werd de stilte door een lichten kuch gestoord, die uit
den donkersten hoek kwam. Zijne Majesteit keerde zich om en zag den
infant Philippus, in 't zwart gekleed bezig een citroen uit te zuigen.

--Don Philippus, sprak hij, kom hier om mij te groeten.

Zonder zich te verroeren, bekeek de infant hem met zijne vreesachtige
oogen, waar geenerlei liefde in blonk.

--Zijt gij het, vroeg de keizer, die dat diertje verbrand hebt?

De infant boog het hoofd.

--Waart gij wreedaardig genoeg om het te bedrijven, wees dan vrank
genoeg om het te bekennen.

De infant zweeg.

Zijne Majesteit ontnam hem den citroen, wierp dien op den grond
en wilde zijn zoon slaan, maar de aartsbisschop hield hem terug,
en fluisterde hem toe:

--Zijne Hoogheid zal later een groote ketterbrander zijn!

De keizer glimlachte en beiden gingen, den infant met zijn aapje
alleen latend.

Maar ook anderen, die geen aapjes waren, kwamen in vlammen om.




XXIII.

De Slachtmaand was gekomen, de kille hoestmaand der borstlijders.

't Is ook de maand, waarin de knapen bij benden over de rapenvelden
heenstormen, om te rooven wat zij kunnen, tot groote schade der boeren,
die ze tevergeefs achternazitten met stokken en vorken.

Op een avond nu dat Uilenspiegel van een strooptocht terugkwam,
hoorde hij in een hoek van den haag, dicht bij hem, een gekerm. Hij
bukte en zag, op eenen steen, een hondeken liggen.

--Wel, mijn beestje, sprak hij, wat doet ge hier zoo spa in den avond?

Hij wilde den hond streelen en hij voelde dat zijn rug nat was. Hij
dacht, dat men hem had willen verdrinken en nam hem in de armen,
om hem te drogen en te koesteren.

Thuis gekomen, sprak hij:

--Hier is een gekwetste, wat moet ik er mee doen?

--Hem verbinden, antwoordde Klaas.

Uilenspiegel zette den hond op de tafel. Klaas, Soetkin en hij zagen
toen, bij het licht van de lamp, dat het diertje eene wond op de
rug had. Soetkin wiesch ze, lei er balsem op en bond er een doek
om. Uilenspiegel nam den hond in zijn bed, hoewel Soetkin hem in
't hare wou hebben, bevreesd dat Uilenspiegel, die woelde als een
duivel in een wijwatervat, in zijn slaap het diertje zou bezeeren.

Maar Uilenspiegel deed zijne goesting; hij verzorgde zijn hond zóó
goed, dat de gekwetste na zes dagen liep zooals de meesten zijner
verwaande natuurgenooten, met den steert omhoog.

En de schoolmeester hiet hem Titus Bibulus Snuffius: Titus in memorie
van zekeren goeden Romeinschen keizer, dewelke dwalende honden placht
op te nemen; Bibulus, omdat de hond eene dronkemansliefde voor kuite
en bruinbier had, en Snuffius omdat hij steeds met den snoet in
rattenholen en mollenritten aan 't snuffelen was.




XXIV.

Aan het einde van de Onze-Lieve-Vrouwestraat stonden twee wilgeboomen
aan den boord van een diepe gracht.

Tusschen de twee wilgen spande Uilenspiegel eene koorde, waarop hij
op een Zondag na de vespers danste, zoo vlug, dat heel de menigte
van straatloopers in de handen kletste. Toen kwam hij beneden en ging
rond met zijn schaaltje, dat met geld gevuld werd, maar hij ledigde
het in de schorte van Soetkin, en hield elf duiten voor zich.

Den volgenden Zondag wilde hij weer op de koorde dansen, maar eenige
bengels, uit nijd over zijne behendigheid, hadden eene snee in de
koorde gegeven, zoodat zij na eenige sprongen brak en Uilenspiegel in
't water tuimelde.

Terwijl hij naar den oever zwom, riepen de kleine koordesnijders
hem toe:

--Hoe gaat het, Uilenspiegel-vlug? Gaat gij nu in den vijver den
karpers leeren dansen?

Uilenspiegel kwam uit het water en schudde zich af. En daar zij uit
angst voor een pak slaag wegliepen, riep hij hun toe:

--Vreest niets; komt Zondag terug, 'k zal U andere kunsten toonen en
gij zult uw deel in de winst hebben!

's Zondags nadien sneden de bengels de koorde niet door, doch hielden
er de wacht bij, opdat niemand ze aanraakte, want er waren toeschouwers
in groote menigte.

Uilenspiegel zei hun:

--Dat ieder mij een zijner schoenen geve, en 'k wed dat ik er mee dans,
zoowel met den grootsten als met den kleinsten.

--En wat betaalt gij, als gij verliest? vroegen zij hem.

--Veertien pinten bruinbier, antwoordde Uilenspiegel, maar gij betaalt
mij drie oortjes als ik win.

--Goed! riepen zij.

En zij gaven hem elk een hunner schoenen. Uilenspiegel legde ze alle
in het voorschoot dat hij aan had en, met dien last, danste hij op
de koorde, doch niet zonder moeite.

Van beneden riepen de koordesnijders:

--Gij hebt gezegd met elk onzer schoenen te zullen dansen; trek ze
aan en houd uwe wedding.

Uilenspiegel danste voort en antwoordde:

--Ik heb niet gezegd uwe schoenen aan te trekken, doch er mee te
dansen. Nu, ik dans, en alles danst mee in mijn voorschoot. Ziet gij
het niet met uwe paddenoogen? Betaalt mij mijn drie oortjes.

Doch zij jouwden hem uit en schreeuwden, dat zij hunne schoenen
moesten terughebben.

Uilenspiegel smeet ze alle te gelijk in een worp naar beneden. Een
woedend gevecht volgde, daar niemand zijn schoen dadelijk terugvinden
kon.

Uilenspiegel kwam naar beneden en begoot de vechters, maar niet met
klaar water.




XXV.

De infant, nu vijftien jaar oud, dwaalde als naar gewoonte door gangen
en trappen en zalen van 't slot. Doch meestal slenterde hij rond de
vertrekken der edelvrouwen, om de edelknapen te verschalken, die,
gelijk hij, als katten in de gangen op loer lagen. Andere jonkers
waren in den tuin, keken verzuchtend omhoog, en zongen eene ballade
van minne.

Als de infant het hoorde, vertoonde hij zich eensklaps aan een der
vensteren, en de arme edelknapen waren ontsteld als zij zijn bleeke
tronie zagen, in stee van de zoete oogen hunner schoonen.

Onder de edelvrouwen van het hof was een lieftallige dame, een
Vlaamsche van Dudzele, omtrent Damme, van ongemeene schoonheid en in
de volheid harer jaren, met oogen, groenig-bruin, en rossig, krullend
haar, dat schitterde als goud. Vroolijk van zin en vurig van aard,
verheelde zij niemand hare neiging tot den gelukkige, wien zij, op
heur aanbiddelijk erf, het hemelsch privilege van liefde schonk. De
uitverkorene heurs herten was een schoon en fier ridder. Elken dag
op vast uur, ging zij tot hem, hetgeen Philippus wist.

Hij zette zich op eene bank tegenover een venster en wachtte. En
als zij hem voorbijging met flikkerend oog en met rozeroode lippen,
en glanzend van jeugd en van liefde in haar kleed van goudbrocaat,
zag zij den infant, die, zonder zich van zijne plaats te verheffen,
tot haar zegde:

--Mevrouwe, hebt gij een oogenblik voor mij?

Driftig als de merrie, die in haren loop gestuit wordt op 't oogenblik
dat zij rent naar den schoonen hengst, die in den beemd hinnikt,
antwoordde zij:

--Hoogheid, een ieder moet gehoorzamen aan Uwen vorstelijken wil.

--Zet U naast mij, sprak de infant.

Onbeschaamd, listiglijk en onbermhertig zag hij haar aan:

--Zeg mij het Onze-vader in Vlaamsche tale; men heeft het mij geleerd,
laas! ik heb het vergeten.

De arme vrouw zegde een Vader-ons, doch tamelijk vlug, maar hij dwong
haar telkens tot langzamer spreken.

En aldus noodzaakte hij heur het tot tienmaal toe te zeggen, aan haar,
die op dit uur aan andere gebeden dacht.

Daarna sprak hij vleiend van heure schoone gouden lokken, van heure
heldere tint, heur klare oogen, maar niets dorst hij zeggen van
heur gevleesde schouderen, noch van haren fraai gevormden boezem,
noch van iets anders.

Zij meende te mogen heengaan en blikte reeds naar den tuin waar zij
haren minnaar wachtte, toen hij vroeg of ze wist welke de deugden
der vrouw zijn?

Daar zij niet antwoordde uit vreeze van verkeerd te spreken, deed
hij het in heure plaats, en zegde hij op den toon van een zedenpreeker:

--Deugden der vrouwe zijn kuischheid, eerzaamheid en ingetogenheid.

Hij ried haar aan zich zedig te kleeden en alles wat heur was,
zorgvuldiglijk te verbergen.

Zij knikte ten teeken van goedkeuring en zeide, dat zij zich voor
Zijne Noordpoolachtige Hoogheid liever met tien berenhuiden dan met
eene el neteldoek bedekken zou.

En terwijl hij onthutst was over dit antwoord, nam zij lachende
de vlucht.

Nochmaals was het vuur der jeugd in de borst van den infant ontbrand:
maar het was dit gloeiende vuur niet, dat de sterke zielen tot groote
daden drijft, noch het zoete vuur, dat de teedere herten doet weenen:
't was een somber vuur uit de helle, door Satan ontstoken. En het
glom in zijne grijze oogen, gelijk de maan boven een kerkhof, in
winternacht. En het brandde hem wreedelijk.

Daar de arme gluiperd geene liefde voor anderen voelde, dorst hij de
edelvrouwen niet aanspreken; toen ging hij naar een afgelegen hoekje,
in een kamertje, met witte muren, slecht verlicht, waar hij gemeenlijk
zijne lekkernijen at en waar een groote menigte vliegen waren, om den
wille van de brokkelingen. Daar streelde hij zichzelven, terwijl hij de
vliegen met den kop tegen de ruiten plette en er met honderden doodde,
totdat zijne vingeren te danig beefden om hunne bloedige bezigheid
voort te zetten. En in die wreede uitspanning vond hij een genot,
mits geilheid en wreedheid twee eerlooze zusteren zijn. Als hij
uit dat hok kwam, was hij nog somberder dan te voren en een ieder
ontvluchtte het bleeke gelaat van dien terugstootenden prins.

En de treurige Hoogheid leed, want slecht herte is smerte.




XXVI.

De schoone vrouwe verliet Valladolid om naar heur slot van Dudzele,
in Vlaanderen, te gaan.

Toen zij, met heuren dikken bottelier, door Damme trok, zag zij een
veertienjarigen knaap, met den rug tegen eene hut geleund, op eenen
doedelzak spelen. Rechtover hem zat een rosse hond, die jammerlijk
huilde, daar die muziek hem niet aanstond. De zonne stond schitterend
aan den hemel. Nevens den knaap zat een aanminnig meisje, dat, bij
elk erbarmelijk gehuil van den hond, in een gulhertigen lach schoot.

Toen de schoone vrouwe en de dikke bottelier voorbij de stulp kwamen,
bezagen zij Uilenspiegel, die blies, Nele, die lachte en Titus Bibulus
Snuffius, die jankte.

--Stoute jongen, sprak zij tot Uilenspiegel, wilt ge wel ophouden
dien armen hond zoo te doen huilen!

Maar Uilenspiegel bezag haar en blies nog harder op zijnen
doedelzak. En Bibulus Snuffius jankte nog jammerlijker, en Nele
schaterlachte nog luider.

De bottelier ontstak in woede, wees naar Uilenspiegel en sprak tot
de edelvrouwe:

--Als ik dat schavuitengebroed eens afroste met de schee van mijn
degen, zou de onbeschaamderik wel ophouden!

Uilenspiegel bezag den bottelier, hiet hem Jan Papzak, om den wille
van zijn dikken buik en ging voort met blazen op zijn doedelzak. De
bottelier liep op hem toe en dreigde hem met de vuist; maar Bibulus
Snuffius vloog op hem af en beet Papzak in het been; van schrik viel
de bottelier op den grond en schreeuwde om hulp.

De dame lachte Uilenspiegel toe en sprak:

--Kunt ge mij zeggen, doedelzakspeler, of de weg, die van Damme naar
Dudzele leidt, niet veranderd is?

Uilenspiegel bleef voortblazen, schudde den kop en bezag de edelvrouwe.

--Maar waarom ziet ge mij zoo strak aan? vroeg zij.

Doch hij speelde voort en sperde de oogen open, alsof hij voor heur
in bewondering stond.

--Zijt gij niet beschaamd, voor een jongen snaak als gij, de vrouwen
aldus te bezien?

Uilenspiegel bloosde een weinig, speelde voort en bekeek heur nog meer.

--Ik heb U gevraagd, hernam zij, of de weg niet veranderd is, die
van Damme naar Dudzele leidt?

--Weleer was hij groen, thans is hij droef en schraal, sedert hij
het geluk mist U te mogen dragen, antwoordde Uilenspiegel.

--Wilt ge mij leiden?

Maar Uilenspiegel bleef zitten, haar steeds aanziende. En als ze hem
zoo snaaksch zag en zoo jong en zoo levendig, vergaf zij hem geerne
zijne woorden. Hij stond op om binnen te gaan.

--Waar gaat gij?

--Mijn beste kleeren aantrekken, antwoordde hij.

--Spoed U dan, sprak de edelvrouwe.

Toen zette zij zich neer op de bank naast de deur; de bottelier deed
zooals zij. Zij wilde tot Nele spreken, maar Nele antwoordde heur niet,
want zij was jaloersch.

Uilenspiegel kwam terug; hij was schoon gewasschen en had een
bombazijnen wambuis aan. Hij zag er flink uit in zijn zondagspak.

--Gaat gij toch mee? vroeg Nele hem.

--Ik ben dadelijk terug.

--Wil ik in uwe plaats gaan? sprak Nele.

--Neen, zegde hij, de wegen zijn vol modder.

--Waarom, vroeg de dame gestoord en insgelijks jaloersch, waarom,
kleine meid, wilt gij hem beletten van mede te gaan?

Nele antwoordde heur niet, maar twee dikke tranen welden in heure
oogen, en treuriglijk en gramstorig bekeek zij de schoone edel vrouwe.

Gevieren begaven zij zich op weg, de dame op hare witte hakkenei
met zwart fluweel getuigd; de bottelier met zijn waggelenden buik;
Uilenspiegel, die de hakkenij bij den breidel hield, en Bibulus
Snuffius, die, met den steert in de lucht, fier naast zijn meester
stapte.

Geruimen tijd reden en gingen zij aldus voort, maar Uilenspiegel was
niet op zijn gemak; stom als een visch snoof hij den fijnen benjoëreuk
op, die opsteeg uit de kleeren van de dame, en hij bekeek, ter sluip,
heur schoon paardentuig, heure zeldzame kleinooden en juweelen, en ook
heur zachtaardig uitzicht, heure schitterende oogen, heuren schoonen
boezem en heur haar, dat als een gouden helmet in de zonne schitterde.

--Waarom zegt gij zoo weinig, vriendje? vroeg zij.

Hij antwoordde niet.

--'t Ware jammer als gij uwe tong verloren hadt, want 'k had U geerne
met een boodschap belast.

--Welke? vroeg Uilenspiegel.

--Gij moet, sprak de dame, mij hier verlaten en naar Koolkerke gaan,
aan den anderen kant van den wind, en aldaar zeggen aan een edelman,
half in 't zwart, half in 't rood gekleed, dat hij mij vandaag niet
mag verwachten, maar Zondag komen moet, te tien uren van den nacht,
in mijn slot, langs de sluippoort.

--Ik ga niet! sprak Uilenspiegel.

--Waarom niet? vroeg de dame.

--Neen, ik ga niet! volhardde Uilenspiegel.

De dame sprak toen:

--Maar waarom toch maakt gij u driftig als een haantje, en wilt
niet gaan?

--Ik ga niet! sprak Uilenspiegel.

--Maar als ik U een gulden gaf?

--Neen! sprak hij.

--Een dukaat?

--Neen.

--Een karolus?

--Neen, sprak Uilenspiegel nog. En toch, voegde hij er bij met
een zucht, zou ik dien liever dan eene mosselschelp in moeder heur
tassche zien.

De dame glimlachte, en eensklaps riep zij uit:

--Ik ben mijne beugeltassche kwijt, een schoone zeldzame tassche van
zijdelaken, met fijne perelen geborduurd. Te Damme had ik ze nog aan
mijne ceintuur bevestigd.

Uilenspiegel verroerde zich niet, maar de bottelier ging naar de
edelvrouwe:

--Mevrouwe, sprak hij, als die jonge schavuit ze gaat zoeken, ziet
gij hem nimmer terug.

--En wie zal dan gaan? vroeg de edelvrouwe.

--Ik, sprak hij, hoewel ik oud van jaren ben.

En terstond ging hij op zoek.

Middag sloeg de klok; 't was drukkend warm en stille en eenzaam in
het ronde. Uilenspiegel sprak geen woord, doch deed zijn nieuw wambuis
uit, om de dame onder de schaduwe van een lindeboom te laten rusten,
zonder dat de koelte van het gras heur kwellen kon. En hij bleef
rechtstaan in verzuchting, naast heur.

Zij zag hem aan en voelde medelijden voor dien blooden jongen,
en vroeg hem of hij niet moede was, zoo lang op zijne jonge beenen
te staan. Hij antwoordde niet, doch liet zich naast heur vallen, en
zij trok hem tot zich, en zijn hoofd rustte op heuren blooten boezem,
en daar lag hij zoo goed, dat zij het als eene zonde beschouwd hadde,
hem te zeggen, dat hij elders een hoofdkussen zoeken moest,

De bottelier kwam intusschen terug, zeggende dat hij de beugeltassche
niet gevonden had.

--Ik heb ze wedergevonden, ik, antwoordde de dame, toen ik van mijn
peerd steeg; in 't vallen was zij aan den stijgbeugel vastgeraakt. En
nu, sprak zij tot Uilenspiegel, leid ons nu recht naar Dudzele en
zeg mij uw naam.

--Mijn patroon, antwoordde hij, is de heer Sint Thijlbert, naam,
die bediedt vlug te been, om te gaan waar het goed is; mijn naam is
Klaas en mijn toenaam Uilenspiegel. Als ge U zelf in mijn spiegel wilt
aanschouwen, zult gij overtuigd zijn, dat er, gansch Vlaanderenland
door, geen schitterender bloem van schoonheid bestaat dan Uwe geurige
bekoorlijkheid.

De dame bloosde van welbehagen en was geenszins verbolgen.

En gedurende die lange afwezigheid weenden Soetkin en Nele bitterlijk.




XXVII.

Toen Uilenspiegel van Dudzele terugkwam, zag hij Nele, aan den
inkoom van de stad, met den rug tegen een hek geleund, en een tros
blauwe druiven in de hand. Een voor een at zij de vruchten, die
haar verfrischten, maar blijken liet zij dit niet. Integendeel, zij
scheen verstoord, want driftig beet zij de druiven van de rist. Zij
was weemoedig, en had zulk een droevig en spijtig gezicht, dat
Uilenspiegel, vol liefde, medelijden kreeg en, stille achter haar,
heur eenen kus in den hals gaf.

Maar zij gaf hem een klinkenden kaakslag in de plaats.

--Die was raak, zei Uilenspiegel.

Zij weende dat de tranen over heure wangen rolden.

--Nele, sprak hij, gaat gij nu de fonteinen aan den ingang van de
stad stellen?

--Loop heen! zegde zij.

--Maar ik kan niet heengaan, als gij zoo weent, liefste?

--Ik ben geene liefste, sprak Nele, en weenen doe ik niet.

--Neen, gij weent niet, maar er komt water uit uwe oogen.

--Wilt gij gaan, sprak zij.

--Neen! zegde hij.

Maar met heure bevende handjes, hield zij heur schort voor 't gezicht;
zij beet er de stof van aan stukken, en heure tranen maakten het nat.

--Nele, vroeg Uilenspiegel, zeg eens, zal het straks schoon weder zijn?

En glimlachend zag hij heur liefderijk aan.

--Waarom vraagt ge mij dat? sprak zij.

--Omdat het spreekwoord zegt: na regen komt zonneschijn, antwoordde
Uilenspiegel.

--Ga, sprak zij, ga bij uw schoone dame met haar zijden kleed, die
hebt gij genoeg doen lachen.

Toen zong Uilenspiegel:


    Hoor ik mijn lieveken krijschen
    't Doet mijn herteken groot verdriet.
    Honig zoo gij haar lachen hoort,
    Peerlen zoo gij heur traantjes ziet....
    Ei, mijn lieveken laat ik niet!...
    En ik geef een bottel ten beste
    Lekkeren Leuvenschen wijn.
    En ik geef een bottel ten beste
    Als Nele vroo wil zijn....


--Gemeene man, sprak zij, gij lacht mij dan noch uit!

--Nele, sprak Uilenspiegel, ik ben een man, dat is waar, maar gemeen
ben ik niet, want onze adellijke familie, eene schepenfamilie, voert
drie zilveren pinten in een veld van bruinbier. Nele, is 't waar,
dat men, in Vlaanderenland, kaaksmeten maait als men kussen zaait?

--Ik spreek u niet aan, zegde zij.

--Waarom doet ge dan uw mondje open om het mij te zeggen?

--Ik ben kwaad, sprak zij.

Uilenspiegel klopte heel zachtjes op heuren rug en sprak:

--Kus een vrouwtje en ze zal u kloppen; klop een vrouwtje en ze zal
u kussen. Kus mij dan, liefste, vermits ik u klopte.

Nele keerde zich om. Hij opende zijne armen en, nog weenend, wierp
zij er zich in en vroeg:

--Zult ge ginder niet meer gaan, Thijl?

Maar hij antwoordde niet, want hij had het te druk met heure bevende
vingeren in de zijne te drukken en, met de lippen, de heete tranen
te wisschen, die uit Nele's oogen vielen als de dikke droppelen van
een stormregen.




XXVIII.

In dien tijd weigerde Gent, de edele stad, haar aandeel te betalen
in de bede, die haar zoon, keizer Karel, heur vroeg. Zij kon niet
betalen, want zij had geen geld meer, en dit was de schuld van Karel
zelf. Toch was dat een groote misdaad, en hij besloot haar in persoon
te gaan kastijden.

Want de slagen, die eene moeder het zeerst doen, zijn die van heuren
zoon.

Frans met den Langen Neus, zijn vijand, deed hem het aanbod om
door Frankrijk te gaan. Karel nam het aan, en in stee van te
worden gevangengezet, werd hij op vorstelijke wijze onthaald en
gevierd. Altijd zijn de vorsten bereid elkander te helpen, om 't volk
te onderdrukken.

Karel verbleef langen tijd te Valencijn, zonder eenig teeken van
toorn te geven. De stad Gent, zijne moeder, leefde zonder vrees in
het geloof, dat de Keizer, haar zoon, vergeten zou, wijl zij gehandeld
had volgens recht.

Karel kwam onder de muren van de stad met vier duizend peerden. Alva
was bij hem, alsmede de prins van Oranje. Het gemeen en de kleine
ambachten hadden geerne die kinderlijke intrede belet en de tachtig
duizend man van de stad en den bijvang op de been gebracht; maar de
hoogpoorters verzetten zich daartegen, want zij vreesden, dat het volk
de overhand zou krijgen. Nochtans had de stad Gent haren zoon met zijne
vier duizend peerden in de pan kunnen hakken. Maar zij beminde hem nog,
en de kleine ambachten zelven hadden weder vertrouwen gekregen.

Karel ook had haar lief, maar 't was om het geld, dat hij van haar
in zijne kisten had en nog van haar trekken wilde.

Toen hij zich meester gemaakt had van de stad, stelde hij overal
krijgswachten en liet hij dag- en nachtronden doen. Daarna sprak hij,
in groote statie, de sententie over de stede uit.

De voornaamste poorters moesten vóór zijnen troon vergiffenis komen
vragen, met een strop om den hals; Gent werd schuldig verklaard aan
de ergste misdaden, dewelke zijn: ontrouw, inbreuk op de tractaten,
ongehoorzaamheid, muiterij, opstand en majesteitsschennis. De keizer
verklaarde alle geschonken privileges, rechten, vrijheden, costumen
en gebruiken verbeurd en, de toekomst verbindende alsof hij God zelf
was, bepaalde hij verder, dat zijne opvolgers, bij hunne komst als
landheer, zweren moesten niets te zullen naleven dan de vernederende
Karolijnsche Concessie, door hem aan de stad verleend.

De abdij van Sint-Baafs deed hij afbreken, om ter plaatse eene vesting
te bouwen, van waar hij, gemakkelijk, de borst zijner moeder met
kogels kon doorboren.

Als een slechte zoon, die met ongeduld naar den dood zijner ouderen
wacht, verbeurde hij alle goederen en eigendommen van Gent, inkomsten
en panden, geschut en oorlogstuig.

En hij vond, dat de stad te goed verdedigd was: daarom deed hij den
Rooden Toren, den Paddenhoektoren, de Braampoort, de Steenpoort,
de Walpoort, de Ketelpoort en vele andere poorten afbreken, dewelke
als meesterstukken van bouwkunst en beeldhouwkunst doorgingen.

En als later vreemdelingen naar Gent kwamen, spraken zij verbaasd
tot elkaar:

--Is dàt Gent, die platte en treurige stad? Men vertelde er ons
wonderen van: men heeft ons bedrogen.

En die van Gent antwoordden:

--Keizer Karel heeft de stad heure krone ontnomen.

En dit zeggende, waren zij grammoedig en beschaamd. En uit het puin
van de poorten haalde de keizer steenen voor zijne vesting.

Hij wilde, dat Gent arm werd, daar aldus de stad noch door arbeid,
noch door handel of geld, zich tegen zijne stoutmoedige inzichten
verzetten kon; daarom veroordeelde hij haar tot het betalen van het
geweigerde aandeel in de schatting van vierhonderd duizend gouden
karolusgulden en, daarboven, honderd vijftig duizend karolussen in
eens, en elk jaar nog zes duizend als eeuwigdurende rente. Hij had
geld van de stad in leening gekregen en moest haar voor hetzelve
eene rente betalen van honderd vijftig pond grooten. Met geweld deed
hij zich de schuldbrieven overhandigen en verscheurde ze. En op die
manier betaalde hij zijne schuld.

In menige aangelegenheid had Gent hem lief gehad en geholpen. Maar
hij stak haar eenen dolk in de borst, om bloed te hebben, daar hij
geene melk meer vond.

Toen bezag hij Roeland, de schoone klokke, en aan haren klepel liet hij
den poorter opknoopen, die storm geluid had, om de stad ten strijde
te roepen, ten einde heur recht te verdedigen. Geene genade had hij
voor Roeland, de fiere klokke, de tong zijner moeder, waarmee zij
tot Vlaanderen sprak:


    Als men my slaat dan is 't brandt,
    Als men my luydt dan is 't storm in Vlaenderland.


Mits zijne moeder te luide sprak, nam hij de klokke weg. En die van
't platteland zeiden, dat Gent dood was, dat heur zoon, met eene tang,
heure tong uit heuren mond had gerukt.




XXIX.

Op een van die dagen,--heldere en frissche lentedagen, als heel de
aarde liefde ademt,--zat Soetkin bij het open venster te naaien,
neurde Klaas een deuntje, terwijl Uilenspiegel bezig was met Titus
Bibulus Snuffius eene rechterskap op te zetten. De hond ging met
zijne pooten te werk, alsof hij eene sententie moest uitspreken, maar
't was alleen om den hoed af te krijgen.

Doch eensklaps sprong Uilenspiegel naar het venster en deed het
dicht. Klaas en Soetkin keken op en zagen hun zoon rond de kamer
loopen, op tafels en stoelen springen om een vogeltje te vangen,
dat, met trillende vleugelen en piepend van angst, in den hoek van
een balk aan de zoldering eene schuilpaats ging zoeken.

Uilenspiegel wilde het diertje grijpen, toen Klaas hem met ruwe
stemme vroeg:

--Waarom springt gij aldus?

--Om het te vangen, antwoordde Uilenspiegel, het in eene kevie te
zetten, zaad te geven en voor mij te doen zingen.

Maar de vogel piepte van angst, vloog weer rond de kamer en bezeerde
zijn kopje tegen de ruiten.

Daar Uilenspiegel niet ophield met grijpen en springen, pakte Klaas
hem ruw bij den schouder.

--Vang het beestje, sprak hij, doe het voor u zingen, maar ik zal
u ook in eene kooi steken, met kloeke ijzeren staven gesloten en ik
zal ook u doen zingen. Gij, die zoo geerne loopt, wordt opgesloten;
in de schaduw gestoken als gij koude hebt, in den zonneschijn als
gij het warm hebt. En op een Zondag zullen wij uitgaan en vergeten u
eten te geven, en als wij 's Donderdags terugkomen, zullen wij Thijl,
gestorven van honger, met de beenen uitgestrekt vinden.

Soetkin weende, Uilenspiegel vloog naar het venster.

--Wat doet gij? vroeg Klaas.

--Het venster open doen om den vogel buiten te laten, antwoordde hij.

Inderdaad, de vogel, een distelvink, vloog het venster uit, tjilpte
blijde in de vrije lucht, en steeg als een pijl naar omhoog. Dan
ging hij op een perelaar zitten, waar hij zijne vleugelen streek en
zijne pluimen schudde en grammoedig, in zijne vogeltaal, Uilenspiegel
allerlei verwenschingen naar het hoofd stuurde.

Toen sprak Klaas:

--Mijn zoon, nooit moogt ge aan mensch of dier de vrijheid ontnemen,
want die is het hoogste goed. Laat een iegelijk de zonne zoeken als hij
koude heeft, en de schaduw als hij het warm heeft. En God oordeele
Zijne Heilige Majesteit, die het vrije geloof in Vlaanderenland
aan ketenen legt en Gent, de edele stad, in een ijzeren kooi van
slavernije sluit!




XXX.

Philippus was getrouwd met Maria van Portugal, wier bezittingen hij
bij de Spaansche krone gevoegd had; van haar had hij don Carlos,
den wreedaardigen zot. Maar liefde gevoelde hij voor zijne vrouw niet.

De koningin leed aan de gevolgen van heure kraam. Zij bleef te bed en
bij haar waren heure eeredames, onder dewelke de hertoginne van Alva.

Philippus liet heur dikwijls alleen, om ketters om hals te zien
brengen. De edelvrouwen en kamerheeren deden als hij. En zoo ook de
hertoginne van Alva, de adellijke baker van Maria.

In dien tijd vatte de officiaal een Vlaamschen beeldhouwer,
Roomsch-katholiek van geloove, omdat een monnik hem den overeengekomen
prijs voor een houten Lieve-Vrouwenbeeld geweigerd had, en nu het
aangezicht van het beeld met zijnen beitel had geschonden, zeggende:
dat hij liever zijn werk vernielde, dan het te laten onder den prijs.

Door den monnik als beeldschenner aangeklaagd, werd hij zonder genade
op de pijnbank gelegd, en veroordeeld om levend te worden verbrand.

Op de pijnbank had men hem de voetzolen geroosterd en onderwege,
van het gevang naar den brandstapel, met den san benito op het hoofd,
riep hij gedurig:

--Snijdt mijne voeten af! Snijdt mijne voeten af!

En van verre hoorde Philippus die bange kreten, en hij trilde van
genot, maar hij lachte niet.

De eeredames verlieten koningin Maria om de voltrekking van het vonnis
bij te wonen: na haar volgde de hertoginne van Alva, die bij het hooren
van de kreten van den Vlaamschen kunstenaar, ook het schouwspel wilde
zien en de Koningin alleen liet.

Toen Philippus, zijne hooge dienaren, prinsen, graven, schildknapen en
hofdames dáár waren, werd de beeldhouwer met een lange keten aan een
paal geklonken, te midden van een vuur, gemaakt van rijshout en stroo,
dat hem langzaam moest braden, terwijl hij zich zoo verre mogelijk
van het laaie vuur wilde houden.

Hij was zoo goed als naakt, en nieuwsgieriglijk keek men hoe hij
beproefde zijne zielskracht te stellen tegen de hitte des vuurs.

En middelerwijl had Maria dorst. Zij zag een halven meloen op eene
schaal liggen, sleepte zich uit heur bedde, greep de vrucht en verslond
die gulzig.

De verkoelende vrucht deed de kraamvrouw huiveren. Zij bleef op de
vloer liggen, ze kon zich niet bewegen.

--Ik zou mij verwarmen, was hier iemand om mij te bedde te leggen?

Toen hoorde zij den armen beeldhouwer schreeuwen:

--Snijdt mijne voeten af!

--Ach! riep de arme vorstinne, is dat een hond, die huilt om mijnen
dood te voorspellen?

Op dat oogenblik zag de beeldhouwer rondom zich; doch hij bespeurde
niets dan vijandige Spaansche gezichten, en hij dacht aan Vlaanderen,
het land van de dapperen; en, zijne lange keten achter zich sleepend,
stapte hij naar den vuurgloed van stroo en van rijshout. Zich in zijn
gansche lengte verheffend en de armen kruisend sprak hij:

--Ziet hoe de Vlamingen sterven onder het oog van de Spaansche
beulen! Snijdt niet mijne, maar hunne voeten af, opdat ze naar
geen nieuwe euveldaden loopen! Leve Vlaanderen! Vlaanderen in der
eeuwigheid!

En de edelvrouwen juichten hem toe, vroegen genade voor hem, als ze
zijne fiere houding zagen.

En de kunstenaar stierf.

Koningin Maria rilde over gansch heur lichaam, heure tanden klapperden
van koude en, armen en beenen uitrekkend, kreunde zij:

--Legt mij te bedde, dat ik mij verwarme. En zij stierf.

En alzoo, volgens de voorzegging van Katelijne, de goede tooveres,
zaaide Philippus overal dood, bloed en tranen.




XXXI.

Maar Uilenspiegel en Nele hadden elkander innig lief.

Het was op 't einde van de Grasmaand; al de boomen stonden in bloei,
de planten waren in lichtgroen gedost, de nachtegalen kwinkeleerden
in het loover: de heele natuur had zich gereedgemaakt om de Meimaand
waardig te ontvangen.

Dikwerf dwaalden Uilenspiegel en Nele getweeën langs de wegen. Nele
ging aan Uilenspiegel's arm en hield hem met hare twee handjes
vast. Uilenspiegel had dit geerne en sloeg soms zijn arm om Nele's
middel, om heur beter vast te houden, zegde hij. En dit deed heur
genoegen, doch zij uitte geen woord.

De wind voerde den balsemgeur der beemden over de wegen; in de verte
loeide traagzaam de zee. Uilenspiegel stapte fier vooruit; als een
jonge duivel, en Nele volgde schuchter als eene heilige uit den hemel,
beschaamd over 't genot dat zij smaakte.

Zij leunde heur hoofdje op den schouder van Uilenspiegel: hij nam
heure handjes in de zijne en kuste heur, al gaande, op het voorhoofd,
op de koonen en op heuren liefelijken mond. Doch zij uitte geen woord.

Het werd warm en zij kregen dorst; zij gingen melk drinken bij eenen
boer, maar zij waren niet verkoeld.

En zij zetten zich neer in het gras, aan den boord eener gracht. Nele's
gelaat was bleek en zij scheen bekommerd; angstig keek Uilenspiegel
heur aan.

--Zijt ge droef? sprak zij.

--Ja, antwoordde hij.

--Waarom? vroeg zij.

--Ik weet het niet, sprak hij, maar die bloesem van appelaars en
kriekelaars, die zoele lucht als bezwangerd met het vuur van den
bliksem, die blozende madeliefjes in de beemden, die witte hagedoorn,
hier dicht bij ons....

... Wie zal mij zeggen waarom ik heel ontroerd ben, waarom ik mij
steeds bereid voel tot sterven of slapen? En mijn hert klopt hevig als
ik de vogelen hoor zingen, als ik zie dat de zwaluwen terugkeeren; ik
zou willen vliegen, verder dan zon en mane. En nu eens heb ik koud,
dan weer heb ik warm. Ha, Nele! Ik zou niet meer van deze wereld
willen zijn, of duizend levens geven voor haar, die mij heure minne
schenken zou....

Maar zij uitte geen woord en, glimlachend van geluk, keek zij naar
Uilenspiegel.




XXXII.

Op Allerzielen kwam Uilenspiegel uit Onze Lieve Vrouwekerk met eenige
deugnieten van zijn leeftijd. Lamme Goedzak was onder hen verdwaald,
als een lam te midden van de wolven.

Lamme, die op alle Zon- en feestdagen van zijne moeder drie oortjes
kreeg, trakteerde de jonge snaken.

Hij trok dus met hen in het Roode Schild, bij Jan van Liebeke, die
Kortrijkschen dobbelen knollaard opbracht.

De drank verhitte hunne hersenen en, wijl zij over kerken en gebeden
spraken, uitte Uilenspiegel de meening, dat zielmissen enkel voordeel
brengen aan de priesters.

Maar er was een judas onder 't gezelschap: hij ging Uilenspiegel als
ketter verklikken. En ondanks de tranen van Soetkin en het smeeken van
Klaas, werd Uilenspiegel gepakt en gevangengezet. Eene maand en drie
dagen bleef hij in den kerker opgesloten, zonder iemand te zien. De
cipier at de drie kwart van zijn eten op. Intusschentijd deed men
onderzoek over het gedrag van den beklaagde. Er werd alleen bevonden,
dat hij een meedoogenlooze spotter was, die met iedereen gekscheerde,
maar dat hij nooit het minste kwaad gesproken had noch van den Heere
God, noch van de Maagd Maria, noch van de santen. Weshalve de sententie
dan ook zacht was; want men hadde hem kunnen brandmerken of geeselen
met schorpioenen.

Om den wille van zijn jeugdigen leeftijd, veroordeelden de rechters
hem enkel om, in zijn hemde, barrevoets en blootshoofds en met eene
waskeers in de hand, achter de priesters te stappen, in 't midden
van de eerste processie, die zou uitgaan.

Het was Ons-Heeren-Hemelvaart.

Als de processie binnentrok, moest hij in 't portaal van
Onze-Lieve-Vrouwekerk blijven staan en uitroepen:

--Dank zij Jezus-Christus! Dank zij de eerweerde geestelijken! Hunne
gebeden zijn zoet en verkwikkend voor de zielen in 't vagevuur; want
elk ave is een emmer water, die haar op den rug valt, en elk pater
eene kuip.

En het volk aanhoorde hem devotelijk, doch niet zonder lachen.

Op den Eersten-Sinksendag, moest hij nogmaals de processie volgen; hij
was barrevoets en blootshoofds, in zijn hemde, met eene waskeers in de
hand. Bij het binnengaan in 't portaal, met zijne keers eerbiediglijk
in de hand, hoewel hij moeite deed om niet in lachen uit te bersten,
sprak hij met een luide en heldere stem:

--Zoo de gebeden der christenen veel verlichting brengen
aan de zielen van 't vagevuur, zoo geven die van den deken van
Onze-Lieve-Vrouwekerk--een heilig man die alle deugden beoefent--zulk
eene verkwikking aan de smerten des vuurs, dat dit laatste seffens in
ijs verandert. Maar de duivelen, die het vuur moeten poken, krijgen
er geen zier van.

En weer luisterde het volk devotelijk, doch niet zonder lachen,
en de deken glimlachte inwendiglijk.

Verder werd Uilenspiegel voor drie jaren uit Vlaanderenland gebannen;
hem werd tevens opgelegd eene bedevaart naar Rome te doen en terug
te komen met de Pauselijke absolutie.

Klaas moest drie gulden voor deze sententie betalen, maar hij gaf er
nog eenen aan zijn zoon en daarboven eene pelgrimspij.

Op den dag van 't vertrek was Uilenspiegel 't hert in, toen hij
Klaas en Soetkin kuste, want ze schreide bitter, de arme moeder. Zij
deden hem uitgeleide tot verre op den weg, in gezelschap van meerdere
poorters en poorteressen.

Toen Klaas terug in de hut trad, sprak hij tot Soetkin:

--Vrouwe, 't is toch wreed een zoo jongen knaap tot zulke strenge
straf te veroordeelen, en dit voor eenige lichtzinnige woorden.

--Gij weent, man, sprak Soetkin; gij bemint hem meer dan ge wilt laten
blijken, want daar berst gij uit in mannelijke snikken, die de tranen
van den leeuw zijn.

Maar hij antwoordde niet.

Nele was zich in de schuur gaan verbergen, opdat niemand zien zou,
dat ook zij weende om Uilenspiegel. Van verre volgde zij Soetkin en
Klaas, de poorters en poorteressen; en toen zij heuren vriend alleen
zag voortgaan, liep ze naar hem en sprong hem om den hals:

--Ginder zult gij schoone vrouwen vinden, sprak zij.

--Schoon, misschien, antwoordde Uilenspiegel, maar toch zoo frisch
niet als gij, want zij zijn allen verbrand van de zonne.

Lang nog stapten zij samen voort: Uilenspiegel was nadenkend en
prevelde van tijd tot tijd:

--Die zielmissen zullen ze mij betalen.

--Welke missen en wie zal betalen? vroeg Nele.

Uilenspiegel antwoordde:

--Alle dekenen, parochiepapen, geestelijken, kosters en andere
hooge en lage zotskappen, die ons allerhande domheden willen doen
slikken. Was ik een noeste arbeider geweest, dan was ik voor drie jaar
mijn dagloon bestolen, met hunne bedevaart. Maar 't is de arme Klaas,
die betaalt. Mijne drie jaar zal ik hun honderdvoudig betaald zetten;
ik zal hun eene zielmis zingen, die hun aan de ribben zal hangen.

--Laas! Thijl, wees toch voorzichtig, zij zouden u levend verbranden,
antwoordde Nele.

Ik ben vuurvast, antwoordde Uilenspiegel.

En zij namen afscheid van elkander: zij badend in tranen, hij
droefgeestig en gram.




XXXIII.

Toen Uilenspiegel door Brugge, over de Woensdagmarkt kwam, zag hij
daar eene vrouw, die rondgeleid werd door den beul en zijne knechten,
en een groote menigte andere vrouwen, die rondom haar tierden en heur
allerhande vuile beleedigingen toewierpen.

Daar zij boven aan heur kleed roode lapjes en den steen der justitie
met zijne ijzeren ketenen om den hals droeg, begreep Uilenspiegel,
dat het eene vrouw was, die het jeugdig en maagdelijk lichaam van hare
dochteren verkocht had. Men zei hem, dat zij Barbara hiet en getrouwd
was met Jason Darue; dat ze in dit gewaad van de eene plaats naar
de andere gesleurd werd, totdat zij terugkwam op de Groote Markt,
waar het schavot voor haar gereed stond. Uilenspiegel volgde haar
met de menigte, die achter heur huilde en tierde. Op de Groote Markt
teruggekomen, werd zij op het schavot gebracht, aan eenen paal
gebonden, en de beul legde voor hare voeten een hoop gras en een
klomp aarde: de bediedenis van het graf.

Ook zei men aan Uilenspiegel, dat ze vooraf in 't gevang gegeeseld was.

Voortgaande, ontmoette hij Hendrik Marischal, schooier, die in de
kasselrij West-Ieperen gehangen was geweest; rond den hals toonde
hij nog het merk van de koorden. Hij hing al in de lucht, zegde hij,
en was verlost geworden alleenlijk door een goed gebed te doen tot
Onze-Lieve-Vrouwe van Halle, zoodanig, dat, toen de baljuws en de
rechters vertrokken waren, door een echt mirakel de koorden braken
en hij ongedeerd ten gronde viel.

Maar later hoorde Uilenspiegel zeggen, dat die van de koorden verloste
bedelaar een valsche Hendrik Marischal was, en dat men hem zijne
leugen liet rondventen, omdat hij een perkament had, afgegeven door
den deken van Onze-Lieve-Vrouwe van Halle, die, door het vertelsel
van dien Hendrik Marischal, de galgenazen in grooten getale met rijke
offeranden naar zijne kerk lokte. En Onze-Lieve-Vrouwe van Halle werd,
zeer lang nog, Onze-Lieve-Vrouwe der Gehangenen genoemd.




XXXIV.

In dien tijd moesten kettermeesters en godgeleerden keizer Karel
voor de tweede reis vermanen: dat de Kerke ten onder ging; dat haar
gezag miskend werd; dat zoo hij menigvuldige zegepralen behaald had,
hij dit verschuldigd was aan de gebeden der Katholieke Kerk, die de
keizerlijke macht op haren troon in stand hield.

Een aartsbisschop van Spanje vroeg hem om zes duizend hoofden te
laten afkappen en evenveel lichamen te laten verbranden, ten einde
de kwaadaardige Luthersche ketterije in de Nederlanden uit te
roeien. Zijne Heilige Majesteit oordeelde, dat dit niet voldoende was.

Overal waar de ontzette Uilenspiegel dan ook voorbij kwam, zag hij
niets dan hoofden op palen, meisjes in zakken genaaid en levend in 't
water gesmeten, mannen naakt op het rad gebonden en met ijzeren staven
geslagen, vrouwen levend in eenen kuil gelegd, met aarde boven haar
en den beul op haren boezem dansen om dien te verpletteren. Maar de
biechtvaders van hen die zich vooraf bekeerd hadden, kregen telkenmale
twaalf stuivers voor hunne moeite.

Te Leuven zag hij de beulen dertig Lutheranen tegelijk verbranden
en den brandstapel met schietpoeder aansteken. Te Limburg zag hij
een gansche familie, mannen en vrouwen, dochteren en zonen, ter
strafplaatse leiden. Allen zongen psalmen. Alleen de oude vader
schreeuwde, terwijl hij verbrandde.

En Uilenspiegel ging zijns weegs, met beklemd en toegenepen herte.




XXXV.

In het open veld gekomen schudde hij zich als een vogeltje, als een
hond die den band ontloopen is, en zijn herte was verkwikt bij het
aanschouwen van de boomen, van de beemden, van de heldere zonne.

Als hij drie dagen lang gegaan had, kwam hij omtrent Brussel, in de
machtige gemeente Ukkel. Vóór het gasthof de Trompet, werd zijn neus
gestreeld door een hemelschen geur van stoverije. Aan een kleinen
schooier, die den reuk van de saus opsnoof, vroeg hij ter eere van
welken heilige die wierook omhoog steeg? De kleine antwoordde, dat
de broeders van de Goede Tronie na de vespers moesten bijeenkomen
om de herinnering te vieren van de verlossing der gemeente door hare
vrouwen en meisjes.

Uilenspiegel zag van verre eenen staak met een gaai erop, rond
denwelken vrouwen met bogen liepen; hij vroeg of de vrouwlieden nu
boogschieters waren geworden?

De jongen snoof nog eens den reuk van de keuken op en antwoordde, dat,
in den tijd van den goeden hertog, die zelfde bogen, in de handen der
Ukkelsche vrouwen, meer dan honderd baanstroopers van het leven naar
den dood hadden gestuurd.

Uilenspiegel wilde meer weten, doch de kleine schooier zei dat hij
geen woord meer zeggen zou, zoolang hij honger en dorst had, ten ware
hij een oortje kreeg om zich te verzadigen. Uilenspiegel gaf het hem
uit medelijden.

Zoodra de jongen het oortje had, trok hij, als een vos in een
hoenderhok, het gasthof binnen, om weldra, met een halve worst en
eene dikke snee brood triomfantelijk te voorschijn te komen.

Plotseling hoorde Uilenspiegel een zoete muziek van tamboerijnen en
violen en zag hij vele dansende vrouwen, en onder haar een schoon
wijf met een gouden ketting om den hals.

De schooier, in zijn schik, omdat hij zoo lekker gegeten had, zegde
tot Uilenspiegel dat die jonge, schoone vrouw de koningin van de
gaaischieting was, dat zij Mietje heette en de vrouw was van messire
Renonckel, schepene van de gemeente. Dan vroeg hij aan Uilenspiegel
nog zes duiten om te drinken: Uilenspiegel gaf ze hem. Toen hij
gegeten en gedronken had, zette hij zich in de zonne op de hurken,
en kuischte met zijne nagelen zijne tanden.

Als de boogschietsters Uilenspiegel in zijne pelgrimspij zagen,
begonnen zij rond hem te dansen, zeggende:

--Dag, schoone pelgrim; komt gij van verre, jonge pelgrim?

Uilenspiegel antwoordde:

--Ik kom uit Vlaanderen, het schoone land der verliefde meidekens.

En droefgeestig dacht hij aan Nele.

--Wat was uwe misdaad? vroegen zij, terwijl zij haren dans staakten.

--Ik durf het niet zeggen, daar ze zoo groot is, sprak hij. Bij mij,
mijne hertjes, is niemendal klein.

Zij lachten en vroegen waarom hij aldus moest reizen met den
pelgrimsstaf, den bedelzak en de oesterschelpen?

--'t Is, antwoordde hij, omdat ik gezegd heb, dat de zielmissen
voordeelig zijn voor de priesters.

--Zij brengen hun klinkende munt op, antwoordden de vrouwen, maar
toch zijn zij ook voordeelig voor de zielen in 't vagevuur.

--Daar was ik niet bij, antwoordde Uilenspiegel.

--Komt gij met ons eten? vroeg de schoonste.

--Ik wil, sprak hij, met u eten, en u eten, u en uwe vriendinnen,
de eene na de andere, want gij zijt fijne brokjes, lekkerder dan
ortolanen, lijsters of sneppen.

--De Hemel vergeve u, dat wild is buiten prijs, zeiden zij.

--Zooals gij allen, mijne hertjes, antwoordde hij.

--'t Is te zien, spraken zij, maar wij zijn niet te koop.

--Noch te geven? vroeg hij.

--Ja, zegden zij, wij geven slagen aan degenen die te stout zijn. Hebt
gij er van doen, wij zullen op u slaan lijk op kaf.

--Ik doe niet mee, sprak hij.

--Kom dan mee-eten, zegden zij.

Blijde als hij was rond zich vroolijke, lachende gezichten te zien,
volgde hij heur naar de binnenplaats van het gasthof. Plotseling
zag hij de broeders van de Goede Tronie, in groote staatsie, met
vaandel, fluit, bazuin en tamboerijn, in het binnenhof komen; zij
droegen waardiglijk den naam van hunne broederschap. Daar zij hem
nieuwsgieriglijk bekeken, zeiden de vrouwen dat het een pelgrim was,
dien ze op den weg ontmoet en meegebracht hadden naar 't festijn,
omdat zijne tronie haar aanstond.

De mannen stemden hiermee in, en een hunner sprak:

--Reizende pelgrim, wilt gij eene bedevaart doen in sausen en
stoverije?

--Daarvoor wil ik de leerzen van Duimken aantrekken, antwoordde
Uilenspiegel.

Als hij met hen de zaal van 't festijn binnenging, zag hij op den
Parijschen steenweg een twaalftal blinden. En toen zij voorbij hem
kwamen, kloegen zij van honger en dorst. Uilenspiegel zei tot zich
zelven, dat zij dien dag zouden avondmalen als prinsen, en wel ten
koste van den deken van Ukkel, op afkorting van de zielmissen.

Hij ging tot hen en sprak:

--Hier zijn negen gulden, gij kunt komen eten. Riekt gij den geur
niet van de stoverije?

--Laas! spraken zij, reeds een half uur lang, doch zonder hope.

--Gij zult eten, zegde Uilenspiegel, vermits gij nu negen gulden
hebt. Maar hij gaf ze hun niet.

--Wees gezegend! spraken zij.

En, door Uilenspiegel geleid, zetten zij zich rond een kleine tafel,
terwijl de broeders van de Goede Tronie met hunne wijven en dochteren
aan de groote tafel gingen zitten.

Met een zelfvertrouwen van negen gulden, riepen de blinden luide
en stout:

--Baas, geef ons te eten en te drinken, alles van 't beste!

De weerd, die van negen gulden had hooren spreken, dacht, dat die in
hunne tasschen staken en vroeg wat de gasten wenschten.

Toen riepen allen tegelijk:

--Boonen met spek, hutspot met rundvleesch, kiekens, kalfsvleesch en
hamelbout. Zijn de worsten voor de honden gemaakt?

--'k Heb witte en zwarte pensen geroken; 'k zou ze zien als ik nog
mijne lanteernen had.--Waar zijn de koekebakken met Anderlechtsche
boter? Zij zingen in de pan, sappig, knappend en hijgend naar het bier,
waarmede wij ze zullen begieten.--Wie geeft de hesp met eieren die
onzen mond placht te streelen?--Waar zijt gij, hemelsche soezels, die
zwemmen midden in de nieren, hanekammen, kalfszwezeriken, ossesteerten,
schapepooten, met veel ajuin, peper, kruidnagelen, muskaat, goed
ondereengestoofd met drie pinten witten wijn als saus?--Wie brengt
u tot mij, goddelijke kalfsworsten, die zoo goed zijt dat ge niets
zegt als men u opeet? Kwaamt gij recht uit Luilekkerland, waar niet
te werken valt, en eten en drinken een ambacht is? Gij zijt verdwenen
lijk de droge bladeren van den jongsten herfst.--Ik wil een hamelbout
met erwtjes.--Ik, verkensooren.--Ik, een rozenkrans van ortolanen,
met sneppen als paters en een vetten kapoen als credo.

De weerd antwoordde bedaard:

--Gij krijgt een pannekoek van zestig eieren en, als wegwijzers om
uwe vorken te bestieren, vijftig zwarte pensen, rookend op dien berg
van eieren gestoken, en als drank dobbelen peeterman: dat zal de
riviere wezen.

Het water kwam in den mond van de arme blinden.

Breng ons den berg, de wegwijzers en de rivier!

En de broeders van de Goede Tronie en hunne vrouwen, die reeds
met Uilenspiegel aan tafel zaten, zeiden, dat deze voor de blinden
onzichtbare smulpartij hun slechts de helft van het genot deed smaken.

Toen de weerd en vier koks den pannekoek opdienden, opgesmukt met
peterselie en keur van kruiden, wilden de blinden er zich op werpen,
maar de weerd gaf, niet zonder moeite, eerlijk aan elk zijn deel.

De boogschietsters waren verrukt als zij hen zagen slempen en zuchten
van genoegen, want zij hadden grooten honger en sloegen de pensen
binnen lijk oesters. De dobbele peeterman liep in hunne magen gelijk
een waterval van het hooggebergte.

Toen de blinden hunne teilen uitgewischt hadden, vroegen zij opnieuw
koekebakken, ortolanen en weer stoverije. De weerd bracht hun slechts
een grooten schotel ossen-, kalfs- en schapenbeenderen, die in goede
saus zwommen. Maar hij deelde niet rond.

Als zij hun brood en hunne handen, tot over de polsen, in de saus
gedoopt hadden, en niets vonden dan hamel-, kalfs- en andere beenderen,
meende een iegelijk dat zijn buurman al het vleesch had, en verwoed
sloegen zij met de beenderen op elkanders gezicht.

Bij dat schouwspel lachten de broeders van de Goede Tronie naar
hertelust en legden goedhertig een deel van 't festijn op de teil van
de arme blinden, en een iegelijk die een been zocht om er mee te slaan,
legde de hand op eene lijster, een kieken, een koppel leeuwerikken,
terwijl de vrouwen hun het hoofd achterover hielden en hun Brusselschen
wijn lieten drinken, zooveel zij konden. En als de arme lieden op den
tast zochten van waar die stroom godendrank kwam, grepen zij een rok,
die gezwind uit hunne handen glipte.

Zij lachten, aten, dronken en zongen zoo heerlijk! Eenigen vermoedden
dat er poezele wijfjes moesten zijn en liepen, dol van liefde,
de eetzale rond, maar de plaagzieke vrouwen draaiden zich om en
verborgen zich achter een broeder van de Goede Tronie, roepende:
"Kus mij, toe!" En als de blinden het deden, kusten zij in stee van
de donzige huid eener vrouw, het harig gezicht van een man--maar niet
zonder kletsen te krijgen.

De broeders van de Goede Tronie zongen, en zij zongen insgelijks. En
de vrouwen glimlachten teeder, met stil genoegen, als zij hen zoo
vol vroolijkheid zagen.

Toen die heerlijke uren voorbij waren, sprak de baas:

--Gij hebt goed gegeten en gedronken, geef mij nu zeven gulden.

Elk hunner zwoer dat hij de beurze niet had en beschuldigde zijn
buurman. Een nieuw gevecht ontstond, in hetwelk zij elkaar trachtten
te schoppen, te slaan en te stompen, maar de broeders van de Goede
Tronie hielden de vechtenden van elkaar. En 't regende slagen in de
lucht, behalve een die bij ongeluk terecht kwam op 't aangezicht van
den baas, die, verwoed, nu allen aftastte en niets anders vond dan een
versleten schapulier, zeven duiten, drie knoopen en hunne paternosters.

Hij wilde hen in het verkenskot steken en hen daar op water en brood
zetten, totdat liefdadige zielen voor hen betaald hadden.

--Wilt gij, vroeg Uilenspiegel, dat ik borg voor hen blijve?

--Ja, antwoordde de baas, als iemand ook voor u borg is.

De Goede Tronies wilden borg zijn, maar Uilenspiegel voorkwam hen
en zei:

--De deken zal borg zijn, ik ga het hem vragen.

Gedachtig aan de zielmissen, trok hij naar den deken en vertelde hem
dat de baas uit de Trompet van den duivel bezeten was, en dat hij
van anders niets sprak dan van verkens en blinden; dat de verkens de
blinden opaten en de blinden de verkens. Middelerwijl, zoo vertelde
hij, brak de baas thuis alles aan stukken, en hij bad hem den armen
man van dien boozen duivel te komen verlossen.

De deken beloofde het, maar zei, dat hij niet dadelijk kon komen,
mits hij bezig was met de rekening van 't kapittel te maken en dat
dit zeer lastig was, zoo hij zijn garande wilde hebben.

Toen Uilenspiegel zag dat hij ongeduldig werd, zegde hij dat hij
zou terugkomen met het wijf van den baas en dat de deken haar zelve
kon spreken.

--'t Is goed, antwoordde de deken.

Uilenspiegel keerde terug bij den baas en zegde:

--Ik heb den deken gesproken, hij blijft borg voor de blinden. Terwijl
gij op hen let, kan de bazinne meekomen, en hij zal heur herhalen
wat ik u zegde.

--Ga mee, vrouw, sprak de baas.

De bazinne ging met Uilenspiegel bij den deken, die maar altijd aan
't cijferen was, om zijn aandeel te vinden. Toen zij binnenkwam met
Uilenspiegel, maakte hij met de hand een driftig gebaar, zeggende:

--Ga heen en wees gerust: morgen of overmorgen kom ik bij uwen man.

En toen Uilenspiegel naar de Trompet terugkeerde, sprak hij onderweg
in zich zelven: "Hij zal honderd gulden betalen en dat zal mijn eerste
zielmisse zijn."

En hij ging zijns weegs, en de blinden insgelijks.




XXXVI.

's Anderen daags kwam Uilenspiegel op eene baan vol volk. Hij volgde
de menigte en vernam, dat het dien dag beeweg naar Alsemberg was.

Hij zag er arme oude vrouwen, die, voor een gulden en om de zonden van
voorname dames te boeten, barrevoets achterweerts gingen. Terzijde
van den weg deed meer dan één pelgrim zich te goed aan wafelen en
bruinbier, bij geschal van lieren, violen en doedelzakken. En de reuk
van allerhande spijzen steeg ten hemel als een zoete wierook.

Maar daar waren ook pelgrims, die er gemeen en ellendig uitzagen;
die hadden zes stuivers van de Kerk gekregen, om achterweerts den
beeweg te doen.

Een kaalhoofdig manneken, met opengesperde oogen, volgde hen insgelijks
achterweerts springend en vaderonzen zeggend.

Uilenspiegel, die wilde weten waarom hij aldus de kreeften naäapte,
ging voor hem staan en sprong glimlachend lijk hij. Lieren, pijpen,
violen en doedelzakken, waren met het geschreeuw van de pelgrims,
de muziek van dien dans.

--Zeg eens, Jan van den Duivel, sprak Uilenspiegel, is het om zeker
te zijn van vallen, dat gij averechts gaat?

De man antwoordde niet en bad voort.

--Of is het om de boomen te tellen, vervolgde Uilenspiegel, en
misschien ook de bladeren er bij?

De man, die een credo zei, deed Uilenspiegel teeken dat hij zwijgen
moest.

--Of, sprak deze, altijd vóór hem springend en zijne gebaren
nabootsend, zijt gij misschien eensklaps zot geworden, dat gij
loopt lijk de kreeften? Maar wie van een zot een verstandig antwoord
verwacht, is zelf niet wijs. Niet waar, mijnheer de kaalkop?

Daar de man nog niet antwoordde, danste en sprong Uilenspiegel voort,
doch hij maakte daarbij zooveel lawijd met zijne zolen, dat de weg
klonk als een houten kist.

--Of zijt gij stom, mijnheer? vroeg Uilenspiegel ten slotte.

--Ave Maria, sprak de man, gratia plena et benedictus fructus ventris
tui, Jesu.

--Of misschien doof? zei Uilenspiegel. Dat gaan wij dadelijk zien:
men zegt, dat dooven vleierij noch beleediging hooren. Laat zien of
de trommel van uw ooren van vel of van ijzer is: Meent gij, lanteerne
zonder keers, mislukte voetganger, dat gij een mensch gelijkt? Ge
kunt wachten totdat wij van vodden gemaakt zijn. Zag men ooit zulke
gele tronie, zulk een kletshoofd, elders dan op een galgeveld? Zijt
ge in uw leven nooit gehangen geweest?

En Uilenspiegel danste steeds voort, en de man, die kwaad werd,
stapte boosaardig achterwaarts en bad zijn vaderonzen met heimelijke
verbolgenheid.

--Of misschien, sprak Uilenspiegel, verstaat gij geen Hoogvlaamsch;
daarom ga ik u in 't Platvlaamsch aanspreken: Zijt gij geen gulzigaard,
dan zijt gij een dronkaard; zijt gij geen dronkaard, dan zijt gij
verstopt; zijt gij niet verstopt, dan hebt gij den afgang; als er
matigheid is, dan is zij het niet, die de tonnen van uw buik vult;
zijt gij geen losbol, dan zijt gij een kapuin en als er op de duizend
millioen mannen der aarde maar één horendrager was, dan zoudt gij
het zijn....

Op die rede, viel Uilenspiegel op zijn achterste, met de beenen omhoog,
want de man had hem zulk een vuistslag op den neus toegediend, dat
het vuur hem uit zijne oogen sprong. Dan liet de man zich, ondanks
zijn dikken buik, verraderlijk op hem vallen en sloeg hem overal,
dat de slagen als hagelsteenen op het magere lichaam van Uilenspiegel
vielen. En Uilenspiegels stok rolde mede ten gronde.

--Dat zal u leeren, sprak de man, eerlijke menschen kwellen die
op bedevaart gaan. Want--gij moogt het wel weten--ik ook ga naar
Alsemberg, volgens aloud gebruik, om Onze-Lieve-Vrouwe te bidden,
een kind te willen doen afkomen, dat mijne vrouw ontving terwijl
ik op reis was. Om zulk een groote genade te verkrijgen, moet men,
zonder spreken, achterweerts loopen en dansen van den twintigsten
stap voorbij zijn huis tot aan de trappen der kerk. Laas! nu moet ik
geheel opnieuw beginnen.

Uilenspiegel, die zijn stok opgeraapt had, sprak:

--Ik zal u helpen, deugniet, die Onze-Lieve-Vrouwe wilt smeeken om
de kinderen vóór hun geboorte te vermoorden.

En meteen sloeg hij den leelijken horendrager zoo deerlijk, dat hij
hem voor dood op den weg liet.

En nog altijd steeg het gehuil der pelgrims en het geluid van  pijpen,
lieren, violen en doedelzakken omhoog, met den geurigen wierook van
gekook en gebraad.




XXXVII.

Klaas, Soetkin en Nele zaten samen rond den heerd en praatten over
den reizenden pelgrim.

--Meisje, sprak Soetkin, kondet gij hem voor altijd bij ons houden
door uwe jeugd en uwe schoonheid!

--Laas! sprak Nele, ik kan niet.

--Omdat hij, antwoordde Klaas, meer behagen vindt in loopen, zonder
ooit te rusten, tenzij om te eten.

--De leelijke stouterik! zuchtte Nele.

--Ik geef toe dat hij stout is, sprak Soetkin, maar leelijk is
hij niet. Als Uilenspiegel Grieksch noch Romeinsch van gezicht is,
is hij des te schooner; want Vlaamsch zijn zijne vlugge voeten, van
't Brugsche Vrije zijn levendige bruine oogen; en zijn neus en mond
zijn gemaakt door twee vossen, ervaren in de kunsten van slimheid
en verstand.

--Wie dan, vroeg Klaas, maakte hem zijne armen van luierik en zijne
beenen, die al te vlug loopen naar vermaak en pleizier?

--Zijn al te jeugdig herte, was 't antwoord van Soetkin.




XXXVIII.

In dien tijd genas Katelijne, met kruiden, een os, drie schapen en een
verken toebehoorende aan Speelman, doch de koe van Jan Beloen kon ze
niet genezen. Jan beschuldigde haar van hekserij en verklaarde, dat
zij het dier betooverd had, daar zij, terwijl zij het de geneeskruiden
gaf, het gestreeld en aangesproken had, zeker in een duivelsche tale,
want een eerlijk christenmensch mag het woord tot geen dier richten.

Gemelde Jan Beloen voegde er bij, dat hij gebuur was van Speelman,
wiens os, schapen en verken zij genezen had en, zoo zij zijne
koe gedood had, het zeker was op het opstoken van Speelman, die
jaloersch was, omdat zijne akkers slechter bebouwd waren en minder
opbrachten dan de zijne--van Beloen namelijk. Op getuigenis van Pieter
Meulemeester, een man van goed gedrag en zeden, en ook van Jan Beloen,
die bevestigden dat Katelijne te Damme bekend stond als tooveres,
en naar allen schijn de koe gedood had, werd Katelijne aangehouden
en veroordeeld om op de pijnbank te worden gelegd totdat zij hare
misdaden bekende.

Zij werd ondervraagd door een schout, die altijd narrig was, want
heel den dag door dronk hij brandewijn. Vóór hem en vóór die van de
Vierschaar, deed hij Katelijne op de eerste pijnbank zetten.

De beul ontkleedde haar en keek of zij geenerlei hekserij verborgen
hield.

Hij vond niets, en bond heur met koorden op de pijnbank. Toen
zegde zij:

--Heilige Moeder Gods, laat mij sterven, dat ik mijne schamelheid
aan die mannen niet hoeve te toonen!

Toen legde de beul natte doeken op heure borst, heuren buik en heure
armen; vervolgens hief hij de bank op en goot hij heet water in
heure keel, bij zulke groote hoeveelheid, dat zij gansch opgeblazen
scheen. Vervolgens liet hij de bank nedervallen.

De schout vroeg aan Katelijne of zij hare misdaad wilde bekennen. Zij
schudde het hoofd. Toen goot de beul nog heet water in haren mond,
maar Katelijne gaf het allemaal over.

Op het oordeel van den heelmeester, werd zij toen losgemaakt. Zij
sprak niet, doch klopte op hare borst om te zeggen, dat het heet
water haar verbrand had. Toen de schout zag dat zij van haar eerste
foltering bekomen was, sprak hij:

--Beken, dat gij tooveres zijt en dat gij de koe betooverd hebt.

--Ik zal niet bekennen, sprak zij. Zooveel het in de macht van mijn
zwak herte ligt, zie ik alle beesten geerne, en ik deed nog liever leed
aan mij zelve dan aan hen, daar zij zich niet verdedigen kunnen. Om
de koe te helpen, heb ik de geneeskruiden gebruikt, die ik moest.

Maar de schout sprak:

--Gij hebt vergif gebruikt, want de koe is gestorven.

--Heere schout, antwoordde Katelijne, ik ben hier voor u en in uwe
macht; en toch durf ik zeggen, dat een dier, evenals een mensch,
van ziekte kan sterven, niettegenstaande de hulp van artsen en
heelmeesteren. En bij Jezus-Christus, die voor onze zonden op het
kruis is gestorven, zweer ik dat ik die koe geenerlei kwaad gewild heb,
doch getracht heb ze te genezen met de gebruikelijke kruiden.

Woedend sprak toen de schout:

--Die tooverkol zal niet blijven afstrijden; men brenge heur op een
andere pijnbank!

En daarna dronk hij een groot glas brandewijn.

De beul deed Katelijne zitten op het deksel eener eiken doodkist,
die op pikkels stond. Dat deksel, in den vorm van een dak, was scherp
als een mes. Een groot vuur brandde in den schoorsteen, want men was
toen in de slachtmaand.

Katelijne werd op de doodkist en op een scherpe houten pinne gezet,
en men deed haar nieuwlederen schoenen aan die te smal waren. Zóó
schoof men heur tegen het vuur. Als zij de snede van de doodkist
en de scherpe pinne in heur vleesch voelde dringen, en de hitte van
't vuur het leder van de schoenen deed krimpen, riep zij uit:

--Ik lijd ongemeene smerten! Wie geeft mij zwart vergif?

--Breng haar dichter bij 't vuur, sprak de schout.

Toen ondervroeg hij Katelijne.

--Hoe dikwijls, sprak hij, reedt gij op een bezemsteel naar den
heksensabbat? Hoe dikwijls deedt gij het koren in de aar, de vrucht
op den boom, het kind in den schoot vergaan? Hoe dikwijls zaaidet
gij haat en nijd in de herten van broeders en zusters?

Katelijne wilde spreken, maar zij kon niet, en zij zwaaide met hare
handen als om "neen" te bedieden. Toen zegde de schout:

--Zij zal niet spreken vooraleer zij al heur heksenvet zal voelen
smelten. Breng haar nog dichter bij het vuur.

Katelijne schreeuwde. De schout zegde heur:

--Bid Satan dat hij u verfrissching bezorge.

Met het gezicht vol smerte, wees zij naar heure schoenen, die rookten
ten gevolge van de hitte des vuurs.

--Bid Satan, dat hij ze uitdoe, sprak de schout.

Tien uren sloeg de klok, dit was het etensuur van den wreedaard;
hij vertrok met zijn schrijver, den beul en zijn knechten, en liet
Katelijne alleen bij 't vuur, in de folterkamer.

Te elf uren kwamen zij terug, en zij vonden Katelijne stijf en
onbeweeglijk zitten. De schrijver sprak:

--Ik geloof, dat zij dood is.

De schout beval Katelijne van de doodkist te nemen en heure schoenen
uit te doen. De beul moest ze vaneen snijden en Katelijne's voeten
waren rood en bloedden.

En de schout, die aan zijn maaltijd dacht, bezag ze, doch uitte geen
woord; doch weldra kwam ze tot heur zelve terug; zij viel ten gronde
zonder zich te kunnen oprichten ondanks al heure krachtsinspanning,
en sprak tot den schout:

--Vroeger wildet gij mij voor echtgenoote, maar nu zult gij mij niet
meer hebben. Viermaal drie is een heilig getal, en de dertiende is
de echtgenoot.

Vervolgens, daar de schout wilde spreken, zegde zij tot hem:

--Zwijg stille: hij hoort beter dan de aartsengel, die in den hemel
de hertkloppingen der rechtvaardigen telt. Waarom komt gij zoo
spa? Viermaal drie is een heilig getal; het doodt de ellendelingen,
die mij willen vervolgen.

De schout sprak:

--Zij ontvangt den duivel in heur bedde.

--Zij is uitzinnig, ten gevolge van de smerten der foltering, sprak
de schrijver.

Katelijne werd terug naar 't gevang gebracht. Drie dagen nadien kwamen
de schepenen in de Vierschaar bijeen en, na rijpe beraadslaging,
werd Katelijne veroordeeld tot de straffe des vuurs.

De beul en zijne knechten brachten heur naar de Groote Markt van
Damme, alwaar een schavot opgericht was, hetwelk zij beklom. Op de
Markt stonden de provoost, de heraut en de rechters.

Driemaal klonken de bazuinen van den stadsheraut en deze sprak tot
het volk:

--De magistraat van Damme, medelijden gekregen hebbende met vrouwe
Katelijne, heeft haar niet willen straffen volgens al de strengheid
van de wet van de stede, maar tot teeken dat zij tooveres is, zal heur
haar verbrand worden; verder zal zij twintig gouden karolussen boete
betalen en voor drie jaar gebannen worden uit de stede van Damme,
op verbeurte van een lid.

En het volk juichte die barbaarsche goedertierenheid toe.

De beul bond Katelijne toen aan eenen paal, legde op heur geschoren
hoofd eene pruik van werk en stak die in brand. En het werk brandde
lang, en Katelijne schreeuwde en huilde van pijn.

Eindelijk werd zij losgemaakt; zij werd op eene kar buiten het
grondgebied van Damme gebracht, want heure voeten waren verbrand.




XXXIX.

Terwijl Uilenspiegel te 's-Hertogenbosch in Brabant was, wilden de
heeren van de stad hem tot hunnen nar benoemen, maar die weerdigheid
weigerde hij, zeggende: "Reizende pelgrims mogen zich nergens vestigen;
hun verblijf is de groote baan."

Rond dien tijd kwam Philippus, die koning van Engeland was, zijne
toekomstige erfstaten Vlaanderen, Brabant, Henegouwen, Holland en
Zeeland bezoeken. Hij was in zijn negen en twintigste jaar; in zijne
grijze oogen las men bittere droefgeestigheid, woeste geveinsdheid
en wreedaardige vastberadenheid. Koud was zijn aangezicht, stijf
zijn hoofd met vaalrood haar, alsmede zijn mager lichaam en zijne
schrale beenen. Langzaam en slijmerig sprak hij, alsof hij wolle in
den mond had.

Te midden van tornooien, steekspelen en feesten, bezocht hij
achtereenvolgens het vroolijke hertogdom Brabant, het rijke graafschap
Vlaanderen en zijne andere heerlijkheden. Overal beloofde hij onder
eede de privileges te zullen eerbiedigen; maar toen hij te Brussel
op 't Evangelie zwoer de Brabantsche gouden bul te zullen in stand
houden, trok zijne hand zoodanig te zamen, dat men hem het heilige
boek moest afnemen.

Hij ging naar Antwerpen, waar men drie en twintig zegebogen
oprichtte om hem te ontvangen. De stad gaf tweehonderd zeven en
tachtig duizend gu