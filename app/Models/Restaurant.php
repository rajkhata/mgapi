<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rest_code', 'restaurant_name', 'description', 'lat', 'lng', 'address', 'street', 'zipcode', 'mobile',
        'phone', 'accept_cc', 'accept_dc', 'delivery', 'takeout', 'dinning', 'reservation', 'menu_available',
        'total_seats', 'facebook_url', 'twitter_url', 'gmail_url', 'pinterest_url', 'instagram_url', 'yelp_url',
        'tripadvisor_url', 'foursquare_url', 'twitch_url', 'youtube_url','language_id','supported_languages','landmark',
        'delivery_provider_id','producer_key','delivery_provider_apikey','contact_address','delivery_service_charge','delivery_charge',
	 'minimum_delivery',' delivery_charge_type', 'free_delivery',' flat_discount_start_date',' flat_discount_end_date',' flat_discount','flat_discount_type',
	 'flat_discount_min','service_tax','additional_charge_type','additional_charge','additional_charge_name','currency_symbol','currency_code','country_iso_code','common_message','common_message_status','common_message_url'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'password',
    ];


    public function users() {
        //return $this->hasMany(User::class);
        return $this->hasMany('\App\Models\User','restaurant_id');
    }

    /* belongsTo Relation with Cities @27-07-2018 by RG */

    public function city() {
        return $this->belongsTo('\App\Models\City');
    }

    public function dashboard_services_logs() {
        //return $this->hasMany(User::class);
        return $this->hasMany('\App\Models\DashboardServicesLog','restaurant_id');
    }

    public function scopeActive($query)
    {
        return $query->select('id', 'restaurant_name', 'parent_restaurant_id','is_default_outlet', 'slug')
            ->where('status', 1)
            ->get();
    }
}
