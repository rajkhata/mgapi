<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract, JWTSubject
{
    use Authenticatable, CanResetPassword;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id','surname', 'username', 'fname', 'lname', 'email', 'password', 'mobile', 'phone', 'dob', 'display_pic_url',
        'billing_address', 'shipping_address', 'status', 'stripe_customer_id', 'last_login', 'user_group_id','language_id','category', 'is_registered', 'referral_code', 'referral_restaurant_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'password',
    ];

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function order()
    {
        return $this->hasMany('App\Models\UserOrder');
    }

    public function address()
    {
        return $this->hasMany('App\Models\UserAddresses');
    }

    public function card()
    {
        return $this->hasMany('App\Models\UserCards');
    }
}
