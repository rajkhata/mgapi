<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ItemAddonGroupsLanguage;

class ItemAddonGroup extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'restaurant_id', 'menu_item_id', 'group_name', 'language_id', 'prompt', 'quantity_type', 'quantity',
    ];

    protected $table = 'item_addon_groups';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function state() {
        return $this->belongsTo('App\Models\State');
    }


    public function addonitems() {
        return $this->hasMany('App\Models\ItemAddons', 'addon_group_id');
    }

    public function itemAddonGroupsLanguage() {
        return $this->hasMany(ItemAddonGroupsLanguage::class, 'item_addon_group_id', 'id');

    }
}
