<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RestaurantTakeoutWeekCalendar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_restaurant_carryout_week_time';
    protected $fillable = [
        'id','restaurant_id','calendar_day','slot_detail','is_dayoff'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}
