<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryConfig extends Model
{
    protected $table = 'delivery_config';
    /**
    }
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','restaurant_id','delivery_service_id','minimum_order_amount','additional_charge_delivery_type','additional_charge_delivery_order','additional_charge_takeout_type','additional_charge_takeout_order','additional_charge_description','rate_type','sort_order','tax','service_tax','tip','free_delivery_above_sub_total','delivery_condition','filtering_rule','status'
    ];

}