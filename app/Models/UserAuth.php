<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserAuth extends Model
{
    protected $table = 'user_auth';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_details', 'platform', 'guest_token', 'access_token', 'ttl','otp','language_id', 'parent_restaurant_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
