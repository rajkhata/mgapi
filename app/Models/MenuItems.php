<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class MenuItems extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id',
        'category_id', 
        'sub_category_id', 
        'name', 
        'description', 
        'image', 
        'thumb_image_med', 
        'image_caption', 
        'size', 
        'price', 
        'status',
        'language_id',
        'product_type',
        'gift_wrapping_fees',
        'gift_message',
        'display_order',
        'is_popular',
        'is_favourite',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];

     public function maincat()
    {
        return $this->belongsTo('App\Models\MenuCategories','menu_category_id');
    }
    public function getItemOtherInfoAttribute()
    {
      return $gifcardinfo =(object) Config('constants.gift_card_config');
        //return json_encode($gifcardinfo);  
    }


}
