<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'is_guest', 'user_id', 'user_auth_id', 'location_id', 'user_ip', 'surname','fname','lname', 'email', 'pos_status','order_state',
        'address', 'address2', 'city', 'zipcode', 'latitude', 'longitude', 'phone', 'order_type', 'order_amount','routed_restaurant_id','is_urgent',
        'card_number', 'card_type', 'tax','service_tax','additional_charge','additional_charge_name','flat_discount', 'tip_amount', 'tip_percent', 'total_amount', 'delivery_charge', 'delivery_time',
        'delivery_date', 'user_comments', 'status','order_state', 'stripe_card_id', 'stripe_charge_id', 'cod','language_id','product_type', 'is_asap_order', 'address_label', 'state','state_code','is_pot','updated_at','promocode','promocode_discount'

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    // Removed created form hidden by RG in find query this field was not coming /patments/opa_order

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function details()
    {
        return $this->hasMany('App\Models\UserOrderDetail', 'user_order_id');
    }
}
