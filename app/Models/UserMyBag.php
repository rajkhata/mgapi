<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMyBag extends Model
{
    protected $table = 'user_mybags';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id','parent_restaurant_id','user_auth_id','user_id','menu_id','menu_json','special_instruction','quantity','unit_price',
        'image', 'total_menu_amount','order_type','delivery_charge','status','language_id', 'size', 'user_auth_id','is_pot','is_byp','item_other_info','discount',
			'additional_charge', 'additional_charge_type', 'additional_charge_name' ,'coupon_code','pos_id','pos_data','images'
    ];


    

     public function addonoptions()
    {
        return $this->hasMany('App\Models\MybagOptions','bag_id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at'
    ];
}
