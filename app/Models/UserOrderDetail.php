<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrderDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='user_order_details';
    protected $fillable = [
        'user_order_id', 'item', 'item_size', 'menu_id', 'image', 'menu_json', 'special_instruction', 'quantity', 'unit_price', 'total_item_amt', 'status','is_pot', 'is_byp', 'quantity','item_other_info','pos_id','pos_data'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function giftdetails()
    {
        return $this->hasMany('App\Models\GiftcardCoupons', 'order_detail_id');
    }

    public function addonoptions()
    {
        return $this->hasMany('App\Models\OrderDetailOptions','user_order_detail_id');
    }

     public function getOrderDetail($orderId)
    {
        return self::where('user_order_id', $orderId)->get();
    }
}
