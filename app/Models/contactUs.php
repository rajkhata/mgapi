<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class contactUs extends Model{
    
    protected $table = 'contactus';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email','restaurant_id','language_id','inquiry_type','customer_service','address','city','state','zip','phone_no','best_buy_date','comment','upc_code','status','device_ip','platform',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
