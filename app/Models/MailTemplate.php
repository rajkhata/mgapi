<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class MailTemplate extends Model
{
    protected $table = 'mail_templates';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'template_name', 'content','subject', 'status', 'language_id','restaurant_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}
