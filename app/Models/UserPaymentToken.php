<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserPaymentToken extends Model
{
    protected $guarded = ['id'];
    protected $table = 'user_payment_tokent';	
   
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

     
}
