<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierOptionGroupItem extends Model
{
	use SoftDeletes;

    protected $fillable = ['modifier_option_name','price','quantity_type','quantity','modifier_option_group_id'];
    protected $table='modifier_item_options';

    protected $dates = ['deleted_at'];

    
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function itemmodifiers()
    {
        return $this->hasMany('App\Models\ItemModifier','modifier_option_group_id');
    }

    public function itemOptionsModifierLanguage() {
        return $this->hasMany('App\Models\ItemOptionsModifierLanguage', 'modifier_option_id');
    }

   
}
