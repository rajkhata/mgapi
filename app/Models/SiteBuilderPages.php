<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class SiteBuilderPages extends Model
    {
        use SoftDeletes;

        protected $fillable = ['title','slug','meta_description','meta_keyword','description','status','restaurant_id','promo_banners','include_in_navigation','user_group_promotion','sort_order'];
        protected $table='sb_pages';

        protected $dates = ['deleted_at'];




    }
