<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_id','country_id','city_name','neighbouring','locality','state_code','latitude','longitude',
        'sales_tax','status','time_zone','city_name_alias','is_browse_only','seo','language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
