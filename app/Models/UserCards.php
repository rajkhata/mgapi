<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCards extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id','parent_restaurant_id','user_id', 'last_four', 'card', 'type', 'name', 'expiry', 'status', 'stripe_card_id','stripe_user_key', 'stripe_token_id', 'zipcode','language_id','is_default_card'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
