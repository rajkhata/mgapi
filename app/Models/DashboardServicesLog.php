<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DashboardServicesLog extends Model{   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cms_user_id','effective_date','service_type','activity_type','restaurant_id','reason','ipaddress','created_at',
        'updated_at'
    ];
   
}
