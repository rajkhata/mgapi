<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewMenuItems extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'restaurant_id',
        'prompt',
        'category_id', 
        'sub_category_id', 
        'name', 
        'description', 
        'image', 
        'thumb_image_med', 
        'image_caption', 
        'size', 
        'price', 
        'status',
        'language_id',
        'product_type',
        'gift_wrapping_fees',
        'gift_message',
        'display_order',
        'is_popular',
        'is_favourite',
        'manage_inventory',
        'max_allowed_quantity',
        'min_allowed_quantity',
        'inventory',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function maincat() {
        return $this->belongsTo('App\Models\MenuCategories', 'menu_category_id');
    }

    public function getItemOtherInfoAttribute() {
        return $gifcardinfo = (object) Config('constants.gift_card_config');
        //return json_encode($gifcardinfo);
    }

    public function addongroup() {
        return $this->hasMany('App\Models\ItemAddonGroup', 'menu_item_id');
    }

    public function menuLanguage() {
        return $this->hasMany('App\Models\NewMenuItemsLanguage', 'new_menu_items_id');
    }

}
