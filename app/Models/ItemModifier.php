<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifier extends Model
{
	use SoftDeletes;

    protected $fillable = ['modifier_group_id','modifier_category_id','modifier_name','price','size','is_selected','item_image'];
    protected $table='modifier_items';

    protected $dates = ['deleted_at'];

    
    public function modifiercategory()
    {
        return $this->belongsTo('App\Models\ModifierCategories', 'modifier_category_id');
    }

    public function itemModifierLanguage() {
        return $this->hasMany('App\Models\ItemModifierLanguage', 'modifier_item_id');
    }
   
}
