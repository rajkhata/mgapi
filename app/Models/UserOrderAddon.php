<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrderAddon extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='user_order_addons';
    protected $fillable = [
        'user_order_detail_id', 'user_order_id', 'menu_addons_id', 'menu_addons_option_id', 'addons_name', 'addons_option', 'price', 'quantity', 'selection_type', 'priority', 'was_free'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];


}
