<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ItemAddons extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'restaurant_id','addon_group_id','item_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function addongroup()
    {
        return $this->belongsTo('App\Models\ItemAddonGroup','addon_group_id');
    }
}
