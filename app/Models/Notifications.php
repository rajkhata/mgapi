<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Notifications extends Model{
    
    protected $table = 'pubnub_notification';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','notification_msg','type','read_status','restaurant_id','channel','status','pubnub_info','cronUpdate','language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}

####### http://qc.bravvurashowcase.com/phpmyadmin/ (hri_dev)
//CREATE TABLE `pubnub_notification` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `user_id` int(11) DEFAULT NULL,
//  `notification_msg` varchar(255) COLLATE utf8mb4_bin NOT NULL,
//  `type` tinyint(4) NOT NULL COMMENT '0=>others,1=>order,2=>group order,3=>reservation,4=>reviews,6=>accept_friendship,7=>tip,8=>upload_photo,9=>bookmark',
//  `read_status` int(11) DEFAULT '0' COMMENT '0=>unread,1=>read',
//  `restaurant_id` int(11) DEFAULT NULL,
//  `channel` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
//  `created_at` datetime DEFAULT NULL,
//  `updated_at` datetime DEFAULT NULL,
//  `status` tinyint(4) NOT NULL DEFAULT '0',
//  `pubnub_info` text COLLATE utf8mb4_bin,
//  `cronUpdate` tinyint(1) NOT NULL DEFAULT '0',
//  `language_id` int(11) NOT NULL DEFAULT '1',
//  PRIMARY KEY (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin AUTO_INCREMENT=1
