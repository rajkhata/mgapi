<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryRates  extends Model
{
    protected $table = 'delivery_rates';
    /**
    }
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','restaurant_id','country','state','city','zipcode_from','zipcode_to','weight_from','weight_to','price_from','price_to','quantity_from','quantity_to','item_code','rate_type','delivery_charge_type','delivery_charge_amount','delivery_name','algorithm'
    ];

}