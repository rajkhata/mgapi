<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierOptionGroup extends Model
{
	use SoftDeletes;

    protected $fillable = ['restaurant_id','group_name','language_id','is_required','quantity_type','quantity','modifier_item_id'];
    protected $table='modifier_item_option_groups';

    protected $dates = ['deleted_at'];

    
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function itemmodifiers()
    {
        return $this->hasMany('App\Models\ItemModifier','id');
    }

    public function itemModifierOptionGroupLanguage() {
        return $this->hasMany('App\Models\ItemModifierOptionGroupLanguage', 'modifier_option_group_id');
    }
   
}
