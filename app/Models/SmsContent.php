<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class SmsContent extends Model
{
    protected $table = 'sms_content';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sms_name', 'content', 'status', 'language_id','restaurant_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];
}
