<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatuses extends Model
{


    protected $table = 'order_statuses';


    protected $fillable = [
        'order_id', 'user_id', 'restaurant_id','comments','status'
    ];

    
}
