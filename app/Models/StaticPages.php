<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticPages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_id', 'parent_id', 'page_type', 'page_heading', 'page_sub_heading', 'page_content', 'priority', 'image', 'video','language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function parent()
    {
        return $this->belongsTo('StaticPages', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('StaticPages', 'parent_id');
    }
}
