<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsAuth extends Model
{
    protected $table = 'cms_auth';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cms_user_id', 'restaurant_id', 'device_id','token', 'expiry', 'created_at', 'update_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at'
    ];

}
