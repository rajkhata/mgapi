<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    protected $table = 'cms_restaurant_resv_floor_tables';
    protected $fillable = ['restaurant_id', 'floor_id', 'layout', 'name', 'configuration','min','max'];

    public function floor()
    {
        return $this->belongsTo('App\Models\Floor');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation', 'table_id', 'id');
    }

    /*public function tableType()
    {
        return $this->belongsTo('App\Models\TableType', 'type_id', 'id');
    }*/
}
?>