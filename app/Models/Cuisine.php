<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
    /*public $timestamps = false;             // disable all behaviour
    public $timestamps = true;              // enable all behaviour
    public $timestamps = [ "created_at" ];  // enable only to created_at
    public $timestamps = [ "updated_at" ];  // enable only to updated_at
    public $timestamps = [ "created_at", "updated_at" ]; // same that true*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cuisine', 'cuisine_type', 'description', 'image_name', 'search_status', 'status', 'priority','language_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
