<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewMenuItemsLanguage extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'new_menu_items_id', 'item_other_info', 'gift_message', 'name', 'description', 'sub_description', 'language_id',
    ];

    protected $table = 'new_menu_items_language';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function getMenuItems() {
        return $this->belongsTo('App\Models\NewMenuItems', 'new_menu_items_id');
    }

}
