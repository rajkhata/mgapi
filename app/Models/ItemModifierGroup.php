<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModifierGroup extends Model
{
	use SoftDeletes;

    //protected $fillable = ['restaurant_id','group_name','prompt','language_id','is_required','quantity_type','quantity','menu_item_id'];
    protected $fillable = ['restaurant_id','group_name','prompt','language_id','is_required','quantity_type','quantity','menu_item_id','pos_id', 'calorie','sort_order','show_as_dropdown'];

    protected $table='modifier_groups';

    protected $dates = ['deleted_at'];

    
    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function itemmodifiers()
    {
        return $this->hasMany('App\Models\ItemModifier','modifier_group_id');
    }

    public function modifierGroupLangauges()
    {
        return $this->hasMany('Modules\MenuManagement\Entities\ItemModifierGroupLanguage','modifier_group_id');
    }

   
}
