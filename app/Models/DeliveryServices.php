<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryServices extends Model
{
    protected $table = 'delivery_services';
    /**
}
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','provider_name','short_name','api_key','producer_key','order_creation_url','order_ready_url','order_cancel_url','msg_sent_url','status'
    ];

} 