<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Floor extends Model
{   
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'cms_restaurant_resv_floors';
    protected $fillable = ['restaurant_id', 'layout', 'name', 'type', 'completed_step', 'layout_config', 'duplicate_from_id'];

    /**
     * Get the tables for the floor.
     */
    public function tables()
    {
        return $this->hasMany('App\Models\Table');
    }

    public function get_floor_menu()
    {
        return $this->where('restaurant_id', \Auth::user()
            ->restaurant_id)->where('completed_step', 2)->has('tables')->get(['id', 'name']);
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
}
?>