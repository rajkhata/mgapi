<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionCoupon extends Model
{
    protected $table = 'promotion_coupon';
    /**
    }
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','restaurant_id','coupon_code','discount_type','discount','minimum_amount','uses_per_customer','uses_per_coupon','start_date','end_date','status','created_at' ];

}