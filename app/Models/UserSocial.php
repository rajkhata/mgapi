<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserSocial extends Model
{
    protected $table = 'user_social';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social', 'user_id', 'access_token', 'user_source','language_id','parent_restaurant_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
