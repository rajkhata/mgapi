<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSubCategories extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','menu_category_id','restaurant_id', 'name', 'status', 'priority','language_id','is_popular','is_favourite','promotional_banner_url','promotional_banner_image','promotional_banner_image_mobile'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}