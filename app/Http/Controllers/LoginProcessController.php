<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CmsUser;
use App\Helpers\MongoClient;

class LoginProcessController extends Controller{
    public function create()
    {
        \Log::info('OPA Mongo DB called');
        $ms = microtime(true);
       
        $accountDetails = CmsUser::select("restaurant_id","city_id","name","email","password")->where("status","1")->get();
        $restAccount = [];
        foreach($accountDetails as $key =>$val){
            $restAccount[] = array("restaurant_id"=>$val->restaurant_id,"city_id"=>$val->city_id,"user_name"=>$val->name,"email"=>$val->email,'user_password'=>$val->password);
        }
        
        $mongoHost = env('MONGO_HOST');
        $mongoDb = env('MONGO_DB');
        $mongoEnable = env('MONGO_ENABLE');
        $mongoUser = env('MONGO_USER');
        $mongoPwd = env('MONGO_PWD');
        $error = '';
        
        if(!$restAccount) {
            $mongoClient = new MongoClient();
            $mongoClient->type = "pre";
            $mongoClient->insert($restAccount);
            $me = microtime(true) - $ms;
            return response()->json(['data' => $restAccount, 'error' => $error, 'xtime' => $me]);
        }else{
            $me = microtime(true) - $ms;
            return response()->json(['data'=>"",'error'=>"Record not inserted",'xtime'=>$me]);
        }

    }
}