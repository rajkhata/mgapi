<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\UserAuth;
use App\Helpers\CommonFunctions;
use App\Helpers\CanRestaurantDeliver;
use App\Helpers\LocationWorkingDay;
use App\Helpers\LocationTimeSlot;
use App\Helpers\Twinjet;
use DB;

class CanDeliverController extends Controller {

    /**
     * Function to return can restaurant deliver on particular address 
     * @param restauraid : $id
     * @param lat : lat
     * @param long : long
     * @return \Illuminate\Http\JsonResponse
     * vertex,inside,outside,boundary
     */
    private function objectToArray($object) {
        if (is_object($object)) {
            $object = get_object_vars($object);
        }
        if (is_array($object)) {
            return array_map(array($this, 'objectToArray'), $object);
        } else {
            return $object;
        }
    }
    public function get(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
        $latlong = $request->lng . " " . $request->lat;
        $points = [$latlong];
     
	$branchSelectFields = ['delivery_charge','minimum_delivery','phone','delivery_area','price','reservation','dinning','takeout','delivery','restaurants.id','restaurant_name','lat','lng',
		'address','landmark','street', 'zipcode','rest_code','delivery_geo',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name',
		'states.state', 'countries.country_name','countries.country_short_name','address2','is_delivery_geo','delivery_zipcode','parent_restaurant_id'];


            $restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1,'delivery' => 1])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get();	
        $result = [];
        if (count($restaurantData)) {
            $restaurantData = $restaurantData->toArray();
    	    if($restaurantData[0]['is_delivery_geo'] == 0 && $request->has('zipcode')){
    		  $result = $this->chkForDeliveryAvailableZipcode($restaurantData, $request->zipcode);
    	    }else{	
            	$result = $this->chkForDeliveryAvailable($restaurantData, $points);
    	    }
            
            $error = (empty($result)) ? "Sorry, we are not delivering in your area at this time." : "";
            

        } else {
            
            $error = "";
            $me = microtime(true) - $ms;
            return response()->json(['data' =>$result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    private function  isZipCodeMatch($zipCodeArray,$zipmatch){
        $status=0;
        $invalid=0;
        $zipmatch=trim($zipmatch);
        $invalid_match=0;
        foreach($zipCodeArray as $zip) {
            $zip=trim($zip);
            if (strpos($zipmatch, $zip) !== false || $zipmatch==$zip) {
                $status=1;
            }
            if (strpos($zip, $zipmatch) !== false) {
                $invalid_match=1;
            }
        }
        $zipexplode=explode(" ",$zipmatch);
        if($invalid_match && count($zipexplode)<2){
            $invalid=1;
        }
        if($status==0 && $invalid){
            return response()->json(['data' => null, 'error' => 'Please enter a full, valid postcode (Ex. EH10 5LY)'], Config('constants.status_code.STATUS_SUCCESS'),['Access-Control-Allow-Origin' => '*'])->send();die;

        }
        return $status;

    }

    private function chkForDeliveryAvailableZipcode($restaurantData, $zipcode) {
	 
		$result=null;
		foreach($restaurantData as $rest){
		//$zipCodeArray = explode(",",$rest['delivery_zipcode']);
		$zipCodeArray = explode(",",rtrim($rest['delivery_zipcode'], ','));
         if(!empty($rest['delivery_zipcode']) && $this->isZipCodeMatch($zipCodeArray,$zipcode)){
	  //if(!empty($rest['delivery_zipcode']) ){ //&& $this->isZipCodeMatch($zipCodeArray,$zipcode) commented by me
		//foreach($zipCodeArray as $zip){
		 //if (strlen(strstr(trim($zip),$zipcode))>0) {	
		 //if(trim($zip) == $zipcode){
			$result['has_delivery'] = 1;
			$canDeliver = true;
			$key = 0;
			if ($canDeliver) {
                        $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $rest['id']));
                        $get_restaurant_setting = CommonFunctions::getRestaurantSetting($rest['id']);
                        if(is_object($currentDate)) {
                            $restcurrentDateTime =  $currentDate->format('Y-m-d H:i:00');
                            $currentDate =  $currentDate->format('Y-m-d');
                        }else {
                            $restcurrentDateTime = date('Y-m-d H:i:00');
                            $currentDate = date('Y-m-d');
                        }
                        $result[$key]['id'] = $rest['id'];
                        $result[$key]['parent_restaurant_id'] = $rest['parent_restaurant_id'];
                        $result[$key]['restaurant_name'] = $rest['restaurant_name'];
                        $result[$key]['delivery_geo'] = $rest['delivery_geo'];
                        $result[$key]['lat'] = $rest['lat'];
                        $result[$key]['lng'] = $rest['lng'];

                        $result[$key]['city_id'] = isset( $rest['city_id'])? $rest['city_id']:""; 
                        $result[$key]['address'] = $rest['address'];
                        //$result[$key]['zipcode'] = $rest['zipcode'];
                        $result[$key]['address_2'] = isset( $rest['address2'])? $rest['address2']:"";
                        //$result[$key]['landmark']= $value['landmark'];
                        $result[$key]['street'] = $rest['street'];
                        $result[$key]['city_name'] = isset($rest['city_name'])?$rest['city_name']:"";
                        $result[$key]['country_code'] = isset($rest['country_short_name'])?$rest['country_short_name']:"";
                        $result[$key]['state_name'] = isset($rest['state'])?$rest['state']:"";
                        $result[$key]['country_name'] = isset($rest['country_name'])?$rest['country_name']:"";
                        $result[$key]['zipcode'] = $rest['zipcode'];
                        $result[$key]['rest_code'] = $rest['rest_code'];
                        $result[$key]['has_delivery'] = $rest['delivery'];
                        $result[$key]['has_takeout'] = $rest['takeout'];
                        $result[$key]['has_dining'] = $rest['dinning'];
                        $result[$key]['has_reservation'] = $rest['reservation'];
                        $result[$key]['price'] = $rest['price'];
                        $result[$key]['delivery_area'] = $rest['delivery_area'];
                        $result[$key]['minimum_delivery'] = $rest['minimum_delivery'];
                        $result[$key]['delivery_charge'] = $rest['delivery_charge'];
                        $result[$key]['phone_no'] = $rest['phone'];
                        $result[$key]['canDeliver'] = $canDeliver;
                        $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($rest['id']);
                        $result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($rest['id']);
                        $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($rest['id']);
                        #$result[$key]['is_currently_open_delivery'] = LocationTimeSlot::isRestaurantCurrentlyOpenForDelivery($value['id'],$restcurrentDateTime);
                        #commented by RG as we are not using this as we are already showing the times as Discussed with Prakash @ 20-09-2018
                        $result[$key]['is_currently_open_delivery'] = true;
                        #$result[$key]['is_currently_open_carryout'] = LocationTimeSlot::isRestaurantCurrentlyOpenForCarryout($value['id'],$restcurrentDateTime);
                        $result[$key]['is_currently_open_carryout'] = true;
                        $result[$key]['rest_current_datetime'] = $restcurrentDateTime;
                        $result[$key]['restaurant_services'] = $get_restaurant_setting['restaurant_services'];
                        return $result;
                    }
		 //}
		} 
		}	
		
	 
        return $result;
    }
    private function chkForDeliveryAvailable($restaurantData, $points) {
	

        $canRestaurantDeliverObj = new CanRestaurantDeliver();
        $result = [];
        foreach ($restaurantData as $key => $value) {
            foreach ($points as $key => $point) {
                $polygon = CommonFunctions::formatDeliveryGeo($value['delivery_geo']);
                if ($polygon) {
                    $canDeliver = $canRestaurantDeliverObj->isInPolygon($point, $polygon);

                    if ($canDeliver) {
                        $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['id']));
                        $get_restaurant_setting = CommonFunctions::getRestaurantSetting($value['id']);
                        if(is_object($currentDate)) {
                            $restcurrentDateTime =  $currentDate->format('Y-m-d H:i:00');
                            $currentDate =  $currentDate->format('Y-m-d');
                        }else {
                            $restcurrentDateTime = date('Y-m-d H:i:00');
                            $currentDate = date('Y-m-d');
                        }
                        $result[$key]['id'] = $value['id'];
                        $result[$key]['parent_restaurant_id'] = $value['parent_restaurant_id'];
                        $result[$key]['restaurant_name'] = $value['restaurant_name'];
                        $result[$key]['delivery_geo'] = $value['delivery_geo'];
                        $result[$key]['lat'] = $value['lat'];
                        $result[$key]['lng'] = $value['lng'];

                        $result[$key]['city_id'] = isset( $rest['city_id'])? $rest['city_id']:""; 
                        $result[$key]['address'] = $value['address'];
                        //$result[$key]['zipcode'] = $value['zipcode'];
                        $result[$key]['address_2'] = isset( $value['address2'])? $value['address2']:"";
//$result[$key]['landmark']= $value['landmark'];
                        $result[$key]['street'] = $value['street'];
                        $result[$key]['city_name'] = isset($value['city_name'])?$value['city_name']:"";
                        $result[$key]['country_code'] = isset($value['country_short_name'])?$value['country_short_name']:"";
                        $result[$key]['state_name'] = isset($value['state'])?$value['state']:"";
                        $result[$key]['country_name'] = isset($value['country_name'])?$value['country_name']:"";
                        $result[$key]['zipcode'] = $value['zipcode'];
                        $result[$key]['rest_code'] = $value['rest_code'];
                        $result[$key]['has_delivery'] = $value['delivery'];
                        $result[$key]['has_takeout'] = $value['takeout'];
                        $result[$key]['has_dining'] = $value['dinning'];
                        $result[$key]['has_reservation'] = $value['reservation'];
                        $result[$key]['price'] = $value['price'];
                        $result[$key]['delivery_area'] = $value['delivery_area'];
                        $result[$key]['minimum_delivery'] = $value['minimum_delivery'];
                        $result[$key]['delivery_charge'] = $value['delivery_charge'];
                        $result[$key]['phone_no'] = $value['phone'];
                        $result[$key]['canDeliver'] = $canDeliver;
                        $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($value['id']);
                        $result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($value['id']);
                        $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($value['id']);
                        #$result[$key]['is_currently_open_delivery'] = LocationTimeSlot::isRestaurantCurrentlyOpenForDelivery($value['id'],$restcurrentDateTime);
                        #commented by RG as we are not using this as we are already showing the times as Discussed with Prakash @ 20-09-2018
                        $result[$key]['is_currently_open_delivery'] = true;
                        #$result[$key]['is_currently_open_carryout'] = LocationTimeSlot::isRestaurantCurrentlyOpenForCarryout($value['id'],$restcurrentDateTime);
                        $result[$key]['is_currently_open_carryout'] = true;
                        $result[$key]['rest_current_datetime'] = $restcurrentDateTime;
                        $result[$key]['restaurant_services'] = $get_restaurant_setting['restaurant_services'];
                        return $result;
                    }
                }
            }
        }
        return $result;
    }
    //will check if restaurant has 3rd party delivery system
    /*
	sql -
	1) ALTER TABLE `restaurants` CHANGE `is_delivery_geo` `is_delivery_geo` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1 for Geo, 0 for Zipcode,2 3rd Party Delivery';
    */
    public function checkdeliveryavailable(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
	$location_id = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $langId = $language_id->id;
        $latlong = $request->lng . " " . $request->lat;
        $points = [$latlong];
        $result = [];
        $response="";
        $delivery=2;
        
        if($this->hasLocationDelivery($location_id,$ms,$response)){ return $response;}        
        //$restaurantData = Restaurant::select('id', 'restaurant_name')->where(['language_id' => $langId ,'status' => 1,'parent_restaurant_id'=>$id])->get()->toArray()[0];
		$branchSelectFields = ['delivery_charge','minimum_delivery','phone','delivery_area','price','reservation','dinning','takeout','delivery','restaurants.id','restaurant_name',
		'lat','lng','address','landmark', 'street', 'zipcode','rest_code','delivery_geo',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','is_delivery_geo','delivery_zipcode','parent_restaurant_id'];
		$restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1,'delivery' => 1])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get();
      /////// $restaurantData = restaurant::where(['language_id' => $langId, 'parent_restaurant_id' => $id, 'status' => 1, 'delivery' => 1])->get();
	//$restaurantData = restaurant::where(['language_id' => $langId, 'id' => $location_id, 'status' => 1, 'delivery' => 1])->get();
        
        if (count($restaurantData)) {
         $delivery=1;
         $restaurantData = $restaurantData->toArray();
         $is_zipcode_check=0;
	       if($restaurantData[0]['is_delivery_geo'] == 0){
 		       // if($request->has('zipcode')){
 		 if($request->filled('zipcode')){
                    // $restaurantData = restaurant::where(['language_id' => $langId, 'parent_restaurant_id' => $restaurantId, 'status' => 1, 'delivery' => 1])->get()->toArray();
                    //to match other locations zip code

                    $result = $this->chkForDeliveryAvailableZipcode($restaurantData, $request->zipcode);
                }else{

                    $is_zipcode_check=1;
                }

    	    }elseif($restaurantData[0]['is_delivery_geo'] == 2){
		   if($request->has('zipcode') && isset($request->zipcode) && isset($request->state) && isset($request->city) && isset($request->street_address))
    		  $result = $this->chkForDeliveryAvailableThirdParty($restaurantData, $request);
    	    }else{
		if(isset($request->lng) && isset($request->lat))
            	$result = $this->chkForDeliveryAvailable($restaurantData, $points);
 
    	    }
            if($is_zipcode_check==1){
                $error ='Hey, we need your full address with Post Code. (Ex 9 Morningside Drive, Edinburg, EH10 5LY)';

            }else{
                $error = (empty($result)) ? "Sorry, we are not delivering in your area at this time." : "";
            }

        } else {
            //@08-10-2018 by RG Jira Bug ID: PE-1879
            //$error = "Restaurant not found.";
            $error = "";
            $me = microtime(true) - $ms;
            return response()->json(['data' => $result, 'error' => $error,'delivery'=>$delivery, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error,'delivery'=>$delivery, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    private function chkForDeliveryAvailableThirdParty($restaurantData, $request) {


        $canRestaurantDeliverObj = new CanRestaurantDeliver();
        $result = [];
	$deliverServiceProvider = null;
	$canDeliver =  false;

        foreach ($restaurantData as $key => $rest) {
	   // $deliveryrestaurant = DB::table('delivery_restaurant')->where('restaurant_id', $value['id'])->get()->toArray();
	     //print_r($deliveryrestaurant);
	    $deliveryprovider = DB::table('delivery_services')->where('id', $rest['delivery_provider_id'])->get()->toArray();
	    $city = DB::table('cities')->where('id', $rest['city_id'])->get()->toArray();
 
	    $city = $city[0];
	    //$deliveryprovider1 = $deliveryprovider->toArray();
	    $deliveryprovider = $deliveryprovider[0];
 
	    if($deliveryprovider->provider_name=="twinjet" && isset($rest['delivery_provider_apikey'])){
		$pickupData = ["street_address" => $rest['address'],
                        "city" => $city->city_name,
                        "state" =>  $city->state_code,//statecode
                        "zip_code" =>  $rest['zipcode']];
		$userData = ["street_address" => $request->street_address,
                        "city" => $request->city,
                        "state" =>  $request->state,//statecode
                        "zip_code" => $request->zipcode];
	    	$deliverServiceProvider = new Twinjet(['apikey'=>$rest['delivery_provider_apikey']]);
		if($canDeliver1 = $deliverServiceProvider->validateDeliveryAddress($userData,$pickupData)){

			if(!isset($canDeliver1->non_field_errors) && !isset($canDeliver1->errors) && isset($canDeliver1->delivery_eta) && isset($canDeliver1->pickup_eta)){
				$result['has_delivery'] = 1;
				$canDeliver = true;
				$key = 0;
				if ($canDeliver) {
		                $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $rest['id']));
		                if(is_object($currentDate)) {
		                    $restcurrentDateTime =  $currentDate->format('Y-m-d H:i:00');
		                    $currentDate =  $currentDate->format('Y-m-d');
		                }else {
		                    $restcurrentDateTime = date('Y-m-d H:i:00');
		                    $currentDate = date('Y-m-d');
		                }
		                $result[$key]['id'] = $rest['id'];
		                $result[$key]['parent_restaurant_id'] = $rest['parent_restaurant_id'];
		                $result[$key]['restaurant_name'] = $rest['restaurant_name'];
		                $result[$key]['delivery_geo'] = $rest['delivery_geo'];
		                $result[$key]['lat'] = $rest['lat'];
		                $result[$key]['lng'] = $rest['lng'];

		                $result[$key]['address'] = $rest['address'];
				    $result[$key]['address_2'] = isset( $rest['address2'])? $rest['address2']:"";
				    //$result[$key]['landmark']= $value['landmark'];
				    $result[$key]['street'] = $rest['street'];
				    $result[$key]['city_name'] = isset($rest['city_name'])?$rest['city_name']:"";
				     $result[$key]['country_code'] = isset($rest['country_short_name'])?$rest['country_short_name']:"";
				    $result[$key]['state_name'] = isset($rest['state'])?$rest['state']:"";
				    $result[$key]['country_name'] = isset($rest['country_name'])?$rest['country_name']:"";
				    $result[$key]['zipcode'] = $rest['zipcode'];
		                $result[$key]['rest_code'] = $rest['rest_code'];
		                $result[$key]['has_delivery'] = $rest['delivery'];
		                $result[$key]['has_takeout'] = $rest['takeout'];
		                $result[$key]['has_dining'] = $rest['dinning'];
		                $result[$key]['has_reservation'] = $rest['reservation'];
		                $result[$key]['price'] = $rest['price'];
		                $result[$key]['delivery_area'] = $rest['delivery_area'];
		                $result[$key]['minimum_delivery'] = $rest['minimum_delivery'];
		                $result[$key]['delivery_charge'] = $rest['delivery_charge'];
		                $result[$key]['phone_no'] = $rest['phone'];
		                $result[$key]['canDeliver'] = $canDeliver;
		                $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($rest['id']);
		                $result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($rest['id']);
		                $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($rest['id']);
		                #$result[$key]['is_currently_open_delivery'] = LocationTimeSlot::isRestaurantCurrentlyOpenForDelivery($value['id'],$restcurrentDateTime);
		                #commented by RG as we are not using this as we are already showing the times as Discussed with Prakash @ 20-09-2018
		                $result[$key]['is_currently_open_delivery'] = true;
		                #$result[$key]['is_currently_open_carryout'] = LocationTimeSlot::isRestaurantCurrentlyOpenForCarryout($value['id'],$restcurrentDateTime);
		                $result[$key]['is_currently_open_carryout'] = true;
		                $result[$key]['rest_current_datetime'] = $restcurrentDateTime;
				$result[$key]['twinjet'] = $canDeliver1;
		                return $result;
		            }
			 }
		}
	    }



        }
        return $result;
    }
    
    private function hasLocationDelivery($locationId,$ms,&$response){
        $has_location_delivery = Restaurant::select(['delivery','restaurant_name'])->where(['id' => $locationId])->get();

            if(count($has_location_delivery)){
                $has_delivery = $has_location_delivery->toArray();
                $me = microtime(true) - $ms;
                if($has_delivery[0]['delivery'] == 0){
                    $response = response()->json(
                            [
                                'data' => [], 
                                'error' => ucfirst($has_delivery[0]['restaurant_name'])." : Delivery order for this location has been temporarily pause. Please try the delivery order with other locations",
                                'delivery'=>0, 
                                'xtime' => $me
                            ], 
                                Config('constants.status_code.STATUS_SUCCESS')
                            );
                    return true;
                }else{
                    return false;
                }
            }
    }
    public function getNearestRestaurant(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
	$localization_id = $request->header('X-localization');
    $location_id = $request->header('X-location');
        
        $branchSelectFields = ['restaurants.id','restaurant_name','lat','lng','address','landmark','street', 'zipcode','rest_code',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','common_message','common_message_status','common_message_url','delivery'];


            /*$restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1 ])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get(); */
            $restaurantData = Restaurant::select($branchSelectFields)->where(['restaurants.id' => $location_id, 'restaurants.status' => 1 ])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get(); 
	 
        if($request->has('lat') && $request->has('lng')){
            CommonFunctions::$lat1 = $request->lat;
            CommonFunctions::$lon1 = $request->lng;  
            CommonFunctions::$unit="M";
        }else{
            $result=  $this->getRestaurantDetails($restaurantData);
            $me = microtime(true) - $ms; 
            if(isset($result['error'])){
                return response()->json(['data' => [], 'error' => $result['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }elseif(empty($result)){
                 //@08-10-2018 by RG Jira Bug ID: PE-1879
                return response()->json(['data' => $result, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
            return response()->json(['data' => $result, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
         $result = [];
        //if ($restaurantData) {
            $result = $this->chkNearestDistance($request,$restaurantData);
            if(LocationWorkingDay::$timezoneError){
                $me = microtime(true) - $ms;                
                return response()->json(['data' => [], 'error' => LocationWorkingDay::$timezoneError['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }          
            /*if(empty($result)) {
                $error ="Restaurant not deliver";
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }*/
        /*} else {
             
            $error = "";
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }*/
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    private function chkNearestDistanceOld(Request $request,$restaurantData) {
	
        $result = null;
        foreach ($restaurantData as $key => $rest) {
            
            CommonFunctions::$lat2 = $rest['lat'];
            CommonFunctions::$lon2 = $rest['lng'];
            $result[$key]['id'] = $rest['id'];
             
            $distance = CommonFunctions::distanceBetweenTwoPointOnEarth();
	    //if($distance<1.6){
		    $result[$key]['sortby_distance'] = $distance;
		    $result[$key]['distance'] = $distance." M";  
		    $result[$key]['lat'] = $rest['lat'];
		    $result[$key]['lng'] = $rest['lng'];
		   // $result['has_delivery'] = 1;
		    //$canDeliver = true;
		    $result[$key]['id'] = $rest['id'];
		    $result[$key]['orderCount'] = $this->countOrderInPreparingState($rest['id']);	
		    $result[$key]['parent_restaurant_id'] = $rest['parent_restaurant_id'];
		    $result[$key]['restaurant_name'] = $rest['restaurant_name'];
		    $result[$key]['delivery_geo'] = $rest['delivery_geo'];
		    $result[$key]['lat'] = $rest['lat'];
		    $result[$key]['lng'] = $rest['lng'];
		    $result[$key]['city_id'] = isset( $rest['city_id'])? $rest['city_id']:""; 
		    $result[$key]['address'] = $rest['address'];
		    //$result[$key]['zipcode'] = $rest['zipcode'];
		    $result[$key]['address_2'] = isset( $rest['address2'])? $rest['address2']:"";
		    //$result[$key]['landmark']= $value['landmark'];
		    $result[$key]['street'] = $rest['street'];
		    $result[$key]['city_name'] = isset($rest['city_name'])?$rest['city_name']:"";
		    $result[$key]['country_code'] = isset($rest['country_short_name'])?$rest['country_short_name']:"";
		    $result[$key]['state_name'] = isset($rest['state'])?$rest['state']:"";
		    $result[$key]['country_name'] = isset($rest['country_name'])?$rest['country_name']:"";
		    $result[$key]['zipcode'] = $rest['zipcode'];
		    $result[$key]['rest_code'] = $rest['rest_code'];
		    $result[$key]['has_delivery'] = $rest['delivery'];
		    $result[$key]['has_takeout'] = $rest['takeout'];
		    $result[$key]['has_dining'] = $rest['dinning'];
		    $result[$key]['has_reservation'] = $rest['reservation'];
		    $result[$key]['price'] = $rest['price'];
		    $result[$key]['delivery_area'] = $rest['delivery_area'];
		    $result[$key]['minimum_delivery'] = $rest['minimum_delivery'];
		    $result[$key]['delivery_charge'] = $rest['delivery_charge'];
		    $result[$key]['phone_no'] = $rest['phone'];
		    $result[$key]['canDeliver'] = true;
		    $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($rest['id']);
		    //$result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($rest['id']);
		    $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($rest['id']);
		    $result[$key]['is_currently_open_delivery'] = true;
	    //}else{
		//unset($result[$key]);
	    //}
             
             
        }
        if($result!=null){ 
		  usort($result, function ($item1, $item2) {
                 return $item1['sortby_distance'] <=> $item2['sortby_distance'];
                });
        } 
	 
	    $return_result = [];
	    // only one location will be returned
	    $bearerToken = $request->bearerToken();
        CommonFunctions::$langId = $langId = config('app.language')['id'];
        if($bearerToken) { // for logged in user
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
	        $userId       = $userAuth->user_id;
            $userInfo      = User::find($userId);
           //use App\Models\UserOrder;	
           ///'referral_code', 'referral_restaurant_id'
           //$i=0;
           if(isset($userInfo['referral_code']) && isset($userInfo['referral_code'])){
            foreach($result as $rest){
                if($rest['id']==(int)$userInfo['referral_restaurant_id']){
                    $return_result = $rest;
                }elseif($rest['sortby_distance']<=1.5){
                    $return_result = $result[0];
                } 
            }

           }else{
            foreach($result as $rest){
                //if($rest['sortby_distance']<=1.5){
                    $return_result = $result[0];
                //} 
                }
           } 
	   
	   
        }else {  // guest user return nearest branch and can deliver
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            foreach($result as $rest){
              if($rest['sortby_distance']<=1.5){
                    $return_result = $result[0];
              } 
            }
        }
        $userAuth1 = UserAuth::find($userAuth->id);
        $userAuth1->restaurant_distance=isset($return_result['sortby_distance'])?$return_result['sortby_distance']:'';        
        $userAuth1->save();
        return [$return_result];
        
    }
    private function countOrderInPreparingState($restaurant_id){
	   $order = DB::table('user_orders')->where('restaurant_id', $restaurant_id)->where('order_state',1)->where('status', 'preparing')->get()->toArray();
	   return count($order); 
    } 	
    private function chkNearestDistance(Request $request,$restaurantData) {
	
        $result = null;
        if(count($restaurantData)>0){
        foreach ($restaurantData as $key => $rest) {
            
            CommonFunctions::$lat2 = $rest['lat'];
            CommonFunctions::$lon2 = $rest['lng'];
            $result[$key]['id'] = $rest['id'];
             
            $distance = CommonFunctions::distanceBetweenTwoPointOnEarth();
            if($distance<=2.5){
                $result[$key]['sortby_distance'] = $distance;
                $result[$key]['distance'] = $distance." M";  
                $result[$key]['lat'] = $rest['lat'];
                $result[$key]['lng'] = $rest['lng'];
               // $result['has_delivery'] = 1;
                //$canDeliver = true;
                $result[$key]['id'] = $rest['id'];
                $result[$key]['orderCount'] = $this->countOrderInPreparingState($rest['id']);	
                $result[$key]['parent_restaurant_id'] = $rest['parent_restaurant_id'];
                $result[$key]['restaurant_name'] = $rest['restaurant_name'];
                $result[$key]['delivery_geo'] = $rest['delivery_geo'];
                $result[$key]['is_delivery_on'] = $rest['delivery'];
                $result[$key]['lat'] = $rest['lat'];
                $result[$key]['lng'] = $rest['lng'];
                $result[$key]['city_id'] = isset( $rest['city_id'])? $rest['city_id']:""; 
                $result[$key]['address'] = $rest['address'];
                //$result[$key]['zipcode'] = $rest['zipcode'];
                $result[$key]['address_2'] = isset( $rest['address2'])? $rest['address2']:"";
                //$result[$key]['landmark']= $value['landmark'];
                $result[$key]['street'] = $rest['street'];
                $result[$key]['city_name'] = isset($rest['city_name'])?$rest['city_name']:"";
                $result[$key]['country_code'] = isset($rest['country_short_name'])?$rest['country_short_name']:"";
                $result[$key]['state_name'] = isset($rest['state'])?$rest['state']:"";
                $result[$key]['country_name'] = isset($rest['country_name'])?$rest['country_name']:"";
                $result[$key]['zipcode'] = $rest['zipcode'];
                $result[$key]['rest_code'] = $rest['rest_code'];
                $result[$key]['has_delivery'] = $rest['delivery'];
                $result[$key]['has_takeout'] = $rest['takeout'];
                $result[$key]['has_dining'] = $rest['dinning'];
                $result[$key]['has_reservation'] = $rest['reservation'];
                $result[$key]['price'] = $rest['price'];
                $result[$key]['delivery_area'] = $rest['delivery_area'];
                $result[$key]['minimum_delivery'] = $rest['minimum_delivery'];
                $result[$key]['delivery_charge'] = $rest['delivery_charge'];
                $result[$key]['phone_no'] = $rest['phone'];
                $result[$key]['canDeliver'] = true;
                $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($rest['id']);
                //$result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($rest['id']);
                $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($rest['id']);
                $result[$key]['is_currently_open_delivery'] = true;
            }else{
            unset($result[$key]);
            }
             
             
        }}
        if($result!=null){ 
		 usort($result, function ($item1, $item2) {
                 return $item1['sortby_distance'] <=> $item2['sortby_distance'];
                });
        } 
        $messageId=0;

        $return_result['is_form'] = 0;
        $return_result['data'] = [];
        $restaurantFound = 0;
        $restaurant_distance = 0;
        // only one location will be returned
        $bearerToken = $request->bearerToken();
            CommonFunctions::$langId = $langId = config('app.language')['id'];
        // for logged in user
            if($bearerToken) { 
                $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
                $userId       = $userAuth->user_id;
                $userInfo      = User::find($userId);
		$UserOrder = DB::table('user_orders')->where('user_id', $userId)->get()->toArray();
                   if(isset($userInfo['referral_code']) && isset($userInfo['referral_code']) && $result!=null && count($UserOrder)<1){   
                        foreach($result as $rest){
                           if(($rest['id']==(int)$userInfo['referral_restaurant_id']) && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1 && $restaurantFound==0){ 
                                // check 1.5 mile logic and delivery on logic and referral code
                                $return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $return_result['is_form'] = 0;
				$return_result['type'] = "referral code restaurant -deliver on";    
                                $restaurant_distance = $rest['sortby_distance'];   
                            } 
                        }
			foreach($result as $rest){
                           if(($rest['id']!=(int)$userInfo['referral_restaurant_id']) && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1 && $restaurantFound==0){ 
                                // check 1.5 mile logic and delivery on logic and referral code
                                $return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $return_result['is_form'] = 0;
				$return_result['type'] = "referral code but delivery off- pass order to other nearest";    
                                $restaurant_distance = $rest['sortby_distance'];   
                            } 
                        }
                        // check 1.5 mile logic and delivery on logic  but maxed out bars

                          
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $messageId = '5';
                                $return_result['type'] = "refferal -- max out order 1.5 mile";   
                                //$restaurant_distance = $rest['sortby_distance'];
                          } 
                        }  
                        // check 1.5 mile logic and  delivery inactive

                          
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $messageId = '4';
                                $return_result['type'] = "refferal -- is delivery=0  1.5 mile"; 
                          } 
                        }   
                        // check 2.5 mile logic and delivery on logic 
                          
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1){
                                $return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $return_result['is_form'] = 0;
                                $restaurant_distance = $rest['sortby_distance'];
				$return_result['type'] = "refferal -- 2.5 mile logic and delivery on -- logic"; 
                          } 
                        } 
                        // check 2.5 mile logic but maxed out bars

                           
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                //$return_result['is_form'] = 0;
                                $messageId = '5';
                                $return_result['type'] = "refferal -- 2.5 mile logic but maxed out bars"; 
                          } 
                        }  
                        // check 2.5 mile logic but logic and delivery off logic 

                            
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                //$return_result['is_form'] = 0;
                                $messageId = '4';
                                $return_result['type'] = "refferal -- 2.5 mile logic but is delivery off"; 
                          } 
                        } 	

                   }else{
                     /// IF LOGGED IN USER AND NOT SUBMIT REFFERAL CODE
                     if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1){
                                $return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $return_result['is_form'] = 0;
				$return_result['type'] = "1.5 mile logic short route distance [LOGGED IN USER without reffral user]"; 
                                $restaurant_distance = $rest['sortby_distance'];
                          } 
                        }} 
                        // check 1.5 mile logic and delivery on logic  but maxed out bars

                        if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $messageId = '5';
                                $return_result['type'] = "1.5 mile logic max out order [LOGGED IN USER without reffral user]"; 
                          } 
                        }} 
                        // check 1.5 mile logic and  delivery inactive

                        if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $messageId = '4';
                                $return_result['type'] = "1.5 mile logic DELIVERY OFF [LOGGED IN USER without reffral user]"; 
                          } 
                        }} 
                        // check 2.5 mile logic and delivery on logic 
                        //if($restaurantFound==0){ 
                        if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1){
                                $return_result['data'] = $rest;
                                $restaurantFound= 1;
                                $return_result['is_form'] = 0;
                                $restaurant_distance = $rest['sortby_distance'];
				$return_result['type'] = "2.5 mile logic short distance [LOGGED IN USER without reffral user]";
                          } 
                        }} 
                        // check 2.5 mile logic but maxed out bars

                        if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                //$return_result['is_form'] = 0;
                                $messageId = '5';
                                $return_result['type'] = "2.5 mile logic max out order [LOGGED IN USER without reffral user]";
                          } 
                        }} 
                        // check 2.5 mile logic but logic and delivery off logic 

                        if($result!=null){    
                        foreach($result as $rest){
                          if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                                //$return_result['data'] = $rest;
                                $restaurantFound= 1;
                                //$return_result['is_form'] = 0;
                                $messageId = '4';
                               $return_result['type'] = "2.5 mile logic delivery off [LOGGED IN USER without reffral user]";
                          } 
                        }} 	
			
		          }

            }else {  
            // guest user return nearest branch and can deliver
                $token = explode(' ', $request->header('Authorization'))[1];
                $userAuth = UserAuth::where(['guest_token' => $token])->first();
                // check 1.5 mile logic and delivery on logic 
                 
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1){
                        $return_result['data'] = $rest;
                        $restaurantFound= 1;
                        $return_result['is_form'] = 0;
			 $return_result['type'] = "1.5 mile logic short distance [Not LOGGED IN USER without reffral user]";
                        $restaurant_distance = $rest['sortby_distance'];
                  } 
                }} 
                // check 1.5 mile logic and delivery on logic  but maxed out bars
                
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                        //$return_result['data'] = $rest;
                        $restaurantFound= 1;
                        $messageId = '5';
                        $return_result['type'] = "1.5 mile logic max out order [Not LOGGED IN USER without reffral user]";
                  } 
                }} 
                // check 1.5 mile logic and  delivery inactive
                 
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=1.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                        //$return_result['data'] = $rest;
                        $restaurantFound= 1;
                        $messageId = '4';
                        $return_result['type'] = "1.5 mile logic delivery off [Not LOGGED IN USER without reffral user]";
                  } 
                }} 
                // check 2.5 mile logic and delivery on logic 
                //if($restaurantFound==0){ 
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==1){
                        $return_result['data'] = $rest;
                        $restaurantFound= 1;
                        $return_result['is_form'] = 0;
                        $restaurant_distance = $rest['sortby_distance'];
			$return_result['type'] = "2.5 mile logic short distance [Not LOGGED IN USER without reffral user]";
                  } 
                }} 
                // check 2.5 mile logic but maxed out bars
                 
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']>2 && $rest['is_delivery_on']==1){
                        //$return_result['data'] = $rest;
                        $restaurantFound= 1;
                        //$return_result['is_form'] = 0;
                        $messageId = '5';
                        $return_result['type'] = "2.5 mile logic max out [Not LOGGED IN USER without reffral user]";
                  } 
                }} 
                // check 2.5 mile logic but logic and delivery off logic 
                 
                if($result!=null){    
                foreach($result as $rest){
                  if($restaurantFound==0 && $rest['sortby_distance']<=2.5 && $rest['orderCount']<3 && $rest['is_delivery_on']==0){
                        //$return_result['data'] = $rest;
                        $restaurantFound= 1;
                        //$return_result['is_form'] = 0;
                        $messageId = '4';
                        $return_result['type'] = "2.5 mile logic delivery off [Not LOGGED IN USER without reffral user]";
                  } 
                }} 

        }
        $userAuth1 = UserAuth::find($userAuth->id);
        $userAuth1->restaurant_distance=$restaurant_distance;        
        $userAuth1->save();  
        if($restaurantFound==0 && $messageId<1) {
               $messageId =1;
               $return_result['is_form'] = 1;
        }elseif($result==null){
               $messageId =1;
               $return_result['is_form'] = 1;
        }
        $messageArray=['0'=>'','1'=>'The Restaurant is not yet delivering in your area but we hope to be soon.','4'=>'Due to unforeseen circumstances we have no available bars in their area this evening but we are remedying the problem and expect to be back online tomorrow.','5'=>'Due to heavy rush there is a long queue at the bar and deliveries are taking longer than expected.  We request you to check back after 10 minutes','6'=>'Restaurant not deliver'];
        $return_result['message'] = $messageArray[$messageId];
        return [$return_result];
        
    }  
}

