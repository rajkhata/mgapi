<?php

namespace App\Http\Controllers;

use App\Models\StaticBlock;
use App\Models\StaticPages;
use App\Models\UserAuth;
use Illuminate\Http\Request;
use App\Helpers\CommonFunctions;
class StaticController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['showRestPageContent', 'showRestContent']]);
    }

    /**
     * Function to return Specific Page Content of a restaurant
     * @param $id
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function showRestPageContent($page, Request $request)
    {
        $ms = microtime(true);

        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
          $langId =  $language_id->id;	
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');

        $bearerToken = $request->bearerToken();
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            // content length to be trimmed
            $trimContentLenght = config('constants.content_trim_length');

            // page type needs to be constant
            $page = StaticPages::where('restaurant_id', $locationId)
                ->leftJoin('restaurants', 'restaurants.id', '=', 'static_pages.restaurant_id')
                ->select('static_pages.*', 'restaurants.restaurant_name')
                ->where(['page_type'=> $page,'static_pages.language_id'=>$langId])->first();
            //return response()->json($page);
            $error = '';
            if ($page) {
                $page['image'] = json_decode($page['image'], true);
                $page['video'] = json_decode($page['video'], true);
                if (strlen($page['page_content']) > $trimContentLenght) {
                    $offset               = ($trimContentLenght - 3) - strlen($page['page_content']);
                    $page['page_excerpt'] = substr($page['page_content'], 0, strrpos($page['page_content'], ' ', $offset)) . '...';
                } else {
                    $page['page_excerpt'] = $page['page_content'];
                }
                // Check for static blocks
                $staticBlockIds = explode(',', $page->static_block_ids);
                $staticBlocks = StaticBlock::whereIn('id', $staticBlockIds)->select('block_data','block_title')->get();
                $staticBlockData = [];
                foreach($staticBlocks as $block) {
                    $staticBlockData[$block['block_title']] = [];
                    array_push($staticBlockData[$block['block_title']], json_decode($block['block_data'], true));
		    //array_push($staticBlockData, json_decode($block, true));	
                }
                $page['static_blocks'] = $staticBlockData;
            } else {
                $error = 'Not a valid restaurant.';
            }
            $me = microtime(true) - $ms;

            return response()->json(['data' => [$page], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Guest token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Function to return all Page Contents of a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showRestContent(Request $request)
    {
        $ms = microtime(true);

        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $langId =  $language_id->id;
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');

        $bearerToken = $request->bearerToken();
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            // content length to be trimmed
            $trimContentLenght = config('constants.content_trim_length');

            $pages = StaticPages::where(['restaurant_id'=> $locationId,'static_pages.language_id'=>$langId])
                ->leftJoin('restaurants', 'restaurants.id', '=', 'static_pages.restaurant_id')
                ->select('static_pages.*', 'restaurants.restaurant_name')->get();

            $error = !$pages ? 'Not a valid restaurant.' : '';
            $newPagesArr       = [];
            foreach ($pages as $pg) {
                $pg['image'] = json_decode($pg['image'], true);
                $pg['video'] = json_decode($pg['video'], true);
                if (strlen($pg['page_content']) > $trimContentLenght) {
                    $offset               = ($trimContentLenght - 3) - strlen($pg['page_content']);
                    $pg['page_excerpt'] = substr($pg['page_content'], 0, strrpos($pg['page_content'], ' ', $offset)) . '...';
                } else {
                    $pg['page_excerpt'] = $pg['page_content'];
                }
                // Check for static blocks
                $staticBlockIds = explode(',', $pg->static_block_ids);
                $staticBlocks = StaticBlock::whereIn('id', $staticBlockIds)->select('block_data')->get();
                $staticBlockData = [];
                foreach($staticBlocks as $block) {
                    array_push($staticBlockData, json_decode($block['block_data'], true));
                }
                $pg['static_blocks'] = $staticBlockData;
                $newPagesArr[$pg['page_type']] = $pg;
            }
            $me = microtime(true) - $ms;

            return response()->json(['data' => [$newPagesArr], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Guest token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    private function testAlter(&$itemArr, $key) {

    }
     
}
