<?php

namespace App\Http\Controllers;

use App\Helpers\ApiPayment;
use App\Helpers\UserFunctions;
use App\Models\TableType;
use App\Models\Reservation;
use App\Models\UserCards;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\Floor;
use App\Models\Table;
use App\Models\ReservationStatus;
use App\Models\UserAuth;
use App\Helpers\CommonFunctions;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Events\TableReservation;
use App\Events\TableReservationCancel;
use App\Events\TableReservationModify;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Modules\Reservation\Http\Requests\CheckValidity;
use App\Models\CustomWorkingHours;
use App\Models\WorkingHours;
use App\Helpers\ReservationFunction;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $availability_status = false;
    protected $availability_msg = false;


    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {

    }

    public function getReservationSlots(Request $request)
    {
        $restaurant_id = $request->header('X-location');
        $ms = microtime(true);
        $success_msg = ('Request processed successfully.');
        $reservation_date = $request->get('reservation_date');
        $payment_integration_required = false;
        $payment_data = '';
        try {
            $start_date_time = Carbon::parse($reservation_date . ' ' . "24:00:00")->format('Y-m-d H:i:s');
            $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
            if ($start_date_time < $localCurrentTime) {
                $me = microtime(true) - $ms;
                return ['error' => 'Reservations cannot be made for a past time.', 'xtime' => $me];
            }
            if (empty($reservation_date)) {
                $reservation_date = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'Y-m-d');
            }
            $partySizeArr = Restaurant::where('id', $restaurant_id)->select(DB::raw('REPLACE(json_extract(reservation_settings, "$.reservation.min_for_online_reservation"), \'"\', \'\')  as min, REPLACE(json_extract(reservation_settings, "$.reservation.max_for_online_reservation"), \'"\', \'\') as max, REPLACE(json_extract(reservation_settings, "$.reservation.accept_online_reservation"), \'"\', \'\') as accept_reservation'))->first()->toArray();
            \Log::info($partySizeArr);
            // as discussed with pravish, if setting is not exist then default should be 1 to 10
            $min_party = !empty($partySizeArr) && (($partySizeArr['min']) && $partySizeArr['min'] > 0) ? $partySizeArr['min'] : 1;
            $max_party = !empty($partySizeArr) && (($partySizeArr['max']) && $partySizeArr['max'] > 0) ? $partySizeArr['max'] : 10;
            $accept_reservation = !empty($partySizeArr) && (($partySizeArr['accept_reservation']) && $partySizeArr['accept_reservation'] > 0) ? true : false;
            if (!$accept_reservation) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => ['slots' => [], 'min_party' => $min_party, 'max_party' => $max_party]
                    , 'reservation_date'=>$reservation_date, 'payment_integration_required'=>$payment_integration_required, 'payment_data'=>$payment_data,  'success_msg' => 'Restaurant is currently not taking online reservations.', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
            $scheduled_hours = $this->getWorkingHours($reservation_date, $restaurant_id);
            if (!$scheduled_hours) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => ['slots' => [], 'min_party' => $min_party, 'max_party' => $max_party]
                , 'reservation_date'=>$reservation_date, 'payment_integration_required'=>$payment_integration_required, 'payment_data'=>$payment_data,  'success_msg' => 'Restaurant is currently closed. Please choose a different date for your reservation.', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

                //return response()->json(['error' => 'Restaurant is currently closed. Please choose a different date for your reservation.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            if ($scheduled_hours->is_dayoff) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => ['slots' => [], 'min_party' => $min_party, 'max_party' => $max_party], 'reservation_date'=>$reservation_date, 'payment_integration_required'=>$payment_integration_required, 'payment_data'=>$payment_data,  'success_msg' => 'Restaurant is closed. No reservations can be made on this date.', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

                //return response()->json(['error' => 'Restaurant is closed. No reservations can be made on this date.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            $slots_data = $this->getAvailableSlots($scheduled_hours);
            if (!$slots_data['success']) {
                $me = microtime(true) - $ms;
                return response()->json(['error' => $slots_data['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }

            /*$slots = ReservationFunction::getSlotsFromTimeRange(config('reservation.restaurant_timing.open_time'), config('reservation.restaurant_timing.close_time'), config('reservation.slot_interval_time'), false);
            $data = ['slots' => ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $slots, true), 'min_party' => $min_party, 'max_party' => $max_party];
            $me = microtime(true) - $ms;
            return response()->json(['data' => $data, 'reservation_date'=>$reservation_date, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));*/

            if (isset($slots_data['slots_availability']['slot_range']) && !empty($slots_data['slots_availability']['slot_range'])) {
                $payment_integration_data = $this->getCancellationChargeInfo($restaurant_id);
                if($payment_integration_data['success']){
                    $payment_integration_required = true;
                    $payment_data = isset($payment_integration_data['data']) && !empty($payment_integration_data['data']) ? $payment_integration_data['data']: '';
                }
                $slot = ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, array_keys($slots_data['slots_availability']['slot_range']), true);
                usort($slot, function($a, $b) {
                    return ($a['key'] < $b['key']) ? -1 : 1;
                });
                $data = ['slots' => $slot, 'min_party' => $min_party, 'max_party' => $max_party];
                if(empty($slot) || count($slot)<=0){
                    $success_msg = 'Restaurant is currently closed. Please choose a different date for your reservation.';
                }
                $me = microtime(true) - $ms;
                return response()->json(['data' => $data, 'reservation_date'=>$reservation_date, 'payment_integration_required'=>$payment_integration_required, 'payment_data'=>$payment_data,  'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            // Failure response
            return response()->json(['error' => Config('constants.status_code.BAD_REQUEST'), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
        }
    }

    public function checkAvailabilityAndGetAvailableSlots(Request $request)
    {
        $source = config('reservation.source.online');
        $reservation_date = $request->get('reservation_date');
        $start_time = $request->get('reservation_time');
        $start_time = $request->has('slot_time') ? $request->get('slot_time') : $start_time;
        $start_time = date('H:i', strtotime($start_time));
        $party_size = $request->get('party_size');
        $restaurant_id = $request->header('X-location');
        $table_id = $floor_id = NULL;
        $response = $this->checkAvailability($reservation_date, $start_time, $party_size, $floor_id, $table_id, null, $restaurant_id, null, $source);
        if (isset($response) && empty($response['error'])) {
            return response()->json($response, Config('constants.status_code.STATUS_SUCCESS'));
        }
        return response()->json($response, Config('constants.status_code.BAD_REQUEST'));
    }

    private function checkAvailability($reservation_date, $start_time, $party_size, $floor_id = NULL, $table_id = NULL, $walkInFlag = NULL, $restaurant_id, $reservation_id = NULL, $source)
    {

        $ms = microtime(true);
        $payment_integration_required = false;
        $warning_message = null;
        $start_date_time = Carbon::parse($reservation_date . ' ' . $start_time)->format('Y-m-d H:i:s');
        $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
        if ($start_date_time < $localCurrentTime) {
            $me = microtime(true) - $ms;
            return ['error' => 'Reservations cannot be made for a past time.', 'xtime' => $me];
        }
        $global_setting_resp = $this->checkGlobalSettings($restaurant_id, $source, $start_date_time, $party_size);
        if (!$global_setting_resp['success']) {
            $me = microtime(true) - $ms;
            return ['error' => $global_setting_resp['message'], 'xtime' => $me];
        }
        try {
            $scheduled_hours = $this->getWorkingHours($reservation_date, $restaurant_id);
            if (!$scheduled_hours) {
                $me = microtime(true) - $ms;
                return ['error' => 'Restaurant is currently closed. Please choose a different date for your reservation.', 'xtime' => $me];
            }
            if ($scheduled_hours->is_dayoff) {
                $me = microtime(true) - $ms;
                return ['error' => 'Restaurant is closed. No reservations can be made on this date.', 'xtime' => $me];
            }
            $slots_data = $this->getAvailableSlots($scheduled_hours);

            if (!$slots_data['success']) {
                $me = microtime(true) - $ms;
                return ['error' => $slots_data['message'], 'xtime' => $me];
            }
            $table_types = $this->getAllTableTypeInfo($restaurant_id);

            $table_type_id = $this->getTableTypeIdByPartySize($restaurant_id, $party_size);
            if (!$table_type_id) {
                $me = microtime(true) - $ms;
                return ['error' => 'No turnover time setting found for this party size', 'xtime' => $me];
            }
            $tat = isset($slots_data['slots_availability']['party_size'][$start_time][$table_type_id]) && !empty($slots_data['slots_availability']['party_size'][$start_time][$table_type_id]) ? $slots_data['slots_availability']['party_size'][$start_time][$table_type_id] : $table_types[$table_type_id]['tat'];
            // As discussed with Pravish if tat is not defined then consider it as defalut slot interval, do not show validation message
            if (empty($tat)) {
                $tat = config('reservation.slot_interval_time');
            }
            $end_time = Carbon::parse($start_time)->addMinutes($tat)->format('H:i:s');
            $time_interval = config('reservation.slot_interval_time');
            // NEW WALK IN
            if (!is_null($table_id) && is_string($table_id) && strpos($table_id, ',')) {
                $table_id = explode(',', $table_id);
            }
            $all_special_occasion = config('reservation.special_occasion');
            $all_special_occasion = ReservationFunction::getSpecialOccasionKeyPair($all_special_occasion);


            $payment_integration_data = $this->getCancellationChargeInfo($restaurant_id);
            if($payment_integration_data['success']){
                $payment_integration_required = true;
            }
            if (ReservationFunction::isStartTimeEndTimeExistInTimeRangeArr($slots_data['slots_availability']['slot_range'], $start_time, $end_time)) {
                if ($slots_data['slots_availability']['max_covers_limit'][$start_time] < $party_size || $slots_data['slots_availability']['table_count'][$start_time] < 1) {
                    if ($source == config('reservation.source.online')) {
                        $near_by_slots = $this->getNearBySlots($start_time, $end_time, $slots_data['slots'], $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                        if (empty($near_by_slots)) {
                            $me = microtime(true) - $ms;
                            return ['error' => 'No tables are available at the selected timeslot.', 'xtime' => $me];
                        }
                        $me = microtime(true) - $ms;
                        return ['data' => ['available_nearby_slots' => ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $near_by_slots, true)], 'special_occasion'=>$all_special_occasion, 'available'=>false, 'payment_integration_required'=>$payment_integration_required, 'error' => null, 'message' => 'No tables are available at the selected timeslot. You can choose a nearby timeslot.', 'xtime' => $me];
                    } else {
                        $warning_message = 'Maximum covers limit for this shift has been reached. However, you can still book for this timeslot.';
                    }
                }
                $response = $this->checkReservationSlotsAvailableForGivenSlot($start_time, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                if (!$response['success']) {
                    /*$me = microtime(true) - $ms;
                    return ['error' => $response['message'], 'xtime' => $me];*/
                    $near_by_slots = $this->getNearBySlots($start_time, $end_time, $slots_data['slots'], $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                    if (empty($near_by_slots)) {
                        $me = microtime(true) - $ms;
                        return ['error' => 'No tables are available at the selected timeslot.', 'xtime' => $me];
                    }
                    $me = microtime(true) - $ms;
                    return ['data' => ['available_nearby_slots' => ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $near_by_slots, true)], 'special_occasion'=>$all_special_occasion, 'available'=>false, 'payment_integration_required'=>$payment_integration_required, 'error' => null, 'message' => 'No tables are available at the selected timeslot. You can choose a nearby timeslot.', 'xtime' => $me];
                }
                $floorTableData = $response['data'];
                $me = microtime(true) - $ms;
                $warning_message = ($warning_message) ? $warning_message : $response['message'];
                return ['data' => $floorTableData, 'special_occasion'=>$all_special_occasion, 'available'=>true, 'payment_integration_required'=>$payment_integration_required, 'error' => null, 'message' => $warning_message, 'xtime' => $me];
            } /*elseif (ReservationFunction::checkEndTimeExceedFromRange($slots_data['slots_availability']['slot_range'], $start_time, $end_time)) {
                \Log::info('heree');
                $me = microtime(true) - $ms;
                if ($source == config('reservation.source.online')) {
                    return ['error' => 'There is no table available at the restaurant for the requested time. Please choose another time.', 'xtime' => $me];
                } else {
                    return ['error' => 'Turnover time of selected party size is greater than the remaining shift time.', 'xtime' => $me];
                }
            } */else {
                \Log::info('hhhh');
                $near_by_slots = $this->getNearBySlots($start_time, $end_time, $slots_data['slots'], $party_size, $time_interval, $reservation_date, $restaurant_id, $slots_data['slots_availability'], $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                if (empty($near_by_slots)) {
                    $me = microtime(true) - $ms;
                    return ['error' => 'No tables are available at the selected timeslot.', 'xtime' => $me];
                }
                $me = microtime(true) - $ms;
                return ['data' => ['available_nearby_slots' => ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $near_by_slots, true)], 'special_occasion'=>$all_special_occasion, 'available'=>false, 'payment_integration_required'=>$payment_integration_required, 'error' => null, 'message' => 'No tables are available at the selected timeslot. You can choose a nearby timeslot.', 'xtime' => $me];
            }
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            return ['error' => $e->getMessage(), 'xtime' => $me];
        }
    }

    public function getWorkingHours($input_date, $restaurant_id)
    {
        // Check restaurant availability in custom working hours
        $custom_hours = DB::table('cms_restaurant_resv_custom_time')->where('calendar_date->start_date', '=', $input_date)->where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->get()->first();
        /*$custom_hours = CustomWorkingHours::where('calendar_date->start_date', '<=', $input_date)
     ->where('calendar_date->end_date', '>=', $input_date)
     ->where('restaurant_id', $restaurant_id)
     ->first();*/
        if ($custom_hours) {
            return $custom_hours;
        }
        // Check restaurant availability in week day working hours
        $input_day = ReservationFunction::getDayOfWeekByDate($input_date);
        $week_hours = WorkingHours::where('calendar_day', $input_day)
            ->where('restaurant_id', $restaurant_id)
            ->first();
        return $week_hours;
    }

    public function getAvailableSlots($hour_schedule)
    {
        $slot_detail = ($hour_schedule->slot_detail) ? json_decode($hour_schedule->slot_detail, true) : NULL;
        if (!$slot_detail) {
            return ['success' => false, 'message' => 'Restaurant is currently closed. Please choose a different date for your reservation.'];
        }
        $slot_arr = $slot_availability = array();
        foreach ($slot_detail as $slot) {
            if(!empty($slot)) {
                $slot_arr_new = json_decode($slot['pacing'], true);
                if(!empty($slot_arr_new) && count($slot_arr_new)>0){
                    $available_table = ($slot['available_tables']) ? json_decode($slot['available_tables'], true) : null;
                    $table_available_count = array_sum(array_column($available_table, 'table_count'));
                    $floors_available = json_decode($slot['floors'], true);
                    $party_size = json_decode($slot['party_size'], true);
                    $max_covers_limit = $slot['max_covers_limit'];
                    $slot_range = $slot['open_time'] . '-' . $slot['close_time'];
                    if(!empty($slot_arr_new)){
                        foreach ($slot_arr_new as $slot_key => $slot_time) {
                            if(isset($slot_time['time']) && !empty($slot_time['time'])){
                                $slot_availability['floors'][$slot_time['time']] = $floors_available;
                                $slot_availability['party_size'][$slot_time['time']] = $party_size;
                                $slot_availability['table'][$slot_time['time']] = !empty($available_table) ? $available_table : null;
                                $slot_availability['max_covers_limit'][$slot_time['time']] = $max_covers_limit;
                                $slot_availability['table_count'][$slot_time['time']] = $table_available_count;
                                //$slot_availability['cover_count'][$slot_time['time']] = $cover_available;
                                $slot_availability['slot_range'][$slot_time['time']] = $slot_range;
                                $slot_availability['pacing'][$slot_time['time']] = $slot_time;
                            }
                        }
                        $slot_arr = array_merge($slot_arr, $slot_arr_new);
                    }
                }
            }
        }
        $slot_arr = array_sort($slot_arr);
        return ['success' => true, 'slots' => $slot_arr, 'slots_availability' => $slot_availability];
    }

    public function getNearBySlots($start_time, $end_time, $slot_arr, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id, $table_id = NULL, $reservation_id = NULL, $source, $tat, $warning_message = NULL)
    {
        $available_block = array();
        if (isset($slot_availability['slot_range']) && !empty($slot_availability['slot_range'])) {
            $defined_slots = ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, array_keys($slot_availability['slot_range']), false);
            $slots = $defined_slots;
        }
        if(in_array($start_time, $slots)){
            $split = array_search($start_time, $slots);
            $before_arr = array_slice($slots, 0, $split);  // first part
            $after_arr = array_slice($slots, $split + 1); // second part
        }else{
            $slots = ReservationFunction::getSlotsFromTimeRange(config('reservation.restaurant_timing.open_time'), config('reservation.restaurant_timing.close_time'), config('reservation.slot_interval_time'), false);
            $slots = ReservationFunction::getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $slots, false);
            if(in_array($start_time, $slots)){
                $split = array_search($start_time, $slots);
                $before_arr = array_slice($slots, 0, $split);  // first part
                $before_arr=isset($before_arr) && !empty($before_arr) ? array_intersect($before_arr,$defined_slots):[];

                $after_arr = array_slice($slots, $split + 1); // second part
                $after_arr=isset($after_arr) && !empty($after_arr) ? array_intersect($after_arr,$defined_slots):[];
            }
        }
        if(isset($before_arr) && !empty($before_arr)){
            $before_arr = array_reverse($before_arr);
            $before_available_block = $this->getNearBySlotsByAllSlots($before_arr, $reservation_date, $restaurant_id, $slot_availability, $time_interval, $floor_id, $table_id, $reservation_id, $source, $tat, $party_size, $warning_message);
            $before_available_block = !empty($before_available_block) ? array_reverse($before_available_block):[];
        }
        if(isset($after_arr) && !empty($after_arr)){
            $after_available_block = $this->getNearBySlotsByAllSlots($after_arr, $reservation_date, $restaurant_id, $slot_availability, $time_interval, $floor_id, $table_id, $reservation_id, $source, $tat, $party_size, $warning_message);
            $after_available_block = !empty($after_available_block) ? $after_available_block:[];
        }
        if((isset($before_available_block) && !empty($before_available_block)) && (isset($after_available_block) && !empty($after_available_block)))
        {
            $available_block = array_merge($before_available_block, $after_available_block);
        }elseif (isset($before_available_block) && !empty($before_available_block)){
            $available_block = $before_available_block;
        }elseif (isset($after_available_block) && !empty($after_available_block)){
            $available_block = $after_available_block;
        }
        return $available_block;
    }

    public function checkReservationSlotsAvailableForGivenSlot($start_time, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id = NULL, $table_id = NULL, $reservation_id = NULL, $source, $tat, $warning_message = NULL)
    {
        //\Log::info($slot_availability);
        if (ReservationFunction::isStartTimeEndTimeExistInTimeRangeArr($slot_availability['slot_range'], $start_time, $end_time)) {
            if (isset($slot_availability) && $slot_availability['table'][$start_time] && !empty($slot_availability['table'][$start_time])) {
                $table_avail = $this->checkTableAvailableOrNot($slot_availability, $reservation_date, $start_time, $end_time, $party_size, $restaurant_id, $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                if ($table_avail['success']) {
                    //\Log::info([$warning_message , $table_avail['message']]);
                    $table_avail['message'] = ($warning_message) ? $warning_message : $table_avail['message'];
                    return $table_avail;
                }
                //return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
                return ['success' => false, 'message' => $table_avail['message']];
            }
            return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
        }
        return ['success' => false, 'message' => 'The restaurant is not operational at the requested time. Please choose another time.'];
    }

    public function reserveTable(Request $request)
    {
        $ms = microtime(true);
        DB::beginTransaction();
        $payment_integration_required = false;
        try {
            $source = config('reservation.source.online');
            $reservation_date = $request->get('reservation_date');
            $start_time = $request->get('reservation_time');
            $start_time = date('H:i', strtotime($start_time));
            $party_size = $request->get('party_size');
            $userId = null;
            $restaurant_id = $request->header('X-location');
            $parent_restaurant_id = $request->header('X-restaurant');

            $table_id = $floor_id = NULL;
            $response = $this->checkAvailability($reservation_date, $start_time, $party_size, $floor_id, $table_id, null, $restaurant_id, null, $source);
            //\Log::info($response);
            if (!empty($response['error'])) {
                return response()->json($response, Config('constants.status_code.BAD_REQUEST'));
            }
            $response_data = $response['data'];
            if(!isset($response['data']['tat']) || empty($response['data']['tat']) || !isset($response_data['slot_range']) || empty($response_data['slot_range'])){
                $me = microtime(true) - $ms;
                return response()->json(['error' => 'There is no table available at the restaurant for the requested time. Please choose another time.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            $slot_range = $response_data['slot_range'];
            $end_time = Carbon::parse($start_time)->addMinutes($response['data']['tat'])->format('H:i:s');

            if (count($response_data) <= 0) {
                $me = microtime(true) - $ms;
                return response()->json(['error' => 'There is no table available at the restaurant for the requested time. Please choose another time.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            $fname = $request->get('fname');
            $lname = $request->get('lname');
            $email = $request->get('email');
            $phone = $request->get('phone');
            $special_instruction = $request->get('special_instruction');
            $special_occasion = $request->has('special_occasion') ? $request->get('special_occasion') : null;
            $special_occasion = !empty($special_occasion) ? config('reservation.special_occasion.' . $special_occasion) : null;
            $notify_me = $request->has('notify_me') ? $request->get('notify_me') : 0;


            $userPayCustId = "";
            $stripeCardId = "";

            $bearerToken = $request->bearerToken();
            $userData = ['fname' => $fname, 'lname' => $lname, 'email' => $email, 'phone' => $phone];
            //CommonFunctions::$langId = $langId = config('app.language')['id'];
            if ($bearerToken) {
                $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
                $tokenField = 'Access';
            } else {
                $token = explode(' ', $request->header('Authorization'))[1];
                $userAuth = UserAuth::where(['guest_token' => $token])->first();
                $tokenField = 'Guest';
            }
            $restaurantData = Restaurant::where(array('id'=>$restaurant_id))->first();
            if ($userAuth) {
                $restInfo = CommonFunctions::getRestaurantDetails($parent_restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name', 'custom_from', 'support_from'));
                $bearerToken = $request->bearerToken();
                //\Log::info($bearerToken);
                if ($bearerToken) {
                    $userId       = $userAuth->user_id;
                    $userInfo = User::find($userId);
                    $userPayCustId = $userInfo['stripe_customer_id'];
                    $is_guest = 0;
                } else {
                    $is_guest = 1;
                    $userObj = User::where('restaurant_id', $parent_restaurant_id)
                        ->where(function ($query) use ($email, $phone) {
                            $query->where('email', $email)
                                ->orWhere('mobile', $phone);
                        })->first();

                    if (empty($userObj)) {
                        /*$userData['restaurant_id'] = $parent_restaurant_id;
                        $userHlprObj = new UserFunctions();
                        $userObj = $userHlprObj->userRegister($request, $userData);
                        if (empty($userObj['error']) && isset($userObj['data']['user']['id'])) {
                            $userId = $userObj['data']['user']['id'];
                            ################ mail #######
                            $restaurant_data = Restaurant::where('id', $restaurant_id)->select('id', 'email', 'restaurant_name', 'source_url')->first();
                            $mailKeywords = array(
                                'SITE_URL' => $restaurant_data->source_url,
                                'LOGIN_URL' => $restaurant_data->source_url . 'login',
                                'SITE_NAME' => $restInfo['restaurant_name'],
                                'REST_NAME' => strtolower($restaurant_data->restaurant_name),
                                'USER_NAME' => $fname,
                                'EMAIL' => $email,
                                'PASSWORD' => config('constants.user_default_password'),
                                'title' => "Welcome to {$restInfo['restaurant_name']}",
                            );

                            $mailTemplate = CommonFunctions::mailTemplate('guest_registration', $mailKeywords, 'header_layout', 'footer_layout', $parent_restaurant_id);
                            $mailData['subject'] = "Welcome to {$restInfo['restaurant_name']}";
                            $mailData['body'] = $mailTemplate;
                            $mailData['receiver_email'] = $email;
                            $mailData['receiver_name'] = $fname;
                            $mailData['MAIL_FROM_NAME'] = $restInfo['restaurant_name'];
                            $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                            CommonFunctions::sendMail($mailData);
                        }*/
                    } else {
                        $userId = $userObj->id;
                    }
                }
                // reserve table for offline/online
                $floor_table_info = reset($response_data['floor_table_info']);
                if (count($floor_table_info) <= 0) {
                    $me = microtime(true) - $ms;
                    return response()->json(['error' => 'Reservation slots are not available', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
                $table_info = reset($floor_table_info['tables']);
                $start_date_time = ReservationFunction::getDateTime($reservation_date, $start_time);
                $start_date_time1 = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
                $end_date_time = ReservationFunction::getDateTime($reservation_date, $end_time);
                $end_date_time1 = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
                $notify_me = ($notify_me) ? 1 : 0;
                $reserved_status_id = ReservationStatus::getReservedStatusId();
                $table_id = $table_info['id'];
                $txn_id = ReservationFunction::generateReservationReceipt();
                $reservation = Reservation::create(['parent_restaurant_id'=>$parent_restaurant_id,'restaurant_id' => $restaurant_id, 'user_id' => $userId, 'fname' => $fname, 'lname' => $lname, 'email' => $email, 'phone' => $phone, 'start_time' => $start_date_time1, 'end_time' => $end_date_time1, 'status_id' => $reserved_status_id, 'source' => $source, 'reserved_seat' => $party_size, 'notify_me' => $notify_me, 'floor_id' => $floor_table_info['id'], 'table_id' => $table_id, 'mobile' => $phone, 'slot_time' => $slot_range,
                    'host_name' => $request->getHost(),
                    'user_ip' => $request->ip(),
                    'receipt_no' => $txn_id,
                    'special_occasion' => $special_occasion,
                    'user_instruction' => $special_instruction,
                    'is_guest' =>$is_guest,
                    'restaurant_name' => $restaurantData->restaurant_name
                ]);
                if ($userId) {
                    CommonFunctions::changeUserCategory($userId, $parent_restaurant_id);
                }
                $payment_integration_data = $this->getCancellationChargeInfo($restaurant_id);
                if($payment_integration_data['success']){
                    if((isset($payment_integration_data['data']['charge']) && !empty($payment_integration_data['data']['charge'])) && (isset($payment_integration_data['data']['charge_type']) && !empty($payment_integration_data['data']['charge_type']))){
                        $payment_integration_required = true;
                        $tot_amount = $amount = $payment_integration_data['data']['charge'];
                        if($payment_integration_data['data']['charge_type']==config('reservation.cancellation_charge_type.per_cover')){
                            $tot_amount = $amount*intval($party_size);
                        }
                    }
                }
                if($payment_integration_required){
                    $customerName  = $fname . ' ' . $lname;
                    $customerEmail = $email;
                    $cardInfo = $request->input('card_info');

                    $cardInfoData  = array();
                    $cardInfoData = json_decode(base64_decode($cardInfo));

                    $transaction_id  = $cardNum = $cardType = "";
                    if ($userPayCustId == null) {
                        $data = [
                            'description' => $customerName,
                            'email'       => $customerEmail,
                            //'source'      => 'tok_visa',
                        ];
                        $apiObj        = new ApiPayment();
                        $result        = $apiObj->createCustomer($data);
                        $userPayCustId = $result->id;
                        $cardNumber = str_replace(' ', '', $cardInfoData->card);

                        $cardData = [
                            'number'    => $cardNumber,
                            'exp_month' => $cardInfoData->expiry_month,
                            'exp_year'  => $cardInfoData->expiry_year,
                            'cvc'       => $cardInfoData->cvv,
                            'name'      => $cardInfoData->name,
                        ];
                        //print_r($cardData); die;
                        $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);

                        if(isset($result2['errorMesg']))
                        {
                            $me = microtime(true) - $ms;
                            DB::rollback();
                            return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));

                        }
                        else {
                            $stripeCardId = $result2['id'];
                        }

                        $data = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'ReservationID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        //print_r($data); die;
                        try {
                            $chargeResponse = $apiObj->captureCharge($data);
                        } catch (\Exception $e) {
                            $me = microtime(true) - $ms;
                            \Log::info($e);
                            DB::rollback();
                            return response()->json(['error' => $e->getMessage(), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
                        }
                        $transaction_id  = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                        //first time user save card
                        if(isset($cardInfoData->card_id)==null){
                            $userInfo = User::where('id', $userId)->first();
                            if($userInfo){
                                $userCard = "";
                                $userCard = UserCards::where('last_four', substr($cardNumber, -4))->where('user_id', $userId)->first();
                                if($userId && ($cardInfoData->isSave && !$userCard)){
                                    $data = [
                                        'user_id'        => $userId,
                                        'name'           => $cardInfoData->name,
                                        'last_four'      => substr($cardNumber, -4),
                                        'type'           => $cardInfoData->type,
                                        'status'         => 1,
                                        'stripe_card_id' => $stripeCardId,
                                    ];
                                    UserCards::create($data);
                                    $userInfo->stripe_customer_id = $userPayCustId;
                                    $userInfo->save();
                                }
                            }
                        }
                        // code end
                    } else {
                        if(isset($cardInfoData->card_id)==null)
                        {
                            $cardNumber = str_replace(' ', '', $cardInfoData->card);
                            $cardData = [
                                'number'    => $cardNumber,
                                'exp_month' => $cardInfoData->expiry_month,
                                'exp_year'  => $cardInfoData->expiry_year,
                                'cvc'       => $cardInfoData->cvv,
                                'name'      => $cardInfoData->name,
                            ];
                            $apiObj        = new ApiPayment();
                            $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);
                            if(isset($result2['errorMesg']))
                            {
                                $me = microtime(true) - $ms;
                                DB::rollback();
                                return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));

                            }
                            else {
                                $stripeCardId = $result2['id'];
                            }
                            $userInfo = User::where('id', $userId)->first();
                            $userCard = "";
                            $userCard = $userInfo->card()->where('last_four', substr($cardNumber, -4))->where('user_id', $userId)->first();
                            if($cardInfoData->isSave && !$userCard) {
                                $data = [
                                    'user_id'        => $userId,
                                    'name'           => $cardInfoData->name,
                                    'last_four'      => substr($cardNumber, -4),
                                    'type'           => $cardInfoData->type,
                                    'status'         => 1,
                                    'stripe_card_id' => $stripeCardId,
                                ];
                                UserCards::create($data);
                            }
                        }
                        else{
                            $stripeCardId         = $cardInfoData->card_id;
                        }

                        // Charge the user
                        $data           = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'ReservationID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        $apiObj         = new ApiPayment();
                        try {
                            $chargeResponse = $apiObj->captureCharge($data);
                        } catch (\Exception $e) {
                            $me = microtime(true) - $ms;
                            \Log::info($e);
                            DB::rollback();
                            return response()->json(['error' => $e->getMessage(), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
                        }
                        $transaction_id = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                    }

                    $reservation_update = Reservation::find($reservation->id);
                    $reservation_update->cancellation_charge_type = $payment_integration_data['data']['charge_type'];
                    $reservation_update->cancellation_charge = $amount;
                    $reservation_update->total_cancellation_charge = $tot_amount;
                    $reservation_update->total_charged_amount = $tot_amount;
                    $reservation_update->stripe_charge_id = $transaction_id;
                    $reservation_update->stripe_card_id   = $stripeCardId;
                    $reservation_update->card_number = 'XXXX-XXXX-XXXX-'.$cardNum;
                    $reservation_update->card_type = $cardType;
                    $reservation_update->payment_gatway = config('reservation.payment_gatway.stripe');
                    $reservation_update->payment_status = config('reservation.payment_status.charged');
                    $reservation_update->save();
                }
                try {
                    \Log::info('Table Reservation event CALLED');
                    ReservationFunction::sendReservationNotification($parent_restaurant_id, $reservation, $by='Customer');
                } catch (\Exception $e) {
                    \Log::info('Notification not sent');
                    \Log::info($e);
                }
                $me = microtime(true) - $ms;
                DB::commit();
                return response()->json(['success' => true, 'message' => 'You have successfully created the reservation.', 'error' => null, 'xtime' => $me], 200);
            } else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
            }
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            \Log::info($e);
            DB::rollback();
            return response()->json(['error' => $e->getMessage(), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
        }
    }


    private function checkTableAvailableOrNot($slot_availability, $reservation_date, $start_time, $end_time, $party_size, $restaurant_id, $floor_id = NULL, $table_id = NULL, $reservation_id = NULL, $source, $tat, $warning_message = NULL)
    {
        $start_date_time = ReservationFunction::getDateTime($reservation_date, $start_time);
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $end_date_time = ReservationFunction::getDateTime($reservation_date, $end_time);
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
        $reserved_seat_info = $this->getTotalReservationCountInDateTimeRange($reservation_date, $slot_availability['slot_range'][$start_time], $restaurant_id, $reservation_id);

        //exit;
        //\Log::info($reserved_seat_info);exit;
        $total_seat_reserved = $reserved_seat_info->reserved_seat;
        //$total_table_reserved = $reserved_seat_info->reserved_table;
        $available_seat = $slot_availability['max_covers_limit'][$start_time] - $total_seat_reserved;
        $reserved_table_info = $this->getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id);
        $available_tables = $slot_availability['table_count'][$start_time] - $reserved_table_info->reserved_table;
        if ($available_seat < $party_size || $available_tables < 1) {
            if ($source == config('reservation.source.online')) {
                return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
            } else {
                $warning_message = "Maximum covers limit for this shift has been reached. However, you can still book for this timeslot.";
            }
        }
        $pacing_response = $this->checkPacing($restaurant_id, $reservation_id, $slot_availability, $reservation_date, $start_time, $end_time, $party_size, $start_date_time, $end_date_time);
        if (!$pacing_response['success']) {
            if ($source == config('reservation.source.online')) {
                return ['success' => false, 'message' => $pacing_response['message']];
            } else {
                $warning_message = ($warning_message) ? $warning_message : 'The pacing limit set for this time slot has been reached. However, you can still book for this timeslot.';
            }
        }
        //}
        $available_floor_table_info = $this->getAvailableFloorAndTables($restaurant_id, $source, $start_date_time, $end_date_time, $slot_availability, $start_time, $table_id, $floor_id, $party_size, $reservation_id, $reservation_date);
        //\Log::info($available_floor_table_info);exit;
        if (isset($available_floor_table_info) && count($available_floor_table_info) > 0) {
            return ['success' => true, 'message' => $warning_message, 'data' => ['floor_table_info' => $available_floor_table_info, 'slot_range' => $slot_availability['slot_range'][$start_time], 'tat' => $tat]];
        }
        return ['success' => false, 'message' => 'There is no table available at the restaurant for the requested time. Please choose another time.'];
    }

    public function getTotalReservationCountInDateTimeRange($reservation_date, $time_range, $restaurant_id, $reservation_id = NULL)
    {
        $time_range = explode('-', $time_range);
        //\Log::info($time_range);
        $start_date_time = $reservation_date . ' ' . $time_range[0] . ':00';
        //\Log::info($start_date_time);

        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        //\Log::info($start_date_time);

        $end_date_time = $reservation_date . ' ' . $time_range[1] . ':00';

        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);

        $reservation_count = Reservation::where('restaurant_id', $restaurant_id)->whereHas('status', function ($query) {
            return $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(\DB::raw('sum(reserved_seat) as reserved_seat'), \DB::raw('count(*) as reserved_table'))
            ->first();
        return $reservation_count;
    }

    public function getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id = NULL, $floor_id = NULL)
    {
        $reservation_count = Reservation::where('restaurant_id', $restaurant_id);
        if ($reservation_id) {
            $reservation_count = $reservation_count->where('id', '!=', $reservation_id);
        }
        if ($floor_id) {
            $reservation_count = $reservation_count->where('floor_id', $floor_id);
        }
        $reservation_count = $reservation_count->whereHas('status', function ($query) {
            return $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'))
            ->first();
        return $reservation_count;
    }


    private function checkGlobalSettings($restaurant_id, $source, $reservation_date_time, $party_size)
    {
        $reservation_settings = Restaurant::where('id', $restaurant_id)->value('reservation_settings');
        $reservation_settings = isset($reservation_settings) && !empty($reservation_settings) ? json_decode($reservation_settings, true) : false;
        if (empty($reservation_settings) || empty($reservation_settings['reservation']) || count($reservation_settings['reservation'])<=0) {
            return ['success' => false, 'message' => 'Restaurant is currently not taking online reservations.'];
        }

        if ($source == config('reservation.source.online')) {
            //check online reservation accept or not
            if (!$reservation_settings['reservation']['accept_online_reservation']) {
                return ['success' => false, 'message' => 'Restaurant is currently not taking online reservations.'];
            }
            //Min/Max setting for online reservation
            if ($party_size < $reservation_settings['reservation']['min_for_online_reservation'] || $party_size > $reservation_settings['reservation']['max_for_online_reservation']) {
                return ['success' => false, 'message' => 'Restaurant does not support this party size'];
            }
            //check online reservation cutoff and advance time
            if ($reservation_settings['reservation']['cuttoff_type'] || $reservation_settings['reservation']['allow_guest_advance_online_reservation_type']) {
                $advanceType = ($reservation_settings['reservation']['allow_guest_advance_online_reservation_type']) ? $reservation_settings['reservation']['allow_guest_advance_online_reservation_type'] : null;
                $advanceTime = ($reservation_settings['reservation']['allow_guest_advance_reservation_time']) ? $reservation_settings['reservation']['allow_guest_advance_reservation_time'] : null;
                $cutOffType = ($reservation_settings['reservation']['cuttoff_type']) ? $reservation_settings['reservation']['cuttoff_type'] : null;
                $cutOffTime = ($reservation_settings['reservation']['cuttoff_time']) ? $reservation_settings['reservation']['cuttoff_time'] : null;

                if (!empty($advanceType) && !empty($advanceTime)) {
                    if (Carbon::parse($reservation_date_time)->format('Y-m-d') > $this->addTimeInDateTime($advanceType, $advanceTime, $restaurant_id)) {
                        return ['success' => false, 'message' => "Restaurant allow guests to make reservation in $advanceTime $advanceType advance"];
                    }
                }
                if (!empty($cutOffType) && !empty($cutOffTime)) {
                    if ($reservation_date_time < $this->addTimeInDateTime($cutOffType, $cutOffTime, $restaurant_id)) {
                        return ['success' => false, 'message' => "Restaurant reservation cut-off time is $cutOffTime $cutOffType"];
                    }
                }
            }
        }
        return ['success' => true];
    }

    private function getCancellationChargeInfo($restaurant_id)
    {
        $paymentInfoArr = Restaurant::where('id', $restaurant_id)->select(DB::raw('REPLACE(json_extract(reservation_settings, "$.reservation.cancellation_charge"), \'"\', \'\')  as charge, REPLACE(json_extract(reservation_settings, "$.reservation.cancellation_charge_type"), \'"\', \'\') as charge_type'))->first()->toArray();
        if (empty($paymentInfoArr) || empty($paymentInfoArr['charge']) || empty($paymentInfoArr['charge_type'])) {
            return ['success' => false, 'error' => 'No setting found'];
        }
        return ['success' => true, 'data' => $paymentInfoArr];
    }

    private function addTimeInDateTime($timeType, $timeTypeCount, $restaurant_id)
    {
        if ($timeType == config('reservation.cut_off_time_type')[0]) {
            if($timeTypeCount=='0.5' || $timeTypeCount==0.5){
                return Carbon::parse(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id))->addMinutes(30);
            }else{
                return Carbon::parse(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id))->addHours($timeTypeCount);
            }
        } elseif ($timeType == config('reservation.cut_off_time_type')[1]) {
            return Carbon::parse(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id))->addDays($timeTypeCount)->format('Y-m-d');
        } elseif ($timeType == config('reservation.allow_guest_advance_online_reservation_type')[1]) {
            return Carbon::parse(CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id))->addMonths($timeTypeCount);
        }
    }

    private function getAvailableFloorAndTables($restaurant_id, $source, $start_date_time, $end_date_time, $slot_availability, $start_time, $table_id, $floor_id, $party_size, $reservation_id, $reservation_date)
    {
        //\Log::info($slot_availability);
        $available_floor_table_info = [];
        $reservation_detail_arr = $this->getReservationCountBetweenDates($start_date_time, $end_date_time, $restaurant_id, $table_id, $reservation_id);
        $table_type_reservation_count = $this->getReservationCountTableTypeWise($restaurant_id, $reservation_date, $slot_availability['slot_range'][$start_time], $reservation_id);
        $floor_table_info = Floor::select('id', 'name')->where('restaurant_id', $restaurant_id);
        if ($floor_id) {
            $floor_table_info = $floor_table_info->where('id', $floor_id);
        } else {
            if ($source == config('reservation.source.online')) {
                $floor_table_info = $floor_table_info->whereIn('id', $slot_availability['floors'][$start_time]);
            }
        }
        if (is_array($table_id)) {
            $floor_table_info = $floor_table_info->whereHas('tables', function ($query) use ($table_id) {
                $query->whereIn('id', $table_id);
            });
        } elseif ($table_id) {
            $floor_table_info = $floor_table_info->whereHas('tables', function ($query) use ($table_id) {
                $query->where('id', $table_id);
            });
        }
        $floor_table_info = $floor_table_info->where('completed_step', 2)->with(['tables' => function ($query) use ($slot_availability, $start_time, $table_id) {
            if (is_array($table_id)) {
                $query = $query->whereIn('id', $table_id);
            } elseif ($table_id) {
                $query = $query->where('id', $table_id);
            }
            $query->select('id', 'floor_id', 'name', 'min', 'max');
        }]);
        if ($table_id) {
            $floor_table_info = $floor_table_info->whereHas('tables', function ($query) use ($table_id) {
                $query->where('id', $table_id);
            });
        }
        $floor_table_info = $floor_table_info->get()->toArray();
        $blocked_table = ReservationFunction::getBlockedTableArr($restaurant_id, $floor_id=null, $table_id=null, $start_date_time, $end_date_time);
        foreach ($floor_table_info as $floor_key => $floor) {
            foreach ($floor['tables'] as $table_key => $table) {

                //check table is currently occupied or not
                /*if($reservation_date==Carbon::today()->toDateString()){
                    $current_reservation = app('Modules\Reservation\Http\Controllers\FloorController')->getTableReservations($table['id'], 'current', $reservation_date);
                    if($current_reservation && count($current_reservation)>0){
                        continue;
                    }
                }*/
                //code end
                $table_type = $table['max'];
                $table_type_arr = array();
                //\Log::info($slot_availability['table'][$start_time]);
                foreach ($slot_availability['table'][$start_time] as $slot_key => $slot_arr) {
                    if ($slot_availability['table'][$start_time][$slot_key]['table_type'] == $table_type) {
                        $table_type_arr = $slot_availability['table'][$start_time][$slot_key];
                    }
                }
                //\Log::info($table_type_arr);
                $available_table_seat = isset($table_type_arr['table_cover_count']) ? $table_type_arr['table_cover_count'] : 0;
                $available_tables = isset($table_type_arr['table_count']) ? $table_type_arr['table_count'] : 0;
                $available_table = true;
                if (!empty($table_type_reservation_count) && isset($table_type_reservation_count[$table['max']])) {
                    $table_reserved_seat = (isset($table_type_reservation_count[$table['max']]) && !empty($table_type_reservation_count[$table['max']])) ? $table_type_reservation_count[$table['max']]['reserved_seat'] : 0;
                    $table_reserved = (isset($table_type_reservation_count[$table['max']]) && !empty($table_type_reservation_count[$table['max']])) ? $table_type_reservation_count[$table['max']]['table_count'] : 0;

                    $available_table_seat = $available_table_seat - $table_reserved_seat;
                    $available_tables = $available_tables - $table_reserved;
                    /*if ($available_tables <= 0) {
                        $available_table = false;
                    }*/
                }
                if ((count($reservation_detail_arr) == 0 || !isset($reservation_detail_arr[$table['id']])) || (($reservation_detail_arr && isset($reservation_detail_arr[$table['id']])) && ($reservation_detail_arr[$table['id']]['table_count'] <= 0))) {
                    $available_table = true;
                }else{
                    $available_table = false;
                }
                if(count($blocked_table)==0  || ($blocked_table && !in_array($table['id'], $blocked_table))) {
                    if ($available_table_seat >= $party_size && $table['max'] >= $party_size && $available_table) {
                        $available_floor_table_info[$floor_key]['id'] = $floor['id'];
                        $available_floor_table_info[$floor_key]['name'] = $floor['name'];
                        $available_floor_table_info[$floor_key]['tables'][$table_key] = $table;
                    }
                }
            }
        }
        return $available_floor_table_info;
    }

    private function getReservationCountTableTypeWise($restaurant_id, $reservation_date, $time_range, $reservation_id = null, $table_id = null)
    {
        $reservationArr = array();
        $time_range = explode('-', $time_range);

        $start_date_time = $reservation_date . ' ' . $time_range[0] . ':00';
        $start_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $start_date_time);
        $end_date_time = $reservation_date . ' ' . $time_range[1] . ':00';
        $end_date_time = CommonFunctions::convertRestaurantDateTimeInUTC($restaurant_id, $end_date_time);
        $reservations = Reservation::where('restaurant_id', $restaurant_id)/*->with('table')*/;
        if ($reservation_id) {
            $reservations = $reservations->where('id', '!=', $reservation_id);
        }
        $reservations = $reservations->whereHas('status', function ($query) {
            $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)
            ->select(DB::Raw('sum(reserved_seat) as reserved_seat'), DB::Raw('count(*) as reserved_table'), 'table_id');
        if ($table_id) {
            $reservations = $reservations->where('table_id', $table_id);
        }
        $reservations = $reservations->groupBy('table_id')->get();
        \Log::info($reservations);
        //if (isset($reservations->table) && !empty($reservations->table)) {
        foreach ($reservations as $key => $reservation) {
            $table_info = Table::find($reservation->table_id);
            if(count($reservationArr)>1){
                if(in_array($table_info->max, $reservationArr)){
                    $reservationArr[$table_info->max]['table_id'] = $table_info->id;
                    $reservationArr[$table_info->max]['reserved_seat'] = $reservationArr[$table_info->max]['reserved_seat']+$reservation->reserved_seat;
                    $reservationArr[$table_info->max]['table_count'] = $reservationArr[$table_info->max]['table_count']+$reservation->reserved_table;
                    $reservationArr[$table_info->max]['table_type'] = $table_info->id;
                }
                else{
                    $reservationArr[$table_info->max]['table_id'] = $table_info->id;
                    $reservationArr[$table_info->max]['reserved_seat'] = $reservation->reserved_seat;
                    $reservationArr[$table_info->max]['table_count'] = $reservation->reserved_table;
                    $reservationArr[$table_info->max]['table_type'] = $table_info->id;
                }
            }else{
                $reservationArr[$table_info->max]['table_id'] = $table_info->id;
                $reservationArr[$table_info->max]['reserved_seat'] = $reservation->reserved_seat;
                $reservationArr[$table_info->max]['table_count'] = $reservation->reserved_table;
                $reservationArr[$table_info->max]['table_type'] = $table_info->id;
            }
        }
        /*foreach ($reservations as $key => $reservation) {
                $reservationArr[$reservations->table->max]['table_id'] = $reservation->table->id;
                $reservationArr[$reservations->table->max]['reserved_seat'] = $reservation->reserved_seat;
                $reservationArr[$reservations->table->max]['table_count'] = $reservation->reserved_table;
                $reservationArr[$reservations->table->max]['table_type'] = $reservations->table->max;
            }*/
        //}
        \Log::info($reservationArr);
        return $reservationArr;
    }

    private function checkPacing($restaurant_id, $reservation_id = null, $slot_availability, $reservation_date, $start_time, $end_time, $party_size, $start_date_time, $end_date_time)
    {
        $reserved_seat_info = $this->getTotalReservationCountInGivenRange($start_date_time, $end_date_time, $restaurant_id, $reservation_id);
        $total_seat_reserved = $reserved_seat_info->reserved_seat;
        $total_table_reserved = $reserved_seat_info->reserved_table;
        $available_seat = $slot_availability['pacing'][$start_time]['covers'] - $total_seat_reserved;
        $available_tables = $slot_availability['pacing'][$start_time]['reservations'] - $total_table_reserved;
        if ($available_seat < $party_size || $available_tables < 1) {
            return ['success' => false, 'message' => 'The pacing limit set for this time slot has been reached.'];
        }
        return ['success' => true, 'message' => 'Seats are available'];
    }

    private function getAllTableTypeInfo($restaurant_id)
    {
        $table_types = [];
        $all_table_type = TableType::where('restaurant_id', $restaurant_id)->get()->toArray();
        foreach ($all_table_type as $table_type) {
            $table_types[$table_type['id']] = $table_type;
        }
        return $table_types;
    }

    private function getTableTypeIdByPartySize($restaurant_id, $party_size)
    {
        return TableType::where('restaurant_id', $restaurant_id)->where('min_seat', '<=', $party_size)->where('max_seat', '>=', $party_size)->value('id');
    }

    private function getReservationCountBetweenDates($start_date_time, $end_date_time, $restaurant_id, $table_id = NULL, $reservation_id = NULL)
    {
        $reservationArr = array();
        $reservations = Reservation::where('restaurant_id', $restaurant_id);//->with('table');
        if ($reservation_id) {
            $reservations = $reservations->where('id', '!=', $reservation_id);
        }
        $reservations = $reservations->whereHas('status', function ($query) {
            $query->whereIn('slug', config('reservation.status_slug_check_for_book_reservation'));
        })->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time);
        if (is_array($table_id)) {
            $reservations = $reservations->whereIn('table_id', $table_id);
        } elseif ($table_id) {
            $reservations = $reservations->where('table_id', $table_id);
        }
        $reservations = $reservations->groupBy('table_id')->select('table_id', DB::raw("sum(reserved_seat) as reserved_seat"), DB::raw("count(*) as table_count"))->get();
        if ($reservations->count() > 0) {
            foreach ($reservations as $reservation) {
                if ($reservation->table_id) {
                    if (strpos($reservation->table_id, ',')) {
                        $multiTableIds = explode(',', $reservation->table_id);
                        if (count($multiTableIds)) {
                            foreach ($multiTableIds as $multiTab) {
                                $tableDetail = Table::where('restaurant_id', $restaurant_id)->where('id', '=', $multiTab)->first();
                                if ($tableDetail) {
                                    $tempTableDetail = $tableDetail->toArray();
                                    $reservationArr[$multiTab]['table_id'] = $multiTab;
                                    $reservationArr[$multiTab]['reserved_seat'] = $reservation->reserved_seat;
                                    $reservationArr[$multiTab]['table_count'] = $reservation->table_count;
                                    $reservationArr[$multiTab]['table_type'] = $tempTableDetail['max'];
                                }
                            }
                        }
                        //\Log::info([$reservation,$tableNames,$floorName,$reservation->table->max]);die();
                    } else {
                        $reservation->load('table');
                        \Log::info($reservation);
                        $reservationArr[$reservation->table_id]['table_id'] = $reservation->table_id;
                        $reservationArr[$reservation->table_id]['reserved_seat'] = $reservation->reserved_seat;
                        $reservationArr[$reservation->table_id]['table_count'] = $reservation->table_count;
                        $reservationArr[$reservation->table_id]['table_type'] = ($reservation->table) ? $reservation->table->max : null;
                    }
                }
            }
        }
        return $reservationArr;
    }

    private function getNearBySlotsByAllSlots($all_slots, $reservation_date, $restaurant_id, $slot_availability, $time_interval, $floor_id, $table_id, $reservation_id, $source, $tat, $party_size, $warning_message)
    {
        $available_block = array();
        if(isset($all_slots) && !empty($all_slots)){
            foreach ($all_slots as $slot){
                $start_date_time = Carbon::parse($reservation_date . ' ' . $slot . ':00');
                $start_time = Carbon::parse($start_date_time)->format('H:i');
                $start_date = Carbon::parse($start_date_time)->format('Y-m-d');

                $end_date_time = Carbon::parse($reservation_date . ' ' . $slot . ':00')->addMinutes($tat);
                $end_time = Carbon::parse($end_date_time)->format('H:i');
                $end_date = Carbon::parse($end_date_time)->format('Y-m-d');

                if ($reservation_date == $start_date && $reservation_date == $end_date) {
                    if(count($available_block)<2){
                        $response = $this->checkReservationSlotsAvailableForGivenSlot($start_time, $end_time, $party_size, $time_interval, $reservation_date, $restaurant_id, $slot_availability, $floor_id, $table_id, $reservation_id, $source, $tat, $warning_message);
                        if ($response['success']) {
                            $available_block[] = $slot;
                            $available_block = array_unique($available_block);
                        }
                    }
                }
            }
        }
        return $available_block;
    }

    public function getMyReservation(Request $request)
    {
        try {
            $ms = microtime(true);
            $restaurant_id = $request->header('X-location');
            $parent_restaurant_id = $request->header('X-restaurant');
            $success_msg = ('Request processed successfully.');
            $bearerToken = $request->bearerToken();
            if ($bearerToken) {
                $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
                if ($userAuth) {
                    $userId = $userAuth->user_id;
                    \Log::info($userId."***".$parent_restaurant_id);
                    $recent_reservation = Reservation::where('parent_restaurant_id', $parent_restaurant_id)->where('user_id', $userId)->where('end_time', '>', Carbon::now())->get();
                    $recent_reservation = $this->getModifiedReservationsForListing($restaurant_id, $recent_reservation);
                    $past_reservation = Reservation::where('parent_restaurant_id', $parent_restaurant_id)->where('user_id', $userId)->where('end_time', '<', Carbon::now())->get();
                    $past_reservation = $this->getModifiedReservationsForListing($restaurant_id, $past_reservation);

                    $me = microtime(true) - $ms;
                    return response()->json(['data' => ['recent_reservation' => $recent_reservation, 'past_reservation'=>$past_reservation], 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => 'access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
                }
            }
        }catch (\Exception $e){
            $me = microtime(true) - $ms;
            // Failure response
            return response()->json(['error' => Config('constants.status_code.BAD_REQUEST'), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
        }
    }

    private function getModifiedReservationsForListing($restaurant_id, $reservations)
    {
        foreach ($reservations as $reservation){
            $this->getModifiedReservationDetail($reservation, $restaurant_id);
        }
        return $reservations;
    }

    private function getModifiedReservationDetail($reservation, $restaurant_id)
    {
        if(($reservation->status)){
            $reservation->waiting_icon =  $reservation->status->slug == config('reservation.status_slug.waitlist')? $reservation->status->slug : null;
        }
        if(isset($reservation->user) && isset($reservation->user->category)){
            unset($reservation->user->stripe_customer_id);
            $reservation->user_category_icon = strtolower($reservation->user->category);
        }
        $reservation->reservation_start_time = ($reservation->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->format('h:i A'): '';
        $reservation->reservation_start_date = ($reservation->start_time)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time)->format('m-d-Y'): '';
        $reservation->total_amount = round(($reservation->total_charged_amount - $reservation->total_refund_amount), 2);
        $reservation->created_at = ($reservation->created_at)? CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->created_at)->format('Y-m-d h:i:s'): '';
        $reservation->full_name = empty($reservation->fname) && empty($reservation->lname)? $name = 'Walk In' : $reservation->fname . ' ' . $reservation->lname;
        if($reservation->status){
            $status_color = ($reservation->status->color) ?json_decode($reservation->status->color, true):null;
            $reservation->status_color = isset($status_color['sidebar'])?$status_color['sidebar']:'';
            $status_icon = ($reservation->status->icon) ?json_decode($reservation->status->icon, true):null;
            $reservation->status_icon = isset($status_icon['sidebar'])?$status_icon['sidebar']:'';
            if($reservation->source == config('reservation.source.online')){
                $reservation->source_icon = 'icon-online';
            }elseif($reservation->source == config('reservation.source.offline')){
                $reservation->source_icon = 'fa fa-phone';
            }else{
                $reservation->source_icon = 'icon-waiting-source';
            }
        }
        $reservation->payment_status = isset($reservation->payment_status) && !empty($reservation->payment_status)? ucfirst($reservation->payment_status==config('reservation.payment_status.charged')?'hold':$reservation->payment_status) : 'N/A';
        unset($reservation->stripe_charge_id);
        unset($reservation->stripe_card_id);
        return $reservation;
    }

    public function checkPaymentLink(Request $request)
    {
        try {
            $ms = microtime(true);
            $restaurant_id = $request->header('X-location');
            $token = $token_encrypted = $request->get('token');
            if ($token) {
                $token = base64_decode($token);
                $token = explode('#', $token);
                if (empty($token) || count($token) < 2) {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => 'Payment link has been expired.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
                $token_created_time = $token[0];
                $reservation_id = $token[1];
                if (!empty($token_created_time) && !empty($reservation_id)) {
                    $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
                    $token_created_time = Carbon::createFromFormat("U", $token_created_time);
                    //\Log::info($token_created_time);
                    //\Log::info($localCurrentTime);
                    $timeDiff = $localCurrentTime->diffInMinutes($token_created_time);
                    //\Log::info($timeDiff);
                    if ($timeDiff > config('reservation.payment_link_expire_time')) {
                        $me = microtime(true) - $ms;
                        return response()->json(['data' => null, 'error' => 'Payment link has been expired.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    }
                }
                $cancelled_status_id = ReservationStatus::getCancelledStatusId();
                $reservation_detail = Reservation::where('restaurant_id', $restaurant_id)->where('source', config('reservation.source.offline'))->where('total_cancellation_charge', '>', 0)->where('start_time', '>', Carbon::now())->where('status_id', '!=', $cancelled_status_id)->whereNotNull('payment_link_token')->where('payment_status', config('reservation.payment_status.pending'))->where('id', $reservation_id)->first();
                if (!$reservation_detail || $reservation_detail->payment_link_token!=$token_encrypted) {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => 'Payment link has been expired.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
            }
            $success_msg = ('Request processed successfully.');
            $me = microtime(true) - $ms;
            $reservation_detail = $this->getModifiedReservationDetail($reservation_detail, $restaurant_id);
            return response()->json(['data' => ['reservation_detail' => $reservation_detail], 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } catch (\Exception $e) {
            $me = microtime(true) - $ms;
            //Failure response
            return response()->json(['error' => Config('constants.status_code.BAD_REQUEST'), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
        }
    }

    public function reservationPayment(Request $request)
    {
        $ms = microtime(true);
        try {
            $restaurant_id = $request->header('X-location');
            $parent_restaurant_id = $request->header('X-restaurant');
            $cardInfo = $request->input('card_info');
            $cardInfoData = json_decode(base64_decode($cardInfo));
            $transaction_id = $cardNum = $cardType = "";

            $bearerToken = $request->bearerToken();
            if ($bearerToken) {
                $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
                $tokenField = 'Access';
            } else {
                $token = explode(' ', $request->header('Authorization'))[1];
                $userAuth = UserAuth::where(['guest_token' => $token])->first();
                $tokenField = 'Guest';
            }
            if (!$userAuth) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
            }
            $cancelled_status_id = ReservationStatus::getCancelledStatusId();
            $reservation_id = $request->get('reservation_id');
            $reservation_detail = Reservation::where('restaurant_id', $restaurant_id)->where('source', config('reservation.source.offline'))->where('total_cancellation_charge', '>', 0)->where('start_time', '>', Carbon::now())->where('status_id', '!=', $cancelled_status_id)->whereNotNull('payment_link_token')->where('payment_status', config('reservation.payment_status.pending'))->where('id', $reservation_id)->first();
            //\Log::info($reservation_detail);
            if (!$reservation_detail) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'Payment link has been expired.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }

            $restInfo = CommonFunctions::getRestaurantDetails($parent_restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name', 'custom_from', 'support_from'));
            //\Log::info($restInfo);
            $tot_amount = $reservation_detail->total_cancellation_charge;
            $customerName = $reservation_detail->fname . ' ' . $reservation_detail->lname;
            $customerEmail = $reservation_detail->email;

            $data = [
                'description' => $customerName,
                'email' => $customerEmail
            ];

            $apiObj = new ApiPayment();
            $result = $apiObj->createCustomer($data);
            $userPayCustId = $result->id;
            $cardNumber = str_replace(' ', '', $cardInfoData->card);

            $cardData = [
                'number' => $cardNumber,
                'exp_month' => $cardInfoData->expiry_month,
                'exp_year' => $cardInfoData->expiry_year,
                'cvc' => $cardInfoData->cvv,
                'name' => $cardInfoData->name,
            ];
            $result2 = $apiObj->createCustomerCard($userPayCustId, $cardData);
            \Log::info($result2['errorMesg']);
            if (isset($result2['errorMesg'])) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            } else {
                $stripeCardId = $result2['id'];
            }

            $data = [
                'customer' => $userPayCustId,
                'card_id' => $stripeCardId,
                'amount' => $tot_amount,
                'metadata' => 'ReservationID:' . $reservation_detail->receipt_no,
                'description' => 'MunchAdo-(' . $restInfo['restaurant_name'] . ')-' . $reservation_detail->restaurant_name,
            ];
            //print_r($data); die;
            try {
                $chargeResponse = $apiObj->captureCharge($data);
            } catch (\Exception $e) {
                $me = microtime(true) - $ms;
                \Log::info($e);
                return response()->json(['error' => $e->getMessage(), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
            }

            $transaction_id = $chargeResponse->id;
            $cardNum = $chargeResponse->source->last4;
            $cardType = $chargeResponse->source->brand;
            //first time user save card

            // code end
            $reservation_detail->total_charged_amount = $tot_amount;
            $reservation_detail->stripe_charge_id = $transaction_id;
            $reservation_detail->stripe_card_id = $stripeCardId;
            $reservation_detail->card_number = 'XXXX-XXXX-XXXX-' . $cardNum;
            $reservation_detail->card_type = $cardType;
            $reservation_detail->payment_gatway = config('reservation.payment_gatway.stripe');
            $reservation_detail->payment_status = config('reservation.payment_status.charged');
            $reservation_detail->payment_link_token = NULL;
            $reservation_detail->save();
            try {
                \Log::info('Table Reservation event CALLED');
                ReservationFunction::sendReservationNotification($parent_restaurant_id, $reservation_detail, $by = 'Customer');
            } catch (\Exception $e) {
                \Log::info('Notification not sent');
                \Log::info($e);
            }
            $me = microtime(true) - $ms;
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Payment received for this reservation.', 'error' => null, 'xtime' => $me], 200);
        } catch (\Exception $e) {
            \Log::info($e);
            $me = microtime(true) - $ms;
            return response()->json(['error' => Config('constants.status_code.BAD_REQUEST'), 'xtime' => $me], Config('constants.status_code.INTERNAL_ERROR'));
        }
    }

}