<?php

namespace App\Http\Controllers;

use App\Models\UserAuth;
use Illuminate\Http\Request;
use App\Helpers\CommonFunctions;
use App\Models\Notifications;
use App\Models\UserOrder;

class NotificationController extends Controller {
    #header[Authorization,X-restaurant,X-localization]
    #Required data [type(int Ex: order-1,reservation-3),user_id
    #orderType(Takeout,Delivery),recieptNo,currentDate(2018-08-24 06:15),
    #restaurantName,orderId,orderStatus(int 0,1,2,3),username,notificationfor(dashboard,user,cms,web)]#
    #Below notification only written for Dashboard#

    public function sendNotification(Request $request){
      
       $ms = microtime(true);

       $bearerToken = $request->bearerToken();
        $error = null;
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            
            $restaurantId = $request->header('X-location');
            $langId = config('app.language')['id'];             
            $orderType = $request->input('orderType');
            $recieptNo = $request->input('recieptNo');
            $type = $request->input('type');
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurantId));
            $currentDateTime =  $currentDate->format('Y-m-d h:i');
            $restaurantName = $request->input('restaurantName');
            $orderId = $request->input('orderId');
            $userOrder = UserOrder::where(['id' => $orderId])->first();          
            #PE-2932
            if($userOrder && $userOrder->product_type == 'food_item') {           
                $orderStatus = $request->input('orderStatus');
                $username = $request->input('username');
                $userId = $request->input('user_id');
                //'msg_time'=>$msgTime,//Today, 24 Aug
                $notificationMsg = "You have a new ".$orderType." order. Receipt number: ".$recieptNo." Way to go!";

                $channel = "dashboard_".$restaurantId;
                if($_SERVER['HTTP_HOST'] == 'dev-api.bravvurashowcase.com') {
                    $channel = "dev_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST'] == 'qc-api.bravvurashowcase.com') {
                    $channel = "qc_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST'] == 'api-qc-main.munchadoshowcase.biz') {
                    $channel = "qc_main_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST'] == 'api-qc-core.munchadoshowcase.biz') {
                    $channel = "qc_core_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST'] == 'betaapi.munchado.biz') {
                    $channel = "beta_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST']=="dev-nkdapi.munchadoshowcase.biz"){
                    $channel = "dev_dashboard_".$restaurantId;      
                }elseif($_SERVER['HTTP_HOST']=="api-nkd.munchadoshowcase.biz"){
                     $channel = "nkd_dashboard_".$restaurantId;      
                }
                elseif($_SERVER['HTTP_HOST']=="maproject.munchadoshowcase.biz"){
                     $channel = "dev_dashboard_".$restaurantId;      
                }
                $channel1 = $request->input('channel');
                $channel = (isset($channel1) && $channel1!='')?$channel1:"dev_dashboard_9";
                $notificationArray = array(
                    "msg" => $notificationMsg,
                    "channel" => $channel,            
                    "type" => $type,
                    "restaurant_id" => $restaurantId,
                    'curDate' => $currentDateTime,//2018-08-24 06:15
                    'restaurant_name' => $restaurantName,
                    'order_id' => $orderId,            
                    'order_status'=>$orderStatus,
                    'username'=>$username,
                    'is_friend'=>0
                );
                $r = CommonFunctions::pubnubPushNotification($notificationArray);
                if(!$r[0]){
                    $me = microtime(true) - $ms;                
                    return response()->json(['data' => array("fail"), 'error' => "Notification not sent", 'xtime' => $me ]);
                }
                $error = null;
                Notifications::create([
                    'restaurant_id' => $restaurantId,
                    'user_id' => $userId,
                    'notification_msg' => $notificationMsg,
                    'type' => $type,
                    'read_status' => 1,
                    'language_id' => $langId,
                    'channel' => $channel,
                    'status' => 1,
                    'pubnub_info' => json_encode($notificationArray),
                    'cronUpdate' => 0,                
                ]);
            }else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => array("fail"), 'error' => "Notification allowed for food items only.", 'xtime' => $me]);
            }
            
            $me = microtime(true) - $ms;
            return response()->json(['data' => array("success"), 'error' => $error, 'xtime' => $me,'pubnub_result'=>$r]);   
        }else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
}
