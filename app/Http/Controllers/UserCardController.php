<?php

namespace App\Http\Controllers;

use App\Helpers\ApiPayment;
use App\Models\User;
use App\Models\UserAuth;
use App\Models\UserCards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Payment\StripeGateway;
use App\Helpers\Payment\BraintressGateway;
use App\Models\PaymentGateway;
use App\Models\PaymentGatewayLog;
use App\Models\UserPaymentToken;

class UserCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['showAllUserCards', 'create']]);
        // use Illuminate\Support\Facades\Auth;
        // $userId = Auth::guard()->user();
    }

    /**
     * Function to show all User's address,
     * Registered to a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllUserCards(Request $request)
    {
        $ms          = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        }
        if ($userAuth) {
            $userId    = $userAuth->user_id;
            $restaurantId = $request->header('X-restaurant');
	    $locationId   = $request->header('X-location');	 
            $user      = User::where(['id' => $userId, 'restaurant_id' => $restaurantId])->first();
            //$userCards = $user->card ?? [];
	    $userCards=UserCards::where(['user_id' => $userId,'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$restaurantId])->get()->toArray(); 
            if($userCards) {
                $me        = microtime(true) - $ms;

                return response()->json(['data' => $userCards, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $me        = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => 'No cards found for User.', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Function to get show one particular User's address
     * Registered to a restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneUserCard($userId, $id)
    {
        $ms       = microtime(true);
        $user     = User::find($userId);
        $userCard = [];
        if ($user) {
            $error    = '';
            $userCard = $user->card()->where('id', $id)->first() ?? [];
            if (!$userCard) {
                $error = 'User card not found.';
            }
        } else {
            $error = 'User not found.';
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userCard, 'error' => $error, 'xtime' => $me], 200);
    }

    /**
     * Create User's address, for a particular restaurant, after signup
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $ms = microtime(true);
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $bearerToken = $request->bearerToken();
        $userAuth = array();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken, 'parent_restaurant_id' => $restaurantId])->first();
        }
        if ($userAuth) {
            $validation = Validator::make($request->all(), [
                'name'         => 'regex:/[a-zA-Z\s]+/|max:255',
                'card'         => 'required|regex:/[0-9\s]+/|max:20',
                'type'         => 'regex:/[a-zA-Z\s]+/|max:255',
                'expiry_month' => 'required|numeric|between:1,12',
                'expiry_year'  => 'required|numeric',
            ]);
            if (!$validation->fails()) {
                $userInfo = User::find($userAuth->user_id);
                $card = $cardType = "";

                $card = str_replace(' ', '', $request->input('card'));
                $cardType = $request->input('type');
	        $name = $request->input('name');
		$paymentAccountv= UserPaymentToken::where(['user_id' => $userAuth->user_id,'restaurant_id' =>$restaurantId])->get()->toArray();
                $cardData1 = UserCards::where(['name' => $name,'user_id' => $userAuth->user_id, 'last_four' => substr($card, -4), 'type' => $cardType,'restaurant_id' =>$restaurantId])->first();
		//print_r($cardData1);print_r($paymentAccountv);die;
                if(empty($cardData1)){
                    if (!isset($paymentAccountv[0])) {
                        // STRIPE - create customer
                        $data = [
                            'description' => ucwords($userInfo['fname'] . ' ' . $userInfo['lname']),
                            'email' => $userInfo['email'],
                            //'source' => 'tok_visa',
                        ];
                         
			$paymentGatewayConfig = $this->getPaymentGetwayInfo($restaurantId);
			 
			if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="stripe"){
				$apiObj = new StripeGateway($paymentGatewayConfig); 
				$result = $apiObj->createCustomer($data);
				if ($result) {
				    $stripeCustomerId         = $result->id;
				    //$user                     = User::find($userInfo['id']);
				    //$userInfo->stripe_customer_id = $stripeCustomerId;
				    //$userInfo->save();
				    $userToken = UserPaymentToken::create([
					    'restaurant_id' =>$restaurantId,
					    'parent_restaurant_id'=>  $restaurantId,
					    'user_id'  => $userAuth->user_id,
					    'token_id' => $stripeCustomerId,
					    'card_id'=> $stripeCustomerId
					]);	
				} 
			} 
                    } else {
                        $stripeCustomerId = $paymentAccountv[0]['token_id'];//$userInfo['stripe_customer_id'];
                    }

                    $userId = $userInfo['id'];

                    $card = str_replace(' ', '', $request->input('card'));
                    $is_default_card = 0;
                    if($request->has('is_default_card') && $request->input('is_default_card')) {
                        $is_default_card = 1;
                        //Set old cards to default = 0
                       UserCards::where('user_id', '=', $userAuth->user_id)->update(array('is_default_card' => 0));
                    }
                    $data = [
                        'user_id' => $userId,
                        'name' => $request->input('name'),
                        'restaurant_id' =>$restaurantId,
			'parent_restaurant_id'=>  $restaurantId,
                        'last_four' => substr($card, -4),
                        'type' => $request->input('type') ?? 'visa',
                        'expiry'    => $request->input('expiry'),
                        'zipcode' => $request->input('zipcode'),
                        'status' => 1,
                        'is_default_card' => $is_default_card
                    ];
		    $cardData = [
                        'number' => $card,
                        'exp_month' => $request->input('expiry_month'),
                        'exp_year' => $request->input('expiry_year'), 
                        'cvc'       => $request->input('cvv'),
                        'name'       => $request->input('name'),
                    ]; 	
                    // STRIPE - add user card
                   
		    $paymentGatewayConfig = $this->getPaymentGetwayInfo($restaurantId);
		    if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="stripe"){
				$apiObj = new StripeGateway($paymentGatewayConfig); 
				$cardInfo = $apiObj->createCustomerCard($stripeCustomerId, $cardData);
		    } 
                    if(isset($cardInfo['errorMesg'])){
                        $me = microtime(true) - $ms;
                        return response()->json(['data' => null, 'error' => $cardInfo['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    } else {
                        $stripeCardId = $cardInfo['id'];
                        //return response()->json([$userId,$userStripeId,$stripeCardId]);
                        $userCard = UserCards::create($data);
                        $userCard = UserCards::find($userCard->id);
                        $userCard->stripe_card_id = $stripeCardId;
                        $userCard->save();

                        $me = microtime(true) - $ms;
                        return response()->json(['data' => $userCard, 'error' => '', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                    } 
                } else {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => 'Card alreday exist.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }

            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
           
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Delete User's address for a particular restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $ms = microtime(true);
        UserCards::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data' => 'You have successfully removed your card.', 'error' => '', 'xtime' => $me], 200);
    }

    /**
     * Update an address of a user, for a particular restaurant
     * @param         $id
     * @param         $addId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($userId, $id, Request $request)
    {
        $ms = microtime(true);

        $validation = Validator::make($request->all(), [
            'user_id'      => 'required|exists:users,id',
            'name'         => 'regex:/[a-zA-Z\s]+/|max:255',
            'card'         => 'required|regex:/[0-9\s]+/|max:20',
            'type'         => 'regex:/[a-zA-Z\s]+/|max:255',
            'expiry_month' => 'required|numeric',
            'expiry_year'  => 'required|numeric',
            'zipcode'      => 'required|in:billing,shipping',
        ]);
        if (!$validation->fails()) {
            $data = [
                'user_id' => $request->input('user_id'),
                'name'    => $request->input('name'),
                'card'    => $request->input('card'),
                'type'    => $request->input('type') ?? '',
                'expiry'  => $request->input('expiry_month') . '/' . $request->input('expiry_year'),
                'zipcode' => $request->input('zipcode'),
                'status'  => 1,
            ];

            /*$user = UserCards::findOrFail($id);
            $user->update($data);*/
            $me = microtime(true) - $ms;

            return response()->json(['data' => $data, 'error' => '', 'xtime' => $me], 200);
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
        }
    }
    private function getPaymentGetwayInfo($restaurant_id){
        $paymentGatewayConfig = null;
        $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("status", 1)->get()->toArray();
        //print_r($paymentGatewayInfo);die;
        if(isset($paymentGatewayInfo[0])) {
            $paymentGatewayConfig['ID']=$paymentGatewayInfo[0]['id'];
            $config = json_decode($paymentGatewayInfo[0]['config']);
            $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
            foreach ($config as $val1) {
                foreach ($val1 as $key => $val) {
                    $paymentGatewayConfig[$key] = $val;
                }
            }
        }else{
            $paymentGatewayConfig['ID'] = 0;
            $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
            $paymentGatewayConfig['GATEWAY'] = "stripe";
        }
        return $paymentGatewayConfig;
    }	

}
