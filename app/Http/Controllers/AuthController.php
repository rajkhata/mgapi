<?php

namespace App\Http\Controllers;



use DB;
use App\Helpers\ApiPayment;
use App\Helpers\CommonFunctions;
use App\Models\MenuCategories;
use App\Models\User;
use App\Models\CmsUser;
use App\Models\CmsAuth;
use App\Models\UserAuth;
use App\Models\UserMyBag;
use App\Models\UserSocial;
use Carbon\Carbon;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\MybagOptions;
use App\Models\City;
use App\Helpers\Payment\StripeGateway;
use App\Helpers\Payment\BraintressGateway;
use App\Models\PaymentGateway;
use App\Models\PaymentGatewayLog;
use App\Models\UserPaymentToken;
use App\Helpers\Netcore;
use App\Models\MailTemplate;
use App\Models\UserAddresses;
//use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends Controller {
    /**
     * @var Client|null
     */
    protected $client = null;
    /**
     * @var CommunicatorConfiguration
     */
    protected $communicatorConfiguration;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'guest', 'register', 'verifyOtp', 'forgot', 'resetpwd', 'social','checkSocial', 'opa_login', 'opa_token', 'opa_checkupdate', 'opa_logout']]);
    }
	
    public function guest(Request $request) {
        $restaurantId = $request->header('X-restaurant');
        // CHECK IF DEVICE ID & TOKEN exists
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $langId = $language_id = $language_id->id;
        $ms = microtime(true);
        $error = '';
        $status = 200;
        $platformData = [];

        // input data
        $deviceIpAdd = $request->getClientIp() ?? $request->ip();
        $platform = strtolower($request->input('platform')) ?? 'web';
        $deviceId = $request->input('device_id') ?? '';
        $deviceToken = $request->input('device_token') ?? '';

        // generate a guest token
        $token = sha1(uniqid(mt_rand(), true));
        // save device token
        if ($platform == 'android' || $platform == 'ios') {
            $platformData = [
                'platform' => $platform,
                'device_id' => $deviceId,
                'device_token' => $deviceToken,
            ];
        }

        $data = [
            'user_id' => 0,
            'user_details' => json_encode(['ip_address' => $deviceIpAdd]),
            'guest_token' => $token,
            'platform' => count($platformData) ? json_encode([$platformData]) : null,
            'language_id' => $langId,
            'parent_restaurant_id' => $restaurantId
        ];
        UserAuth::create($data);
        $result = ['guest_token' => $token];
        $me = microtime(true) - $ms;

        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Login a user and get JWT access token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $ms = microtime(true);

        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $result       = null;
        $error        = 'Invalid email and/or password.';
        $status       = Config('constants.status_code.BAD_REQUEST');

        // check if guest token exists
        $authHeader = $request->header('Authorization');
        if (!empty($authHeader)) {
            $guestToken = explode(' ', $authHeader)[1];
        }
        $userAuth = UserAuth::where(['guest_token' => $guestToken])->where('parent_restaurant_id', $restaurantId)->first();        
        if ($userAuth) {
            $email = $request->input('email');
            if (filter_var($email, FILTER_VALIDATE_EMAIL) || strpos($email, '@') !== false) {
                $credentials = $request->all(['email', 'password']);
            } else {
                $credentials['mobile']   = $email;
                $credentials['password'] = $request->input('password');
            }
            $credentials['restaurant_id'] = $restaurantId;
            # user is being registered for Parent rest By RG
            //$credentials['restaurant_id'] = $locationId;
            $credentials['status']        = 1;
            // generate token for the user
            if ($token = $this->guard()->attempt($credentials)) {
                $error  = null;
                $status = Config('constants.status_code.STATUS_SUCCESS');
                $result = $this->respondWithToken($token);
                if (count($result)) {
                    $userInfo               = $this->guard()->user();
                    $result['user']         = $userInfo;
                    $result['user']['show_change_password_option'] = true;
                    $userAuth->access_token = $result['access_token'];
                    $userAuth->user_id      = $userInfo->id;
                    $userAuth->ttl          = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                    $userAuth->save();
                    unset($result['expires_in']);
                }
            }
        } else {
            $result = null;
            $error  = 'Guest token is not valid.';
            $status = Config('constants.status_code.UNAUTHORIZED_REQUEST');
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $ms = microtime(true);
        // check if guest token exists
        $oldAccessToken = $request->bearerToken();
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');

        $userAuth = UserAuth::where(['access_token' => $oldAccessToken])->where('parent_restaurant_id', $restaurantId)->first();
        $result   = null;
        $error    = null;
        if ($userAuth) {            
            // empty cart
            $mybagID = UserMyBag::where(['user_auth_id' => $userAuth->id, 'restaurant_id' => $locationId])->first();

            if(isset($mybagID->id) and $mybagID->id != ''){
                MybagOptions::where(['bag_id' => $mybagID->id])->delete();

                UserMyBag::where(['user_auth_id' => $userAuth->id, 'restaurant_id' => $locationId])->delete();
            }
            // logout user
            $this->guard()->logout();
            $userAuth->user_id      = 0;
		$userAuth->email      = null;
            $userAuth->access_token = null;
            $userAuth->ttl          = null;
            $userAuth->save();

            // guest token
            $result = ['guest_token' => $userAuth->guest_token];
            $status = Config('constants.status_code.STATUS_SUCCESS');
        } else {
            $error  = 'Access token not valid.';
            $status = Config('constants.status_code.UNAUTHORIZED_REQUEST');
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     * Note * Not in use @03-08-2018 by RG
     */
    public function refresh(Request $request) {
        $ms = microtime(true);
        // check if guest token exists
        $oldAccessToken = $request->bearerToken();

        // need to save the new access token
        $newTokenArr = $this->respondWithToken($this->guard()->refresh());
        $result = null;
        $error = null;
        if ($newTokenArr) {
            $userAuth = UserAuth::where(['access_token' => $oldAccessToken])->first();
            $userAuth->access_token = $newTokenArr['access_token'];
            $userAuth->ttl = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
            $userAuth->save();
            $result = $newTokenArr;
            $status = 200;
        } else {
            $error = 'Unable to generate token, please try again.';
            $status = 400;
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        // response()->json()
        // JWTAuth::setTTL(60);
        return [
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => $this->guard()->factory()->getTTL(),// * config('constants.session_expiry'),
        ];
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard() {
        return Auth::guard();
    }

    public function register(Request $request) {
        $ms     = microtime(true);
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
        // input data
        $deviceIpAdd  = $request->getClientIp() ?? $request->ip();
        $platform     = strtolower($request->input('platform')) ?? 'web';
        $deviceId     = $request->input('device_id') ?? '';
        $deviceToken  = $request->input('device_token') ?? '';
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','source_url'));

        // check guest token
        $userAuth   = '';
        $authHeader = $request->header('Authorization');
        
        if (!empty($authHeader)) {
            $guestToken = explode(' ', $authHeader);           
            $userAuth   = UserAuth::where(['guest_token' => $guestToken[1]])->where('parent_restaurant_id', $restaurantId)->first();
        }
       
        if ($userAuth) {

            $inputData                  = $request->all();
            $inputData['restaurant_id'] = $restaurantId;

            $validation = Validator::make($inputData, [
                'restaurant_id' => 'required|exists:restaurants,id',
                'fname'         => 'required|string|max:255',
                //'lname'         => 'string|max:255',
                // 'email'         => 'required|string|email|max:255',
                'password'      => 'required|min:8|max:64',
                'email' => [
                    'required',
                    'email',
                    'string',
                    'max:255',
                    Rule::unique('users')->where(function ($query) use ($restaurantId) {
                        $query->where('restaurant_id', $restaurantId)->where('is_registered', 1);
                    }),
                ],
            ]);
	    
         
            if (!$validation->fails()) {
		$user = User::where('restaurant_id', $restaurantId)->where('is_registered', 0)->where('email', $request->input('email'))->first();
		$ref_restaurantId = 0;
		$code = $request->input('promocode');
		if(isset($code)){
			$resultsReferral = DB::select('select * from restaurant_referral_promotions where referral_code = :code', ['code' => $code]);
			if(isset($resultsReferral[0])){
				$ref_restaurantId =  $resultsReferral[0]->restaurant_id;
			}else{
			    $code = null;  
			}
	        }
		$surname = $request->input('surname');
                // registration data
		$udata = [
		            'restaurant_id' => $restaurantId,
		            'fname'         => $request->input('fname'),
			    'username'      => $request->input('fname') ." ".$request->input('lname') ,
			    'surname'	    => $surname,
		            'lname'         => $request->input('lname'),
		            'email'         => $request->input('email'),
		            'password'      => Hash::make($request->input('password')),
		            'status'        => 1,
		            'user_group_id' => 1,
		            'language_id'   => $langId,
			    'referral_code' => $code,
			    'referral_restaurant_id' => $ref_restaurantId,
		        ];
		
		if(!isset($user->id)){
		        $user = User::create($udata);
		}else{
		   $udata['is_registered'] = 1;
		   DB::table('users')->where('email',$request->input('email'))->update($udata);	
		   //$del_users_add = UserAddresses::where(['user_id' => $user->id])->delete();

		}	
                // STRIPE - create customer
                $data   = [
                    'description' => ucwords($user->fname . ' ' . $user->lname),
                    'email'       => $user->email,
                    #'source'      => 'tok_visa',
                ];

                
		$paymentGatewayConfig = $this->getPaymentGetwayInfo($restaurantId);
		//print_r($paymentGatewayConfig);
		if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="stripe"){
			$apiObj = new StripeGateway($paymentGatewayConfig);
		        $result = $apiObj->createCustomer($data);
		        if ($result) {
		            $stripeCustomerId         = $result->id;
		            /*$user                     = User::find($user->id);
		            $user->stripe_customer_id = $stripeCustomerId;
		            $user->save();*/
			    $userToken = UserPaymentToken::create([
					    'restaurant_id' =>$restaurantId,
					    'parent_restaurant_id'=>  $restaurantId,
					    'user_id'  => $user->id,
					    'token_id' => $stripeCustomerId,
					    'card_id'=> $stripeCustomerId
					]);	
		        } 
		} 

                // generate token for the user
                if ($token = $this->guard()->login($user)) {
                    $result         = $this->respondWithToken($token);
                    $userInfo       = $this->guard()->user();
                    $result['user'] = $user;
                    $result['user']['show_change_password_option'] = true;
                    unset($result['expires_in']);
                    $userAuth->access_token = $result['access_token'];
                    $userAuth->user_id      = $userInfo->id;
                    $userAuth->ttl          = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                    $userAuth->save();
                }
		$email = $request->input('email');
                $fname  = $request->input('fname');
                self::sendRegistrationEmail($locationId,$restaurantId,$email,$fname,$langId);
		$nt=null;
		 	
                $me = microtime(true) - $ms;

                return response()->json(['data' => $result,'nt'=>$nt, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_CREATED'));
	      
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Guest token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
     public static function sendRegistrationEmail($locationId,$restaurantId,$email,$fname,$langId){
		$restaurant_data = Restaurant::where('id', $locationId)->select('id', 'email', 'restaurant_name', 'source_url','slug')->first();

                // NKD-148 - SITE URL -> View Menu on emails
                // SITE_URL/store/<branch_slug>/menu/<any_category_slug>
                $categorySlug = MenuCategories::select('slug')->where('restaurant_id', $locationId)->first();
                $siteViewMenuUrl = $restaurant_data->source_url . '/store/'.$restaurant_data->slug.'/menu/'.$categorySlug->slug;

                //send email
                CommonFunctions::$langId = $langId;
                $mailKeywords            = array(
                    'SITE_URL'     => $siteViewMenuUrl, // $restaurant_data->source_url,
                    'LOGIN_URL'    => config('constants.login_url'),
                    'SITE_NAME'    => config('constants.site_name'),
                    'USER_NAME'    => $fname,
                    'REST_NAME' => $restaurant_data->restaurant_name,
                    'MAIL_CONTENT' => "Thanks",
                );

                //@14-08-2018 Chnage mail keywords
                $mailKeywords = array(
                    'header' => array(
                        'logo_url' => 'https://dev-keki.bravvurashowcase.com/mailer-images/keki/logo.jpg',
                        'about' => 'https://dev-keki.bravvurashowcase.com/', 
                        'home' => 'https://dev-keki.bravvurashowcase.com/',
                        'menu' => 'https://dev-keki.bravvurashowcase.com/',
                    ),
                    'footer' => array(

                    ),
                    'data' => array(
                        'SITE_URL'     => $siteViewMenuUrl, //$restInfo['source_url'],
                        'LOGIN_URL'    => config('constants.login_url'),
                        'SITE_NAME'    => config('constants.site_name'),
                        'USER_NAME'    => $fname,
                        'REST_NAME'    => $restaurant_data->restaurant_name,
                        'MAIL_CONTENT' => "Thanks",
                    )
                );
                $mailTemplate = CommonFunctions::restMailTemplate('registration', $restaurantId, $mailKeywords);
		$subject = MailTemplate::where(['template_name' => 'registration', 'language_id' => $langId])->whereIn('restaurant_id', array(0, $restaurantId))
		->orderBy('restaurant_id', 'DESC')->first();
		if($subject) {
		        $subject = $subject->toArray();
		        $mailData['subject']  =  $subject['subject'];
		}else $mailData['subject']        = trans("Welcome to ".$restaurant_data->restaurant_name."!");
                $mailData['body']           = $mailTemplate;
                $mailData['receiver_email'] = $email;
                $mailData['receiver_name']  = $fname;
                $mailData['MAIL_FROM_NAME'] =  $restaurant_data->restaurant_name;
                //$mailData['MAIL_FROM'] = $restInfo['custom_from'];

                CommonFunctions::sendMail($mailData);

                /*CommonFunctions::netcoreEvent(array(
                    'email'       => $request->input('email'),
                    'first_name'  => $request->input('fname'),
                    'mobile'      => '',
                    'last_name'   => $request->input('lname'),
                    'is_register' => 'yes',
		    'id'	=> $user->id,
                    'host_url'    => '',
                ));*/
     }	
     public function registerGuestUser(Request $request) {
        $restaurantId = $request->header('X-restaurant');
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $langId = $language_id->id;
        $ms = microtime(true);
        $isEmail = false;
        $email = $request->input('email');
	$password = $request->input('password');
	$locationId   = $request->header('X-location');

        if (filter_var($email, FILTER_VALIDATE_EMAIL) || strpos($email, '@') !== false) {
            $isEmail = true;
        }
        $fieldName = '';
        if ($isEmail) {
            $validation = Validator::make($request->all(), [
                        'email' => "required|string|email|max:255|exists:users,email,restaurant_id,$restaurantId,is_registered,1",
            ],[
                'email.exists' => 'The email address is not registered with us.'
            ]);
            $fieldName = 'email';
        } else {
            $validation = Validator::make($request->all(), [
                        'email' => "required|max:15|exists:users,mobile,restaurant_id,$restaurantId,is_registered,1",
            ],[
                'email.exists' => 'The mobile number is not registered with us.'
            ]);
            $fieldName = 'mobile';
        }
        if (!$validation->fails()) {
		$email = $request->input('email');
            	// generate otp
            	$otp = mt_rand(100000, 999999);
		$userinfo = User::select('id', 'fname', 'lname', 'mobile')->where(['email' => $email, 'language_id' => $langId, 'restaurant_id' => $restaurantId])->first()->toArray();
		DB::table('users')->where('email',$email)->where('is_registered',0)->update(['is_registered'=>1,'password',Hash::make($password)]);
		self::sendRegistrationEmail($locationId,$restaurantId,$email,$userinfo[0]['fname']);
	}
     }	

    /**
     * Forgot password, will sent OTP on email/sms
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request) {
        $restaurantId = $request->header('X-restaurant');
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
        $ms = microtime(true);
        $isEmail = false;
        $email = $request->input('email');

        if (filter_var($email, FILTER_VALIDATE_EMAIL) || strpos($email, '@') !== false) {
            $isEmail = true;
        }
        $fieldName = '';
        if ($isEmail) {
            $validation = Validator::make($request->all(), [
                        'email' => "required|string|email|max:255|exists:users,email,restaurant_id,$restaurantId,is_registered,1",
            ],[
                'email.exists' => 'The email address is not registered with us.'
            ]);
            $fieldName = 'email';
        } else {
            $validation = Validator::make($request->all(), [
                        'email' => "required|max:15|exists:users,mobile,restaurant_id,$restaurantId,is_registered,1",
            ],[
                'email.exists' => 'The mobile number is not registered with us.'
            ]);
            $fieldName = 'mobile';
        }
        if (!$validation->fails()) {
            $email = $request->input('email');
            // generate otp
            $otp = mt_rand(100000, 999999);

            // ALTER TABLE  `user_auth` ADD  `otp` INT( 6 ) NULL AFTER  `platform` ;
            $guestToken = explode(' ', $request->header('Authorization'));
            $updateData = [
                'otp'      => $otp,
                $fieldName => $email,
            ];

           UserAuth::where(['guest_token' => $guestToken[1]])->where('parent_restaurant_id', $restaurantId)->update($updateData);
            // send EMAIL
            CommonFunctions::$langId = $langId;

            if ($isEmail) {
                $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name', 'restaurants.restaurant_name','custom_from','source_url'));
                $userinfo = User::select('id', 'fname', 'lname', 'mobile')->where(['email' => $email, 'language_id' => $langId, 'restaurant_id' => $restaurantId])->first()->toArray();
		//DB::table('users')->where('email',$email)->where('is_registered',0)->update(['is_registered'=>1]);
                //@14-08-2018 Chnage mail keywords
                $mailKeywords = array(
                    'header' => array(
                        'logo_url' => 'https://dev-keki.bravvurashowcase.com/mailer-images/keki/logo.jpg',
                        'about' => 'https://dev-keki.bravvurashowcase.com/', 
                        'home' => 'https://dev-keki.bravvurashowcase.com/',
                        'menu' => 'https://dev-keki.bravvurashowcase.com/',
                    ),
                    'footer' => array(

                    ),
                    'data' => array(
                        'SITE_URL'     => $restInfo['source_url'],
                        'LOGIN_URL'    => config('constants.login_url'),
                        'SITE_NAME'    => config('constants.site_name'),
                        'USER_NAME'    => $userinfo['fname'],
                        'PASSWORD'    => $otp,
                        'title' => "Forgot Password",
                        'REST_NAME'    => $restInfo['restaurant_name'],
                    )
                );
                $mailTemplate = CommonFunctions::restMailTemplate('forgot_password', $restaurantId, $mailKeywords);


                $mailData['subject'] = ucfirst($restInfo['restaurant_name'])." Password Recovery";//.ucfirst($restInfo['restaurant_name'])
                $mailData['body'] = $mailTemplate;
                $mailData['receiver_email'] = $email;
                $mailData['receiver_name'] = $userinfo['fname'];
                $mailData['MAIL_FROM_NAME'] =  $restInfo['restaurant_name'];
                $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                CommonFunctions::sendMail($mailData);
            } else {
                //send sms  
                //$userinfo = User::select('id', 'fname', 'lname','mobile')->where(['mobile' => $email])->get()->toArray()[0];
                
                // $message = CommonFunctions::getSMSContent("forgot_password")['0']['content']." ".$otp;
                // $smsData['user_mob_no'] = $email;
                // $smsData['message'] = $message;
                // CommonFunctions::sendSms($smsData);

                $insert_data = UserAuth::where(['guest_token' => $guestToken[1]])->where('parent_restaurant_id', $restaurantId)->orderBy('created_at', 'DESC')->first();

                if($insert_data) {
                    /******** SMS Send **** @28-11-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $insert_data->id,
                        'sms_module' => 'forgot_password',
                        'action' => 'forgot',
                        'sms_to' => 'customer',
                        'mobile_no' => array($email)
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                }
            }

            $me = microtime(true) - $ms;

            return response()->json(['data' => 1, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
    }

    /**
     * Verify OTP details
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyOtp(Request $request) {
        $restaurantId = $request->header('X-restaurant');
        $ms = microtime(true);
        $validation = Validator::make($request->all(), [
                    'otp' => 'required|numeric|digits:6',
        ]);

        if (!$validation->fails()) {
            $otp = $request->input('otp');
            $guestToken = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['otp' => $otp, 'guest_token' => $guestToken, 'parent_restaurant_id' => $restaurantId])->first();
            if ($userAuth) {
                $data = [
                    'otp' => $userAuth->otp,
                ];
                $me = microtime(true) - $ms;

                return response()->json(['data' => $data, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => ['otp' => ['OTP is not valid.']], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
    }

    /**
     * Reset password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetpwd(Request $request) {
        $restaurantId = $request->header('X-restaurant');
        $ms = microtime(true);
        $validation = Validator::make($request->all(), [
                    'password' => 'required|min:8|max:64',
        ]);

        if (!$validation->fails()) {
            $guestToken = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $guestToken])->first();
            if ($userAuth) {
                $email = $userAuth->email;
                $mobile = $userAuth->mobile;
                if ($email) {
                    $user = User::where('email', '=', $email)->where('restaurant_id', $restaurantId)->first();
                } else {
                    $user = User::where('mobile', '=', $mobile)->where('restaurant_id', $restaurantId)->first();
                }

                if ($user) {
                    // update user password
                    $password = Hash::make($request->input('password'));
                    $user->password = $password;
                    $user->save();

                    // remove OTP
                    UserAuth::where(['guest_token' => $guestToken, 'parent_restaurant_id' => $restaurantId])->update(['otp' => null]);
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => 1, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'User not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => ['otp' => ['OTP is not valid.']], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
    }


    /**
     * User's Social registration/login
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function social(Request $request) {
        $ms = microtime(true);
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        // Guest token
        $userAuth   = '';
        $authHeader = $request->header('Authorization');
        if (!empty($authHeader)) {
            $guestToken = explode(' ', $authHeader)[1];
            $userAuth   = UserAuth::where(['guest_token' => $guestToken, 'parent_restaurant_id' => $restaurantId])->first();
        }
        if ($userAuth) {
            //$langId = config('app.language')['id'];
	  $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
            // check if social and access_token already exists
            $checkUserSocial = UserSocial::where(['social' => $request->input('social'), 'parent_restaurant_id' => $restaurantId])->first();

            $email = $request->input('email');
            if (filter_var($email, FILTER_VALIDATE_EMAIL) || strpos($email, '@') !== false) {
                $key = 'email';
                $emailValidation = 'required|string|email|max:255';
            } else {
                $key = 'mobile';
                $emailValidation = 'required|numeric|exists:users';
            }
            $display_pic_url = '';
            if($request->has('profile_image') && $request->input('profile_image')) {
                //upload profile picture and save
                $display_pic_url = $this->file_upload($request->input('profile_image'));
            }
            if ($checkUserSocial) {
                // social credentials exists, so LOGIN
                $inputData                  = $request->all();
                $inputData['restaurant_id'] = $restaurantId;
                $inputData[$key] = $email;
                $validation = Validator::make($inputData, [
                    'restaurant_id' => 'required|exists:restaurants,id',
                     $key         => $emailValidation,
                    'social'        => 'required|max:255',
                    'access_token'  => 'required|max:255',
                    'user_source'   => 'required|in:fb,gp', //,tw,ws
                ]);

                if (!$validation->fails()) {

                    $userInfo = User::where([$key => $request->input('email'), 'restaurant_id' => $restaurantId])->first();

                    if ($userInfo) {
                        $userInfo->display_pic_url=$display_pic_url;
                        $userInfo->save();
                        $userSocial = UserSocial::where(['user_id' => $userInfo->id, 'social' => $request->input('social'), 'parent_restaurant_id' => $restaurantId])->first();
                        if ($userSocial) {
                            // generate token for the user
                            if ($token = $this->guard()->login($userInfo)) {
                                $userSocial->access_token = $request->input('access_token');
                                $userSocial->save();
                                $result = $this->respondWithToken($token);
                                $userInfo = $this->guard()->user();
                                $result['user'] = $userInfo; //User::find($userInfo->id);
                                $result['is_new_user'] = false;
                                $result['show_change_password_option'] = false;
                                unset($result['expires_in']);
                                $userAuth->email = $request->input('email');
                                $userAuth->access_token = $result['access_token'];
                                $userAuth->user_id = $userInfo->id;
                                $userAuth->ttl = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                                $userAuth->save();
                            }
                            $me = microtime(true) - $ms;
			    $code = $request->input('promocode');
				if(isset($code) && $code!=""){
					$resultsReferral = DB::select('select * from restaurant_referral_promotions where referral_code = :code', ['code' => $code]);
					if(isset($resultsReferral[0])){
					   $ref_restaurantId = $resultsReferral[0]->restaurant_id;
					   DB::table('users')->where('email',$request->input('email'))->update(['referral_code'=>$code,'referral_restaurant_id'=>$resultsReferral[0]->restaurant_id]);	
					} 
				}	
                            return response()->json(['data' => $result, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                        } else {
                            $me = microtime(true) - $ms;

                            return response()->json(['data' => null, 'error' => 'Social credential is not valid.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                        }
                    } else {
                        $me = microtime(true) - $ms;

                        return response()->json(['data' => null, 'error' => 'User not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    }

                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }

            } else {
                // social credentials don't exists, so SIGNUP

                // check if user data already exists (2nd social login)
                $user = User::where([$key => $email, 'restaurant_id' => $restaurantId])
                    ->leftJoin('user_social', 'user_social.user_id', '=', 'users.id')
                    ->select('users.*')
                    ->first();
               
                if (!$user) { 
                    // check unique, when we'll add as a new user
                    #$emailValidation = '|unique:users';
                    if($key == 'email') {
                        $emailValidation = [
                            'required',
                            'email',
                            'string',
                            'max:255',
                            Rule::unique('users')->where(function ($query) use ($restaurantId) {
                                $query->where('restaurant_id', $restaurantId);
                            }),
                        ];
                    }else {
                        $emailValidation = [
                            'required',
                            'numeric',
                            Rule::unique('users')->where(function ($query) use ($restaurantId) {
                                $query->where('restaurant_id', $restaurantId);
                            }),
                        ];   
                    }
                }
                $inputData                  = $request->all();
                $inputData['restaurant_id'] = $restaurantId;
                $inputData[$key] = $email;
                
                $validation = Validator::make($inputData, [
                    'restaurant_id' => 'required|exists:restaurants,id',
                    'fname'         => 'string|max:255',
                    'lname'         => 'string|max:255',
                     $key        =>  $emailValidation,                    
                    'phone'         => 'sometimes|nullable|max:15',
                    'dob'           => 'sometimes|nullable|date',
                    //'display_pic_url' => $local_url,
                    'social'        => 'required|max:255',
                    'access_token'  => 'required|max:255',
                    'user_source'   => 'required|in:fb,gp', //,tw,ws
                ]);
               
                if (!$validation->fails()) {

                    if (!$user) {
                        // user exists, just create social data
                        // user don't exists, create user
                        // registration data
                        $dobInput = $request->input('dob');
                        $dob      = null;
                        if (!empty($dobInput)) {
                            $dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
                        }
                        $user = User::create([
                            'restaurant_id' => $restaurantId,
                            'display_pic_url' => $display_pic_url,
                            'fname'         => $request->input('fname'),
                            'lname'         => $request->input('lname'),
                             $key          => $email,
                            'password'      => Hash::make(config('constants.user_default_password')),
                            'dob'           => $dob,
                            'status'        => 1,
                            'user_group_id' => 1,
                            'language_id' => $langId,
                        ]);
                        $is_new_user = true;
                    }else {
                        //@23-07-2018 ny RG No need to show profile setup option if user is already exists!
                        $is_new_user = false;
                    }
                    if ($user) {
                        UserSocial::create([
                            'user_id'      => $user->id,
                            'social'       => $request->input('social'),
                            'parent_restaurant_id'       => $restaurantId,
                            'access_token' => $request->input('access_token'),
                            'user_source'  => $request->input('user_source'),
                            'language_id'  => $langId
                        ]);
                        if($key == 'email') {
                            $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','source_url'));
            			    $mailKeywords            = array(
            		            'SITE_URL'     => $restInfo['source_url'],
            		            'LOGIN_URL'    => config('constants.login_url'),
            		            'SITE_NAME'    => config('constants.site_name'),
                                'REST_NAME'    => $restInfo['restaurant_name'],
            		            'USER_NAME'    => $request->input('fname'),
            		            'MAIL_CONTENT' => "Thanks",
            		        );
                            // @ 07-08-2018 by RG Get Outlet based rest Info : false as we need only outlet info if true then it will fetch the parent Rest Details
            		        $mailTemplate = CommonFunctions::mailTemplate('registration', $mailKeywords, 'header_layout', 'footer_layout',$restaurantId);

            		        $subject = MailTemplate::where(['template_name' => 'registration', 'language_id' => $langId])->whereIn('restaurant_id', array(0, $restaurantId))
                            ->orderBy('restaurant_id', 'DESC')->first();
                            if($subject) {
                                    $subject = $subject->toArray();
                                    $mailData['subject']  =  $subject['subject'];
                            }else $mailData['subject']        = trans("Welcome to {$restInfo['restaurant_name']}!");
            		        $mailData['body']           = $mailTemplate;
            		        $mailData['receiver_email'] = $request->input('email');
            		        $mailData['receiver_name']  = $request->input('fname');
                            $mailData['MAIL_FROM_NAME'] =  $restInfo['restaurant_name'];
                            $mailData['MAIL_FROM'] = $restInfo['custom_from'];

            		        CommonFunctions::sendMail($mailData);

            		        CommonFunctions::netcoreEvent(array(
            		            'email'       => $request->input('email'),
            		            'first_name'  => $request->input('fname'),
            		            'mobile'      => $request->input('mobile'),
            		            'last_name'   => $request->input('lname'),
            		            'is_register' => 'yes',
            		            'host_url'    => '',
            		        ));
                        }else {
                            //send sms to DO
                        }
                    }
		     
			        $code = $request->input('promocode');
				if(isset($code) && $code!=""){
					$resultsReferral = DB::select('select * from restaurant_referral_promotions where referral_code = :code', ['code' => $code]);
					if(isset($resultsReferral[0])){
					   $ref_restaurantId = $resultsReferral[0]->restaurant_id;
					   DB::table('users')->where('email',$request->input('email'))->update(['referral_code'=>$code,'referral_restaurant_id'=>$resultsReferral[0]->restaurant_id]);	
					} 
				}	
			   
			 
                    // generate token for the user
                    if ($token = $this->guard()->login($user)) {
                        $result                = $this->respondWithToken($token);
                        $userInfo              = $this->guard()->user();
                        $result['user']        = $user;
                        $result['is_new_user'] = $is_new_user;
                        $result['show_change_password_option'] = false;
                        unset($result['expires_in']);
                        $userAuth->email        = $request->input('email');
                        $userAuth->access_token = $result['access_token'];
                        $userAuth->user_id      = $userInfo->id;
                        $userAuth->parent_restaurant_id      = $restaurantId;
                        $userAuth->ttl          = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                        $userAuth->save();
                    }
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => $result, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_CREATED'));
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Guest token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    public function checkSocial(Request $request) {
        $ms = microtime(true);
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        // Guest token
        $userAuth   = '';
        $authHeader = $request->header('Authorization');
        if (!empty($authHeader)) {
            $guestToken = explode(' ', $authHeader)[1];
            $userAuth   = UserAuth::where(['guest_token' => $guestToken, 'parent_restaurant_id' => $restaurantId])->first();
        }
        if ($userAuth) {
            //$langId = config('app.language')['id'];
	    $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
            // check if social and access_token already exists
            $checkUserSocial = UserSocial::where(['social' => $request->input('social'), 'parent_restaurant_id' => $restaurantId])->first();
		if ($checkUserSocial) {
		 
		   $user = User::where(['id' => $checkUserSocial->user_id ])->select('users.email') ->first();
                   $me = microtime(true) - $ms;
			return response()->json(['data' => ['user_exist'=>1,'email'=>$user->email], 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_CREATED'));
                 }else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => ['user_exist'=>0], 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_CREATED'));
                }
            
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Guest token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    /**
    * @author : Rahul Gupta
    * @Date Created : 21-08-2018
    * @return It will send a token that will be assigned to particular login's
    */

    public function opa_token(Request $request) {
        $ms = microtime(true);
        $response = array(
            'result' => true,
            'message' => 'Success',            
            'base_url' => env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );
        $device_id = '';
        if($request->has('device_id')) {
            $device_id = $request->input('device_id');
        }

        $token = $this->getNewToken(rand(), $device_id); 
        $status  = Config('constants.status_code.STATUS_SUCCESS');
        $response['data']['token'] = $token;
       
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }

    /**
    * @param current_version 
    * @param token
    * @author : Rahul Gupta
    * @Date Created : 21-08-2018
    * @return It will send a success or fialure based on current version of app
    */

    public function opa_checkupdate(Request $request) { 
        $status  = Config('constants.status_code.STATUS_SUCCESS');
        $ms = microtime(true);
        $response = array(
            'result' => true,
            'message' => 'Success!',            
            'base_url' => env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );       
        $currentVersion = $request->input('current_version');
        $hardVersion = Config('constants.opa.DASHBOARD_HARD_VERSION_ANDROID');

        $softVersion = Config('constants.opa.DASHBOARD_SOFT_VERSION_ANDROID');
        
        if($currentVersion < $hardVersion){ 
            $updateType = "hard";       
        }elseif($currentVersion < $softVersion){
            $updateType = "soft";
        }else{
            $updateType = "no";
        }
        
         $response['data'] = array(
            "upgrade_type"=>$updateType,
            "counter"=>  Config('constants.opa.COUNTER'),
            "message"=>  Config('constants.opa.FOURCE_UPDATE_MESSAGE'),
            "clear_data"=>  Config('constants.opa.CLEAR_DATA'),
            "apk_link"=>  Config('constants.opa.APK_FILE_PATH')
        );

        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }

    /**
    * @param username : 
    * @param password : 
    * @author : Rahul Gupta
    * @Date Created : 20-08-2018
    * @return Login Data
    */

    public function opa_login(Request $request) {
     
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again!',            
            'base_url' => env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $requestData = json_decode($request->getContent(), true);
        #$requestData = $request->all();
        
        $status  = Config('constants.status_code.BAD_REQUEST');
        if(isset($requestData['dashboard_username']) && isset($requestData['dashboard_password']) && isset($requestData['token'])) {
            $email = $requestData['dashboard_username'];
            if (filter_var($email, FILTER_VALIDATE_EMAIL) || strpos($email, '@') !== false) {
                $key = 'email';
            } else {
                $key = 'mobile';                
            }
            $check_token = CmsAuth::select('id', 'restaurant_id', 'token')->where(['token' => $requestData['token']])->orderby('created_at', 'DESC')->first(); 
            
                // generate token for the user
                if($check_token) {
                $check_users = CmsUser::select('id', 'restaurant_id', 'password')->where(['email' => $email])->first(); 
                if ($check_users) {

                    $passcheck = Hash::check($requestData['dashboard_password'], $check_users->password);
                    $get_restaurant_setting = CommonFunctions::getRestaurantSetting($check_users->restaurant_id);                  
                    if($passcheck){
                        if($check_users->restaurant_id) {
                            // Restaurant currency symbol
                            $restData = Restaurant::where('restaurants.id',$check_users->restaurant_id)
                                    ->select('currency_symbol','currency_code','kpt','kpt_calender','city_id','cities.time_zone')
                                    ->leftJoin('cities','restaurants.city_id','=','cities.id')
                                    ->get();
                            if($restData && isset($restData[0])) {
                                $curSymbol = $restData[0]->currency_symbol;
                                $curCode = $restData[0]->currency_code;
                            } else {
                                $curSymbol = config('constants.currency');
                                $curCode = config('constants.currency_code');
                            }
                            $status = Config('constants.status_code.STATUS_SUCCESS');
                            $check_token->cms_user_id = $check_users->id;
                            $check_token->restaurant_id = $check_users->restaurant_id;
                            $check_token->expiry          = date('Y-m-d H:i:s', strtotime('+'. config('constants.session_expiry'). ' minutes'));
                            $check_token->save();  
                            $response['message'] = 'Success!';
                            $response['is_token_valid'] = $response['result'] = true;
                            //$timezone = City::find($restData->city_id)->select('time_zone')->first();
                            $response['data'] = array(
                                'restaurant_id' => $check_users->restaurant_id,
                                'preparation_time'=>isset($restData[0]->kpt_calender)?$restData[0]->kpt_calender:"0-0-0",
                                'timezone'=>isset($restData[0]->time_zone)?$restData[0]->time_zone:"",
                                'restService' => $get_restaurant_setting,
                                'currency_symbol' => $curSymbol,
                                'currency_code' => $curCode,
                                'token' => $requestData['token'],
                                'message' => true
                            );                  
                        }else {
                           $response['message'] = 'No restaurant is assigned on this login. Please try again!';  
                        }

                    }else{
                        $response['message'] = 'Invalid UserName OR Password. Please try again with valid UserName / Password';

                    }
                    
                }else {
                    $response['message'] = 'User does not exists. Please try again with valid user ID / Password';
                }
            }else {
                $response['message'] = 'Invalid Token. Please try again!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }

    public function getNewToken($location_id, $device_id) {
        $check_token = CmsAuth::select('id', 'restaurant_id', 'token')->where(['device_id' => $device_id])->whereDate('expiry', '>=', date('Y-m-d'))->orderby('created_at', 'DESC')->first(); 
        if($check_token) {
            $token =  $check_token->token;          
        }else {
            $token = $location_id.md5($location_id.microtime());
            $save_data = array(
                'device_id' => $device_id,
                'token' => $token,
                'expiry' => date('Y-m-d H:i:s', strtotime('+'. config('constants.session_expiry'). ' minutes'))
            );
            CmsAuth::create($save_data);
        }
        return $token;
    }

     /**
    * @param current_version 
    * @param token
    * @author : Rahul Gupta
    * @Date Created : 22-08-2018
    * @return It will logout 
    * It is not required as app is clearing the token
    */

    public function opa_logout(Request $request) { 
        $status  = Config('constants.status_code.STATUS_SUCCESS');
        $access_token = $request->header('token');
        $ms = microtime(true);
        $response = array(
            'result' => true,
            'message' => 'Success!',            
            'base_url' => env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array('message' => true)
        ); 
        $check_users = CmsAuth::where(['token' => $access_token])->orderby('created_at', 'DESC')->delete();
       
        return response()->json($response, $status);
    }

    public function file_upload($file_url) {
        $filename='/social_'.md5(microtime()).'.jpg';
        $imagePath  = config('constants.socialimage').$filename;
        @file_put_contents($imagePath, @file_get_contents($file_url));
        return '/images/user'.$filename;
    }
    private function getPaymentGetwayInfo($restaurant_id){
        $paymentGatewayConfig = null;
        $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("status", 1)->get()->toArray();
        //print_r($paymentGatewayInfo);die;
        if(isset($paymentGatewayInfo[0])) {
            $paymentGatewayConfig['ID']=$paymentGatewayInfo[0]['id'];
            $config = json_decode($paymentGatewayInfo[0]['config']);
            $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
            foreach ($config as $val1) {
                foreach ($val1 as $key => $val) {
                    $paymentGatewayConfig[$key] = $val;
                }
            }
        }else{
            $paymentGatewayConfig['ID'] = 0;
            $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
            $paymentGatewayConfig['GATEWAY'] = "stripe";
        }
        return $paymentGatewayConfig;
    }
	
}
