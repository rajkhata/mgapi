<?php

namespace App\Http\Controllers;

use App\Models\BuildYourOwnPizza;
use Illuminate\Http\Request;
use App\Helpers\CommonFunctions;

class PizzaExpController extends Controller
{

    public function cmp($a, $b)
    {
        return strcmp($a['priority'], $b['priority']);
    }
    /**
     * Function to return All Pizza Settings of a restaurant
     * @param $id
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllPizzaSettings($id, Request $request)
    {
        $ms = microtime(true);
        $error = '';
	$id = $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        // page type needs to be constant
        $pizzaSetting = BuildYourOwnPizza::where(['restaurant_id'=> $id, 'status' => 1])->orderBy('priority')->get();
        $result = [];
        if($pizzaSetting) {
            foreach ($pizzaSetting as $setting) {
                $result['restaurant_id'] = $setting->restaurant_id;
                $settingdata=(!is_null($setting['setting']))?json_decode($setting['setting'], true):[];
                foreach ($settingdata as &$set){

                    $set['positions']=isset($set['positions'])?(array)CommonFunctions::groupingPositionsData($set['positions']):[];
                }
                usort( $settingdata, array($this, "cmp"));
                if ($setting->is_preference) {
                   

                    $result['preferences'][] = [
                        'id'              => $setting['id'],
                        'label'           => $setting['label'],
                        'is_multiselect'  => $setting['is_multiselect'],
                        'is_customizable' => $setting['is_customizable'],
                        'items'           => $settingdata,
                    ];
                } else {
                    if($setting->parent_id>0) {
                        $index = array_search($setting->parent_id, array_column($result['customizable_data'], 'id'));
                        $result['customizable_data'][$index]['has_child'] = 1;
                        $result['customizable_data'][$index]['items'][] = [
                            'id'              => $setting['id'],
                            'label'           => $setting['label'],
                            'has_parent'      => 1,
                            'is_multiselect'  => $setting['is_multiselect'],
                            'is_customizable' => $setting['is_customizable'],
                            'items'           => $settingdata,
                        ];

                    } else {
                        $result['customizable_data'][] = [
                            'id'              => $setting['id'],
                            'label'           => $setting['label'],
                            'has_child'       => 0,
                            'is_multiselect'  => $setting['is_multiselect'],
                            'is_customizable' => $setting['is_customizable'],
                            'items'           => $settingdata,
                        ];
                    }

                }

            }
        } else {
            $error = 'Not a valid restaurant.';
            $me = microtime(true) - $ms;
            return response()->json(['data'=>$result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data'=>$result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    /**
     * Function to return specific Pizza Setting of a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPizzaSetting($id, $setting , Request $request)
    {
        $ms = microtime(true);
	$id = $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        if(in_array($setting, config('constants.pizza_setting'))) {
            $error = '';
            $pizzaSetting = BuildYourOwnPizza::where('restaurant_id', $id)
                ->select('setting')->where('label',$setting)->first();
            if($pizzaSetting) {
                $pizzaSetting[$setting]  = json_decode($pizzaSetting[$setting], true);
                unset($pizzaSetting['setting']);
            } else {
                $error = 'Not a valid restaurant.';
            }
            $me = microtime(true) - $ms;

            return response()->json(['data'=>[$pizzaSetting], 'error' => $error, 'xtime' => $me]);
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data'=>[], 'error' => 'Invalid pizza setting.', 'xtime' => $me]);
        }

    }

    public function showToppingsCount($id, $size, Request $request)
    {
        $ms = microtime(true);
	$id = $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        $topCountArr = config('constants.topcount');

        /*$res = array_filter($topCountArr, function($key) use($size) {
            $sizeArr = explode('-', $key);
            if($size>= $sizeArr[0] && $size <=$sizeArr[1]) {
                return $key;
            }
        }, ARRAY_FILTER_USE_KEY);

        if($res) {
            $key = key($res);
            $data = $res[$key] ?? $res;
            $error = '';
            $statusCode = 200;
        } else {
            $data = $res;
            $error = 'Invalid pizza setting.';
            $statusCode = 401;
        }*/
        $data = $topCountArr;
        if(count($topCountArr)){
            $error = '';
            $statusCode = 200;
        } else {
            $error = 'Invalid pizza setting.';
            $statusCode = 200;
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => [$data], 'error' => $error, 'xtime' => $me], $statusCode);
    }
}
