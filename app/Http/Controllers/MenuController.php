<?php

namespace App\Http\Controllers;

use App\Models\MenuCategories;
use App\Models\MenuSubCategories;
use App\Models\MenuItems;
use App\Models\NewMenuItems;
use App\Models\ItemAddonGroup;
use App\Models\ItemAddons;
use App\Models\ModifierCategories;
use App\Models\ItemModifier;
use App\Models\ItemModifierGroup;
use App\Models\User;
use App\Models\UserAuth;
use App\Models\UserMyBag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;
use App\Models\CustomVisibilityHours;
use App\Models\VisibilityHours;
use App\Models\ItemAddonGroupsLanguage;
use App\Models\ItemModifierOptionGroup;
use App\Models\ItemModifierOptionGroupLanguage;
use App\Models\ItemModifierOptionGroupItem;
use App\Models\ItemOptionsModifierLanguage;
use App\Models\ItemModifierLanguage;
use App\Models\ItemModifierGroupLanguage;
use App\Models\NewMenuItemsLanguage;
use App\Models\Labels;
use App\Models\MenuItemLabels;
use App\Models\UserOrder;
use \stdClass;


class MenuController extends Controller {

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['showRestaurantSuggestionsToDelete', 'showOneRestaurantMenu', 'showAllRestaurantCategoryMenusTemp', 'getAllMenuCategories', 'getMicroMenus','showAllMenuByRestaurant','showAllMenuswithCats','showAlaCartCategoryMenu','allNormalMenusByParentCat','showAllMenuswithCatsV2']]);
    }

 public function activesubcatcheck($id,$locId){

       $categoryData = MenuSubCategories::where('id', '=', $id)
                        ->where('restaurant_id' , '=',$locId)
                        ->first(['id','parent','status']);  
       $response=true;
       if($categoryData && $categoryData->status==0){
                $response=false; 
       }else if($categoryData && $categoryData->parent!=0){
             $response=$this->activesubcatcheck($categoryData->parent,$locId);

           } 
       return $response;
       
    }


    public function showAllMenuByRestaurant(Request $request){
      $locId = $request->header('X-location');
       $ms = microtime(true);
         $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

         $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            $userId = $userAuth->user_id;


          }
            $request_params=[
                'menu_items.restaurant_id' => $locId,
               // 'menu_items.menu_category_id' => $cat_id,
               // 'menu_items.menu_sub_category_id' => $sub_cat_id,
                'menu_items.status' => 1
            ];

         //  $menuCats = MenuCategories::where($request_params)->get();
          // print_r( $menuCats);

            //$menuItems = array();
        
            $menuItems = MenuItems::leftJoin('menu_sub_categories', 'menu_items.menu_sub_category_id', '=',                          'menu_sub_categories.id')
                        ->where($request_params)                       
                        ->whereHas('maincat', function($q){
                            $q->where('status', '=', 1);
                        })
                        ->select('menu_items.*', 'menu_sub_categories.name as sub_category_name')               
                        ->orderBy('menu_items.display_order', 'DESC')->orderBy('menu_items.images', 'DESC')->get()->toArray();
             
            
            // Remove menu items already in cart
            $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
            $cartMenuIds = array_column($cartMenuItems, 'menu_id');
            $temp = [];
            $defaultImage = null;

            if ($request->has('time')) {
                $current_time = $request->query('time') . ':00';
                if ($request->has('date')) {
                    $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                } else {
                    $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                }
            } else {
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                if (is_object($currentDateTimeObj)) {
                    $current_time = $currentDateTimeObj->format('H:i:00');
                    $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');
                } else {
                    $current_time = date('H:i:00');
                    $restcurrentDateTime = date('Y-m-d H:i:00');
                }
            }

            $deleteFlagNIndex = 0;
            for ($i = 0; $i < count($menuItems); $i++) {

             
             if($menuItems[$i]['menu_sub_category_id']!=NULL){
              $filtersubcat=$this->activesubcatcheck($menuItems[$i]['menu_sub_category_id'],$locId);
                if ($filtersubcat==false){                  
                 unset($menuItems[$i]);
                 continue;

                }
           }

            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
            if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 ) {
            $menuItems[$i]['to_delete'] = $i;
            $deleteFlagNIndex = $i - 1;
            }
            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            $size_prices = json_decode($menuItems[$i]['size_price'], true);
            $extra_params = array('menu_item_id' => $menuItems[$i]['id'], 'slot_code' => $menuItems[$i]['slot_code'],  'restaurant_id' => $locId, 'datetime' => $restcurrentDateTime); 
            $this->getSizePrice($size_prices, $locId, $current_time, $extra_params);

            $menuItems[$i]['size_price'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

            $menuItems[$i]['image'] = [
            "image" => $menuItems[$i]['image'] ?? $defaultImage,
            "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
            "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
            "image_caption" => $menuItems[$i]['image_caption'] ?? '',
            "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
            "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
            "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
            "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
            "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
            "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
            "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
            "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
            "banner_image" => $menuItems[$i]['banner_image'],
            "thumb_banner_image" => $menuItems[$i]['banner_image'],
            ];

            $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
            if ($catkey !== false) {
            $menuItems[$i]['in_cart'] = true;
            $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
            } else {
            $menuItems[$i]['in_cart'] = false;
            $menuItems[$i]['in_cart_quantity'] = 0;
            }

            $menuItems[$i]['priority'] = $i + 1;
            unset($menuItems[$i]['customizable_data_old']);
            unset($menuItems[$i]['thumb_image_med']);
            unset($menuItems[$i]['banner_image']);
            unset($menuItems[$i]['thumb_banner_image_med']);

            #@27-07-2018 By RG Multiple images for KEKI
            $get_item_images = CommonFunctions::get_menu_item_images($menuItems[$i]['id']);
            $menuItems[$i]['images'] = $get_item_images;
            }
          $me = microtime(true) - $ms;


        //return $menuItems;
            if(count($menuItems)){
                return response()->json(['data' => $menuItems, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }else{

               return response()->json(['error' => 'No menu item found!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));

            }
           
    }

    /**
     * Function to show all Restaurant menus
     * Registered to a restaurant
     * @param $restId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllRestaurantMenus($restId) {
        $ms = microtime(true);
        $error = '';
        /* $user          = User::find($restId);
          $userAddresses = [];
          if ($user) {
          $error         = '';
          $userAddresses = $user->address ?? [];
          if (!count($userAddresses)) {
          $error = 'User address not found.';
          }
          } else {
          $error = 'User not found.';
          } */
        $menuData = json_decode('{"id":2,"restaurant_id":3,"menu":[{"image":[{"image":"/images/menu/spinach_tomatoes.png","thumb_med":"/images/menu/thumb/spinach_tomatoes.png","thumb_sml":"/images/menu/thumb/spinach_tomatoes.png","web_image":"/images/menu/spinach_tomatoes.png","web_thumb_med":"/images/menu/thumb/spinach_tomatoes.png","web_thumb_sml":"/images/menu/thumb/spinach_tomatoes.png"}],"sizes":["7","9","12"],"item_name":"Laura\'s favorite","item_description":"Spinach and plum tomatoes on garlic butter","price":"19.59","status":"1","priority":"1","id":2},{"image":[{"image":"/images/menu/pepperoni_olives.png","thumb_med":"/images/menu/thumb/pepperoni_olives.png","thumb_sml":"/images/menu/thumb/pepperoni_olives.png","web_image":"/images/menu/pepperoni_olives.png","web_thumb_med":"/images/menu/thumb/pepperoni_olives.png","web_thumb_sml":"/images/menu/thumb/pepperoni_olives.png"}],"sizes":["7","9","12"],"item_name":"Nick\'s Super","item_description":"Thin crust, pepperoni and black olives.","price":"21.70","status":"1","priority":"2","id":4},{"image":[{"image":"/images/menu/sausage_onion_mushroom.png","thumb_med":"/images/menu/thumb/sausage_onion_mushroom.png","thumb_sml":"/images/menu/thumb/sausage_onion_mushroom.png","web_image":"/images/menu/sausage_onion_mushroom.png","web_thumb_med":"/images/menu/thumb/sausage_onion_mushroom.png","web_thumb_sml":"/images/menu/thumb/sausage_onion_mushroom.png"}],"sizes":["7","9","12"],"item_name":"The 1947","item_description":"Sausage, caramelized onion and sauteed mushroom.","price":"22.95","status":"1","priority":"3","id":5}]}', true);
        $me = microtime(true) - $ms;

        return response()->json(['data' => $menuData, 'error' => $error, 'xtime' => $me]);
    }

    public function showAllRestaurantCategoryMenus($restId, $category) {
        $ms = microtime(true);
        $error = '';
        $menuCat = MenuCategories::where(['restaurant_id' => $restId, 'slug' => $category, 'status' => 1])->first();
        $menuItems = MenuItems::where(['restaurant_id' => $restId, 'category_id' => $menuCat->id, 'status' => 1])->orderBy('image', 'DESC')->get();

        $temp = [];
        for ($i = 0; $i < count($menuItems); $i++) {
            $menuItems[$i]['size_price'] = json_decode($menuItems[$i]['size_price'], true);
            $menuItems[$i]['image'] = [
                "image" => $menuItems[$i]['image'],
                "thumb_med" => $menuItems[$i]['thumb_image_med'],
                "thumb_sml" => $menuItems[$i]['thumb_image_med'],
                "web_image" => $menuItems[$i]['image'],
                "web_thumb_med" => $menuItems[$i]['thumb_image_med'],
                "web_thumb_sml" => $menuItems[$i]['thumb_image_med'],
            ];
            $menuItems[$i]['priority'] = $i + 1;
            unset($menuItems[$i]['thumb_image_med']);
        }
        /* $user          = User::find($restId);
          $userAddresses = [];
          if ($user) {
          $error         = '';
          $userAddresses = $user->address ?? [];
          if (!count($userAddresses)) {
          $error = 'User address not found.';
          }
          } else {
          $error = 'User not found.';
          } */

        //$menuData = json_decode('{"id":2,"restaurant_id":3,"menu":[{"image":[{"image":"/images/menu/spinach_tomatoes.png","thumb_med":"/images/menu/thumb/spinach_tomatoes.png","thumb_sml":"/images/menu/thumb/spinach_tomatoes.png","web_image":"/images/menu/spinach_tomatoes.png","web_thumb_med":"/images/menu/thumb/spinach_tomatoes.png","web_thumb_sml":"/images/menu/thumb/spinach_tomatoes.png"}],"sizes":["7","9","12"],"item_name":"Laura\'s favorite","item_description":"Spinach and plum tomatoes on garlic butter","price":"19.59","status":"1","priority":"1","id":2},{"image":[{"image":"/images/menu/pepperoni_olives.png","thumb_med":"/images/menu/thumb/pepperoni_olives.png","thumb_sml":"/images/menu/thumb/pepperoni_olives.png","web_image":"/images/menu/pepperoni_olives.png","web_thumb_med":"/images/menu/thumb/pepperoni_olives.png","web_thumb_sml":"/images/menu/thumb/pepperoni_olives.png"}],"sizes":["7","9","12"],"item_name":"Nick\'s Super","item_description":"Thin crust, pepperoni and black olives.","price":"21.70","status":"1","priority":"2","id":4},{"image":[{"image":"/images/menu/sausage_onion_mushroom.png","thumb_med":"/images/menu/thumb/sausage_onion_mushroom.png","thumb_sml":"/images/menu/thumb/sausage_onion_mushroom.png","web_image":"/images/menu/sausage_onion_mushroom.png","web_thumb_med":"/images/menu/thumb/sausage_onion_mushroom.png","web_thumb_sml":"/images/menu/thumb/sausage_onion_mushroom.png"}],"sizes":["7","9","12"],"item_name":"The 1947","item_description":"Sausage, caramelized onion and sauteed mushroom.","price":"22.95","status":"1","priority":"3","id":5}]}', true);
        $me = microtime(true) - $ms;

        return response()->json(['data' => $menuItems, 'error' => $error, 'xtime' => $me]);
    }

    /**
     * Function to get one Restaurant menu
     * Registered to a restaurant
     * @param $userId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneRestaurantMenu($id, Request $request) {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }
        if ($userAuth) {
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            // NEED TO CHANGE QUERY ON MULTI LOCATION CHANGE
            //'location_id' => $locationId
            $item = MenuItems::where(['id' => $id, 'restaurant_id' => $locationId])->first();
            $item['customizable_data'] = json_decode($item['customizable_data'], true);

            if(!is_null($item['item_other_info']) && !empty($item['item_other_info'])){
            //  $item['item_other_info'] = json_decode($item['item_other_info'], true);
            }
           
            if(!is_null($item['images']) && !empty($item['images'])){
            //if($item['product_type']=='gift_card' && !is_null($item['images']) && !empty($item['images'])){
              $item['images'] = json_decode($item['images'], true);
            }


            #@16-07-2018 By RG 
            if ($request->has('time')) {
                $current_time = $request->query('time') . ':00';
            } else {
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                if (is_object($currentDateTimeObj)) {
                    $current_time = $currentDateTimeObj->format('H:i:00');
                } else {
                    $current_time = date('H:i:00');
                }
            }
            $size_prices = json_decode($item['size_price'], true);
            $this->getSizePrice($size_prices, $locationId, $current_time);
            $item['size_price'] = $size_prices;

            unset($item['customizable_data_old']);
            $me = microtime(true) - $ms;

            return response()->json(['data' => $item, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }


        $user = User::find($userId);
        $userAddresses = [];
        if ($user) {
            $error = '';
            $userAddresses = $user->address()->where('id', $id)->first() ?? [];
            if (!$userAddresses) {
                $error = 'User address not found.';
            }
        } else {
            $error = 'User not found.';
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userAddresses, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    /**
     * Create Restaurant menu
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request) {
        $ms = microtime(true);
        $error = '';
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'restaurant_id' => 'required|exists:restaurants,id',
            'label' => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'address1' => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'address2' => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'street' => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'city' => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'state' => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'zipcode' => 'required|alpha_num|max:255',
            'phone' => 'numeric',
            'address_type' => 'required|in:billing,shipping',
            'lat' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng' => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);

        //return response()->json($request->all());
        // check if user is registered to the restaurant
        $userInfo = User::find($request->input('user_id'));
        //return response()->json($userInfo);
        if ($request->input('restaurant_id') == $userInfo['restaurant_id']) {
            $user = UserAddresses::create($request->all());
            $status = 201;
        } else {
            $user = [];
            $error = 'User not found.';
            $status = 404;
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Delete a particular Restaurant Menu
     * @param $userId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($userId, $id) {
        $ms = microtime(true);
        UserAddresses::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data' => 'Deleted successfully', 'error' => '', 'xtime' => $me], 200);
    }

    /**
     * Update a particular Restaurant Menu
     * @param         $userId
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($userId, $id, Request $request) {
        // PENDING
        // check if user exists in a restaurant
        return response()->json([$userId, $id]);
        $ms = microtime(true);
        $res = UserAddresses::where(['id' => $id, 'user_id' => $userId])->get();
        $user->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => '', 'xtime' => $me], 200);
    }

    /**
     * Get all category menus for a restaurant
     * @param $restId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllMenuCategories(Request $request,$type='food_item') {

        $ms = microtime(true);
        $bearerToken = $request->bearerToken();

        $error = null;
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }

        if ($userAuth) {
            $status = Config('constants.status_code.STATUS_SUCCESS');
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id); 
            $language_id = $language_id->id;    
            // need to change to location id : done changes
            $menuCategories = MenuCategories::where(['restaurant_id' => $locationId,'language_id' =>$language_id, 'status' => 1]);

            $menuCategories = $menuCategories->where('product_type',$type);  
            $menuCategories = $menuCategories->orderBy('priority','asc')->get();
            $request->request->add(['only_cat' =>1]); //add request

            $extra_cats=$this->getUserSpecificItems($request,['last_ordered']);


            if(!empty($extra_cats)){
                $menuCategories->push($extra_cats);
            }
	    $menuCategories1 = null;
            $defaultImage = '/images/Category_Sample_Image.png';
            foreach($menuCategories as $i=>$menu){ //image
                //iprint_r($menu);die;
                $menu = json_encode($menu,true);
                $menu = json_decode($menu,true);
                $menu['image'] = isset($menu['promotional_banner_image'])?$menu['promotional_banner_image']:$defaultImage;
		$menu['image_mobile'] = isset($menu['promotional_banner_image_mobile'])?$menu['promotional_banner_image_mobile']:$defaultImage;
                $menuCategories1[$i]=$menu;

            }
            //print_r($menuCategories1[0]);die; 
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuCategories1, 'error' => $error, 'xtime' => $me], $status);

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }


    /**
     * Get all category and sub category with items menus for a restaurant
     * @param $restId
     * date: 06-12-018
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCategoriesMenu(Request $request,$type='food_item') {

        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        $with_sub_cat=$request->has('with_sub_cat')?1:0;
        $error = null;
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
            $userId = $userAuth->user_id;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
            $userId = $userAuth->user_id;
        }

        if ($userAuth) {
            $status = Config('constants.status_code.STATUS_SUCCESS');
            $locId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;
            // need to change to location id : done changes
            $menuCategories = MenuCategories::where(['restaurant_id' => $locId,'language_id' =>$language_id, 'status' => 1]);

            $menuCategories = $menuCategories->where('product_type',$type);
            $menuCategories = $menuCategories->orderBy('priority','asc')->get();

            $allCategoriesItems = [];
            //print_r($menuCategories); die;

            if($menuCategories){
                foreach($menuCategories as $data) {
                    $category = $data->id;
                    $menuCat = $menuItems = array();
                    $menuCat = MenuCategories::where(['restaurant_id' => $locId, 'slug' => $category, 'status' => 1])->first();

                    if ($menuCat) {
                        #$parent_id nees to be removed
                        if ($request->has('time')) {
                            $current_time = $request->query('time') . ':00';
                            if ($request->has('date')) {
                                $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                            } else {
                                $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                            }
                        } else {
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                            if (is_object($currentDateTimeObj)) {
                                $current_time = $currentDateTimeObj->format('H:i:00');
                                $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');
                            } else {
                                $current_time = date('H:i:00');
                                $restcurrentDateTime = date('Y-m-d H:i:00');
                            }
                        }

                        #for Salaty Iguana hard Code done Bt Rahul gupta 06-11-2018 will changes later
                        $show_menu_item = 1;
                        if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                            #1=> Monday ....
                            $day = date('N', strtotime($restcurrentDateTime));
                            if (in_array($day, array(1, 2, 3, 4, 5))) {
                                $currnt_time = strtotime(date('H:i:00', strtotime($restcurrentDateTime)));
                                if ($currnt_time < strtotime('11:00:00') || $currnt_time > strtotime('16:00:00')) {
                                    $show_menu_item = 0;
                                }
                            } else {
                                $show_menu_item = 0;
                            }
                        }

                        if ($with_sub_cat) {
                            $subcats = $this->getItemsWithSubcat($menuCat->id, $locId, $userId, $category, $request);
                            $parent_cat_items = $this->getDirectParentCatOnly($menuCat->id, $locId, $userId, $category, $request);
                            #will removed it @11-05-2018 Rg
                            if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                                foreach ($parent_cat_items as $key => $subcat) {
                                    foreach ($subcat['size_price'] as $key1 => $subsubcat) {
                                        $parent_cat_items[$key]['size_price'][$key1]['is_delivery'] = $show_menu_item;
                                        $parent_cat_items[$key]['size_price'][$key1]['is_carryout'] = $show_menu_item;
                                    }
                                }
                            }
                            $menuItems = [
                                'subcats' => $subcats,
                                'menu_items_list' => $parent_cat_items

                            ];


                        } else {

                            $menuItems = MenuItems::leftJoin('menu_sub_categories', 'menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
                                ->where(['menu_items.restaurant_id' => $locId, 'menu_items.menu_category_id' => $menuCat->id, 'menu_items.status' => 1])
                                ->select('menu_items.*', 'menu_sub_categories.name as sub_category_name')
                                /* ->orderBy('menu_items.sub_category_id', 'DESC')
                                  ->orderBy('menu_sub_categories.priority') */
                                //->orderBy('menu_items.image', 'DESC')->get()->toArray();
                                ->orderBy('menu_items.display_order', 'ASC')->orderBy('menu_items.images', 'DESC')->get()->toArray();


                            // Remove menu items already in cart
                            $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
                            $cartMenuIds = array_column($cartMenuItems, 'menu_id');

                            $temp = [];
                            $defaultImage = null;
                            if (strtolower($category) == 'pizza') {
                                $defaultImage = '/images/menu/dummy_pizza_image.png';
                            }
                            $deleteFlagNIndex = 0;
                            for ($i = 0; $i < count($menuItems); $i++) {
                                // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                                if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                                    $menuItems[$i]['to_delete'] = $i;
                                    $deleteFlagNIndex = $i - 1;
                                }
                                #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
                                $size_prices = json_decode($menuItems[$i]['size_price'], true);
                                $this->getSizePrice($size_prices, $locId, $current_time);


                                $menuItems[$i]['size_price'] = $size_prices;
                                $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
                                //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

                                $menuItems[$i]['image'] = [
                                    "image" => $menuItems[$i]['image'] ?? $defaultImage,
                                    "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                                    "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                                    "image_caption" => $menuItems[$i]['image_caption'] ?? '',
                                    "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
                                    "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
                                    "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
                                    "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
                                    "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
                                    "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
                                    "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
                                    "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
                                    "banner_image" => $menuItems[$i]['banner_image'],
                                    "thumb_banner_image" => $menuItems[$i]['banner_image'],
                                ];

                                $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
                                if ($catkey !== false) {
                                    $menuItems[$i]['in_cart'] = true;
                                    $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
                                } else {
                                    $menuItems[$i]['in_cart'] = false;
                                    $menuItems[$i]['in_cart_quantity'] = 0;
                                }

                                $menuItems[$i]['priority'] = $i + 1;
                                unset($menuItems[$i]['customizable_data_old']);
                                unset($menuItems[$i]['thumb_image_med']);
                                unset($menuItems[$i]['banner_image']);
                                unset($menuItems[$i]['thumb_banner_image_med']);

                                #@27-07-2018 By RG Multiple images for KEKI
                                $get_item_images = CommonFunctions::get_menu_item_images($menuItems[$i]['id']);
                                $menuItems[$i]['images'] = $get_item_images;
                            }
                            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                            if ($deleteFlagNIndex) {
                                //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
                                //array_splice($menuItems, $deleteFlagNIndex, 1);
                            }
                        }
                    }

                    $allCategoriesItems[$category] = $menuItems;
                }

            }


            $me = microtime(true) - $ms;
            return response()->json(['data' => $allCategoriesItems, 'error' => $error, 'xtime' => $me], $status);
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }




    /**
     * Get Suggestions data with menu items
     * @param $restId
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showRestaurantSuggestions($restId, $userId) {
        $ms = microtime(true);
        $error = '';
        $status = 200;
        $suggestionsArr = [];
        // Check for user id
        /* if (!isset($_GET['user_id'])) {
          $status = 404;
          $error = 'User not found.';
          $me = microtime(true) - $ms;
          return response()->json(['data' => $suggestionsArr, 'error' => $error, 'xtime' => $me], $status);
          } */
        // Check for menu items limit
        /* if (!isset($_GET['limit'])) {
          $status = 404;
          $error = 'Limit not specified.';
          $me = microtime(true) - $ms;
          return response()->json(['data' => $suggestionsArr, 'error' => $error, 'xtime' => $me], $status);
          } */

        $catLimit = $_GET['cat_limit'] ?? 5;
        $menuLimit = $_GET['menu_limit'] ?? 4;

        // Loop through all categories
        $menuCat = MenuCategories::select('id', 'name', 'slug', 'priority')
                        ->where(['restaurant_id' => $restId, 'status' => 1])->limit($catLimit)->get()->toArray();

        // Get all menu ids, into array
        $catIdArr = array_column($menuCat, 'id');

        // Remove menu items already in cart
        $cartMenuItems = UserMyBag::select('menu_id')->where(['restaurant_id' => $restId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
        $cartMenuIds = array_column($cartMenuItems, 'menu_id');

        // Loop through all menu items
        $allMenuItemIds = MenuItems::whereIn('category_id', $catIdArr)->whereNotIn('id', $cartMenuIds)->where('restaurant_id', $restId)->get();

        $i = 0;
        foreach ($allMenuItemIds as $menuItem) {
            $menuId = $menuItem['id'];
            $key = array_search($menuItem['category_id'], array_column($menuCat, 'id'));

            $slug = $menuCat[$key]['slug'];
            $category = $menuCat[$key]['name'];
            $menuItem['category'] = $category;
            $menuItem['category_slug'] = $slug;
            $menuItem['size_price'] = json_decode($menuItem['size_price'], true);

            $pushedCats = array_column($suggestionsArr, 'category');
            $index = array_search($category, $pushedCats);
            if ($index !== false) {
                if (count($suggestionsArr[$index]['items']) < $menuLimit) {
                    array_push($suggestionsArr[$index]['items'], $menuItem);
                }
            } else {
                $suggestionsArr[] = ['category' => $category, 'items' => [$menuItem]];
            }
            $i++;
        }


        $me = microtime(true) - $ms;

        return response()->json(['data' => $suggestionsArr, 'error' => $error, 'xtime' => $me], $status);
    }

    // Suggestion api changed on 14-07-2018 By Rahul Gupta
    public function showRestaurantSuggestionsToDelete(Request $request) {
        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        $ms = microtime(true);
        $suggestionsArr = [];
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $catLimit = $_GET['cat_limit'] ?? 5;
            $menuLimit = $_GET['menu_limit'] ?? 4;
            // Loop through all categories
            $menuCat = MenuCategories::select('id', 'name', 'slug', 'priority')
                            ->where(['restaurant_id' => $locationId, 'status' => 1])->limit($catLimit)->get()->toArray();

            // Get all menu ids, into array
            $catIdArr = array_column($menuCat, 'id');

            // Remove menu items already in cart
            $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId, 'status' => 0])->get()->toArray();
            $cartMenuIds = array_column($cartMenuItems, 'menu_id');

            // Loop through all menu items
            $allMenuItemIds = MenuItems::whereIn('menu_category_id', $catIdArr)->where('restaurant_id', $locationId)->where('image', '<>', NULL)->get();
            if ($request->has('time')) {
                $current_time = $request->query('time') . ':00';
            } else {
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                if (is_object($currentDateTimeObj)) {
                    $current_time = $currentDateTimeObj->format('H:i:00');
                } else {
                    $current_time = date('H:i:00');
                }
            }
            $i = 0;
            foreach ($allMenuItemIds as $key => $menuItem) {
                $menuId = $menuItem['id'];
                $key = array_search($menuItem['category_id'], array_column($menuCat, 'id'));

                $slug = $menuCat[$key]['slug'];
                $category = $menuCat[$key]['name'];
                $menuItem['category'] = $category;
                $menuItem['category_slug'] = $slug;

                #@14-07-2018 By RG 
                $size_prices = json_decode($menuItem['size_price'], true);
                $this->getSizePrice($size_prices, $locationId, $current_time);
                $menuItem['size_price'] = $size_prices;

                $defaultImage = null;
                if (strtolower($category) == 'pizza') {
                    $defaultImage = '/images/menu/dummy_pizza_image.png';
                }
                # Added Image key as It was not sync with category/pizza api
                $menuItem['image'] = [
                    "image" => $menuItem['image'] ?? $defaultImage,
                    "thumb_med" => $menuItem['thumb_image_med'] ?? $defaultImage,
                    "thumb_sml" => $menuItem['thumb_image_med'] ?? $defaultImage,
                    "image_caption" => $menuItem['image_caption'] ?? '',
                    "web_image" => $menuItem['web_image'] ?? $defaultImage,
                    "web_thumb_med" => $menuItem['web_thumb_med'] ?? $defaultImage,
                    "web_thumb_sml" => $menuItem['web_thumb_sml'] ?? $defaultImage,
                    "web_image_caption" => $menuItem['web_image_caption'] ?? '',
                    "mobile_web_image" => $menuItem['mobile_web_image'] ?? $defaultImage,
                    "mobile_web_thumb_med" => $menuItem['mobile_web_thumb_med'] ?? $defaultImage,
                    "mobile_web_thumb_sml" => $menuItem['mobile_web_thumb_sml'] ?? $defaultImage,
                    "mobile_web_image_caption" => $menuItem['mobile_web_image_caption'] ?? '',
                    "banner_image" => $menuItem['banner_image'],
                    "thumb_banner_image" => $menuItem['banner_image'],
                ];

                $menuItem['customizable_data'] = json_decode($menuItem['customizable_data'], true);
                unset($menuItem['customizable_data_old']);
                $catkey = array_search($menuItem['id'], $cartMenuIds);
                if ($catkey !== false) {
                    $menuItem['in_cart'] = true;
                    $menuItem['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
                } else {
                    $menuItem['in_cart'] = false;
                    $menuItem['in_cart_quantity'] = 0;
                }

                $pushedCats = array_column($suggestionsArr, 'category');
                $index = array_search($category, $pushedCats);
                if ($index !== false) {
                    if (count($suggestionsArr[$index]['items']) < $menuLimit) {
                        array_push($suggestionsArr[$index]['items'], $menuItem);
                    }
                } else {
                    $suggestionsArr[] = ['category' => $category, 'items' => [$menuItem]];
                }
                $i++;
            }
            $me = microtime(true) - $ms;

            return response()->json(['data' => $suggestionsArr, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    public function getMenuItemList($cat_id,$sub_cat_id=NULL,$locId,$userId,$category,$request){

            $request_params=[
                'menu_items.restaurant_id' => $locId,
                'menu_items.menu_category_id' => $cat_id,
                'menu_items.menu_sub_category_id' => $sub_cat_id,
                'menu_items.status' => 1
            ];

            $menuItems = array();
        
            $menuItems = MenuItems::leftJoin('menu_sub_categories', 'menu_items.menu_sub_category_id', '=',                          'menu_sub_categories.id')
                        ->where($request_params)
                        ->select('menu_items.*', 'menu_sub_categories.name as sub_category_name')               
                        ->orderBy('menu_items.display_order', 'ASC')->orderBy('menu_items.images', 'DESC')->get()->toArray();

            // Remove menu items already in cart
            $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
            $cartMenuIds = array_column($cartMenuItems, 'menu_id');
            $temp = [];
            $defaultImage = null;
            if (strtolower($category) == 'pizza') {
            $defaultImage = '/images/menu/dummy_pizza_image.png';
            }
            if ($request->has('time')) { 
                $current_time = $request->query('time') . ':00';
                if($request->has('date') ){
                    $restcurrentDateTime = $request->input('date').' '.$request->query('time') . ':00';
                }else {
                    $restcurrentDateTime = date('Y-m-d').' '.$request->query('time') . ':00';
                }
            } else { 
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                if (is_object($currentDateTimeObj)) {
                    $current_time = $currentDateTimeObj->format('H:i:00');
                    $restcurrentDateTime =  $currentDateTimeObj->format('Y-m-d H:i:00');
                } else {
                    $current_time = date('H:i:00');
                    $restcurrentDateTime = date('Y-m-d H:i:00');
                }
            }
            $deleteFlagNIndex = 0;
            for ($i = 0; $i < count($menuItems); $i++) {
            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
            if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
            $menuItems[$i]['to_delete'] = $i;
            $deleteFlagNIndex = $i - 1;
            }
            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            $size_prices = json_decode($menuItems[$i]['size_price'], true);
            $extra_params = array('menu_item_id' => $menuItems[$i]['id'], 'slot_code' => $menuItems[$i]['slot_code'],  'restaurant_id' => $locId, 'datetime' => $restcurrentDateTime); 
            $this->getSizePrice($size_prices, $locId, $current_time, $extra_params);

            $menuItems[$i]['size_price'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

            $menuItems[$i]['image'] = [
            "image" => $menuItems[$i]['image'] ?? $defaultImage,
            "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
            "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
            "image_caption" => $menuItems[$i]['image_caption'] ?? '',
            "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
            "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
            "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
            "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
            "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
            "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
            "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
            "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
            "banner_image" => $menuItems[$i]['banner_image'],
            "thumb_banner_image" => $menuItems[$i]['banner_image'],
            ];

            $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
            if ($catkey !== false) {
            $menuItems[$i]['in_cart'] = true;
            $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
            } else {
            $menuItems[$i]['in_cart'] = false;
            $menuItems[$i]['in_cart_quantity'] = 0;
            }

            $menuItems[$i]['priority'] = $i + 1;
            unset($menuItems[$i]['customizable_data_old']);
            unset($menuItems[$i]['thumb_image_med']);
            unset($menuItems[$i]['banner_image']);
            unset($menuItems[$i]['thumb_banner_image_med']);

            #@27-07-2018 By RG Multiple images for KEKI
            $get_item_images = CommonFunctions::get_menu_item_images($menuItems[$i]['id']);
            $menuItems[$i]['images'] = $get_item_images;
            }
            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
            if ($deleteFlagNIndex) {
            //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
            //array_splice($menuItems, $deleteFlagNIndex, 1);
            }

        return $menuItems;

    }
    public function getDirectParentCatOnly($cat_id,$locId,$userId,$category,$request){    
         return $this->getMenuItemList($cat_id,NULL,$locId,$userId,$category,$request);
    }
    

    public function getItemsWithSubcat($cat_id,$locId,$userId,$category,$request,$parent=0){

        $categoryData = MenuSubCategories::where('menu_category_id', '=', $cat_id)
                        ->where('status' , '=', 1)
                        ->where('parent', '=', $parent);
      
        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
        if(count($categoryData)){
            foreach($categoryData as  &$cat){                
                $cat->subcats=$this->getItemsWithSubcat($cat_id,$locId,$userId,$category,$request,$cat->id);
                $menuItems=$this->getMenuItemList($cat_id,$cat->id,$locId,$userId,$category,$request);
                $cat->menu_items_list=$menuItems;
            }
        }
        return  $categoryData;
    }
    // public function getItemsWithSubcat($cat_id,$locId,$userId,$category,$request){
      
    //     $catitems = array();
    //     if(count($subcats)){
    //         foreach($subcats as &$subcat){
    
    //             $menuItems=$this->getMenuItemList($cat_id,$subcat,$locId,$userId,$category,$request);
    //             $subcat->menu_items_list=$menuItems;
    //         }
    //         $catitems=$subcats;

    //     }
    //    return $catitems;

    // }

    /**
     * TEMP route for mobile with - in_cart, in_cart_quantity
     * @param $restId
     * @param $userId
     * @param $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllRestaurantCategoryMenusTemp($category, Request $request) {
    
        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        
       $with_sub_cat=$request->has('with_sub_cat')?1:0;
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            $userId = $userAuth->user_id;
            $menuCat = $menuItems = array();

           
           $menuCat = MenuCategories::where(['restaurant_id' => $locId, 'slug' => $category, 'status' => 1])->first();
            
          
            if ($menuCat) { 
                #$parent_id nees to be removed
                if ($request->has('time')) { 
                    $current_time = $request->query('time') . ':00';
                    if($request->has('date') ){
                        $restcurrentDateTime = $request->input('date').' '.$request->query('time') . ':00';
                    }else {
                        $restcurrentDateTime = date('Y-m-d').' '.$request->query('time') . ':00';
                    }
                } else { 
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                    if (is_object($currentDateTimeObj)) {
                        $current_time = $currentDateTimeObj->format('H:i:00');
                        $restcurrentDateTime =  $currentDateTimeObj->format('Y-m-d H:i:00');
                    } else {
                        $current_time = date('H:i:00');
                        $restcurrentDateTime = date('Y-m-d H:i:00');
                    }
                }
            
                #for Salaty Iguana hard Code done Bt Rahul gupta 06-11-2018 will changes later
                // $show_menu_item = 1;
                // if(in_array($menuCat->id, array(128, 134, 140, 146, 152,130,136,142,148, 154))) {
                //     #1=> Monday ....
                //     $day = date('N', strtotime($restcurrentDateTime));                   
                //     if(in_array( $day, array(1,2,3,4,5))) {
                //         $currnt_time = strtotime(date('H:i:00', strtotime($restcurrentDateTime)));
                //         if($currnt_time <  strtotime('11:00:00') || $currnt_time >  strtotime('16:00:00')) {
                //             $show_menu_item = 0;
                //         }
                //     }else {
                //         $show_menu_item = 0;
                //     }
                // }

                if($with_sub_cat){
                    $subcats= $this->getItemsWithSubcat($menuCat->id,$locId,$userId,$category,$request);
                    $parent_cat_items= $this->getDirectParentCatOnly($menuCat->id,$locId,$userId,$category,$request);
                     #will removed it @11-05-2018 Rg
                    // if(in_array($menuCat->id, array(128, 134, 140, 146, 152,130,136,142,148, 154))) {                       
                    //     foreach ($parent_cat_items as $key => $subcat) {
                    //        foreach ($subcat['size_price'] as $key1 => $subsubcat) {
                    //           $parent_cat_items[$key]['size_price'][$key1]['is_delivery'] = $show_menu_item;
                    //           $parent_cat_items[$key]['size_price'][$key1]['is_carryout'] = $show_menu_item;
                    //        }
                    //    }
                    // } 
                    $menuItems=[
                         'subcats'=>$subcats,
                         'menu_items_list'=>$parent_cat_items

                    ];

                   
                }else{    

                $menuItems = MenuItems::leftJoin('menu_sub_categories', 'menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
                                ->where(['menu_items.restaurant_id' => $locId, 'menu_items.menu_category_id' => $menuCat->id, 'menu_items.status' => 1])
                                ->select('menu_items.*', 'menu_sub_categories.name as sub_category_name')
                                /* ->orderBy('menu_items.sub_category_id', 'DESC')
                                  ->orderBy('menu_sub_categories.priority') */
                                //->orderBy('menu_items.image', 'DESC')->get()->toArray();
                                  ->orderBy('menu_items.display_order', 'ASC')->orderBy('menu_items.images', 'DESC')->get()->toArray();



                // Remove menu items already in cart
                $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
                $cartMenuIds = array_column($cartMenuItems, 'menu_id');

                $temp = [];
                $defaultImage = null;
                if (strtolower($category) == 'pizza') {
                    $defaultImage = '/images/menu/dummy_pizza_image.png';
                }                
                $deleteFlagNIndex = 0;
                for ($i = 0; $i < count($menuItems); $i++) {
                    // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                    if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                        $menuItems[$i]['to_delete'] = $i;
                        $deleteFlagNIndex = $i - 1;
                    }
                    #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
                    $size_prices = json_decode($menuItems[$i]['size_price'], true);  
                    $extra_params = array('menu_item_id' => $menuItems[$i]['id'], 'slot_code' => $menuItems[$i]['slot_code'],  'restaurant_id' => $locId, 'datetime' => $restcurrentDateTime);                 
                    $this->getSizePrice($size_prices, $locId, $current_time,$extra_params);
                   

                    $menuItems[$i]['size_price'] = $size_prices;
                    $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
                    //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

                    $menuItems[$i]['image'] = [
                        "image" => $menuItems[$i]['image'] ?? $defaultImage,
                        "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                        "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                        "image_caption" => $menuItems[$i]['image_caption'] ?? '',
                        "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
                        "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
                        "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
                        "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
                        "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
                        "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
                        "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
                        "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
                        "banner_image" => $menuItems[$i]['banner_image'],
                        "thumb_banner_image" => $menuItems[$i]['banner_image'],
                    ];

                    $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
                    if ($catkey !== false) {
                        $menuItems[$i]['in_cart'] = true;
                        $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
                    } else {
                        $menuItems[$i]['in_cart'] = false;
                        $menuItems[$i]['in_cart_quantity'] = 0;
                    }

                    $menuItems[$i]['priority'] = $i + 1;
                    unset($menuItems[$i]['customizable_data_old']);
                    unset($menuItems[$i]['thumb_image_med']);
                    unset($menuItems[$i]['banner_image']);
                    unset($menuItems[$i]['thumb_banner_image_med']);

                    #@27-07-2018 By RG Multiple images for KEKI
                    $get_item_images = CommonFunctions::get_menu_item_images($menuItems[$i]['id']);
                    $menuItems[$i]['images'] = $get_item_images;
                }
                // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                if ($deleteFlagNIndex) {
                    //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
                    //array_splice($menuItems, $deleteFlagNIndex, 1);
                }
               }
            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuItems, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * @param $size_price_list = array
     * @param $location_id = Branch Id
     * @param $current_time = Restaurant Current Time
     */
    public function getSizePrice(&$size_price_list, $location_id, $current_time, $extra = array()) {       
        $current_menu_meal = DB::table('menu_meal_types')
                ->whereTime('to_time', ' >= ', $current_time)
                ->whereTime('from_time', '<= ', $current_time)
                ->where('restaurant_id', $location_id)
                ->where('status', 1)
                ->first();
        if ($size_price_list) {
            foreach ($size_price_list as $key => $size) {
                $has_price = false;
                if ((count($size['price']) > 1) || !isset($size['price']['0'])) {

                    // It is price based on menu meal
                    if ($current_menu_meal) {
                        if (isset($size['price'][$current_menu_meal->id])) {

                            $size_price_list[$key]['price'] = $size['price'][$current_menu_meal->id];
                            if (isset($size['available'])) {
                                $size_price_list[$key]['is_delivery'] = $size['available'][$current_menu_meal->id]['is_delivery'];
                                $size_price_list[$key]['is_carryout'] = $size['available'][$current_menu_meal->id]['is_carryout'];
                            } else {
                                // for old data in DB if any 
                                $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                            }
                            $has_price = true;
                        }
                    } else {
                        // unset this price item as This is not available in meal type @26-07-2018
                        unset($size_price_list[$key]);
                    }
                } elseif (count($size['price']) == 1) {
                    // It is all price

                 //   $size_price_list[$key]['price'] = $size['price']['0'];
                    $size_price_list[$key]['price'] = $size['price']['0'];
		    $size_price_list[$key]['actual_price'] = $size['price']['0'];
                    if(isset($size['special_price']) && isset($size['start_date']) && isset($size['end_date'])){
                        $special_price=$size['special_price'];
                        $special_price_start_date=$size['start_date'];
                        $special_price_end_date=$size['end_date'];

                        $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                        $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                        $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                        if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                            $size_price_list[$key]['price'] = $special_price;
                            $size_price_list[$key]['cutout_price'] = $size['price']['0'];

                            unset($size_price_list[$key]['special_price']);
                            unset($size_price_list[$key]['start_date']);
                            unset($size_price_list[$key]['end_date']);
                        }
                    }
		    if(isset($size['happy_hour_price']) && isset($size['from_time']) && isset($size['to_time'])){
                        $special_price=$size['happy_hour_price'];
                        $special_price_start_date=date("Y-m-d ").$size['from_time'];
                        $special_price_end_date=date("Y-m-d ").$size['to_time'];

                        $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                        $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                        $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                        if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
			    
                            $size_price_list[$key]['price'] = $special_price;
                            $size_price_list[$key]['cutout_price'] = $size['price']['0'];

                            
                        }
			unset($size_price_list[$key]['happy_hour_price']);
                        unset($size_price_list[$key]['from_time']);
                        unset($size_price_list[$key]['to_time']);
                    }
                    if (isset($size['available'])) {
                        $size_price_list[$key]['is_delivery'] = $size['available']['is_delivery'];
                        $size_price_list[$key]['is_carryout'] = $size['available']['is_carryout'];
                    } else {
                        // for old data in DB if any 
                        $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                    }
                    $has_price = true;
                }
                //check if price does not exists then remove it from the array
                if (!$has_price) {
                    unset($size_price_list[$key]);
                }
                # 9-Aug-19 - As discussed with Deepak, Prakash, Nishant
                // check if is_delivery & is_carryout is 0, remove it from the array
                if(isset($size_price_list[$key]['is_delivery']) && isset($size_price_list[$key]['is_carryout']) && $size_price_list[$key]['is_delivery'] == 0 && $size_price_list[$key]['is_carryout'] == 0) {
                    unset($size_price_list[$key]);
                }
                unset($size_price_list[$key]['available']);
            }
            $size_price_list = array_values($size_price_list);
        }
        #check availabiltity of the menu Items @21-12-2018 RG
        if(isset($extra['slot_code']) && $extra['slot_code']) {
            $check_availablity = $this->check_menu_availablity($extra);
           
            foreach ($size_price_list as &$list) {
                if($list['is_delivery'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_delivery'] = $check_availablity['is_delivery'];
                }
                if($list['is_carryout'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_carryout'] = $check_availablity['is_carryout'];
                }
            }
        }
        # End visibility Time        
    }

    public function getMicroMenus(Request $request) {
        $ms = microtime(true);   
        $bearerToken = $request->bearerToken();
        $error = null;
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }

        if ($userAuth) {

            $status = Config('constants.status_code.STATUS_SUCCESS');

            $locationId = $request->header('X-location');

            $restaurantId = $request->header('X-restaurant');

            $menuCategories = MenuCategories::where(['restaurant_id' => $locationId, 'status' => 1])->orderBy('priority','asc')->get();
            $categoryCount = $menuCategories->count();
            $priceRange = [];
            $microMenu = [];
            $hasMenuItem = [];
            $priceRangeValue = [];
            if ($categoryCount>0) {
                foreach ($menuCategories as $key => $category) {

                    $menuSubCategories = MenuSubCategories::where(['restaurant_id' => $locationId, 'menu_category_id' => $category['id'], 'status' => 1])->get();
                    $subCategoryCount = $menuSubCategories->count();
                    if ($subCategoryCount>0) {
                        $submenu=[];
                        foreach ($menuSubCategories as $skey => $subcategory) {
                            $menuItems = MenuItems::where(['restaurant_id' => $locationId, 'menu_sub_category_id' => $subcategory['id'], 'status' => 1])->get();
                            $menuCount = $menuItems->count();
                            if ($menuCount > 0) {
                                foreach ($menuItems as $mkey => $item) {
                                    $priceDetails = json_decode($item['size_price']);
                                    $price = (array)$priceDetails[0];
                                    if(!is_array($price['price'])){
                                        $price['price'] = (array)$price['price'];
                                    }                                    
                                    foreach($price['price'] as $pkey => $p){
                                        $priceRange[] = $p;
                                        $key = $pkey;
                                    }                                    
                                    
                                    $hasMenuItem [] = array(
                                        'name' => $item['name'],
                                        '@type' => "MenuItem",
                                        'image' => $item['web_image'],
                                        'description' => $item['description'],
                                        'offers' => array(
                                            '@type' => "offer",
                                            'price' => $price['price'][$key],
                                            'priceCurrency' => "USD"
                                        )
                                    );
                                }
                                $items = $hasMenuItem;
                                $hasMenuItem = [];
                                $submenu[$skey] = array(
                                    'name' => $subcategory['name'],
                                    '@type' => "MenuSection",
                                    'hasMenuItem' => $items
                                );
                            }
                        }
                        $total = count($priceRange);
                        sort($priceRange);
                        $microMenu[] = array('name' => $category['name'], '@type' => "MenuSection", 'hasMenuSection' => $submenu);
                    } else {
                        $menuItems = MenuItems::where(['restaurant_id' => $locationId, 'menu_category_id' => $category['id'], 'status' => 1])->get();
                        $menuCount = $menuItems->count();
                        if ($menuCount > 0) {
                            
                            foreach ($menuItems as $key => $item) {
                                
                                $priceDetails = json_decode($item['size_price']);                                
                                
                                $price = (array)$priceDetails[0];
                                if(!is_array($price['price'])){
                                    $price['price'] = (array)$price['price'];
                                }
                                
                                foreach($price['price'] as $pkey => $p){
                                    $priceRange[] = $p;
                                    $key = $pkey;
                                }
                                
                                $hasMenuItem[] = array(
                                    'name' => $item['name'],
                                    '@type' => "MenuItem",
                                    'image' => $item['web_image'],
                                    'description' => $item['description'],
                                    'offers' => array(
                                        '@type' => "MenuSection",
                                        'price' => $price['price'][$key],
                                        'priceCurrency' => "USD"
                                    )
                                );
                            }
                           
                            $items = $hasMenuItem;
                            $hasMenuItem = [];
                            $total = count($priceRange);
                            sort($priceRange);
                            $microMenu[] = array('name' => $category['name'], '@type' => "MenuSection", 'hasMenuItems' => $items);
                        }
                    }
                }
            }
            if($priceRange){
                $priceRangeValue=array($priceRange[0], $priceRange[$total - 1]);
            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $microMenu,'priceRange'=>$priceRangeValue, 'error' => $error, 'xtime' => $me], $status);
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
        
    /**
     * TEMP route for mobile with - in_cart, in_cart_quantity
     * @param $restId

     * @param $userId
     * @param $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAlaCartCategoryMenu($category, Request $request) {
     
        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        $with_sub_cat = $request->has('with_sub_cat') ? 1 : 0;
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        //$userAuth = true;
        if ($userAuth) {
            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;

            $userId = $userAuth->user_id;
            $menuSubCat = $menuCat = $menuItems = array();

            $menuCat = MenuCategories::where(['restaurant_id' => $locId, 'slug' => $category, 'status' => 1])->first();


            if ($menuCat) {

                if ($request->has('time')) {
                    $current_time = $request->query('time') . ':00';
                    if ($request->has('date')) {
                        $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                    } else {
                        $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                    }
                } else {
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));

                    if (is_object($currentDateTimeObj)) {

                        $current_time = $currentDateTimeObj->format('H:i:00');

                        $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');

                    } else {

                        $current_time = date('H:i:00');

                        $restcurrentDateTime = date('Y-m-d H:i:00');

                    }

                }
                $menuSubCat = MenuSubCategories::where(['restaurant_id' => $locId, 'menu_category_id' => $menuCat->id, 'status' => 1])->get()->toArray();
                foreach($menuSubCat as $subIndex=>$menuSub){
                    $menuItems = NewMenuItems::where(['new_menu_items.restaurant_id' => $locId,'new_menu_items.menu_category_id' => $menuCat->id,  'new_menu_items.menu_sub_category_id' => $menuSub['id'], 'new_menu_items.status' => 1])
                        ->select('new_menu_items.*')
                        ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();

                    foreach ($menuItems as $index => $menu) {
                        $adonsDetails = $this->getItemsAdonsWithAddonsGroup($menu['id'], $language_id, $request, $locId);
                        $menuItems[$index]['adons'] = $adonsDetails;
                        $modifierDetails = $this->getModifiersGroup($menu['id'], $language_id, $request);
                        $menuItems[$index]['modifier'] = $modifierDetails;
                        $menuItems[$index]['labels'] = $this->labelDetails($menu['id']);
                    }

                    // Remove menu items already in cart
                    $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
                    $cartMenuIds = array_column($cartMenuItems, 'menu_id');

                    $temp = [];
                    $defaultImage = null;
                    if (strtolower($category) == 'pizza') {
                        $defaultImage = '/images/menu/dummy_pizza_image.png';
                    }
                    $deleteFlagNIndex = 0;
                    for ($i = 0; $i < count($menuItems); $i++) {
                        // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                        if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                            $menuItems[$i]['to_delete'] = $i;
                            $deleteFlagNIndex = $i - 1;
                        }
                        #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
                        $size_prices = json_decode($menuItems[$i]['size_price'], true);
                        $this->getSizePrice($size_prices, $locId, $current_time);

                        $menuItems[$i]['size_price'] = $size_prices;
                        //$menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
                        //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

                        $menuItems[$i]['img'] = [
                            "image" => $menuItems[$i]['image'] ?? $defaultImage,
                            "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                            "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                            "image_caption" => $menuItems[$i]['image_caption'] ?? '',
                            "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
                            "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
                            "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
                            "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
                            "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
                            "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
                            "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
                            "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
                            "banner_image" => $menuItems[$i]['banner_image'],
                            "thumb_banner_image" => $menuItems[$i]['banner_image'],
                        ];

                        $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
                        if ($catkey !== false) {
                            $menuItems[$i]['in_cart'] = true;
                            $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
                        } else {
                            $menuItems[$i]['in_cart'] = false;
                            $menuItems[$i]['in_cart_quantity'] = 0;
                        }

                        $menuItems[$i]['priority'] = $i + 1;
                        unset($menuItems[$i]['customizable_data_old']);
                        unset($menuItems[$i]['thumb_image_med']);
                        unset($menuItems[$i]['banner_image']);
                        unset($menuItems[$i]['thumb_banner_image_med']);



                        // set language items
                        $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                            'new_menu_items_id' => $menuItems[$i]['id'],
                            'language_id' => $language_id,
                        ])->first();

                        if($newMenuItemsLanguage !== null) {
                            $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                            $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
                            $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
                            $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
                            $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
                        }
			$menuItems[$i]['images']  = ($menuItems[$i]['images']!="")?json_decode($menuItems[$i]['images'],true):null;
			$imageAr1=["mobile_app_images"=>["image" => $defaultImage, "thumb_med" => $defaultImage, "thumb_sml" => $defaultImage, "image_caption" =>  ''],
					"desktop_web_images"=>["web_image" => $defaultImage, "web_thumb_med" => $defaultImage, "web_thumb_sml" => $defaultImage, "web_image_caption" => ''],
					"mobile_web_images"=>["mobile_web_image" =>$defaultImage, "mobile_web_thumb_med" => $defaultImage, "mobile_web_thumb_sml" => $defaultImage, "mobile_web_image_caption" =>  ''],
					"banner_image" => '',
					"thumb_banner_image" => '',
			    	];	
			    if($menuItems[$i]['images']!=null){
				foreach($menuItems[$i]['images'] as $key=>$imageAr){
					$menuItems[$i]['images'][$key] = isset($imageAr[0])?$imageAr:[$imageAr1];  
				}
			    }
			  
                    }
                    $menuSubCat[$subIndex]['menuItems'] = $menuItems;
                }//subcategory For loop
 
            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuSubCat, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    public function labelDetails($menu_id)
    {
        $labelIdRes      = MenuItemLabels::select('id','labels','size','item_id','labels_with_priority')
            ->where('item_id', $menu_id)->first();
        $labelNameData1 = [];
	$priorityArray = null;
        $serving = [0,0];
        if($labelIdRes) {
	     	
            $labelData = $labelIdRes->toArray();
	    $labels_with_priority = $labelData['labels_with_priority'];
	    if($labels_with_priority!=null){
		$labels_with_priority = json_decode($labels_with_priority,true);
		
		foreach($labels_with_priority as $l){
			$priorityArray[$l[1]] = $l[0];	
	        }
	    }	
            $labelIds = strlen($labelData['labels']) ? explode(',', $labelData['labels']) : '';
            if(strpos($labelData['size'], ',') !== false ) {
                $serving  = explode(',', $labelData['size']);
            } else {
                $serving  = [1,1];
            }
            $labelNameData1 = Labels::select('id','name','type')->whereIn('id', $labelIds)->get();
            if($labelNameData1) {
                $labelNameData1 = $labelNameData1->toArray();
            }
        }
	$labelNameData2 = $labelNameData= null;
	if($priorityArray!=null && $labelNameData1!=null){
		foreach($labelNameData1 as $label){
		      if(isset($priorityArray[$label['id']])){
			$index = (int)$priorityArray[$label['id']];
			$index = ($index>0)?($index - 1):0;
			$labelNameData2[$index] = $label;
		      }
		}
		foreach($labelNameData2 as $i=>$label){
			 
			$labelNameData[$i] =  $label;  
		}
	}
	$i = ksort($labelNameData2,1); 
        $labelData = [
            'label' => $labelNameData2 ,
	    'serving_size_min'  => $serving[0] <= $serving[1] ? $serving[0] : $serving[1],
            'serving_size_max'  => $serving[0] > $serving[1] ? $serving[0] : $serving[1],
        ];
	 $labelData1 = [
           'label2' => $labelNameData1,'label' => $labelNameData2,
	    'label1' => $labelNameData,	
	    'labelp' => $priorityArray,	
	    'label_priorty' => $labelNameData,	
            'serving_size_min'  => $serving[0] <= $serving[1] ? $serving[0] : $serving[1],
            'serving_size_max'  => $serving[0] > $serving[1] ? $serving[0] : $serving[1],
        ];
        return $labelData;
    }

    public function getModifiersGroup($menu_id, $language, Request $request) {
        $modifierGroupData = ItemModifierGroup::where('menu_item_id', '=', $menu_id)->select('modifier_groups.*')->orderBy('sort_order', 'ASC')->get()->toArray();

        //$modifierGroupData = null;
        if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $modifierGroupData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $modifierGroupData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
                }
		$modifierGroupData[$index]['to_dependent_group_id'] = $cat['dependent_modifier_group_id'];//$itemModifierGroupLanguage->dependent_modifier_group_id;
                $modifierItems = $this->getModifierItems($cat['id'], $language, $request);
                $modifierGroupData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
        return $modifierGroupData;
    }
    public function getModifiersGroupV2($menu_id, $language, Request $request) {
        $modifierGroupData = ItemModifierGroup::where('menu_item_id', '=', $menu_id)->where('show_as_dropdown', '=', 0)->where('status', '=', 1)->select('modifier_groups.*')->orderBy('sort_order', 'ASC')->get()->toArray();
	$dropDowonData =ItemModifierGroup::where('menu_item_id', '=', $menu_id)->where('status', '=', 1)->where('show_as_dropdown', '=', 1)->select('modifier_groups.*')->orderBy('sort_order', 'ASC')->get()->toArray();;
        //$modifierGroupData = null;
        if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $modifierGroupData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $modifierGroupData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
		    
                }
		$modifierGroupData[$index]['to_dependent_group_id'] = $cat['dependent_modifier_group_id'];//$itemModifierGroupLanguage->dependent_modifier_group_id;
                $modifierItems = $this->getModifierItems($cat['id'], $language, $request);
		$modifierGroupData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
	if (count($dropDowonData)) {
            foreach ($dropDowonData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $dropDowonData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $dropDowonData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItems($cat['id'], $language, $request);
		$dropDowonData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
        return [$modifierGroupData,$dropDowonData];
    }

    public function getModifierItems($group_id, $language, Request $request) {
        $modifierData = ItemModifier::where('modifier_group_id', '=', $group_id)->where('status', '=', 1)->select('modifier_items.*')->orderBy('sort_order', 'ASC')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $ItemModifierLanguage = ItemModifierLanguage::where(['language_id' => $language, 'modifier_item_id' => $cat['id']])->first();
                if ($ItemModifierLanguage !== null) {
                    $modifierData[$index]['modifier_name'] = $ItemModifierLanguage->modifier_name;
                }
                $modifierData[$index]['isSelected']=$cat['is_selected'];
                $modifierData[$index]['price'] = $cat['price'];
		
                if(isset($cat['special_price']) && isset($cat['start_date']) && isset($cat['end_date'])){
                    $special_price=$cat['special_price'];
                    $special_price_start_date=$cat['start_date'];
                    $special_price_end_date=$cat['end_date'];
                    $location_id = $request->header('X-location');
                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                        $modifierData[$index]['price'] = $special_price;
                        $modifierData[$index]['cutout_price'] =$cat['price'];

                        unset($modifierData[$index]['special_price']);
                        unset($modifierData[$index]['start_date']);
                        unset($modifierData[$index]['end_date']);
                    }
                }
		$modifierItems = [];
		$modifierItems1 = DB::select('select id,menu_item_id,modifier_item_id,option_name,option_amount,option_amount,restaurant_id,is_selected,created_at,dependent_modifier_id,dependent_modifier_id as to_dependent_group_id,option_description,status,pos_id from modifier_options where status="1" and modifier_item_id = :id', ['id' => $cat['id']]); 
		 
		$i = 0;
		foreach($modifierItems1 as $itm){  
			$dependent_modifier_id = (int)$itm->dependent_modifier_id;
			$modifierItems[$dependent_modifier_id][] =$itm;
			 
		}
		$modifierData[$index]['dependent_modifier_items'] = (count($modifierItems)==1 && isset($modifierItems[0]))?$modifierItems[0]:$modifierItems;
		 
                $modifierItems = $this->getModifierItemsOptionGroup($cat['id'], $language, $request);
                $modifierData[$index]['modifier_OptionGroup_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }
    public function getModifierItemsOptionGroup($group_id, $language, Request $request) {
        $modifierData = ItemModifierOptionGroup::where('modifier_item_id', '=', $group_id)->select('modifier_item_option_groups.*')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                    'language_id' => $language,
                    'modifier_option_group_id' => $cat['id']
                ])->first();
                if($itemModifierOptionGroupLanguage !== null) {
                    $modifierData[$index]['group_name'] = $itemModifierOptionGroupLanguage->group_name;
                    $modifierData[$index]['prompt'] = $itemModifierOptionGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItemsOptionGroupItem($cat['id'], $language, $request);
                $modifierData[$index]['modifier_OptionGroup_item_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }
    
    public function getModifierItemsOptionGroupItem($group_id, $language, Request $request) {
        $modifierData = ItemModifierOptionGroupItem::where('modifier_option_group_id', '=', $group_id)->select('modifier_item_options.*')->get()->toArray();

        if(count($modifierData)) {
            foreach ($modifierData as $index => $row) {
                $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where(['language_id' => $language, 'modifier_option_id' => $row['id']])->first();
                if ($itemOptionsModifierLanguage !== null) {
                    $modifierData[$index]['modifier_option_name'] = $itemOptionsModifierLanguage->modifier_option_name;
                }
            }
        }
        return $modifierData;
    }

    public function getItemsAdonsWithAddonsGroup($menu_id, $language, Request $request, $locId) {
        $adonsGroupData = ItemAddonGroup::where('menu_item_id', '=', $menu_id)->select('item_addon_groups.*')->get()->toArray();

        if (count($adonsGroupData)) {
            foreach ($adonsGroupData as $index => $cat) {
                // implement language
                $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                    'item_addon_group_id' => $cat['id'],
                    'language_id' => $language
                ])->first();
                if ($itemAddonGroupsLanguage !== null) {
                    $adonsGroupData[$index]['group_name'] = $itemAddonGroupsLanguage->group_name;
                    $adonsGroupData[$index]['prompt'] = $itemAddonGroupsLanguage->prompt;
                }
                $adonItems = $this->getAdonsItems($cat['id'], $language, $request, $locId);

                $adonsGroupData[$index]['adons_items_list'] = $adonItems;
            }
        }
        return $adonsGroupData;
    }

    public function getAdonsItems($group_id, $language, Request $request, $locId) {
        $adonsData = ItemAddons::where('addon_group_id', '=', $group_id)->select('item_addons.*')->get()->toArray();
        $item = null;
        if (count($adonsData)) {
            foreach ($adonsData as $index => $adonsInfo) {
                $item1 = $this->getItemInfoById($adonsInfo['item_id'], $locId, $request, $language);

                if ($item1 !== null) {
                    $item1['adon_details'] = $adonsInfo;
                    $item[] = $item1;
                }

            }
        }
        return $item;
    }

    public function getItemInfoById($id, $locId, Request $request, $language_id) {
        $menuItems1 = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where(['new_menu_items.id' => $id, 'new_menu_items.status' => 1])
            ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        $temp = [];
        $defaultImage = null;
        $menuItems = null;
        if (isset($menuItems1[0])) {
            $menuItems = $menuItems1[0];
            $current_time = $request->query('time') . ':00';

            $deleteFlagNIndex = 0;
            //for ($i = 0; $i < count($menuItems); $i++) {

            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            if (!empty($menuItems['size_price'])) {
                $size_prices = json_decode($menuItems['size_price'][0], true);
                $this->getSizePrice($size_prices, $locId, $current_time);
            }

            $menuItems['size_price'] = $size_prices;
            $menuItems['customizable_data'] = json_decode($menuItems['customizable_data'][0], true);

            $modifierDetails = $this->getModifiersGroup($menuItems['id'], $language_id, $request);
            $menuItems['modifier'] = $modifierDetails;



            // set language items
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $id,
                'language_id' => $language_id,
            ])->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems['name'] = $newMenuItemsLanguage->name;
                $menuItems['description'] = $newMenuItemsLanguage->description;
                $menuItems['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
            if(!empty($menuItems['images'])) {
                $menuItems['images']  = json_decode($menuItems['images'], true);
            }
            //}
        }
        return $menuItems;
    }

    public function getItemsWithSubcatWithLanguage($cat_id, $locId, $userId, $category, $request, $parent = 0, $language) {

        $categoryData = MenuSubCategories::where('menu_category_id', '=', $cat_id)
            ->where('status', '=', 1)
            ->where('parent', '=', $parent);

        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
        if (count($categoryData)) {
            foreach ($categoryData as &$cat) {
                $cat->subcats = $this->getItemsWithSubcat($cat_id, $locId, $userId, $category, $request, $cat->id);
                $menuItems = $this->getMenuItemList($cat_id, $cat->id, $locId, $userId, $category, $request);
                $cat->menu_items_list = $menuItems;
            }
        }
        return $categoryData;
    } 

    public function getMenuItemListV2($cat_id, $sub_cat_id = null, $locId, $userId, $category, $request) {

        $localization_id = $request->header('X-localization');

        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;

        $request_params = [
            'new_menu_items.restaurant_id' => $locId,
            'new_menu_items.menu_category_id' => $cat_id,
            'new_menu_items.menu_sub_category_id' => $sub_cat_id,
            'new_menu_items.status' => 1,
        ];

        $menuItems = array();

        $menuItems = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where($request_params)
            ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        foreach ($menuItems as $index => $menu) {
            $adonsDetails = $this->getItemsAdonsWithAddonsGroup($menu['id'], $language_id, $request, $locId);
            $menuItems[$index]['adons'] = $adonsDetails;
            $modifierDetails = $this->getModifiersGroupV2($menu['id'], $language_id, $request);
            $menuItems[$index]['modifier'] = $modifierDetails[0];
	    $menuItems[$index]['custom_dropdown'] = $modifierDetails[1];	
	    //if(isset())
            $menuItems[$index]['labels'] = $this->labelDetails($menu['id']);

        }
        // Remove menu items already in cart
        $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
        $cartMenuIds = array_column($cartMenuItems, 'menu_id');
        $temp = [];
        $defaultImage = '/images/Product-Image.png';
         
        if ($request->has('time')) {
            $current_time = $request->query('time') . ':00';
        } else {
            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
            if (is_object($currentDateTimeObj)) {
                $current_time = $currentDateTimeObj->format('H:i:00');
            } else {
                $current_time = date('H:i:00');
            }
        }
        $deleteFlagNIndex = 0;
        for ($i = 0; $i < count($menuItems); $i++) {
            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
            if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                $menuItems[$i]['to_delete'] = $i;
                $deleteFlagNIndex = $i - 1;
            }
            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            $size_prices = json_decode($menuItems[$i]['size_price'], true);
            //$this->getSizePrice($size_prices, $locId, $current_time);
	    CommonFunctions::getSizePrice($size_prices, $locId, $current_time);	
            $menuItems[$i]['size_price'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);
	    $menuItems[$i]['image'] = isset($menuItems[$i]['image'])?$menuItems[$i]['image']:$defaultImage;
			$menuItems[$i]['thumb_med'] = isset($menuItems[$i]['thumb_med'])?$menuItems[$i]['thumb_med']:$defaultImage;
			$menuItems[$i]['thumb_sml'] = isset($menuItems[$i]['thumb_sml'])?$menuItems[$i]['thumb_sml']:$defaultImage;
			$menuItems[$i]['web_image'] = isset($menuItems[$i]['web_image'])?$menuItems[$i]['web_image']:$defaultImage;
			$menuItems[$i]['web_thumb_med'] = isset($menuItems[$i]['web_thumb_med'])?$menuItems[$i]['web_thumb_med']:$defaultImage;
			$menuItems[$i]['web_thumb_sml'] = isset($menuItems[$i]['web_thumb_sml'])?$menuItems[$i]['web_thumb_sml']:$defaultImage;
            

            $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
            if ($catkey !== false) {
                $menuItems[$i]['in_cart'] = true;
                $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
            } else {
                $menuItems[$i]['in_cart'] = false;
                $menuItems[$i]['in_cart_quantity'] = 0;
            }
	    $menuItems[$i]['images']  = ($menuItems[$i]['images']!="")?json_decode($menuItems[$i]['images'],true):null;
	    $imageAr1=["mobile_app_images"=>["image" => $defaultImage, "thumb_med" => $defaultImage, "thumb_sml" => $defaultImage, "image_caption" =>  ''],
		        "desktop_web_images"=>["web_image" => $defaultImage, "web_thumb_med" => $defaultImage, "web_thumb_sml" => $defaultImage, "web_image_caption" => ''],
		        "mobile_web_images"=>["mobile_web_image" =>$defaultImage, "mobile_web_thumb_med" => $defaultImage, "mobile_web_thumb_sml" => $defaultImage, "mobile_web_image_caption" =>  ''],
		        "banner_image" => '',
		        "thumb_banner_image" => '',
            	];
	    if($menuItems[$i]['images']!=null){
		foreach($menuItems[$i]['images'] as $key=>$imageAr){
			$menuItems[$i]['images'][$key] = isset($imageAr[0])?$imageAr:[$imageAr1];  
		}
	    }
            
            $menuItems[$i]['priority'] = $i + 1;
           
	    unset($menuItems[$i]['customizable_data_old']);
            unset($menuItems[$i]['thumb_image_med']);
            unset($menuItems[$i]['banner_image']);
            unset($menuItems[$i]['thumb_banner_image_med']);
            // set language description to item fields
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $menuItems[$i]['id'],
                'language_id' => $language_id,
            ])->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
                $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
                $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
        }
        // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
        if ($deleteFlagNIndex) {
            //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
            //array_splice($menuItems, $deleteFlagNIndex, 1);
        }

        return $menuItems;

    }

    public function getDirectParentCatOnlyV2($cat_id, $locId, $userId, $category, $request) {
        return $this->getMenuItemListV2($cat_id, null, $locId, $userId, $category, $request);
    }

    public function getItemsWithSubcatV2($cat_id, $locId, $userId, $category, $request, $parent = 0) {

        $categoryData = MenuSubCategories::where('menu_category_id', '=', $cat_id)
            ->where('status', '=', 1)
            ->where('parent', '=', $parent);

        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
        if (count($categoryData)) {
            foreach ($categoryData as &$cat) {
                $cat->subcats = $this->getItemsWithSubcatV2($cat_id, $locId, $userId, $category, $request, $cat->id);
                $menuItems = $this->getMenuItemListV2($cat_id, $cat->id, $locId, $userId, $category, $request);
                $cat->menu_items_list = $menuItems;
            }
        }
        return $categoryData;
    }

    public function allNormalMenusByParentCat($category, Request $request) {

        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        $with_sub_cat = $request->has('with_sub_cat') ? 1 : 0;
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();

        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {

            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;
            $userId = $userAuth->user_id;
            $menuCat = $menuItems = array();

            $menuCat = MenuCategories::where(['restaurant_id' => $locId, 'slug' => $category, 'status' => 1])->first();

            if ($menuCat) {
                #$parent_id nees to be removed
                if ($request->has('time')) {
                    $current_time = $request->query('time') . ':00';
                    if ($request->has('date')) {
                        $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                    } else {
                        $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                    }
                } else {
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                    if (is_object($currentDateTimeObj)) {
                        $current_time = $currentDateTimeObj->format('H:i:00');
                        $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');
                    } else {
                        $current_time = date('H:i:00');
                        $restcurrentDateTime = date('Y-m-d H:i:00');
                    }
                }

                #for Salaty Iguana hard Code done Bt Rahul gupta 06-11-2018 will changes later
                $show_menu_item = 1;
                /*if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                    #1=> Monday ....
                    $day = date('N', strtotime($restcurrentDateTime));
                    if (in_array($day, array(1, 2, 3, 4, 5))) {
                        $currnt_time = strtotime(date('H:i:00', strtotime($restcurrentDateTime)));
                        if ($currnt_time < strtotime('11:00:00') || $currnt_time > strtotime('16:00:00')) {
                            $show_menu_item = 0;
                        }
                    } else {
                        $show_menu_item = 0;
                    }
                }*/

                if ($with_sub_cat) {

                    $subcats = $this->getItemsWithSubcatV2($menuCat->id, $locId, $userId, $category, $request);
                    $parent_cat_items = $this->getDirectParentCatOnlyV2($menuCat->id, $locId, $userId, $category, $request);
                    #will removed it @11-05-2018 Rg
                    /*if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                        foreach ($parent_cat_items as $key => $subcat) {
                            foreach ($subcat['size_price'] as $key1 => $subsubcat) {
                                $parent_cat_items[$key]['size_price'][$key1]['is_delivery'] = $show_menu_item;
                                $parent_cat_items[$key]['size_price'][$key1]['is_carryout'] = $show_menu_item;
                            }
                        }
                    }*/
                    $menuItems = [
                        'subcats' => $subcats,
                        'menu_items_list' => $parent_cat_items,

                    ];

                } else {

                    $menuItems = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
                        ->where(['new_menu_items.restaurant_id' => $locId, 'new_menu_items.menu_category_id' => $menuCat->id, 'new_menu_items.status' => 1])
                        ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
                        ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
                    foreach ($menuItems as $index => $menu) {
                        $adonsDetails = $this->getItemsAdonsWithAddonsGroup($menu['id'], $language_id, $request, $locId);
                        $menuItems[$index]['adons'] = $adonsDetails;
                        $modifierDetails = $this->getModifiersGroup($menu['id'], $language_id, $request);
                        $menuItems[$index]['modifier'] = $modifierDetails;

                    }
                    // Remove menu items already in cart
                    $cartMenuItems = UserMyBag::select('menu_id', 'quantity')->where(['restaurant_id' => $locId, 'user_id' => $userId, 'status' => 0])->get()->toArray();
                    $cartMenuIds = array_column($cartMenuItems, 'menu_id');

                    $temp = [];
                    $defaultImage = '/images/Product-Image.png';
                     
                    $deleteFlagNIndex = 0;
                    for ($i = 0; $i < count($menuItems); $i++) {
                        // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                        if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                            $menuItems[$i]['to_delete'] = $i;
                            $deleteFlagNIndex = $i - 1;
                        }
                        #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
                        $size_prices = json_decode($menuItems[$i]['size_price'], true);
                        $this->getSizePrice($size_prices, $locId, $current_time);

                        $menuItems[$i]['size_price'] = $size_prices;
                        $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
                        //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);
		
                        $menuItems[$i]['image'] = [
                            "image" => $menuItems[$i]['image'] ?? $defaultImage,
                            "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                            "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                            "image_caption" => $menuItems[$i]['image_caption'] ?? '',
                            "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
                            "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
                            "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
                            "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
                            "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
                            "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
                            "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
                            "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
                            "banner_image" => $menuItems[$i]['banner_image'],
                            "thumb_banner_image" => $menuItems[$i]['banner_image'],
                        ];

                        $catkey = array_search($menuItems[$i]['id'], $cartMenuIds);
                        if ($catkey !== false) {
                            $menuItems[$i]['in_cart'] = true;
                            $menuItems[$i]['in_cart_quantity'] = $cartMenuItems[$catkey]['quantity'];
                        } else {
                            $menuItems[$i]['in_cart'] = false;
                            $menuItems[$i]['in_cart_quantity'] = 0;
                        }

                        $menuItems[$i]['priority'] = $i + 1;
                        unset($menuItems[$i]['customizable_data_old']);
                        unset($menuItems[$i]['thumb_image_med']);
                        unset($menuItems[$i]['banner_image']);
                        unset($menuItems[$i]['thumb_banner_image_med']);

                        #@27-07-2018 By RG Multiple images for KEKI
                        //$get_item_images = CommonFunctions::get_menu_item_imagesV2($menuItems[$i]['id']);
                        //$menuItems[$i]['images'] =  $get_item_images;
			 /*$menuItems[$i]['images']  = ($menuItems[$i]['images']!="")?json_decode($menuItems[$i]['images'],true):null;
			 if(!isset($menuItems[$i]['images']['desktop_web_images']['web_image'])){
				$menuItems[$i]['images'] = [
					"mobile_app_images"=>["image" => $menuItems[$i]['mobile_app_images']['image'] ?? $defaultImage,
					"thumb_med" => $menuItems[$i]['mobile_app_images']['thumb_image_med'] ?? $defaultImage,
					"thumb_sml" => $menuItems[$i]['mobile_app_images']['thumb_image_med'] ?? $defaultImage,
					"image_caption" => $menuItems[$i]['mobile_app_images']['image_caption'] ?? ''],
					"desktop_web_images"=>["web_image" => $menuItems[$i]['desktop_web_images']['web_image'] ?? $defaultImage,
					"web_thumb_med" => $menuItems[$i]['desktop_web_images']['web_thumb_med'] ?? $defaultImage,
					"web_thumb_sml" => $menuItems[$i]['desktop_web_images']['web_thumb_sml'] ?? $defaultImage,
					"web_image_caption" => $menuItems[$i]['desktop_web_images']['web_image_caption'] ?? ''],
					"mobile_web_images"=>["mobile_web_image" => $menuItems[$i]['mobile_web_images']['mobile_web_image'] ?? $defaultImage,
					"mobile_web_thumb_med" => $menuItems[$i]['mobile_web_images']['mobile_web_thumb_med'] ?? $defaultImage,
					"mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_images']['mobile_web_thumb_sml'] ?? $defaultImage,
					"mobile_web_image_caption" => $menuItems[$i]['mobile_web_images']['mobile_web_image_caption'] ?? ''],
					"banner_image" => $menuItems[$i]['banner_image'],
					"thumb_banner_image" => $menuItems[$i]['banner_image'],
			    	];
			 
			  }*/

                        // set language items
                        $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                            'new_menu_items_id' => $menuItems[$i]['id'],
                            'language_id' => $language_id,
                        ])->first();

                        if($newMenuItemsLanguage !== null) {
                            $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                            $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
                            $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
                            $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
                            $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
                        }
			
                    }
                    // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category
                    if ($deleteFlagNIndex) {
                        //Commented on 07-09-2018 By RG as This was code done by Amit as I did not discussed with Amit
                        //array_splice($menuItems, $deleteFlagNIndex, 1);
                    }
                } 
            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuItems,  'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    /**
     * Get all category menus for a restaurant

     * @param $restId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllMenuCategoriesV2(Request $request, $type = 'food_item') {
 
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();

        $error = null;
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }

        if ($userAuth) {
            $status = Config('constants.status_code.STATUS_SUCCESS');
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id =  CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;
            // need to change to location id : done changes
            //$menuCategories = MenuCategories::where(['restaurant_id' => $locationId, 'language_id' => $language_id, 'status' => 1]);
            $menuCategories = MenuCategories::leftJoin('menu_categories_language', 'menu_categories.id', '=', 'menu_categories_language.menu_categories_id')->where(['restaurant_id' => $locationId, 'menu_categories_language.language_id' => $language_id, 'status' => 1])->select('menu_categories.id','menu_categories.restaurant_id','menu_categories.product_type','menu_categories.slug','menu_categories.image_class','menu_categories.status','menu_categories.image_svg','menu_categories.thumb_image_med_svg','menu_categories.image_png','menu_categories.thumb_image_med_png','menu_categories.image_png_selected','menu_categories.thumb_image_med_png_selected','menu_categories.priority','menu_categories.is_delivery','menu_categories.is_carryout',
                'menu_categories_language.name as name','menu_categories_language.description as description','menu_categories_language.sub_description as sub_description','menu_categories_language.language_id');

            $menuCategories = $menuCategories->where('product_type', $type);
            $menuCategories = $menuCategories->orderBy('priority', 'asc')->get();
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuCategories, 'error' => $error, 'xtime' => $me], $status);
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    public function showAllMenuswithCatsV2(Request $request,$type = 'food_item') {

        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            $userId = $userAuth->user_id;
            $menuCat = $menuItems = array();

            $menuCats = MenuCategories::where(['restaurant_id' => $locId, 'product_type' => $type, 'status' => 1])->get();

            if ($menuCats) {

                foreach($menuCats as &$menuCat){
                    $category=$menuCat->slug;

                    #$parent_id nees to be removed
                    if ($request->has('time')) {
                        $current_time = $request->query('time') . ':00';
                        if ($request->has('date')) {
                            $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                        } else {
                            $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                        }
                    } else {
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                        if (is_object($currentDateTimeObj)) {
                            $current_time = $currentDateTimeObj->format('H:i:00');
                            $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');
                        } else {
                            $current_time = date('H:i:00');
                            $restcurrentDateTime = date('Y-m-d H:i:00');
                        }
                    }
                    $subcats = $this->getItemsWithSubcatV2($menuCat->id, $locId, $userId, $category, $request);
                    $parent_cat_items = $this->getDirectParentCatOnlyV2($menuCat->id, $locId, $userId, $category, $request);
                    
                    $menuCat->subcats=$subcats;
                    $menuCat->menu_items_list=$parent_cat_items;
		    $menuCat->image=($menuCat->image=="")?"/images/Category_Sample_Image.png":$menuCat->image;	
		    	

                }

  

                $extra_cats=$this->getUserSpecificItems($request,['last_ordered']);
                if(!empty($extra_cats)){
                    $menuCats->push($extra_cats);
                }


            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuCats, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }


    /************************************all items with cats for old menu**************************************/
    public function showAllMenuswithCats(Request $request,$type = 'food_item') {

        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');

        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }

        if ($userAuth) {
            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            $userId = $userAuth->user_id;
            $menuCat = $menuItems = array();

            $menuCats = MenuCategories::where(['restaurant_id' => $locId, 'product_type' => $type, 'status' => 1])->get();

            if ($menuCats) {

                foreach($menuCats as &$menuCat){
                    $category=$menuCat->slug;

                    #$parent_id nees to be removed
                    if ($request->has('time')) {
                        $current_time = $request->query('time') . ':00';
                        if ($request->has('date')) {
                            $restcurrentDateTime = $request->input('date') . ' ' . $request->query('time') . ':00';
                        } else {
                            $restcurrentDateTime = date('Y-m-d') . ' ' . $request->query('time') . ':00';
                        }
                    } else {
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
                        if (is_object($currentDateTimeObj)) {
                            $current_time = $currentDateTimeObj->format('H:i:00');
                            $restcurrentDateTime = $currentDateTimeObj->format('Y-m-d H:i:00');
                        } else {
                            $current_time = date('H:i:00');
                            $restcurrentDateTime = date('Y-m-d H:i:00');
                        }
                    }

                    #for Salaty Iguana hard Code done Bt Rahul gupta 06-11-2018 will changes later
                    // $show_menu_item = 1;
                    // if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                    //     #1=> Monday ....
                    //     $day = date('N', strtotime($restcurrentDateTime));
                    //     if (in_array($day, array(1, 2, 3, 4, 5))) {
                    //         $currnt_time = strtotime(date('H:i:00', strtotime($restcurrentDateTime)));
                    //         if ($currnt_time < strtotime('11:00:00') || $currnt_time > strtotime('16:00:00')) {
                    //             $show_menu_item = 0;
                    //         }
                    //     } else {
                    //         $show_menu_item = 0;
                    //     }
                    // }

                    $subcats = $this->getItemsWithSubcat($menuCat->id, $locId, $userId, $category, $request);
                    $parent_cat_items = $this->getDirectParentCatOnly($menuCat->id, $locId, $userId, $category, $request);
                    #will removed it @11-05-2018 Rg
                    // if (in_array($menuCat->id, array(128, 134, 140, 146, 152, 130, 136, 142, 148, 154))) {
                    //     foreach ($parent_cat_items as $key => $subcat) {
                    //         foreach ($subcat['size_price'] as $key1 => $subsubcat) {
                    //             $parent_cat_items[$key]['size_price'][$key1]['is_delivery'] = $show_menu_item;
                    //             $parent_cat_items[$key]['size_price'][$key1]['is_carryout'] = $show_menu_item;
                    //         }
                    //     }
                    // }

                    $menuCat->subcats=$subcats;
                    $menuCat->menu_items_list=$parent_cat_items;

                }

            }
            $me = microtime(true) - $ms;
            return response()->json(['data' => $menuCats, 'success_msg' => $success_msg, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
    * $slot_name = 
    * $item_id = 
    * $restaurant_id = 
    * DateTime = $restcurrentDateTime 
    * @return delivery and carryout 0 / 1
    */

    public function check_menu_availablity($extra_params = array()) {
        $return_value = array('is_delivery' => 0, 'is_carryout' => 0);
        $sorted_time_slots = array();
        list($date, $time) = explode(' ', $extra_params['datetime']);       
        #first check custom for the visibilty then check the week time
       
        $custom_date = CustomVisibilityHours::where('restaurant_id', $extra_params['restaurant_id'])->
            where('calendar_date->start_date', '<=', $date)
            ->where('calendar_date->end_date', '>=', $date)
            ->where('slot_code', $extra_params['slot_code'])
            ->orderBy('id', 'DESC')->first();
        if($custom_date && $custom_date->slot_detail) {
            //check if The restaurant is off for this day then send 0 as delivery and carryout
            if($custom_date->is_dayoff == 1) {
                $return_value = array('is_delivery' => 0, 'is_carryout' => 0);
            }else {
                //check time slot
                $time_slots =  json_decode($custom_date->slot_detail, true);
                $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                    return $value['open_time'];
                }));
            }
        }

        ## Week Time slot if any
        if(!$custom_date) {
            $current_day = strtolower(date('D', strtotime($extra_params['datetime'])));
            $week_date = VisibilityHours::where('restaurant_id', $extra_params['restaurant_id'])
                ->where('slot_code', $extra_params['slot_code'])
                ->where('calendar_day', $current_day)
                ->orderBy('id', 'DESC')->first();
            if($week_date && $week_date->slot_detail) {
                //check if The restaurant is off for this day then send 0 as delivery and carryout
                if($week_date->is_dayoff == 1) {
                    $return_value = array('is_delivery' => 0, 'is_carryout' => 0);
                }else {
                    //check time slot
                    $time_slots =  json_decode($week_date->slot_detail, true);                   
                    $sorted_time_slots = array_values(array_sort($time_slots, function ($value) {
                        return $value['open_time'];
                    }));
                }
            }
        }


        if($sorted_time_slots) {
            foreach ($sorted_time_slots as $key => $available_time) {
                #check if time saved in DB is Hour:minute:seconds
                $time_open = explode(':', $available_time['open_time']);
                if(!isset($time_open['2'])) {
                    $available_time['open_time'] =  $available_time['open_time'].':00';
                }
                $time_close = explode(':', $available_time['close_time']);
                if(!isset($time_close['2'])) {
                    $available_time['close_time'] =  $available_time['close_time'].':00';
                }
                if(strtotime($time) >= strtotime($available_time['open_time']) && strtotime($time) <= strtotime($available_time['close_time'])) {
                    $return_value = array('is_delivery' => 1, 'is_carryout' => 1);
                    break;
                }
            }
        }
        return $return_value;
    }



    private  function getLastOrdered($userId,$locId,$request,$for){
        $orderInfo =  UserOrder::select('user_orders.id', 'user_orders.order_type', 'user_orders.status','user_orders.restaurant_id')
            ->where('user_orders.restaurant_id', $locId)->with('details');

        if($for=='guest'){
           $orderInfo =  $orderInfo->where('user_orders.user_auth_id', $userId);
        }else{

            $orderInfo =  $orderInfo->where('user_orders.user_id', $userId);

        }
        $orderInfo=$orderInfo->whereNotIn('status', ['placed'])->latest()->first();
        //print_r($orderInfo);die;
        if($orderInfo){
            $orderDetails=$orderInfo->details;

            $menu_Ids=[];
            foreach ($orderDetails as $order){
                if($order->is_byp==0){
                    $menu_Ids[]=$order->menu_id;
                }
            }
            if(count($menu_Ids)){
                $object = new stdClass();
                $object->id=0;
                $object->restaurant_id =$locId;
                $object->product_type = 'food_item';
                $object->name = 'Last Ordered';
                $object->slug = 'last_ordered';
                $object->image_class = null;
                $object->status = 1;
                $object->image =null;
                $object->image_icon = null;
                $object->image_svg =null;
                $object->thumb_image_med_svg =null;
                $object->image_png_selected = null;
                $object->thumb_image_med_png_selected =null;
                $object->cat_attachment =null;
                $object->priority =0;
                $object->parent =0;
                $object->language_id =1;
                $object->is_delivery =1;
                $object->is_carryout =1;
                $object->is_popular =1;
                $object->is_favourite =1;
                $object->is_popular =1;
                $object->description =null;
                $object->is_popular =1;
                $object->sub_description =null;
                $object->subcats =[];
                if (!$request->has('only_cat')) {
                    $allitems=$this->getMenuItemListForLastOrdered($menu_Ids,$request);
                    if($allitems){
                        $object->menu_items_list=$allitems;

                    }else{
                        $object='';
                    }
                }
                //$object->menu_items_list=$orderInfo;
                return $object;
            }


        }

    }

    public  function getUserSpecificItems($request,$Itemfor) {

        $response='';
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $for='user';
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first(['id','user_id']);
            $userAuth = $userAuth->user_id;
        } else {
            $for='guest';
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first(['id','user_id']);
            $userAuth = $userAuth->id;
        }

        if ($userAuth) {
            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');
            //$userId = $userAuth->user_id;


            if(in_array('last_ordered',$Itemfor)){
                $lastordered=$this->getLastOrdered($userAuth,$locId,$request,$for);
                if(!empty($lastordered)){
                    $response= $lastordered;
                }
            }
        }

        return $response;

    }


    public function getMenuItemListForLastOrdered($menuItemIds,$request) {

        $localization_id = $request->header('X-localization');
        $locId = $request->header('X-location');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;


        $menuItems = array();

        $menuItems = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->whereIn('new_menu_items.id', $menuItemIds)
            ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        foreach ($menuItems as $index => $menu) {
            $adonsDetails = $this->getItemsAdonsWithAddonsGroup($menu['id'], $language_id, $request, $locId);
            $menuItems[$index]['adons'] = $adonsDetails;
            $modifierDetails = $this->getModifiersGroup($menu['id'], $language_id, $request);
            $menuItems[$index]['modifier'] = $modifierDetails;
            $menuItems[$index]['labels'] = $this->labelDetails($menu['id']);

        }

        $temp = [];
        $defaultImage = null;

        if ($request->has('time')) {
            $current_time = $request->query('time') . ':00';
        } else {
            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
            if (is_object($currentDateTimeObj)) {
                $current_time = $currentDateTimeObj->format('H:i:00');
            } else {
                $current_time = date('H:i:00');
            }
        }
        $deleteFlagNIndex = 0;
        for ($i = 0; $i < count($menuItems); $i++) {
            // if image is null, with images are odd (array starts 0), not deleteFlag and only for Non-Pizza category

            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            $size_prices = json_decode($menuItems[$i]['size_price'], true);
            $this->getSizePrice($size_prices, $locId, $current_time);

            $menuItems[$i]['size_price'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            //$menuItems[$i]['customizable_data_old'] = json_decode($menuItems[$i]['customizable_data_old'], true);

            $menuItems[$i]['img'] = [
                "image" => $menuItems[$i]['image'] ?? $defaultImage,
                "thumb_med" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                "thumb_sml" => $menuItems[$i]['thumb_image_med'] ?? $defaultImage,
                "image_caption" => $menuItems[$i]['image_caption'] ?? '',
                "web_image" => $menuItems[$i]['web_image'] ?? $defaultImage,
                "web_thumb_med" => $menuItems[$i]['web_thumb_med'] ?? $defaultImage,
                "web_thumb_sml" => $menuItems[$i]['web_thumb_sml'] ?? $defaultImage,
                "web_image_caption" => $menuItems[$i]['web_image_caption'] ?? '',
                "mobile_web_image" => $menuItems[$i]['mobile_web_image'] ?? $defaultImage,
                "mobile_web_thumb_med" => $menuItems[$i]['mobile_web_thumb_med'] ?? $defaultImage,
                "mobile_web_thumb_sml" => $menuItems[$i]['mobile_web_thumb_sml'] ?? $defaultImage,
                "mobile_web_image_caption" => $menuItems[$i]['mobile_web_image_caption'] ?? '',
                "banner_image" => $menuItems[$i]['banner_image'],
                "thumb_banner_image" => $menuItems[$i]['banner_image'],
            ];

	    $menuItems[$i]['images']  = ($menuItems[$i]['images']!="")?json_decode($menuItems[$i]['images'],true):null;
	    $imageAr1=["mobile_app_images"=>["image" => $defaultImage, "thumb_med" => $defaultImage, "thumb_sml" => $defaultImage, "image_caption" =>  ''],
		        "desktop_web_images"=>["web_image" => $defaultImage, "web_thumb_med" => $defaultImage, "web_thumb_sml" => $defaultImage, "web_image_caption" => ''],
		        "mobile_web_images"=>["mobile_web_image" =>$defaultImage, "mobile_web_thumb_med" => $defaultImage, "mobile_web_thumb_sml" => $defaultImage, "mobile_web_image_caption" =>  ''],
		        "banner_image" => '',
		        "thumb_banner_image" => '',
            	];
	    if($menuItems[$i]['images']!=null){
		foreach($menuItems[$i]['images'] as $key=>$imageAr){
			$menuItems[$i]['images'][$key] = isset($imageAr[0])?$imageAr:[$imageAr1];  
		}
	    }
            $menuItems[$i]['priority'] = $i + 1;
            unset($menuItems[$i]['customizable_data_old']);
            unset($menuItems[$i]['thumb_image_med']);
            unset($menuItems[$i]['banner_image']);
            unset($menuItems[$i]['thumb_banner_image_med']);



            // set language description to item fields
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $menuItems[$i]['id'],
                'language_id' => $language_id,
            ])->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
                $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
                $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
        }

        return $menuItems;

    }
        
}
    
