<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddresses;
use App\Models\UserAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserAddressController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['showAllUserAddresses', 'create']]);
    }
    /**
     * Function to show all User's address,
     * Registered to a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllUserAddresses(Request $request)
    {
        $ms          = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        }
        if ($userAuth) {
            $userId        = $userAuth->user_id;
            //$restaurantId  = $request->header('X-restaurant');
            $restaurantId  = $request->header('X-location');
            $userAddresses = UserAddresses::where(['user_id' => $userId])->select('id', 'user_id', 'fname', 'lname', 'label','surname', 'phone','address1', 'address2', 'street', 'city', 'state', 'zipcode','address_type', 'latitude', 'longitude')->get();
            $user          = User::where(['id' => $userId ])->first();
            //$userAddresses = $user->address ?? [];
            if ($userAddresses) {
		foreach($userAddresses as $i=>$u){
			$u['dob']=$user->dob;	
			$u['mobile']=$user->mobile;
			$u['phone']=$user->phone;
			$u['email']=$user->email;
			$userAddresses[$i]=$u;	
		}	    
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userAddresses, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'No address found for User.', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Function to get show one particular User's address
     * Registered to a restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneUserAddress($userId, $id)
    {
        $ms            = microtime(true);
        $user          = User::find($userId);
        $userAddresses = [];
        if ($user) {
            $error         = '';
            $userAddresses = $user->address()->where('id', $id)->first() ?? [];
            if (!$userAddresses) {
                $error = 'User address not found.';
            }
        } else {
            $error = 'User not found.';
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userAddresses, 'error' => $error, 'xtime' => $me], 200);
    }

    /**
     * Create User's address, for a particular restaurant
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $ms = microtime(true);
        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $userAuth   = '';
        $userID = '';
        $authToken = $request->bearerToken();
        if (!empty($authToken)) {
            $userAuth   = UserAuth::where(['access_token' => $authToken, 'parent_restaurant_id' => $restaurantId])->first();
        }
        if($userAuth) {
            $error = '';
            $userID = $userAuth->user_id;

            $validation = Validator::make($request->all(), [
                //'user_id'       => 'required|exists:users,id',
                //'restaurant_id' => 'required|exists:restaurants,id',
                'fname'          => 'regex:/[a-zA-Z0-9\s]+/|max:255',
                'label'         => 'regex:/[a-zA-Z0-9\s]+/|max:255',
                'address_type'  => 'sometimes|in:billing,shipping',
                'address1'      => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
                'address2'      => 'regex:/[a-zA-Z0-9\s]+/|max:255',
                'street'        => 'regex:/[a-zA-Z0-9\s]+/|max:255',
                //'zipcode'       => 'required|alpha_num|max:255',
		'zipcode'       =>  'regex:/[a-zA-Z0-9\s]+/|max:255',
                'phone'         => 'numeric',
                'latitude'      => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'longitude'     => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
            ]);

            if (!$validation->fails()) {
                $data = $request->all();

                $userAdd = UserAddresses::create($data);
                $userAdd = UserAddresses::find($userAdd->id);
                $userAdd->user_id = $userID;
                $userAdd->save();

                $me = microtime(true) - $ms;

                return response()->json(['data' => $userAdd, 'error' => '', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Delete User's address for a particular restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $ms = microtime(true);
        UserAddresses::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data' => 'Deleted successfully', 'error' => '', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    /**
     * Update an address of a user, for a particular restaurant
     * @param         $id
     * @param         $addId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /*public function update($userId, $id, Request $request)
    {
        // PENDING
        // check if user exists in a restaurant
        return response()->json([$userId,$id]);
        $ms = microtime(true);
        $res = UserAddresses::where(['id' => $id, 'user_id' => $userId])->get();
        $user->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => null, 'xtime' => $me], 200);
    }*/

    public function update($id, Request $request)
    {
        // PENDING
        // check if user exists in a restaurant
        //return response()->json([$userId,$id]);
        $ms = microtime(true);
        //$res = UserAddresses::where(['id' => $id])->get();
        $UserAddresses = UserAddresses::findOrFail($id);
        $UserAddresses->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data' => $UserAddresses, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    /**
     * API to get states list, for sign up process
     * @param         $countryId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStates($countryId, Request $request)
    {
        $ms          = microtime(true);
        $accessToken = $request->bearerToken() ?? '';
        $userAuth    = UserAuth::where(['access_token' => $accessToken])->first();
        if ($userAuth) {
            $cities = State::select('id', 'country_id', 'state as value', 'language_id', 'state_code', 'zone', 'status')->where(['country_id' => $countryId])->get();
            $me     = microtime(true) - $ms;

            return response()->json(['data' => $cities, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * APIs to get cities, for sign up process
     * @param         $stateId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities($stateId, Request $request)
    {
        $ms = microtime(true);
        $accessToken = $request->bearerToken() ?? '';
        $userAuth    = UserAuth::where(['access_token' => $accessToken])->first();
        if ($userAuth) {
            $cities = City::select('id', 'state_id', 'country_id', 'city_name as value', 'locality')->where(['state_id' => $stateId])->get();
            $me = microtime(true) - $ms;

            return response()->json(['data' => $cities, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
}
