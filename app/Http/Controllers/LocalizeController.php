<?php

namespace App\Http\Controllers;

use App\Models\Localization;
use App\Models\Language;
use App\Helpers\CommonFunctions;
use Illuminate\Http\Request;
use App\Models\Restaurant;
class LocalizeController extends Controller
{
    /**
     * Function to return All Pizza Settings of a restaurant
     * @param $id
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllText($id, Request $request)
    {
        $ms = microtime(true);
        $error = '';

        $platform = config('app.userAgent');
        //$langId = config('app.language')['id'];
	$localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $langId = $language_id->id;
        // page type needs to be constant

        //$localizeData = Localization::where(['language_id' => $id ,'status' => 1,'platform'=>'all']) ->orWhere(['platform'=>$platform])->get();
	$localizeData = Localization::where(['language_id' => $langId ,'status' => 1])->get();
        $result = [];
        if($localizeData) {
            for($i=0; $i<count($localizeData); $i++) {
                $result[$localizeData[$i]['key']]=$localizeData[$i]['value'];
            }
        }else{
            $error = 'Not a valid language.';
        }

        $me = microtime(true) - $ms;
        return response()->json(['data'=>$result, 'error' => $error, 'xtime' => $me],Config('constants.status_code.STATUS_SUCCESS'));
    }

    public function getLanguage(Request $request)
    {
        $ms = microtime(true);
        $error = '';
        $languageData = [];
        $restaurant_id = $request->header('X-restaurant');
        if($restaurant_id) {
            $restaurantObj = Restaurant::find($restaurant_id);
            if($restaurantObj) {
                $supportedLanguagesArr = [1];
                if ($restaurantObj->supported_languages != '') {
                    $supportedLanguagesArr = explode(',', $restaurantObj->supported_languages);
                }
                
                $languageData = Language::select('id','language_name','code','short_code')
                ->where('status',1)
                ->whereIn('id',$supportedLanguagesArr)
                ->get();
            }
            
        } else {
            // page type needs to be constant
            $languageData = Language::select('id','language_name','code','short_code')->where('status',1)->get();
        }
        $me = microtime(true) - $ms;
        return response()->json(['data'=>$languageData, 'error' => $error, 'xtime' => $me]);
    }


    
}

