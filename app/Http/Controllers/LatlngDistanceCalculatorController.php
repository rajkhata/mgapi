<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Helpers\CommonFunctions;
use App\Helpers\CanRestaurantDeliver;
use App\Helpers\LocationWorkingDay;
use App\Helpers\LocationTimeSlot;
use Illuminate\Support\Facades\DB;

class LatlngDistanceCalculatorController extends Controller {

    /**
     * Function to return can restaurant deliver on particular address 
     * @param restauraid : $id
     * @param lat : lat
     * @param long : long
     * @return \Illuminate\Http\JsonResponse
     * vertex,inside,outside,boundary
     */
    public function get(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
        $localization_id = $request->header('X-localization');
        //$language_id = CommonFunctions::getLanguageInfo($localization_id);
        //$langId = $language_id = $language_id->id;
        //$langId = config('app.language')['id'];
        $branchSelectFields = ['restaurants.id','restaurant_name','lat','lng','address','landmark','street', 'zipcode','rest_code',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','common_message','common_message_status','common_message_url','source_url', 'facebook_client_id','google_client_id'];


            $restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1 ])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get(); 
    //$restaurantData = restaurant::where([  'parent_restaurant_id' => $id, 'status' => 1, 'takeout' => 1])->get();
        if($request->has('lat') && $request->has('lng')){
            CommonFunctions::$lat1 = $request->lat;
            CommonFunctions::$lon1 = $request->lng;  
            CommonFunctions::$unit="M";
        }else{
            $result=  $this->getRestaurantDetails($restaurantData);
            $me = microtime(true) - $ms; 
            if(isset($result['error'])){
                return response()->json(['data' => [], 'error' => $result['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }elseif(empty($result)){
                 //@08-10-2018 by RG Jira Bug ID: PE-1879
                return response()->json(['data' => $result, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
            return response()->json(['data' => $result, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
       
        //$restaurantData = Restaurant::select('id', 'restaurant_name')->where(['language_id' => $langId ,'status' => 1,'parent_restaurant_id'=>$id])->get()->toArray()[0];
        
       
        $result = [];
        if ($restaurantData) {
            $result = $this->chkdistance($restaurantData);
            if(LocationWorkingDay::$timezoneError){
                $me = microtime(true) - $ms;                
                return response()->json(['data' => [], 'error' => LocationWorkingDay::$timezoneError['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }          
            if(empty($result)) {
                $error ="Restaurant not deliver";
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            //@08-10-2018 by RG Jira Bug ID: PE-1879
            //$error = "Restaurant not found.";
            $error = "";
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    private function chkdistance($restaurantData) {

        $result = [];
        foreach ($restaurantData as $key => $value) {

            CommonFunctions::$lat2 = $value['lat'];
            CommonFunctions::$lon2 = $value['lng'];
            $result[$key]['id'] = $value['id'];
            $result[$key]['restaurant_name'] = $value['restaurant_name'];
            $distance = CommonFunctions::distanceBetweenTwoPointOnEarth();
            $result[$key]['sortby_distance'] = $distance;
            $result[$key]['distance'] = $distance." M";  
            $result[$key]['lat'] = $value['lat'];
            $result[$key]['lng'] = $value['lng'];
            $address = explode("\n", $value['address']);
            $result[$key]['address'] = $value['address'];
            //$result[$key]['address_1'] = isset($address[0])?$address[0]:"";
            $result[$key]['address_2'] = isset( $value['address2'])? $value['address2']:"";
            //$result[$key]['landmark']= $value['landmark'];
            $result[$key]['street'] = $value['street'];
        $result[$key]['city_name'] = isset($value['city_name'])?$value['city_name']:"";
         $result[$key]['country_code'] = isset($value['country_short_name'])?$value['country_short_name']:"";
            $result[$key]['state_name'] = isset($value['state'])?$value['state']:"";
           $result[$key]['country_name'] = isset($value['country_name'])?$value['country_name']:"";
            $result[$key]['zipcode'] = $value['zipcode'];
            $result[$key]['rest_code'] = $value['rest_code'];
            $result[$key]['slug'] = $value['slug'];
            $result[$key]['has_delivery'] = $value['delivery'];
            $result[$key]['has_takeout'] = $value['takeout'];
            $result[$key]['has_dining'] = $value['dinning'];
            $result[$key]['has_reservation'] = $value['reservation'];            
            $result[$key]['delivery_area'] = $value['delivery_area'];
            $result[$key]['minimum_delivery'] = $value['minimum_delivery'];
            $result[$key]['delivery_charge'] = $value['delivery_charge'];
            $result[$key]['is_preorder_accept'] = $value['is_preorder_accept'];
            $result[$key]['phone_no'] = $value['phone'];  
            $result[$key]['pickup_phone_no'] = $value['phone'];  
            $result[$key]['restaurant_url'] = $value['source_url'];  
            $result[$key]['facebook_client_id'] = $value['facebook_client_id'];
            $result[$key]['google_client_id'] = $value['google_client_id']; 
            $result[$key]['gtm_code'] = $value['gtm_code'];
            $result[$key]['google_verification_code'] = $value['google_verification_code'];
            $result[$key]['restaurant_image_name'] = $value['restaurant_image_name'];     
            $result[$key]['rest_code'] = $value['rest_code'];
            $result[$key]['slug'] = $value['slug'];
            $result[$key]['has_delivery'] = $value['delivery'];
            $result[$key]['has_takeout'] = $value['takeout'];
            $result[$key]['common_message'] = $value['common_message']; 
            $result[$key]['common_message_url'] = $value['common_message_url']; 
            $result[$key]['common_message_status'] = $value['common_message_status'];   
            $result[$key]['has_dining'] = $value['dinning'];
            $result[$key]['has_reservation'] = $value['reservation'];            
            $result[$key]['delivery_area'] = $value['delivery_area'];
            $result[$key]['minimum_delivery'] = $value['minimum_delivery'];
            $result[$key]['delivery_charge'] = $value['delivery_charge']; 
            $result[$key]['restaurant_image_name'] = $value['restaurant_image_name']; 
                         
            $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($value['id']);
            $result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($value['id']);
            
            $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($value['id']);

            #timezone By Rahul Gupta @10-07-2018
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['id']));
            if(is_object($currentDate)) {
                $restcurrentDateTime =  $currentDate->format('Y-m-d H:i:s');
            }else {
                $restcurrentDateTime = date('Y-m-d H:i:s');
            }
            $result[$key]['rest_current_datetime'] = $restcurrentDateTime;
            }
            if($result){
                usort($result, function ($item1, $item2) {
                 return $item1['sortby_distance'] <=> $item2['sortby_distance'];
                });
            }
        return $result;
    }

    // changed private to public, as getting used in new restaurant listing from Restaurant's controller
    public function getRestaurantDetails($restaurantData){
        $result = [];
        foreach ($restaurantData as $key => $value) {
            $get_restaurant_setting = CommonFunctions::getRestaurantSetting($value['id']);
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['id']));
            #datetime needed in api by Rahul Gupta 10-07-2018
            if(is_object($currentDate)) {
                $restcurrentDateTime =  $currentDate->format('Y-m-d H:i:s');
            }else {
                $restcurrentDateTime = date('Y-m-d H:i:s');
            }

            if(!empty(CommonFunctions::$timezoneError)){
              return CommonFunctions::$timezoneError;
            }
            $currentDate = $currentDate->format('Y-m-d h:i');
            $result[$key]['id'] = $value['id'];            
            $result[$key]['restaurant_name'] = $value['restaurant_name'];  
            $result[$key]['distance'] = "";  
            $result[$key]['lat'] = $value['lat'];
            $result[$key]['lng'] = $value['lng'];
                
            $address = explode("\n", $value['address']);
            $result[$key]['address'] = $value['address'];
            //$result[$key]['address_1'] = isset($address[0])?$address[0]:"";
            $result[$key]['address_2'] = isset( $value['address2'])? $value['address2']:"";
            //$result[$key]['landmark']= $value['landmark'];
            $result[$key]['street'] = $value['street'];
        $result[$key]['city_name'] = isset($value['city_name'])?$value['city_name']:"";
         $result[$key]['country_code'] = isset($value['country_short_name'])?$value['country_short_name']:"";
            $result[$key]['state_name'] = isset($value['state'])?$value['state']:"";
        $result[$key]['country_name'] = isset($value['country_name'])?$value['country_name']:"";
            $result[$key]['zipcode'] = $value['zipcode'];
            $result[$key]['rest_code'] = $value['rest_code'];
            $result[$key]['slug'] = $value['slug'];
            $result[$key]['has_delivery'] = $value['delivery'];
            $result[$key]['has_takeout'] = $value['takeout'];
            $result[$key]['has_dining'] = $value['dinning'];
            $result[$key]['has_reservation'] = $value['reservation'];            
            $result[$key]['delivery_area'] = $value['delivery_area'];
            $result[$key]['minimum_delivery'] = $value['minimum_delivery'];
            $result[$key]['delivery_charge'] = $value['delivery_charge'];
            $result[$key]['is_preorder_accept'] = $value['is_preorder_accept'];
            $result[$key]['phone_no'] = $value['phone'];  
        $result[$key]['pickup_phone_no'] = $value['phone'];  
        $result[$key]['restaurant_url'] = $value['source_url']; 
        $result[$key]['facebook_client_id'] = $value['facebook_client_id'];
        $result[$key]['google_client_id'] = $value['google_client_id'];
        $result[$key]['gtm_code'] = $value['gtm_code'];
            $result[$key]['google_verification_code'] = $value['google_verification_code'];
        $result[$key]['restaurant_image_name'] = $value['restaurant_image_name'];  
        $result[$key]['common_message'] = $value['common_message'];  
        $result[$key]['common_message_url'] = $value['common_message_url']; 
        $result[$key]['common_message_status'] = $value['common_message_status'];        
            $result[$key]['all_delivery_working_days'] = LocationWorkingDay::getDeliveryWorkingDay($value['id']);
            $result[$key]['all_takeout_working_days'] = LocationWorkingDay::getTakeoutWorkingDay($value['id']);
            $result[$key]['all_days_operational_hours'] = LocationWorkingDay::getOperationalDay($value['id']);
            $result[$key]['is_currently_open_delivery'] = LocationTimeSlot::isRestaurantCurrentlyOpenForDelivery($value['id'],$currentDate);
            $result[$key]['is_currently_open_carryout'] = LocationTimeSlot::isRestaurantCurrentlyOpenForCarryout($value['id'],$currentDate);
            $result[$key]['rest_current_datetime'] = $restcurrentDateTime;
            $result[$key]['contact_address'] = $value['contact_address'];
            $result[$key]['title'] = $value['title'];
            $result[$key]['description'] = $value['description'];
            $result[$key]['meta_tags'] = !empty($value['meta_tags']) ? json_decode($value['meta_tags'], true) : '';
            $result[$key]['gtm_code'] = $value['gtm_code'];
            $result[$key]['google_verification_code'] = $value['google_verification_code'];
            $result[$key]['is_default_outlet'] = $value['is_default_outlet'];
            $resutl[$key]['currency_symbol'] = isset($restaurantData['currency_symbol']) ? $restaurantData['currency_symbol'] : config('constants.currency');
            $resutl[$key]['currency_code']  = isset($restaurantData['currency_code']) ? $restaurantData['currency_code'] : config('constants.currency_code');
            $result[$key]['scripts'] = [
                'header' => $value['header_script'],
                'footer' => $value['footer_script']
            ];
            $result[$key]['social_links'] = [
                [ 'label' => 'facebook', 'link' => $value['facebook_url'] ],
                [ 'label' => 'twitter', 'link' => $value['twitter_url'] ],
                [ 'label' => 'gmail', 'link' => $value['gmail_url'] ],
                [ 'label' => 'pinterest', 'link' => $value['pinterest_url'] ],
                [ 'label' => 'instagram', 'link' => $value['instagram_url'] ],
                [ 'label' => 'yelp', 'link' => $value['yelp_url'] ],
                [ 'label' => 'tripadvisor', 'link' => $value['tripadvisor_url'] ],
                [ 'label' => 'foursquare', 'link' => $value['foursquare_url'] ],
                [ 'label' => 'twitch', 'link' => $value['twitch_url'] ],
                [ 'label' => 'youtube', 'link' => $value['youtube_url'] ],
            ];
            $result[$key]['media'] = [
                'restaurant_logo'   => $value['restaurant_logo_name'],
                'restaurant_image'  => $value['restaurant_image_name'],
                'restaurant_video'  => $value['restaurant_video_name'],
            ];
            $result[$key]['route'] = [];
            $result[$key]['restaurant_services'] = $get_restaurant_setting['restaurant_services'] ?? '';
        }
        return $result;
    }

     /**
     * Function to return can restaurant deliver on particular address 
     * @param restauraid : $id
     * @param lat : lat
     * @param long : long
     * @return 
     * RG Date 07-09-2018 As When we are closing the takeout then Front_end is not able to get Restaurant
     */
    public function get_list(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
    $localization_id = $request->header('X-localization');
        $branchSelectFields = ['restaurants.id','restaurant_name','lat','lng','address','landmark','street', 'zipcode','rest_code',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','common_message','common_message_status','common_message_url','source_url','facebook_client_id','google_client_id','gtm_code','google_verification_code'];


            $restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get(); 
    //$restaurantData = restaurant::where(['parent_restaurant_id' => $id, 'status' => 1])->get();
        if($request->has('lat') && $request->has('lng')){
            CommonFunctions::$lat1 = $request->lat;
            CommonFunctions::$lon1 = $request->lng;  
            CommonFunctions::$unit="M";
        }else{
            $result=  $this->getRestaurantDetails($restaurantData);
        $paymentDetails = DB::select('select id,payment_title,name,payment_method from payment_gateway where status="1" and restaurant_id = :id', ['id' => $id]); 
     
            $me = microtime(true) - $ms; 
            if(isset($result['error'])){
                return response()->json(['data' => [], 'error' => $result['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }elseif(empty($result)){
                return response()->json(['data' => $result, 'error' => "Restaurant not found", 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            return response()->json(['data' => $result, 'error' => "", 'xtime' => $me,'payment_method'=>$paymentDetails], Config('constants.status_code.STATUS_SUCCESS'));
        }
       
        //$restaurantData = Restaurant::select('id', 'restaurant_name')->where(['language_id' => $langId ,'status' => 1,'parent_restaurant_id'=>$id])->get()->toArray()[0];
        $paymentDetails = DB::select('select id,payment_title from payment_gateway where status="1" and restaurant_id = :id', ['id' => $id]); 
       
        $result = [];
        if ($restaurantData) {
            $result = $this->chkdistance($restaurantData);
            if(LocationWorkingDay::$timezoneError){
                $me = microtime(true) - $ms;                
                return response()->json(['data' => [], 'error' => LocationWorkingDay::$timezoneError['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }          
            if(empty($result)) {
                $error ="Restaurant not deliver";
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            $error = "Restaurant not found.";
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me,'payment_method'=>$paymentDetails], Config('constants.status_code.STATUS_SUCCESS'));
    }
    public function checkdeliveryavailable(Request $request, $id) {

        $ms = microtime(true);
        $error = '';
        $platform = config('app.userAgent');
    $localization_id = $request->header('X-localization');
        $branchSelectFields = ['restaurants.id','restaurant_name','lat','lng','address','landmark','street', 'zipcode','rest_code',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','common_message','common_message_status','common_message_url','gtm_code','google_verification_code'];
    $restaurantData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $id, 'restaurants.status' => 1 ])
        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')
        ->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get(); 
    $result = [];
        if ($restaurantData) {
            $result = $this->chkNearestDistance($restaurantData);
            if(LocationWorkingDay::$timezoneError){
                $me = microtime(true) - $ms;                
                return response()->json(['data' => [], 'error' => LocationWorkingDay::$timezoneError['message'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }          
            if(empty($result)) {
                $error ="Restaurant not deliver";
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            $error = "";
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $result, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    
}
