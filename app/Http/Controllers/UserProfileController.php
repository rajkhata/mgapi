<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    /**
     * Function to show all User's address,
     * Registered to a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllUserAddresses($userId)
    {
        $ms            = microtime(true);
        $user          = User::find($userId);
        $userAddresses = [];
        if ($user) {
            $error         = '';
            $userAddresses = $user->address ?? [];
            if (!count($userAddresses)) {
                $error = 'User address not found.';
            }
        } else {
            $error = 'User not found.';
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userAddresses, 'error' => $error, 'xtime' => $me]);
    }

    /**
     * Function to get show one particular User's address
     * Registered to a restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneUserAddress($userId, $id)
    {
        $ms            = microtime(true);
        $user          = User::find($userId);
        $userAddresses = [];
        if ($user) {
            $error         = '';
            $userAddresses = $user->address()->where('id', $id)->first() ?? [];
            if (!$userAddresses) {
                $error = 'User address not found.';
            }
        } else {
            $error = 'User not found.';
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userAddresses, 'error' => $error, 'xtime' => $me]);
    }

    /**
     * Create User's address, for a particular restaurant
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $ms = microtime(true);
        $error = '';
        $this->validate($request, [
            'user_id'       => 'required|exists:users,id',
            'restaurant_id' => 'required|exists:restaurants,id',
            'label'         => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'address1'      => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'address2'      => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'street'        => 'regex:/[a-zA-Z0-9\s]+/|max:255',
            'city'          => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'state'         => 'required|regex:/[a-zA-Z0-9\s]+/|max:255',
            'zipcode'       => 'required|alpha_num|max:255',
            'phone'         => 'numeric',
            'address_type'  => 'required|in:billing,shipping',
            'lat'           => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng'           => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);

        //return response()->json($request->all());
        // check if user is registered to the restaurant
        $userInfo = User::find($request->input('user_id'));
        //return response()->json($userInfo);
        if($request->input('restaurant_id') == $userInfo['restaurant_id']) {
            $user = UserAddresses::create($request->all());
            $status = 201;
        } else {
            $user = [];
            $error = 'User not found.';
            $status = 404;
        }
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => $error, 'xtime' => $me], $status);
    }

    /**
     * Delete User's address for a particular restaurant
     * @param $id
     * @param $addId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($userId, $id)
    {
        $ms = microtime(true);
        UserAddresses::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data' => 'Deleted successfully', 'error' => '', 'xtime' => $me], 200);
    }

    /**
     * Update an address of a user, for a particular restaurant
     * @param         $id
     * @param         $addId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($userId, $id, Request $request)
    {
        // PENDING
        // check if user exists in a restaurant
        return response()->json([$userId,$id]);
        $ms = microtime(true);
        $res = UserAddresses::where(['id' => $id, 'user_id' => $userId])->get();
        $user->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => '', 'xtime' => $me], 200);
    }

}
