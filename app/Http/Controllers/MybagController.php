<?php

namespace App\Http\Controllers;

use App\Helpers\ApiPayment;
use App\Models\Labels;
use App\Models\MenuItem;
use App\Models\MenuItems;
use App\Models\BuildYourOwnPizza;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\UserAuth;
use App\Models\UserMyBag;
use App\Models\ItemAddons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;
use Config;
use App\Models\MybagOptions;
use App\Models\NewMenuItems;
use App\Models\ItemModifierOptionGroup;
use App\Models\ItemModifierOptionGroupItem;
use App\Models\ItemModifier;
use App\Models\ItemModifierGroup;
use App\Models\NewMenuItemsLanguage;
use App\Models\ItemAddonGroup;
use App\Models\ItemAddonGroupsLanguage;
use App\Models\ItemModifierGroupLanguage;
use App\Models\ItemModifierLanguage;
use App\Models\ItemModifierOptionGroupLanguage;
use App\Models\ItemOptionsModifierLanguage;
use App\Helpers\Delivery;
use App\Models\MenuItemLabels;
use App\Helpers\Payment\StripeGateway;
use App\Models\UserOrder;
class MybagController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['getPaymentIntent','getTotalPaymentIntent','showAllUserMyBagItems', 'create', 'showOneOrder','deleteCart','showAllUserMyBagItemsV2', 'cart', 'showOneOrderV2','showOneOrderV2Byw', 'v1_showAllUserMyBagItems','v1_cart','showAllUserMyBagItemsV3','cartV3','cartByw','showAllUserMyBagItemsV2Byw','applyCoupon','removeCoupon','reorder']]);
    }

    /**
     * Get User's MyBag list
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAllUserMyBagItems(Request $request)
    {
        $ms          = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token    = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId     = $userAuth->id;
            $locationId     = $request->header('X-location');
            $restaurantId   = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagList($locationId, $userAuthId);
            if($locationId==53 && $userMyBagItems['orderType']=='delivery' && $userMyBagItems['subTotal']< 100){
                $deliveryCharge = 7;
                $userMyBagItems['service_provider_charge']=0;
            }elseif($locationId==53 && $userMyBagItems['orderType']=='delivery' && $userMyBagItems['subTotal'] >= 100){
                $deliveryCharge = (float)(($userMyBagItems['subTotal']*10)/100);
                $userMyBagItems['service_provider_charge']=0;
            }else{

                $deliveryCharge = $userMyBagItems['delivery_charge'];
            }
            $userMyBagItems['delivery_charge']=$deliveryCharge;

            $me             = microtime(true) - $ms;

            return response()->json(['data' => $userMyBagItems, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField.' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }


    /**
     * @param $size_price_list = array
     * @param $location_id = Branch Id
     * @param $current_time = Restaurant Current Time
     */
    public function getSizePrice(&$size_price_list, $location_id, $current_time) {

        $current_menu_meal = DB::table('menu_meal_types')
            ->whereTime('to_time', ' >= ', $current_time)
            ->whereTime('from_time', '<= ', $current_time)
            ->where('restaurant_id', $location_id)
            ->where('status', 1)
            ->first();
        if ($size_price_list) {
            foreach ($size_price_list as $key => $size) {
                $has_price = false;
                if ((count($size['price']) > 1) || !isset($size['price']['0'])) {

                    // It is price based on menu meal
                    if ($current_menu_meal) {
                        if (isset($size['price'][$current_menu_meal->id])) {

                            $size_price_list[$key]['price'] = $size['price'][$current_menu_meal->id];
                            if (isset($size['available'])) {
                                $size_price_list[$key]['is_delivery'] = $size['available'][$current_menu_meal->id]['is_delivery'];
                                $size_price_list[$key]['is_carryout'] = $size['available'][$current_menu_meal->id]['is_carryout'];
                            } else {
                                // for old data in DB if any
                                $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                            }
                            $has_price = true;
                        }
                    } else {
                        // unset this price item as This is not available in meal type @26-07-2018
                        unset($size_price_list[$key]);
                    }
                } elseif (count($size['price']) == 1) {
                    // It is all price
                    $size_price_list[$key]['price'] = $size['price']['0'];
                    if (isset($size['available'])) {
                        $size_price_list[$key]['is_delivery'] = $size['available']['is_delivery'];
                        $size_price_list[$key]['is_carryout'] = $size['available']['is_carryout'];
                    } else {
                        // for old data in DB if any
                        $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                    }
                    $has_price = true;
                }
                //check if price does not exists then remove it from the array
                if (!$has_price) {
                    unset($size_price_list[$key]);
                }
                unset($size_price_list[$key]['available']);
            }
        }
    }

    /**
     * Get One MyBag item with customization details
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneOrder($id, Request $request)
    {
        $ms          = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth   = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token      = explode(' ', $request->header('Authorization'))[1];
            $userAuth   = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId     = $userAuth->id;
            $locationId     = $request->header('X-location');
            $restaurantId   = $request->header('X-restaurant');
            //$myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'location_id' => $locationId])->first();
            $myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'restaurant_id' => $locationId])->first();
            if ($myBagData) {
                $myBagData['menu_json'] = $myBagData['menu_json'] ? json_decode($myBagData['menu_json'], true) : '';
                if($myBagData['is_byp']) {
                    // BYP
                    $pizzaSetting = BuildYourOwnPizza::where('restaurant_id', $locationId)->get();
                    $preference = 0;
                    foreach ($pizzaSetting as $setting) {

                        $result['restaurant_id'] = $setting->restaurant_id;
                        if ($setting->is_preference) {
                            $result['preferences'][$preference] = [
                                'id'              => $setting['id'],
                                'label'           => $setting['label'],
                                'is_multiselect'  => $setting['is_multiselect'],
                                'is_customizable' => $setting['is_customizable'],
                                'items'           => json_decode($setting['setting'], true),
                            ];
                            /** SET SELECTED @ 03-09-2018 by RG**/
                            $setLabel = $setting->label;
                            if(isset($myBagData['menu_json'][$setLabel])) {
                                foreach ($result['preferences'][$preference]['items'] as &$itm) {
                                    $itm['is_selected'] = 0;
                                }

                                $selectedSetting = $myBagData['menu_json'][$setLabel];

                                $itemArr = array_column($result['preferences'][$preference]['items'], 'label');
                                $index = array_search($selectedSetting, $itemArr);
                                if($index !== FALSE){
                                    $result['preferences'][$preference]['items'][$index]['is_selected'] = 1;
                                }
                            }
                            $preference++;

                            /** SET SELECTED **/


                        } else {
                            if($setting->parent_id>0) {
                                $index = array_search($setting->parent_id, array_column($result['customizable_data'], 'id'));

                                $result['customizable_data'][$index]['has_child'] = 1;
                                $data = [
                                    'id'              => $setting['id'],
                                    'label'           => $setting['label'],
                                    'has_parent'      => 1,
                                    'is_multiselect'  => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items'           => json_decode($setting['setting'], true),
                                ];
                                if($setting['is_customizable'] && $setting->setting!=null) {

                                    if($setting['label'] == 'veg topping') {
                                        $setLabel = 'veg_topping';
                                    } elseif($setting['label'] == 'non-veg topping') {
                                        $setLabel = 'non-veg_topping';
                                    }
                                    if(isset($myBagData['menu_json'][$setLabel])) {
                                        $selectedSetting = $myBagData['menu_json'][$setLabel];
                                        $itemArr = array_column($data['items'], 'label');
                                        foreach($selectedSetting as $selSet) {
                                            $indexTwo = array_search($selSet[$setLabel], $itemArr);
                                            if(isset($selSet[$setLabel])){
                                                $data['items'][$indexTwo]['is_selected'] = 1;
                                                $data['items'][$indexTwo]['side'] = $selSet['side'];
                                                $data['items'][$indexTwo]['quantity'] = $selSet['quantity'];
                                            }
                                        }
                                    }
                                }
                                $result['customizable_data'][$index]['items'][] = $data;
                            } else {
                                $data = [
                                    'id'              => $setting['id'],
                                    'label'           => $setting['label'],
                                    'has_child'       => 0,
                                    'is_multiselect'  => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items'           => json_decode($setting['setting'], true),
                                ];
                                if($setting['is_customizable'] && $setting->setting!=null) {
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');
                                    foreach($selectedSetting as $selSet) {
                                        $index = array_search($selSet[$setting['label']], $itemArr);
                                        if(isset($selSet[$setting['label']])){
                                            $data['items'][$index]['is_selected'] = 1;
                                            $data['items'][$index]['side'] = $selSet['side'];
                                            $data['items'][$index]['quantity'] = $selSet['quantity'];
                                        }
                                    }
                                }
                                //# for non customizable item @ RG 20-07-2018
                                if($setting['is_customizable'] == 0 && $setting->setting!=null) {
                                    // set is selected = 0
                                    foreach ($data['items'] as &$itm) {
                                        $itm['is_selected'] = 0;
                                    }
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');

                                    //foreach($selectedSetting as $selSet) {
                                    $index = array_search($selectedSetting, $itemArr);
                                    if($index !== FALSE){
                                        $data['items'][$index]['is_selected'] = 1;
                                    }
                                    // }
                                }
                                $result['customizable_data'][] = $data;
                            }
                        }
                    }
                    $myBagData['customizable_data'] = $result['customizable_data'];
                    $myBagData['preferences']       = $result['preferences'];
                    $me                             = microtime(true) - $ms;

                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else {
                    // NORMAL MENU
                    $me  = microtime(true) - $ms;
                    // add images
                    $menuItem = MenuItems::where('id', $myBagData['menu_id'])->select('id', 'name','description', 'image','thumb_image_med', 'thumb_image_med', 'image_caption', 'web_image', 'web_thumb_med', 'web_thumb_sml', 'web_image_caption', 'mobile_web_image', 'mobile_web_thumb_med', 'mobile_web_thumb_sml', 'mobile_web_image_caption', 'banner_image','status', 'images','item_other_info','product_type','size_price')->first()->toArray();
                    $defaultImage = null;
                    # Added Image key as It was not sync with category/pizza api
                    $myBagData['image'] = [
                        "image" => $menuItem['image'] ?? $defaultImage,
                        "thumb_med" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "thumb_sml" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "image_caption" => $menuItem['image_caption'] ?? '',
                        "web_image" => $menuItem['web_image'] ?? $defaultImage,
                        "web_thumb_med" => $menuItem['web_thumb_med'] ?? $defaultImage,
                        "web_thumb_sml" => $menuItem['web_thumb_sml'] ?? $defaultImage,
                        "web_image_caption" => $menuItem['web_image_caption'] ?? '',
                        "mobile_web_image" => $menuItem['mobile_web_image'] ?? $defaultImage,
                        "mobile_web_thumb_med" => $menuItem['mobile_web_thumb_med'] ?? $defaultImage,
                        "mobile_web_thumb_sml" => $menuItem['mobile_web_thumb_sml'] ?? $defaultImage,
                        "mobile_web_image_caption" => $menuItem['mobile_web_image_caption'] ?? '',
                        "banner_image" => $menuItem['banner_image'],
                        "thumb_banner_image" => $menuItem['banner_image'],
                    ];
                    $myBagData['name'] =  $menuItem['name'] ?? '';
                    if(!is_null($menuItem['item_other_info']) && !empty($menuItem['item_other_info'])){
                        $myBagData['menu_item_other_info'] = $menuItem['item_other_info'];
                    }else{
                        $myBagData['menu_item_other_info']=NULL;
                    }
                    $myBagData['product_type']=$menuItem['product_type'];
                    $images = array();
                    if($menuItem['images']) {
                        $images = json_decode($menuItem['images'], true);
                    }
                    $myBagData['images'] =  $images;
                    $myBagData['description'] =  $menuItem['description'] ?? '';
                    $myBagData['status'] = $menuItem['status'] ?? '';
                    if($menuItem['product_type']=='gift_card' || $menuItem['product_type']=='product'){
                        #@16-07-2018 By RG
                        if ($request->has('time')) {
                            $current_time = $request->query('time') . ':00';
                        } else {
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                            if (is_object($currentDateTimeObj)) {
                                $current_time = $currentDateTimeObj->format('H:i:00');
                            } else {
                                $current_time = date('H:i:00');
                            }
                        }

                        $size_prices = json_decode($menuItem['size_price'], true);
                        $this->getSizePrice($size_prices, $locationId, $current_time);
                        $myBagData['size_price'] = $size_prices;

                    }


                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => 'Cart item not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /**
     * Get user bag items
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function userMyBagItems(Request $request)
    {
        $ms = microtime(true);

        // Get token
        if ($request->bearerToken('Authorization')) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $userId         = $userAuth->id;
            $userAuthId     = $userAuth->user_id;
            $userMyBagItems = UserMyBag::where(['user_id' => $userId, 'status' => 0])->get();

            $error = '';
            if ($userMyBagItems) {
                $i = 0;
                foreach ($userMyBagItems as $userMyBag) {
                    $menuItemDetail = '';
                    if (isset($userMyBag['menu_json']) && !is_null($userMyBag['menu_json'])) {
                        $tempImage = [
                            "image"         => "/images/mobile/defaultPizza.png",
                            "thumb_med"     => "/images/mobile/defaultPizza.png",
                            "thumb_sml"     => "/images/mobile/defaultPizza.png",
                            "web_image"     => "/images/mobile/defaultPizza.png",
                            "web_thumb_med" => "/images/mobile/defaultPizza.png",
                            "web_thumb_sml" => "/images/mobile/defaultPizza.png",
                        ];
                        $menuJsonData                    = json_decode($userMyBagItems[$i]['menu_json'], true) + ['image' => $tempImage];
                        $userMyBagItems[$i]['menu_json'] = $menuJsonData;
                    } else if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                        $menuItemDetail = MenuItems::select('name', 'description', 'image', 'thumb_image_med', 'image_caption')->find($userMyBag['menu_id']);
                    }
                    $userMyBagItems[$i]['menu_item_detail'] = $menuItemDetail;
                    $i++;
                }
            } else {
                $error = 'Order not found.';
            }
            $me = microtime(true) - $ms;

            return response()->json(['data' => $userMyBagItems, 'error' => $error, 'xtime' => $me]);
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }


    private function promotionalCardHandle($locationId, $userAuthId,$parent_restaurant_id,$userId) {

        /*********************************************************/
        // Salty Ignuana code and logic suggested by Prakash sir , Rahul Prabhakar sir and Team
        //Created 2 fake promotion product for promotion cards as asked to do .
        /*********************************************************/
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $data = [
            'parent_restaurant_id'       => $parent_restaurant_id ?? 0,
            'restaurant_id'         => $locationId,
            'user_id'             => $userId,
            'user_auth_id'        => $userAuthId,
            //'menu_id'             => $menu_id,//dynamicallyget
            'special_instruction' => '',
            // 'quantity'            => $quantity,
            // 'unit_price'          => $unitPrice,
            'total_menu_amount'   =>0,
            'size'                =>0,
            'order_type'          => 0,
            'delivery_charge'     => 0.00,
        ];
        $myselfbonus_amount = 0;
        $recipientbonus_amount = 0;
        $myselfbonus_quantity = 0;
        $recipientbonus_quantity = 0;
        $promotion_amount = 0;
        $promotion_self = 0;
        $promotion_recipient = 0;
        $gift_bag_quantity = 0;

        if ($userBagItems) {
            $i = 0;
            foreach ($userBagItems as $userMyBag) {

                $bag_quantity=$userMyBag['quantity'];
                $menuItemDetail = '';

                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {

                    $menuItemDetail = MenuItems::select('id','product_type')->find($userMyBag['menu_id']);
                    if($menuItemDetail->id) {

                        if($menuItemDetail->product_type=='promotion_self'){
                            $promotion_self=$userMyBag['id'];
                        }else  if($menuItemDetail->product_type=='promotion_recipient'){
                            $promotion_recipient=$userMyBag['id'];
                        }
                        if($menuItemDetail->product_type=='gift_card'){

                            $gift_bag_quantity+=$bag_quantity;
                            $item_otherinfo = !empty($userMyBag['item_other_info']) ? json_decode($userMyBag['item_other_info']) : '';
                            $promotion_applicable=(isset($item_otherinfo->promotion_applicable))?$item_otherinfo->promotion_applicable:0;

                            if($promotion_applicable){

                                $bonus_for=(isset($item_otherinfo->bonus_for) && !empty($item_otherinfo->bonus_for))?$item_otherinfo->bonus_for:'';
                                $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;

                                if($bonus_for=='myself'){
                                    $myselfbonus_amount=$myselfbonus_amount+($promotion_amount * $bag_quantity);
                                    $myselfbonus_quantity+=$bag_quantity;
                                }elseif($bonus_for=='recipient'){
                                    $recipientbonus_amount=$recipientbonus_amount+($promotion_amount * $bag_quantity);
                                    $recipientbonus_quantity+=$bag_quantity;
                                }
                            }
                        }

                    }
                }
                $i++;
            }
        }
        if( $gift_bag_quantity==0){

            /*****************CodeToRemovePromocards********************/
            $id_to_delete=[];
            if($promotion_self!=0){
                $id_to_delete[]=$promotion_self;
            }
            if($promotion_recipient!=0){
                $id_to_delete[]=$promotion_recipient;
            }
            if(count($id_to_delete)){
                UserMyBag::whereIn('id', $id_to_delete)->delete();
            }
            /*****************CodeToRemovePromocards End********************/

        }else{

            /*****************CodeToRemovePromocards********************/
            $id_to_delete=[];
            if($myselfbonus_quantity==0){
                $id_to_delete[]=$promotion_self;
            }
            if($recipientbonus_quantity==0){
                $id_to_delete[]=$promotion_recipient;
            }
            if(count($id_to_delete)){
                UserMyBag::whereIn('id', $id_to_delete)->delete();
            }
            /*****************CodeToRemovePromocards End********************/

            if( $myselfbonus_amount!=0){
                $promoPod=CommonFunctions::promotionProduct($parent_restaurant_id,'promotion_self');
                if($promoPod){
                    if($promotion_self!=0){
                        //update Self Promo Item
                        UserMyBag::where('id', $promotion_self)->update(['quantity' => $myselfbonus_quantity]);
                    }else{
                        //Add  Self Promo Item
                        $data['menu_id']=$promoPod->id;
                        $data['quantity']=$myselfbonus_quantity;
                        $data['unit_price']=$promotion_amount;
                        UserMyBag::create($data);
                    }

                }

            }
            if( $recipientbonus_amount!=0){
                $promoPod=CommonFunctions::promotionProduct($parent_restaurant_id,'promotion_recipient');
                if($promoPod){
                    if($promotion_recipient!=0){
                        //update Recipient Promo Item
                        UserMyBag::where('id', $promotion_recipient)->update(['quantity' => $recipientbonus_quantity]);
                    }else{
                        //Add  Recipient Promo Item
                        $data['menu_id']=$promoPod->id;
                        $data['quantity']=$recipientbonus_quantity;
                        $data['unit_price']=$promotion_amount;
                        UserMyBag::create($data);
                    }
                }

            }

        }

    }



    /**
     * Add to MyBag (Cart), normal menu + customization
     * and/or BYP + customization + restaurant branch id +
     * user id + user auth id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $ms     = microtime(true);
        $error  = null;
        $result = null;
        $status = 200;

        // Verify user token
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp'              => 'sometimes|in:0,1',
                'menu_json'           => 'sometimes',
                'menu_id'             => 'sometimes|exists:menu_items,id',
                'order_type'          => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity'            => 'required|numeric',
            ]);

            if (!$validation->fails()) {
                if($request->post('is_byp')==1 || $request->post('menu_id')>0) {

                    $imageName    = $medImageName = "";
                    $quantity     = $request->post('quantity');
                    $locationId   = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo      = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees=0;
                    $imgdata         = null;
                    $item_other_info=null;
                    $prod_type='';
                    $orderType       = $request->post('order_type') ?? 'delivery';

                    if ($request->input('menu_id')) {
                        $menuInfo      = MenuItems::where('id',$request->input('menu_id'))->first();

                        if(isset($menuInfo) && $menuInfo->product_type=='gift_card') {
                            /***************************SALTY GIFT CARD RELATED*************/

                            $prod_type='gift_card';
                            $promotion_amount=null;


                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                            CommonFunctions::validateGiftCard($request,$quantity,$menuItemPrice);
                            //validating Gift card;

                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $gift_type=($request->has('gift_type') && !empty($request->input('gift_type')) )?$request->input('gift_type'):'egiftcard';

                            $recipient=($request->has('recipient') && !empty($request->input('recipient')) )?$request->input('recipient'):'single';

                            $giftcard_for=($request->has('giftcard_for') && !empty($request->input('giftcard_for')) )?$request->input('giftcard_for'):'recipient';

                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';

                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'gift_type'    => $gift_type,
                                'recipient'    => $recipient,
                                'bonus_for'    => $giftcard_for,
                                'recipient_details'=>$recipientDetails,
                                'promotion_amount'    => $promotion_amount,
                                'promotion_applicable'=>0
                            ];
                            /*

                                                        $promo=CommonFunctions::productPromotions($restaurantId);

                                                        if(!empty($promo) && $promo->amount_or_percent==0){

                                                            $promo_implement=CommonFunctions::productPromoCondition($menuItemPrice,$promo->condition_amount,$promo->condition);

                                                            if($promo_implement){
                                                                $item_other_info['promotion_applicable']=1;
                                                                $item_other_info['promotion_amount']=$promo->discount;
                                                            }

                                                        }*/
                            $item_other_info=json_encode($item_other_info);


                        }else if(isset($menuInfo) && $menuInfo->product_type=='product') {
                            $prod_type='product';
                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;

                            $is_gift_wrapping=($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping')) )?$request->input('is_gift_wrapping'):0;


                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'is_gift_wrapping'      => trim($is_gift_wrapping),
                            ];

                            $item_other_info=json_encode($item_other_info);

                            if($is_gift_wrapping!=0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)){
                                $gift_wrapping_fees= (float)$menuInfo->gift_wrapping_fees;


                            }
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                        }else{


                            $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                            $sizeArrTemp   = array_column($menuSizePrice,'size');
                            $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                            $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                            //return response()->json([$menuInfo,$request->input('menu_id'),$menuSizePrice,$sizeArrTemp,$sizeIndex,$menuItemPrice]);

                        }


                        $imgarray = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);

                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image']           = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] =    $menuInfo->images;
                                $imgdata                      = json_encode($imgarray);
                            }
                        }else {
                            $imgarray['image']           = $menuInfo->web_image;
                            $imgarray['thumb_image_med'] = $menuInfo->web_thumb_med;
                            $imgarray['image_array'] =     $menuInfo->images;
                            $imgdata                      = json_encode($imgarray);
                        }

                    }



                    // add to MyBag
                    if($request->input('menu_id')>0) {
                        $unitPrice       = $menuItemPrice;
                    } else {
                        $unitPrice       = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice+$gift_wrapping_fees);
                    $orderType       = $request->post('order_type') ?? 'delivery';
                    $userAuthId      = $userAuth->id;
                    $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize        = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge  = $restInfo->delivery_charge;
                    $isByp           = false;


                    //@10-09-2018 by RG Calculate Addons-price in case of Normal Customisation Items
                    if(isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                        && $prod_type!='gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price+= $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }

                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if( $prod_type=='gift_card' && !$request->has('giftcard_for')){
                            $item_other_info=json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for=$old_item_otherinfo->bonus_for;
                            $item_other_info=json_encode($item_other_info);

                        }
                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            $result = $myBagItem->delete();
                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size'         => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if(isset($menuData['is_byp']) && $menuData['is_byp'] >0 ){
                                $unitPrice=$myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if( $prod_type=='gift_card'){
                                $myBagItem->unit_price           = $unitPrice ;
                                if($request->has('size')){
                                    $myBagItem->size=$request->post('size');
                                }
                            }
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if(!is_null($menuJsonOld)) { sort($menuJsonOld); }
                                if(!is_null($menuJsonDecoded)) { sort($menuJsonDecoded); }
                                /*sort($menuJsonOld);
                            sort($menuJsonDecoded);*/
                                $menuFlag = false;
                                if(isset($menuData['menu_id']) && $bag->menu_id==$menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif($request->post('is_byp')==1) {
                                    $menuFlag = true;
                                }
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag) {
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id      = $userAuthId;
                                    $myBagItem->user_id           = $userId;
                                    $myBagItem->order_type	  = $orderType;
                                    $myBagItem->quantity          = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = $totalMenuAmount + $bag->total_menu_amount;
                                    //update menu json
                                    $myBagItem->menu_json         = $request->post('menu_json');
                                    $myBagItem->item_other_info   = $item_other_info;
                                    if($request->has('special_instruction')){
                                        $myBagItem->special_instruction   =  $request->post('special_instruction');
                                    }


                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    $bag->delete();
                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
                                $myBagItem->menu_json         = $request->post('menu_json');
                                $myBagItem->quantity          = $quantity;
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->order_type	  = $orderType;
                                $myBagItem->item_other_info   = $item_other_info;
                                if($request->has('special_instruction')){
                                    $myBagItem->special_instruction   =  $request->post('special_instruction');
                                }
                                $myBagItem->save();
                            }
                        }

                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                        $userBagItems = $this->getUserMyBagList($locationId, $userAuthId);
                        if($locationId==53 && $orderType=='delivery' && $userBagItems['subTotal']< 100){
                            $deliveryCharge = 7;
                            $userBagItems['service_provider_charge']=0;
                        }elseif($locationId==53 && $orderType=='delivery' && $userBagItems['subTotal'] >= 100){
                            $deliveryCharge = (float)(($userBagItems['subTotal']*10)/100);
                            $userBagItems['service_provider_charge']=0;
                        }else{
                            $deliveryCharge = $restInfo->delivery_charge;
                        }
                        $userBagItems['delivery_charge']=$deliveryCharge;
                        $me           = microtime(true) - $ms;

                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp                 = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath      = config('constants.image.path.byp');
                            $imageRelPath   = $imagePath . '/thumb/';
                            $image_parts    = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type     = $image_type_aux[1];

                            $img          = str_replace(' ', '+', $image_parts[1]);
                            $data         = base64_decode($img);
                            $uniqId       = uniqid(mt_rand());
                            $imageName    = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1   = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH      = intval($width_orig / 2);
                            $HEIGHT     = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, TRUE);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray                     = array();
                            $imgarray ['image']           = 'images/byp/' . $imageName;
                            $imgarray ['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata                      = json_encode($imgarray);
                        }
                    }

                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id'       => $restaurantId ?? 0,
                        'restaurant_id'         => $locationId,
                        'user_id'             => $userId,
                        'user_auth_id'        => $userAuthId,
                        'menu_id'             => $request->post('menu_id') ?? 0,
                        'is_byp'              => $isByp,
                        'menu_json'           => $request->post('menu_json'),
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity'            => $quantity,
                        'unit_price'          => $unitPrice,
                        'total_menu_amount'   => $totalMenuAmount,
                        'size'                => $itemSize,
                        'order_type'          => $orderType,
                        'delivery_charge'     => $deliveryCharge,
                        'image'               => $imgdata,
                        'status'              => 0,
                        'item_other_info'      => $item_other_info
                    ];


                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id')>0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    }else{
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);


                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if(is_array( $menuJsonOld )){
                                sort($menuJsonOld);
                            }
                            if(is_array( $menuJsonDecoded )){
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id      = $userAuthId;
                                $bag->user_id           = $userId;
                                $bag->order_type	  = $orderType;
                                $bag->quantity          += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result     = $bag;
                                $bagUpdated = true;
                            }


                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if( $prod_type=='gift_card'){
                                $userAuthId      = $userAuth->id;
                                $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    $userBagItems = $this->getUserMyBagList($locationId, $userAuthId);


                    if($locationId==53 && $orderType=='delivery' && $userBagItems['subTotal']< 100){
                        $deliveryCharge = 7;
                        $userBagItems['service_provider_charge']=0;
                    }elseif($locationId==53 && $orderType=='delivery' && $userBagItems['subTotal'] >= 100){
                        $deliveryCharge = (float)(($userBagItems['subTotal']*10)/100);
                        $userBagItems['service_provider_charge']=0;
                    }else{
                        $deliveryCharge = $restInfo->delivery_charge;
                    }
                    $userBagItems['delivery_charge']=$deliveryCharge;
                    $me  = microtime(true) - $ms;




                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }

    private function getUserMyBagList($locationId, $userAuthId) {
        //$userBagItems = UserMyBag::where(['location_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $subTotal = $myselfbonus_amount=0;
        if ($userBagItems) {
            $i = 0;
            $orderType = '';
            foreach ($userBagItems as $userMyBag) {
                $orderType = $userMyBag['order_type'];
                $bag_quantity=$userMyBag['quantity'];
                $orderType = $userMyBag['order_type'];
                $menuItemDetail = '';

                $subTotal += $userMyBag['total_menu_amount'];

                if (isset($userMyBag['menu_json']) && !is_null($userMyBag['menu_json'])) {
                    if($userMyBag['image']) {
                        $userBagItems[$i]['image'] = json_decode($userMyBag['image'], true);
                        $userBagItems[$i]['thumb_med'] = "/images/mobile/defaultPizza.png";
                    }else {
                        $userBagItems[$i]['image']                     = [
                            "image"         => "/images/mobile/defaultPizza.png",
                            "thumb_med"     => "/images/mobile/defaultPizza.png",
                            "thumb_sml"     => "/images/mobile/defaultPizza.png",
                            "web_image"     => "/images/mobile/defaultPizza.png",
                            "web_thumb_med" => "/images/mobile/defaultPizza.png",
                            "web_thumb_sml" => "/images/mobile/defaultPizza.png",
                        ];
                    }
                    $userBagItems[$i]['menu_json'] = json_decode($userBagItems[$i]['menu_json'], true);
                }

                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $menuItemDetail = MenuItems::select('id','product_type','name', 'description', 'image', 'thumb_image_med', 'image_caption','item_other_info')->find($userMyBag['menu_id']);
                    if($menuItemDetail->id) {
                        #@30-07-2018 By RG Multiple images for KEKI
                        $get_item_images = CommonFunctions::get_menu_item_images($menuItemDetail->id);
                        $menuItemDetail['images'] = $get_item_images;
                        $userBagItems[$i]['product_type']=$menuItemDetail->product_type;
                        if($menuItemDetail->product_type=='gift_card'){

                            if(!is_null($menuItemDetail->item_other_info) && !empty($menuItemDetail->item_other_info)){
                                $userBagItems[$i]['menu_item_other_info'] =$menuItemDetail->item_other_info;
                            }else{
                                $userBagItems[$i]['menu_item_other_info']=NULL;
                            }

                            $item_otherinfo = !empty($userMyBag['item_other_info']) ? json_decode($userMyBag['item_other_info']) : '';



                            $promotion_applicable=(isset($item_otherinfo->promotion_applicable))?$item_otherinfo->promotion_applicable:0;

                            if($promotion_applicable){

                                $bonus_for=(isset($item_otherinfo->bonus_for) && !empty($item_otherinfo->bonus_for))?$item_otherinfo->bonus_for:'';

                                $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;

                                if($bonus_for=='myself'){
                                    $myselfbonus_amount=$myselfbonus_amount+($promotion_amount * $bag_quantity);

                                }

                                $userBagItems[$i]['promotion_amount']=$promotion_amount;


                            }



                        }else{
                            $userBagItems[$i]['item_other_info']=$menuItemDetail->item_other_info;
                        }

                    }

                }else{
                    $userBagItems[$i]['product_type']='food_item';   //In case of Menu Id 0

                }
                $userBagItems[$i]['menu_item_detail'] = $menuItemDetail;
                $i++;
            }


        }

        $restaurantInfo = Restaurant::with(array('city' => function($query) {
            $query->select('id', 'sales_tax');
        }))->where('id',$locationId)->select('id','tip','tax','city_tax', 'delivery_charge','city_id','delivery_service_charge', 'minimum_delivery', 'free_delivery')->first();
        $sale_tax = 0.00;
        if($restaurantInfo) {
            if($restaurantInfo->city_tax) {
                $sale_tax = ($restaurantInfo['city'] && $restaurantInfo['city']['sales_tax']) ? $restaurantInfo['city']['sales_tax'] : 0.00;
            }else {
                $sale_tax = $restaurantInfo->tax;
            }
        }

        #@29-08-2018 by RG
        #this charge is applicable only on Delivery Type Orders and if service is enabled
        $service_provider_charge  = 0;
        // $rest_provider_data = Restaurant::select('restaurants.id', 'delivery_provider_id', 'delivery_services.provider_name', 'delivery_service_charge', 'delivery_services.id as del_pro')->join('delivery_services', 'restaurants.delivery_provider_id' , 'delivery_services.id')->where(['restaurants.id' => $locationId, 'delivery_services.status' => '1'])->first();

        // if($rest_provider_data) {
        //     $service_provider_charge = $rest_provider_data->delivery_service_charge;
        // }

        if($restaurantInfo->delivery_service_charge) {
            $service_provider_charge = $restaurantInfo->delivery_service_charge;
        }

        $data = [
            'orderType'     =>$orderType,
            'subTotal'	      => $subTotal,
            'list'            => $userBagItems,
            'tip'             => explode(",", $restaurantInfo->tip),
            'tax'             => $sale_tax,
            'delivery_charge' => $restaurantInfo->delivery_charge,
            'service_provider_charge' =>$service_provider_charge,
            'minimum_delivery' =>$restaurantInfo->minimum_delivery !=null ? $restaurantInfo->minimum_delivery : 0,
            'free_delivery' =>$restaurantInfo->free_delivery,
        ];

        if( $myselfbonus_amount!=0){
            $data['myselfbonus_amount']=$myselfbonus_amount;
            $data['myselfbonus_amount_message']='You will recieve single bonus gift card worth $'.$myselfbonus_amount.'.';

        }

        return $data;
    }

    public function delete($id)
    {
        $ms = microtime(true);
        //Restaurant::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data'=>'Deleted successfully', 'error' => '', 'xtime' => $me], 200);
    }
    public function deleteCart(Request $request)
    {
        $ms = microtime(true);
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            /*if(isset($id)){
                $id = base64_decode($id);
                //UserMyBag::findOrFail($id)->delete();
                UserMyBag::where('id',$id)->delete();
                $code = Config('constants.status_code.STATUS_SUCCESS');
            }*/
            $userAuthId         = $userAuth->id;
            //$userAuthId     = $userAuth->user_id;
            //$userMyBagItems = UserMyBag::where(['user_id' => $userId])->delete();
            //$log = UserMyBag::where('user_auth_id',$userAuthId)->delete();
            $mybagsitems= UserMyBag::where('user_auth_id', '=', $userAuthId)->pluck('id')->toArray();

            MybagOptions::whereIn('bag_id',$mybagsitems)->delete();

            DB::table('user_mybags')->where('user_auth_id', '=', $userAuthId)->delete();
            //UserMyBag::findOrFail($id)->delete();
            $me = microtime(true) - $ms;
            return response()->json(['data'=>'Deleted successfully', 'error' => '', 'xtime' => $me,'id'=>$userAuthId], Config('constants.status_code.STATUS_SUCCESS'));
        }else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }

    public function update($id, Request $request)
    {
        $ms = microtime(true);
        $restaurant = Restaurant::findOrFail($id);
        $restaurant->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data'=>$restaurant, 'error' => '', 'xtime' => $me], 200);
    }

    private function validatePizza($postData)
    {
        // Check if user exists
        $user = User::find($postData['user_id']);

        // Check if restaurant exists
        $rest = Restaurant::find($postData['restaurant_id']);

        $menuJson = $postData['menu_json'];

        // Check if user is registered to the restaurant
        if ($user['restaurant_id'] == $rest['id']) {
            // Get restaurant pizza setting
            $pizzaMenuSettings = BuildYourOwnPizza::where('restaurant_id', $rest['id'])->first();
            $conSettings       = config('constants.pizza_setting');

            $validValues             = [];
            $errorValidation         = [];
            $validValues['quantity'] = array_column(json_decode($pizzaMenuSettings['quantity'], true), 'label');
            $validValues['side']     = array_column(json_decode($pizzaMenuSettings['side'], true), 'label');
            foreach ($conSettings as $sett) {
                if (isset($pizzaMenuSettings[$sett])) {
                    $validValues[$sett] = array_column(json_decode($pizzaMenuSettings[$sett], true), 'label');
                }
                if (isset($menuJson[$sett])) {

                    if (is_array($menuJson[$sett])) {

                        foreach ($menuJson[$sett] as $setting) {
                            $errorArr = [];
                            if (!in_array($setting['quantity'], $validValues['quantity'])) {
                                $errorArr['quantity'] = $setting['quantity'];
                            }
                            if (!in_array($setting['side'], $validValues['side'])) {
                                $errorArr['side'] = $setting['side'];
                            }
                            if (!in_array($setting[$sett], $validValues[$sett]) || isset($errorArr['quantity']) || isset($errorArr['side'])) {
                                $errorArr['label'] = $setting[$sett];
                            }
                            if ($errorArr)
                                $errorValidation['error']['data'][$sett][] = $errorArr;
                        }
                    } else {
                        $postValue = strtolower($menuJson[$sett]);
                        if (!in_array($postValue, $validValues[$sett])) {
                            $errorValidation['error']['data'][$sett] = $postValue;
                        }
                    }
                }
            }
            if (isset($errorValidation['error'])) {
                $errorValidation['error']['message'] = 'There\'s something wrong with your pizza.';
                $postData += $errorValidation;
            } else {
                // success
                // calculate the total price and route to payment gateway
                return $postData;
            }

        } else {
            // user not registered to the restaurant
            $postData['error']['message'] = 'User is not registered to this restaurant.';
        }

        return $postData;
        // Check if pizza settings are acc. to the restaurant
    }

    public function testPayment($id)
    {
        $apiObj       = new ApiPayment();
        $customerAmit = 'cus_Cke9EQcx4Qna9A';
        $customerAlok = 'cus_CnALHYw8MfrtA9';
        $amitCard1    = 'card_1CL8rCF0beTLCNg6XtKu1gS5';
        $amitCard2    = 'card_1CNeDWF0beTLCNg6ZqWXkBmR';
        $alokCard1    = 'card_1CNa1FF0beTLCNg6nLC3N2JB';
        $alokCard2    = 'card_1CNbASF0beTLCNg6hsDhvAd9';
        $chargeId     = 'ch_1CNeCCF0beTLCNg6uar1my2T';

        switch ($id) {
            case 1: // create token
                $data   = [

                ];
                $result = $apiObj->createToken($data);
                break;
            case 2: // create customer
                $data   = [
                    'description' => 'Home Run Inn Customer',
                    'email'       => 'akdubey@bravvura.in',
                    'source'      => 'tok_visa',
                ];
                $result = $apiObj->createCustomer($data);
                break;
            case 3: // get customer
                $result = $apiObj->getCustomer($customerAlok);
                break;
            case 4: // get customer cards
                $result = $apiObj->getCustomerCards($customerAlok);
                break;
            case 5: // create customer cards
                $cardData = [
                    'number'    => '5555555555554444',
                    'exp_month' => '08',
                    'exp_year'  => '29',
                    'cvc'       => '423',
                ];
                $result   = $apiObj->createCustomerCard($customerAmit, $cardData);
                break;
            case 6: // charge a customer card
                $data   = [
                    'customer' => $customerAmit,
                    'card_id'  => $amitCard1,
                    'amount'   => '19.49' * 100,
                ];
                $result = $apiObj->createCharge($data);
                break;
            case 7: // change default customer card
                $result = $apiObj->defaultCustomerCard($customerAmit, $amitCard2);
                break;
            case 8: // get charge details
                $result = $apiObj->getCharge($chargeId);
                break;
            default:
                break;
        }


        return response()->json($result);
    }



    /***********************************Below are V2 API**************************************** */

    public function new_cart(Request $request) {
        $ms = microtime(true);
        $error = null;
        $result = null;
        $status = 200;
        if ($request->has('is_pot') && $request->post('is_pot') == 1) {
            $is_pot_prod = 1;
        } else {
            $is_pot_prod = 0;
        }

        if ($request->has('is_modifier') && $request->post('is_modifier') == 1) {
            $is_modifier = 1;
        } else {
            $is_modifier = 0;
        }

        // Verify user token
        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $field = 'access_token';
            $bearerToken = true;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $field = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp' => 'sometimes|in:0,1',
                'is_pot' => 'sometimes|in:0,1',
                'is_modifier' => 'sometimes|in:0,1',
                'menu_json' => 'sometimes',
                'addon_json' => 'sometimes',
                'menu_id' => 'sometimes|exists:new_menu_items,id',
                'order_type' => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity' => 'required|numeric',
            ]);

            if (!$validation->fails()) {
                if ($request->post('is_byp') == 1 || $request->post('menu_id') > 0 || $is_modifier || $is_pot_prod) {
                    $imageName = $medImageName = "";
                    $quantity = $request->post('quantity');
                    $locationId = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees = 0;
                    $imgdata = null;
                    $item_other_info = null;
                    $prod_type = '';

                    if ($request->input('menu_id')) {
                        $menuInfo = NewMenuItems::where('id', $request->input('menu_id'))->first();

                        if (isset($menuInfo) && $menuInfo->product_type == 'gift_card') {
                            /***************************SALTY GIFT CARD RELATED*************/
                            $prod_type = 'gift_card';
                            $promotion_amount = null;
                            $other_denomination = ($request->has('other_denomination') && !empty($request->input('other_denomination'))) ? $request->input('other_denomination') : null;
                            $other_denomination = trim($other_denomination);
                            if (!is_null($other_denomination) && $other_denomination != 'null' && !empty($other_denomination)) {
                                $menuItemPrice = $other_denomination;
                            } else {
                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp = array_column($menuSizePrice, 'size');
                                $sizeIndex = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                            CommonFunctions::validateGiftCard($request, $quantity, $menuItemPrice);
                            //validating Gift card;
                            $gift_wrapping_message = ($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message'))) ? $request->input('gift_wrapping_message') : null;
                            $gift_type = ($request->has('gift_type') && !empty($request->input('gift_type'))) ? $request->input('gift_type') : 'egiftcard';
                            $recipient = ($request->has('recipient') && !empty($request->input('recipient'))) ? $request->input('recipient') : 'single';
                            $giftcard_for = ($request->has('giftcard_for') && !empty($request->input('giftcard_for'))) ? $request->input('giftcard_for') : 'recipient';
                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';
                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];
                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination' => $other_denomination,
                                'gift_type' => $gift_type,
                                'recipient' => $recipient,
                                'bonus_for' => $giftcard_for,
                                'recipient_details' => $recipientDetails,
                                'promotion_amount' => $promotion_amount,
                                'promotion_applicable' => 0
                            ];

                            $promo = CommonFunctions::productPromotions($restaurantId);

                            if (!empty($promo) && $promo->amount_or_percent == 0) {
                                $promo_implement = CommonFunctions::productPromoCondition($menuItemPrice, $promo->condition_amount, $promo->condition);

                                if ($promo_implement) {
                                    $item_other_info['promotion_applicable'] = 1;
                                    $item_other_info['promotion_amount'] = $promo->discount;
                                }
                            }
                            $item_other_info = json_encode($item_other_info);
                        } else if (isset($menuInfo) && $menuInfo->product_type == 'product') {
                            $prod_type = 'product';
                            $gift_wrapping_message = ($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message'))) ? $request->input('gift_wrapping_message') : null;
                            $other_denomination = ($request->has('other_denomination') && !empty($request->input('other_denomination'))) ? $request->input('other_denomination') : null;
                            $is_gift_wrapping = ($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping'))) ? $request->input('is_gift_wrapping') : 0;

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination' => $other_denomination,
                                'is_gift_wrapping' => trim($is_gift_wrapping),
                            ];

                            $item_other_info = json_encode($item_other_info);

                            if ($is_gift_wrapping != 0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)) {
                                $gift_wrapping_fees = (float)$menuInfo->gift_wrapping_fees;
                            }
                            $other_denomination = trim($other_denomination);
                            if (!is_null($other_denomination) && $other_denomination != 'null' && !empty($other_denomination)) {
                                $menuItemPrice = $other_denomination;
                            } else {
                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp = array_column($menuSizePrice, 'size');
                                $sizeIndex = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                            }
                        } else {
                            if ($is_pot_prod) {
                                $menuItemPrice = 0; //As discussed with prakash sir
                            } else {
                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp = array_column($menuSizePrice, 'size');
                                $sizeIndex = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                //return response()->json([$menuInfo,$request->input('menu_id'),$menuSizePrice,$sizeArrTemp,$sizeIndex,$menuItemPrice]);
                            }
                        }

                        $imgarray = array();
                        if (!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);

                            if ($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image'] = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] = $menuInfo->images;
                                $imgdata = json_encode($imgarray);
                            }
                        } else {
                            $imgarray['image'] = $menuInfo->web_image;
                            $imgarray['thumb_image_med'] = $menuInfo->web_thumb_med;
                            $imgarray['image_array'] = $menuInfo->images;
                            $imgdata = json_encode($imgarray);
                        }
                    }

                    // add to MyBag
                    if ($request->input('menu_id') > 0) {
                        $unitPrice = $menuItemPrice;
                    } else {
                        $unitPrice = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice + $gift_wrapping_fees);
                    $orderType = $request->post('order_type') ?? 'delivery';
                    $userAuthId = $userAuth->id;
                    $userId = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge = $restInfo->delivery_charge;
                    $isByp = false;

                    $addonJsonDecoded = json_decode($request->post('addon_json'), true) ?? null;
                    $addon_array = [];
                    if (!isset($menuData['bag_id']) && $is_pot_prod && $addonJsonDecoded) {
                        $addon_price = 0;
                        foreach ($addonJsonDecoded as $addon_menu) {
                            if ($addon_menu['price'] && $addon_menu['id'] && $addon_menu['quantity']) {
                                $addonInfo = ItemAddons::where('id', $addon_menu['id'])->first(); //
                                //add more validation to it
                                if ($addonInfo) {
                                    $addon_price += $addonInfo->addon_price * $addon_menu['quantity'];
                                    $addon_array[] = [
                                        //  'bag_id'=>,
                                        //addon_group_id
                                        //menu_item_id
                                        'option_name' => $addonInfo->name,
                                        'option_id' => $addonInfo->id,
                                        'price' => $addonInfo->addon_price,
                                        'quantity' => $addon_menu['quantity'],
                                        'total_amount' => $addonInfo->addon_price * $addon_menu['quantity'],
                                    ];
                                }
                            }
                        }
                        $unitPrice = $addon_price;
                        $totalMenuAmount = ($quantity * $addon_price);

                    } else if ($is_modifier && $menuJsonDecoded) {
                        $customization_price = 0;
                        if (isset($menuJsonDecoded['modifier_items'])) {

                            foreach ($menuJsonDecoded['modifier_items'] as $premenu) {
                                $modifier_item_id = $premenu['id'];
                                $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                                if ($modifierInfo) {
                                    $customization_price += ($modifierInfo->price * $quantity);
                                }
                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as $modifier_items_list) {
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as $modifier_OptionGroup) {
                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as $modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();

                                                        $customization_price += $itemModifierOptionGroupItemObj->price * modifier_OptionGroup_item['quantity'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                // if(isset($premenu['modifier_options']) && count($premenu['modifier_options'])) {
                                //     foreach ($premenu['modifier_options'] as $cust_menu) {
                                //         $customization_price+= $cust_menu['price'] * $quantity;

                                //     }
                                // }
                            }

                        }

                        $totalMenuAmount = $totalMenuAmount + $customization_price;
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                               && $prod_type != 'gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if ($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if ($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price += $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount = $totalMenuAmount + $customization_price;
                    }

                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if ($prod_type == 'gift_card' && !$request->has('giftcard_for')) {
                            $item_other_info = json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for = $old_item_otherinfo->bonus_for;
                            $item_other_info = json_encode($item_other_info);

                        }

                        if ($is_pot_prod) {
                            $addon_options = MybagOptions::where('bag_id', $myBagItem->id)->get();
                            if ($addon_options) {
                                $addon_price = 0;
                                foreach ($addon_options as $addon_menu) {
                                    $addon_price += $addon_menu->price * $addon_menu->quantity;
                                }

                                $unitPrice = $addon_price;

                                $totalMenuAmount = ($quantity * $addon_price);

                            }

                        }
                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            if ($is_pot_prod) {
                                MybagOptions::where('bag_id', $myBagItem->id)->delete();
                            }
                            $result = $myBagItem->delete();
                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size' => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if (isset($menuData['is_byp']) && $menuData['is_byp'] > 0) {
                                $unitPrice = $myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if ($prod_type == 'gift_card') {
                                $myBagItem->unit_price = $unitPrice;
                                if ($request->has('size')) {
                                    $myBagItem->size = $request->post('size');
                                }
                            }
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if (!is_null($menuJsonOld)) {
                                    sort($menuJsonOld);
                                }
                                if (!is_null($menuJsonDecoded)) {
                                    sort($menuJsonDecoded);
                                }
                                /*sort($menuJsonOld);
                            sort($menuJsonDecoded);*/
                                $menuFlag = false;
                                if (isset($menuData['menu_id']) && $bag->menu_id == $menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif ($request->post('is_byp') == 1) {
                                    $menuFlag = true;
                                }
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag && !$is_pot_prod) {
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id = $userAuthId;
                                    $myBagItem->user_id = $userId;
                                    $myBagItem->quantity = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = $totalMenuAmount + $bag->total_menu_amount;
                                    //update menu json
                                    $myBagItem->menu_json = $request->post('menu_json');
                                    $myBagItem->item_other_info = $item_other_info;
                                    if ($request->has('special_instruction')) {
                                        $myBagItem->special_instruction = $request->post('special_instruction');
                                    }

                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    if ($is_pot_prod) {
                                        MybagOptions::where('bag_id', $bag->id)->delete();
                                    }
                                    $bag->delete();

                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
                                $myBagItem->menu_json = $request->post('menu_json');
                                $myBagItem->quantity = $quantity;
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->item_other_info = $item_other_info;
                                if ($request->has('special_instruction')) {
                                    $myBagItem->special_instruction = $request->post('special_instruction');
                                }
                                $myBagItem->save();
                            }
                        }

                        if ($prod_type == 'gift_card') {
                            $userAuthId = $userAuth->id;
                            $userId = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }

                        $userBagItems = $this->getUserMyBagListV2($locationId, $userAuthId,  $request);
                        $me = microtime(true) - $ms;

                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath = config('constants.image.path.byp');
                            $imageRelPath = $imagePath . '/thumb/';
                            $image_parts = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type = $image_type_aux[1];

                            $img = str_replace(' ', '+', $image_parts[1]);
                            $data = base64_decode($img);
                            $uniqId = uniqid(mt_rand());
                            $imageName = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1 = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH = intval($width_orig / 2);
                            $HEIGHT = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, true);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray = array();
                            $imgarray['image'] = 'images/byp/' . $imageName;
                            $imgarray['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata = json_encode($imgarray);
                        }
                    }

                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id' => $restaurantId ?? 0,
                        'restaurant_id' => $locationId,
                        'user_id' => $userId,
                        'user_auth_id' => $userAuthId,
                        'menu_id' => $request->post('menu_id') ?? 0,
                        'is_byp' => $isByp,
                        'is_pot' => $is_pot_prod,
                        'menu_json' => $request->post('menu_json'),
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity' => $quantity,
                        'unit_price' => $unitPrice,
                        'total_menu_amount' => $totalMenuAmount,
                        'size' => $itemSize,
                        'order_type' => $orderType,
                        'delivery_charge' => $deliveryCharge,
                        'image' => $imgdata,
                        'status' => 0,
                        'item_other_info' => $item_other_info
                    ];

                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id') > 0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size' => $itemSize, 'restaurant_id' => $locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    } else {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size' => $itemSize, 'restaurant_id' => $locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);
                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if (is_array($menuJsonOld)) {
                                sort($menuJsonOld);
                            }
                            if (is_array($menuJsonDecoded)) {
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded && !$is_pot_prod) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id = $userAuthId;
                                $bag->user_id = $userId;
                                $bag->quantity += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result = $bag;
                                $bagUpdated = true;
                            }
                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if ($prod_type == 'gift_card') {
                                $userAuthId = $userAuth->id;
                                $userId = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if ($prod_type == 'gift_card') {
                            $userAuthId = $userAuth->id;
                            $userId = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    if (!isset($menuData['bag_id']) && $result && $is_pot_prod && count($addon_array)) {
                        foreach ($addon_array as &$addon) {
                            $addon['bag_id'] = $result->id;
                        }
                        MybagOptions::insert($addon_array);
                    }
                    $userBagItems = $this->getUserMyBagListV2($locationId, $userAuthId,  $request);
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }


    public function cart(Request $request)
    {
        $ms     = microtime(true);
        $error  = null;
        $result = null;
        $status = 200;
        if($request->has('is_pot') && $request->post('is_pot') == 1){
            $is_pot_prod=1;
        }else{
            $is_pot_prod=0;
        }

        if($request->has('is_modifier') && $request->post('is_modifier') == 1){
            $is_modifier=1;
        }else{
            $is_modifier=0;
        }



        // Verify user token
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp'              => 'sometimes|in:0,1',
                'is_pot'              => 'sometimes|in:0,1',
                'is_modifier'              => 'sometimes|in:0,1',
                'menu_json'           => 'sometimes',
                'addon_json'           => 'sometimes',
                'menu_id'             => 'sometimes|exists:new_menu_items,id',
                'order_type'          => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity'            => 'required|numeric',
            ]);

            if (!$validation->fails()) {
                if(  $request->post('is_byp')==1 || $request->post('menu_id')>0 ||  $is_modifier || $is_pot_prod ) {

                    $imageName    = $medImageName = "";
                    $quantity     = $request->post('quantity');
                    $locationId   = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo      = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees=0;
                    $imgdata         = null;
                    $item_other_info=null;
                    $prod_type='';
                    $subTotal = 0;

                    if ($request->input('menu_id')) {
                        $menuInfo      = NewMenuItems::where('id',$request->input('menu_id'))->first();

                        if(isset($menuInfo) && $menuInfo->product_type=='gift_card') {
                            ####################SALTY GIFT CARD RELATED########################

                            $prod_type='gift_card';
                            $promotion_amount=null;


                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                                if(isset($menuSizePrice[$sizeIndex]['special_price']) && isset($menuSizePrice[$sizeIndex]['start_date']) && isset($menuSizePrice[$sizeIndex]['end_date'])){
                                    $special_price=$menuSizePrice[$sizeIndex]['special_price'];
                                    $special_price_start_date=$menuSizePrice[$sizeIndex]['start_date'];
                                    $special_price_end_date=$menuSizePrice[$sizeIndex]['end_date'];

                                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' =>date('Y-m-d H:i:s')));
                                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                                        $menuItemPrice = $special_price;
                                    }
                                }
                            }

                            CommonFunctions::validateGiftCard($request,$quantity,$menuItemPrice);
                            //validating Gift card;

                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $gift_type=($request->has('gift_type') && !empty($request->input('gift_type')) )?$request->input('gift_type'):'egiftcard';

                            $recipient=($request->has('recipient') && !empty($request->input('recipient')) )?$request->input('recipient'):'single';

                            $giftcard_for=($request->has('giftcard_for') && !empty($request->input('giftcard_for')) )?$request->input('giftcard_for'):'recipient';

                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';

                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'gift_type'    => $gift_type,
                                'recipient'    => $recipient,
                                'bonus_for'    => $giftcard_for,
                                'recipient_details'=>$recipientDetails,
                                'promotion_amount'    => $promotion_amount,
                                'promotion_applicable'=>0
                            ];

                            /* $promo=CommonFunctions::productPromotions($restaurantId);

                             if(!empty($promo) && $promo->amount_or_percent==0){

                                 $promo_implement=CommonFunctions::productPromoCondition($menuItemPrice,$promo->condition_amount,$promo->condition);

                                 if($promo_implement){
                                     $item_other_info['promotion_applicable']=1;
                                     $item_other_info['promotion_amount']=$promo->discount;
                                 }

                             }*/
                            $item_other_info=json_encode($item_other_info);


                        }else if(isset($menuInfo) && $menuInfo->product_type=='product') {
                            $prod_type='product';
                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;

                            $is_gift_wrapping=($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping')) )?$request->input('is_gift_wrapping'):0;


                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'is_gift_wrapping'      => trim($is_gift_wrapping),
                            ];

                            $item_other_info=json_encode($item_other_info);

                            if($is_gift_wrapping!=0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)){
                                $gift_wrapping_fees= (float)$menuInfo->gift_wrapping_fees;


                            }
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                if(isset($menuSizePrice[$sizeIndex]['special_price']) && isset($menuSizePrice[$sizeIndex]['start_date']) && isset($menuSizePrice[$sizeIndex]['end_date'])){
                                    $special_price=$menuSizePrice[$sizeIndex]['special_price'];
                                    $special_price_start_date=$menuSizePrice[$sizeIndex]['start_date'];
                                    $special_price_end_date=$menuSizePrice[$sizeIndex]['end_date'];

                                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' =>date('Y-m-d H:i:s')));
                                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                                        $menuItemPrice = $special_price;
                                    }
                                }

                            }

                        }else{

                            if($is_pot_prod){
                                $menuItemPrice=0; //As discussed with prakash sir
                            }else{
                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                if(isset($menuSizePrice[$sizeIndex]['special_price']) && isset($menuSizePrice[$sizeIndex]['start_date']) && isset($menuSizePrice[$sizeIndex]['end_date'])){

                                    $special_price=$menuSizePrice[$sizeIndex]['special_price'];
                                    $special_price_start_date=$menuSizePrice[$sizeIndex]['start_date'];
                                    $special_price_end_date=$menuSizePrice[$sizeIndex]['end_date'];

                                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' =>date('Y-m-d H:i:s')));
                                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){

                                        $menuItemPrice = $special_price;
                                    }
                                }
                                //return response()->json([$menuInfo,$request->input('menu_id'),$menuSizePrice,$sizeArrTemp,$sizeIndex,$menuItemPrice]);

                            }

                        }

                        $imgarray = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);

                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image']           = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] =    $menuInfo->images;
                                $imgdata                      = json_encode($imgarray);
                            }
                        }else {
                            $imgarray['image']           = $menuInfo->web_image;
                            $imgarray['thumb_image_med'] = $menuInfo->web_thumb_med;
                            $imgarray['image_array'] =     $menuInfo->images;
                            $imgdata                      = json_encode($imgarray);
                        }

                    }



                    // add to MyBag
                    if($request->input('menu_id')>0) {
                        $unitPrice       = $menuItemPrice;
                    } else {
                        $unitPrice       = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice+$gift_wrapping_fees);
                    $orderType       = $request->post('order_type') ?? 'delivery';
                    $userAuthId      = $userAuth->id;
                    $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize        = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge  = $restInfo->delivery_charge;
                    $isByp           = false;

                    $addonJsonDecoded = json_decode($request->post('addon_json'), true) ?? null;
                    $addon_array=[];
                    if($is_pot_prod && $addonJsonDecoded ) {
                        $addon_price = 0;
                        foreach ($addonJsonDecoded as $addon_menu) {
                            if($addon_menu['price'] && $addon_menu['id'] && $addon_menu['quantity']) {
                                $addonInfo      = ItemAddons::where('id',$addon_menu['id'])->first(); //
                                //add more validation to it
                                if( $addonInfo ){
                                    $addon_price+= $addonInfo->addon_price * $addon_menu['quantity'];
                                    $addon_array[]=[
                                        //  'bag_id'=>,
                                        //addon_group_id
                                        //menu_item_id
                                        'option_name'=>$addonInfo->name,
                                        'option_id'=>$addonInfo->id,
                                        'price'=>$addonInfo->addon_price,
                                        'quantity'=>$addon_menu['quantity'],
                                        'total_amount'=>$addonInfo->addon_price * $addon_menu['quantity'],
                                        'bag_id'=>(isset($menuData['bag_id']) && !empty($menuData['bag_id']))?$menuData['bag_id']:null
                                    ];
                                }


                            }
                        }
                        $unitPrice=$addon_price;

                        $totalMenuAmount  =($quantity* $addon_price);

                    }else if($is_modifier && $menuJsonDecoded ) {
                        $customization_price = 0;
                        if(isset($menuJsonDecoded['modifier_items'])){

                            foreach ($menuJsonDecoded['modifier_items'] as $premenu) {
                                $modifier_item_id=$premenu['id'];
                                $modifierInfo      = ItemModifier::where('id',$premenu['id'])->first();
                                if( $modifierInfo ){
                                    $customization_price+= ($modifierInfo->price*$quantity);

                                }

                                // if(isset($premenu['modifier_options']) && count($premenu['modifier_options'])) {
                                //     foreach ($premenu['modifier_options'] as $cust_menu) {
                                //         $customization_price+= $cust_menu['price'] * $quantity;

                                //     }
                                // }
                            }

                        }

                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }else if(isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                             && $prod_type!='gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price+= $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }

                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if( $prod_type=='gift_card' && !$request->has('giftcard_for')){
                            $item_other_info=json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for=$old_item_otherinfo->bonus_for;
                            $item_other_info=json_encode($item_other_info);

                        }



                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            if($is_pot_prod) {
                                MybagOptions::where('bag_id',$myBagItem->id)->delete();
                            }
                            $result = $myBagItem->delete();


                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size'         => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if(isset($menuData['is_byp']) && $menuData['is_byp'] >0 ){
                                $unitPrice=$myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if( $prod_type=='gift_card'){
                                $myBagItem->unit_price           = $unitPrice ;
                                if($request->has('size')){
                                    $myBagItem->size=$request->post('size');
                                }
                            }
                            if($is_pot_prod) {



                                if(count($addon_array)>0){
                                    MybagOptions::where('bag_id',$myBagItem->id)->delete();
                                    MybagOptions::insert($addon_array);
                                }else{
                                    $unitPrice=$myBagItem->unit_price;
                                    $totalMenuAmount = $quantity * ($unitPrice);

                                }



                            }
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if(!is_null($menuJsonOld)) { sort($menuJsonOld); }
                                if(!is_null($menuJsonDecoded)) { sort($menuJsonDecoded); }
                                /*sort($menuJsonOld);
                            sort($menuJsonDecoded);*/
                                $menuFlag = false;
                                if(isset($menuData['menu_id']) && $bag->menu_id==$menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif($request->post('is_byp')==1) {
                                    $menuFlag = true;
                                }
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag && !$is_pot_prod ) {
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id      = $userAuthId;
                                    $myBagItem->user_id           = $userId;
                                    if($is_pot_prod && count($addon_array)>0) {
                                        $myBagItem->unit_price = $unitPrice;
                                    }
                                    $myBagItem->quantity          = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = $totalMenuAmount + $bag->total_menu_amount;
                                    //update menu json
                                    $myBagItem->menu_json         = $request->post('menu_json');
                                    $myBagItem->item_other_info   = $item_other_info;
                                    if($request->has('special_instruction')){
                                        $myBagItem->special_instruction   =  $request->post('special_instruction');
                                    }


                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    if($is_pot_prod) {
                                        MybagOptions::where('bag_id',$bag->id)->delete();
                                    }
                                    $bag->delete();

                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
                                $myBagItem->menu_json         = $request->post('menu_json');
                                $myBagItem->quantity          = $quantity;
                                if($is_pot_prod && count($addon_array)>0) {
                                    $myBagItem->unit_price = $unitPrice;
                                }
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->item_other_info   = $item_other_info;
                                if($request->has('special_instruction')){
                                    $myBagItem->special_instruction   =  $request->post('special_instruction');
                                }
                                $myBagItem->save();
                            }
                        }
                        $subTotal 	    +=$totalMenuAmount;
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }

                        $userBagItems = $this->getUserMyBagListV2($locationId, $userAuthId,  $request);
                        $me           = microtime(true) - $ms;

                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp                 = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath      = config('constants.image.path.byp');
                            $imageRelPath   = $imagePath . '/thumb/';
                            $image_parts    = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type     = $image_type_aux[1];

                            $img          = str_replace(' ', '+', $image_parts[1]);
                            $data         = base64_decode($img);
                            $uniqId       = uniqid(mt_rand());
                            $imageName    = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1   = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH      = intval($width_orig / 2);
                            $HEIGHT     = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, TRUE);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray                     = array();
                            $imgarray ['image']           = 'images/byp/' . $imageName;
                            $imgarray ['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata                      = json_encode($imgarray);
                        }
                    }

                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id'       => $restaurantId ?? 0,
                        'restaurant_id'         => $locationId,
                        'user_id'             => $userId,
                        'user_auth_id'        => $userAuthId,
                        'menu_id'             => $request->post('menu_id') ?? 0,
                        'is_byp'              => $isByp,
                        'is_pot'              => $is_pot_prod,
                        'menu_json'           => $request->post('menu_json'),
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity'            => $quantity,
                        'unit_price'          => $unitPrice,
                        'total_menu_amount'   => $totalMenuAmount,
                        'size'                => $itemSize,
                        'order_type'          => $orderType,
                        'delivery_charge'     => $deliveryCharge,
                        'image'               => $imgdata,
                        'status'              => 0,
                        'item_other_info'      => $item_other_info
                    ];


                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id')>0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    }else{
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);


                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if(is_array( $menuJsonOld )){
                                sort($menuJsonOld);
                            }
                            if(is_array( $menuJsonDecoded )){
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded && !$is_pot_prod) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id      = $userAuthId;
                                $bag->user_id           = $userId;
                                $bag->quantity          += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result     = $bag;
                                $bagUpdated = true;
                            }


                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if( $prod_type=='gift_card'){
                                $userAuthId      = $userAuth->id;
                                $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    if(!isset($menuData['bag_id']) && $result && $is_pot_prod && count($addon_array)){
                        foreach($addon_array as &$addon){
                            $addon['bag_id']=$result->id;
                        }
                        MybagOptions::insert($addon_array);
                    }
                    $userBagItems = $this->getUserMyBagListV2($locationId, $userAuthId,  $request);
                    $me           = microtime(true) - $ms;

                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }


    private function menuItemLanguageWise( $menuItems,$language_id,$id){
        $newMenuItemsLanguage = NewMenuItemsLanguage::where([
            'new_menu_items_id' => $id,
            'language_id' => $language_id,
        ])->first();

        if($newMenuItemsLanguage !== null) {
            $menuItems['item_other_info'] = $newMenuItemsLanguage->item_other_info;
            $menuItems['gift_message'] = $newMenuItemsLanguage->gift_message;
            $menuItems['name'] = $newMenuItemsLanguage->name;
            $menuItems['description'] = $newMenuItemsLanguage->description;
            $menuItems['sub_description'] = $newMenuItemsLanguage->sub_description;
        }
    }

    private function getUserMyBagListV2($locationId, $userAuthId, Request $request) {
        $localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;
        //$userBagItems = UserMyBag::where(['location_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->with('addonoptions')->get();
        $total_menu_amount = $myselfbonus_amount = 0;
        if ($userBagItems) {
            $i = 0;

            foreach ($userBagItems as $userMyBag) {
                $total_menu_amount  += $userMyBag['total_menu_amount'];
                $is_modifier = 0;
                $bag_quantity = $userMyBag['quantity'];
                $menuItemDetail = '';
                if (isset($userMyBag['menu_json']) && !is_null($userMyBag['menu_json'])) {
                    if ($userMyBag['image']) {
                        $userBagItems[$i]['image'] = json_decode($userMyBag['image'], true);
                        $userBagItems[$i]['thumb_med'] = "/images/mobile/defaultPizza.png";
                    } else {
                        $userBagItems[$i]['image'] = [
                            "image" => "/images/mobile/defaultPizza.png",
                            "thumb_med" => "/images/mobile/defaultPizza.png",
                            "thumb_sml" => "/images/mobile/defaultPizza.png",
                            "web_image" => "/images/mobile/defaultPizza.png",
                            "web_thumb_med" => "/images/mobile/defaultPizza.png",
                            "web_thumb_sml" => "/images/mobile/defaultPizza.png",
                        ];
                    }
                    $menuJsonDecoded = json_decode($userBagItems[$i]['menu_json'], true);
                    $is_modifier = 1;

                    if ($is_modifier && $menuJsonDecoded) {
                        if (isset($menuJsonDecoded['modifier_items'])) {

                            foreach ($menuJsonDecoded['modifier_items'] as &$premenu) {
                                $modifier_item_id = $premenu['id'];
                                $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                                $modifierInfoLang = ItemModifierLanguage::where([
                                    'modifier_item_id' => $premenu['id'],
                                    'language_id' => $userMyBag['language_id']
                                ])->first();
                                if($modifierInfo && $modifierInfoLang) {
                                    $modifierInfo->modifier_name = $modifierInfoLang->modifier_name;
                                }
                                $premenu['modifierInfo'] = $modifierInfo;

                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as &$modifier_items_list) {
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as &$modifier_OptionGroup) {
                                                $itemModifierOptionGroup = ItemModifierOptionGroup::where([
                                                    'id' => $modifier_OptionGroup['id']
                                                ])->first();
                                                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                                                    'modifier_option_group_id' => $modifier_OptionGroup['id'],
                                                    'language_id' => $userMyBag['language_id']
                                                ])->first();
                                                if ($itemModifierOptionGroup && $itemModifierOptionGroupLanguage) {
                                                    $itemModifierOptionGroup->group_name = $itemModifierOptionGroupLanguage->group_name;
                                                    $itemModifierOptionGroup->prompt = $itemModifierOptionGroupLanguage->prompt;
                                                }
                                                $modifier_OptionGroup['group_name'] = $itemModifierOptionGroup->group_name;
                                                $modifier_OptionGroup['prompt'] = $itemModifierOptionGroup->prompt;

                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as &$modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();
                                                        $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                                            'modifier_option_id' => $modifier_OptionGroup_item['id'],
                                                            'language_id' => $userMyBag['language_id']
                                                        ])->first();
                                                        if ($itemModifierOptionGroupItemObj && $itemOptionsModifierLanguage) {
                                                            $itemModifierOptionGroupItemObj->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                                        }
                                                        $modifier_OptionGroup_item['modifier_option_name'] = $itemModifierOptionGroupItemObj->modifier_option_name;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }
                    $userBagItems[$i]['menu_json'] = $menuJsonDecoded;

                }

                $userBagItems[$i]['is_modifier'] = $is_modifier;

                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $menuItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info')->find($userMyBag['menu_id']);
                    if ($menuItemDetail->id) {
                        if (isset($userMyBag->addonoptions) && count($userMyBag->addonoptions)) {
                            foreach ($userMyBag->addonoptions as $key => &$babaddonoption) {
                                $addonInfo = ItemAddons::where('id', $babaddonoption->option_id)->with('addongroup')->first();
                                if ($addonInfo) {
                                    $addonItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info', 'gift_message')->find($addonInfo->item_id);
                                    $addonItemsLanguage = NewMenuItemsLanguage::where([
                                        'new_menu_items_id' => $addonInfo->item_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();
                                    if($addonItemsLanguage) {
                                        $addonItemDetail->item_other_info = $addonItemsLanguage->item_other_info;
                                        $addonItemDetail->gift_message = $addonItemsLanguage->gift_message;
                                        $addonItemDetail->name = $addonItemsLanguage->name;
                                        $addonItemDetail->description = $addonItemsLanguage->description;
                                        $addonItemDetail->sub_description = $addonItemsLanguage->sub_description;
                                    }
                                    $babaddonoption->addon_item_details = $addonItemDetail;

                                    $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                                        'item_addon_group_id' => $addonInfo->addon_group_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();

                                    if ($itemAddonGroupsLanguage) {
                                        $addonInfo->addongroup->group_name = $itemAddonGroupsLanguage->group_name;
                                        $addonInfo->addongroup->prompt = $itemAddonGroupsLanguage->prompt;
                                    }

                                    $babaddonoption->addongroup = $addonInfo->addongroup;
                                    //$userBagItems[$i]['mybagoptions'][$key]['addongroup']=$addonInfo->addongroup;
                                }
                            }
                        }
                        #@30-07-2018 By RG Multiple images for KEKI
                        $get_item_images = CommonFunctions::get_menu_item_imagesV2($menuItemDetail->id);
                        $menuItemDetail['images'] = $get_item_images;
                        $userBagItems[$i]['product_type'] = $menuItemDetail->product_type;
                        if ($menuItemDetail->product_type == 'gift_card') {
                            if (!is_null($menuItemDetail->item_other_info) && !empty($menuItemDetail->item_other_info)) {
                                $userBagItems[$i]['menu_item_other_info'] = $menuItemDetail->item_other_info;
                            } else {
                                $userBagItems[$i]['menu_item_other_info'] = null;
                            }

                            $item_otherinfo = !empty($userMyBag['item_other_info']) ? json_decode($userMyBag['item_other_info']) : '';
                            $promotion_applicable = (isset($item_otherinfo->promotion_applicable)) ? $item_otherinfo->promotion_applicable : 0;

                            if ($promotion_applicable) {

                                $bonus_for = (isset($item_otherinfo->bonus_for) && !empty($item_otherinfo->bonus_for)) ? $item_otherinfo->bonus_for : '';
                                $promotion_amount = (isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount)) ? $item_otherinfo->promotion_amount : 00.00;

                                if ($bonus_for == 'myself') {
                                    $myselfbonus_amount = $myselfbonus_amount + ($promotion_amount * $bag_quantity);

                                }
                                $userBagItems[$i]['promotion_amount'] = $promotion_amount;
                            }
                        } else {
                            $userBagItems[$i]['item_other_info'] = $menuItemDetail->item_other_info;
                        }
                    }
                    $menuItemDetail = &$menuItemDetail;
                    $this->menuItemLanguageWise($menuItemDetail, $userMyBag['language_id'], $menuItemDetail->id);
                } else {
                    $userBagItems[$i]['product_type'] = 'food_item';   //In case of Menu Id 0
                }
                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $adonsDetails = $this->getItemsAdonsWithAddonsGroup($userMyBag['menu_id'], $language_id, $request, $locationId);
                    $modifierDetails = $this->getModifiersGroup($userMyBag['menu_id'], $language_id, $request);
                    $userBagItems[$i]['adons'] = $adonsDetails;
                    $userBagItems[$i]['modifier'] = $modifierDetails;;
                }
                $userBagItems[$i]['menu_item_detail'] = $menuItemDetail;
                $i++;
            }


        }

        $restaurantInfo = Restaurant::with(array('city' => function ($query) {
            $query->select('id', 'sales_tax');
        }))->where('id',$locationId)->select('id','tip','tax','city_tax', 'delivery_charge','city_id', 'delivery_service_charge', 'minimum_delivery', 'free_delivery')->first();
        $sale_tax = 0.00;
        if ($restaurantInfo) {
            if ($restaurantInfo->city_tax) {
                $sale_tax = ($restaurantInfo['city'] && $restaurantInfo['city']['sales_tax']) ? $restaurantInfo['city']['sales_tax'] : 0.00;
            } else {
                $sale_tax = $restaurantInfo->tax;
            }
        }

        #@29-08-2018 by RG
        #this charge is applicable only on Delivery Type Orders and if service is enabled
        $service_provider_charge = 0;
        $rest_provider_data = Restaurant::select('flat_discount','flat_discount_type','flat_discount_min','flat_discount_start_date','flat_discount_end_date','restaurants.id', 'delivery_provider_id', 'delivery_services.provider_name', 'delivery_service_charge', 'delivery_services.id as del_pro')->join('delivery_services', 'restaurants.delivery_provider_id', 'delivery_services.id')->where(['restaurants.id' => $locationId, 'delivery_services.status' => '1'])->first();

        if ($rest_provider_data) {
            $service_provider_charge = $rest_provider_data->delivery_service_charge;
        }
        if($total_menu_amount > $restaurantInfo->free_delivery)
            $delCharge = 0;
        else $delCharge = $restaurantInfo->delivery_charge;



        $data = [
            'list' => $userBagItems,
            'tip' => explode(",", $restaurantInfo->tip),
            'tax' => $sale_tax,
            'delivery_charge' => $delCharge,
            'service_provider_charge' => $service_provider_charge,
			'minimum_delivery' =>$restaurantInfo->minimum_delivery !=null ? $restaurantInfo->minimum_delivery : 0,
            'free_delivery' =>$restaurantInfo->free_delivery,
        ];
        //$data['flat_discount']=Delivery::getDiscount($rest_provider_data,$total_menu_amount);

        if ($myselfbonus_amount != 0) {
            $data['myselfbonus_amount'] = $myselfbonus_amount;
            $data['myselfbonus_amount_message'] = 'You will recieve single bonus gift card worth $' . $myselfbonus_amount . '.';
        }

        return $data;
    }


    public function showAllUserMyBagItemsV2(Request $request) {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagListV2($locationId, $userAuthId,  $request);
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userMyBagItems, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    public function showOneOrderV2Byw($id, Request $request)
    {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;
            //$myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'location_id' => $locationId])->first();
            $myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'restaurant_id' => $locationId])->with('addonoptions')->first();
            $new_menu_item_id = null;
            if ($myBagData) {
                if (isset($myBagData->addonoptions) && count($myBagData->addonoptions)) {
                    $babaddonoption = &$myBagData->addonoptions;
                    if(isset($babaddonoption->option_id)){
                        $addonInfo = ItemAddons::where('id', $babaddonoption->option_id)->with('addongroup')->first();
                        if ($addonInfo) {
                            $new_menu_item_id= $addonInfo->item_id;
                            $addonItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info', 'gift_message')->find($addonInfo->item_id);
                            $addonItemsLanguage = NewMenuItemsLanguage::where([
                                'new_menu_items_id' => $addonInfo->item_id,
                                'language_id' => $myBagData['language_id'],
                            ])->first();
                            if($addonItemsLanguage) {
                                $addonItemDetail->item_other_info = $addonItemsLanguage->item_other_info;
                                $addonItemDetail->gift_message = $addonItemsLanguage->gift_message;
                                $addonItemDetail->name = $addonItemsLanguage->name;
                                $addonItemDetail->description = $addonItemsLanguage->description;
                                $addonItemDetail->sub_description = $addonItemsLanguage->sub_description;
                            }
                            $babaddonoption->addon_item_details = $addonItemDetail;

                            $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                                'item_addon_group_id' => $addonInfo->addon_group_id,
                                'language_id' => $myBagData['language_id'],
                            ])->first();

                            if ($itemAddonGroupsLanguage) {
                                $addonInfo->addongroup->group_name = $itemAddonGroupsLanguage->group_name;
                                $addonInfo->addongroup->prompt = $itemAddonGroupsLanguage->prompt;
                            }

                            $babaddonoption->addongroup = $addonInfo->addongroup;
                            //$userBagItems[$i]['mybagoptions'][$key]['addongroup']=$addonInfo->addongroup;

                        }
                    }

                }
                $menuJsonDecoded = ($myBagData['menu_json'] ? json_decode($myBagData['menu_json'], true) : '');

                if ($menuJsonDecoded) {
                    if (isset($menuJsonDecoded['modifier_items'])) {
                        foreach ($menuJsonDecoded['modifier_items'] as &$premenu) {
                            $modifier_item_id = $premenu['id'];
                            $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                            $modifierInfoLang = ItemModifierLanguage::where([
                                'modifier_item_id' => $premenu['id'],
                                'language_id' => $myBagData['language_id']
                            ])->first();
                            if(isset($modifierInfo) && isset($modifierInfo->modifier_group_id)){
                                $modifierGroupInfo = ItemModifierGroup::where('id', $modifierInfo->modifier_group_id)->first();

                                $new_menu_item_id = $modifierGroupInfo->menu_item_id;
                                if($modifierInfo && $modifierInfoLang) {
                                    $modifierInfo->modifier_name = $modifierInfoLang->modifier_name;
                                }
                                $premenu['modifierInfo'] = $modifierInfo;

                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as &$modifier_items_list) {
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as &$modifier_OptionGroup) {
                                                $itemModifierOptionGroup = ItemModifierOptionGroup::where([
                                                    'id' => $modifier_OptionGroup['id']
                                                ])->first();
                                                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                                                    'modifier_option_group_id' => $modifier_OptionGroup['id'],
                                                    'language_id' => $myBagData['language_id']
                                                ])->first();
                                                if ($itemModifierOptionGroup && $itemModifierOptionGroupLanguage) {
                                                    $itemModifierOptionGroup->group_name = $itemModifierOptionGroupLanguage->group_name;
                                                    $itemModifierOptionGroup->prompt = $itemModifierOptionGroupLanguage->prompt;
                                                }
                                                $modifier_OptionGroup['group_name'] = $itemModifierOptionGroup->group_name;
                                                $modifier_OptionGroup['prompt'] = $itemModifierOptionGroup->prompt;

                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as &$modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();
                                                        $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                                            'modifier_option_id' => $modifier_OptionGroup_item['id'],
                                                            'language_id' => $myBagData['language_id']
                                                        ])->first();
                                                        if ($itemModifierOptionGroupItemObj && $itemOptionsModifierLanguage) {
                                                            $itemModifierOptionGroupItemObj->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                                        }
                                                        $modifier_OptionGroup_item['modifier_option_name'] = $itemModifierOptionGroupItemObj->modifier_option_name;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }//if $modifierInfo->modifier_group_id exist
                        }
                    }
                }
                if (isset($myBagData) && isset($myBagData->menu_id)) {
                    $new_menu_item_id = $myBagData->menu_id;
                    $adonsDetails = $this->getItemsAdonsWithAddonsGroup($new_menu_item_id, $language_id, $request, $locationId);
                    $modifierDetails = $this->getModifiersGroup($new_menu_item_id, $language_id, $request);
                    $myBagData['adons'] = $adonsDetails;
                    $myBagData['modifier'] = $modifierDetails;
                    $myBagData['labels'] = $this->labelDetails($new_menu_item_id);
                }
                $myBagData['menu_json'] = $menuJsonDecoded;
		$myBagData['pos_id'] = $myBagData->pos_id;
                if ($myBagData['is_byp']) {
                    // BYP
                    $pizzaSetting = BuildYourOwnPizza::where('restaurant_id', $locationId)->get();
                    $preference = 0;
                    foreach ($pizzaSetting as $setting) {
                        $settingdata=(!is_null($setting['setting']))?json_decode($setting['setting'], true):[];
                        foreach ($settingdata as &$set){

                            $set['positions']=isset($set['positions'])?(array)CommonFunctions::groupingPositionsData($set['positions']):[];

                        }

                        $result['restaurant_id'] = $setting->restaurant_id;
                        if ($setting->is_preference) {
                            $result['preferences'][$preference] = [
                                'id' => $setting['id'],
                                'label' => $setting['label'],
                                'is_multiselect' => $setting['is_multiselect'],
                                'is_customizable' => $setting['is_customizable'],
                                'items' => $settingdata,
                            ];
                            /** SET SELECTED @ 03-09-2018 by RG**/
                            $setLabel = $setting->label;
                            if (isset($myBagData['menu_json'][$setLabel])) {
                                foreach ($result['preferences'][$preference]['items'] as &$itm) {
                                    $itm['is_selected'] = 0;
                                }

                                $selectedSetting = $myBagData['menu_json'][$setLabel];

                                $itemArr = array_column($result['preferences'][$preference]['items'], 'label');
                                $index = array_search($selectedSetting, $itemArr);
                                if ($index !== false) {
                                    $result['preferences'][$preference]['items'][$index]['is_selected'] = 1;
                                }
                            }
                            $preference++;

                            /** SET SELECTED **/


                        } else {
                            if ($setting->parent_id > 0) {
                                $index = array_search($setting->parent_id, array_column($result['customizable_data'], 'id'));

                                $result['customizable_data'][$index]['has_child'] = 1;
                                $data = [
                                    'id' => $setting['id'],
                                    'label' => $setting['label'],
                                    'has_parent' => 1,
                                    'is_multiselect' => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items' => $settingdata,
                                ];
                                if ($setting['is_customizable'] && $setting->setting != null) {
                                    $setLabel='';
                                    if ($setting['label'] == 'veg topping') {
                                        $setLabel = 'veg_topping';
                                    } elseif ($setting['label'] == 'non-veg topping') {
                                        $setLabel = 'non-veg_topping';
                                    }
                                    if (isset($myBagData['menu_json'][$setLabel]) && !empty($setLabel)) {
                                        $selectedSetting = $myBagData['menu_json'][$setLabel];
                                        $itemArr = array_column($data['items'], 'label');
                                        foreach ($selectedSetting as $selSet) {
                                            $indexTwo = array_search($selSet[$setLabel], $itemArr);
                                            if (isset($selSet[$setLabel])) {
                                                $data['items'][$indexTwo]['is_selected'] = 1;
                                                $data['items'][$indexTwo]['side'] = $selSet['side'];
                                                $data['items'][$indexTwo]['quantity'] = $selSet['quantity'];
                                            }
                                        }
                                    }
                                }
                                $result['customizable_data'][$index]['items'][] = $data;
                            } else {
                                $data = [
                                    'id' => $setting['id'],
                                    'label' => $setting['label'],
                                    'has_child' => 0,
                                    'is_multiselect' => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items' =>$settingdata,
                                ];
                                if ($setting['is_customizable'] && $setting->setting != null) {
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');
                                    foreach ($selectedSetting as $selSet) {
                                        $index = array_search($selSet[$setting['label']], $itemArr);
                                        if (isset($selSet[$setting['label']])) {
                                            $data['items'][$index]['is_selected'] = 1;
                                            $data['items'][$index]['side'] = $selSet['side'];
                                            $data['items'][$index]['quantity'] = $selSet['quantity'];
                                        }
                                    }
                                }
                                //# for non customizable item @ RG 20-07-2018
                                if ($setting['is_customizable'] == 0 && $setting->setting != null) {
                                    // set is selected = 0
                                    foreach ($data['items'] as &$itm) {
                                        $itm['is_selected'] = 0;
                                    }
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');

                                    //foreach($selectedSetting as $selSet) {
                                    $index = array_search($selectedSetting, $itemArr);
                                    if ($index !== false) {
                                        $data['items'][$index]['is_selected'] = 1;
                                    }
                                    // }
                                }
                                $result['customizable_data'][] = $data;
                            }
                        }
                    }
                    $myBagData['customizable_data'] = $result['customizable_data'];
                    $myBagData['preferences'] = $result['preferences'];
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else {
                    // NORMAL MENU
                    $me = microtime(true) - $ms;
                    // add images
                    $menuItem = NewMenuItems::where('id', $myBagData['menu_id'])->select('id', 'name', 'description', 'image', 'thumb_image_med', 'thumb_image_med', 'image_caption', 'web_image', 'web_thumb_med', 'web_thumb_sml', 'web_image_caption', 'mobile_web_image', 'mobile_web_thumb_med', 'mobile_web_thumb_sml', 'mobile_web_image_caption', 'banner_image', 'status', 'images', 'item_other_info', 'product_type', 'size_price')->first()->toArray();
                    $defaultImage = null;
                    # Added Image key as It was not sync with category/pizza api
                    $myBagData['image'] = [
                        "image" => $menuItem['image'] ?? $defaultImage,
                        "thumb_med" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "thumb_sml" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "image_caption" => $menuItem['image_caption'] ?? '',
                        "web_image" => $menuItem['web_image'] ?? $defaultImage,
                        "web_thumb_med" => $menuItem['web_thumb_med'] ?? $defaultImage,
                        "web_thumb_sml" => $menuItem['web_thumb_sml'] ?? $defaultImage,
                        "web_image_caption" => $menuItem['web_image_caption'] ?? '',
                        "mobile_web_image" => $menuItem['mobile_web_image'] ?? $defaultImage,
                        "mobile_web_thumb_med" => $menuItem['mobile_web_thumb_med'] ?? $defaultImage,
                        "mobile_web_thumb_sml" => $menuItem['mobile_web_thumb_sml'] ?? $defaultImage,
                        "mobile_web_image_caption" => $menuItem['mobile_web_image_caption'] ?? '',
                        "banner_image" => $menuItem['banner_image'],
                        "thumb_banner_image" => $menuItem['banner_image'],
                    ];
                    $myBagData['name'] = $menuItem['name'] ?? '';
                    if (!is_null($menuItem['item_other_info']) && !empty($menuItem['item_other_info'])) {
                        $myBagData['menu_item_other_info'] = $menuItem['item_other_info'];
                    } else {
                        $myBagData['menu_item_other_info'] = null;
                    }
                    $myBagData['product_type'] = $menuItem['product_type'];
                    $images = array();
                    if ($menuItem['images']) {
                        $images = json_decode($menuItem['images'], true);
                    }
                    $myBagData['images'] = $images;
                    $myBagData['description'] = $menuItem['description'] ?? '';
                    $myBagData['status'] = $menuItem['status'] ?? '';
                    if ($menuItem['product_type'] == 'gift_card' || $menuItem['product_type'] == 'product') {
                        #@16-07-2018 By RG
                        if ($request->has('time')) {
                            $current_time = $request->query('time') . ':00';
                        } else {
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                            if (is_object($currentDateTimeObj)) {
                                $current_time = $currentDateTimeObj->format('H:i:00');
                            } else {
                                $current_time = date('H:i:00');
                            }
                        }

                        $size_prices = json_decode($menuItem['size_price'], true);
                        $this->getSizePrice($size_prices, $locationId, $current_time);
                        $myBagData['size_price'] = $size_prices;

                    }


                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => 'Cart item not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    public function showOneOrderV2($id, Request $request)
    {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $localization_id = $request->header('X-localization');
            $language_id = CommonFunctions::getLanguageInfo($localization_id);
            $language_id = $language_id->id;
            //$myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'location_id' => $locationId])->first();
            $myBagData = UserMyBag::where(['id' => $id, 'user_auth_id' => $userAuthId, 'restaurant_id' => $locationId])->with('addonoptions')->first();
            $new_menu_item_id = null;
            if ($myBagData) {
                if (isset($myBagData->addonoptions) && count($myBagData->addonoptions)) {
                    $babaddonoption = &$myBagData->addonoptions;
                    if(isset($babaddonoption->option_id)){
                        $addonInfo = ItemAddons::where('id', $babaddonoption->option_id)->with('addongroup')->first();
                        if ($addonInfo) {
                            $new_menu_item_id= $addonInfo->item_id;
                            $addonItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info', 'gift_message')->find($addonInfo->item_id);
                            $addonItemsLanguage = NewMenuItemsLanguage::where([
                                'new_menu_items_id' => $addonInfo->item_id,
                                'language_id' => $myBagData['language_id'],
                            ])->first();
                            if($addonItemsLanguage) {
                                $addonItemDetail->item_other_info = $addonItemsLanguage->item_other_info;
                                $addonItemDetail->gift_message = $addonItemsLanguage->gift_message;
                                $addonItemDetail->name = $addonItemsLanguage->name;
                                $addonItemDetail->description = $addonItemsLanguage->description;
                                $addonItemDetail->sub_description = $addonItemsLanguage->sub_description;
                            }
                            $babaddonoption->addon_item_details = $addonItemDetail;

                            $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                                'item_addon_group_id' => $addonInfo->addon_group_id,
                                'language_id' => $myBagData['language_id'],
                            ])->first();

                            if ($itemAddonGroupsLanguage) {
                                $addonInfo->addongroup->group_name = $itemAddonGroupsLanguage->group_name;
                                $addonInfo->addongroup->prompt = $itemAddonGroupsLanguage->prompt;
                            }

                            $babaddonoption->addongroup = $addonInfo->addongroup;
                            //$userBagItems[$i]['mybagoptions'][$key]['addongroup']=$addonInfo->addongroup;

                        }
                    }

                }
                $menuJsonDecoded = ($myBagData['menu_json'] ? json_decode($myBagData['menu_json'], true) : '');

                if ($menuJsonDecoded) {
                    if (isset($menuJsonDecoded['modifier_items'])) {
                        foreach ($menuJsonDecoded['modifier_items'] as &$premenu) {
                            $modifier_item_id = $premenu['id'];
                            $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                            $modifierInfoLang = ItemModifierLanguage::where([
                                'modifier_item_id' => $premenu['id'],
                                'language_id' => $myBagData['language_id']
                            ])->first();
                            if(isset($modifierInfo) && isset($modifierInfo->modifier_group_id)){
                                $modifierGroupInfo = ItemModifierGroup::where('id', $modifierInfo->modifier_group_id)->first();

                                $new_menu_item_id = $modifierGroupInfo->menu_item_id;
                                if($modifierInfo && $modifierInfoLang) {
                                    $modifierInfo->modifier_name = $modifierInfoLang->modifier_name;
                                }
                                $premenu['modifierInfo'] = $modifierInfo;

                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as &$modifier_items_list) {
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as &$modifier_OptionGroup) {
                                                $itemModifierOptionGroup = ItemModifierOptionGroup::where([
                                                    'id' => $modifier_OptionGroup['id']
                                                ])->first();
                                                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                                                    'modifier_option_group_id' => $modifier_OptionGroup['id'],
                                                    'language_id' => $myBagData['language_id']
                                                ])->first();
                                                if ($itemModifierOptionGroup && $itemModifierOptionGroupLanguage) {
                                                    $itemModifierOptionGroup->group_name = $itemModifierOptionGroupLanguage->group_name;
                                                    $itemModifierOptionGroup->prompt = $itemModifierOptionGroupLanguage->prompt;
                                                }
                                                $modifier_OptionGroup['group_name'] = $itemModifierOptionGroup->group_name;
                                                $modifier_OptionGroup['prompt'] = $itemModifierOptionGroup->prompt;

                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as &$modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();
                                                        $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                                            'modifier_option_id' => $modifier_OptionGroup_item['id'],
                                                            'language_id' => $myBagData['language_id']
                                                        ])->first();
                                                        if ($itemModifierOptionGroupItemObj && $itemOptionsModifierLanguage) {
                                                            $itemModifierOptionGroupItemObj->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                                        }
                                                        $modifier_OptionGroup_item['modifier_option_name'] = $itemModifierOptionGroupItemObj->modifier_option_name;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }//if $modifierInfo->modifier_group_id exist
                        }
                    }
                }
                if (isset($myBagData) && isset($myBagData->menu_id)) {
                    $new_menu_item_id = $myBagData->menu_id;
                    $adonsDetails = $this->getItemsAdonsWithAddonsGroup($new_menu_item_id, $language_id, $request, $locationId);
                    $modifierDetails = $this->getModifiersGroup($new_menu_item_id, $language_id, $request);
                    $myBagData['adons'] = $adonsDetails;
                    $myBagData['modifier'] = $modifierDetails;;
                }
                $myBagData['menu_json'] = $menuJsonDecoded;

                if ($myBagData['is_byp']) {
                    // BYP
                    $pizzaSetting = BuildYourOwnPizza::where('restaurant_id', $locationId)->get();
                    $preference = 0;
                    foreach ($pizzaSetting as $setting) {
                        $settingdata=(!is_null($setting['setting']))?json_decode($setting['setting'], true):[];
                        foreach ($settingdata as &$set){

                            $set['positions']=isset($set['positions'])?(array)CommonFunctions::groupingPositionsData($set['positions']):[];

                        }

                        $result['restaurant_id'] = $setting->restaurant_id;
                        if ($setting->is_preference) {
                            $result['preferences'][$preference] = [
                                'id' => $setting['id'],
                                'label' => $setting['label'],
                                'is_multiselect' => $setting['is_multiselect'],
                                'is_customizable' => $setting['is_customizable'],
                                'items' => $settingdata,
                            ];
                            /** SET SELECTED @ 03-09-2018 by RG**/
                            $setLabel = $setting->label;
                            if (isset($myBagData['menu_json'][$setLabel])) {
                                foreach ($result['preferences'][$preference]['items'] as &$itm) {
                                    $itm['is_selected'] = 0;
                                }

                                $selectedSetting = $myBagData['menu_json'][$setLabel];

                                $itemArr = array_column($result['preferences'][$preference]['items'], 'label');
                                $index = array_search($selectedSetting, $itemArr);
                                if ($index !== false) {
                                    $result['preferences'][$preference]['items'][$index]['is_selected'] = 1;
                                }
                            }
                            $preference++;

                            /** SET SELECTED **/


                        } else {
                            if ($setting->parent_id > 0) {
                                $index = array_search($setting->parent_id, array_column($result['customizable_data'], 'id'));

                                $result['customizable_data'][$index]['has_child'] = 1;
                                $data = [
                                    'id' => $setting['id'],
                                    'label' => $setting['label'],
                                    'has_parent' => 1,
                                    'is_multiselect' => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items' => $settingdata,
                                ];
                                if ($setting['is_customizable'] && $setting->setting != null) {
                                    $setLabel='';
                                    if ($setting['label'] == 'veg topping') {
                                        $setLabel = 'veg_topping';
                                    } elseif ($setting['label'] == 'non-veg topping') {
                                        $setLabel = 'non-veg_topping';
                                    }
                                    if (isset($myBagData['menu_json'][$setLabel]) && !empty($setLabel)) {
                                        $selectedSetting = $myBagData['menu_json'][$setLabel];
                                        $itemArr = array_column($data['items'], 'label');
                                        foreach ($selectedSetting as $selSet) {
                                            $indexTwo = array_search($selSet[$setLabel], $itemArr);
                                            if (isset($selSet[$setLabel])) {
                                                $data['items'][$indexTwo]['is_selected'] = 1;
                                                $data['items'][$indexTwo]['side'] = $selSet['side'];
                                                $data['items'][$indexTwo]['quantity'] = $selSet['quantity'];
                                            }
                                        }
                                    }
                                }
                                $result['customizable_data'][$index]['items'][] = $data;
                            } else {
                                $data = [
                                    'id' => $setting['id'],
                                    'label' => $setting['label'],
                                    'has_child' => 0,
                                    'is_multiselect' => $setting['is_multiselect'],
                                    'is_customizable' => $setting['is_customizable'],
                                    'items' =>$settingdata,
                                ];
                                if ($setting['is_customizable'] && $setting->setting != null) {
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');
                                    foreach ($selectedSetting as $selSet) {
                                        $index = array_search($selSet[$setting['label']], $itemArr);
                                        if (isset($selSet[$setting['label']])) {
                                            $data['items'][$index]['is_selected'] = 1;
                                            $data['items'][$index]['side'] = $selSet['side'];
                                            $data['items'][$index]['quantity'] = $selSet['quantity'];
                                        }
                                    }
                                }
                                //# for non customizable item @ RG 20-07-2018
                                if ($setting['is_customizable'] == 0 && $setting->setting != null) {
                                    // set is selected = 0
                                    foreach ($data['items'] as &$itm) {
                                        $itm['is_selected'] = 0;
                                    }
                                    $selectedSetting = $myBagData['menu_json'][$setting['label']];
                                    $itemArr = array_column($data['items'], 'label');

                                    //foreach($selectedSetting as $selSet) {
                                    $index = array_search($selectedSetting, $itemArr);
                                    if ($index !== false) {
                                        $data['items'][$index]['is_selected'] = 1;
                                    }
                                    // }
                                }
                                $result['customizable_data'][] = $data;
                            }
                        }
                    }
                    $myBagData['customizable_data'] = $result['customizable_data'];
                    $myBagData['preferences'] = $result['preferences'];
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else {
                    // NORMAL MENU
                    $me = microtime(true) - $ms;
                    // add images
                    $menuItem = NewMenuItems::where('id', $myBagData['menu_id'])->select('id', 'name', 'description', 'image', 'thumb_image_med', 'thumb_image_med', 'image_caption', 'web_image', 'web_thumb_med', 'web_thumb_sml', 'web_image_caption', 'mobile_web_image', 'mobile_web_thumb_med', 'mobile_web_thumb_sml', 'mobile_web_image_caption', 'banner_image', 'status', 'images', 'item_other_info', 'product_type', 'size_price')->first()->toArray();
                    $defaultImage = null;
                    # Added Image key as It was not sync with category/pizza api
                    $myBagData['image'] = [
                        "image" => $menuItem['image'] ?? $defaultImage,
                        "thumb_med" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "thumb_sml" => $menuItem['thumb_image_med'] ?? $defaultImage,
                        "image_caption" => $menuItem['image_caption'] ?? '',
                        "web_image" => $menuItem['web_image'] ?? $defaultImage,
                        "web_thumb_med" => $menuItem['web_thumb_med'] ?? $defaultImage,
                        "web_thumb_sml" => $menuItem['web_thumb_sml'] ?? $defaultImage,
                        "web_image_caption" => $menuItem['web_image_caption'] ?? '',
                        "mobile_web_image" => $menuItem['mobile_web_image'] ?? $defaultImage,
                        "mobile_web_thumb_med" => $menuItem['mobile_web_thumb_med'] ?? $defaultImage,
                        "mobile_web_thumb_sml" => $menuItem['mobile_web_thumb_sml'] ?? $defaultImage,
                        "mobile_web_image_caption" => $menuItem['mobile_web_image_caption'] ?? '',
                        "banner_image" => $menuItem['banner_image'],
                        "thumb_banner_image" => $menuItem['banner_image'],
                    ];
                    $myBagData['name'] = $menuItem['name'] ?? '';
                    if (!is_null($menuItem['item_other_info']) && !empty($menuItem['item_other_info'])) {
                        $myBagData['menu_item_other_info'] = $menuItem['item_other_info'];
                    } else {
                        $myBagData['menu_item_other_info'] = null;
                    }
                    $myBagData['product_type'] = $menuItem['product_type'];
                    $images = array();
                    if ($menuItem['images']) {
                        $images = json_decode($menuItem['images'], true);
                    }
                    $myBagData['images'] = $images;
                    $myBagData['description'] = $menuItem['description'] ?? '';
                    $myBagData['status'] = $menuItem['status'] ?? '';
                    if ($menuItem['product_type'] == 'gift_card' || $menuItem['product_type'] == 'product') {
                        #@16-07-2018 By RG
                        if ($request->has('time')) {
                            $current_time = $request->query('time') . ':00';
                        } else {
                            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                            if (is_object($currentDateTimeObj)) {
                                $current_time = $currentDateTimeObj->format('H:i:00');
                            } else {
                                $current_time = date('H:i:00');
                            }
                        }

                        $size_prices = json_decode($menuItem['size_price'], true);
                        $this->getSizePrice($size_prices, $locationId, $current_time);
                        $myBagData['size_price'] = $size_prices;

                    }


                    return response()->json(['data' => $myBagData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => 'Cart item not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    /* New Menu Item */
    public function getModifiersGroup($menu_id, $language, Request $request) {
        $modifierGroupData = ItemModifierGroup::where('menu_item_id', '=', $menu_id)->select('modifier_groups.*')->get()->toArray();

        //$modifierGroupData = null;
        if (count($modifierGroupData)) {
            foreach ($modifierGroupData as $index => $cat) {
                $itemModifierGroupLanguage = ItemModifierGroupLanguage::where(['language_id' => $language, 'modifier_group_id' => $cat['id']])->first();
                if ($itemModifierGroupLanguage !== null) {
                    $modifierGroupData[$index]['group_name'] = $itemModifierGroupLanguage->group_name;
                    $modifierGroupData[$index]['prompt'] = $itemModifierGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItems($cat['id'], $language, $request);
                $modifierGroupData[$index]['modifier_items_list'] = $modifierItems;
            }
        }
        return $modifierGroupData;
    }

    public function getModifierItems($group_id, $language, Request $request) {
        $modifierData = ItemModifier::where('modifier_group_id', '=', $group_id)->select('modifier_items.*')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $ItemModifierLanguage = ItemModifierLanguage::where(['language_id' => $language, 'modifier_item_id' => $cat['id']])->first();
                if ($ItemModifierLanguage !== null) {
                    $modifierData[$index]['modifier_name'] = $ItemModifierLanguage->modifier_name;
                }
		$modifierItems = [];
		$modifierItems1 = DB::select('select id,menu_item_id,modifier_item_id,option_name,option_amount,option_amount,restaurant_id,is_selected,created_at,dependent_modifier_id,dependent_modifier_id as to_dependent_group_id,option_description,status,pos_id from modifier_options where status="1" and modifier_item_id = :id', ['id' => $cat['id']]); 
		 
		$i = 0;
		foreach($modifierItems1 as $itm){  
			$dependent_modifier_id = (int)$itm->dependent_modifier_id;
			$modifierItems[$dependent_modifier_id][] =$itm;
			 
		}
		$modifierData[$index]['dependent_modifier_items'] = (count($modifierItems)==1 && isset($modifierItems[0]))?$modifierItems[0]:$modifierItems;
		 
                $modifierItems = $this->getModifierItemsOptionGroup($cat['id'], $language, $request);
                $modifierData[$index]['modifier_OptionGroup_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }
    public function getModifierItemsOptionGroup($group_id, $language, Request $request) {
        $modifierData = ItemModifierOptionGroup::where('modifier_item_id', '=', $group_id)->select('modifier_item_option_groups.*')->get()->toArray();
        if (count($modifierData)) {
            foreach ($modifierData as $index => $cat) {
                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                    'language_id' => $language,
                    'modifier_option_group_id' => $cat['id']
                ])->first();
                if($itemModifierOptionGroupLanguage !== null) {
                    $modifierData[$index]['group_name'] = $itemModifierOptionGroupLanguage->group_name;
                    $modifierData[$index]['prompt'] = $itemModifierOptionGroupLanguage->prompt;
                }
                $modifierItems = $this->getModifierItemsOptionGroupItem($cat['id'], $language, $request);
                $modifierData[$index]['modifier_OptionGroup_item_list'] = $modifierItems;
            }
        }
        return $modifierData;
    }

    public function getModifierItemsOptionGroupItem($group_id, $language, Request $request) {
        $modifierData = ItemModifierOptionGroupItem::where('modifier_option_group_id', '=', $group_id)->select('modifier_item_options.*')->get()->toArray();

        if(count($modifierData)) {
            foreach ($modifierData as $index => $row) {
                $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where(['language_id' => $language, 'modifier_option_id' => $row['id']])->first();
                if ($itemOptionsModifierLanguage !== null) {
                    $modifierData[$index]['modifier_option_name'] = $itemOptionsModifierLanguage->modifier_option_name;
                }
            }
        }
        return $modifierData;
    }

    public function getItemsAdonsWithAddonsGroup($menu_id, $language, Request $request, $locId) {
        $adonsGroupData = ItemAddonGroup::where('menu_item_id', '=', $menu_id)->select('item_addon_groups.*')->get()->toArray();

        if (count($adonsGroupData)) {
            foreach ($adonsGroupData as $index => $cat) {
                // implement language
                $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                    'item_addon_group_id' => $cat['id'],
                    'language_id' => $language
                ])->first();
                if ($itemAddonGroupsLanguage !== null) {
                    $adonsGroupData[$index]['group_name'] = $itemAddonGroupsLanguage->group_name;
                    $adonsGroupData[$index]['prompt'] = $itemAddonGroupsLanguage->prompt;
                }
                $adonItems = $this->getAdonsItems($cat['id'], $language, $request, $locId);

                $adonsGroupData[$index]['adons_items_list'] = $adonItems;
            }
        }
        return $adonsGroupData;
    }

    public function getAdonsItems($group_id, $language, Request $request, $locId) {
        $adonsData = ItemAddons::where('addon_group_id', '=', $group_id)->select('item_addons.*')->get()->toArray();
        $item = null;
        if (count($adonsData)) {
            foreach ($adonsData as $index => $adonsInfo) {
                $item1 = $this->getItemInfoById($adonsInfo['item_id'], $locId, $request, $language);

                if ($item1 !== null) {
                    $item1['adon_details'] = $adonsInfo;
                    $item[] = $item1;
                }

            }
        }
        return $item;
    }

    public function getItemInfoById($id, $locId, Request $request, $language_id) {
        $menuItems1 = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where(['new_menu_items.id' => $id, 'new_menu_items.status' => 1])
            ->select('new_menu_items.*', 'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();
        $temp = [];
        $defaultImage = null;
        $menuItems = null;
        if (isset($menuItems1[0])) {
            $menuItems = $menuItems1[0];
            $current_time = $request->query('time') . ':00';

            $deleteFlagNIndex = 0;
            //for ($i = 0; $i < count($menuItems); $i++) {

            #Get Current Menu item price based on Restaurant Current Timings @14-07-2018 By RG
            if (!empty($menuItems['size_price'])) {
                $size_prices = json_decode($menuItems['size_price'][0], true);
                $this->getSizePrice($size_prices, $locId, $current_time);
            }

            $menuItems['size_price'] = $size_prices;
            $menuItems['customizable_data'] = json_decode($menuItems['customizable_data'][0], true);

            $modifierDetails = $this->getModifiersGroup($menuItems['id'], $language_id, $request);
            $menuItems['modifier'] = $modifierDetails;



            // set language items
            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
                'new_menu_items_id' => $id,
                'language_id' => $language_id,
            ])->first();

            if($newMenuItemsLanguage !== null) {
                $menuItems['item_other_info'] = $newMenuItemsLanguage->item_other_info;
                $menuItems['gift_message'] = $newMenuItemsLanguage->gift_message;
                $menuItems['name'] = $newMenuItemsLanguage->name;
                $menuItems['description'] = $newMenuItemsLanguage->description;
                $menuItems['sub_description'] = $newMenuItemsLanguage->sub_description;
            }
            if(!empty($menuItems['images'])) {
                $menuItems['images']  = json_decode($menuItems['images'], true);
            }
            //}
        }
        return $menuItems;
    }

    /* Date 21-Jan-2019 by RG */
    public function v1_cart(Request $request)
    {
        $ms     = microtime(true);
        $error  = null;
        $result = null;
        $status = 200;

        // Verify user token
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp'              => 'sometimes|in:0,1',
                'menu_json'           => 'sometimes',
                'menu_id'             => 'sometimes|exists:menu_items,id',
                'order_type'          => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity'            => 'required|numeric',
            ]);

            if (!$validation->fails()) {
                if($request->post('is_byp')==1 || $request->post('menu_id')>0) {

                    $imageName    = $medImageName = "";
                    $quantity     = $request->post('quantity');
                    $locationId   = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo      = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees=0;
                    $imgdata         = null;
                    $item_other_info=null;
                    $prod_type='';
                    $orderType  = $request->post('order_type') ?? 'delivery';

                    if ($request->input('menu_id')) {
                        $menuInfo      = MenuItems::where('id',$request->input('menu_id'))->first();

                        if(isset($menuInfo) && $menuInfo->product_type=='gift_card') {
                            /***************************SALTY GIFT CARD RELATED*************/

                            $prod_type='gift_card';
                            $promotion_amount=null;


                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                            CommonFunctions::validateGiftCard($request,$quantity,$menuItemPrice);
                            //validating Gift card;

                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $gift_type=($request->has('gift_type') && !empty($request->input('gift_type')) )?$request->input('gift_type'):'egiftcard';

                            $recipient=($request->has('recipient') && !empty($request->input('recipient')) )?$request->input('recipient'):'single';

                            $giftcard_for=($request->has('giftcard_for') && !empty($request->input('giftcard_for')) )?$request->input('giftcard_for'):'recipient';

                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';

                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'gift_type'    => $gift_type,
                                'recipient'    => $recipient,
                                'bonus_for'    => $giftcard_for,
                                'recipient_details'=>$recipientDetails,
                                'promotion_amount'    => $promotion_amount,
                                'promotion_applicable'=>0
                            ];

                            $item_other_info=json_encode($item_other_info);


                        }else if(isset($menuInfo) && $menuInfo->product_type=='product') {
                            $prod_type='product';
                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;

                            $is_gift_wrapping=($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping')) )?$request->input('is_gift_wrapping'):0;


                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'is_gift_wrapping'      => trim($is_gift_wrapping),
                            ];

                            $item_other_info=json_encode($item_other_info);

                            if($is_gift_wrapping!=0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)){
                                $gift_wrapping_fees= (float)$menuInfo->gift_wrapping_fees;


                            }
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                        }else{


                            $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                            $sizeArrTemp   = array_column($menuSizePrice,'size');
                            $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                            $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                            //return response()->json([$menuInfo,$request->input('menu_id'),$menuSizePrice,$sizeArrTemp,$sizeIndex,$menuItemPrice]);

                        }


                        $imgarray = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);

                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image']           = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] =    $menuInfo->images;
                                $imgdata                      = json_encode($imgarray);
                            }
                        }else {
                            $imgarray['image']           = $menuInfo->web_image;
                            $imgarray['thumb_image_med'] = $menuInfo->web_thumb_med;
                            $imgarray['image_array'] =     $menuInfo->images;
                            $imgdata                      = json_encode($imgarray);
                        }

                    }



                    // add to MyBag
                    if($request->input('menu_id')>0) {
                        $unitPrice       = $menuItemPrice;
                    } else {
                        $unitPrice       = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice+$gift_wrapping_fees);
                    $orderType       = $request->post('order_type') ?? 'delivery';
                    $userAuthId      = $userAuth->id;
                    $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize        = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge  = $restInfo->delivery_charge;
                    $isByp           = false;



                    //@10-09-2018 by RG Calculate Addons-price in case of Normal Customisation Items
                    if(isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                        && $prod_type!='gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price+= $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }

                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if( $prod_type=='gift_card' && !$request->has('giftcard_for')){
                            $item_other_info=json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for=$old_item_otherinfo->bonus_for;
                            $item_other_info=json_encode($item_other_info);

                        }
                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            $result = $myBagItem->delete();
                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size'         => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if(isset($menuData['is_byp']) && $menuData['is_byp'] >0 ){
                                $unitPrice=$myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if( $prod_type=='gift_card'){
                                $myBagItem->unit_price           = $unitPrice ;
                                if($request->has('size')){
                                    $myBagItem->size=$request->post('size');
                                }
                            }
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if(!is_null($menuJsonOld)) { sort($menuJsonOld); }
                                if(!is_null($menuJsonDecoded)) { sort($menuJsonDecoded); }
                                /*sort($menuJsonOld);
                            sort($menuJsonDecoded);*/
                                $menuFlag = false;
                                if(isset($menuData['menu_id']) && $bag->menu_id==$menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif($request->post('is_byp')==1) {
                                    $menuFlag = true;
                                }
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag) {
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id      = $userAuthId;
                                    $myBagItem->user_id           = $userId;
                                    $myBagItem->quantity          = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = $totalMenuAmount + $bag->total_menu_amount;
                                    //update menu json
                                    $myBagItem->menu_json         = $request->post('menu_json');
                                    $myBagItem->item_other_info   = $item_other_info;
                                    if($request->has('special_instruction')){
                                        $myBagItem->special_instruction   =  $request->post('special_instruction');
                                    }


                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    $bag->delete();
                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
                                $myBagItem->menu_json         = $request->post('menu_json');
                                $myBagItem->quantity          = $quantity;
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->order_type    = $orderType;
                                $myBagItem->item_other_info   = $item_other_info;
                                if($request->has('special_instruction')){
                                    $myBagItem->special_instruction   =  $request->post('special_instruction');
                                }
                                $myBagItem->save();
                            }
                        }

                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                        $userBagItems = $this->getUserMyBagList($locationId, $userAuthId);

                        $me           = microtime(true) - $ms;
                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp                 = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath      = config('constants.image.path.byp');
                            $imageRelPath   = $imagePath . '/thumb/';
                            $image_parts    = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type     = $image_type_aux[1];

                            $img          = str_replace(' ', '+', $image_parts[1]);
                            $data         = base64_decode($img);
                            $uniqId       = uniqid(mt_rand());
                            $imageName    = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1   = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH      = intval($width_orig / 2);
                            $HEIGHT     = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, TRUE);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray                     = array();
                            $imgarray ['image']           = 'images/byp/' . $imageName;
                            $imgarray ['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata                      = json_encode($imgarray);
                        }
                    }

                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id'       => $restaurantId ?? 0,
                        'restaurant_id'         => $locationId,
                        'user_id'             => $userId,
                        'user_auth_id'        => $userAuthId,
                        'menu_id'             => $request->post('menu_id') ?? 0,
                        'is_byp'              => $isByp,
                        'menu_json'           => $request->post('menu_json'),
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity'            => $quantity,
                        'unit_price'          => $unitPrice,
                        'total_menu_amount'   => $totalMenuAmount,
                        'size'                => $itemSize,
                        'order_type'          => $orderType,
                        'delivery_charge'     => $deliveryCharge,
                        'image'               => $imgdata,
                        'status'              => 0,
                        'item_other_info'      => $item_other_info
                    ];


                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id')>0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    }else{
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);


                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if(is_array( $menuJsonOld )){
                                sort($menuJsonOld);
                            }
                            if(is_array( $menuJsonDecoded )){
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id      = $userAuthId;
                                $bag->user_id           = $userId;
                                $bag->order_type      = $orderType;
                                $bag->quantity          += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result     = $bag;
                                $bagUpdated = true;
                            }


                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if( $prod_type=='gift_card'){
                                $userAuthId      = $userAuth->id;
                                $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    $userBagItems = $this->getUserMyBagList($locationId, $userAuthId);

                    $me  = microtime(true) - $ms;

                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }

    /**
     * Get User's MyBag list
     * @return \Illuminate\Http\JsonResponse
     * Date 21-Jan-2019 by RG
     */
    public function v1_showAllUserMyBagItems(Request $request)
    {
        $ms          = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token    = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId     = $userAuth->id;
            $locationId     = $request->header('X-location');
            $restaurantId   = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagList($locationId, $userAuthId);
            $deliveryCharge = $userMyBagItems['delivery_charge'];
            $userMyBagItems['delivery_charge']=$deliveryCharge;
            $me             = microtime(true) - $ms;

            return response()->json(['data' => $userMyBagItems, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField.' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
	public function showAllUserMyBagItemsV3(Request $request) {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagListV3($locationId, $userAuthId,  $request);
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userMyBagItems, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
   private function getUserMyBagListV3($locationId, $userAuthId, Request $request) {
        $localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;
        //$userBagItems = UserMyBag::where(['location_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->with('addonoptions')->get();
        $total_menu_amount = $myselfbonus_amount = 0;
        if ($userBagItems) {
            $i = 0;
	    $totalQuantity = 0;
            foreach ($userBagItems as $userMyBag) {
		
                $total_menu_amount  += $userMyBag['total_menu_amount'];
                $is_modifier = 0;
                $bag_quantity = $userMyBag['quantity'];
		$totalQuantity += $bag_quantity;
                $menuItemDetail = '';
                if (isset($userMyBag['menu_json']) && !is_null($userMyBag['menu_json'])) {
                    if ($userMyBag['image']) {
                        $userBagItems[$i]['image'] = json_decode($userMyBag['image'], true);
                        $userBagItems[$i]['thumb_med'] = "/images/mobile/defaultPizza.png";
                    } else {
                        $userBagItems[$i]['image'] = [
                            "image" => "/images/mobile/defaultPizza.png",
                            "thumb_med" => "/images/mobile/defaultPizza.png",
                            "thumb_sml" => "/images/mobile/defaultPizza.png",
                            "web_image" => "/images/mobile/defaultPizza.png",
                            "web_thumb_med" => "/images/mobile/defaultPizza.png",
                            "web_thumb_sml" => "/images/mobile/defaultPizza.png",
                        ];
                    }
                    $menuJsonDecoded = json_decode($userBagItems[$i]['menu_json'], true);
                    $is_modifier = 1;

                    if ($is_modifier && $menuJsonDecoded) {
                        if (isset($menuJsonDecoded['modifier_items'])) {

                            foreach ($menuJsonDecoded['modifier_items'] as &$premenu) {
                                $modifier_item_id = $premenu['id'];
                                $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                                $modifierInfoLang = ItemModifierLanguage::where([
                                    'modifier_item_id' => $premenu['id'],
                                    'language_id' => $userMyBag['language_id']
                                ])->first();
                                if($modifierInfo && $modifierInfoLang) {
                                    $modifierInfo->modifier_name = $modifierInfoLang->modifier_name;
                                }
                                $premenu['modifierInfo'] = $modifierInfo;

                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as &$modifier_items_list) {
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as &$modifier_OptionGroup) {
                                                $itemModifierOptionGroup = ItemModifierOptionGroup::where([
                                                    'id' => $modifier_OptionGroup['id']
                                                ])->first();
                                                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                                                    'modifier_option_group_id' => $modifier_OptionGroup['id'],
                                                    'language_id' => $userMyBag['language_id']
                                                ])->first();
                                                if ($itemModifierOptionGroup && $itemModifierOptionGroupLanguage) {
                                                    $itemModifierOptionGroup->group_name = $itemModifierOptionGroupLanguage->group_name;
                                                    $itemModifierOptionGroup->prompt = $itemModifierOptionGroupLanguage->prompt;
                                                }
                                                $modifier_OptionGroup['group_name'] = $itemModifierOptionGroup->group_name;
                                                $modifier_OptionGroup['prompt'] = $itemModifierOptionGroup->prompt;

                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as &$modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();
                                                        $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                                            'modifier_option_id' => $modifier_OptionGroup_item['id'],
                                                            'language_id' => $userMyBag['language_id']
                                                        ])->first();
                                                        if ($itemModifierOptionGroupItemObj && $itemOptionsModifierLanguage) {
                                                            $itemModifierOptionGroupItemObj->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                                        }
                                                        $modifier_OptionGroup_item['modifier_option_name'] = $itemModifierOptionGroupItemObj->modifier_option_name;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }
                    $userBagItems[$i]['menu_json'] = $menuJsonDecoded;

                }

                $userBagItems[$i]['is_modifier'] = $is_modifier;

                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $menuItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info')->find($userMyBag['menu_id']);
                    if ($menuItemDetail->id) {
                        if (isset($userMyBag->addonoptions) && count($userMyBag->addonoptions)) {
                            foreach ($userMyBag->addonoptions as $key => &$babaddonoption) {
                                $addonInfo = ItemAddons::where('id', $babaddonoption->option_id)->with('addongroup')->first();
                                if ($addonInfo) {
                                    $addonItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info', 'gift_message')->find($addonInfo->item_id);
                                    $addonItemsLanguage = NewMenuItemsLanguage::where([
                                        'new_menu_items_id' => $addonInfo->item_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();
                                    if($addonItemsLanguage) {
                                        $addonItemDetail->item_other_info = $addonItemsLanguage->item_other_info;
                                        $addonItemDetail->gift_message = $addonItemsLanguage->gift_message;
                                        $addonItemDetail->name = $addonItemsLanguage->name;
                                        $addonItemDetail->description = $addonItemsLanguage->description;
                                        $addonItemDetail->sub_description = $addonItemsLanguage->sub_description;
                                    }
                                    $babaddonoption->addon_item_details = $addonItemDetail;

                                    $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                                        'item_addon_group_id' => $addonInfo->addon_group_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();

                                    if ($itemAddonGroupsLanguage) {
                                        $addonInfo->addongroup->group_name = $itemAddonGroupsLanguage->group_name;
                                        $addonInfo->addongroup->prompt = $itemAddonGroupsLanguage->prompt;
                                    }

                                    $babaddonoption->addongroup = $addonInfo->addongroup;
                                    //$userBagItems[$i]['mybagoptions'][$key]['addongroup']=$addonInfo->addongroup;
                                }
                            }
                        }
                        #@30-07-2018 By RG Multiple images for KEKI
                        $get_item_images = CommonFunctions::get_menu_item_imagesV2($menuItemDetail->id);
                        $menuItemDetail['images'] = $get_item_images;
                        $userBagItems[$i]['product_type'] = $menuItemDetail->product_type;
                        if ($menuItemDetail->product_type == 'gift_card') {
                            if (!is_null($menuItemDetail->item_other_info) && !empty($menuItemDetail->item_other_info)) {
                                $userBagItems[$i]['menu_item_other_info'] = $menuItemDetail->item_other_info;
                            } else {
                                $userBagItems[$i]['menu_item_other_info'] = null;
                            }

                            $item_otherinfo = !empty($userMyBag['item_other_info']) ? json_decode($userMyBag['item_other_info']) : '';
                            $promotion_applicable = (isset($item_otherinfo->promotion_applicable)) ? $item_otherinfo->promotion_applicable : 0;

                            if ($promotion_applicable) {

                                $bonus_for = (isset($item_otherinfo->bonus_for) && !empty($item_otherinfo->bonus_for)) ? $item_otherinfo->bonus_for : '';
                                $promotion_amount = (isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount)) ? $item_otherinfo->promotion_amount : 00.00;

                                if ($bonus_for == 'myself') {
                                    $myselfbonus_amount = $myselfbonus_amount + ($promotion_amount * $bag_quantity);

                                }
                                $userBagItems[$i]['promotion_amount'] = $promotion_amount;
                            }
                        } else {
                            $userBagItems[$i]['item_other_info'] = $menuItemDetail->item_other_info;
                        }
                    }
                    $menuItemDetail = &$menuItemDetail;
                    $this->menuItemLanguageWise($menuItemDetail, $userMyBag['language_id'], $menuItemDetail->id);
                } else {
                    $userBagItems[$i]['product_type'] = 'food_item';   //In case of Menu Id 0
                }
                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $adonsDetails = $this->getItemsAdonsWithAddonsGroup($userMyBag['menu_id'], $language_id, $request, $locationId);
                    $modifierDetails = $this->getModifiersGroup($userMyBag['menu_id'], $language_id, $request);
                    $userBagItems[$i]['adons'] = $adonsDetails;
                    $userBagItems[$i]['modifier'] = $modifierDetails;;
                }
                $userBagItems[$i]['menu_item_detail'] = $menuItemDetail;
                $i++;
            }


        }

        $restaurantInfo = Restaurant::with(array('city' => function ($query) {
            $query->select('id', 'sales_tax');
        }))->where('id',$locationId)->select('id','tip','tax','city_tax', 'delivery_charge','city_id', 'delivery_service_charge', 'minimum_delivery','restaurants.delivery_charge_type', 'free_delivery','restaurants.flat_discount_start_date','restaurants.flat_discount_end_date','restaurants.flat_discount','restaurants.flat_discount_type','restaurants.flat_discount_min','restaurants.tax','restaurants.service_tax','restaurants.additional_charge_type','restaurants.additional_charge','restaurants.additional_charge_name')->first();
        $sale_tax = 0.00;
        if ($restaurantInfo) {
            if ($restaurantInfo->city_tax) {
                $sale_tax = ($restaurantInfo['city'] && $restaurantInfo['city']['sales_tax']) ? $restaurantInfo['city']['sales_tax'] : 0.00;
            } else {
                $sale_tax = $restaurantInfo->tax;
            }
        }

        #@29-08-2018 by RG
        #this charge is applicable only on Delivery Type Orders and if service is enabled
        $service_provider_charge = 0;
        $rest_provider_data = Restaurant::select('restaurants.id', 'delivery_provider_id', 'delivery_services.provider_name', 'delivery_service_charge', 'delivery_services.id as del_pro')->join('delivery_services', 'restaurants.delivery_provider_id', 'delivery_services.id')->where(['restaurants.id' => $locationId, 'delivery_services.status' => '1'])->first();

        if ($rest_provider_data) {
            $service_provider_charge = $restaurantInfo->delivery_service_charge;
        }
	$discount = 0;
	
	if($total_menu_amount > $restaurantInfo->flat_discount_min && $restaurantInfo->flat_discount > 0 ){
		$dataTimeStamp= strtotime(date("Y-m-d"));
	        if(strtotime($restaurantInfo->flat_discount_start_date) >=$dataTimeStamp  &&   $dataTimeStamp<=strtotime($restaurantInfo->flat_discount_end_date)  ) {
			if($restaurantInfo->flat_discount_type == 1) $discount = $restaurantInfo->flat_discount;
			else $discount = ($total_menu_amount * $restaurantInfo->flat_discount)/100;
		}
	}
        /*if($total_menu_amount > $restaurantInfo->free_delivery)
            $delCharge = 0;
        else $delCharge = $restaurantInfo->delivery_charge;*/
	if($total_menu_amount > $restaurantInfo->free_delivery)
            $delCharge = 0;
        elseif($restaurantInfo->delivery_charge_type=='flat')$delCharge =$restaurantInfo->delivery_charge;
	else $delCharge = ($total_menu_amount*$restaurantInfo->delivery_service_charge)/100;
	$additionalCharge = 0;
	if($restaurantInfo->additional_charge>0){
		if( $restaurantInfo->additional_charge_type ==2)$additionalCharge = $restaurantInfo->additional_charge * $totalQuantity ;
		else $additionalCharge =$restaurantInfo->additional_charge;
	}
	  
	//$totalQuantity 	
        $data = [
            'list' => $userBagItems,
            'tip' => explode(",", $restaurantInfo->tip),
            'tax' => $restaurantInfo->tax,'service_tax' => $restaurantInfo->service_tax,'discount'=>$discount,
            'delivery_charge' => $delCharge,
            'service_provider_charge' => $service_provider_charge,
	    'minimum_delivery' =>$restaurantInfo->minimum_delivery !=null ? $restaurantInfo->minimum_delivery : 0,
            'free_delivery' =>$restaurantInfo->free_delivery,'additional_charge'=>$additionalCharge,'additional_charge_type'=>$restaurantInfo->additional_charge_type,'additional_charge_name'=>$restaurantInfo->additional_charge_name,
        ];

        if ($myselfbonus_amount != 0) {
            $data['myselfbonus_amount'] = $myselfbonus_amount;
            $data['myselfbonus_amount_message'] = 'You will recieve single bonus gift card worth $' . $myselfbonus_amount . '.';
        }

        return $data;
    }	
    public function cartV3(Request $request)
    {
        $ms     = microtime(true);
        $error  = null;
        $result = null;
        $status = 200;
        if($request->has('is_pot') && $request->post('is_pot') == 1){
            $is_pot_prod=1;
        }else{
            $is_pot_prod=0;
        }

        if($request->has('is_modifier') && $request->post('is_modifier') == 1){
            $is_modifier=1;
        }else{
            $is_modifier=0;
        }



        // Verify user token
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
            $bearerToken = true;
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp'              => 'sometimes|in:0,1',
                'is_pot'              => 'sometimes|in:0,1',
                'is_modifier'              => 'sometimes|in:0,1',
                'menu_json'           => 'sometimes',
                'addon_json'           => 'sometimes',
                'menu_id'             => 'sometimes|exists:new_menu_items,id',
                'order_type'          => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity'            => 'required|numeric',
            ]);

            if (!$validation->fails()) {
                if(  $request->post('is_byp')==1 || $request->post('menu_id')>0 ||  $is_modifier || $is_pot_prod ) {

                    $imageName    = $medImageName = "";
                    $quantity     = $request->post('quantity');
                    $locationId   = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo      = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees=0;
                    $imgdata         = null;
                    $item_other_info=null;
                    $prod_type='';
                    $subTotal = 0;

                    if ($request->input('menu_id')) {
                        $menuInfo      = NewMenuItems::where('id',$request->input('menu_id'))->first();

                        if(isset($menuInfo) && $menuInfo->product_type=='gift_card') {
                            ####################SALTY GIFT CARD RELATED########################

                            $prod_type='gift_card';
                            $promotion_amount=null;


                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                            CommonFunctions::validateGiftCard($request,$quantity,$menuItemPrice);
                            //validating Gift card;

                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $gift_type=($request->has('gift_type') && !empty($request->input('gift_type')) )?$request->input('gift_type'):'egiftcard';

                            $recipient=($request->has('recipient') && !empty($request->input('recipient')) )?$request->input('recipient'):'single';

                            $giftcard_for=($request->has('giftcard_for') && !empty($request->input('giftcard_for')) )?$request->input('giftcard_for'):'recipient';

                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';

                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'gift_type'    => $gift_type,
                                'recipient'    => $recipient,
                                'bonus_for'    => $giftcard_for,
                                'recipient_details'=>$recipientDetails,
                                'promotion_amount'    => $promotion_amount,
                                'promotion_applicable'=>0
                            ];

                            /* $promo=CommonFunctions::productPromotions($restaurantId);

                             if(!empty($promo) && $promo->amount_or_percent==0){

                                 $promo_implement=CommonFunctions::productPromoCondition($menuItemPrice,$promo->condition_amount,$promo->condition);


                                 if($promo_implement){
                                     $item_other_info['promotion_applicable']=1;
                                     $item_other_info['promotion_amount']=$promo->discount;
                                 }

                             }*/
                            $item_other_info=json_encode($item_other_info);


                        }else if(isset($menuInfo) && $menuInfo->product_type=='product') {
                            $prod_type='product';
                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;

                            $is_gift_wrapping=($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping')) )?$request->input('is_gift_wrapping'):0;


                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'is_gift_wrapping'      => trim($is_gift_wrapping),
                            ];

                            $item_other_info=json_encode($item_other_info);

                            if($is_gift_wrapping!=0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)){
                                $gift_wrapping_fees= (float)$menuInfo->gift_wrapping_fees;


                            }
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;

                            }

                        }else{

                            if($is_pot_prod){
                                $menuItemPrice=0; //As discussed with prakash sir
                            }else{
                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                //return response()->json([$menuInfo,$request->input('menu_id'),$menuSizePrice,$sizeArrTemp,$sizeIndex,$menuItemPrice]);

                            }

                        }


                        $imgarray = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);

                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image']           = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] =    $menuInfo->images;
                                $imgdata                      = json_encode($imgarray);
                            }
                        }else {
                            $imgarray['image']           = $menuInfo->web_image;
                            $imgarray['thumb_image_med'] = $menuInfo->web_thumb_med;
                            $imgarray['image_array'] =     $menuInfo->images;
                            $imgdata                      = json_encode($imgarray);
                        }

                    }



                    // add to MyBag
                    if($request->input('menu_id')>0) {
                        $unitPrice       = $menuItemPrice;
                    } else {
                        $unitPrice       = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice+$gift_wrapping_fees);
                    $orderType       = $request->post('order_type') ?? 'delivery';
                    $userAuthId      = $userAuth->id;
                    $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize        = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge  = $restInfo->delivery_charge;
                    $isByp           = false;

                    $addonJsonDecoded = json_decode($request->post('addon_json'), true) ?? null;
                    $addon_array=[];
                    if($is_pot_prod && $addonJsonDecoded ) {
                        $addon_price = 0;
                        foreach ($addonJsonDecoded as $addon_menu) {
                            if($addon_menu['price'] && $addon_menu['id'] && $addon_menu['quantity']) {
                                $addonInfo      = ItemAddons::where('id',$addon_menu['id'])->first(); //
                                //add more validation to it
                                if( $addonInfo ){
                                    $addon_price+= $addonInfo->addon_price * $addon_menu['quantity'];
                                    $addon_array[]=[
                                        //  'bag_id'=>,
                                        //addon_group_id
                                        //menu_item_id
                                        'option_name'=>$addonInfo->name,
                                        'option_id'=>$addonInfo->id,
                                        'price'=>$addonInfo->addon_price,
                                        'quantity'=>$addon_menu['quantity'],
                                        'total_amount'=>$addonInfo->addon_price * $addon_menu['quantity'],
                                        'bag_id'=>(isset($menuData['bag_id']) && !empty($menuData['bag_id']))?$menuData['bag_id']:null
                                    ];
                                }


                            }
                        }
                        $unitPrice=$addon_price;

                        $totalMenuAmount  =($quantity* $addon_price);

                    }else if($is_modifier && $menuJsonDecoded ) {
                        $customization_price = 0;
                        if(isset($menuJsonDecoded['modifier_items'])){

                            foreach ($menuJsonDecoded['modifier_items'] as $premenu) {
                                $modifier_item_id=$premenu['id'];
                                $modifierInfo      = ItemModifier::where('id',$premenu['id'])->first();
                                if( $modifierInfo ){
                                    $customization_price+= ($modifierInfo->price*$quantity);

                                }

                                // if(isset($premenu['modifier_options']) && count($premenu['modifier_options'])) {
                                //     foreach ($premenu['modifier_options'] as $cust_menu) {
                                //         $customization_price+= $cust_menu['price'] * $quantity;

                                //     }
                                // }
                            }

                        }

                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }else if(isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                             && $prod_type!='gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price+= $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }

                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if( $prod_type=='gift_card' && !$request->has('giftcard_for')){
                            $item_other_info=json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for=$old_item_otherinfo->bonus_for;
                            $item_other_info=json_encode($item_other_info);

                        }



                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            if($is_pot_prod) {
                                MybagOptions::where('bag_id',$myBagItem->id)->delete();
                            }
                            $result = $myBagItem->delete();


                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size'         => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if(isset($menuData['is_byp']) && $menuData['is_byp'] >0 ){
                                $unitPrice=$myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if( $prod_type=='gift_card'){
                                $myBagItem->unit_price           = $unitPrice ;
                                if($request->has('size')){
                                    $myBagItem->size=$request->post('size');
                                }
                            }
                            if($is_pot_prod) {



                                if(count($addon_array)>0){
                                    MybagOptions::where('bag_id',$myBagItem->id)->delete();
                                    MybagOptions::insert($addon_array);
                                }else{
                                    $unitPrice=$myBagItem->unit_price;
                                    $totalMenuAmount = $quantity * ($unitPrice);

                                }



                            }
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if(!is_null($menuJsonOld)) { sort($menuJsonOld); }
                                if(!is_null($menuJsonDecoded)) { sort($menuJsonDecoded); }
                                /*sort($menuJsonOld);
                            sort($menuJsonDecoded);*/
                                $menuFlag = false;
                                if(isset($menuData['menu_id']) && $bag->menu_id==$menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif($request->post('is_byp')==1) {
                                    $menuFlag = true;
                                }
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag && !$is_pot_prod ) {
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id      = $userAuthId;
                                    $myBagItem->user_id           = $userId;
                                    if($is_pot_prod && count($addon_array)>0) {
                                        $myBagItem->unit_price = $unitPrice;
                                    }
                                    $myBagItem->quantity          = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = $totalMenuAmount + $bag->total_menu_amount;
                                    //update menu json
                                    $myBagItem->menu_json         = $request->post('menu_json');
                                    $myBagItem->item_other_info   = $item_other_info;
                                    if($request->has('special_instruction')){
                                        $myBagItem->special_instruction   =  $request->post('special_instruction');
                                    }


                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    if($is_pot_prod) {
                                        MybagOptions::where('bag_id',$bag->id)->delete();
                                    }
                                    $bag->delete();

                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
                                $myBagItem->menu_json         = $request->post('menu_json');
                                $myBagItem->quantity          = $quantity;
                                if($is_pot_prod && count($addon_array)>0) {
                                    $myBagItem->unit_price = $unitPrice;
                                }
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->item_other_info   = $item_other_info;
                                if($request->has('special_instruction')){
                                    $myBagItem->special_instruction   =  $request->post('special_instruction');
                                }
                                $myBagItem->save();
                            }
                        }
                        $subTotal 	    +=$totalMenuAmount;
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }

                        $userBagItems = $this->getUserMyBagListV3($locationId, $userAuthId,  $request);
                        $me           = microtime(true) - $ms;

                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp                 = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath      = config('constants.image.path.byp');
                            $imageRelPath   = $imagePath . '/thumb/';
                            $image_parts    = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type     = $image_type_aux[1];

                            $img          = str_replace(' ', '+', $image_parts[1]);
                            $data         = base64_decode($img);
                            $uniqId       = uniqid(mt_rand());
                            $imageName    = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1   = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH      = intval($width_orig / 2);
                            $HEIGHT     = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, TRUE);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray                     = array();
                            $imgarray ['image']           = 'images/byp/' . $imageName;
                            $imgarray ['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata                      = json_encode($imgarray);
                        }
                    }
		   // store additional charge info in my bag
		        $restaurantInfo = Restaurant::where('id',$locationId)->select('id','tip','tax','city_tax', 'city_id', 'delivery_service_charge','delivery_charge',
				 'minimum_delivery','delivery_charge_type', 'free_delivery','flat_discount_start_date','flat_discount_end_date','flat_discount','flat_discount_type',
		'flat_discount_min','service_tax','additional_charge_type','additional_charge','additional_charge_name')->first();

			 
			$discount = 0;
			
			if($totalMenuAmount > $restaurantInfo->flat_discount_min && $restaurantInfo->flat_discount > 0 ){
				$dataTimeStamp= strtotime(date("Y-m-d"));
				if(strtotime($restaurantInfo->flat_discount_start_date) >=$dataTimeStamp  &&   $dataTimeStamp<=strtotime($restaurantInfo->flat_discount_end_date)  ) {
					if($restaurantInfo->flat_discount_type == 1) $discount = $restaurantInfo->flat_discount;
					else $discount = ($totalMenuAmount * $restaurantInfo->flat_discount)/100;
				}
			}
			 
			if($totalMenuAmount > $restaurantInfo->free_delivery)
			    $delCharge = 0;
			elseif($restaurantInfo->delivery_charge_type=='flat')$delCharge =$restaurantInfo->delivery_charge;
			else $delCharge = ($totalMenuAmount*$restaurantInfo->delivery_service_charge)/100;
			$additionalCharge = 0;
			if($restaurantInfo->additional_charge>0){
				if( $restaurantInfo->additional_charge_type ==2)$additionalCharge = $restaurantInfo->additional_charge * $totalQuantity ;
				else $additionalCharge =$restaurantInfo->additional_charge;
			}		
                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id'       => $restaurantId ?? 0,
                        'restaurant_id'         => $locationId,
                        'user_id'             => $userId,
                        'user_auth_id'        => $userAuthId,
                        'menu_id'             => $request->post('menu_id') ?? 0,
                        'is_byp'              => $isByp,
                        'is_pot'              => $is_pot_prod,
                        'menu_json'           => $request->post('menu_json'),
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity'            => $quantity,
                        'unit_price'          => $unitPrice,
                        'total_menu_amount'   => $totalMenuAmount,
                        'size'                => $itemSize,
                        'order_type'          => $orderType,
                        'delivery_charge'     => $deliveryCharge,
                        'image'               => $imgdata,
                        'status'              => 0,
                        'item_other_info'      => $item_other_info,
			'service_tax' => $restaurantInfo->service_tax,
			'discount'=>$discount,
			'additional_charge'=>$additionalCharge,
			'additional_charge_type'=>$restaurantInfo->additional_charge_type,
			'additional_charge_name'=>$restaurantInfo->additional_charge_name
                    ];


                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id')>0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    }else{
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);


                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if(is_array( $menuJsonOld )){
                                sort($menuJsonOld);
                            }
                            if(is_array( $menuJsonDecoded )){
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded && !$is_pot_prod) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id      = $userAuthId;
                                $bag->user_id           = $userId;
                                $bag->quantity          += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result     = $bag;
                                $bagUpdated = true;
                            }


                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if( $prod_type=='gift_card'){
                                $userAuthId      = $userAuth->id;
                                $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    if(!isset($menuData['bag_id']) && $result && $is_pot_prod && count($addon_array)){
                        foreach($addon_array as &$addon){
                            $addon['bag_id']=$result->id;
                        }
                        MybagOptions::insert($addon_array);
                    }
                    $userBagItems = $this->getUserMyBagListV3($locationId, $userAuthId,  $request);
                    $me           = microtime(true) - $ms;

                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }

    public function isUserEligibleForRefferalDiscount($email){
	$return = true;
	if($email!=""){
		$order = UserOrder::where(['email'=>$email])->get(); 
		if(count($order)>0)$return = false;
		//promocode,promocode_discount
	}
	return $return;
    }
    public function getRefferalDiscountAmount($code){
	$return = [1,10];
	return $return;
    }	

    public function cartByw(Request $request)
    {
        $ms     = microtime(true);
        $error  = null;
        $result = null;
        $status = 200;
        if($request->has('is_pot') && $request->post('is_pot') == 1){
            $is_pot_prod=1;
        }else{
            $is_pot_prod=0;
        }

        if($request->has('is_modifier') && $request->post('is_modifier') == 1){
            $is_modifier=1;
        }else{
            $is_modifier=0;
        }
	if($request->has('pos_id')){
            $pos_id=$request->has('pos_id');
        }else{
            $pos_id=null;
        }


        $deliveryChargeMessage = "";
        // Verify user token
        if ($request->bearerToken()) {
            $token       = $request->bearerToken();
            $field       = 'access_token';
	    $is_guest = 0;	
            $bearerToken = true;
            if(strtolower($request->post('order_type'))=="delivery"){
                $deliveryChargeMessage = "Our standard delivery fee is £2.50 with £15 order minimum. \n Orders with 'Free Pizza' promotions are charged £6.00 for delivery. \n <B> You qualify for FREE delivery If your order total (after discount) is £25 or above.</B>";
            }
        } else {
            $token       = explode(' ', $request->header('Authorization'))[1];
            $field       = 'guest_token';
            $bearerToken = false;
	    $is_guest = 1;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $menuData = $request->all();

            $validation = Validator::make($request->all(), [
                'is_byp'              => 'sometimes|in:0,1',
                'is_pot'              => 'sometimes|in:0,1',
                'is_modifier'              => 'sometimes|in:0,1',
                'menu_json'           => 'sometimes',
                'addon_json'           => 'sometimes',
                'menu_id'             => 'sometimes|exists:new_menu_items,id',
                'order_type'          => 'string|max:255',
                'special_instruction' => 'sometimes|string|max:255',
                'quantity'            => 'required|numeric',
            ]);

            if (!$validation->fails()) { 
                if(  $request->post('is_byp')==1 || $request->post('menu_id')>0 ||  $is_modifier || $is_pot_prod ) {

                    $imageName    = $medImageName = "";
                    $quantity     = $request->post('quantity');
                    $locationId   = $request->header('X-location');
                    $restaurantId = $request->header('X-restaurant');
                    // restaurant details
                    $restInfo = Restaurant::find($locationId)->first();
                    // menu item details
                    $menuInfo      = [];
                    $menuItemPrice = 0.00;
                    $gift_wrapping_fees=0;
                    $imgdata         = null;
                    $item_other_info=null;
                    $prod_type='';
                    $subTotal = 0;

                    if ($request->input('menu_id')) {
                        $menuInfo      = NewMenuItems::where('id',$request->input('menu_id'))->first();
			//$pos_id	     = $menuInfo->pos_id;
                        if(isset($menuInfo) && $menuInfo->product_type=='gift_card') {
                            ####################SALTY GIFT CARD RELATED########################

                            $prod_type='gift_card';
                            $promotion_amount=null;


                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                if(isset($menuSizePrice[$sizeIndex]['special_price']) && isset($menuSizePrice[$sizeIndex]['start_date']) && isset($menuSizePrice[$sizeIndex]['end_date'])){

                                    $special_price=$menuSizePrice[$sizeIndex]['special_price'];
                                    $special_price_start_date=$menuSizePrice[$sizeIndex]['start_date'];
                                    $special_price_end_date=$menuSizePrice[$sizeIndex]['end_date'];

                                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' =>date('Y-m-d H:i:s')));
                                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){

                                        $menuItemPrice = $special_price;
                                    }
                                }

                            }

                            CommonFunctions::validateGiftCard($request,$quantity,$menuItemPrice);
                            //validating Gift card;

                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $gift_type=($request->has('gift_type') && !empty($request->input('gift_type')) )?$request->input('gift_type'):'egiftcard';

                            $recipient=($request->has('recipient') && !empty($request->input('recipient')) )?$request->input('recipient'):'single';

                            $giftcard_for=($request->has('giftcard_for') && !empty($request->input('giftcard_for')) )?$request->input('giftcard_for'):'recipient';

                            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : '';

                            $recipientDetails = isset($menuJson->recipient_details) ? $menuJson->recipient_details : [];

                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'gift_type'    => $gift_type,
                                'recipient'    => $recipient,
                                'bonus_for'    => $giftcard_for,
                                'recipient_details'=>$recipientDetails,
                                'promotion_amount'    => $promotion_amount,
                                'promotion_applicable'=>0
                            ];

                            /* $promo=CommonFunctions::productPromotions($restaurantId);

                             if(!empty($promo) && $promo->amount_or_percent==0){

                                 $promo_implement=CommonFunctions::productPromoCondition($menuItemPrice,$promo->condition_amount,$promo->condition);

                                 if($promo_implement){
                                     $item_other_info['promotion_applicable']=1;
                                     $item_other_info['promotion_amount']=$promo->discount;
                                 }

                             }*/
                            $item_other_info=json_encode($item_other_info);


                        }else if(isset($menuInfo) && $menuInfo->product_type=='product') {
                            $prod_type='product';
                            $gift_wrapping_message=($request->has('gift_wrapping_message') && !empty($request->input('gift_wrapping_message')) )?$request->input('gift_wrapping_message'):null;

                            $other_denomination=($request->has('other_denomination') && !empty($request->input('other_denomination')) )?$request->input('other_denomination'):null;

                            $is_gift_wrapping=($request->has('is_gift_wrapping') && !empty($request->input('is_gift_wrapping')) )?$request->input('is_gift_wrapping'):0;


                            $item_other_info = [
                                'gift_wrapping_message' => $gift_wrapping_message,
                                'other_denomination'    => $other_denomination,
                                'is_gift_wrapping'      => trim($is_gift_wrapping),
                            ];

                            $item_other_info=json_encode($item_other_info);

                            if($is_gift_wrapping!=0 && $menuInfo->gift_wrapping_fees && !empty($menuInfo->gift_wrapping_fees)){
                                $gift_wrapping_fees= (float)$menuInfo->gift_wrapping_fees;


                            }
                            $other_denomination=trim($other_denomination);
                            if(!is_null($other_denomination) && $other_denomination!='null' && !empty($other_denomination)){
                                $menuItemPrice=$other_denomination;

                            }else{

                                $menuSizePrice = $menuInfo->size_price ? json_decode($menuInfo->size_price, true) : '';
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);

                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? current($menuSizePrice[$sizeIndex]['price']) : 0.00;
                                if(isset($menuSizePrice[$sizeIndex]['special_price']) && isset($menuSizePrice[$sizeIndex]['start_date']) && isset($menuSizePrice[$sizeIndex]['end_date'])){

                                    $special_price=$menuSizePrice[$sizeIndex]['special_price'];
                                    $special_price_start_date=$menuSizePrice[$sizeIndex]['start_date'];
                                    $special_price_end_date=$menuSizePrice[$sizeIndex]['end_date'];

                                    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' =>date('Y-m-d H:i:s')));
                                    $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                                    $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                                    if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){

                                        $menuItemPrice = $special_price;
                                    }
                                }

                            }

                        }else{

                            if($is_pot_prod){
                                $menuItemPrice=0; //As discussed with prakash sir
                            }else{
                                 
				if ($request->has('time')) {
            				$current_time = $request->query('time') . ':00';
				} else {
				    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
				    if (is_object($currentDateTimeObj)) {
					$current_time = $currentDateTimeObj->format('H:i:00');
				    } else {
					$current_time = date('H:i:00');
				    }
				}
				$menuSizePrice = json_decode($menuInfo->size_price, true);
				CommonFunctions::getSizePrice($menuSizePrice, $locationId, $current_time);	
				$sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($request->post('size'), $sizeArrTemp);
                                $menuItemPrice = (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ? $menuSizePrice[$sizeIndex]['price'] : 0.00;
                            }

                        }
			$imgdata1 = $imgarray1 = array();
			$isImg = json_decode($menuInfo->images,true);
			$defaultImage = '/images/Product_Sample_Image.png';
			//$imgdata1 = $menuInfo->images;
			$imgarray = array();
                        if(!empty($menuInfo->images)) {
                           $images_all = json_decode($menuInfo->images, true);
			   if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                //$images_all = json_decode($menuInfo->images, true)['desktop_web_images'];
                                $imgarray['image']           = $images_all['desktop_web_images'][0]['web_image'];
                                $imgarray['thumb_image_med'] = $images_all['desktop_web_images'][0]['web_thumb_med'];
                                $imgarray['image_array'] =    $images_all;
                                $imgdata                      = json_encode($imgarray);
                            }
                        }else {
                            $imgarray['image']           = isset($menuInfo->web_image)?$menuInfo->web_image:$defaultImage;
                            $imgarray['thumb_image_med'] = isset($menuInfo->web_thumb_med)?$menuInfo->web_thumb_med:$defaultImage;
                            $imgarray['image_array'] =     $menuInfo->images;
                            $imgdata                      = json_encode($imgarray);
                        }

                    }

		

                    // add to MyBag
                    if($request->input('menu_id')>0) {
                        $unitPrice       = $menuItemPrice;
                    } else {
                        $unitPrice       = $request->post('unit_price');
                    }
                    $totalMenuAmount = $quantity * ($unitPrice+$gift_wrapping_fees);
                    $orderType       = $request->post('order_type') ?? 'delivery';
                    $userAuthId      = $userAuth->id;
                    $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                    $menuJsonDecoded = json_decode($request->post('menu_json'), true) ?? null;
                    $itemSize        = $request->post('size') ?? $menuJsonDecoded['size'] ?? 0;
                    $deliveryCharge  = $restInfo->delivery_charge;
                    $isByp           = false;
		    
                    $addonJsonDecoded = json_decode($request->post('addon_json'), true) ?? null;
                    $addon_array=[];
                    if($is_pot_prod && $addonJsonDecoded ) {
                        $addon_price = 0;
                        foreach ($addonJsonDecoded as $addon_menu) {
                            if($addon_menu['price'] && $addon_menu['id'] && $addon_menu['quantity']) {
                                $addonInfo      = ItemAddons::where('id',$addon_menu['id'])->first(); //
                                //add more validation to it
                                if( $addonInfo ){
                                    $addon_price+= $addonInfo->addon_price * $addon_menu['quantity'];
                                    $addon_array[]=[
                                        //  'bag_id'=>,
                                        //addon_group_id
                                        //menu_item_id
                                        'option_name'=>$addonInfo->name,
                                        'option_id'=>$addonInfo->id,
                                        'price'=>$addonInfo->addon_price,
                                        'quantity'=>$addon_menu['quantity'],
                                        'total_amount'=>$addonInfo->addon_price * $addon_menu['quantity'],
                                        'bag_id'=>(isset($menuData['bag_id']) && !empty($menuData['bag_id']))?$menuData['bag_id']:null
                                    ];
                                }


                            }
                        }
                        $unitPrice=$addon_price;

                        $totalMenuAmount  =($quantity* $addon_price);

                    }else if($is_modifier && $menuJsonDecoded ) {
                        $customization_price = 0;
                        if(isset($menuJsonDecoded['modifier_items'])){

                            foreach ($menuJsonDecoded['modifier_items'] as $i=>$premenu) {
                                $modifier_item_id=$premenu['id'];
                                $modifierInfo      = ItemModifier::where('id',$premenu['id'])->first();
				
                                if( $modifierInfo ){
                                    $customization_price+= ($modifierInfo->price*$quantity);
			            $menuJsonDecoded['modifier_items'][$i]['price'] = $modifierInfo->price;		 	
				    //$modifier_pos_id	     =$modifierInfo->pos_id;
				    ///modifier options
				    if(!empty($premenu['dependent_modifier_items']['id'])){
				    	$modifierItems1 = DB::select('select option_amount from modifier_options  where id = :id', ['id' => $premenu['dependent_modifier_items']['id']]); 
			  		foreach($modifierItems1 as $itm){  
						$customization_price =  $customization_price + ($itm->option_amount*$quantity);
					}
				  }	
                                }

                                 
                            }

                        }

                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }else if(isset($menuData['is_byp']) && $menuData['is_byp'] == 0 && $menuJsonDecoded
                        && $prod_type!='gift_card') {
                        $customization_price = 0;
                        foreach ($menuJsonDecoded as $premenu) {
                            if($premenu['items']) {
                                foreach ($premenu['items'] as $cust_menu) {
                                    if($cust_menu['price'] && $cust_menu['default_selected']) {
                                        $customization_price+= $cust_menu['price'] * $quantity;
                                    }
                                }
                            }
                        }
                        $totalMenuAmount  = $totalMenuAmount  + $customization_price;
                    }
		    
                    if (isset($menuData['bag_id']) && $menuData['bag_id'] > 0) {
                        $myBagItem = UserMyBag::find($menuData['bag_id']);
                        /*****************Quantity increase bonus_for check*******************/
                        if( $prod_type=='gift_card' && !$request->has('giftcard_for')){
                            $item_other_info=json_decode($item_other_info);
                            $old_item_otherinfo = !empty($myBagItem->item_other_info) ? json_decode($myBagItem->item_other_info) : '';
                            $item_other_info->bonus_for=$old_item_otherinfo->bonus_for;
                            $item_other_info=json_encode($item_other_info);

                        }



                        /*****************Quantity increase bonus_for check End*******************/

                        // item already exists in cart
                        // in case of +/- update cart
                        if ($quantity < 1) {
                            // remove item from MyBag
                            if($is_pot_prod) {
                                MybagOptions::where('bag_id',$myBagItem->id)->delete();
                            }
                            $result = $myBagItem->delete();


                        } else {
                            // check if menu item exists in db
                            $bagItem = UserMyBag::where([
                                //'size'         => $itemSize, 'location_id' => $locationId,
                                'size'         => $itemSize, 'restaurant_id' => $locationId,
                                'user_auth_id' => $userAuthId,
                            ])->where('id', '<>', $menuData['bag_id'])->get();  // 'menu_id' => $request->post('menu_id'),

                            $bagUpdated = false;
                            if(isset($menuData['is_byp']) && $menuData['is_byp'] >0 ){
                                $unitPrice=$myBagItem->unit_price;
                                $totalMenuAmount = $quantity * ($unitPrice);
                            }
                            if( $prod_type=='gift_card'){
                                $myBagItem->unit_price           = $unitPrice ;
                                if($request->has('size')){
                                    $myBagItem->size=$request->post('size');
                                }
                            }
                            if($is_pot_prod) {

                                if(count($addon_array)>0){
                                    MybagOptions::where('bag_id',$myBagItem->id)->delete();
                                    MybagOptions::insert($addon_array);
                                }else{
                                    $unitPrice=$myBagItem->unit_price;
                                    $totalMenuAmount = $quantity * ($unitPrice);

                                }



                            }
			    $no_of_referral_applied = 0;$referal_discount=0;$coupon_code="";$referral_applied=0; 
			    if($is_guest != 1 && $userAuth->user_id > 0){
				   
				    $userInfo      = User::find($userAuth->user_id);
				    $coupon_code = $userInfo->referral_code;	
				    //referral_restaurant_id	
				    if($coupon_code!="" && $this->isUserEligibleForRefferalDiscount($userInfo->email)){
					 $referral_applied=1; 
					 $ref_amount = $this->getRefferalDiscountAmount($coupon_code);
					 $referal_discount =   $ref_amount[0];	
					 $no_of_referral_applied = $ref_amount[1];
				    }
				     
			    } 
                            foreach ($bagItem as $bag) {
                                // check for menu_json customization if same or not
                                $menuJsonOld = json_decode($bag->menu_json, true);
                                if(!is_null($menuJsonOld)) { sort($menuJsonOld); }
                                if(!is_null($menuJsonDecoded)) { sort($menuJsonDecoded); }
                                 
                                $menuFlag = false;
                                if(isset($menuData['menu_id']) && $bag->menu_id==$menuData['menu_id']) {
                                    $menuFlag = true;
                                } elseif($request->post('is_byp')==1) {
                                    $menuFlag = true;
                                }
				
				
                                if (($menuJsonOld == $menuJsonDecoded) && !$bagUpdated && $menuFlag && !$is_pot_prod ) {
				    
                                    // same customization, update same row with quantity
                                    $myBagItem->user_auth_id      = $userAuthId;
                                    $myBagItem->user_id           = $userId;
                                    if($is_pot_prod && count($addon_array)>0) {
                                        $myBagItem->unit_price = $unitPrice;
                                    }
				    $referal_discount =  (($quantity + $bag->quantity) * $referal_discount);
				    $myBagItem->coupon_code          =	$coupon_code;
				    
				    $referal_discount = ($no_of_referral_applied < $referal_discount)?$referal_discount:$no_of_referral_applied;
				    $myBagItem->discount =  $referal_discount;	
                                    $myBagItem->quantity          = $quantity + $bag->quantity;
                                    $myBagItem->total_menu_amount = (($totalMenuAmount + $bag->total_menu_amount) - $referal_discount);
                                    //update menu json
                                    //$myBagItem->menu_json         = json_encode($menuJsonDecoded);//$request->post('menu_json');
				    $myBagItem->pos_id         = $request->post('pos_id');	
                                    $myBagItem->item_other_info   = $item_other_info;
                                    if($request->has('special_instruction')){
                                        $myBagItem->special_instruction   =  $request->post('special_instruction');
                                    }


                                    $myBagItem->save();

                                    // delete duplicate cart item
                                    if($is_pot_prod) {
                                        MybagOptions::where('bag_id',$bag->id)->delete();
                                    }
                                    $bag->delete();

                                    $bagUpdated = true;
                                }
                            }

                            // update quantity in MyBag, if not already updated
                            if (!$bagUpdated) {
				//$myBagItem->pos_id         = $request->post('pos_id');
                                $myBagItem->menu_json         = $request->post('menu_json');//json_encode($menuJsonDecoded);//
                                $myBagItem->quantity          = $quantity;
                                if($is_pot_prod && count($addon_array)>0) {
                                    $myBagItem->unit_price = $unitPrice;
                                }
                                $myBagItem->total_menu_amount = $totalMenuAmount;
                                $myBagItem->item_other_info   = $item_other_info;
                                if($request->has('special_instruction')){
                                    $myBagItem->special_instruction   =  $request->post('special_instruction');
                                }     
                                $myBagItem->save();
                            }
                        }
                        $subTotal 	    +=$totalMenuAmount;
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }

                        $userBagItems = $this->getUserMyBagListV2Byw($locationId, $userAuthId,  $request);
                        $me           = microtime(true) - $ms;
                        $userBagItems['delivery_charge_message'] = $deliveryChargeMessage;

                        return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                    } else if (isset($menuData['is_byp']) && $menuData['is_byp']) {
                        $isByp                 = true;
                        $menuData['menu_json'] = json_decode($request->post('menu_json'), true);
                        //$result = $this->validatePizza($menuData);
                        if (isset($menuData['image'])) {
                            //$imageRelPath = config('constants.image.rel_path.byp');
                            $imagePath      = config('constants.image.path.byp');
                            $imageRelPath   = $imagePath . '/thumb/';
                            $image_parts    = explode(";base64,", $menuData['image']);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $image_type     = $image_type_aux[1];

                            $img          = str_replace(' ', '+', $image_parts[1]);
                            $data         = base64_decode($img);
                            $uniqId       = uniqid(mt_rand());
                            $imageName    = $restaurantId . '_BYP_' . $uniqId . '.' . $image_type;
                            $medImageName = $restaurantId . '_BYP_' . $uniqId . '_med' . '.' . $image_type;
                            $imagePath1   = $imagePath . '/' . $imageName;
                            file_put_contents($imagePath1, $data);
                            // The directory where is your image
                            $filePath = $imagePath1;
                            // This little part under depend if you wanna keep the ratio of the image or not
                            list($width_orig, $height_orig) = getimagesize($filePath);
                            $ratio_orig = $width_orig / $height_orig;
                            $WIDTH      = intval($width_orig / 2);
                            $HEIGHT     = intval($height_orig / 2);
                            if ($WIDTH / $HEIGHT > $ratio_orig) {
                                $WIDTH = $HEIGHT * $ratio_orig;
                            } else {
                                $HEIGHT = $WIDTH / $ratio_orig;
                            }
                            if ($image_type == "png") {
                                $image = imagecreatefrompng($filePath);
                            } else {
                                $image = imagecreatefromjpeg($filePath);
                            }
                            $bg = imagecreatetruecolor($WIDTH, $HEIGHT);
                            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                            imagealphablending($bg, TRUE);
                            imagecopyresampled($bg, $image, 0, 0, 0, 0, $WIDTH, $HEIGHT, $width_orig, $height_orig);
                            imagedestroy($image);
                            imagejpeg($bg, $imageRelPath . $medImageName, 100);
                            file_get_contents($imageRelPath . $medImageName);

                            $imgarray                     = array();
                            $imgarray ['image']           = 'images/byp/' . $imageName;
                            $imgarray ['thumb_image_med'] = 'images/byp/thumb/' . $medImageName;
                            $imgdata                      = json_encode($imgarray);
                        }
                    }
		    $posJson = $request->post('pos_data')??'';
		    	
                    // NO BAG_ID, check for Normal/BYP
                    $data = [
                        //'restaurant_id'       => $restaurantId ?? 0,
                        // 'location_id'         => $locationId,
                        'parent_restaurant_id'   => $restaurantId ?? 0,
                        'restaurant_id'   => $locationId,
                        'user_id'             => $userId,
                        'user_auth_id'        => $userAuthId,
                        'menu_id'             => $request->post('menu_id') ?? 0,
                        'is_byp'              => $isByp,
                        'is_pot'              => $is_pot_prod,
                        'menu_json'           => json_encode($menuJsonDecoded),//$request->post('menu_json'),
			'pos_data'	      => $posJson,
                        'special_instruction' => $request->post('special_instruction') ?? '',
                        'quantity'            => $quantity,
                        'unit_price'          => $unitPrice,
                        'total_menu_amount'   => $totalMenuAmount,
                        'size'                => $itemSize,
                        'order_type'          => $orderType,
                        'delivery_charge'     => $deliveryCharge,
                        'image'               => $imgdata,
			'images'               => '',
                        'status'              => 0,
                        'item_other_info'      => $item_other_info,
			'pos_id'	       => $request->post('pos_id')
                    ];


                    // check if menu item exists in db
                    if ($request->has('menu_id') && $request->post('menu_id')>0) {
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId, 'menu_id' => $request->post('menu_id'),
                        ])->get();

                    }else{
                        $bagItem = UserMyBag::where([
                            //'size'         => $itemSize, 'location_id' => $request->post('location_id'),
                            'size'         => $itemSize, 'restaurant_id' =>$locationId,
                            'user_auth_id' => $userAuthId
                        ])->get();

                    }
                    //return response()->json([$bagItem, count($bagItem)]);

		    //$data['pos_id']=$menuInfo->pos_id;
                    if (count($bagItem)) {
                        $bagUpdated = false;
                        foreach ($bagItem as $bag) {
                            // check for menu_json customization if same or not

                            $menuJsonOld = json_decode($bag->menu_json, true);
                            if(is_array( $menuJsonOld )){
                                sort($menuJsonOld);
                            }
                            if(is_array( $menuJsonDecoded )){
                                sort($menuJsonDecoded);
                            }

                            if ($menuJsonOld == $menuJsonDecoded && !$is_pot_prod) {
                                // same customization, update same row with quantity
                                $bag->user_auth_id      = $userAuthId;
                                $bag->user_id           = $userId;
                                $bag->quantity          += $quantity;
                                $bag->total_menu_amount += $totalMenuAmount;
                                $bag->save();
                                $result     = $bag;
                                $bagUpdated = true;
                            }


                        }
                        if (!$bagUpdated) {
                            $result = UserMyBag::create($data);
                            if( $prod_type=='gift_card'){
                                $userAuthId      = $userAuth->id;
                                $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                                // $this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                            }
                        }
                    } else {
                        // item don't exists, add new record
                        $result = UserMyBag::create($data);
                        if( $prod_type=='gift_card'){
                            $userAuthId      = $userAuth->id;
                            $userId          = $userAuth->user_id ?? $request->post('user_id') ?? 0;
                            //$this->promotionalCardHandle($locationId, $userAuthId,$restaurantId,$userId);
                        }
                    }
                    if(!isset($menuData['bag_id']) && $result && $is_pot_prod && count($addon_array)){
                        foreach($addon_array as &$addon){
                            $addon['bag_id']=$result->id;
                        }
                        MybagOptions::insert($addon_array);
                    }
                    $userBagItems =  $this->getUserMyBagListV2Byw($locationId, $userAuthId,  $request);
                    $me           = microtime(true) - $ms;
                    $userBagItems['delivery_charge_message'] = $deliveryChargeMessage;
                    return response()->json(['data' => $userBagItems, 'error' => null, 'xtime' => $me], 200);
                } else {
                    $me = microtime(true) - $ms;

                    return response()->json(['data' => null, 'error' => 'Not a valid item.', 'xtime' => $me], 400);
                }
            } else {
                $me = microtime(true) - $ms;

                return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], 400);
            }
        } else {
            $tokenType = $bearerToken ? 'Access' : ' Guest';
            $me        = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenType . ' token is not invalid.', 'xtime' => $me], 400);
        }
    }

    public function showAllUserMyBagItemsV2Byw(Request $request) {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagListV2Byw($locationId, $userAuthId,  $request);
	     	
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userMyBagItems, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

     
    private function getUserMyBagListV2Byw($locationId, $userAuthId, Request $request) {
        $localization_id = $request->header('X-localization');
        $language_id = CommonFunctions::getLanguageInfo($localization_id);
        $language_id = $language_id->id;
        $orderType = $request->input('order_type');     
      
        
        if(Delivery::getPizzaItemForDeal($locationId, $userAuthId) && Delivery::$deal_on_takeout_only_fifty_percent==1){  
            Delivery::removeCouponForPizzaDeal($locationId, $userAuthId);
        }
        //$userBagItems = UserMyBag::where(['location_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->with('addonoptions')->get();
        $total_menu_amount = $myselfbonus_amount = $total_item=0;
        $pizzas_amount = 0;
        $order_type='';
        $coupon_code='';
	$ref_code_quantity=0;
	$userAuth = UserAuth::where(['id' => $userAuthId])->first();	
        if ($userBagItems) {
            $i = 0;
            
            foreach ($userBagItems as $userMyBag) {

                if(!empty($userMyBag['coupon_code'])){
                    $coupon_code=$userMyBag['coupon_code'];
                }
                $order_type=$userMyBag['order_type'];
                $total_menu_amount  += $userMyBag['total_menu_amount'];
                if(Delivery::getPizzaItemForDealV2($userMyBag['menu_id']) && $userMyBag['order_type']=="carryout" && Delivery::$deal_on_takeout_only_fifty_percent==1){                    
                    $pizzas_amount += $userMyBag['unit_price'] * $userMyBag['quantity'];
                    $coupon_code='';                   
                }      
                $ref_code_quantity += $userMyBag['quantity'];         
                $total_item  += $userMyBag['quantity'];
                $is_modifier = 0;
                $bag_quantity = $userMyBag['quantity'];
                $menuItemDetail = '';
                if (isset($userMyBag['menu_json']) && !is_null($userMyBag['menu_json'])) {
                    if ($userMyBag['image']) {
                        $userBagItems[$i]['image'] = json_decode($userMyBag['image'], true);
                        //$userBagItems[$i]['thumb_med'] = "/images/mobile/defaultPizza.png";
                    } else {
                        $userBagItems[$i]['image'] = [
                            "image" => "/images/mobile/defaultPizza.png",
                            "thumb_med" => "/images/mobile/defaultPizza.png",
                            "thumb_sml" => "/images/mobile/defaultPizza.png",
                            "web_image" => "/images/mobile/defaultPizza.png",
                            "web_thumb_med" => "/images/mobile/defaultPizza.png",
                            "web_thumb_sml" => "/images/mobile/defaultPizza.png",
                        ];
                    }
                    $menuJsonDecoded = json_decode($userBagItems[$i]['menu_json'], true);
                    $is_modifier = 1;

                    if ($is_modifier && $menuJsonDecoded) {
                        if (isset($menuJsonDecoded['modifier_items'])) {

                            foreach ($menuJsonDecoded['modifier_items'] as &$premenu) {
                                $modifier_item_id = $premenu['id'];
                                $modifierInfo = ItemModifier::where('id', $premenu['id'])->first();
                                $modifierInfoLang = ItemModifierLanguage::where([
                                    'modifier_item_id' => $premenu['id'],
                                    'language_id' => $userMyBag['language_id']
                                ])->first();
                                if($modifierInfo && $modifierInfoLang) {
                                    $modifierInfo->modifier_name = $modifierInfoLang->modifier_name;
                                }
                                $premenu['modifierInfo'] = $modifierInfo;

                                if (isset($premenu['modifier_items_list'])) {
                                    foreach($premenu['modifier_items_list'] as &$modifier_items_list) {
					 
                                        if(isset($modifier_items_list['modifier_OptionGroup_list'])) {
                                            foreach ($modifier_items_list['modifier_OptionGroup_list'] as &$modifier_OptionGroup) {
                                                $itemModifierOptionGroup = ItemModifierOptionGroup::where([
                                                    'id' => $modifier_OptionGroup['id']
                                                ])->first();
                                                $itemModifierOptionGroupLanguage = ItemModifierOptionGroupLanguage::where([
                                                    'modifier_option_group_id' => $modifier_OptionGroup['id'],
                                                    'language_id' => $userMyBag['language_id']
                                                ])->first();
                                                if ($itemModifierOptionGroup && $itemModifierOptionGroupLanguage) {
                                                    $itemModifierOptionGroup->group_name = $itemModifierOptionGroupLanguage->group_name;
                                                    $itemModifierOptionGroup->prompt = $itemModifierOptionGroupLanguage->prompt;
                                                }
                                                $modifier_OptionGroup['group_name'] = $itemModifierOptionGroup->group_name;
                                                $modifier_OptionGroup['prompt'] = $itemModifierOptionGroup->prompt;

                                                if($modifier_OptionGroup['modifier_OptionGroup_item_list']) {
                                                    foreach ($modifier_OptionGroup['modifier_OptionGroup_item_list'] as &$modifier_OptionGroup_item) {
                                                        $itemModifierOptionGroupItemObj = ItemModifierOptionGroupItem::where([
                                                            'id' => $modifier_OptionGroup_item['id']
                                                        ])->first();
							
                                                        $itemOptionsModifierLanguage = ItemOptionsModifierLanguage::where([
                                                            'modifier_option_id' => $modifier_OptionGroup_item['id'],
                                                            'language_id' => $userMyBag['language_id']
                                                        ])->first();
                                                        if ($itemModifierOptionGroupItemObj && $itemOptionsModifierLanguage) {
                                                            $itemModifierOptionGroupItemObj->modifier_option_name = $itemOptionsModifierLanguage->modifier_option_name;
                                                        }
                                                        $modifier_OptionGroup_item['modifier_option_name'] = $itemModifierOptionGroupItemObj->modifier_option_name;
							
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                    }

                    $userBagItems[$i]['menu_json'] = $menuJsonDecoded;

                }

                $userBagItems[$i]['is_modifier'] = $is_modifier;
		$defaultImage = '/images/Product-Image.png';
                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $menuItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image','images', 'thumb_image_med', 'image_caption', 'item_other_info')->find($userMyBag['menu_id']);
		    $images = null;
		   //$userBagItems[$i]['images'] = json_decode($menuItemDetail->images,true);
		    //if(isset($userBagItems[$i]['images'])  && $userBagItems[$i]['images']!="") {
		        //$userBagItems[$i]['images']  = json_decode($menuItemDetail->images,true);
			$userBagItems[$i]['images']  = ($menuItemDetail->images!="")?json_decode($menuItemDetail->images,true):null;
		        $imageAr1=["mobile_app_images"=>["image" => $defaultImage, "thumb_med" => $defaultImage, "thumb_sml" => $defaultImage, "image_caption" =>  ''],
					"desktop_web_images"=>["web_image" => $defaultImage, "web_thumb_med" => $defaultImage, "web_thumb_sml" => $defaultImage, "web_image_caption" => ''],
					"mobile_web_images"=>["mobile_web_image" =>$defaultImage, "mobile_web_thumb_med" => $defaultImage, "mobile_web_thumb_sml" => $defaultImage, "mobile_web_image_caption" =>  ''],
					"banner_image" => '',
					"thumb_banner_image" => '',
			    	];	
			    //if($userBagItems[$i]['images']!=null){
				foreach($userBagItems[$i]['images'] as $key=>$imageAr){
					//$userBagItems[$i]['images'][$key] = $imageAr;//isset($imageAr[0])?$imageAr[0]:$imageAr1;  
				}
			    //}
		    //}
		 		
                    if ($menuItemDetail->id) {
			//$images = $menuItemDetail->id
                        if (isset($userMyBag->addonoptions) && count($userMyBag->addonoptions)) {
                            foreach ($userMyBag->addonoptions as $key => &$babaddonoption) {
                                $addonInfo = ItemAddons::where('id', $babaddonoption->option_id)->with('addongroup')->first();
                                if ($addonInfo) {
                                    $addonItemDetail = NewMenuItems::select('id', 'prompt', 'product_type', 'name', 'description', 'image', 'thumb_image_med', 'image_caption', 'item_other_info', 'gift_message')->find($addonInfo->item_id);
                                    $addonItemsLanguage = NewMenuItemsLanguage::where([
                                        'new_menu_items_id' => $addonInfo->item_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();
                                    if($addonItemsLanguage) {
                                        $addonItemDetail->item_other_info = $addonItemsLanguage->item_other_info;
                                        $addonItemDetail->gift_message = $addonItemsLanguage->gift_message;
                                        $addonItemDetail->name = $addonItemsLanguage->name;
                                        $addonItemDetail->description = $addonItemsLanguage->description;
                                        $addonItemDetail->sub_description = $addonItemsLanguage->sub_description;
                                    }
                                    $babaddonoption->addon_item_details = $addonItemDetail;

                                    $itemAddonGroupsLanguage = ItemAddonGroupsLanguage::where([
                                        'item_addon_group_id' => $addonInfo->addon_group_id,
                                        'language_id' => $userMyBag['language_id'],
                                    ])->first();

                                    if ($itemAddonGroupsLanguage) {
                                        $addonInfo->addongroup->group_name = $itemAddonGroupsLanguage->group_name;
                                        $addonInfo->addongroup->prompt = $itemAddonGroupsLanguage->prompt;
                                    }

                                    $babaddonoption->addongroup = $addonInfo->addongroup;
                                    //$userBagItems[$i]['mybagoptions'][$key]['addongroup']=$addonInfo->addongroup;
                                }
                            }
                        }
                        #@30-07-2018 By RG Multiple images for KEKI
                        $get_item_images = CommonFunctions::get_menu_item_imagesV2($menuItemDetail->id);
                        $menuItemDetail['images'] = $get_item_images;
                        $userBagItems[$i]['product_type'] = $menuItemDetail->product_type;
                        if ($menuItemDetail->product_type == 'gift_card') {
                            if (!is_null($menuItemDetail->item_other_info) && !empty($menuItemDetail->item_other_info)) {
                                $userBagItems[$i]['menu_item_other_info'] = $menuItemDetail->item_other_info;
                            } else {
                                $userBagItems[$i]['menu_item_other_info'] = null;
                            }

                            $item_otherinfo = !empty($userMyBag['item_other_info']) ? json_decode($userMyBag['item_other_info']) : '';
                            $promotion_applicable = (isset($item_otherinfo->promotion_applicable)) ? $item_otherinfo->promotion_applicable : 0;

                            if ($promotion_applicable) {

                                $bonus_for = (isset($item_otherinfo->bonus_for) && !empty($item_otherinfo->bonus_for)) ? $item_otherinfo->bonus_for : '';
                                $promotion_amount = (isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount)) ? $item_otherinfo->promotion_amount : 00.00;

                                if ($bonus_for == 'myself') {
                                    $myselfbonus_amount = $myselfbonus_amount + ($promotion_amount * $bag_quantity);

                                }
                                $userBagItems[$i]['promotion_amount'] = $promotion_amount;
                            }
                        } else {
                            $userBagItems[$i]['item_other_info'] = $menuItemDetail->item_other_info;
                        }
                    }
                    $menuItemDetail = &$menuItemDetail;
                    $this->menuItemLanguageWise($menuItemDetail, $userMyBag['language_id'], $menuItemDetail->id);
                } else {
                    $userBagItems[$i]['product_type'] = 'food_item';   //In case of Menu Id 0
                }
                if (!is_null($userMyBag['menu_id']) && $userMyBag['menu_id'] > 0) {
                    $adonsDetails = $this->getItemsAdonsWithAddonsGroup($userMyBag['menu_id'], $language_id, $request, $locationId);
                    $modifierDetails = $this->getModifiersGroup($userMyBag['menu_id'], $language_id, $request);
                    $userBagItems[$i]['adons'] = $adonsDetails;
                    $userBagItems[$i]['modifier'] = $modifierDetails;;
                    $userBagItems[$i]['labels'] = $this->labelDetails($userMyBag['menu_id']);
                }
                $userBagItems[$i]['menu_item_detail'] = $menuItemDetail;
                $i++;
            }

        }

        
        /*********************NEW DELIVERY LOGIC --30th September 2019****************************/
        $data=[];
        $coupon_discount_amount=0;
        $voucher_applied=0;

        if(!empty($coupon_code) && !Delivery::getPizzaItemForDeal($locationId, $userAuthId)){
            $coupan_applied=Delivery::validateCoupan('cart',$request,$coupon_code,$userAuthId,$total_menu_amount);
            if(!empty($coupan_applied)&& count($coupan_applied)){
                $data['promocode_error']=$coupan_applied['promocode_error'];
            }
            if(!empty($coupan_applied)&& count($coupan_applied) && isset($coupan_applied['promocode_discount'])){
                $voucher_applied=1;
                $coupon_discount_amount=$coupan_applied['promocode_discount'];
                $data['promocode'] = $coupon_code;
                //$data['flat_discount']=0;
                $data['promocode_discount'] = $coupan_applied['promocode_discount'];
            }
        }
	  

        $delivery_charge_amnt=0;
                
        $delivery_data=Delivery::getDelivery('cart',$request,$locationId,$order_type,$total_item,$total_menu_amount,0,$coupon_discount_amount,$pizzas_amount);
         //print_r($delivery_data);die;
        if(!empty($delivery_data) && is_array($delivery_data)){
            $datadelivery=(array)$delivery_data;
            $data= array_merge($data,$datadelivery);
            $delivery_charge_amnt=$data['delivery_charge'];
        }
        /*if($data['delivery_charge']!=0 && $data['special_deal_delivery']==1){
            $data['delivery_charge'] = Delivery::$special_deal_delivery_charge;
        }*/
        if($voucher_applied){

            $data['minimum_delivery']=$coupan_applied['minimum_delivery'];
            $data['checkout_allowed']=$coupan_applied['minimum_delivery'] <= $total_menu_amount;
            $validateAmountForCoupon=isset($data['flat_discount'])?($total_menu_amount-$data['flat_discount']):$total_menu_amount;
            if($coupan_applied['promocode_discount']>$validateAmountForCoupon){
                $data['promocode_discount'] = $validateAmountForCoupon;

            }

          /*********Static deliver charge for particular coupon*************/
           // if((Delivery::$static_pizza_coupon_free==$coupon_code || Delivery::$static_pizza_coupon==$coupon_code || Delivery::$static_pizza_coupon2==$coupon_code) && $delivery_charge_amnt!=0){
            if((Delivery::$static_pizza_coupon_free==$coupon_code || Delivery::$static_pizza_coupon==$coupon_code || Delivery::$static_pizza_coupon2==$coupon_code) && $data['is_greater_freedelivery']==0){

                $data['delivery_charge'] = Delivery::$static_pizza_coupon_delivery;

            }
           
        }
	if($userAuth->user_id > 0 && $voucher_applied!=1){
		$userInfo      = User::find($userAuth->user_id);
		$coupon_code = $userInfo->referral_code;	
		if($coupon_code!="" && $this->isUserEligibleForRefferalDiscount($userInfo->email)){
			 $referral_applied=1; 
			 $ref_amount = $this->getRefferalDiscountAmount($coupon_code);
			// $referal_discount =   $ref_amount[0];	
			// $no_of_referral_applied = $ref_amount[1];
			 $data['promocode_discount'] = ($ref_amount[0] * $ref_code_quantity);
			 $data['promocode_discount'] = ($data['promocode_discount'] > $ref_amount[1])?$ref_amount[1]:$data['promocode_discount'];
			 $data['promocode'] = $coupon_code;
		}
	} 

        /*********************End -- NEW DELIVERY LOGIC --30th September 2019****************************/

        $data['list']=$userBagItems;
        /*************************************************/

        if ($myselfbonus_amount != 0) {
            $data['myselfbonus_amount'] = $myselfbonus_amount;
            $data['myselfbonus_amount_message'] = 'You will recieve single bonus gift card worth $' . $myselfbonus_amount . '.';
        }

        return $data;
    }

    public function labelDetails($menu_id)
    {
        $labelIdRes      = MenuItemLabels::select('id','labels','size','item_id')
            ->where('item_id', $menu_id)->first();
        $labelNameData = [];
        $serving = [0,0];
        if($labelIdRes) {
            $labelData = $labelIdRes->toArray();
            $labelIds = strlen($labelData['labels']) ? explode(',', $labelData['labels']) : '';
            if(strpos($labelData['size'], ',') !== false ) {
                $serving  = explode(',', $labelData['size']);
            } else {
                $serving  = [1,1];
            }
            $labelNameData = Labels::select('id','name','type')->whereIn('id', $labelIds)->get();
            if($labelNameData) {
                $labelNameData = $labelNameData->toArray();
            }
        }
        $labelData = [
            'label' => $labelNameData,
            'serving_size_min'  => $serving[0] <= $serving[1] ? $serving[0] : $serving[1],
            'serving_size_max'  => $serving[0] > $serving[1] ? $serving[0] : $serving[1],
        ];
        return $labelData;
    }

    public  function reorder($id,Request $request){

        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if ($userAuth) {
            $invalid_products=CommonFunctions::getMybagDataByOrder($id,$request);

            $userAuthId = $userAuth->id;
            $locationId = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $userMyBagItems = $this->getUserMyBagListV2Byw($locationId, $userAuthId, $request);

            return response()->json(['data' =>['invalid_products'=>$invalid_products,'bag_items'=>$userMyBagItems], 'error' => null], Config('constants.status_code.STATUS_SUCCESS'));
        }else {

            return response()->json(['data' => null, 'error' => $tokenField . ' token not valid.'], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }


    }

    public  function applyCoupon(Request $request){
         Delivery::applyCoupon($request);         

    }


    public  function removeCoupon(Request $request){

         Delivery::removeCoupon($request);


    }

    public function getTotalPaymentIntent(Request $request) {

        $localization_id = $request->header('X-localization');

        $locationId   = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
        if($restData) {
            $curSymbol = $restData->currency_symbol;
            $curCode = $restData->currency_code;
        } else {
            $curSymbol = config('constants.currency');
            $curCode = config('constants.currency_code');
        }
        $userAuthId = $userAuth->id;
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->get();
        $total_menu_amount = $total_item=0;
        $order_type='';

        $tipAmount      = 0;
        $tipPercent      = 0;
        $tax            = 0;
        $service_tax   = 0;
        $deliveryCharge   = 0;
        $additional_charge   = 0;
        $flat_discount   = 0;
        $coupon_code='';

        $finalOrderAmount=0;
        $intent_key='';
        if ($userBagItems) {


            foreach ($userBagItems as $userMyBag) {

                if(!empty($userMyBag['coupon_code'])){
                    $coupon_code=$userMyBag['coupon_code'];
                }
                if(!empty($userMyBag['intent_key'])){
                    $intent_key=$userMyBag['intent_key'];
                }


                $order_type=$userMyBag['order_type'];
                $total_item  += $userMyBag['quantity'];

                $total_menu_amount += $userMyBag['total_menu_amount'];
            }

            $delivery_data=Delivery::getDelivery('cart',$request,$locationId,$order_type,$total_item,$total_menu_amount,0);

            $data=[];
            if(!empty($delivery_data) && is_array($delivery_data)){

                $tax=$delivery_data['tax'];
                $service_tax=$delivery_data['service_tax'];
                $deliveryCharge=$delivery_data['delivery_charge'];
                $additional_charge=$delivery_data['additional_charge'];
                $additional_charge_name=$delivery_data['additional_charge_name'];
                $tipAmount=$delivery_data['tip_amount'];
                $flat_discount=$delivery_data['flat_discount'];

            }

            if(!empty($coupon_code)){
                $coupan_applied=Delivery::validateCoupan('cart',$request,$coupon_code,$userAuthId,$total_menu_amount);
                if(!empty($coupan_applied)&& count($coupan_applied) && isset($coupan_applied['promocode_discount'])){
                    $coupon_discount = $coupan_applied['promocode_discount'];

                }
            }
            $finalOrderAmount=(($total_menu_amount + $tax +$service_tax+ $tipAmount + $deliveryCharge+$additional_charge)-$flat_discount-$coupon_discount);

            if(empty($intent_key)){
                $intent_key=StripeGateway::createPaymentIntent($finalOrderAmount,$curCode);
                UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(['intent_key'=>$intent_key]);
            }else{

                $intent_key=StripeGateway::upatePaymentIntent($finalOrderAmount,$intent_key);
            }
            return $intent_key;
        }

    }

    public function getPaymentIntent(Request $request)
    {
        return  $this->getTotalPaymentIntent($request);

    }


}
