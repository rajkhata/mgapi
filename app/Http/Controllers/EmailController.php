<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Helpers\CommonFunctions;


class EmailController extends Controller {

    public function sendMail(Request $request) {
//        print_r(config('app.userAgent'));
//        die;
//        print_r(config('app.language'));
//        print_r(app('translator')->getLocale());
//        print_r(trans('messages.forgot_password_title'));
//        die;
        $langId = config('app.language')['id'];
        $userName = "sud";
        $userEmail= "sumca2004@gmail.com";
        
        $mailKeywords = array(
            'SITE_URL'  => 'http://www.hri.com',
            'LOGIN_URL' => 'http://www.hri.com/login',
            'USER_NAME' => $userName,
            'USER_EMAIL'=> $userEmail,
            'PASSWORD'=>"test3982",
            'SITE_NAME'=> "HRI",
            'MAIL_CONTENT'=>"Testing the dynamic mail template"
        );
        CommonFunctions::$langId = $langId;
        $mailTemplate = CommonFunctions::mailTemplate('registration',$mailKeywords);
        
        $mailData['subject'] = "Forgot Password";
        $mailData['body'] = $mailTemplate;
        $mailData['receiver_email'] = "sumca2004@gmail.com";
        $mailData['receiver_name'] = "sudhanshu";   
        
        $message = CommonFunctions::sendMail($mailData);
        return response()->json(['data' => $message]);
    }
    
    public function sendSms(Request $request){
        //print_r($_ENV);
        $smsData['user_mob_no'] = $request->user_mob_no;
        $smsData['message']=$request->message;
        $message = CommonFunctions::sendSms($smsData);
        return response()->json(['data' => $message]);
    }
    
    public function pushNetcore(Request $request){
       
        $currentDate = "2018-06-06 03:15";
        $restname = "test";
        $restid ="81";
        $host_name = "http://www.hri.com";
        $netcoreData =[       
            "user_id"=>$request->userId,
            "name"=>($request->first_name)?$request->first_name:"",
            "email"=>$request->email,
            "mobile"=>$request->mobile,
            "age"=>"",
            "city"=>$request->city
        ];
       
        $message = CommonFunctions::netcoreEvent($netcoreData);
        return response()->json(['data' => "success"]);
    }

}
