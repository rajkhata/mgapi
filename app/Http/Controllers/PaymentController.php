<?php
namespace App\Http\Controllers;
use App\Helpers\ApiPayment;
use App\Helpers\CommonFunctions;
use App\Helpers\Delivery;
use App\Helpers\DeliveryServiceProvider;
use App\Helpers\Payment\BraintressGateway;
use App\Helpers\Payment\IngenicoGateway;
use App\Helpers\Payment\BridgePayGateway;
use App\Helpers\Payment\StripeGateway;
use App\Helpers\Twinjet;
use App\Models\City;
use App\Models\FinancialRecord;
use App\Models\DeliveryService;
use App\Models\DeliveryServices;
use App\Models\DeliveryServicesLogs;
use App\Models\GiftcardCoupons;
use App\Models\MenuItems;
use App\Models\MybagOptions;
use App\Models\NewMenuItems;
use App\Models\OrderDetailOptions;
use App\Models\PaymentGateway;
use App\Models\PaymentGatewayLog;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\UserAuth;
use App\Models\UserCards;
use App\Models\UserMyBag;
use App\Models\UserOrder;
use App\Models\OrderStatuses;
use App\Models\UserOrderDetail;
use App\Models\UserPaymentToken;
use Excption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\UserAddresses;
use Illuminate\Support\Facades\Hash;
//use DB;
class PaymentController extends Controller
{
    public function showAllOrders($userId)
    {
        $ms = microtime(true);
        $paymentData = [];
        $me = microtime(true) - $ms;
        return response()->json(['data'=> json_decode($paymentData, true), 'error' => '', 'xtime' => $me]);
    }
    public function showOneOrder($userId, $id)
    {
        $ms = microtime(true);
        $error = '';
        $paymentData = null;
        $me = microtime(true) - $ms;
        return response()->json(['data'=> [$paymentData], 'error' => $error, 'xtime' => $me]);
    }
    public function create($userId, Request $request)
    {
        $ms           = microtime(true);
        $error        = '';
        $status       = 201;
        $restaurantId = $request->input('restaurant_id');
        $userMyBag    = UserMyBag::where(['user_id' => $userId, 'restaurant_id' => $restaurantId, 'status' => 0])->get();
        //return response()->json(gettype($userMyBag[0]['menu_json']));
        $i            = 0;
        $totalAmount  = 0;
        $totalQuant   = 0;
        $bypFlag      = false;
        // find the total quantity and total amount
        foreach ($userMyBag as $item) {
            if (isset($item['menu_json'])) {
                $userMyBag[$i]['menu_json'] = json_decode($item['menu_json'], true);
                $bypFlag                    = true;
            }
            $totalAmount += $item['quantity'] * $item['unit_price'];
            $totalQuant  += $item['quantity'];
            $i++;
        }
        // get user payment customer id
        $userInfo      = User::find($userId);
        $userPayCustId = $userInfo['stripe_customer_id'];
        // get user payment card id
        // static card id for now, later user will select any card for payment
        $cardId = 1;
        $userCardInfo  = UserCards::where(['user_id' => $userId, 'status' => 1, 'id' => $cardId])->first();
        $userPayCardId = $userCardInfo['stripe_card_id'];
        // Charge the user
        $data           = [
            'customer' => $userPayCustId,
            'card_id'  => $userPayCardId,
            'amount'   => $totalAmount * 100,
        ];
        $apiObj         = new ApiPayment();
        $chargeResponse = $apiObj->createCharge($data);
        // on success move data from MyBag to user_orders & user_order_details page
        if ($chargeResponse) {
            $paymentChargeId = $chargeResponse['id'];
            $tax             = !empty($request->input('tax')) ? (float)$request->input('tax') : 0.00;
            $orderAmount     = (float)$request->input('order_amount');
            $tipPercent      = (float)$request->input('tip_percent');
            $tipAmount       = ($orderAmount * $tipPercent) / 100;
            $deliveryCharge  = (float)$request->input('delivery_charge') ?? 0.00;
            $ordersData      = [
                'restaurant_id'            => $restaurantId,
                'user_id'                  => $userId,
                'user_ip'                  => $_SERVER['REMOTE_ADDR'],
                'lat'                      => $request->input('lat'),
                'lng'                      => $request->input('lng'),
                'phone'                    => $request->input('phone'),
                'order_type'               => $request->input('order_type') ?? 'delivery',
                'order_amount'             => $orderAmount,                                         #post
                'pay_via_card'             => $request->input('pay_via_card') ?? 'card',       #post
                'tax'                      => $tax,                                                 #post
                'tip_amount'               => $tipAmount,                                           #post
                'tip_percent'              => $tipPercent,                                          #post
                'delivery_charge'          => $deliveryCharge,                                      #post
                'total_amount'             => $orderAmount + $tax + $tipAmount + $deliveryCharge,   #post
                'delivery_user_address_id' => $request->input('delivery_user_address_id'),     #post
                'delivery_time'            => $request->input('delivery_time'),                #post
                'instructions'             => $request->input('instructions'),                 #post
                'status'                   => 'placed',                                             #post
                'card_id'                  => $cardId,                                              #post
                'stripe_charge_id'         => $paymentChargeId,                                     #post
                'cod'                      => $request->input('cod') ?? 0,                     #post
            ];
            $userOrder    = UserOrder::create($ordersData);
            $orderId      = $userOrder->id;
            $bagItemsArr  = [];
            $bagItemIdArr = [];
            //return response()->json($userMyBag);
            foreach ($userMyBag as $bgItem) {
                $orderDetailsData = [
                    'order_id'            => $orderId,
                    'menu_id'             => $bgItem->menu_id,
                    'menu_json'           => json_encode($bgItem->menu_json),
                    'special_instruction' => $bgItem->special_instruction,
                    'quantity'            => $bgItem->quantity,
                    'unit_price'          => $bgItem->unit_price,
                    'total_menu_amount'   => $bgItem->total_menu_amount,
                    'status'              => 1,
                ];
                $bagItemIdArr[] = $bgItem->id;
                array_push($bagItemsArr, $orderDetailsData);
                // Create order details from MyBag
                UserOrderDetail::create($orderDetailsData);
            }
            // Delete MyBag items
            UserMyBag::whereIn('id', $bagItemIdArr)->delete();
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => 'Unable to charge customer.', 'xtime' => $me], $status);
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $chargeResponse, 'error' => $error, 'xtime' => $me], $status);
    }
    public function delete($userId, $id)
    {
        $ms = microtime(true);
        //Restaurant::findOrFail($id)->delete();
        $me = microtime(true) - $ms;
        return response()->json(['data'=>'Deleted successfully', 'error' => '', 'xtime' => $me], 200);
    }
    public function update($userId, $id, Request $request)
    {
        $ms = microtime(true);
        $restaurant = [];//Restaurant::findOrFail($id);
        //$restaurant->update($request->all());
        $me = microtime(true) - $ms;
        return response()->json(['data'=>$restaurant, 'error' => '', 'xtime' => $me], 200);
    }
    /*private function createUser2(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'restaurant_id' => 'required|exists:restaurants,id',
            'fname'         => 'required|string|max:255',
            'lname'         => 'string|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|alpha_num|min:8',
        ]);
        if ($validation->fails())
        {
            return response()->json(['error' => $validation->errors()]);
        }
        else
        {
            $userArr = [
                'restaurant_id' => $request->input('restaurant_id'),
                'fname' => $request->input('fname'),
                'lname' => $request->input('lname'),
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ];
            $user = User::create($userArr);
            if(!empty($user))
                return $user->id;
            else
                return response()->json(['error' => 'Invalid Data']);
        }
    }*/
    private function createUser(Request $request) {
        $ms = microtime(true);
        //$langId = config('app.language')['id'];
        $localization_id = $request->header('X-localization');
        $language_id =  CommonFunctions::getLanguageInfo($localization_id);
        $langId = $language_id->id;
        // input data
        $deviceIpAdd = $request->getClientIp() ?? $request->ip();
        $platform = strtolower($request->input('platform')) ?? 'web';
        $deviceId = $request->input('device_id') ?? '';
        $deviceToken = $request->input('device_token') ?? '';
        $validation = Validator::make($request->all(), [
            'restaurant_id' => 'required|exists:restaurants,id',
            'fname'         => 'required|string|max:255',
            'lname'         => 'string|max:255',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|min:8|max:64',
        ]);
        if (!$validation->fails()) {
            // check guest token
            $userAuth = '';
            $authHeader = $request->header('Authorization');
            if (!empty($authHeader)) {
                $guestToken = explode(' ', $authHeader)[1];
                $userAuth = UserAuth::where(['guest_token' => $guestToken])->first();
            }
            // registration data
            $user = User::create([
                'restaurant_id' => $request->input('restaurant_id'),
                'fname' => $request->input('fname'),
                'lname' => $request->input('lname'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'status' => 1,
                'user_group_id' => 1,
                'language_id' => $langId
            ]);
            // STRIPE - create customer
            $data   = [
                'description' => ucwords($user->fname .' '. $user->lname),
                'email'       => $user->email,
                'source'      => 'tok_visa',
            ];
            $apiObj = new ApiPayment();
            $result = $apiObj->createCustomer($data);
            if($result) {
                $stripeCustomerId = $result->id;
                $user = User::find($user->id);
                $user->stripe_customer_id = $stripeCustomerId;
                $user->save();
            }
            // generate token for the user
            if ($token = $this->guard()->login($user)) {
                $result         = $this->respondWithToken($token);
                $userInfo       = $this->guard()->user();
                $result['user'] = $user;
                unset($result['expires_in']);
                $userAuth->access_token = $result['access_token'];
                $userAuth->user_id = $userInfo->id;
                $userAuth->ttl = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                $userAuth->save();
            }
            //send email
            CommonFunctions::$langId = $langId;
            $mailKeywords = array(
                'SITE_URL' => config('constants.site_url'),
                'LOGIN_URL' => config('constants.login_url'),
                'SITE_NAME' => config('constants.site_name'),
                'USER_NAME' => $request->input('fname'),
                'MAIL_CONTENT' => "Thanks"
            );
            $mailTemplate = CommonFunctions::mailTemplate('registration', $mailKeywords);
            $mailData['subject'] = trans('messages.greeting');
            $mailData['body'] = $mailTemplate;
            $mailData['receiver_email'] = $request->input('email');
            $mailData['receiver_name'] = $request->input('fname');
            CommonFunctions::sendMail($mailData);
            CommonFunctions::netcoreEvent(array(
                'email' => $request->input('email'),
                'first_name' => $request->input('fname'),
                'mobile' => '',
                'last_name' => $request->input('lname'),
                'is_register' => 'yes',
                'host_url' => ''
            ));
            $me = microtime(true) - $ms;
            return response()->json(['data' => $result, 'error' => null, 'xtime' => $me], 201);
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['error' => $validation->errors(), 'xtime' => $me], 400);
        }
    }
    /***************************SALTY GIFT CARD RELATED*************/
    public function giftCardGenerate($order_id){
        $orderDetail = new UserOrderDetail();
        $orderDetailData = $orderDetail->where('user_order_id', $order_id)->select('id','quantity', 'menu_json','item_other_info','unit_price')->get();;
        if($orderDetailData){
            $myselfbonus_amount=0;
            $myself_bonusdata=NULL;
            foreach($orderDetailData as $data){
                $qty=$data->quantity;
                $unit_price=$data->unit_price;
                $item_otherinfo = !empty($data->item_other_info) ? json_decode($data->item_other_info) : '';
                $recipientDetails = isset($item_otherinfo->recipient_details) ? $item_otherinfo->recipient_details : [];
                if ($item_otherinfo!='' && count($recipientDetails)) {
                    $recipient=$item_otherinfo->recipient;
                    $gift_type=$item_otherinfo->gift_type;
                    $bonus_for=$item_otherinfo->bonus_for;
                    $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                    $promotion_applicable=isset($item_otherinfo->promotion_applicable)?$item_otherinfo->promotion_applicable:0;
                    if($recipient=='single'){
                        $rec=$recipientDetails[0];
                        if(isset($qty) && $qty>0) {
                            for($j=0; $j<$qty; $j++) {
                                /*****************Genererate Normal Card***************/
                                $gift_fields = [
                                    'order_detail_id' =>$data->id,
                                    'gift_unique_code' => CommonFunctions::UniqueCoupancode(),
                                    'type' => $gift_type,
                                    'recipient' => $recipient,
                                    'amount' => $unit_price,
                                    'card_type' =>'normal',
                                    'bonus_for' => $bonus_for,
                                    'name' => $rec->name,
                                    'email' => $rec->email,
                                    'phone' => $rec->phone,
                                    'floor_building' => $rec->floor_building,
                                    'city' => $rec->city,
                                    'state' => $rec->state,
                                    'address' => $rec->address,
                                    'zip' => $rec->zip,
                                ];
                                GiftcardCoupons::create($gift_fields);
                                /*****************Genererate Normal Card End***************/
                                /*****************Genererate Bonus Card ***************/
                                if($promotion_applicable==1){
                                    $gift_fields['card_type']='bonus';
                                    if($bonus_for=='recipient'){
                                        $gift_fields['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                                        $gift_fields['amount']=$promotion_amount;
                                        GiftcardCoupons::create($gift_fields);
                                    }else if($bonus_for=='myself'){
                                        $myselfbonus_amount+=$promotion_amount;
                                        $gift_fields = [
                                            'order_detail_id' =>$data->id,
                                            'type' => $gift_type,
                                            'recipient' => $recipient,
                                            'card_type' =>'bonus',
                                            'bonus_for' => $bonus_for,
                                        ];
                                        $myself_bonusdata=$gift_fields;
                                    }
                                }
                                /*****************Genererate Bonus Card End***************/
                            }
                        }
                    }else{
                        if($qty==count($recipientDetails)){
                            foreach($recipientDetails as $rec) {
                                /*****************Genererate Normal Card***************/
                                $gift_fields = [
                                    'order_detail_id' =>$data->id,
                                    'gift_unique_code' => CommonFunctions::UniqueCoupancode(),
                                    'type' => $gift_type,
                                    'recipient' => $recipient,
                                    'amount' => $unit_price,
                                    'card_type' =>'normal',
                                    'bonus_for' => $bonus_for,
                                    'name' => $rec->name,
                                    'email' => $rec->email,
                                    'phone' => $rec->phone,
                                    'floor_building' => $rec->floor_building,
                                    'city' => $rec->city,
                                    'state' => $rec->state,
                                    'address' => $rec->address,
                                    'zip' => $rec->zip,
                                ];
                                GiftcardCoupons::create($gift_fields);
                                /*****************Genererate Normal Card End***************/
                                /*****************Genererate Bonus Card ***************/
                                if($promotion_applicable==1){
                                    $gift_fields['card_type']='bonus';
                                    if($bonus_for=='recipient'){
                                        $gift_fields['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                                        $gift_fields['amount']=$promotion_amount;
                                        GiftcardCoupons::create($gift_fields);
                                    }else if($bonus_for=='myself'){
                                        $myselfbonus_amount+=$promotion_amount;
                                        $gift_fields = [
                                            'order_detail_id' =>$data->id,
                                            'type' => $gift_type,
                                            'recipient' => $recipient,
                                            'card_type' =>'bonus',
                                            'bonus_for' => $bonus_for,
                                        ];
                                        $myself_bonusdata=$gift_fields;
                                    }
                                }
                                /*****************Genererate Bonus Card End***************/
                            }
                        }
                    }
                }
            }
            if( $myselfbonus_amount!=0 && !empty($myself_bonusdata)){
                /*****************Genererate Merge Single Card for Myself***************/
                $myself_bonusdata['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                $myself_bonusdata['amount']=$myselfbonus_amount;
                GiftcardCoupons::create($myself_bonusdata);
                /*****************Genererate Merge Single Card for Myself End***************/
            }
        }
    }
    public function giftCardGenerateNew($order_id){
        $orderDetail = new UserOrderDetail();
        $orderDetailData = $orderDetail->where('user_order_id', $order_id)->select('id','quantity', 'menu_json','item_other_info','unit_price')->get();;
        if($orderDetailData){
            $myselfbonus_amount=0;
            $myself_bonusdata=NULL;
            foreach($orderDetailData as $data){
                $qty=$data->quantity;
                $unit_price=$data->unit_price;
                $item_otherinfo = !empty($data->item_other_info) ? json_decode($data->item_other_info) : '';
                $recipientDetails = isset($item_otherinfo->recipient_details) ? $item_otherinfo->recipient_details : [];
                if ($item_otherinfo!='' && count($recipientDetails)) {
                    $recipient=$item_otherinfo->recipient;
                    $gift_type=$item_otherinfo->gift_type;
                    $bonus_for=$item_otherinfo->bonus_for;
                    $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                    $promotion_applicable=isset($item_otherinfo->promotion_applicable)?$item_otherinfo->promotion_applicable:0;
                    $promotion_applicable_on_amount=$qty*$unit_price;
                    if($recipient=='single'){
                        $rec=$recipientDetails[0];
                        if(isset($qty) && $qty>0) {
                            for($j=0; $j<$qty; $j++) {
                                /*****************Genererate Normal Card***************/
                                $gift_fields = [
                                    'order_detail_id' =>$data->id,
                                    'gift_unique_code' => CommonFunctions::UniqueCoupancode(),
                                    'type' => $gift_type,
                                    'recipient' => $recipient,
                                    'amount' => $unit_price,
                                    'card_type' =>'normal',
                                    'bonus_for' => $bonus_for,
                                    'name' => $rec->name,
                                    'email' => $rec->email,
                                    'phone' => $rec->phone,
                                    'floor_building' => $rec->floor_building,
                                    'city' => $rec->city,
                                    'state' => $rec->state,
                                    'address' => $rec->address,
                                    'zip' => $rec->zip,
                                ];
                                GiftcardCoupons::create($gift_fields);
                                /*****************Genererate Normal Card End***************/
                            }
                            /*****************Genererate Bonus Card ***************/
                            if($promotion_applicable==1){
                                $gift_fields['card_type']='bonus';
                                if($bonus_for=='recipient'){
                                    $promotion_amount=floor($promotion_applicable_on_amount/$promotion_amount)*$promotion_amount;
                                    $gift_fields['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                                    $gift_fields['amount']=$promotion_amount;
                                    GiftcardCoupons::create($gift_fields);
                                }else if($bonus_for=='myself'){
                                    $promotion_amount=floor($promotion_applicable_on_amount/$promotion_amount)*$promotion_amount;
                                    $myselfbonus_amount+=$promotion_amount;
                                    $gift_fields = [
                                        'order_detail_id' =>$data->id,
                                        'type' => $gift_type,
                                        'recipient' => $recipient,
                                        'card_type' =>'bonus',
                                        'bonus_for' => $bonus_for,
                                    ];
                                    $myself_bonusdata=$gift_fields;
                                }
                            }
                            /*****************Genererate Bonus Card End***************/
                        }
                    }else{
                        if($qty==count($recipientDetails)){
                            foreach($recipientDetails as $rec) {
                                /*****************Genererate Normal Card***************/
                                $gift_fields = [
                                    'order_detail_id' =>$data->id,
                                    'gift_unique_code' => CommonFunctions::UniqueCoupancode(),
                                    'type' => $gift_type,
                                    'recipient' => $recipient,
                                    'amount' => $unit_price,
                                    'card_type' =>'normal',
                                    'bonus_for' => $bonus_for,
                                    'name' => $rec->name,
                                    'email' => $rec->email,
                                    'phone' => $rec->phone,
                                    'floor_building' => $rec->floor_building,
                                    'city' => $rec->city,
                                    'state' => $rec->state,
                                    'address' => $rec->address,
                                    'zip' => $rec->zip,
                                ];
                                GiftcardCoupons::create($gift_fields);
                                /*****************Genererate Normal Card End***************/
                                /*****************Genererate Bonus Card ***************/
                                if($promotion_applicable==1){
                                    $gift_fields['card_type']='bonus';
                                    if($bonus_for=='recipient'){
                                        $promotion_amount=floor($promotion_applicable_on_amount/$promotion_amount)*$promotion_amount;
                                        $gift_fields['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                                        $gift_fields['amount']=$promotion_amount;
                                        GiftcardCoupons::create($gift_fields);
                                    }
                                }
                                /*****************Genererate Bonus Card End***************/
                            }
                            /*****************Genererate Bonus Card ***************/
                            if($promotion_applicable==1){
                                $gift_fields['card_type']='bonus';
                                if($bonus_for=='myself'){
                                    $promotion_amount=floor($promotion_applicable_on_amount/$promotion_amount)*$promotion_amount;
                                    $myselfbonus_amount+=$promotion_amount;
                                    $gift_fields = [
                                        'order_detail_id' =>$data->id,
                                        'type' => $gift_type,
                                        'recipient' => $recipient,
                                        'card_type' =>'bonus',
                                        'bonus_for' => $bonus_for,
                                    ];
                                    $myself_bonusdata=$gift_fields;
                                }
                            }
                            /*****************Genererate Bonus Card End***************/
                        }
                    }
                }
            }
            if( $myselfbonus_amount!=0 && !empty($myself_bonusdata)){
                /*****************Genererate Merge Single Card for Myself***************/
                $myself_bonusdata['gift_unique_code']=CommonFunctions::UniqueCoupancode();
                $myself_bonusdata['amount']=$myselfbonus_amount;
                GiftcardCoupons::create($myself_bonusdata);
                /*****************Genererate Merge Single Card for Myself End***************/
            }
        }
    }
    /***************************SALTY GIFT CARD RELATED End*************/
    public function placeAOrder(Request $request)
    {
        #get HTML For Order type
        // $html  = CommonFunctions::getProductList(2290);
        // echo $html;die;
        $userData = json_decode($request->input('user_info'), true);
        $userAddress = json_decode($request->input('address'), true);
        $product_type=$request->has('product_type')?$request->input('product_type'):'food_item'; //default food_item
        $userMyBag = array();
        $userPayCustId = "";
        $stripeCardId = "";
        $ms = microtime(true);
        $userMarkedNewFlag = false; // RESERVATION MODULE
        $bearerToken = $request->bearerToken();
        CommonFunctions::$langId = $langId = config('app.language')['id'];
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if($userAuth) {
            $locationId   = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','support_from'));
            $userId       = $userAuth->user_id;
            $userAuthId   = $userAuth->id;
            $email = $userData['email'];
            $fname = $userData['fname'];
            $lname = $userData['lname'];
            $phone = $userData['phone'];
            $address1 = $userAddress['address1'];
            $address2 = $userAddress['address2'];
            $city = $userAddress['city'];
            $zipcode = $userAddress['zipcode'];
            $lat = $userAddress['lat'];
            $lng = $userAddress['lng'];
            if ($bearerToken) {
                $userId       = $userAuth->user_id;
                $userInfo      = User::find($userId);
                $userPayCustId = $userInfo['stripe_customer_id'];
                $is_guest = 0;
                //$userMyBag = UserMyBag::where(['user_id' => $userId, 'restaurant_id' => $restaurantId, 'location_id'=>$locationId, 'status' => 0])->get();
                //$userMyBag = UserMyBag::where(['user_id' => $userId, 'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
                # Commented above on 23-07-2018 Because When Guest User signin at checkout screen then cart not found
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId,'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
            } else {
                // GUEST
                $is_guest = 1;
                //$address_id = $userAddress['address_id'];
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId, 'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
                $email_id = $email;
                $mobile_no = $phone;
                $userObj = User::where('restaurant_id', $restaurantId)
                    ->where(function ($query) use ($email_id,$mobile_no){
                        $query->where('email',$email_id)
                            ->orWhere('mobile', $mobile_no);
                    })->first();
                if(empty($userObj)) {
                    #PE-2341 AS discussed we will not register the user RG 23-11-2018 commented
                    // $userData['restaurant_id'] = $restaurantId;
                    // $userHlprObj = new UserFunctions();
                    // $userObj = $userHlprObj->userRegister($request, $userData);
                    // $userMarkedNewFlag = true; // RESERVATION MODULE
                    // if(empty($userObj['error']) && isset($userObj['data']['user']['id'])) {
                    //     $userId     = $userObj['data']['user']['id'];
                    //     ################ mail #######
                    //     $restaurant_data = Restaurant::where('id', $locationId)->select('id', 'email', 'restaurant_name', 'source_url')->first();
                    //     $mailKeywords               = array(
                    //         'SITE_URL'  => $restaurant_data->source_url,
                    //         'LOGIN_URL' => $restaurant_data->source_url.'login',
                    //         'SITE_NAME' => $restInfo['restaurant_name'],
                    //         'REST_NAME' => strtolower($restaurant_data->restaurant_name),
                    //         'USER_NAME' => $fname,
                    //         'EMAIL'     => $email,
                    //         'PASSWORD'  => config('constants.user_default_password'),
                    //         'title'     => "Welcome to {$restInfo['restaurant_name']}",
                    //     );
                    //     $mailTemplate               = CommonFunctions::mailTemplate('guest_registration', $mailKeywords, 'header_layout', 'footer_layout', $restaurantId);
                    //     $mailData['subject']        = "Welcome to {$restInfo['restaurant_name']}";
                    //     $mailData['body']           = $mailTemplate;
                    //     $mailData['receiver_email'] = $email;
                    //     $mailData['receiver_name']  = $fname;
                    //     $mailData['MAIL_FROM_NAME'] =  $restInfo['restaurant_name'];
                    //     $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                    //     CommonFunctions::sendMail($mailData);
                    // }
                }else {
                    // $status = Config('constants.status_code.BAD_REQUEST');
                    // $me     = microtime(true) - $ms;
                    // $err_message = 'You are already registered. Please login to place an order.';
                    // if($mobile_no == $userObj->mobile && $email_id != $userObj->email) {
                    //     $err_message = "This phone number is associated with a registered user. Please login with your registered email ID to place an order.";
                    // }elseif($email_id == $userObj->email && $mobile_no != $userObj->mobile) {
                    //     $err_message = "This email ID is associated with a registered user. Please login with your registered email ID to place an order.";
                    // }elseif($mobile_no == $userObj->mobile && $email_id == $userObj->email) {
                    //     $err_message = "This phone number and email ID are associated with a registered user. Please login to place an order.";
                    // }
                    // return response()->json(['data' => [], 'error' => $err_message, 'xtime' => $me], $status);
                    $userId     = $userObj->id;
                    #commnted on 23-10-2018 As per Rahul Prabhakar Jira Bug: PE-2341
                }
            }
            $customerName  = $fname . ' ' . $lname;
            $customerEmail = $email;
            $cardInfo = $request->input('card_info');
            $cardInfoData  = array();
            $cardInfoData = json_decode(base64_decode($cardInfo));
            //$userMyBag = UserMyBag::where(['user_id' => $userId, 'restaurant_id' => $restaurantId, 'status' => 0])->get();
            // Restaurant Currency symbol
            // Restaurant currency symbol
            $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
            if($restData) {
                $curSymbol = $restData->currency_symbol;
                $curCode = $restData->currency_code;
            } else {
                $curSymbol = config('constants.currency');
                $curCode = config('constants.currency_code');
            }
            $i           = 0;
            $orderAmount = 0;
            $totalQuant  = 0;
            $orderId     = 0;
            $bypFlag     = false;
            // find the total quantity and total amount
            if ($userMyBag->toArray()) {
                /* Expired OR time */
                $orderType = $request->input('order_type') ?? 'delivery';
                $time = new TimeslotController;
                if($orderType == 'delivery') {
                    // Get delivery slot
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'delivery',  $request->input('delivery_date'));
                }else {
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'carryout', $request->input('delivery_date'));
                }
                $selected_time =  $request->input('delivery_time');
                //if(!$request->has('product_type')) {
                if($product_type=='food_item') {
                    if($get_time_slot['status']) {
                        if($get_time_slot['slot']) {
                            $all_timeslots = array_column($get_time_slot['slot'], 'key');
                            if(!in_array($selected_time, $all_timeslots)) {
                                $status = Config('constants.status_code.BAD_REQUEST');
                                $me     = microtime(true) - $ms;
                                return response()->json(['data' => [], 'error' => 'Timeslot you has chosen has been expired.', 'xtime' => $me], $status);
                            }
                        }else {
                            $status = Config('constants.status_code.BAD_REQUEST');
                            $me     = microtime(true) - $ms;
                            return response()->json(['data' => [], 'error' => 'Invalid timeslot OR time has expired.', 'xtime' => $me], $status);
                        }
                    }else {
                        $status = Config('constants.status_code.BAD_REQUEST');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [], 'error' => $get_time_slot['error'], 'xtime' => $me], $status);
                    }
                }
                /* Expired OR time */
                $restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                $cityID = $restaurantData->city_id;
                $taxPercent=0;
                if($cityID!="" && $restaurantData->city_tax)
                {
                    $salesTax = City::where('id', $cityID)->first(['sales_tax']);
                    if($salesTax){
                        $taxPercent = $salesTax->sales_tax;
                    }
                }else {
                    $taxPercent = $restaurantData->tax;
                }
                $is_pot=0;
                foreach ($userMyBag as $item) {
                    $customization_price = 0;
                    $gift_wrapping_fees=0;
                    if($is_pot==0 && $item['is_pot']==1) {
                        $is_pot=1;
                    }
                    if($item['is_byp']) {
                        $bypData     = json_decode($item['menu_json'], true);
                        $userMyBag[$i]['item'] = $bypData['label'];
                        $userMyBag[$i]['image'] = $item['image'];
                    } elseif($item['menu_id']>0) {
                        $menuItem = NewMenuItems::where('id', $item['menu_id'])->first();
                        $userMyBag[$i]['item'] = $menuItem->name;
                        $userMyBag[$i]['product_type'] = $menuItem->product_type;
                        $dataImg = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);
                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                /*
                                if($menuItem->images) {
                                    $images_all = json_decode($menuItem->images, true);
                                    if($images_all && isset($images_all['desktop_web_images'])) {
                                        $images_all = json_decode($menuItem->images, true)['desktop_web_images'];
                                        */
                                $dataImg[0] = array("image"=> $images_all['desktop_web_images'][0]['web_image'], "thumb_image_med"=>$images_all['desktop_web_images'][0]['web_thumb_med']);
                            }
                        }
                        $userMyBag[$i]['image'] = json_encode($dataImg);
                        //@10-09-2018 by RG Calculate Addons-price in case of Normal Customisation Items
                        //  if(isset($item['is_byp']) && $item['is_byp'] == 0 && $item['menu_json'] && (!empty($item['menu_json']) &&  !is_null($item['menu_json']) && $item['menu_json']!='null' && $item['menu_json']!=null && $item['menu_json']!='NULL' &&  $item['menu_json']!=NULL) && $menuItem->product_type!='gift_card') {
                        //     $menuJsonDecoded = json_decode($item['menu_json'], true);
                        //     foreach ($menuJsonDecoded as $premenu) {
                        //         if($premenu['items']) {
                        //             foreach ($premenu['items'] as $cust_menu) {
                        //                if($cust_menu['price'] && $cust_menu['default_selected']) {
                        //                     $customization_price+= $cust_menu['price'] * $item['quantity'];
                        //                 }
                        //             }
                        //         }
                        //     }
                        // }
                    }
                    if (isset($item['menu_json'])) {
                        // TO REMOVE JSON DECODE AND ENCODE FROM HERE
                        $userMyBag[$i]['menu_json'] = json_decode($item['menu_json'], true);
                        $bypFlag = true;
                    }
                    if(isset($menuItem) && $menuItem->product_type=='gift_card' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        //$gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $gift_wrapping_fees=0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else if(isset($menuItem) && $menuItem->product_type=='product' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else{
                        // $orderAmount += ($item['quantity'] * $item['unit_price']) + $customization_price;
                        $orderAmount +=$item['total_menu_amount'];
                    }
                    $totalQuant  += $item['quantity'];
                    $i++;
                }
                $tipAmount      = 0;
                $tax            = 0;
                //$tax          = ($orderAmount * config('constants.tax')) / 100;
                $tax            = ($orderAmount * $taxPercent)/100;
                $tipPercent     = (float)$request->input('tip_percent');
                $tip_by_cash    = $request->input('tip_by_cash');
                if($tip_by_cash==0)
                    $tipAmount      = ($orderAmount * $tipPercent) / 100;
                if($orderType == 'delivery' && $orderAmount <= $restaurantData->free_delivery) {
                    $deliveryCharge = (float)$request->input('delivery_charge') ?? 0.00;
                }elseif($orderType == 'delivery' && $orderAmount > $restaurantData->free_delivery) {
                    $deliveryCharge =  0.00;
                }else{
                    $deliveryCharge = (float)$request->input('delivery_charge') ?? 0.00;
                }
                $delivery_date =  $request->input('delivery_date');
                $is_asap_order = 0;$order_state =2;
                if($request->has('is_asap') && $request->input('is_asap') && $request->has('asap_gap') && $request->input('asap_gap')) {
                    //get restaurant_current_time @16-08-2018 By RG
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                    if(is_object($currentDateTimeObj)) {
                        $selected_time = $currentDateTimeObj->format('H:i');
                    }else {
                        $selected_time = date('H:i');
                    }
                    $delivery_datetime = strtotime($delivery_date. ' '.$selected_time) + $request->input('asap_gap');
                    $delivery_date = date('Y-m-d', $delivery_datetime);
                    $selected_time = date('H:i',  $delivery_datetime);
                    $is_asap_order = 1;$order_state =1;
                }
                $ordersData     = [
                    'parent_restaurant_id'   => $restaurantId,
                    'is_guest'        => $is_guest,
                    //'location_id'     => $locationId,
                    'restaurant_id'     => $locationId,
                    'is_pot'           =>$is_pot,
                    'user_id'         => $userId,
                    'user_auth_id'    => $userAuthId,
                    'user_ip'         => $_SERVER['REMOTE_ADDR'],
                    'fname'           => $fname,
                    'lname'           => $lname,
                    'email'           => $email,
                    'address'         => $address1,
                    'address2'        => $address2,
                    'city'            => $city,
                    'zipcode'         => $zipcode,
                    'latitude'        => $lat,
                    'longitude'       => $lng,
                    'phone'           => $phone,
                    'order_type'      => $request->input('order_type') ?? 'delivery',
                    'order_amount'    => $orderAmount,                                         #post
                    'tax'             => $tax,                                                 #post
                    'tip_amount'      => $tipAmount,                                           #post
                    'tip_percent'     => $tipPercent,                                          #post
                    'delivery_charge' => $deliveryCharge,                                      #post
                    'total_amount'    => $orderAmount + $tax + $tipAmount + $deliveryCharge,   #post
                    'delivery_time'   => $selected_time,                #post
                    'delivery_date'   => $delivery_date,
                    'user_comments'   => $request->input('instructions'),                 #post
                    'status'          => 'pending',
                    'order_state'  =>  $order_state,
                    'product_type'   =>  $product_type,
                    'is_asap_order'   =>  $is_asap_order                                      #post
                ];
                //print_r($ordersData); die;
                #RG By transaction ID @24-07-2018
                $randStr = md5(microtime());
                $txn_id = strtoupper(substr($randStr, -10));// last 10 Digit
                $userOrder = UserOrder::create($ordersData);
                $orderId   = $userOrder->id;
                if ($orderId > 0) {
                    $bagItemsArr  = [];
                    $bagItemIdArr = [];
                    $addonsData = [];
                    $orderDetailsData = [];
                    foreach ($userMyBag as $bgItem) {
                        $is_pot_item=$bgItem->is_pot;
                        $orderDetailsData = array();
                        $gift_wrapping_feesjson = NULL;
                        $menuItem = NewMenuItems::where('id', $bgItem->menu_id)->first();
                        if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' ) && !empty($bgItem->item_other_info)) {
                            $gift_wrapping_feesjson=json_decode($bgItem->item_other_info,true);
                            if($menuItem->product_type=='product'){
                                $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?$menuItem->gift_wrapping_fees :0;
                                $gift_wrapping_feesjson['gift_wrapping_fees']=$gift_wrapping_fees;
                            }
                            /*
                            if($menuItem->product_type=='gift_card'){
                                $promotion_amount=null;
                                $promotion_applicable=0;
                                $promo=CommonFunctions::productPromotions($restaurantId);
                                if(!empty($promo) && $promo->amount_or_percent==0){
                                    $promo_implement=CommonFunctions::productPromoCondition($bgItem->unit_price,$promo->condition_amount,$promo->condition);
                                    if($promo_implement){
                                        $promotion_amount=$promo->discount;
                                    }
                                }
                                $gift_wrapping_feesjson['promotion_amount']=$promotion_amount;
                            }
                            */
                            $gift_wrapping_feesjson=json_encode( $gift_wrapping_feesjson);
                        }
                        $orderDetailsData = [
                            'user_order_id'       => $orderId,
                            'item'                => $bgItem->item,
                            'item_size'           => $bgItem->size,
                            'menu_id'             => $bgItem->menu_id,
                            'is_byp'              => $bgItem->is_byp,
                            'is_pot'              => $bgItem->is_pot,
                            'menu_json'           => json_encode($bgItem->menu_json),
                            'image'               => $bgItem->image,
                            'special_instruction' => $bgItem->special_instruction,
                            'quantity'            => $bgItem->quantity,
                            'unit_price'          => $bgItem->unit_price,
                            'total_item_amt'      => $bgItem->total_menu_amount,
                            'item_other_info'     => $gift_wrapping_feesjson,
                            'status'              => 1,
                        ];
                        $bagItemIdArr[] = $bgItem->id;
                        //array_push($bagItemsArr, $orderDetailsData);
                        // Create order details from MyBag
                        $UserOrderDetail = UserOrderDetail::create($orderDetailsData);
                        $user_order_detail_id = $UserOrderDetail->id;
                        $addons_data = [];
                        if($is_pot_item) {
                            $addon_options= MybagOptions::where('bag_id',$bgItem->id)->get();
                            if( $addon_options){
                                foreach ($addon_options as $addon_menu) {
                                    $mybag_option_data=[
                                        'option_name'=>$addon_menu->option_name,
                                        'option_id'=>$addon_menu->option_id,
                                        'price'=>$addon_menu->price,
                                        'quantity'=>$addon_menu->quantity,
                                        'user_order_detail_id'=>$user_order_detail_id,
                                        'total_amount'=>$addon_menu->total_amount,
                                    ];
                                    $addonsData[$bgItem->id] = $mybag_option_data;
                                    OrderDetailOptions::create($mybag_option_data);
                                }
                            }
                        }else if (!is_null($bgItem->menu_json)) {
                            /*Addons code*/
                            $addons_data =  CommonFunctions::changeMyBagJsonData($bgItem->is_byp, $bgItem->menu_json);
                            $addonsData[$bgItem->id] = $addons_data;
                        }
                        /*End code*/
                        //$addonsData[$bgItem->id] = $addons_data;
                    }
                    $tot_amount = $orderAmount + $tax + $tipAmount + $deliveryCharge;
                    $tot_amount = number_format($tot_amount, 2, '.', '') * 100 ;
                    #$tot_amount = (integer)($tot_amount * 100);
                    $transaction_id  = $cardNum = $cardType = "";
                    //$cardNumber = str_replace(' ', '', $cardInfoData->card);
                    if ($userPayCustId == null) {
                        $data = [
                            'description' => $customerName,
                            'email'       => $customerEmail,
                            //'source'      => 'tok_visa',
                        ];
                        $apiObj        = new ApiPayment();
                        $result        = $apiObj->createCustomer($data);
                        $userPayCustId = $result->id;
                        $cardData = [
                            'number'    => str_replace(' ', '', $cardInfoData->card),
                            'exp_month' => $cardInfoData->expiry_month,
                            'exp_year'  => $cardInfoData->expiry_year,
                            'cvc'       => $cardInfoData->cvv,
                            'name'      => $cardInfoData->name,
                        ];
                        //print_r($cardData); die;
                        $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);
                        if(isset($result2['errorMesg']))
                        {
                            $me = microtime(true) - $ms;
                            return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                        }
                        else {
                            //$stripeCardId = $result2->sources->data[0]->id;
                            $stripeCardId = $result2['id'];
                        }
                        //$stripeCardId = $result2->sources->data[0]->id;
                        $data = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'OrderID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        //print_r($data); die;
                        $chargeResponse  = $apiObj->createCharge($data);
                        $transaction_id  = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                    } else {
                        if(isset($cardInfoData->card_id)==null)
                        {
                            $cardNumber = str_replace(' ', '', $cardInfoData->card);
                            $cardData = [
                                'number'    => $cardNumber,
                                'exp_month' => $cardInfoData->expiry_month,
                                'exp_year'  => $cardInfoData->expiry_year,
                                'cvc'       => $cardInfoData->cvv,
                                'name'      => $cardInfoData->name,
                            ];
                            $apiObj        = new ApiPayment();
                            $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);
                            if(isset($result2['errorMesg']))
                            {
                                $me = microtime(true) - $ms;
                                return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                            }
                            else {
                                $stripeCardId = $result2['id'];
                            }
                            $userInfo = User::where('id', $userId)->first();
                            $userCard = "";
                            $userCard = $userInfo->card()->where('last_four', substr($cardNumber, -4))->where('user_id', $userId)->first();
                            if($cardInfoData->isSave && !$userCard) {
                                $data = [
                                    'user_id'        => $userId,
                                    'name'           => $cardInfoData->name,
                                    //'card'         => $cardNumber,
                                    'last_four'      => substr($cardNumber, -4),
                                    'type'           => $cardInfoData->type,
                                    //'expiry'         => $cardInfoData->expiry_month . '/' . $cardInfoData->expiry_year,
                                    'status'         => 1,
                                    'stripe_card_id' => $stripeCardId,
                                ];
                                UserCards::create($data);
                            }
                        }
                        else{
                            $stripeCardId         = $cardInfoData->card_id;
                            //$userCardInfo   = UserCards::where(['user_id' => $userId, 'status' => 1, 'id' => $cardID])->first();
                            //$userPayCardId = $userCardInfo['stripe_card_id'];
                        }
                        // Charge the user
                        $data           = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'ReceiptID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        $apiObj         = new ApiPayment();
                        $chargeResponse = $apiObj->createCharge($data);
                        $transaction_id = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                    }
                    $userOrder                   = UserOrder::find($userOrder->id);
                    $userOrder->status           = 'placed';
                    $userOrder->stripe_charge_id = $transaction_id;
                    $userOrder->stripe_card_id   = $stripeCardId;
                    $userOrder->card_number = 'XXXX-XXXX-XXXX-'.$cardNum;
                    $userOrder->card_type = $cardType;
                    $userOrder->payment_receipt = $txn_id;
                    $userOrder->order_state  =  $order_state;
                    $userOrder->restaurant_name = $restaurantData->restaurant_name;
                    if ($userOrder->save()) {
                        if($product_type=='gift_card') {
                            $this->giftCardGenerate($orderId);
                        }
                        // Replace stripe txn ID by Custom txn id
                        $userOrder->stripe_charge_id = $txn_id;
                        /*if($is_pot){
                            MybagOptions::whereIn('bag_id', $bagItemIdArr)->delete();
                        }
                        UserMyBag::whereIn('id', $bagItemIdArr)->delete();*/
                        //$langId = config('app.language')['id'];
                        //CommonFunctions::$langId = $langId;
                        $localization_id = $request->header('X-localization');
                        $language_id =  CommonFunctions::getLanguageInfo($localization_id);
                        CommonFunctions::$langId = $langId = $language_id->id;
                        //$restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                        $orderItem = $label = "";
                        $productType = "no";
                        if($product_type !='gift_card') {
                            foreach ($userMyBag as $bgItem){
                                $label = "";
                                $addon_modifier=CommonFunctions::getAddonsAndModifiers($bgItem,'bag');
                                $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                $instruct='';
                                $orderItem .=  '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>    
                                <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <b>'.$bgItem->quantity.' '.$bgItem->item.'</b>';
                                $result = isset($addonsData[$bgItem->id]) ? $addonsData[$bgItem->id] : null;
                                $menuItem = NewMenuItems::where('id', $bgItem->menu_id)->first();
                                if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' )) {
                                    $productType = "yes";
                                    if(!is_null( $bgItem->item_other_info)) {
                                        /*if (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) {
                                            $label = $bgItem->size . ' ';
                                        } else {
                                            $label = "";
                                        }*/
                                        $item_other_info = json_decode($bgItem->item_other_info);
                                        $value3 = '';
                                        $value = (isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?'Yes':'No';
                                        //$value2 = 'Gift Wrapping: ' . $value;
                                        $value2 =($menuItem->product_type=='product' )?'Gift Wrapping: ' . $value:'';
                                        if(isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message!='null')
                                        {
                                            $value3 = 'Message: ' .$item_other_info->gift_wrapping_message;
                                        }
                                        $gift_wrapping_fees=(isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?$menuItem->gift_wrapping_fees :0;
                                        if($gift_wrapping_fees!=0) {
                                            $label .= $value2 . '<br/>' . $value3.'<br/>Price: $'.$bgItem->quantity * $gift_wrapping_fees;
                                        }
                                        else {
                                            $label .= $value2 . '<br/>' . $value3;
                                        }
                                        $label.= (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$bgItem->size.'</td></tr>' .' ' : '';
                                        $label .= $addon_modifier_html;
                                    }
                                } else {
                                    //$label = (strtolower($bgItem->size) != 'na' || $bgItem->size != 'full') ? $bgItem->size .' ' : '';
                                    /*if(strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size))
                                    {
                                        $label = $bgItem->size.' ';
                                    }
                                    else
                                    {
                                        $label = "";
                                    }*/
                                    if($is_pot) {
                                    }else if($bgItem->is_byp==1 && $result){
                                        /*foreach($result as $key=>$data){
                                            if($key!="")
                                                $label .= $key.', ';
                                            if($data!="")
                                                $label .= '['.$data.']>';
                                        }*/
                                    }else if($bgItem->is_byp==0 && $result) {
                                        /*foreach ($result as $data) {
                                            if($data['addons_name']!=""){
                                                $label .= $data['addons_name'].', ';
                                            }
                                            if($data['addons_option']!="")
                                                $label .= '[' . $data['addons_option'] . ']>';
                                        }*/
                                    }
                                    $instruct = '';
                                    if(!empty($bgItem->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$bgItem->special_instruction.'</p>';
                                    }
                                    $label.= (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$bgItem->size.'</td></tr>' .' ' : '';
                                    $label .= $addon_modifier_html;
                                }
                                $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.' </span>'.$bgItem->total_menu_amount.'</p></th></tr></table></th></tr></tbody></table>
                                <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                            }
                        }
                        /*else {
                            $orderItem = CommonFunctions::getProductList($userOrder->id);
                        }*/
                        $timestamp = strtotime($userOrder->delivery_date. ' '.$userOrder->delivery_time.':00');
                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => $userOrder->created_at)));
                        $address = $userOrder->address;
                        if ($userOrder->address2) {
                            $address = $address . ' ' . $userOrder->address2;
                        }
                        $address = $address . ' ' . $userOrder->city . ' ' . $userOrder->state_code . ' ' . $userOrder->zipcode;
                        $cardInfo = "<span style='color: #000;'>".$userOrder->card_type."</span>".' ('.$userOrder->card_number.')';
                        if($is_asap_order) {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp). '(Possibly sooner!)';
                        }else {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp);
                        }
                        // RESERVATION MODULE
                        // if user category hasn't been marked yet, means REPEAT user
                        $user_update = User::where('restaurant_id', $restaurantId)->where('id', $userId)->where('category', config('constants.guest_category.New'))->first();
                        if ($user_update && !$userMarkedNewFlag) {
                            $user_update->category = config('constants.guest_category.Repeat');
                            $user_update->save();
                        }
                        $mailKeywords = array(
                            'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                            'URL' => $restaurantData->source_url,
                            'ORDER_NUMBER' => $userOrder->payment_receipt,
                            'USER_NAME' => ucfirst($customerName),
                            'USER_EMAIL' => $userOrder->email,
                            'ORDER_TYPE' => ucfirst($userOrder->order_type),
                            'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                            'ORDER_DELIVERY_TIME' => $odr_delivery_time,
                            'DELIVERY_ADD' => $address,
                            'PHONE' => $userOrder->phone,
                            'SUB_TOTAL' => $curSymbol.$userOrder->order_amount,
                            'TOT_AMOUNT' => $curSymbol.$userOrder->total_amount,
                            'ITEM_DETAIL' => $orderItem,
                            'SITE_URL' => $restaurantData->source_url,
                            'ADMIN_URL' => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER' => $cardInfo,
                            'title' => "Order Place"
                        );
                        if($userOrder->tax==0.00){
                            $mailKeywords['TAX'] = '';
                            $mailKeywords['TAX_TITLE'] = '';
                            $mailKeywords['DISPLAY'] = 'none';
                        } else{
                            $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$userOrder->tax;
                            $mailKeywords['TAX_TITLE'] = "Taxes";
                            $mailKeywords['DISPLAY'] = 'table';
                        }
                        if($userOrder->user_comments!=""){
                            $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions</b> <br><i>'.$userOrder->user_comments.'</i><br><br>';
                        }
                        else {
                            $mailKeywords['SPECIAL_INFO'] = '';
                        }
                        if($userOrder->tip_amount==0.00){
                            $mailKeywords['TIP_AMOUNT'] = '';
                            $mailKeywords['TIP_TITLE'] = '';
                            $mailKeywords['DISPLAY_TIP'] = 'none';
                        }
                        else{
                            //$mailKeywords['TIP_AMOUNT'] = $userOrder->tip_percent.'%'.$curSymbol. $userOrder->tip_amount;
                            $mailKeywords['TIP_AMOUNT'] = '<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
<p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$userOrder->tip_percent.'<span>%</span></p>
                            </th></tr></table></th>
                            <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$userOrder->tip_amount.'</p></th></tr></table></th>
                        </tr></tbody></table>';
                            $mailKeywords['DISPLAY_TIP'] = 'table';
                            $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                        }
                        if($userOrder->order_type=="delivery") {
                            $mailKeywords['DELIVERY_CHARGE'] = $curSymbol.$userOrder->delivery_charge;
                            if($product_type == "product"){
                                $mailTemplate = 'place_order_product_delivery';
                            }elseif($product_type == "food_item"){
                                $mailTemplate = 'place_order_delivery';
                            }elseif($product_type == "gift_card"){
                                $mailTemplate = 'giftcard_order_placed_restaurant';
                            }
                        } else {
                            if($productType == "yes")
                                $mailTemplate = 'place_order_product_carryout';
                            elseif($product_type == "food_item")
                                $mailTemplate = 'place_order_carryout';
                            else
                                $mailTemplate = 'place_order_carryout';
                        }
                        $header = "header_layout_restaurant";
                        $footer = "footer_layout_restaurant";
                        if($product_type == "gift_card") {
                            $mailData['subject'] = "Someone Bought $".intval($userOrder->total_amount)." Worth  of  Gift  Cards!";
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'manager');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                        }else {
                            $mailData['subject'] = "You've Got a New ".ucfirst($userOrder->order_type)." Order from Munch Ado!";
                        }
                        $mailTemplate = CommonFunctions::mailTemplate($mailTemplate, $mailKeywords, $header, $footer, $restaurantId);
                        $mailData['body'] = $mailTemplate;
                        //$mailData['receiver_email'] = $customerEmail;
                        $restaurantEmail = $restaurantData->email;
                        if($restaurantData->email=="")
                            $restaurantEmail = "sksingh1@bravvura.in";
                        $mailData['receiver_email'] = $restaurantEmail;
                        //$mailData['receiver_name'] = $customerName;
                        $mailData['receiver_name'] = $restaurantData->restaurant_name;
                        $mailData['MAIL_FROM_NAME'] = 'Munch Ado';
                        $mailData['MAIL_FROM'] = $restInfo['support_from'];
                        CommonFunctions::sendMail($mailData);
                        $userOrder['restaurant_name'] = $restaurantData->restaurant_name;
                        $me     = microtime(true) - $ms;
                        $status = 200;
                        #send mail to Buyers whi placed an Gift card ORders
                        if($product_type == "gift_card" && $userOrder->email) {
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'buyer');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                            $mailKeywords['BUYER_NAME'] = $userOrder->fname;
                            $mailTemplate = CommonFunctions::mailTemplate('giftcard_order_placed_buyer', $mailKeywords, 'header_layout', 'footer_layout', $restaurantId);
                            $mailData['subject'] = "Thank you for your Gift Card Order!";
                            $mailData['body'] = $mailTemplate;
                            $mailData['receiver_email'] = $userOrder->email;
                            #$mailData['receiver_email'] = 'rgupta@bravvura.in';
                            $mailData['MAIL_FROM_NAME'] = $restaurantData->restaurant_name;
                            $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                            CommonFunctions::sendMail($mailData);
                        }
                        /******** SMS Send **** @18-10-2018 by RG *****/
                        $sms_keywords = array(
                            'order_id' => $userOrder->id,
                            'sms_module' => 'order',
                            'action' => 'placed',
                            'sms_to' => 'customer'
                        );
                        CommonFunctions::getAndSendSms($sms_keywords);
                        /************ END SMS *************/
                        if($is_pot){
                            MybagOptions::whereIn('bag_id', $bagItemIdArr)->delete();
                        }
                        UserMyBag::whereIn('id', $bagItemIdArr)->delete();
                        return response()->json(['data' => $userOrder, 'error' => '', 'xtime' => $me], $status);
                    } else {
                        $status = Config('constants.status_code.INTERNAL_ERROR');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [], 'error' => 'Order status could not be updated', 'xtime' => $me], $status);
                    }
                } else {
                    $status = Config('constants.status_code.INTERNAL_ERROR');
                    $me     = microtime(true) - $ms;
                    return response()->json(['data' => [], 'error' => 'Order not generated in DB.', 'xtime' => $me], $status);
                }
            } else {
                $me = microtime(true) - $ms;
                $status =Config('constants.status_code.BAD_REQUEST');
                return response()->json(['data' => [], 'error' => 'Sorry, Invalid Request OR there is no items in your Bag.', 'xtime' => $me], $status);
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField.' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    public function placeOrder(Request $request)
    {
        #get HTML For Order type
        // $html  = CommonFunctions::getProductList(2290);
        // echo $html;die;
        $userData = json_decode($request->input('user_info'), true);
        $userAddress = json_decode($request->input('address'), true);
        $product_type=$request->has('product_type')?$request->input('product_type'):'food_item'; //default food_item
        $state_code=$request->has('state_code')?$request->input('state_code'):'';
        $userMyBag = array();
        $userPayCustId = "";
        $stripeCardId = "";
        $ms = microtime(true);
        $userMarkedNewFlag = false; // RESERVATION MODULE
        $bearerToken = $request->bearerToken();
        //CommonFunctions::$langId = $langId = config('app.language')['id'];
        $localization_id = $request->header('X-localization');
        $language_id =  CommonFunctions::getLanguageInfo($localization_id);
        CommonFunctions::$langId = $langId = $language_id->id;
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if($userAuth) {
            $locationId   = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','support_from'));
            $userId       = $userAuth->user_id;
            $userAuthId   = $userAuth->id;
            $email = $userData['email'];
            $fname = $userData['fname'];
            $lname = $userData['lname'];
            $phone = $userData['phone'];
            $address1 = $userAddress['address1'];
            $address2 = $userAddress['address2'];
            $city = $userAddress['city'];
            $zipcode = $userAddress['zipcode'];
            $lat = $userAddress['lat'];
            $lng = $userAddress['lng'];
            if ($bearerToken) {
                $userId       = $userAuth->user_id;
                $userInfo      = User::find($userId);
                $userPayCustId = $userInfo['stripe_customer_id'];
                $is_guest = 0;
                
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId,'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
            } else {
                // GUEST
                $is_guest = 1;
                //$address_id = $userAddress['address_id'];
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId, 'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
                $email_id = $email;
                $mobile_no = $phone;
                $userObj = User::where('restaurant_id', $restaurantId)
                    ->where(function ($query) use ($email_id,$mobile_no){
                        $query->where('email',$email_id)
                            ->orWhere('mobile', $mobile_no);
                    })->first();
                if(empty($userObj)) {
                     
                }else {
                   
                    $userId     = $userObj->id;
                    
                }
            }
            $customerName  = $fname . ' ' . $lname;
            $customerEmail = $email;
            $cardInfo = $request->input('card_info');
            $cardInfoData  = array();
            $cardInfoData = json_decode(base64_decode($cardInfo));
            //$userMyBag = UserMyBag::where(['user_id' => $userId, 'restaurant_id' => $restaurantId, 'status' => 0])->get();
            // Restaurant currency symbol
            $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
            if($restData) {
                $curSymbol = $restData->currency_symbol;
                $curCode = $restData->currency_code;
            } else {
                $curSymbol = config('constants.currency');
                $curCode = config('constants.currency_code');
            }
            $i           = 0;
            $orderAmount = 0;
            $totalQuant  = 0;
            $orderId     = 0;
            $bypFlag     = false;
            // find the total quantity and total amount
            if ($userMyBag->toArray()) {
                /* Expired OR time */
                $orderType = $request->input('order_type') ?? 'delivery';
                $time = new TimeslotController;
                if($orderType == 'delivery') {
                    // Get delivery slot
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'delivery',  $request->input('delivery_date'));
                }else {
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'carryout', $request->input('delivery_date'));
                }
                $selected_time =  $request->input('delivery_time');
                //if(!$request->has('product_type')) {
                if($product_type=='food_item') {
                    if($get_time_slot['status']) {
                        if($get_time_slot['slot']) {
                            $all_timeslots = array_column($get_time_slot['slot'], 'key');
                            if(!in_array($selected_time, $all_timeslots)) {
                                $status = Config('constants.status_code.BAD_REQUEST');
                                $me     = microtime(true) - $ms;
                                return response()->json(['data' => [], 'error' => 'Timeslot you has chosen has been expired.', 'xtime' => $me], $status);
                            }
                        }else {
                            $status = Config('constants.status_code.BAD_REQUEST');
                            $me     = microtime(true) - $ms;
                            return response()->json(['data' => [], 'error' => 'Invalid timeslot OR time has expired.', 'xtime' => $me], $status);
                        }
                    }else {
                        $status = Config('constants.status_code.BAD_REQUEST');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [], 'error' => $get_time_slot['error'], 'xtime' => $me], $status);
                    }
                }
                /* Expired OR time */
                $restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                $cityID = $restaurantData->city_id;
                $taxPercent=0;
                if($cityID!="" && $restaurantData->city_tax)
                {
                    $salesTax = City::where('id', $cityID)->first(['sales_tax']);
                    if($salesTax){
                        $taxPercent = $salesTax->sales_tax;
                    }
                }else {
                    $taxPercent = $restaurantData->tax;
                }
                foreach ($userMyBag as $item) {
                    $customization_price = 0;
                    $gift_wrapping_fees=0;
                    if($item['is_byp']) {
                        $bypData     = json_decode($item['menu_json'], true);
                        $userMyBag[$i]['item'] = $bypData['label'];
                        $userMyBag[$i]['image'] = $item['image'];
                    } elseif($item['menu_id']>0) {
                        $menuItem = MenuItems::where('id', $item['menu_id'])->first();
                        $userMyBag[$i]['item'] = $menuItem->name;
                        $userMyBag[$i]['product_type'] = $menuItem->product_type;
                        $dataImg = array();
                        if(!empty($menuInfo->images)) {
                            //if($menuInfo->images) {
                            $images_all = json_decode($menuInfo->images, true);
                            if($images_all && isset($images_all['desktop_web_images']) && count($images_all['desktop_web_images'])) {
                                /*
                                if($menuItem->images) {
                                    $images_all = json_decode($menuItem->images, true);
                                    if($images_all && isset($images_all['desktop_web_images'])) {
                                        $images_all = json_decode($menuItem->images, true)['desktop_web_images'];
                                        */
                                $dataImg[0] = array("image"=> $images_all['desktop_web_images'][0]['web_image'], "thumb_image_med"=>$images_all['desktop_web_images'][0]['web_thumb_med']);
                            }
                        }
                        $userMyBag[$i]['image'] = json_encode($dataImg);
                        //@10-09-2018 by RG Calculate Addons-price in case of Normal Customisation Items
                        if(isset($item['is_byp']) && $item['is_byp'] == 0 && $item['menu_json'] && (!empty($item['menu_json']) &&  !is_null($item['menu_json']) && $item['menu_json']!='null' && $item['menu_json']!=null && $item['menu_json']!='NULL' &&  $item['menu_json']!=NULL) && $menuItem->product_type!='gift_card') {
                            $menuJsonDecoded = json_decode($item['menu_json'], true);
                            foreach ($menuJsonDecoded as $premenu) {
                                if(isset($premenu['items'])&&$premenu['items']) {
                                    foreach ($premenu['items'] as $cust_menu) {
                                        if($cust_menu['price'] && $cust_menu['default_selected']) {
                                            $customization_price+= $cust_menu['price'] * $item['quantity'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (isset($item['menu_json'])) {
                        // TO REMOVE JSON DECODE AND ENCODE FROM HERE
                        $userMyBag[$i]['menu_json'] = json_decode($item['menu_json'], true);
                        $bypFlag = true;
                    }
                    if(isset($menuItem) && $menuItem->product_type=='gift_card' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        //$gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $gift_wrapping_fees=0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else if(isset($menuItem) && $menuItem->product_type=='product' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else{
                        $orderAmount += ($item['quantity'] * $item['unit_price']) + $customization_price;
                    }
                    $totalQuant  += $item['quantity'];
                    $i++;
                }
                $tipAmount      = 0;
                $tax            = 0;
                //$tax          = ($orderAmount * config('constants.tax')) / 100;
                $tax            = ($orderAmount * $taxPercent)/100;
                $tipPercent     = (float)$request->input('tip_percent');
                $tip_by_cash    = $request->input('tip_by_cash');
                if($tip_by_cash==0)
                    $tipAmount      = ($orderAmount * $tipPercent) / 100;
                $deliveryCharge = (float)$request->input('delivery_charge') ?? 0.00;
                $delivery_date =  $request->input('delivery_date');
                $is_asap_order = 0;
                if($request->has('is_asap') && $request->input('is_asap') && $request->has('asap_gap') && $request->input('asap_gap')) {
                    //get restaurant_current_time @16-08-2018 By RG
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                    if(is_object($currentDateTimeObj)) {
                        $selected_time = $currentDateTimeObj->format('H:i');
                    }else {
                        $selected_time = date('H:i');
                    }
                    $delivery_datetime = strtotime($delivery_date. ' '.$selected_time) + $request->input('asap_gap');
                    $delivery_date = date('Y-m-d', $delivery_datetime);
                    $selected_time = date('H:i',  $delivery_datetime);
                    $is_asap_order = 1;
                }
                $ordersData     = [
                    'parent_restaurant_id'   => $restaurantId,
                    'is_guest'        => $is_guest,
                    //'location_id'     => $locationId,
                    'restaurant_id'     => $locationId,
                    'user_id'         => $userId,
                    'user_auth_id'    => $userAuthId,
                    'user_ip'         => $_SERVER['REMOTE_ADDR'],
                    'fname'           => $fname,
                    'lname'           => $lname,
                    'email'           => $email,
                    'address'         => $address1,
                    'address2'        => $address2,
                    'city'            => $city,
                    'zipcode'         => $zipcode,
                    'latitude'        => $lat,
                    'longitude'       => $lng,
                    'phone'           => $phone,
                    'address_label'   => $userAddress['address_label'] ?? '',
                    'state'           => $userAddress['state'] ?? '',
                    'state_code'      => $state_code ?? '',
                    'order_type'      => $request->input('order_type') ?? 'delivery',
                    'order_amount'    => $orderAmount,                                         #post
                    'tax'             => $tax,                                                 #post
                    'tip_amount'      => $tipAmount,                                           #post
                    'tip_percent'     => $tipPercent,                                          #post
                    'delivery_charge' => $deliveryCharge,                                      #post
                    'total_amount'    => $orderAmount + $tax + $tipAmount + $deliveryCharge,   #post
                    'delivery_time'   => $selected_time,                #post
                    'delivery_date'   => $delivery_date,
                    'user_comments'   => $request->input('instructions'),                 #post
                    'status'          => 'pending',
                    'product_type'   =>  $product_type,
                    'is_asap_order'   =>  $is_asap_order                                      #post
                ];
                //print_r($ordersData); die;
                #RG By transaction ID @24-07-2018
                $randStr = md5(microtime());
                $txn_id = strtoupper(substr($randStr, -10));// last 10 Digit
                $userOrder = UserOrder::create($ordersData);
                $orderId   = $userOrder->id;
                if ($orderId > 0) {
                    $bagItemsArr  = [];
                    $bagItemIdArr = [];
                    $addonsData = [];
                    $orderDetailsData = [];
                    foreach ($userMyBag as $bgItem) {
                        $orderDetailsData = array();
                        $gift_wrapping_feesjson = NULL;
                        $menuItem = MenuItems::where('id', $bgItem->menu_id)->first();
                        if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' ) && !empty($bgItem->item_other_info)) {
                            $gift_wrapping_feesjson=json_decode($bgItem->item_other_info,true);
                            if($menuItem->product_type=='product'){
                                $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?$menuItem->gift_wrapping_fees :0;
                                $gift_wrapping_feesjson['gift_wrapping_fees']=$gift_wrapping_fees;
                            }
                            /*
                            if($menuItem->product_type=='gift_card'){
                                $promotion_amount=null;
                                $promotion_applicable=0;
                                $promo=CommonFunctions::productPromotions($restaurantId);
                                if(!empty($promo) && $promo->amount_or_percent==0){
                                    $promo_implement=CommonFunctions::productPromoCondition($bgItem->unit_price,$promo->condition_amount,$promo->condition);
                                    if($promo_implement){
                                        $promotion_amount=$promo->discount;
                                    }
                                }
                                $gift_wrapping_feesjson['promotion_amount']=$promotion_amount;
                            }
                            */
                            $gift_wrapping_feesjson=json_encode( $gift_wrapping_feesjson);
                        }
                        $orderDetailsData = [
                            'user_order_id'       => $orderId,
                            'item'                => $bgItem->item,
                            'item_size'           => $bgItem->size,
                            'menu_id'             => $bgItem->menu_id,
                            'is_byp'              => $bgItem->is_byp,
                            'menu_json'           => json_encode($bgItem->menu_json),
                            'image'               => $bgItem->image,
                            'special_instruction' => $bgItem->special_instruction,
                            'quantity'            => $bgItem->quantity,
                            'unit_price'          => $bgItem->unit_price,
                            'total_item_amt'      => $bgItem->total_menu_amount,
                            'item_other_info'     => $gift_wrapping_feesjson,
                            'status'              => 1,
                        ];
                        $bagItemIdArr[] = $bgItem->id;
                        //array_push($bagItemsArr, $orderDetailsData);
                        // Create order details from MyBag
                        $UserOrderDetail = UserOrderDetail::create($orderDetailsData);
                        $user_order_detail_id = $UserOrderDetail->id;
                        $addons_data = [];
                        /*Addons code*/
                        if (!is_null($bgItem->menu_json)) {
                            $addons_data =  CommonFunctions::changeMyBagJsonData($bgItem->is_byp, $bgItem->menu_json);
                            $addonsData[$bgItem->id] = $addons_data;
                        }
                        /*End code*/
                        //$addonsData[$bgItem->id] = $addons_data;
                    }
                    $tot_amount = $orderAmount + $tax + $tipAmount + $deliveryCharge;
                    $tot_amount = number_format($tot_amount, 2, '.', '') * 100 ;
                    #$tot_amount = (integer)($tot_amount * 100);
                    $transaction_id  = $cardNum = $cardType = "";
                    //$cardNumber = str_replace(' ', '', $cardInfoData->card);
                    if ($userPayCustId == null) {
                        $data = [
                            'description' => $customerName,
                            'email'       => $customerEmail,
                            //'source'      => 'tok_visa',
                        ];
                        $apiObj        = new ApiPayment();
                        $result        = $apiObj->createCustomer($data);
                        $userPayCustId = $result->id;
                        $cardData = [
                            'number'    => str_replace(' ', '', $cardInfoData->card),
                            'exp_month' => $cardInfoData->expiry_month,
                            'exp_year'  => $cardInfoData->expiry_year,
                            'cvc'       => $cardInfoData->cvv,
                            'name'      => $cardInfoData->name,
                        ];
                        //print_r($cardData); die;
                        $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);
                        if(isset($result2['errorMesg']))
                        {
                            $me = microtime(true) - $ms;
                            return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                        }
                        else {
                            //$stripeCardId = $result2->sources->data[0]->id;
                            $stripeCardId = $result2['id'];
                        }
                        //$stripeCardId = $result2->sources->data[0]->id;
                        $data = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'OrderID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        //print_r($data); die;
                        $chargeResponse  = $apiObj->createCharge($data);
                        $transaction_id  = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                    } else {
                        if(isset($cardInfoData->card_id)==null)
                        {
                            $cardNumber = str_replace(' ', '', $cardInfoData->card);
                            $cardData = [
                                'number'    => $cardNumber,
                                'exp_month' => $cardInfoData->expiry_month,
                                'exp_year'  => $cardInfoData->expiry_year,
                                'cvc'       => $cardInfoData->cvv,
                                'name'      => $cardInfoData->name,
                            ];
                            $apiObj        = new ApiPayment();
                            $result2      = $apiObj->createCustomerCard($userPayCustId, $cardData);
                            if(isset($result2['errorMesg']))
                            {
                                $me = microtime(true) - $ms;
                                return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                            }
                            else {
                                $stripeCardId = $result2['id'];
                            }
                            $userInfo = User::where('id', $userId)->first();
                            $userCard = "";
                            $userCard = $userInfo->card()->where('last_four', substr($cardNumber, -4))->where('user_id', $userId)->first();
                            if($cardInfoData->isSave && !$userCard) {
                                $data = [
                                    'user_id'        => $userId,
                                    'name'           => $cardInfoData->name,
                                    //'card'         => $cardNumber,
                                    'last_four'      => substr($cardNumber, -4),
                                    'type'           => $cardInfoData->type,
                                    //'expiry'         => $cardInfoData->expiry_month . '/' . $cardInfoData->expiry_year,
                                    'status'         => 1,
                                    'stripe_card_id' => $stripeCardId,
                                ];
                                UserCards::create($data);
                            }
                        }
                        else{
                            $stripeCardId         = $cardInfoData->card_id;
                            //$userCardInfo   = UserCards::where(['user_id' => $userId, 'status' => 1, 'id' => $cardID])->first();
                            //$userPayCardId = $userCardInfo['stripe_card_id'];
                        }
                        // Charge the user
                        $data           = [
                            'customer' => $userPayCustId,
                            'card_id'  => $stripeCardId,
                            'amount'   => $tot_amount,
                            'metadata' => 'ReceiptID:' . $txn_id,
                            'description' => 'MunchAdo-('.$restInfo['restaurant_name'].')-'.$restaurantData->restaurant_name,
                        ];
                        $apiObj         = new ApiPayment();
                        $chargeResponse = $apiObj->createCharge($data);
                        $transaction_id = $chargeResponse->id;
                        $cardNum = $chargeResponse->source->last4;
                        $cardType = $chargeResponse->source->brand;
                    }
                    $userOrder                   = UserOrder::find($userOrder->id);
                    $userOrder->status           = 'placed';
                    $userOrder->stripe_charge_id = $transaction_id;
                    $userOrder->stripe_card_id   = $stripeCardId;
                    $userOrder->card_number = 'XXXX-XXXX-XXXX-'.$cardNum;
                    $userOrder->card_type = $cardType;
                    $userOrder->payment_receipt = $txn_id;
                    $userOrder->restaurant_name = $restaurantData->restaurant_name;
                    if ($userOrder->save()) {
                        if($product_type=='gift_card') {
                            $this->giftCardGenerate($orderId);
                        }
                        // Replace stripe txn ID by Custom txn id
                        $userOrder->stripe_charge_id = $txn_id;
                        UserMyBag::whereIn('id', $bagItemIdArr)->delete();
                        //$langId = config('app.language')['id'];
                        //CommonFunctions::$langId = $langId;
                        $localization_id = $request->header('X-localization');
                        $language_id =  CommonFunctions::getLanguageInfo($localization_id);
                        CommonFunctions::$langId = $langId = $language_id->id;
                        //$restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                        $orderItem = $label = "";
                        $productType = "no";
                        if($product_type !='gift_card') {
                            foreach ($userMyBag as $bgItem){
                                $instruct='';
                                $orderItem .=  '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                                <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <b>'.$bgItem->quantity.' '.$bgItem->item.'</b>';
                                $result = isset($addonsData[$bgItem->id]) ? $addonsData[$bgItem->id] : null;
                                $menuItem = MenuItems::where('id', $bgItem->menu_id)->first();
                                if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' )) {
                                    $productType = "yes";
                                    if(!is_null( $bgItem->item_other_info)) {
                                        if (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) {
                                            $label = $bgItem->size . ' ';
                                        } else {
                                            $label = "";
                                        }
                                        $item_other_info = json_decode($bgItem->item_other_info);
                                        $value3 = '';
                                        $value = (isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?'Yes':'No';
                                        //$value2 = 'Gift Wrapping: ' . $value;
                                        $value2 =($menuItem->product_type=='product' )?'Gift Wrapping: ' . $value:'';
                                        if(isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message!='null')
                                        {
                                            $value3 = 'Message: ' .$item_other_info->gift_wrapping_message;
                                        }
                                        $gift_wrapping_fees=(isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?$menuItem->gift_wrapping_fees :0;
                                        if($gift_wrapping_fees!=0) {
                                            $label .= $value2 . '<br/>' . $value3.'<br/>Price: $'.$bgItem->quantity * $gift_wrapping_fees;
                                        }
                                        else {
                                            $label .= $value2 . '<br/>' . $value3;
                                        }
                                    }
                                } else {
                                    //$label = (strtolower($bgItem->size) != 'na' || $bgItem->size != 'full') ? $bgItem->size .' ' : '';
                                    if(strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size))
                                    {
                                        $label = $bgItem->size.' ';
                                    }
                                    else
                                    {
                                        $label = "";
                                    }
                                    if($bgItem->is_byp==1 && $result){
                                        foreach($result as $key=>$data){
                                            if($key!="")
                                                $label .= $key.', ';
                                            if($data!="")
                                                $label .= '['.$data.']>';
                                        }
                                    }
                                    if($bgItem->is_byp==0 && $result) {
                                        foreach ($result as $data) {
                                            if($data['addons_name']!=""){
                                                $label .= $data['addons_name'].', ';
                                            }
                                            if($data['addons_option']!="")
                                                $label .= '[' . $data['addons_option'] . ']>';
                                        }
                                    }
                                    if(!empty($bgItem->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$bgItem->special_instruction.'</p>';
                                    }
                                }
                                $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$bgItem->total_menu_amount.'</p></th></tr></table></th></tr></tbody></table>
                                <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                            }
                        }
                        /*else {
                            $orderItem = CommonFunctions::getProductList($userOrder->id);
                        }*/
                        $timestamp = strtotime($userOrder->delivery_date. ' '.$userOrder->delivery_time.':00');
                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => $userOrder->created_at)));
                        $address = $userOrder->address;
                        if ($userOrder->address2) {
                            $address = $address . ' ' . $userOrder->address2;
                        }
                        $address = $address . ' ' . $userOrder->city . ' ' . $userOrder->state . ' ' . $userOrder->zipcode;
                        $cardInfo = "<span style='color: #000;'>".$userOrder->card_type."</span>".' ('.$userOrder->card_number.')';
                        if($is_asap_order) {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp). '(Possibly sooner!)';
                        }else {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp);
                        }
                        // RESERVATION MODULE
                        // if user category hasn't been marked yet, means REPEAT user
                        $user_update = User::where('restaurant_id', $restaurantId)->where('id', $userId)->where('category', config('constants.guest_category.New'))->first();
                        if ($user_update && !$userMarkedNewFlag) {
                            $user_update->category = config('constants.guest_category.Repeat');
                            $user_update->save();
                        }
                        $mailKeywords = array(
                            'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                            'URL' => $restaurantData->source_url,
                            'ORDER_NUMBER' => $userOrder->payment_receipt,
                            'USER_NAME' => ucfirst($customerName),
                            'USER_EMAIL' => $userOrder->email,
                            'ORDER_TYPE' => ucfirst($userOrder->order_type),
                            'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                            'ORDER_DELIVERY_TIME' => $odr_delivery_time,
                            'DELIVERY_ADD' => $address,
                            'PHONE' => $userOrder->phone,
                            'SUB_TOTAL' => $curSymbol.$userOrder->order_amount,
                            'TOT_AMOUNT' => $curSymbol.$userOrder->total_amount,
                            'ITEM_DETAIL' => $orderItem,
                            'SITE_URL' => $restaurantData->source_url,
                            'ADMIN_URL' => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER' => $cardInfo,
                            'title' => "Order Place"
                        );
                        if($userOrder->tax==0.00){
                            $mailKeywords['TAX'] = '';
                            $mailKeywords['TAX_TITLE'] = '';
                            $mailKeywords['DISPLAY'] = 'none';
                        } else{
                            $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$userOrder->tax;
                            $mailKeywords['TAX_TITLE'] = "Taxes";
                            $mailKeywords['DISPLAY'] = 'table';
                        }
                        if($userOrder->user_comments!=""){
                            $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions</b> <br><i>'.$userOrder->user_comments.'</i><br><br>';
                        }
                        else {
                            $mailKeywords['SPECIAL_INFO'] = '';
                        }
                        if($userOrder->tip_amount==0.00){
                            $mailKeywords['TIP_AMOUNT'] = '';
                            $mailKeywords['TIP_TITLE'] = '';
                            $mailKeywords['DISPLAY_TIP'] = 'none';
                        }
                        else{
                            //$mailKeywords['TIP_AMOUNT'] = $userOrder->tip_percent.'%'.$curSymbol. $userOrder->tip_amount;
                            $mailKeywords['TIP_AMOUNT'] = '<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                            <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
<p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$userOrder->tip_percent.'<span>%</span></p>
                            </th></tr></table></th>
                            <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$userOrder->tip_amount.'</p></th></tr></table></th>
                        </tr></tbody></table>';
                            $mailKeywords['DISPLAY_TIP'] = 'table';
                            $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                        }
                        if($userOrder->order_type=="delivery") {
                            $mailKeywords['DELIVERY_CHARGE'] = $curSymbol.$userOrder->delivery_charge;
                            if($product_type == "product"){
                                $mailTemplate = 'place_order_product_delivery';
                            }elseif($product_type == "food_item"){
                                $mailTemplate = 'place_order_delivery';
                            }elseif($product_type == "gift_card"){
                                $mailTemplate = 'giftcard_order_placed_restaurant';
                            }
                        } else {
                            if($productType == "yes")
                                $mailTemplate = 'place_order_product_carryout';
                            else
                                $mailTemplate = 'place_order_carryout';
                        }
                        $header = "header_layout_restaurant";
                        $footer = "footer_layout_restaurant";
                        if($product_type == "gift_card") {
                            $mailData['subject'] = "Someone Bought $".intval($userOrder->total_amount)." Worth  of  Gift  Cards!";
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'manager');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                        }
                        elseif($product_type == "food_item"){
                            $mailData['subject'] = "You've Got a New ".ucfirst($userOrder->order_type)." Order from Munch Ado!";
                        }
                        elseif($product_type == "product"){
                            $mailData['subject'] = "New Shop Order from Your Munch Ado Website";
                        }
                        $mailTemplate = CommonFunctions::mailTemplate($mailTemplate, $mailKeywords, $header, $footer, $restaurantId);
                        $mailData['body'] = $mailTemplate;
                        //$mailData['receiver_email'] = $customerEmail;
                        $restaurantEmail = $restaurantData->email;
                        if($restaurantData->email=="")
                            $restaurantEmail = "sksingh1@bravvura.in";
                        $mailData['receiver_email'] = $restaurantEmail;
                        //$mailData['receiver_name'] = $customerName;
                        $mailData['receiver_name'] = $restaurantData->restaurant_name;
                        $mailData['MAIL_FROM_NAME'] = 'Munch Ado';
                        $mailData['MAIL_FROM'] = $restInfo['support_from'];
                        CommonFunctions::sendMail($mailData);
                        $userOrder['restaurant_name'] = $restaurantData->restaurant_name;
                        $me     = microtime(true) - $ms;
                        $status = 200;
                        #send mail to Buyers whi placed an Gift card ORders
                        if($product_type == "gift_card" && $userOrder->email) {
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'buyer');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                            $mailKeywords['BUYER_NAME'] = $userOrder->fname;
                            $mailTemplate = CommonFunctions::mailTemplate('giftcard_order_placed_buyer', $mailKeywords, 'header_layout', 'footer_layout', $restaurantId);
                            $mailData['subject'] = "Thank you for your Gift Card Order!";
                            $mailData['body'] = $mailTemplate;
                            $mailData['receiver_email'] = $userOrder->email;
                            #$mailData['receiver_email'] = 'rgupta@bravvura.in';
                            $mailData['MAIL_FROM_NAME'] = $restaurantData->restaurant_name;
                            $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                            CommonFunctions::sendMail($mailData);
                        }
                        /******** SMS Send **** @18-10-2018 by RG *****/
                        $sms_keywords = array(
                            'order_id' => $userOrder->id,
                            'sms_module' => 'order',
                            'action' => 'placed',
                            'sms_to' => 'customer'
                        );
                        CommonFunctions::getAndSendSms($sms_keywords);
                        /************ END SMS *************/
                        return response()->json(['data' => $userOrder, 'error' => '', 'xtime' => $me], $status);
                    } else {
                        $status = Config('constants.status_code.INTERNAL_ERROR');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [], 'error' => 'Order status could not be updated', 'xtime' => $me], $status);
                    }
                } else {
                    $status = Config('constants.status_code.INTERNAL_ERROR');
                    $me     = microtime(true) - $ms;
                    return response()->json(['data' => [], 'error' => 'Order not generated in DB.', 'xtime' => $me], $status);
                }
            } else {
                $me = microtime(true) - $ms;
                $status =Config('constants.status_code.BAD_REQUEST');
                return response()->json(['data' => [], 'error' => 'Sorry, Invalid Request OR there is no items in your Bag.', 'xtime' => $me], $status);
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => $tokenField.' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }
    public function opa_restaurant_list(Request $request) {
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
         
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version') && $request->has('type')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data'] = Restaurant::where('parent_restaurant_id', $check_token['restaurant_id'])->active()->toArray();
		    //print_r( $restaurantData);die;
		 }else {
                    $response['message'] = $check_opa_version['message'];
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        return response()->json($response, $status);
    }
    /**
     * @param order_type
     * @param
     * @author : Rahul Gupta
     * @Date Created : 21-08-2018
     * @return Archive Orders and Live Orders based on query
     */
    public function opa_order(Request $request) {
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version') && $request->has('type')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                if(!$request->has('search_keyword')) { 
                    $limit = $request->input('limit');
                    $page = $request->input('page');
                    // 12-Jul-19
                    // sorting by delivery_datetime ASC (discussed with Rajiv)

		    if($request->has('restaurant_id'))
		    $check_token['restaurant_id']=$request->input('restaurant_id');
		    $restaurantData = Restaurant::where('parent_restaurant_id', $check_token['restaurant_id'])->active()->toArray();
		    //print_r( $restaurantData);die;
		    
		    $restId[]=$check_token['restaurant_id'];	
		    if(isset($restaurantData[0]['parent_restaurant_id'])){
			foreach($restaurantData as $res){
			  $restId[]=$res['id'];
			}
                    }	
                    if($request->input('type') == 'archive') {
                        $order_type_key = 'archive_order';
                        $total_record_key = 'total_archive_records';
                        $statusArray = array("archived", "cancelled", "arrived", "sent", "rejected", "refunded");
                        $order_find =  UserOrder::select('user_orders.id')->whereIn('user_orders.restaurant_id', $restId)->whereIn('user_orders.status', $statusArray)->where('user_orders.product_type','=',"food_item")->orderBy('user_orders.id', 'DESC');
                        $total_counts = $order_find->count();
                        $order_ids = $order_find->paginate($limit);
                        $odr_ids = $order_ids->toArray();
                        $odr_ids = $odr_ids['data'];
                        if($odr_ids) {
                            $odr_ids = array_pluck($odr_ids, 'id');
                        }
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',                                
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.status', 
                                'user_orders.is_order_viewed',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_order_details.item', 
                                'user_order_details.item_size',
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'delivery_time','delivery_date',
                                'restaurants_comments',
                                'user_comments',
                                'is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update','menu_id',
                                'user_orders.updated_at'
                                )
                            ->whereIn('user_orders.id', $odr_ids)
                            ->whereIn('user_orders.restaurant_id', $restId)
                            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.updated_at', 'DESC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
//                            ->where('user_orders.product_type', 'food_item');
//                        
//                            if($request->has('sortorder')){
//                                $sortingfor = $request->input('sortorder');//dod or doo
//                                if($sortingfor == 'dod') {
//                                    $orderData = $orderData->orderBy('user_orders.delivery_datetime', 'ASC'); 
//                                }else {
//                                    $orderData = $orderData->orderBy('user_orders.created_at', 'ASC'); 
//                                }
//                            }else{
//                                $orderData = $orderData->orderBy('user_orders.id', 'DESC');
//                            }
//                            $orderData = $orderData->get();
                            ->where('user_orders.product_type', 'food_item')->get();
                    } else if($request->input('type') == 'schedule') {
                        //schedule Orders
                        
                        $order_type_key = 'schedule_order';
                        $total_record_key = 'total_schedule_records';
                        $statusArray = array("placed", "confirmed");
//                        if($request->input('type') == 'all') {
//                            $order_type_key = 'orders';
//                            $total_record_key = 'total_records';
//                            $statusArray = array('placed','ordered','confirmed','delivered','arrived','cancelled','rejected','archived','edited','sent','ready','refunded');
//                        }
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_orders.status', 
                                'user_order_details.item', 
                                'user_order_details.item_size', 
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'user_orders.is_order_viewed',
                                'delivery_time',
                                'delivery_date',
                                'restaurants_comments',
                                'user_comments','is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',                                
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update',
                                'item_price_desc',
                                'special_instruction',
                                'unit_price',
                                'menu_id',
                                'user_orders.updated_at');
//                        if($request->input('type') == 'all') {
//                            $orderData = $orderData
//                                ->whereDate('user_orders.created_at', '>=', $request->input('fromDate'))
//                                ->whereDate('user_orders.created_at', '<=',$request->input('toDate'))
//                            ;
//                        }
                        $orderData = $orderData
                            ->whereIn('user_orders.restaurant_id', $restId)
                            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.delivery_date', 'ASC')->orderBy('user_orders.delivery_time', 'ASC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->where('user_orders.product_type', 'food_item')
//                            ->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') );
//                            if($request->has('sortorder')){
//                                $sortingfor = $request->input('sortorder');//dod or doo
//                                if($sortingfor == 'dod') {
//                                    $orderData = $orderData->orderBy('user_orders.delivery_datetime', 'ASC'); 
//                                }else {
//                                    $orderData = $orderData->orderBy('user_orders.created_at', 'ASC'); 
//                                }
//                            }else{
//                                $orderData = $orderData->orderBy('user_orders.id', 'DESC');
//                            }
//                            $orderData = $orderData->get();                    
                            ->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )->get();
                    }else {
                        //Live Orders
                        $order_type_key = 'live_order';
                        $total_record_key = 'total_live_records';
                        $statusArray = array("placed", "confirmed", "ready");
                        if($request->input('type') == 'all') {
                            $order_type_key = 'orders';
                            $total_record_key = 'total_records';
                            $statusArray = array('placed','ordered','confirmed','delivered','arrived','cancelled','rejected','archived','edited','sent','ready','refunded');
                        }
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.is_order_viewed',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.status', 
                                'user_order_details.item', 
                                'user_order_details.item_size', 
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'delivery_time',
                                'delivery_date',
                                'restaurants_comments',
                                'user_comments',
                                'is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update',
                                'item_price_desc',
                                'special_instruction',
                                'unit_price','menu_id',
                                'user_orders.updated_at');
                        if($request->input('type') == 'all') {
                            $orderData = $orderData
                                ->whereDate('user_orders.created_at', '>=', $request->input('fromDate'))
                                ->whereDate('user_orders.created_at', '<=',$request->input('toDate'))
                            ;
                        }
                        $orderData = $orderData
                            ->whereIn('user_orders.restaurant_id', $restId)
                            ->whereIn('user_orders.status', $statusArray)->orderBy('user_orders.delivery_date', 'ASC')->orderBy('user_orders.delivery_time', 'ASC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->where('user_orders.product_type', 'food_item')
//                            ->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') );
//                            if($request->has('sortorder')){
//                                $sortingfor = $request->input('sortorder');//dod or doo
//                                if($sortingfor == 'dod') {
//                                    $orderData = $orderData->orderBy('user_orders.delivery_datetime', 'ASC'); 
//                                }else {
//                                    $orderData = $orderData->orderBy('user_orders.created_at', 'ASC'); 
//                                }
//                            }else{
//                                $orderData = $orderData->orderBy('user_orders.id', 'DESC');
//                            }
//                            $orderData = $orderData->get();                     
                            ->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )->get();
                            //->where(DB::raw('DATE_ADD(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL 12 HOUR)'),'>',DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)'))->get();
                    }
                }else{
                    $order_type_key = 'searched_orders';
                    $total_record_key = 'total_searched_records';
                    $statusArray = array('placed','ordered','confirmed','delivered','arrived','cancelled','rejected','archived','edited','sent','ready','refunded');
                    $orderData = UserOrder::select(
                            'restaurants.id','restaurants.restaurant_name','restaurants.delivery_provider_apikey',
                            'user_orders.id','user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount',
                            'user_orders.created_at','user_orders.delivery_time','user_orders.delivery_date','restaurants.kpt',
                            'user_orders.status', 'user_order_details.item', 'user_order_details.item_size', 'cities.time_zone',
                            'user_order_details.quantity', 'user_order_details.user_order_id','user_orders.is_read',
                            'restaurants_comments','user_comments','is_reviewed','review_id','user_orders.apt_suite',
                            'payment_receipt','city','user_orders.fname','user_orders.lname','user_orders.email',
                            'user_orders.phone','menu_items.name', 'menu_items.status as menu_status','manual_update',
                            'item_price_desc','special_instruction','unit_price','menu_id','users.fname as ufname',
                            'users.lname as ulname','users.email as uemail','users.mobile as umobile','restaurants.rest_code',
                            'restaurants.address as raddress','restaurants.zipcode as rzipcode','restaurants.street as rstreet', 
                            'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.updated_at',
                            'user_orders.zipcode', 'address_label','user_orders.user_id','is_order_printed','is_order_viewed',
                            DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'));
                       $orderData = $orderData
                                ->whereIn('user_orders.restaurant_id', $restId)
                                ->whereIn('user_orders.status', $statusArray)
                                ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                                ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                                ->leftJoin('users', 'user_orders.user_id', '=', 'users.id')
                                ->leftJoin('cities','restaurants.city_id','=','cities.id')
                                ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                                ->where('user_orders.product_type', 'food_item');
                        //sort Order
                        if($request->has('sortorder')){
                                $sortingfor = $request->input('sortorder');//dod or doo
                                if($sortingfor == 'dod') {
                                    $orderData = $orderData->orderBy('user_orders.delivery_datetime', 'ASC'); 
                                }else {
                                    $orderData = $orderData->orderBy('user_orders.created_at', 'ASC'); 
                                }
                        }else{
                                $orderData = $orderData->orderBy('user_orders.id', 'DESC');
                        }
                        if($request->input('search_keyword') !='') {
                            $keyword = $request->input('search_keyword');
                            $orderData = $orderData->where(function ($query) use ($keyword){
                                        $query->where('user_orders.email', 'like', '%' . $keyword. '%')
                                            ->orWhere('user_orders.phone', 'like', '%' . $keyword. '%')
                                            ->orWhere('user_orders.payment_receipt', 'like', '%' . $keyword. '%')
                                            ->orWhere('users.mobile', 'like', '%' . $keyword. '%')
                                            ->orWhere('users.email', 'like', '%' . $keyword. '%')
                                            ->orWhere(DB::raw('CONCAT(users.fname, " ", users.lname)'), 'like', '%' . $keyword. '%')
                                            ->orWhere(DB::raw('CONCAT(user_orders.fname, " ", user_orders.lname)'), 'like', '%' . $keyword. '%')
                                        ;
                                        });
                        }
                        $orderData = $orderData->get();
                }
                    if($orderData) {
                        $orderData = $orderData->toArray();
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                        if(is_object($currentDateTimeObj)) {
                            $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                            $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i');
                        }else {
                            $restcurrentDateTime['current_date'] = date('Y-m-d');
                            $restcurrentDateTime['current_time'] = date('H:i');
                        }
                        $currentVersion = $request->input('current_version');
                        $hardVersion = Config('constants.opa.DASHBOARD_HARD_VERSION_ANDROID');
                        $softVersion = Config('constants.opa.DASHBOARD_SOFT_VERSION_ANDROID');
                        if($currentVersion < $hardVersion){
                            $updateType = "hard";
                        }elseif($currentVersion < $softVersion){
                            $updateType = "soft";
                        }else{
                            $updateType = "no";
                        }
                        $update_app = array(
                            "upgrade_type"=>$updateType,
                            "counter"=>  Config('constants.opa.COUNTER'),
                            "message"=>  Config('constants.opa.FOURCE_UPDATE_MESSAGE'),
                            "clear_data"=>  Config('constants.opa.CLEAR_DATA'),
                            "apk_link"=>  Config('constants.opa.APK_FILE_PATH')
                        );
                        // if($request->input('type') == 'archive') {
                        //     foreach ($orderData as $key => $order) {
                        //        if(array_key_exists($order['user_order_id'], $orders)) {
                        //             $orders[$order['user_order_id']]['order_descriptions'] = $orders[$order['user_order_id']]['order_descriptions'].', '.$order['quantity'] .' '.$order['item'];
                        //        }else {
                        //             $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['restaurant_id'], 'datetime' => $order['created_at'])));
                        //             $delivery_time_left_hour = explode('.',(strtotime($order['delivery_date'].' '.$order['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                        //             $orders[$order['user_order_id']] = array(
                        //                 'id' => (string)$order['id'],
                        //                 'fname' => $order['fname'],
                        //                 'lname' => $order['lname'],
                        //                 'restaurant_id' => (string)$order['restaurant_id'],
                        //                 'restaurant_name' => $order['restaurant_name'],
                        //                 'accept_cc_phone' => '0',
                        //                 'delivery_time' => date('M d, Y', strtotime($order['delivery_date'])),
                        //                 'created_at' => date('M d, Y', $local_timestamp),
                        //                 'requested_delivery_time' =>date('h:i A', strtotime($order['delivery_time'])),
                        //                 'status' => $order['status'],
                        //                 'order_type' => $order['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                        //                 'type' => '1',
                        //                 'total_amount' => $order['total_amount'],
                        //                 'menu_status' => $order['menu_status'],
                        //                 'order_descriptions' => $order['quantity'] .' '.$order['item'],
                        //                 'order_status' => $order['status'],
                        //                 'is_reviewed' => (string)$order['is_reviewed'],
                        //                 'review_id' => $order['review_id'] ? $order['review_id'] : '',
                        //                 'is_live' => $order['menu_status'] == 1 ? 'yes' : 'no',
                        //                 'closed' => $order['menu_status'] == 1 ? 'yes' : 'no',
                        //                 'inactive' => $order['menu_status'] == 1 ? 'yes' : 'no',
                        //                 'city_id' => '1',
                        //                 'city_name' => $order['city']
                        //             );
                        //        }
                        //     }
                        // }else {
                        //live Orders keys
                        $kptCalendar = "0-0-0";
                        foreach ($orderData as $key => $order) {
                            if(array_key_exists($order['user_order_id'], $orders)) {
                                $item_count = count($orders[$order['user_order_id']]['item_list']);
                                $orders[$order['user_order_id']]['item_list'][$item_count] =  array(
                                    "order_item_id"=> (string)$order['menu_id'],
                                    "item_name"=> (string)$order['item'],
                                    "item_size"=>  $order['item_size'],
                                    "item_qty"=>  (string)$order['quantity']
                                );
                            }else {
                                $refunded_amount = DB::table('refund_histories')->where('order_id', $order['id'])->sum('amount_refunded');
                                $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['restaurant_id'], 'datetime' => $order['created_at'])));
                                $delivery_time_left_hour = explode('.',(strtotime($order['delivery_date'].' '.$order['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                                $orderType = ($order['order_type'] == 'carryout')? 'Takeout': 'Delivery';
                                $stageOfOrder = $this->orderStage($order['status'], $orderType);
                                $deliveryAddress = "";
                                if($orderType == "Delivery"){
                                    $address = [trim($order['address']),trim($order['address2']),trim($order['apt_suite']),trim($order['city']),trim($order['zipcode'])];
                                    $deliveryAddress  = array_filter($address);
                                    $deliveryAddress = implode(",", $deliveryAddress);                                    
                                }
                                
                                $customerTotalOrder = CommonFunctions::getCustomerOrders($order);
                                $timezone = $order['time_zone'];
                                $preparation_time = $order['kpt'];
                                $orders[$order['user_order_id']] = array(
                                    'id' => (string)$order['id'],
                                    'fname' =>  $order['fname'],
                                    'lname' =>  $order['lname'],
                                    'email' =>  $order['email'],
                                    'phone' =>  $order['phone'],
                                    'delivery_address'=>$deliveryAddress,
                                    'is_order_viewed'=>$order['is_order_viewed'],
                                    'delivery_time_left_hour' => (int)$delivery_time_left_hour,
                                    'delivery_time_left' => 0,
                                    'restaurant_id' => (string)$order['restaurant_id'],                                              
                                    'restaurant_name' => $order['restaurant_name'],
                                    'is_restaurant_exists' => 'Yes',
                                    'order_type1' => 'I',
                                    'order_type' => $order['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                                    'delivery_date' => $order['delivery_date'].' '.date('H:i:00', strtotime($order['delivery_time'])),
                                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                                    'menu_status' => $order['menu_status'],
                                    'is_reviewed' => $order['is_reviewed'] ? $order['is_reviewed'] : 0,
                                    'review_id' => $order['review_id'] ? $order['review_id'] : 0,
                                    'manual_update' => $order['manual_update'],
                                    'payment_receipt' =>  $order['payment_receipt'],
                                    'delivery_service' => $order['delivery_provider_apikey'] ? 1 : 0,
                                    'service_provider' => '',
                                    'status' => $order['status'],
                                    'refunded_amount' => $refunded_amount ? (string) number_format($refunded_amount, 2) : 'NA',
                                    'total_amount' => $order['total_amount'],
                                    'user_total_order'=>$customerTotalOrder,
                                    'item_list' => array(
                                        array(
                                            "order_item_id"=> (string)$order['menu_id'],
                                            "item_name"=> (string)$order['item'],
                                            "item_size"=>  $order['item_size'],
                                            "item_qty"=> (string) $order['quantity']
                                        )
                                    ),
                                    'order_stage'=>$stageOfOrder,
                                    'time_zone'=>$order['time_zone'],
                                    'kpt'=>$order['kpt'],
                                    'kpt_cal'=>$order['kpt_calender'],
                                    'updated_at'=>$order['updated_at']
                                );
                                $kptCalendar = $order['kpt_calender'];
                            }
                            //}
                        }
                    }
                    if($orders) {
                        $orders = array_values($orders);
                    }
                    $get_restaurant_setting = CommonFunctions::getRestaurantSetting($check_token['restaurant_id']);
                                     
                    $restInfo = CommonFunctions::getRestaurantDetails($check_token['restaurant_id'], false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','support_from'));
                    $statusArray = array("placed");
                    $liveOrderCount = UserOrder::select('user_orders.id')
                        ->whereIn('user_orders.restaurant_id', $restInfo)
                        ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
                        ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',
                            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')
                        ->where('user_orders.product_type','=',"food_item")->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )
                        ->whereIn('user_orders.status', $statusArray)->get()->count();
                    $scheduleOrderCount = UserOrder::select('user_orders.id')
                       ->whereIn('user_orders.restaurant_id', $restId)
                        ->where('user_orders.is_order_viewed', 0)
                        ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
                        ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',
                            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')
                        ->where('user_orders.product_type','=',"food_item")->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )
                        ->whereIn('user_orders.status', $statusArray)->get()->count();
                    $timekpt = $this->getTimeZoneAndKpt($check_token['restaurant_id']); 
                   
                    $response['data'] = array(
                        $order_type_key => $orders,
                        'restService' => $get_restaurant_setting,
                         $total_record_key => $total_counts ? $total_counts :  count($orders),
                        'fource_update' => $update_app,
                        'timezone'=>isset($timekpt['time_zone'])?$timekpt['time_zone']:"",
                        'preparation_time'=>isset($timekpt['kpt_calender'])?$timekpt['kpt_calender']:"0-0-0",                        
                        'order_counts' => [
                            'live'      => $liveOrderCount,     // active confirmed orders count
                            'schedule'  => $scheduleOrderCount  // unread scheduled orders count
                        ]
                    );
                }else {
                    $response['message'] = $check_opa_version['message'];
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        return response()->json($response, $status);
    }
    /**
     * @param order ID
     * @param
     * @author : Rahul Gupta
     * @Date Created : 21-08-2018
     * @return Detail of Orders
     */
    public function opa_order_detail(Request $request, $order_id) {
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    //Live Orders
                    $order_type_key = 'live_order';
                    $total_record_key = 'total_live_records';
                    $statusArray = array("placed", "confirmed", "ready");
                    
                    $orderForList = $request->input('list');
                    if($orderForList==1){
                        $response['data'] = $this->getOrderDetailsForList($order_id);
                        return response()->json($response);
                    }
                    // Using query builder
                    DB::table('user_orders')->where('id', $order_id)->update(['is_order_viewed'=>1]);
                    //UserOrder::where('id', $order_id)->update(['is_order_viewed'=>1]);
                    
                    $orderData = UserOrder::select('users.fname as ufname','users.lname as ulname','users.email as uemail','users.mobile as umobile','users.display_pic_url','restaurants.id','user_orders.user_id','restaurants.restaurant_name','restaurants.delivery_provider_apikey','restaurants.rest_code','restaurants.address as raddress','restaurants.zipcode as rzipcode','restaurants.street as rstreet' ,'restaurant_logo_name','user_orders.id','user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount','user_orders.created_at','name_on_card','card_type','expired_on','billing_zip','expired_on',
                   'user_orders.delivery_time','user_orders.delivery_date','user_orders.status', 'user_order_details.item','user_order_details.id as user_order_detail_id', 'user_order_details.quantity', 'user_order_details.item_size', 'user_order_details.total_item_amt', 'user_order_details.user_order_id','delivery_time','delivery_date','restaurants_comments','user_comments','is_reviewed','review_id','payment_receipt','city','user_orders.fname','user_orders.lname','user_orders.email','user_orders.phone','menu_items.name', 'menu_items.status as menu_status','tip_percent', 'user_orders.cod', 'host_name', 'user_orders.order_pass_through', 'user_orders.city_id', 'manual_update', 'ds_order_id', 'latitude', 'longitude','restaurants.phone as rphone', 'user_orders.tax', 'user_orders.zipcode', 'header_color_code', 'special_checks', 'restaurant_image_name', 'apt_suite', 'tip_amount', 'user_orders.delivery_charge', 'deal_discount', 'promocode_discount', 'redeem_point', 'pay_via_point', 'pay_via_card','facebook_url', 'twitter_url','card_number','order_amount','user_orders.address','user_orders.address2','address_label','user_orders.state','user_order_details.menu_id','user_order_details.menu_json','unit_price',
                   'special_instruction','menu_items.status as mstatus', 'item_price_desc', 'user_orders.is_byp', 'user_orders.is_guest','user_order_details.is_byp as byp_new','user_orders.is_order_viewed','user_orders.flat_discount','user_orders.service_tax','user_orders.additional_charge','user_orders.additional_charge_name','restaurants.kpt')
                    ->where('user_orders.id', $order_id)
                    ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                    ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                    ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                    ->leftJoin('users', 'user_orders.user_id', '=', 'users.id')
                    ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                    ->get();
                    #Get Refund of this Order if any
                    $refunded_amount = 0;
                    $refund_reason = array();
                    $refunded_amount_data = DB::table('refund_histories')->where('order_id', $order_id)->select('amount_refunded', 'reason')->get();
                    if( $refunded_amount_data) {
                        foreach ($refunded_amount_data as $key => $amount_data) {
                            $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                            $refund_reason[$key] = array('amount' => number_format($amount_data->amount_refunded, 2), 'reason' => $amount_data->reason);
                        }
                    }
                    if($orderData) {
                        $orderDataObj=$orderData;
                        $orderData = $orderData->toArray();
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                        if(is_object($currentDateTimeObj)) {
                            $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                            $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
                        }else {
                            $restcurrentDateTime['current_date'] = date('Y-m-d');
                            $restcurrentDateTime['current_time'] = date('H:i:00');
                        }
                        $currentVersion = $request->input('current_version');
                        $hardVersion = Config('constants.opa.DASHBOARD_HARD_VERSION_ANDROID');
                        $softVersion = Config('constants.opa.DASHBOARD_SOFT_VERSION_ANDROID');
                        if($currentVersion < $hardVersion){
                            $updateType = "hard";
                        }elseif($currentVersion < $softVersion){
                            $updateType = "soft";
                        }else{
                            $updateType = "no";
                        }
                        $update_app = array(
                            "upgrade_type"=>$updateType,
                            "counter"=>  Config('constants.opa.COUNTER'),
                            "message"=>  Config('constants.opa.FOURCE_UPDATE_MESSAGE'),
                            "clear_data"=>  Config('constants.opa.CLEAR_DATA'),
                            "apk_link"=>  Config('constants.opa.APK_FILE_PATH')
                        );
                        foreach ($orderData as $key => $order) {                            
                            $curorderobj=$orderDataObj[$key];
                            $curorderobj->id=$curorderobj->user_order_detail_id;
                            $add_data = CommonFunctions::getAddonsAndModifiers($curorderobj);
                            $add_data_returned = empty($add_data) ? (object)[] : $add_data;
                            $addon_modifier=!empty($curorderobj->user_order_detail_id)? $add_data_returned : (object)[];
                            $addon_list = array();
                            if(array_key_exists($order['user_order_id'], $orders)) {
                                $item_count = count($orders[$order['user_order_id']]['item_list']);
                                //@10-09-2018 Addon by RG
                                $menuJsonDecoded = json_decode($order['menu_json'], true) ?? null;
                                if($order['is_byp'] == 0 && $menuJsonDecoded) {
                                    foreach ($menuJsonDecoded as $premenu) {
                                        if(isset($premenu['items']) && $premenu['items']) {
                                            $addon_label = $premenu['label'];
                                            foreach ($premenu['items'] as $cust_menu) {
                                                if($cust_menu['price'] && $cust_menu['default_selected']) {
                                                    $addon_list[] = array(
                                                        "addons_id"=> $cust_menu['id'],
                                                        "addon_label"=> $addon_label,
                                                        "addon_name"=> $cust_menu['label'],
                                                        "addon_price"=>  $cust_menu['price'],
                                                        "addons_total_price"=> (string) ($order['quantity'] * $cust_menu['price']),
                                                        "addon_quantity"=>  (string) $order['quantity']
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                                #Addons End#
                                /*added by jawed*/
                                $is_byp = 0;
                                if($order['byp_new'] == 1 ) {
                                    $addon_modifier = json_decode($order['menu_json'], true) ?? null;
                                    $is_byp = 1;
                                }
                                /*added by jawed*/
                                $orders[$order['user_order_id']]['item_list'][$item_count] =  array(
                                    "order_item_id"=> $order['menu_id'],
                                    "item_name"=> $order['item'],
                                    "item_qty"=>  $order['quantity'],
                                    "item_size"=>  $order['item_size'],
                                    "unit_price"=>  $order['unit_price'],
                                    "total_item_amt"=>  $order['total_item_amt'],
                                    "item_special_instruction"=>  $order['special_instruction'],
                                    "item_price_desc"=>  $order['item_price_desc'],
                                    "item_status"=>  $order['mstatus'],
                                    "addons_list"=> $addon_list,
                                    "addon_modifier"=> $addon_modifier,
                                    "is_byp" => $is_byp,
                                );
                            }else {
                                $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['restaurant_id'], 'datetime' => $order['created_at'])));
                                $delivery_time_left_hour = explode('.',(strtotime($order['delivery_date'].' '.$order['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                                $orderArchived = false;
                                if(in_array($order['status'], array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                                    $orderArchived = true;
                                }
                                if($order['user_id']) {
                                    $total_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $check_token['restaurant_id'])->where('user_orders.user_id', $order['user_id'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
                                }else {
                                    $total_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $check_token['restaurant_id'])->where('user_orders.email', $order['email'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
                                }
                                //@10-09-2018 Addon by RG
                                $menuJsonDecoded = json_decode($order['menu_json'], true) ?? null;
                                if($order['is_byp'] == 0 && $menuJsonDecoded) {
                                    foreach ($menuJsonDecoded as $premenu) {
                                        if(isset($premenu['items']) && $premenu['items']) {
                                            $addon_label = $premenu['label'];
                                            foreach ($premenu['items'] as $cust_menu) {
                                                if($cust_menu['price'] && $cust_menu['default_selected']) {
                                                    $addon_list[] = array(
                                                        "addons_id"=> $cust_menu['id'],
                                                        "addon_label"=> $addon_label,
                                                        "addon_name"=> $cust_menu['label'],
                                                        "addon_price"=>  $cust_menu['price'],
                                                        "addons_total_price"=> (string) ($order['quantity'] * $cust_menu['price']),
                                                        "addon_quantity"=>  (string) $order['quantity']
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                                #Addons End#
                                $cust_fname = $order['fname'];
                                $cust_lname = $order['lname'];
                                $cust_email = $order['email'];
                                $phone_no = $order['phone'];
                                if($order['is_guest'] == 0) {
                                    $cust_fname = $order['ufname'];
                                    $cust_lname = $order['ulname'];
                                    $cust_email = $order['uemail'];
                                    $phone_no = $order['umobile'] ? $order['umobile'] : $order['phone'];
                                }else {
                                    $order['fname']  = '';
                                    $order['lname']  = '';
                                    $order['email']  = '';
                                    $order['phone'] = '';
                                }
                                if($order['order_type'] == 'delivery') {
                                    $del_address = $order['address'].' '.$order['address2'].' '.$order['city'].' '.$order['state'].' '.$order['zipcode'];
                                    if($order['address_label']) {
                                        $del_address = $order['address_label'].' '.$del_address;
                                    }
                                }else {
                                    $del_address = $order['restaurant_name'].' '.$order['raddress'].' '.$order['rstreet'].' '.$order['rzipcode'];
                                }
                                // As discussed with Rajiv, no calculations can be done on Mobile devices so adding check on server side ;)
                                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array( "restaurant_id" => $order['restaurant_id'] ));
                                if (is_object($currentDateTimeObj)) {
                                    $currentTimestamp = strtotime($currentDateTimeObj->format('Y-m-d H:i:s'));
                                } else {
                                    $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
                                }
                                $deliveryTimestamp  = strtotime($order['delivery_date'] . ' ' . $order['delivery_time'] . ':00');
                                $order_del_time     = ($deliveryTimestamp - $order['kpt'] * 60);
                                $is_order_scheduled = 0;
                                if ($currentTimestamp < $order_del_time) {
                                    $is_order_scheduled = 1;
                                }
                                /*added by jawed*/
                                $is_byp = 0;
                                if($order['byp_new'] == 1) {
                                    $addon_modifier = json_decode($order['menu_json'], true) ?? null;
                                    $is_byp = 1;
                                }
                                /*added by jawed*/
                                $orders[$order['user_order_id']] = array(
                                    'id' => (string) $order['id'],
                                    'user_id' => $order['user_id'],
                                    // As discussed with Ramesh, Deepak (Only for OPA, not for CMS)
                                    'customer_first_name' => !empty($order['fname']) ? $order['fname'] : $cust_fname,
                                    'customer_last_name' => !empty($order['lname']) ? $order['lname'] : $cust_lname,
                                    'email' => !empty($order['email']) ? $order['email'] : $cust_email,
                                    "phone_no" => !empty($order['phone']) ? $order['phone'] : $phone_no,
                                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                                    'status' => $order['status'],
                                    'order_type1' => 'I',
                                    'order_type2' => 'p',
                                    'is_byp' => $order['is_byp'],
                                    'is_order_viewed' => $order['is_order_viewed'],
                                    'is_order_scheduled' => $is_order_scheduled,
                                    'delivery_date' => $order['delivery_date'].' '.date('H:i:s', strtotime($order['delivery_time'])),
                                    'order_type' => $order['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                                    'special_instruction' => $order['user_comments'],
                                    'user_comments' => $order['user_comments'],
                                    'restaurant_id' => $order['restaurant_id'],
                                    'tip_percent' => $order['tip_percent'],
                                    'redeem_point' => $order['redeem_point'],
                                    'pay_via_point' => $order['pay_via_point'],
                                    'pay_via_card' => $order['pay_via_card'],
                                    'total_amount' => $order['total_amount'],
                                    'payment_receipt' => $order['payment_receipt'],
                                    'is_reviewed' => $order['is_reviewed'],
                                    'review_id' => (int)$order['review_id'] ? $order['review_id'] : 0,
                                    'cod' => $order['is_reviewed'],
                                    'created_at' =>  date('Y-m-d H:i:s', $local_timestamp),
                                    'host_name' => $order['host_name'],
                                    'order_pass_through' => $order['order_pass_through'],
                                    'city_id' => $order['city_id'],
                                    'restaurants_comments' => $order['restaurants_comments'] ? $order['restaurants_comments'] : '',
                                    'manual_update' => $order['manual_update'],
                                    'ds_order_id' => $order['ds_order_id'],
                                    'latitude' => $order['latitude'],
                                    'longitude' => $order['longitude'],
                                    'pay_via_cash' => '0',
                                    'orderArchived' => $orderArchived,
                                    'delivery_service' => $order['delivery_provider_apikey'] ? 1 : 0,
                                    'service_provider' => '',
                                    "pos" => "1",
                                    "orderStatus" => $order['status'],
                                    "sales_tax" => $order['tax'],
                                    "minimum_delivery" => "0.00",
                                    "current_date" => $restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'],
                                    'restaurant_name' => $order['restaurant_name'],
                                    "restaurant_address" => $order['raddress'],
                                    "restaurant_city" => $order['city'],
                                    "restaurant_state_code" =>  "NY",
                                    "restaurant_zipcode" => $order['rzipcode'],
                                    "rest_code" => $order['rest_code'],
                                    "restaurantLogo" => $order['restaurant_logo_name'],
                                    "social_link" => array(
                                        'i_fb.png#Facebook' =>  (string) $order['facebook_url'],
                                        'i_twtr.png#Twitter' => $order['twitter_url'],
                                    ),
                                    "header_color_code" => $order['header_color_code'],
                                    "time_of_delivery" => date('M d, Y', strtotime($order['delivery_date'])).' @ '.date('h:i A', strtotime($order['delivery_time'])),
                                    "delivery_hour" => '00',
                                    "delivery_minutes" => '00',
                                    'time_of_order' => date('M d, Y @ h:i A', $local_timestamp),
                                    "delivery_time_left" => 0,
                                    'delivery_time_left_hour' => $delivery_time_left_hour,
                                    'created_time' => date('M d, Y', $local_timestamp),
                                    "special_checks" => $order['user_comments'],
                                    "restaurant_image_name" => $order['restaurant_image_name'],
                                    "my_payment_details" => array(
                                        "card_name" => $order['name_on_card'],
                                        "card_number" => $order['card_number'],
                                        "card_type" => $order['card_type'],
                                        "expiry_year" => '2022',
                                        "expiry_month" => '10',
                                        "billing_zip" => $order['zipcode'],
                                    ),
                                    "my_delivery_detail" => array(
                                        "first_name" => $order['fname'],
                                        "last_Name"  => $order['lname'],
                                        "email"      => $order['email'],
                                        "address_label" => $order['address_label'],
                                        "city"       => $order['city'],
                                        "apt_suite"  => $order['apt_suite'],
                                        "state"      => $order['state'],
                                        "phone"      => $order['phone'],
                                        "delivery_address" => $del_address,
                                        "address" => $del_address,
                                        "zipcode" => $order['zipcode'],
                                    ),
                                    "order_amount_calculation" => array(
                                        "subtotal"=>  $order['order_amount'],
                                        "tax_amount"=> $order['tax'],
                                        "tip_amount"=>  $order['tip_amount'],
                                        "delivery_charge"=> $order['delivery_charge'],
                                        "promocode_discount"=> (string)$order['promocode_discount'],
                                        "redeem_point"=> (string)$order['redeem_point'],
                                        "pay_via_point"=> $order['pay_via_point'],
                                        "pay_via_card"=> $order['pay_via_card'],
                                        "pay_via_cash"=> '0',
                                        "total_order_price"=> $order['total_amount'],
                                        'refunded_amount' => number_format($refunded_amount, 2),
                                        'refund_history' => $refund_reason,
                                        // As discussed with Rakesh & Rajeev
                                        "discount"=> (string)$order['flat_discount'],//$order['deal_discount'],
                                        'service_tax'               => (string)$order['service_tax'],
                                        // As discussed with Rajeev (acc. to CMS)
                                        'additional_charge'         => (string)$order['additional_charge'],
                                        'additional_charge_name'    => $order['additional_charge_name']
                                    ),
                                    'item_list' => array(
                                        array(
                                            "order_item_id"=> $order['menu_id'],
                                            "item_name"=> $order['item'],
                                            "item_qty"=>  $order['quantity'],
                                            "item_size"=>  $order['item_size'],
                                            "unit_price"=>  $order['unit_price'],
                                            "total_item_amt"=>  $order['total_item_amt'],
                                            "item_special_instruction"=>  $order['special_instruction'],
                                            "item_price_desc"=>  $order['item_price_desc'],
                                            "item_status"=>  $order['mstatus'],
                                            "addons_list"=> $addon_list,
                                            "addon_modifier"=> $addon_modifier,
                                            "is_byp" => $is_byp,
                                        )
                                    ),
                                    'user_image' => $order['display_pic_url'],
                                    'user_default_image' => '',
                                    'user_activity' => array(
                                        "total_user_order" => $total_counts,
                                        "total_user_review" => "0",
                                        "total_user_checkin" => "0",
                                        "total_user_reservation" => "0"
                                    ),
                                    'fource_update' => $update_app
                                );
                            }
                        }
                    }
                    if($orders) {
                        $orders = array_values($orders);
                        $orders['0']['item_list'] = array_values($orders['0']['item_list']);
                        $orders = $orders['0'];
                    }
                    $response['data'] = $orders;
                }else {
                    $response['message'] = $check_opa_version['message'];
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
    /**
     * @param order ID
     * @param
     * @author : Rahul Gupta
     * @Date Created : 22-08-2018
     * @return Set the Order status and send email based on that
     */
    public function opa_order_update_status(Request $request, $order_id) {
        $requestData = json_decode($request->getContent(), true);
        #$requestData = $request->input();
        \Log::info('opa_order_update_status');\Log::info($requestData);
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );
        $delivery_service_error_message = '';
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $orderData = UserOrder::find($order_id);
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                if(is_object($currentDateTimeObj)) {
                    $currentTimestamp =  strtotime($currentDateTimeObj->format('Y-m-d H:i:s'));
                    $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                    $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
                }else {
                    $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
                    $restcurrentDateTime['current_date'] = date('Y-m-d');
                    $restcurrentDateTime['current_time'] = date('H:i:00');
                }
                $action = $requestData['status'];                
                $restaurantData = Restaurant::where(array('id'=> $check_token['restaurant_id']))->first();
                if($restaurantData) {
                    $curSymbol = $restaurantData->currency_symbol;
                    $curCode = $restaurantData->currency_code;
                } else {
                    $curSymbol = config('constants.currency');
                    $curCode = config('constants.currency_code');
                }
                $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
                $order_del_time = ($deliveryTimestamp - $restaurantData->kpt * 60);
                $is_order_scheduled = 0;
                if($currentTimestamp < $order_del_time){
                    $is_order_scheduled = 1;
                }
                //Update time slot:
                $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                    ->where('status', 1)->where('id', $restaurantData->parent_restaurant_id)->first();
                
                if(isset($requestData['status']) && $requestData['status'] == 'placed' && isset($requestData['delivery_date'])) {
                    #$orderData = UserOrder::where('id',$order_id)->select('id','user_id')->get();
                    if($orderData) {
                        $status = Config('constants.status_code.STATUS_SUCCESS');
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        list($date, $time) = explode(' ', $requestData['delivery_date']);
                        $orderData->delivery_date = $date;
                        $orderData->delivery_time = $time;                       
                        $orderData->save();
                        $delivery_time_left_hour = (int) explode('.',(strtotime($requestData['delivery_date']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                        $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
                        $order_del_time = ($deliveryTimestamp - $restaurantData->kpt * 60);
                        $is_order_scheduled = 0;
                        if($currentTimestamp < $order_del_time){
                            $is_order_scheduled = 1;
                        }
                        $response['data'] = array(
                            'message' => true,
                            'status' => $requestData['status'],
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => $delivery_time_left_hour
                        );
                    }
                    // UPDATE STATUS EMAIL >>>
                    if($orderData->email && filter_var($orderData->email, FILTER_VALIDATE_EMAIL) ) {
                        $restaurant_name = htmlentities($restaurantData->restaurant_name, ENT_QUOTES, "UTF-8");
                        $mailKeywords = array(
                            'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                            'RESTAURANT_ADD' => $restaurantData->address,
                            'ORDER_NUMBER' => $orderData->payment_receipt,
                            'USER_NAME' => ucfirst($orderData->fname),
                            'ORDER_TYPE' => ucfirst($orderData->order_type),
                            'ORDER_DELIVERY_TIME' => date('l d M, h:i A', strtotime($orderData->delivery_date.' '.$orderData->delivery_time.':00')),
                            'HEAD' => 'Order Time Update',
                            'URL' => $restaurantData->source_url,
                            'REST_NAME' => $restaurant_name,
                            'SITE_URL' => $restaurantData->source_url,
                            'title' => "Timeslot update",
                            'APP_BASE_URL'=>config('constants.mail_image_url'),
                        );
                        $mailKeywords = array(
                            'header' => array(),
                            'footer' => array(),
                            'data' => $mailKeywords
                        );
                        $mailTemplate = 'order_time_update';
                        $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                        $mailData['subject']        = "Your order time updated";
                        $mailData['body']           = $mail_html;
                        $mailData['receiver_email'] =  $orderData->email;
                        $mailData['receiver_name']  = ucfirst($orderData->fname);
                        $mailData['MAIL_FROM_NAME'] =  $restaurant_name;
                        $mailData['MAIL_FROM'] = $parentData->custom_from;
                        CommonFunctions::sendMail($mailData);
                        // <<< UPDATE STATUS EMAIL
                    }
                    return response()->json($response, $status);
                }
                #common data required:
                $langId = config('app.language')['id'];
                CommonFunctions::$langId = $langId;
                $orderItem = $label = "";
                $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
                $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
                $cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
                $orderDetail = new UserOrderDetail();
                $orderDetailData = $orderDetail->where('user_order_id', $order_id)->get();;
                $orderItem = '';
                $total_refunded_amount = DB::table('refund_histories')->where('order_id', $order_id)->sum('amount_refunded');
                ##-----------Cancel Order --- ###
                if(isset($requestData['status']) && $requestData['status'] == 'cancelled') {
                    $orderData->status = $requestData['status'];
                    $orderData->restaurants_comments = $requestData['reason'];
                    $orderData->save();
                    //twinjet cancel
                    $deliveryServicesData1 = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                    if(isset($deliveryServicesData1) && $deliveryServicesData1->provider_name=="twinjet"){
                        $orderDataArray = $orderData->toArray();
                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                        $restaurantData1= $restaurantData->toArray();
                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                    }// send email
                    $orderItem = '';
                    foreach($orderDetailData as $data){
                        $label = "";
                        $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                        $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                        $addons_data = [];
                        $bypData = json_decode($data->menu_json, true );
                        //print_r($bypData); die;
                        if (!is_null($bypData)) {
                            $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                        }
                        $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                    <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <b>'.$data->quantity.' '.$data->item.'</b>';
                        $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                        
                        if($data->is_byp){
                            $label='';
                            $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                        }else{
                            $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                            $label .= $addon_modifier_html;
                        }
                            $instruct = '';
                            if(!empty($data->special_instruction)){
                                $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                            }
                            $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                    }
                    $mailKeywords = array(
                        'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                        'RESTAURANT_ADD' => $restaurantData->address,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                        'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                        'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                        'CARD_NUMBER' => $cardInfo,
                        'REASON' => $requestData['reason'],
                        'ITEM_DETAIL' => $orderItem,
                        'SITE_URL' => $restaurantData->source_url,
                        'title' => "Cancel Order"
                    );
                    /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);
                    /**************************************************************************************************/
                   
                    $tipAmount = $orderData->tip_amount;
                    
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                    if($orderData->order_type=="delivery") {
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery-cancel';
                    } else {
                        $mailTemplate = 'carryout_cancel';
                        $mailKeywords['USER_ADDRESS'] = '';
                    }
                    $mailKeywords = array(
                        'header' => array(),
                        'footer' => array(),
                        'data' => $mailKeywords
                    );
                    $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                    $mailData['subject']        = "Your order has been cancelled";
                    $mailData['body']           = $mail_html;
                    $mailData['receiver_email'] =  $orderData->email;
                    $mailData['receiver_name']  = ucfirst($orderData->fname);
                    $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                    $mailData['MAIL_FROM'] = $parentData->custom_from;
                    CommonFunctions::sendMail($mailData);
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data'] = array(
                        'message' => true,
                        'status' => 'cancelled',
                        'order_id' => $order_id,
                        'orders' => 0,
                        'is_order_scheduled' => $is_order_scheduled,
                    );
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'rejected',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                    $status  = Config('constants.status_code.STATUS_SUCCESS');
                }
                ## END CANCEL ORDER Template
                #status = archived, sent, ready, confirmed
                ############ Confirm Order / Ready Order ##########
                if(isset($requestData['status']) && in_array($requestData['status'], array('confirmed',' sent','ready', 'archived'))) {
                    $action = $requestData['status'];
                    ############ Relay Delivery service ##########
                    if($orderData->order_type == "delivery" && $restaurantData->delivery_provider_id > 0 && !$requestData['manual_update'] && $requestData['status']==="confirmed"){
                        $deliveryServiceObj = new DeliveryServiceProvider();
                        $deliveryServicesData = DeliveryServices::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                        $deliveryRelayDetails = array(
                            'id'=>$restaurantData->delivery_provider_id,
                            'producer_key'=>$restaurantData->producer_key,
                            'order_creation_url'=>$deliveryServicesData->order_creation_url,
                            'api_key'=>$restaurantData->delivery_provider_apikey
                        );
                        $deliveryServiceObj->deliveryRelayDetails = $deliveryRelayDetails;
                        if(!$deliveryServiceObj->orderCreation($orderData)){
                            if(isset($deliveryServiceObj->orderCreationSuccess['message'])) {
                                #$response['message'] = $deliveryServiceObj->orderCreationSuccess['message'];
                                $delivery_service_error_message = $response['message'] = "There was an error connecting to your delivery service. Please call them to schedule delivery for this order.";
                            } else {
                                #$response['message'] = "Something went wrong while pushing an order to Relay Service provider.";
                                $delivery_service_error_message = $response['message'] = "There was an error connecting to your delivery service. Please call them to schedule delivery for this order.";
                            }
                            $me = microtime(true) - $ms;
                            $response['xtime'] = $me;
                            #return response()->json($response, $status);
                        }
                        //twinjet confirm order
                        if($deliveryServicesData->provider_name=="twinjet" && $orderData->order_type=="delivery"){ //trigger twinjet
                            $orderDataArray = $orderData->toArray();
                            $restaurantDataArray = $restaurantData->toArray();
                            $orderDetailsData =  UserOrderDetail::find($orderData->id);
                            //$orderDetailsData = $orderDetail->getOrderDetail($orderData->id);
                            $orderDetailsData1 = $orderDetailsData->toArray();
                            $orderDataArray['item_list'][] = $orderDetailsData1;
                            $responseData = $this->submitOrderToVendor($orderDataArray,$restaurantDataArray,1,$deliveryServicesData->id);
                        }
                    }
                    ####################### end ###############
                    $orderData->status = $action;
                    $orderData->save();
                    //foreach($orderDetailData as $data){
                    //$orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><b>'.$data->item.'</b> (QTY: '.$data->quantity.')</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>$</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table><hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                    //}
                    $orderItem = '';
                    foreach($orderDetailData as $data){
                        $label = "";
                        $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                        $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                        $addons_data = [];
                        $bypData = json_decode($data->menu_json, true );
                        //print_r($bypData); die;
                        if (!is_null($bypData)) {
                            $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                        }
                        $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                    <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <b>'.$data->quantity.' '.$data->item.'</b>';
                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                  
                        if($data->is_byp){
                            $label='';
                            $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                        }else{
                            $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                            $label .= $addon_modifier_html;
                        }
                        $instruct = '';
                        if(!empty($data->special_instruction)){
                            $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                        }
                        $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                    }
                    $mailKeywords = array(
                        'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'DELIVERY_ADD' => $orderData->address.' '.$orderData->city,
                        'PHONE' => $orderData->phone,
                        'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                        'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                        'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                        'ITEM_DETAIL' => $orderItem,
                        'CARD_NUMBER' => $cardInfo,
                        'URL' => $restaurantData->source_url,
                        'SITE_URL' => $restaurantData->source_url,
                        'title' => "Order Confirmation"
                    );
                    /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);
                    /**************************************************************************************************/
                   /* if($orderData->tax==0.00){
                        $mailKeywords['TAX'] = '';
                        $mailKeywords['TAX_TITLE'] = '';
                        $mailKeywords['DISPLAY'] = 'none';
                    } else{
                        $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$orderData->tax;
                        $mailKeywords['TAX_TITLE'] = "Taxes";
                        $mailKeywords['DISPLAY'] = 'place_order_delivery';
                    }*/
                    $tipAmount = $orderData->tip_amount;
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                  /*  if($orderData->tip_amount==0.00){
                        $mailKeywords['TIP_AMOUNT'] = '';
                        $mailKeywords['TIP_TITLE'] = '';
                        $mailKeywords['DISPLAY_TIP'] = 'none';
                    } else{
                        $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                    <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
        <p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                                    </th></tr></table></th>
                                    <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                        <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$tipAmount.'</p></th></tr></table></th>
                                </tr></tbody></table>';
                        $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                        $mailKeywords['DISPLAY_TIP'] = 'table';
                    }*/
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->order_type=="delivery") {
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery_confirm_user';
                    } else {
                        $mailTemplate = 'carryout_confirm_user';
                        $mailKeywords['USER_ADDRESS'] = '';
                        $img = "";
                        if($action == "ready"){
                            $heading = "We have finished preparing your takeout order and can't wait for you to enjoy it.";
                            $img = '<a href="#"><img src="https://dashboard.munchado.biz/mailer-images/keki/get-direction.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                            $mailKeywords['DISPLAY_IMG'] = 'none';
                            /******** SMS Send **** @18-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $orderData->id,
                                'sms_module' => 'order',
                                'action' => 'ready',
                                'sms_to' => 'customer'
                            );
                            CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }else{
                            $heading = "You've put together an amazing order.";
                            $img = '<a href="#"><img src="https://dashboard.munchado.biz/mailer-images/keki/manage-order.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                            $mailKeywords['DISPLAY_IMG'] = 'table';
                            /******** SMS Send **** @18-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $orderData->id,
                                'sms_module' => 'order',
                                'action' => 'confirmed',
                                'sms_to' => 'customer'
                            );
                            CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }
                        $mailKeywords['HEAD'] = $heading;
                        $mailKeywords['IMG'] = $img;
                        $mailKeywords['ACTION'] = ucfirst($action);
                    }
                    $config['to'] = $orderData->email;
                    $config['subject'] = "Your order has been ".$action;
                    //$config['from'] = 'KEKI'."<MunchAdo2015@gmail.com>";
                    $status  = Config('constants.status_code.STATUS_SUCCESS');
                    $delivery_time_left_hour = explode('.',(strtotime($orderData->delivery_date.' '.$orderData->delivery_time) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                    if(($action == "archived") || ($action=="sent")) {
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        $response['data'] = array(
                            'message' => true,
                            'status' => $action,
                            'order_id' => $order_id,
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            #'delivery_service' => false,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => (int) $delivery_time_left_hour
                        );
                    }
                    else{
                        $mailKeywords = array(
                            'header' => array(),
                            'footer' => array(),
                            'data' => $mailKeywords
                        );
                        $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                        $mailData['subject']        = "Your order has been ".$action;
                        $mailData['body']           = $mail_html;
                        $mailData['receiver_email'] =  $orderData->email;
                        $mailData['receiver_name']  = ucfirst($orderData->fname);
                        $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                        $mailData['MAIL_FROM'] = $parentData->custom_from;
                        CommonFunctions::sendMail($mailData);
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        if($action == "ready") {
                            $action = "arrived";
                        }
                        $response['data'] = array(
                            'message' => true,
                            'status' => $action,
                            'order_id' => $order_id,
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            #'delivery_service' => false,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => (int) $delivery_time_left_hour
                        );
                    }
                    # END Confirm Order ####3
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
    /**
     * @param order ID
     * @param
     * @author : Rahul Gupta
     * @Date Created : 14-12-2018
     * @return Refund Order amount, If fully refunded then only status will be refunded in partial refund status will not change
     */
    public function order_refund(Request $request) {
        $requestData = json_decode($request->getContent(), true);
        #$requestData = $request->input();
        \Log::info('opa_order_order_refund');\Log::info($requestData);
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('order_id')  && $request->has('refund_type') && in_array($request->input('refund_type'), array('F','P'))) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                #$orderData = UserOrder::find($request->input('order_id'));
                $orderData = UserOrder::where('id', $request->input('order_id'))->whereNotIn('status', ['refunded','pending'])->first();
                if($orderData) {
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                    if(is_object($currentDateTimeObj)) {
                        $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                        $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
                    }else {
                        $restcurrentDateTime['current_date'] = date('Y-m-d');
                        $restcurrentDateTime['current_time'] = date('H:i:00');
                    }
                    $action = $orderData->status;
                    $globalSetting = DB::table('global_settings')->whereNotNull('bcc_email')->whereNotNull('bcc_name')->first();
                    if($request->input('refund_mode') && $request->input('refund_mode') == 'fully') {
                        $action = 'refunded';
                        // if($request->input('refund_type') == 'F') {
                        //     $refund_value = $orderData->total_amount;
                        //     $amount_refunded = $refund_value = $orderData->total_amount;
                        // }else {
                        //     $amount_refunded  = $orderData->total_amount;
                        //     $refund_value = 100; //100% is being send
                        // }
                        $refund_value = $orderData->total_amount;
                        $amount_refunded = $refund_value = $orderData->total_amount;
                    }else {
                        if($request->input('refund_type') == 'F') {
                            $refund_value = $request->input('refund_value');
                            $amount_refunded = $request->input('refund_value');
                        }else {
                            $amount_refunded  =  ($request->input('refund_value') * $orderData->total_amount) / 100;
                            $refund_value = $request->input('refund_value'); //100% is being send
                        }
                    }
                    if($amount_refunded <= $orderData->total_amount) {
                        $restaurantData = Restaurant::where(array('id'=> $check_token['restaurant_id']))->first();
                        // Restaurant currency symbol
                        if($restaurantData) {
                            $curSymbol = $restaurantData->currency_symbol;
                            $curCode = $restaurantData->currency_code;
                        } else {
                            $curSymbol = config('constants.currency');
                            $curCode = config('constants.currency_code');
                        }
                        //Update time slot:
                        $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                            ->where('status', 1)->where('id', $restaurantData->parent_restaurant_id)->first();
                        /*$apiObj         = new ApiPayment();
                        $stripeRefundData = [
                            'charge' => $orderData->stripe_charge_id,
                            'amount'   => ($amount_refunded * 100),
                            'reason' => 'requested_by_customer',
                        ];
                        #$getCharge =  $apiObj->getCharge($orderData->stripe_charge_id);
                        #echo "<pre>";print_r($getCharge); die;
                        $refundResponse = $apiObj->createRefund($stripeRefundData);*/
                        $refundResponse = $this->refundInitatedOnPaymentGateway($orderData,$amount_refunded,$action);
                        if(!isset($refundResponse['errorMessage'])) {
                            #Refund Data Saved
                            $refund_data = array(
                                'order_id' => $request->input('order_id'),
                                'refund_type' => $request->input('refund_type'),
                                'refund_value' => $refund_value,
                                'amount_refunded' => $amount_refunded,
                                'charge_id' => $refundResponse->charge,
                                'refund_id' =>  $refundResponse->id,
                                'reason' =>  $request->input('reason'),
                                'host_name' =>  $request->input('host_name'),
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            );
                            $oldOrderStatus = $orderData->status;
                            #RefundHistory::create($refund_data);
                            DB::table('refund_histories')->insert($refund_data);
                            $orderData->status = $action;
                            $orderData->total_amount = $orderData->total_amount - $amount_refunded;
                            if($orderData->total_amount <= 0) {
                                $orderData->status = 'refunded';
                            }
                            $orderData->save();
                            $deliveryServicesData1 = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                            if(isset($deliveryServicesData1) && $deliveryServicesData1->provider_name=="twinjet"){
                                if($request->input('refund_mode') != 'partially') {
                                    if($orderData->order_type=="delivery" && ($oldOrderStatus!="delivered" && $oldOrderStatus!="archived" )){
                                        $orderDataArray = $orderData->toArray();
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $restaurantData1 = $restaurantData->toArray();
                                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                                    }
                                }else{
                                    if($orderData->total_amount == 0  && $orderData->order_type=="delivery" && ($oldOrderStatus!="delivered" && $oldOrderStatus!="archived" )){
                                        $orderDataArray = $orderData->toArray();
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $restaurantData1 = $restaurantData->toArray();
                                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                                    }else{
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $deliveryServicesData = DeliveryServices::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                                        if($deliveryServicesData && $deliveryServicesData->provider_name=="twinjet"){ //trigger twinjet
                                            $orderDataArray = $orderData->toArray();
                                            $restaurantDataArray = $restaurantData->toArray();
                                            $orderDetailsData1 = [];
                                            $orderDataArray['item_list'][] = $orderDetailsData1;
                                            $responseData = $this->submitOrderToVendor($orderDataArray,$restaurantDataArray,0,$deliveryServicesData->id);
                                        }
                                    }
                                }
                            }
                            #common data required:
                            $langId = config('app.language')['id'];
                            CommonFunctions::$langId = $langId;
                            $orderItem = $label = "";
                            $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
                            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
                            $cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
                            if($orderData->payment_gatway=='Card'){
                                $cardInfo = "<br /> Card <br />".$orderData->card_type.' ('.$orderData->card_number.')';
                            }elseif($orderData->payment_gatway=='Cash'){
                                $cardInfo = "Cash";
                            }else{
                                $cardInfo = $orderData->payment_gatway;
                            }
                            $orderDetail = new UserOrderDetail();
                            $orderDetailData = $orderDetail->where('user_order_id',  $request->input('order_id'))->get();
                            $orderItem = '';
                            #if($orderData->status == 'refunded') {
                            if(1) {
                            ##-----------Refund Order Email and sms--- ###
                                $total_refunded_amount = DB::table('refund_histories')->where('order_id', $request->input('order_id'))->sum('amount_refunded');
                                    // send email
                                    $orderItem = '';
                                foreach($orderDetailData as $data){
                                    $label = "";
                                    $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true );
                                    //print_r($bypData); die;
                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }
                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                            <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                            <b>'.$data->quantity.' '.$data->item.'</b>';
                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                    //$label = strtolower($data->size) != 'full' ? $data->size .' ' : '';
                                   // $label = (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? $data->item_size .' ' : '';
                                    // if($data->is_byp==1 && $result){
                                    //     foreach($result as $key=>$data1){
                                    //         $label .= $key;
                                    //         $label .= '['.$data1.'] ';
                                    //     }
                                    // }
                                    // if($data->is_byp==0 && $result) {
                                    //     foreach ($result as $data1) {
                                    //         $label .= $data1['addons_name'];
                                    //         $label .= '[' . $data1['addons_option'] . '] ';
                                    //     }
                                    // }
                                    if($data->is_byp){
                                        $label='';
                                        $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                                    }else{
                                        $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                                        $label .= $addon_modifier_html;
                                    }
                                    $instruct = '';
                                    if(!empty($data->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                                    }
                                    $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }
                                $mailKeywords = array(
                                    'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                                    'RESTAURANT_ADD' => $restaurantData->address,
                                    'ORDER_NUMBER' => $orderData->payment_receipt,
                                    'USER_NAME' => ucfirst($orderData->fname),
                                    'ORDER_TYPE' => ucfirst($orderData->order_type),
                                    'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                                    'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                                    'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                                    'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                                    'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($orderData->total_amount,2),
                                    'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                                    'CARD_NUMBER' => $cardInfo,
                                    'REASON' => $request->input('reason'),
                                    'ITEM_DETAIL' => $orderItem,
                                    'SITE_URL' => $restaurantData->source_url,
                                    'title' => "Refund Order",
                                    'URL' => $restaurantData->source_url,
                                );
                                /*************************************************************************************************/
                                $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);
                                /**************************************************************************************************/
                                if($orderData->tax==0.00){
                                    $mailKeywords['TAX'] = '';
                                    $mailKeywords['TAX_TITLE'] = '';
                                    $mailKeywords['DISPLAY'] = 'none';
                                } else{
                                    $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$orderData->tax;
                                    $mailKeywords['TAX_TITLE'] = "Taxes";
                                    $mailKeywords['DISPLAY'] = 'table';
                                }
                                $tipAmount = $orderData->tip_amount;
                                if($orderData->tip_amount==0.00){
                                    $mailKeywords['TIP_AMOUNT'] = '';
                                    $mailKeywords['TIP_TITLE'] = '';
                                    $mailKeywords['DISPLAY_TIP'] = 'none';
                                } else{
                                    $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                                    $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                                <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left"><p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                                                </th></tr></table></th>
                                                <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$tipAmount.'</p></th></tr></table></th>
                                            </tr></tbody></table>';
                                    $mailKeywords['DISPLAY_TIP'] = 'table';
                                }
                                $mailKeywords['DISPLAY_REFUND'] = 'table';
                                if($orderData->user_comments!=""){
                                    $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                                }
                                else {
                                    $mailKeywords['SPECIAL_INFO'] = '';
                                }
                                if($orderData->order_type=="delivery") {
                                    $address = $orderData->address;
                                    if ($orderData->address2) {
                                        $address = $address . ' ' . $orderData->address2;
                                    }
                                    $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                                    //echo $address;
                                    $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                                    $mailKeywords['USER_ADDRESS'] = $address;
                                    $mailTemplate = 'delivery_order_refund';
                                } else {
                                    $mailKeywords['USER_ADDRESS'] = '';
                                    $mailTemplate = 'carryout_order_refund';
                                }
                                $order_refund_date = date('M d, Y h:i A', strtotime($orderData->delivery_date.' '.$orderData->delivery_time.':00'));
                                if($orderData->status == 'refunded'){
                                    $mailKeywords['HEAD'] = "We've initiated a refund for your experience with us on $order_refund_date";
                                }else {
                                    $mailKeywords['HEAD'] = "We've initiated a partial refund for your experience with us on $order_refund_date";
                                }
                                $mailKeywords = array(
                                    'header' => array(),
                                    'footer' => array(),
                                    'data' => $mailKeywords
                                );
                                $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                                $mailData['subject']  = "Your Refund Is Being Processed";
                                $mailData['body']           = $mail_html;
                                $mailData['receiver_email'] =  $orderData->email;
                                $mailData['receiver_name']  = ucfirst($orderData->fname);
                                $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                                $mailData['MAIL_FROM'] = $parentData->custom_from;
                                if($globalSetting) {
                                    $mailData['bcc_email'] = $globalSetting->bcc_email;
                                    $mailData['bcc_name'] = $globalSetting->bcc_name;
                                }
                                CommonFunctions::sendMail($mailData);
                                /******** SMS Send **** @18-10-2018 by RG *****/
                                $sms_keywords = array(
                                    'order_id' => $orderData->id,
                                    'sms_module' => 'order',
                                    'action' => 'refund',
                                    'sms_to' => 'customer',
                                    'amount_refunded' => $total_refunded_amount
                                );
                                CommonFunctions::getAndSendSms($sms_keywords);
                                /************ END SMS *************/
                            }
                            $status  = Config('constants.status_code.STATUS_SUCCESS');
                            #$response['message'] = 'Refund initiated successfully!';
                            $response['result'] = true;
                            $response['message'] = 'Success';
                            $response['data'] = array(
                                'message' => true,
                                'status' => $action,
                                'order_id' => $request->input('order_id'),
                                'orders' => 0
                            );
                        }else {
                            $response['message'] = $refundResponse['errorMessage'];
                        }
                        ## END CANCEL ORDER Template
                    }else {
                        $response['message'] = 'Invalid refund amount!';
                    }
                }else {
                    $response['message'] = 'Invalid refund Order Request!';
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
    public function submitOrderToVendor($orderData,$restaurantData1,$isCreate=1,$provider_id){
        $res = null;
        try{
            if(isset($restaurantData1['delivery_provider_apikey'])){
                $deliverServiceProvider = new Twinjet(['apikey'=>$restaurantData1['delivery_provider_apikey']]);
                if($isCreate == 1){ //create job
                      $city = DB::table('cities')->where('id', $restaurantData1['city_id'])->first();
                      if($city && $city->time_zone !='') {
                        $restaurantData1['city'] = $city->city_name;
                        $restaurantData1['state'] = $city->state_code;
                        $restaurantData1['time_zone'] = $city->time_zone;
                        $res= $deliverServiceProvider->createOrderTwinjet($orderData,$restaurantData1);
                        $deliveryServiceLogObj = new DeliveryServicesLogs();
                        $deliveryServiceLogObj->provider_id  = $provider_id;
                        $deliveryServiceLogObj->order_id  = $orderData['id'];
                        $deliveryServiceLogObj->order_source_id  = 0;
                        if(isset($res['response_data']['request_id']))
                        $deliveryServiceLogObj->order_receipt  = $res['response_data']['request_id'];
                        $deliveryServiceLogObj->restaurant_id   = $restaurantData1['id'];
                        $deliveryServiceLogObj->order_key  = $orderData['id'];
                        $deliveryServiceLogObj->response_data  = (string)json_encode($res['response_data']);
                        $deliveryServiceLogObj->request_data  = (string)$res['request_data'];
                        $deliveryServiceLogObj->updated_at   = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->created_at  = date("Y-m-d H:i:s");
                        $deliveryServiceLogObj->save();
                    }
                }elseif($isCreate == 2){ //delete job
                      $log = DB::table('delivery_services_logs')->where('order_id',$orderData['id'])->orderBy('delivery_services_logs.id', 'DESC')->whereNotNull('order_receipt')->first();
                      if($log){
                            $deliveryServiceLogObj  = DeliveryServicesLogs::find($log->id);
                            $res = $deliverServiceProvider->canceledOrderTwinjet($log->order_receipt);
                            $deliveryServiceLogObj = new DeliveryServicesLogs();
                            $deliveryServiceLogObj->provider_id  = $provider_id;
                            $deliveryServiceLogObj->order_id  = $orderData['id'];
                            $deliveryServiceLogObj->order_source_id  = 0;
                            if(isset($res['response_data']['request_id']))
                            $deliveryServiceLogObj->order_receipt  = $res['response_data']['request_id'] ."- Cancel";
                            $deliveryServiceLogObj->restaurant_id   = $restaurantData1['id'];
                            $deliveryServiceLogObj->order_key  = $orderData['id'];
                            $deliveryServiceLogObj->response_data  = (string)json_encode($res['response_data']);
                            $deliveryServiceLogObj->request_data  = (string)json_encode($res['request_data']);
                            $deliveryServiceLogObj->updated_at   = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->created_at  = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->save();
                     }
                }else{  //edit job
                      $city = DB::table('cities')->where('id', $restaurantData1['city_id'])->first();
                      if($city && $city->time_zone !='') {
                        $restaurantData1['city'] = $city->city_name;
                        $restaurantData1['state'] = $city->state_code;
                        $restaurantData1['time_zone'] =$city->time_zone;
                        $log = DB::table('delivery_services_logs')->where('order_id',$orderData['id'])->orderBy('delivery_services_logs.id', 'DESC')->whereNotNull('order_receipt')->first();
                        if($log) {
                            $deliveryServiceLogObj  = DeliveryServicesLogs::find($log->id);
                            $res = $deliverServiceProvider->editOrderTwinjet($orderData,$restaurantData1,$log->order_receipt);
                            $deliveryServiceLogObj->provider_id  = $provider_id;
                            $deliveryServiceLogObj->order_id  = $orderData['id'];
                            $deliveryServiceLogObj->order_source_id  = 0;
                            if(isset($res['response_data']['request_id']))
                            $deliveryServiceLogObj->order_receipt  = $res['response_data']['request_id'];
                            $deliveryServiceLogObj->restaurant_id   = $restaurantData1['id'];
                            $deliveryServiceLogObj->order_key  = $orderData['id'];
                            $deliveryServiceLogObj->response_data  = (string)json_encode($res['response_data']);
                            $deliveryServiceLogObj->request_data  = (string)$res['request_data'];
                            $deliveryServiceLogObj->updated_at   = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->created_at  = date("Y-m-d H:i:s");
                            $deliveryServiceLogObj->save();
                        }else {
                            $this->submitOrderToVendor($orderData,$restaurantData1,1, $provider_id);
                        }
                       }
                }
            }
        }catch(Excption $e){
                $mailKeywords = array(
                                'header' => array(),
                                'footer' => array(),
                                'data' => $mailKeywords
                            );
                            $mailData['subject']        = "Twinjet API error order->id".$orderData['id'];
                            $mailData['body']           =  "Twinjet Error";
                            $mailData['receiver_email'] =  'cprabvakar@bravvura.in';
                            $mailData['receiver_name']  =  "Twinjet Error";
                            $mailData['MAIL_FROM_NAME'] =  "Bar Cargo";
                            $mailData['MAIL_FROM'] = "cprabvakar@bravvura.in";
                            CommonFunctions::sendMail($mailData);
        }
        return $res;
    }
    /**
    * @param order ID
    * @param
    * @author : Rahul Gupta
    * @Date Created : 08-02-2019
    * @return Detail of Orders Logs
    */
    public function opa_order_log(Request $request, $order_id) {
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $order_logs = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token')) {
            // generate token for the user
            $requestData = $request->input();
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $logs = CmsUserLog::where('cms_user_logs.restaurant_id', $check_token['restaurant_id'])->where('cms_user_logs.updated_id', $order_id)
                ->with(['cms_user']);
                if(isset($requestData['to']) && $requestData['from']) {
                    $logs = $logs->whereDate('created_at', '>=', $request->input('from'))->whereDate('created_at', '<=', $request->input('to'));
                }
                $log_data = $logs->orderBy('created_at')->get();
                if($log_data) {
                    $log_data = $log_data->toArray();
                    foreach ($log_data as $key => $log) {
                        $json = json_decode($log['action_data']);
                        $order_logs[] = array(
                            'username' => $log['cms_user']['name'],
                            'action' => ucfirst( $json->status),
                            'updated_time' =>  $log['created_at'],
                            'comments' =>  '',
                        );
                    }
                }
                $refunded_amount_data = DB::table('refund_histories')->where('order_id', $order_id)->select('host_name', 'reason','created_at')->orderBy('created_at')->get();
                if( $refunded_amount_data) {
                    foreach ($refunded_amount_data as $key => $refunded) {
                       $order_logs[] = array(
                            'username' => $refunded->host_name,
                            'action' => ucfirst('Refund'),
                            'updated_time' => $refunded->created_at,
                            'comments' =>  $refunded->reason,
                        );
                    }
                }
                $status = Config('constants.status_code.STATUS_SUCCESS');
                $response['result'] = true;
                $response['message'] = 'Success';
                $response['data'] = $order_logs;
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
    public static function registerUser($userData,$restaurantId){
	$firstTimeGuestUser = 0;
        $user = User::where(['email' => trim($userData['email']), 'restaurant_id' => $restaurantId, 'status' => 1])->first();
        if(isset($user->id) and $user->id !=''){
             if($user->is_registered==0){
		$userAddresses = UserAddresses::where(['user_id' =>$user->id]);
		 
		//DB::table('user_addresses')->where('user_id', $user->id)->update($userData);
		$firstTimeGuestUser=1;	
	     }	
	//ALTER TABLE `users` ADD `is_registered` INT(1) NOT NULL DEFAULT '1' COMMENT '0 for guest user, 1 for registered user' AFTER `parent_restaurant_id`;
        }else{
		 $data['fname']=$userData['fname'];
		 $data['lname']=$userData['lname'];
		 $data['username']=$data['fname']." ".$data['lname'];
	         $data['email']=$userData['email'];
		 $data['restaurant_id']=$restaurantId;
		 $data['password ']=Hash::make(time());
		 $data['old_password']=Hash::make(time());
		 $data['phone']=$userData['phone'];
		 $data['status']=1;	
		 $data['category']='New';
		 $data['is_registered']=0;
		 $user = User::create($data);
		 //$userData['user_id']=$user->id;
		 //$userData['address_type']='billing';			
		 //$userAdd = UserAddresses::create($userData);
		$firstTimeGuestUser=1;
	}
	return [$user->id,$firstTimeGuestUser];
    } 
    public function isUserEligibleForRefferalDiscount($email){
	$return = true;
	if($email!=""){
		$order = UserOrder::where(['email'=>$email])->get(); 
		if(count($order)>0)$return = false;
		//promocode,promocode_discount
	}
	return $return;
    }
    public function getRefferalDiscountAmount($code){
	$return = [1,10];
	return $return;
    }	 	
    public function placeAOrderByw(Request $request)
    {
        $host_name = config('app.userAgent');
        $userData = json_decode($request->input('user_info'), true);
        $paymentGatewayId = $request->has('payment_gateway_id')?$request->input('payment_gateway_id'):null;
        $userAddress = json_decode($request->input('address'), true);
        $product_type=$request->has('product_type')?$request->input('product_type'):'food_item'; //default food_item
        $orderType = $request->has('order_type')?$request->input('order_type'):"";
        $userMyBag = array();
        $userPayCustId = "";
        $stripeCardId = "";
        $coupon_code='';
        $coupon_discount=0;
        $ms = microtime(true);
        $userMarkedNewFlag = false; // RESERVATION MODULE
        $bearerToken = $request->bearerToken();
        CommonFunctions::$langId = $langId = config('app.language')['id'];
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if($userAuth) {
            $locationId   = $request->header('X-location');
            $restaurantId = $request->header('X-restaurant');
            $restInfo = CommonFunctions::getRestaurantDetails($restaurantId, false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','support_from'));
            $userId       = $userAuth->user_id;
            $userAuthId   = $userAuth->id;
            $email = $userData['email'];
	    $surname = isset($userData['surname'])?$userData['surname']:null;	
            $fname = $userData['fname'];
            $lname = $userData['lname'];
            $phone = $userData['phone'];
            $address1 = $userAddress['address1'];
            $address2 = $userAddress['address2'];
            $city = $userAddress['city'];
	        $state = isset($userAddress['state'])?$userAddress['state']:'';	
            $zipcode = $userAddress['zipcode'];
            $lat = $userAddress['lat'];
            $lng = $userAddress['lng'];
	    $firstTimeGuestUser = 0;
            if ($bearerToken) {
                $userId       = $userAuth->user_id;
                $userInfo      = User::find($userId);
                if(strlen($userInfo['mobile'])==0){
                    $userphonedata['phone'] = (strlen($userInfo['phone'])==0)?$phone:$userInfo['phone'];
                    $userphonedata['mobile'] = (strlen($userInfo['mobile'])==0)?$phone:$userInfo['mobile'];
                    $userInfo->update($userphonedata);
                }
                //$userPayCustId = $userInfo['stripe_customer_id'];
		        $userPayCustIdInfo=UserPaymentToken::where(['user_id' => $userId,'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$restaurantId])->get()->toArray();
		        if(isset($userPayCustIdInfo[0]))$userPayCustId=$userPayCustIdInfo[0]['token_id'];
                $is_guest = 0;
                # Commented above on 23-07-2018 Because When Guest User signin at checkout screen then cart not found
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId,'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
            } else {
                // GUEST
                $is_guest = 1;
                //$address_id = $userAddress['address_id'];
		$userData1 = array_merge($userData,$userAddress); 
		unset($userData1['address_id']);unset($userData1['lat']);unset($userData1['lng']);
                $guestuserInf = self::registerUser($userData1,$restaurantId);
		$userId = $guestuserInf[0];
		$firstTimeGuestUser = $guestuserInf[1];
		
                $userInfo      = User::find($userId);
                $userMyBag = UserMyBag::where(['user_auth_id' => $userAuthId, 'parent_restaurant_id' => $restaurantId, 'restaurant_id'=>$locationId, 'status' => 0])->get();
                $email_id = $email;
                $mobile_no = $phone;
                $userObj = User::where('restaurant_id', $restaurantId)
                    ->where(function ($query) use ($email_id,$mobile_no){
                        $query->where('email',$email_id)
                            ->orWhere('mobile', $mobile_no);
                    })->first();
                if(empty($userObj)) {
                    #PE-2341 AS discussed we will not register the user RG 23-11-2018 commented
                }else {
                    $userId     = $userObj->id;
                    #commnted on 23-10-2018 As per Rahul Prabhakar Jira Bug: PE-2341
                }
            }
            $customerName  = $surname.' '.$fname . ' ' . $lname;
            $customerEmail = $email;
            $cardInfo = $request->input('card_info');
            $cardInfoData  = array();
            $cardInfoData = json_decode(base64_decode($cardInfo));
            $i           = 0;
            $orderAmount = 0;
            $totalQuant  = 0;
            $orderId     = 0;
            $bypFlag     = false;
            // find the total quantity and total amount
            if ($userMyBag->toArray()) {
                /* Expired OR time */
                $orderType = $request->input('order_type') ?? 'delivery';
                $time = new TimeslotController;
                if($orderType == 'delivery') {
                    // Get delivery slot
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'delivery',  $request->input('delivery_date'));
                }else {
                    $get_time_slot = $time->validateRestaurantTime($locationId, 'carryout', $request->input('delivery_date'));
                }
                $selected_time =  $request->input('delivery_time');
                //if(!$request->has('product_type')) {
                if($product_type=='food_item') {
                    if($get_time_slot['status']) {
                        if($get_time_slot['slot']) {
                            $all_timeslots = array_column($get_time_slot['slot'], 'key');
                            if(!in_array($selected_time, $all_timeslots)) {
                                $status = Config('constants.status_code.BAD_REQUEST');
                                $me     = microtime(true) - $ms;
                                return response()->json(['data' => [], 'error' => 'Timeslot you has chosen has been expired.', 'xtime' => $me], $status);
                            }
                        }else {
                            $status = Config('constants.status_code.BAD_REQUEST');
                            $me     = microtime(true) - $ms;
                            return response()->json(['data' => [], 'error' => 'Invalid timeslot OR time has expired.', 'xtime' => $me], $status);
                        }
                    }else {
                        $status = Config('constants.status_code.BAD_REQUEST');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [], 'error' => $get_time_slot['error'], 'xtime' => $me], $status);
                    }
                }
                /* Expired OR time */
                $restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                $cityID = $restaurantData->city_id;
                $taxPercent=0;
                $is_pot=0;
                $pizzas_amount = 0;
                foreach ($userMyBag as $item) {
                    if(!empty($item['coupon_code'])){
                        $coupon_code=$item['coupon_code'];
                    }
                    $customization_price = 0;
                    $gift_wrapping_fees=0;
                    if($is_pot==0 && $item['is_pot']==1) {
                        $is_pot=1;
                    }
                    if($item['is_byp']) {
                        $bypData     = json_decode($item['menu_json'], true);
                        $userMyBag[$i]['item'] = $bypData['label'];
                        $userMyBag[$i]['image'] = $item['image'];
                    } elseif($item['menu_id']>0) {
                        $menuItem = NewMenuItems::where('id', $item['menu_id'])->first();
                        $userMyBag[$i]['item'] = $menuItem->name;
			//$userMyBag[$i]['pos_id'] = $menuItem->pos_id;
                        $userMyBag[$i]['product_type'] = $menuItem->product_type;
                        $dataImg = array();
                        $userMyBag[$i]['image'] = json_encode($dataImg);
                        //@10-09-2018 by RG Calculate Addons-price in case of Normal Customisation Items
                    }
                    if (isset($item['menu_json'])) {
                        // TO REMOVE JSON DECODE AND ENCODE FROM HERE
                        $userMyBag[$i]['menu_json'] = json_decode($item['menu_json'], true);
                        $bypFlag = true;
                    }
                    if(isset($menuItem) && Delivery::getPizzaItemForDealV2($item['menu_id']) && $item['order_type']=="carryout" && Delivery::$deal_on_takeout_only_fifty_percent==1){
                         $pizzas_amount += $item['unit_price'] * $item['quantity'];
                         $coupon_code='';  
                    }
                    if(isset($menuItem) && $menuItem->product_type=='gift_card' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        //$gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $gift_wrapping_fees=0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else if(isset($menuItem) && $menuItem->product_type=='product' && !empty($item->item_other_info)) {
                        $gift_wrapping_feesjson=json_decode($item->item_other_info,true);
                        $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?  $menuItem->gift_wrapping_fees :0;
                        $orderAmount += ($item['quantity'] * ($item['unit_price']+$gift_wrapping_fees)) + $customization_price;
                        $taxPercent = 0;
                    }else{
                        // $orderAmount += ($item['quantity'] * $item['unit_price']) + $customization_price;
                        $orderAmount +=$item['total_menu_amount'];
                    }
                    $totalQuant  += $item['quantity'];
                    $i++;
                }
                $tipAmount      = 0;
                $tipPercent      = 0;
                $tax            = 0;
                $service_tax   = 0;
                $deliveryCharge   = 0;
                $additional_charge   = 0;
                $flat_discount   = 0;
                $coupon_discount_amount=0;
                $voucher_applied=0;
		$referral_applied = $no_of_referral_applied = 0;
		$total_refferal_item_quantity= 0;

                $tipAmount=(float)$request->input('tip_amount') ?? 0;

                if(!empty($coupon_code) && !Delivery::getPizzaItemForDeal($locationId,$userAuthId)){
                    $coupan_applied=Delivery::validateCoupan('order',$request,$coupon_code,$userAuthId,$orderAmount);
                    if(!empty($coupan_applied)&& count($coupan_applied) && isset($coupan_applied['promocode_discount'])){
                        $voucher_applied=1;
                         $coupon_discount_amount=$coupan_applied['promocode_discount'];
                         $coupon_discount= $coupan_applied['promocode_discount'];
                    }
                }
                if($this->isUserEligibleForRefferalDiscount($email) && $is_guest != 1 && $voucher_applied!=1){
		    $coupon_code = $userInfo->referral_code;	
		    //referral_restaurant_id	
		    if($coupon_code!=""){
			 $referral_applied=1; 
			 $ref_amount = $this->getRefferalDiscountAmount($coupon_code);
			 $coupon_discount_amount = $ref_amount[0];
                         $referal_discount =   $ref_amount[0];	
			 $no_of_referral_applied = $ref_amount[1];
		    }
                     
                }

                $delivery_data=Delivery::getDelivery('order',$request,$locationId,$orderType,$totalQuant,$orderAmount,0,$coupon_discount_amount,$pizzas_amount);
                $additional_charge=$service_tax=$flat_discount=0;
                $additional_charge_name='';
                $minimum_delivery_amount=0;
                $delivery_charge_amnt=0;
                $is_greater_freedelivery=1;
                if(!empty($delivery_data) && is_array($delivery_data)){
                    $tax=$delivery_data['tax'];
                    $service_tax=$delivery_data['service_tax'];
                    $deliveryCharge=$delivery_data['delivery_charge'];
                    $additional_charge=$delivery_data['additional_charge'];
                    $additional_charge_name=$delivery_data['additional_charge_name'];
                    $tipAmount=$delivery_data['tip_amount'];
                    $flat_discount=$delivery_data['flat_discount'];
                    $minimum_delivery_amount=$delivery_data['minimum_delivery'];
                    $is_greater_freedelivery=$delivery_data['is_greater_freedelivery'];
                    $delivery_charge_amnt=$deliveryCharge;
                   
                }else{
                    $delivery_data='';
                }

                if($voucher_applied) {

                    $minimum_delivery_amount=$coupan_applied['minimum_delivery'];
                    $validateAmountForCoupon=$orderAmount-$flat_discount;

                    if($coupan_applied['promocode_discount']>$validateAmountForCoupon){
                        $coupon_discount = $validateAmountForCoupon;
                    }

                    /*********Static deliver charge for particular coupon*************/
                    //if((Delivery::$static_pizza_coupon_free==$coupon_code || Delivery::$static_pizza_coupon==$coupon_code || Delivery::$static_pizza_coupon2==$coupon_code)  && $delivery_charge_amnt!=0){
                    if((Delivery::$static_pizza_coupon_free==$coupon_code || Delivery::$static_pizza_coupon==$coupon_code || Delivery::$static_pizza_coupon2==$coupon_code)  && $is_greater_freedelivery==0){
                        $deliveryCharge = Delivery::$static_pizza_coupon_delivery;
                    }
                    /*else if((Delivery::$static_pizza_coupon_free==$coupon_code || Delivery::$static_pizza_coupon==$coupon_code || Delivery::$static_pizza_coupon2==$coupon_code)){
                        $deliveryCharge = $delivery_charge_amnt;
                    }*/
                }
		

                if ($minimum_delivery_amount > $orderAmount && $request->input('order_type') == 'delivery') {
                    return response()->json(['data' => null, 'error' => 'Minimum delivery amount is ' . $minimum_delivery_amount], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send(); die();
                }

                $delivery_date =  $request->input('delivery_date');
                $is_asap_order = 0;$order_state=2;
                if($request->has('is_asap') && $request->input('is_asap') && $request->has('asap_gap') && $request->input('asap_gap')) {
                    //get restaurant_current_time @16-08-2018 By RG
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locationId));
                    if(is_object($currentDateTimeObj)) {
                        $selected_time = $currentDateTimeObj->format('H:i');
                    }else {
                        $selected_time = date('H:i');
                    }
                    $delivery_datetime = strtotime($delivery_date. ' '.$selected_time) + $request->input('asap_gap');
                    $delivery_date = date('Y-m-d', $delivery_datetime);
                    $selected_time = date('H:i',  $delivery_datetime);
                    $is_asap_order = 1;$order_state=1;
                }
                $ordersData     = [
                    'parent_restaurant_id'   => $restaurantId,
                    'is_guest'        => $is_guest,
                    //'location_id'     => $locationId,
                    'restaurant_id'     => $locationId,
                    'is_pot'           =>$is_pot,
                    'user_id'         => $userId,
                    'user_auth_id'    => $userAuthId,
                    'user_ip'         => $_SERVER['REMOTE_ADDR'],
                    'fname'           => $fname,
                    'lname'           => $lname,
		    'surname'        => $surname,
                    'email'           => $email,
                    'address'         => $address1,
                    'address2'        => $address2,
                    'city'            => $city,
                    'state'            =>$state,
                    'zipcode'         => $zipcode,
                    'latitude'        => $lat,
                    'longitude'       => $lng,
                    'phone'           => $phone,
                    'order_type'      => $request->input('order_type') ?? 'delivery',
                    'order_amount'    => $orderAmount,                                         #post
                    'tax'             => $tax,                                                 #post
                    'service_tax'     => $service_tax,                                                 #post
                    'flat_discount'     => $flat_discount,                                                 #post
                    'additional_charge'     =>$additional_charge,                                                 #post
                    'additional_charge_name'     => $additional_charge_name,                                                 #post
                    'tip_amount'      => $tipAmount,                                           #post
                    'tip_percent'     => $tipPercent,                                          #post
                    'delivery_charge' => $deliveryCharge,                                      #post
                    'total_amount'    =>max((($orderAmount + $tax +$service_tax+ $tipAmount + $deliveryCharge+$additional_charge)-$flat_discount-$coupon_discount),0),   #post
                    'delivery_time'   => $selected_time,                #post
                    'delivery_date'   => $delivery_date,
                    'user_comments'   => $request->input('instructions'),                 #post
                    'status'          => 'pending',
                    'product_type'   =>  $product_type,
                    'is_asap_order'   =>  $is_asap_order,
                    'host_name'       =>  $host_name,                                     #post
                ];
                if(!empty($coupon_code) && !empty($coupon_discount) && !Delivery::getPizzaItemForDeal($locationId,$userAuthId)){
                    $ordersData['promocode_discount']=$coupon_discount;
                    $ordersData['promocode']=$coupon_code;
                }
                //print_r($ordersData); die;
                #RG By transaction ID @24-07-2018
                $randStr = md5(microtime());
                $txn_id = strtoupper(substr($randStr, -10));// last 10 Digit
                $userOrder = UserOrder::create($ordersData);   
		$orderStatus = new OrderStatuses();
		$orderStatus->order_id = $userOrder->id;
		$orderStatus->status = 'pending';
		$orderStatus->user_id = $userId;
		$orderStatus->restaurant_id =$locationId;
		//$orderStatus->created_at = now();
		$orderStatus->save();          
                $orderId   =  $userOrder->id;
		//print_r($ordersData);
                if ($orderId > 0) {
                    $bagItemsArr  = [];
                    $bagItemIdArr = [];
                    $addonsData = [];
                    $orderDetailsData = [];
                    foreach ($userMyBag as $bgItem) {
                        $is_pot_item=$bgItem->is_pot;
                        $orderDetailsData = array();
                        $gift_wrapping_feesjson = NULL;
                        $menuItem = NewMenuItems::where('id', $bgItem->menu_id)->first();
			$unit_price=$bgItem->unit_price;
			$total_menu_amount = $bgItem->total_menu_amount;
                        if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' ) && !empty($bgItem->item_other_info)) {
                            $gift_wrapping_feesjson=json_decode($bgItem->item_other_info,true);
                            if($menuItem->product_type=='product'){
                                $gift_wrapping_fees=(isset($gift_wrapping_feesjson['is_gift_wrapping']) && $gift_wrapping_feesjson['is_gift_wrapping']==1)?$menuItem->gift_wrapping_fees :0;
                                $gift_wrapping_feesjson['gift_wrapping_fees']=$gift_wrapping_fees;
                            }
                            $gift_wrapping_feesjson=json_encode( $gift_wrapping_feesjson);
                        }
			if($referral_applied==1 && $no_of_referral_applied>0){
				$unit_price = ($bgItem->unit_price - $referal_discount);
				//$total_menu_amount = ($bgItem->total_menu_amount - $referal_discount);
				$no_of_referral_applied = $no_of_referral_applied - 1;
				$coupon_discount = $coupon_discount + ($referal_discount * $bgItem->quantity);
				if($coupon_discount>$ref_amount[1])$coupon_discount = $ref_amount[1];
					
			}else{
				$total_refferal_item_quantity = $total_refferal_item_quantity + $bgItem->quantity;
			}
                        $orderDetailsData = [
                            'user_order_id'       => $orderId,
                            'item'                => $bgItem->item,
                            'item_size'           => $bgItem->size,
                            'menu_id'             => $bgItem->menu_id,
                            'is_byp'              => $bgItem->is_byp,
                            'is_pot'              => $bgItem->is_pot,
                            'menu_json'           => json_encode($bgItem->menu_json),
			    'pos_data'           =>  $bgItem->pos_data,
                            'image'               => $bgItem->image,
                            'special_instruction' => $bgItem->special_instruction,
                            'quantity'            => $bgItem->quantity,
                            'unit_price'          => $unit_price,
			    'pos_id'   		  => $bgItem->pos_id,
                            'total_item_amt'      => ($total_menu_amount - $coupon_discount),
                            'item_other_info'     => $gift_wrapping_feesjson,
                            'status'              => 1,
                        ];
                        $bagItemIdArr[] = $bgItem->id;
                        //array_push($bagItemsArr, $orderDetailsData);
                        // Create order details from MyBag
			//print_r($orderDetailsData); die;
                        $UserOrderDetail = UserOrderDetail::create($orderDetailsData);
                        $user_order_detail_id =   $UserOrderDetail->id;
                        $addons_data = [];
                        if($is_pot_item) {
                            $addon_options= MybagOptions::where('bag_id',$bgItem->id)->get();
                            if( $addon_options){
                                foreach ($addon_options as $addon_menu) {
                                    $mybag_option_data=[
                                        'option_name'=>$addon_menu->option_name,
                                        'option_id'=>$addon_menu->option_id,
                                        'price'=>$addon_menu->price,
                                        'quantity'=>$addon_menu->quantity,
                                        'user_order_detail_id'=>$user_order_detail_id,
                                        'total_amount'=>$addon_menu->total_amount,
                                    ];
                                    $addonsData[$bgItem->id] = $mybag_option_data;
                                    OrderDetailOptions::create($mybag_option_data);
                                }
                            }
                        }else if (!is_null($bgItem->menu_json)) {
                            /*Addons code*/
                            $addons_data =  CommonFunctions::changeMyBagJsonData($bgItem->is_byp, $bgItem->menu_json);
                            $addonsData[$bgItem->id] = $addons_data;
                        }
                        /*End code*/
                        //$addonsData[$bgItem->id] = $addons_data;
                    }
                    $transaction_id  = $cardNum = $cardType = "";
                    $tot_amount = $ordersData['total_amount'];
                    $total_paid_amount = $tot_amount;
                    $tot_amount = number_format($tot_amount, 2, '.', '') * 100 ;
                    #$tot_amount = (integer)($tot_amount * 100);
                    $transaction_id  = $cardNum = $cardType = "";
                    //$cardNumber = str_replace(' ', '', $cardInfoData->card);
                    //chitrasen
                    $data = [
                        'description' => "Online Customer",
			'name' => $customerName,
                        'email'       => $customerEmail,
                        'first_name' =>$fname,
                        'last_name'=> $lname,
                        'phone'=> $phone,
			            'address'=> ["line1"=>$address1.", ".$address2,"city"=>$city,"postal_code"=>$zipcode,'state'=>$state],
                        //'source'      => 'tok_visa',
                    ];
                    $paymentResponse = $this->initiatePaymentGateway($paymentGatewayId,$userId,$userPayCustId,$data,$cardInfoData,$tot_amount,$total_paid_amount,$txn_id,$restInfo,$restaurantData,$ms);
                    //$result2 = $paymentResponse["result2"];
                    $orderLog = ['payment_gateway_order_id'=>$paymentResponse["transaction_id"],
                        'restaurant_id'=>$locationId,
                        'order_id'=>$userOrder->id,
                        'payment_gateway_id'=>$paymentResponse["payment_gateway_id"],
                        'order_status'=>$paymentResponse["transaction_id"],
                        'platform'=>'api',
                        'cms_user_id'=>0,
                        'request_data'=>isset($paymentResponse["request"])?json_encode($paymentResponse["request"]):'',
                        'response_data'=>isset($paymentResponse["response"])?json_encode($paymentResponse["response"]):''
                    ];
                    PaymentGatewayLog::create($orderLog);//die;
                    //print_r($paymentResponse);die;
                    if($paymentResponse["errortype"]==1){
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => null, 'error' => $paymentResponse["result2"]['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    }
                    $userOrder                   = UserOrder::find($userOrder->id);
                    $userOrder->status           = 'placed';
                    $userOrder->stripe_charge_id = $paymentResponse["transaction_id"];
                    $userOrder->stripe_card_id   = $paymentResponse["CardId"];
                    $userOrder->card_number = 'XXXX-XXXX-XXXX-'.$paymentResponse["cardNum"];
                    $userOrder->card_type = $paymentResponse["cardType"];
                    $userOrder->expired_on = "XXXX/XX";
                    $userOrder->payment_receipt = $txn_id;
                    $userOrder->payment_receipt = $txn_id;
                    $userOrder->order_state = $order_state;
                    $userOrder->restaurant_name = $restaurantData->restaurant_name;
		    //$userOrder->total_amount = $ordersData['total_amount'];
		    if($referral_applied==1){
			$userOrder->total_amount  = ($userOrder->total_amount - $coupon_discount);	
		    	$userOrder->promocode = $coupon_code;
			$userOrder->promocode_discount = $coupon_discount;	
		    } 
                    if($userOrder->card_type=='No Payment Required')
                    $userOrder->total_amount = 0;
                    if ($userOrder->save()) {
			// restaurnat bonus for 
			 if(!$this->isUserEligibleForRefferalDiscount($email) && $userInfo->referral_restaurant_id>0){
			       	 $ref_amount = $this->getRefferalDiscountAmount($coupon_code);
				 $total_refferal_item_quantity = $total_refferal_item_quantity*$ref_amount[0];
				 $total_refferal_item_quantity = ($total_refferal_item_quantity>10)?10:$total_refferal_item_quantity; 
				 if($total_refferal_item_quantity>0){
				 	FinancialRecord::create(["restaurant_id"=>$userInfo->referral_restaurant_id,
			"ma_order_id"=>$userOrder->id,"order_id"=>$userOrder->payment_receipt,"amount"=>$total_refferal_item_quantity,"order_type"=>'Referral',"payment_type"=>"Credit","status"=>1]);
					//$ref_amount[1] = $ref_amount[1] - 1;
				 }
			    }
		             
		         
                        if($product_type=='gift_card') {
                            $this->giftCardGenerate($orderId);
                        }
                        // Replace stripe txn ID by Custom txn id
                        $userOrder->stripe_charge_id = $txn_id;
                       
                        $localization_id = $request->header('X-localization');
                        $language_id =  CommonFunctions::getLanguageInfo($localization_id);
                        CommonFunctions::$langId = $langId = $language_id->id;
                        //$restaurantData = Restaurant::where(array('id'=>$locationId))->first();
                        // Restaurant currency symbol
                        $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
                        if($restData) {
                            $curSymbol = $restData->currency_symbol;
                            $curCode = $restData->currency_code;
                        } else {
                            $curSymbol = config('constants.currency');
                            $curCode = config('constants.currency_code');
                        }
                        $orderItem = $label = "";
                        $productType = "no";
                        if($product_type !='gift_card') {
                            foreach ($userMyBag as $bgItem){
                                $label = "";
                                $addon_modifier=CommonFunctions::getAddonsAndModifiers($bgItem,'bag');
                                $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                $instruct='';
                                $orderItem .=  '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>    
                                <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <b>'.$bgItem->quantity.' '.$bgItem->item.'</b>';
                                $result = isset($addonsData[$bgItem->id]) ? $addonsData[$bgItem->id] : null;
                                $menuItem = NewMenuItems::where('id', $bgItem->menu_id)->first();
                                if(isset($menuItem) && ($menuItem->product_type=='product' || $menuItem->product_type=='gift_card' )) {
                                    $productType = "yes";
                                    if(!is_null( $bgItem->item_other_info)) {
                                      /*  if (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) {
                                            $label = $bgItem->size . ' ';
                                        } else {
                                            $label = "";
                                        }*/
                                        $item_other_info = json_decode($bgItem->item_other_info);
                                        $value3 = '';
                                        $value = (isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?'Yes':'No';
                                        //$value2 = 'Gift Wrapping: ' . $value;
                                        $value2 =($menuItem->product_type=='product' )?'Gift Wrapping: ' . $value:'';
                                        if(isset($item_other_info->gift_wrapping_message) && !empty($item_other_info->gift_wrapping_message) && $item_other_info->gift_wrapping_message!='null')
                                        {
                                            $value3 = 'Message: ' .$item_other_info->gift_wrapping_message;
                                        }
                                        $gift_wrapping_fees=(isset($item_other_info->is_gift_wrapping) && $item_other_info->is_gift_wrapping==1)?$menuItem->gift_wrapping_fees :0;
                                        if($gift_wrapping_fees!=0) {
                                            $label .= $value2 . '<br/>' . $value3.'<br/>Price: $'.$bgItem->quantity * $gift_wrapping_fees;
                                        }
                                        else {
                                            $label .= $value2 . '<br/>' . $value3;
                                        }
                                        if($bgItem->is_byp){
                                            $label='';
                                            $label =  CommonFunctions::getOldAddons($label, $bgItem->is_byp, $result);
                                        }else{
                                            $label .= (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$bgItem->size.'</td></tr>' .' ' : '';
                                            $label .= $addon_modifier_html;
                                        }
                                    }
                                } else {
                                    //$label = (strtolower($bgItem->size) != 'na' || $bgItem->size != 'full') ? $bgItem->size .' ' : '';
                                   /* if(strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size))
                                    {
                                        $label = $bgItem->size.' ';
                                    }
                                    else
                                    {
                                        $label = "";
                                    }*/
                                    if($is_pot) {
                                    }else if($bgItem->is_byp==1 && $result){
                                        /*foreach($result as $key=>$data){
                                            if($key!="")
                                                $label .= $key.', ';
                                            if($data!="")
                                                $label .= '['.$data.']>';
                                        }*/
                                    }else if($bgItem->is_byp==0 && $result) {
                                        /*foreach ($result as $data) {
                                            if($data['addons_name']!=""){
                                                $label .= $data['addons_name'].', ';
                                            }
                                            if($data['addons_option']!="")
                                                $label .= '[' . $data['addons_option'] . ']>';
                                        }*/
                                    }
                                    if(!empty($bgItem->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$bgItem->special_instruction.'</p>';
                                    }
                                    if($bgItem->is_byp){
                                        $label='';
                                        $label =  CommonFunctions::getOldAddons($label, $bgItem->is_byp, $result);
                                    }else{
                                        $label .= (strtolower($bgItem->size) != 'null' && $bgItem->size != "NA" && !empty($bgItem->size) && !is_null($bgItem->size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$bgItem->size.'</td></tr>' .' ' : '';
                                        $label .= $addon_modifier_html;
                                    }
                                }
                                $orderItem .= '<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$bgItem->total_menu_amount.'</p></th></tr></table></th></tr></tbody></table>
                                <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                            }
                        }
                        /*else {
                            $orderItem = CommonFunctions::getProductList($userOrder->id);
                        }*/
			$oDetail = new UserOrderDetail();
	    		$oDetailData = $oDetail->getOrderDetail($userOrder->id);
                        $timestamp = strtotime($userOrder->delivery_date. ' '.$userOrder->delivery_time.':00');
                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => $userOrder->created_at)));
			if($userOrder->order_type=='delivery'){
		                $address = $userOrder->address;
		                if ($userOrder->address2) {
		                    $address = $address . ' ' . $userOrder->address2;
		                }
		                $address = $address . ' ' . $userOrder->city . ' ' . $userOrder->state_code . ' ' . $userOrder->zipcode;
			}else{
				$address = $restaurantData->address;
				if ($restaurantData->address2) {
		                    $address = $address . ' ' . $restaurantData->address2;
		                }
				$cityInfo = City::where('id', $restaurantData->city_id)->first();
		                $address = $address . ' '.$cityInfo->city_name. ' '.$cityInfo->state_code.' ' . $restaurantData->zipcode;
				 
			}
                        if($userOrder->card_type=='Cash' || $userOrder->card_type=='No Payment Required'){
                            $cardInfo = "<span style='color: #000;'>".$userOrder->card_type."</span>";
                            $cardInfoTxt = '';
                        }else{
                            $cardInfo = "<span style='color: #000;'>".$userOrder->card_type."</span>".' ('.$userOrder->card_number.')';
                            $cardInfoTxt = '<b>Credit Card:</b>';
                        }
                        if($is_asap_order) {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp). '(Possibly sooner!)';
                        }else {
                            $odr_delivery_time = date('l d M, h:i A', $timestamp);
                        }
                        // RESERVATION MODULE
                        // if user category hasn't been marked yet, means REPEAT user
                        $user_update = User::where('restaurant_id', $restaurantId)->where('id', $userId)->where('category', config('constants.guest_category.New'))->first();
                        if ($user_update && !$userMarkedNewFlag) {
                            $user_update->category = ($is_guest!=1)?config('constants.guest_category.Repeat'):config('constants.guest_category.New');
                            $user_update->save();
                        }
                        $mailKeywords = array(
                            'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                            'URL' => $restaurantData->source_url,
                            'ORDER_NUMBER' => $userOrder->payment_receipt,
                            'USER_NAME' => ucfirst($customerName),
                            'USER_EMAIL' => $userOrder->email,
                            'ORDER_TYPE' => ucfirst($userOrder->order_type),
                            'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                            'ORDER_DELIVERY_TIME' => $odr_delivery_time,
                            'DELIVERY_ADD' => $address,
                            'PHONE' => $userOrder->phone,
                             
                            'SITE_URL' => $restaurantData->source_url,
                            'ADMIN_URL' => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER' => $cardInfo,
                            'cardInfoTxt'=> $cardInfoTxt,
                            'title' => "Order Place",
			    'DELIVERY_ADD' => $address,
                            'PHONE' => $userOrder->phone,
                            'SUB_TOTAL' => $curSymbol.$userOrder->order_amount,
                            'TOT_AMOUNT' => $curSymbol.$userOrder->total_amount,
                            'ITEM_DETAIL' => $orderItem,
                            'SITE_URL' => $restaurantData->source_url,
                            'ADMIN_URL' => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER' => $cardInfo,
			    'USER_ADDRESS'	   =>$address,
			    'ITEM_DETAIL'         =>  $this->getItemHtmlTemplate($oDetailData),
			     'ORDER_DETAILS'      => $orderItem, 	
                            'cardInfoTxt'=> $cardInfoTxt,
				'SUB_TOTAL'           =>  $userOrder->order_amount,
                            'TOT_AMOUNT'          =>  $userOrder->total_amount,
                            'DELIVERY_CHARGE'	  =>  $userOrder->delivery_charge,
                            'GRAND_TOTAL'	      =>  $userOrder->total_amount,
                            'TIP'	 	          =>  $userOrder->tip_amount,
                            'TAX'	 	          =>  $userOrder->tax,		
                        );
                        /*************************************************************************************************/
                        $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$userOrder,$curSymbol);
                        
                        if($userOrder->user_comments!=""){
                            $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions</b> <br><i>'.$userOrder->user_comments.'</i><br><br>';
                        }
                        else {
                            $mailKeywords['SPECIAL_INFO'] = '';
                        }
                      
                        if($userOrder->order_type=="delivery") {
                            //$mailKeywords['DELIVERY_CHARGE'] = $curSymbol.$userOrder->delivery_charge;
                            if($product_type == "product"){
                                $mailTemplate = 'place_order_product_delivery';
                            }elseif($product_type == "food_item"){
                                $mailTemplate = 'place_order_delivery';
                            }elseif($product_type == "gift_card"){
                                $mailTemplate = 'giftcard_order_placed_restaurant';
                            }
                        } else {
                            if($productType == "yes")
                                $mailTemplate = 'place_order_product_carryout';
                            else
                                $mailTemplate = 'place_order_carryout';
                        }
                        $header = "header_layout_restaurant";
                        $footer = "footer_layout_restaurant";
                        if($product_type == "gift_card") {
                            $mailData['subject'] = "Someone Bought $".intval($userOrder->total_amount)." Worth  of  Gift  Cards!";
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'manager');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                        }else {
                            $mailData['subject'] = "You've Got a New ".ucfirst($userOrder->order_type)." Order from Munch Ado!";
                        }
                        $mailTemplate = CommonFunctions::mailTemplate($mailTemplate, $mailKeywords, $header, $footer, $restaurantId);
                        $mailData['body'] = $mailTemplate;
                        //$mailData['receiver_email'] = $customerEmail;
                        $restaurantEmail = $restaurantData->email;
                        
                        $mailData['receiver_email'] = $restaurantEmail;
                        //$mailData['receiver_name'] = $customerName;
                        $mailData['receiver_name'] = $restaurantData->restaurant_name;
                        $mailData['MAIL_FROM_NAME'] = 'Believers';
                        $mailData['MAIL_FROM'] = $restInfo['support_from'];
			$mailData['subject'] = 'You have a new order!';
			
                        CommonFunctions::sendMail($mailData);
                        // USER EMAIL STARTS >>>
                        $userMailTemplate = 'place_order_success_user';//:'place_order_success_takeout_user'; // place_order_delivery
                        $header = "header_layout";
                        $footer = "footer_layout";   
			 $prestaurantData = Restaurant::where(array('id'=>$restaurantData->parent_restaurant_id))->first();
                          $userMailkeywords = [
                            'RESTAURANT_NAME'     => $prestaurantData->restaurant_name,
                            'URL'                 => $prestaurantData->source_url,
                            'ORDER_NUMBER'        => $userOrder->payment_receipt,
                            'USER_NAME'           => ucfirst($customerName),
                            'USER_EMAIL'          => $userOrder->email,
                            'ORDER_TYPE'          => ($userOrder->order_type=="delivery")?ucfirst($userOrder->order_type):'Takeout',
                            'ORDER_TIME'          => date('l d M, h:i A', $timestamp2),
                            'ORDER_DELIVERY_TIME' => $odr_delivery_time,
                            'DELIVERY_ADD'        => $address ,
			     'USER_ADDRESS'	   =>$address,
                            'PHONE'               => $userOrder->phone,
                            'SUB_TOTAL'           =>  $userOrder->order_amount,
                            'TOT_AMOUNT'          =>  $userOrder->total_amount,
                            'DELIVERY_CHARGE'	  =>  $userOrder->delivery_charge,
                            'GRAND_TOTAL'	      =>  $userOrder->total_amount,
                            'TIP'	 	          =>  $userOrder->tip_amount,
                            'TAX'	 	          =>  $userOrder->tax,
                            'ITEM_DETAIL'         =>  $this->getItemHtmlTemplate($oDetailData),
			                 'ORDER_DETAILS'      => $orderItem, 
                            'SITE_URL'            => $restaurantData->source_url,
                            'ADMIN_URL'           => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER'         => $cardInfo,
                            'cardInfoTxt'         => $cardInfoTxt,
                            'title'               => "Order Place",
                            'SPECIAL_INFO'        => '<b>Special Instructions:</b> <br><i>'.$userOrder->user_comments.'</i>',
                            'INTERVAL_RANGE'      => $restaurantData->kpt,
			   
                            'DELIVERY_ADD' => $address,
                            'PHONE' => $userOrder->phone,
                            'SUB_TOTAL' => $curSymbol.$userOrder->order_amount,
                            'TOT_AMOUNT' => $curSymbol.$userOrder->total_amount,
                            'ITEM_DETAIL' => $orderItem,
                            'SITE_URL' => $restaurantData->source_url,
                            'ADMIN_URL' => $restaurantData->restaurant_image_name,
                            'CARD_NUMBER' => $cardInfo,
                            'cardInfoTxt'=> $cardInfoTxt,	
                        ];
                        $userMailkeywords               = CommonFunctions::getAllChargesForMailer($userMailkeywords,$userOrder,$curSymbol);
                        $userMailTemplate               = CommonFunctions::mailTemplate($userMailTemplate, $userMailkeywords, $header, $footer, $restaurantId);
                        //$userMailData['subject']        = "We've Received Your {$restaurantData->restaurant_name} Order!";
			            $userMailData['subject']           = "We're On it!"; 
                        $userMailData['body']           = $userMailTemplate;
                        $userMailData['receiver_email'] = $userOrder->email;
                        $userMailData['receiver_name']  = ucfirst($customerName);
                        $userMailData['MAIL_FROM_NAME'] = $prestaurantData->restaurant_name;
                        $userMailData['MAIL_FROM']      = $restInfo['support_from'];
                           
                        CommonFunctions::sendMail($userMailData);
                        // <<< USER EMAIL ENDS
                        $userOrder['restaurant_name'] = $restaurantData->restaurant_name;
                        $me     = microtime(true) - $ms;
                        $status = 200;
                        #send mail to Buyers whi placed an Gift card ORders
                        if($product_type == "gift_card" && $userOrder->email) {
                            $orderItem = CommonFunctions::getProductList($userOrder->id, 'buyer');
                            $mailKeywords['ITEM_DETAIL'] = $orderItem;
                            $mailKeywords['BUYER_NAME'] = $userOrder->fname;
                            $mailTemplate = CommonFunctions::mailTemplate('giftcard_order_placed_buyer', $mailKeywords, 'header_layout', 'footer_layout', $restaurantId);
                            $mailData['subject'] = "Thank you for your Gift Card Order!";
                            $mailData['body'] = $mailTemplate;
                            $mailData['receiver_email'] = $userOrder->email;
                            
                            $mailData['MAIL_FROM_NAME'] = $restaurantData->restaurant_name;
                            $mailData['MAIL_FROM'] = $restInfo['custom_from'];
                            CommonFunctions::sendMail($mailData);
                        }
                        /******** SMS Send **** @18-10-2018 by RG *****/
                        $sms_keywords = array(
                            'order_id' => $userOrder->id,
                            'sms_module' => 'order',
                            'action' => 'placed',
                            'sms_to' => 'customer'
                        );
                        CommonFunctions::getAndSendSms($sms_keywords);
                        /************ END SMS *************/
                        if($is_pot){
                            MybagOptions::whereIn('bag_id', $bagItemIdArr)->delete();
                        }
                        UserMyBag::whereIn('id', $bagItemIdArr)->delete();
                        $userOrder->tax_delivery=$delivery_data;
                        return response()->json(['data' => $userOrder, 'isStillGuestUser'=> $firstTimeGuestUser ,'error' => '', 'xtime' => $me], $status);
                    } else {
                        $status = Config('constants.status_code.INTERNAL_ERROR');
                        $me     = microtime(true) - $ms;
                        return response()->json(['data' => [],'isStillGuestUser'=> 0, 'error' => 'Order status could not be updated', 'xtime' => $me], $status);
                    }
                } else {
                    $status = Config('constants.status_code.INTERNAL_ERROR');
                    $me     = microtime(true) - $ms;
                    return response()->json(['data' => [],'isStillGuestUser'=> 0, 'error' => 'Order not generated in DB.', 'xtime' => $me], $status);
                }
            } else {
                $me = microtime(true) - $ms;
                $status =Config('constants.status_code.BAD_REQUEST');
                return response()->json(['data' => [],'isStillGuestUser'=> 0, 'error' => 'Sorry, Invalid Request OR there is no items in your Bag.', 'xtime' => $me], $status);
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null,'isStillGuestUser'=> 0, 'error' => $tokenField.' token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    } 
    public function getItemHtmlTemplate($orderDetailData){
	$temp ="";$modifierData = "";
	foreach($orderDetailData as $oData){
		$addon_modifier=json_decode($oData->menu_json,true);
		$modifier_data_records = $addon_modifier['modifier_items'];
		$i=0; $addonDataArrary = null;
		foreach($modifier_data_records as $modifier_data){  
			  $groupname =   "<b>".$modifier_data['group_prompt']."</b>". $modifier_data['modifier_name'];
			  
		 
			$modifierData ='<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; position: relative; text-align: left; vertical-align: top; width: 100%; padding: 0;"> <tbody> <tr style="vertical-align: top; padding: 0;" align="left"> <th class="small-12 large-12 columns first last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 100%; margin: 0 auto; padding: 0 0 8px;" align="left"> <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;"> <tbody> <tr style="vertical-align: top; padding: 0;" align="left"> <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left"> <p class="addon" style="color: #848484; font-family: Helvetica,Arial,sans-serif; font-size: 13px; font-weight: 700; line-height: 1.3; margin: 0; padding: 0;" align="left">'.$groupname.'</p> </th> <th class="expander" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; visibility: hidden; width: 0; margin: 0; padding: 0;" align="left"></th> </tr> </tbody> </table> </th> </tr> </tbody> </table>';
		
		} 
	$temp .='<tr style="vertical-align: top; padding: 0;" align="left">
                                                       <th class="small-6 large-6 columns first" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 282px; margin: 0 auto; padding: 0 4px 8px 8px;" align="left">
                                                          <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                             <tbody>
                                                                <tr style="vertical-align: top; padding: 0;" align="left">
                                                                   <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                                                                      <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                         <tbody>
                                                                            <tr style="vertical-align: top; padding: 0;" align="left">
                                                                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
                                                                            </tr>
                                                                         </tbody>
                                                                      </table>
                                                                      <p class="item" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 700; line-height: 1.3; margin: 0; padding: 0;" align="left">'.$oData->item.'</p>
                                                                      <table class="spacer" style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                         <tbody>
                                                                            <tr style="vertical-align: top; padding: 0;" align="left">
                                                                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 6px; font-weight: 400; hyphens: auto; line-height: 6px; mso-line-height-rule: exactly; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="6px" align="left">&nbsp;</td>
                                                                            </tr>
                                                                         </tbody>
                                                                      </table>
                                                                      <!--Customisation Row Starts-->
                                                                      <table class="row customisation" style="border-collapse: collapse; border-spacing: 0; display: table; position: relative; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                         <tbody>
                                                                            <tr style="vertical-align: top; padding: 0;" align="left">
                                                                               <th class="nopadding small-12 large-12 columns first last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 100%; margin: 0 auto; padding: 0 0 8px;" width="12" align="left">
                                                                                  <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                                     <tbody>
                                                                                        <tr style="vertical-align: top; padding: 0;" align="left">
                                                                                           <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                                                                                              <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;" width="100%" cellspacing="0" cellpadding="0">
                                                                                                 <tbody>
                                                                                                    <tr style="vertical-align: top; padding: 0;" align="left">
                                                                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" align="left">
                                                                                                          <!--addon start-->
										
                                                                                                          '.$modifierData.'
                                                                                                           
                                                                                                          <!--addon end--> 
                                                                                                       </td>
                                                                                                    </tr>
                                                                                                 </tbody>
                                                                                              </table>
                                                                                           </th>
                                                                                        </tr>
                                                                                     </tbody>
                                                                                  </table>
                                                                               </th>
                                                                            </tr>
                                                                         </tbody>
                                                                      </table>
                                                                   </th>
                                                                </tr>
                                                             </tbody>
                                                          </table>
                                                       </th>
						        
                                                       <th class="small-3 large-3 columns" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 137px; margin: 0 auto; padding: 0 4px 8px;" valign="top" align="left">
                                                          <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                             <tbody>
                                                                <tr style="vertical-align: top; padding: 0;" align="left">
                                                                   <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                                                                      <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                         <tbody>
                                                                            <tr style="vertical-align: top; padding: 0;" align="left">
                                                                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
                                                                            </tr>
                                                                         </tbody>
                                                                      </table>
                                                                      <p style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 100; line-height: 1.3; margin: 0 0 auto; padding: 0;" class="item text-center" align="center">'.$oData->quantity.'</p>
                                                                   </th>
                                                                </tr>
                                                             </tbody>
                                                          </table>
                                                       </th>
                                                       <th class="small-3 large-3 columns last" style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; width: 137px; margin: 0 auto; padding: 0 8px 8px 4px;" valign="top" align="left">
                                                          <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                             <tbody>
                                                                <tr style="vertical-align: top; padding: 0;" align="left">
                                                                   <th style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0;" align="left">
                                                                      <table style="border-collapse: collapse; border-spacing: 0; text-align: left; vertical-align: top; width: 100%; padding: 0;">
                                                                         <tbody>
                                                                            <tr style="vertical-align: top; padding: 0;" align="left">
                                                                               <td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 400; hyphens: auto; line-height: 1.3; word-wrap: break-word; margin: 0; padding: 0;" valign="top" height="15" align="left">&nbsp;</td>
                                                                            </tr>
                                                                         </tbody>
                                                                      </table>
                                                                      <p style="color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 14px; font-weight: 700; line-height: 1.3; margin: 0 0 auto; padding: 0;" class="item text-right" align="right">£<span>'.$oData->total_item_amt.'</span></p>
                                                                   </th>
                                                                </tr>
                                                             </tbody>
                                                          </table>
                                                       </th>
                                                    </tr>';
	}
	return $temp;
    } 	
    public function orderStage($status,$orderType){      
        if($status=="confirmed"){
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 1],["key"=>"Sent","value"=>0]):array(["key"=>"Confirmed", "value"=> 1],["key"=>"Ready for Pick Up","value"=>0],["key"=>"Picked Up","value"=>0]);            
        }elseif($status=="arrived" || $status=="delivered" || $status=="ready"){
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed", "value"=> 1],["key"=>"Sent", "value"=>1]):array(["key"=>"Confirmed","value" => 1],["key"=>"Ready for Pick Up","value"=>1],["key"=>"Picked Up","value"=>0]);
        }elseif($status=="archived" || $status=="sent"){    // as discussed with Rajiv
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 1],["key"=>"Sent","value"=>1]):array(["key"=>"Confirmed","value" => 1],["key"=>"Ready for Pick Up","value"=>1],["key"=>"Picked Up","value"=>1]);
        }else{
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 0],["key"=>"Sent","value"=>0]):array(["key"=>"Confirmed","value" => 0],["key"=>"Ready for Pick Up","value"=>0],["key"=>"Picked Up","value"=>0]);
        }
    }
    
    public function getOrderDetailsForList($orderid){
        $orderData = UserOrder::select(
                    'restaurants.id','restaurants.restaurant_name','restaurants.delivery_provider_apikey',
                    'restaurants.kpt','user_orders.id','user_orders.user_id',
                    'user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount',
                    'user_orders.is_order_viewed','user_orders.address','user_orders.address2','user_orders.apt_suite',
                    'user_orders.zipcode','user_orders.created_at','user_orders.delivery_time',
                    'user_orders.delivery_date', 'user_orders.status', 'delivery_time','delivery_date',
                    'restaurants_comments','user_comments','is_reviewed','review_id','payment_receipt','city',
                    'fname','lname','user_orders.email','user_orders.phone',
                    'user_orders.updated_at'
                );
        $orderData = $orderData->where('user_orders.id', $orderid)->where('user_orders.product_type', 'food_item')->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->get();                        
                
        if($orderData) {
            $order = $orderData->toArray();
           
            $orderType = ($order[0]['order_type'] == 'carryout')? 'Takeout': 'Delivery';
            $stageOfOrder = $this->orderStage($order[0]['status'], $orderType);
            
            $deliveryAddress = "";
            if($order['0']['order_type'] == "delivery"){
                $address = [trim($order['0']['address']),trim($order['0']['address2']),trim($order['0']['apt_suite']),trim($order['0']['city']),trim($order['0']['zipcode'])];
                $deliveryAddress  = array_filter($address);
                $deliveryAddress = implode(",", $deliveryAddress);                                    
            }
            
            $customerTotalOrder = CommonFunctions::getCustomerOrders($order[0]);
            
            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $order['0']['restaurant_id']));
            if(is_object($currentDateTimeObj)) {
                $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
            }else {
                $restcurrentDateTime['current_date'] = date('Y-m-d');
                $restcurrentDateTime['current_time'] = date('H:i:00');
            }
            
            $delivery_time_left_hour = explode('.',(strtotime($order['0']['delivery_date'].' '.$order['0']['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
            $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['0']['restaurant_id'], 'datetime' => $order['0']['created_at'])));
            
            $refunded_amount = 0;
            $refund_reason = array();
            $refunded_amount_data = DB::table('refund_histories')->where('order_id', $orderid)->select('amount_refunded', 'reason')->get();
            if( $refunded_amount_data) {
                foreach ($refunded_amount_data as $key => $amount_data) {
                    $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                    $refund_reason[$key] = array('amount' => number_format($amount_data->amount_refunded, 2), 'reason' => $amount_data->reason);
                }
            }
            
            
            $dataList = array(
                    'id' => (string)$order['0']['id'],
                    'fname' =>  $order['0']['fname'],
                    'lname' =>  $order['0']['lname'],
                    'email' =>  $order['0']['email'],
                    'phone' =>  $order['0']['phone'],
                    'delivery_address'=>$deliveryAddress,
                    'is_order_viewed'=>$order['0']['is_order_viewed'],
                    'delivery_time_left_hour' => (int)$delivery_time_left_hour,
                    'delivery_time_left' => 0,
                    'restaurant_id' => (string)$order['0']['restaurant_id'],                                              
                    'restaurant_name' => $order['0']['restaurant_name'],
                    'is_restaurant_exists' => 'Yes',
                    'order_type1' => 'I',
                    'order_type' => $order['0']['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                    'delivery_date' => $order['0']['delivery_date'].' '.date('H:i:00', strtotime($order['0']['delivery_time'])),
                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                    'menu_status' => "",
                    'is_reviewed' => $order['0']['is_reviewed'] ? $order['0']['is_reviewed'] : 0,
                    'review_id' => $order['0']['review_id'] ? $order['0']['review_id'] : 0,
                    'manual_update' => "",
                    'payment_receipt' =>  $order['0']['payment_receipt'],
                    'delivery_service' => $order['0']['delivery_provider_apikey'] ? 1 : 0,
                    'service_provider' => '',
                    'status' => $order['0']['status'],
                    'refunded_amount' => $refunded_amount ? (string) number_format($refunded_amount, 2) : 'NA',
                    'total_amount' => $order['0']['total_amount'],
                    'user_total_order'=>$customerTotalOrder,
                    'item_list' => array(),
                    'order_stage'=>$stageOfOrder,
                    'time_zone'=>"",
                    'kpt'=>$order['0']['kpt'],
                    'updated_at'=>$order['0']['updated_at']
                 );
            return $dataList;
            }else{
                return array();
            }
    }
    private function getPaymentGetwayInfo($restaurant_id,$paymentGatewayId=null){
        $paymentGatewayConfig = null;
        if($paymentGatewayId!=null){
            $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("id", $paymentGatewayId)->where("status", 1)->get()->toArray();
            //print_r($paymentGatewayInfo);die;
            if(isset($paymentGatewayInfo[0])) {
                $paymentGatewayConfig['ID']=$paymentGatewayInfo[0]['id'];
                $config = json_decode($paymentGatewayInfo[0]['config']);
		$paymentGatewayConfig['method'] = $paymentGatewayInfo[0]['payment_method'];
                $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
                foreach ($config as $val1) {
                    foreach ($val1 as $key => $val) {
                        $paymentGatewayConfig[$key] = $val;
                    }
                }
            }else{
                $paymentGatewayConfig['ID'] = 0;
                $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
                $paymentGatewayConfig['GATEWAY'] = "stripe";
		$paymentGatewayConfig['method'] = "card";	
            }
        }else{
            $paymentGatewayInfo = PaymentGateway::select('*')->where("restaurant_id", $restaurant_id)->where("status", 1)->get()->toArray();
            //print_r($paymentGatewayInfo);die;
            if(isset($paymentGatewayInfo[0])) {
                $paymentGatewayConfig['ID']=$paymentGatewayInfo[0]['id'];
                $config = json_decode($paymentGatewayInfo[0]['config']);
                $paymentGatewayConfig['GATEWAY'] = strtolower(trim($paymentGatewayInfo[0]['name']));
		$paymentGatewayConfig['method'] = $paymentGatewayInfo[0]['payment_method'];
                foreach ($config as $val1) {
                    foreach ($val1 as $key => $val) {
                        $paymentGatewayConfig[$key] = $val;
                    }
                }
            }else{
                $paymentGatewayConfig['ID'] = 0;
                $paymentGatewayConfig['STRIPE_SECRET'] = config('constants.payment_gateway.stripe.secret');
                $paymentGatewayConfig['GATEWAY'] = "stripe";
		$paymentGatewayConfig['method'] = "card";	
            }
        }
        return $paymentGatewayConfig;
    }
    private function initiatePaymentGateway($paymentGatewayId,$userId,$userPayCustId,$data,$cardInfoData,$tot_amount,$total_paid_amount,$txn_id,$restInfo,$restaurantData,$ms)
    {
     try{
        $paymentGatewayConfig = null;
        $payment_gateway_id = 0;
        $errortype = 0;
        $transaction_id = null;
        $cardNum = null;
        $cardType = null;
        $stripeCardId = $result2 = $error = null;
        $chargeResponse=$CardId=NULL;
        $paymentGatewayConfig = $this->getPaymentGetwayInfo($restaurantData->parent_restaurant_id,$paymentGatewayId);
        //print_r($paymentGatewayConfig);die;
                  
        if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="paybycash"){
            $payment_gateway_id = $paymentGatewayConfig['ID'];
            $cardType = 'Cash';
            $chargeResponse = null;
            $data = null;
            $errortype = 0;
            $transaction_id = time()."ORDER".$payment_gateway_id."TXN".$txn_id;
            $cardNum = "0000"; 
            $CardId = time();
            $result2 = null;
            $error = null;
            $request = "paybycash - request";
            $response = "paybycash - success";
            $paymentGatewayConfig["payment_gateway"]=$paymentGatewayConfig['method'];
               
        }elseif(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="nopayment"){
            $payment_gateway_id = $paymentGatewayConfig['ID'];
            $cardType = 'No Payment Required';
            $chargeResponse = null;
            $data = null;
            $errortype = 0;
            $transaction_id = time()."ORDER".$payment_gateway_id."TXN".$txn_id;
            $cardNum = "0000"; 
            $CardId = time();
            $result2 = null;
            $error = null;
            $request = "nopayment - request";
            $response = "nopayment - success";
            $paymentGatewayConfig["payment_gateway"]=$paymentGatewayConfig['method'];
        }else if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="stripe"){
            unset($data['first_name']);unset($data['last_name']);unset($data['phone']);
            $payment_gateway_id = $paymentGatewayConfig['ID'];
            if ($userPayCustId == null) {
                $apiObj = new StripeGateway($paymentGatewayConfig);
                $result = $apiObj->createCustomer($data);
                $userPayCustId = $result->id;
		        $userToken = UserPaymentToken::create([
			      'restaurant_id' =>$restaurantData->parent_restaurant_id,
			      'parent_restaurant_id'=>  $restaurantData->parent_restaurant_id,
			      'user_id'  => $userId,
			      'token_id' => $userPayCustId,
			      'card_id'=> $userPayCustId]);
                $cardData = [
                    'number' => str_replace(' ', '', $cardInfoData->card),
                    'exp_month' => $cardInfoData->expiry_month,
                    'exp_year' => $cardInfoData->expiry_year,
                    'cvc' => $cardInfoData->cvv,
                    'name' => $cardInfoData->name,
                ];
		if($cardInfoData->isSave){
		   $ccdata = [
                        'user_id' => $userId,
                        'name' =>$cardInfoData->name,
                        'restaurant_id' =>$restaurantData->parent_restaurant_id,
			'parent_restaurant_id'=> $restaurantData->parent_restaurant_id,
                        'last_four' => substr(str_replace(' ', '', $cardInfoData->card), -4),
                        'type' => $cardInfoData->type,
                        'expiry'    => $cardInfoData->expiry_month."/".$cardInfoData->expiry_year,
                        'zipcode' => $cardInfoData->zipcode,
                        'status' => 1,
                        'is_default_card' => 0
                    ];
		   //$userCard123 = UserCards::create($ccdata);
		}
                //print_r($cardData); die;
                $result2 = $apiObj->createCustomerCard($userPayCustId, $cardData);
                if (isset($result2['errorMesg'])) {
                    $me = microtime(true) - $ms;
                    $errortype = 1;
                    //return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    $error = ['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me, "status_code" => Config('constants.status_code.BAD_REQUEST')];
                } else {
 
                    $CardId = $result2['id'];
                    $cdata = [
                        'customer' => $userPayCustId,
                        'card_id' => $CardId ,
                        'amount' => $tot_amount,
			            "currency" => isset($restaurantData->currency_code)?$restaurantData->currency_code:"gbp",
                        'metadata' => 'OrderID:' . $txn_id,
                        'description' => 'GM-(' . $restInfo['restaurant_name'] . ')-' . $restaurantData->restaurant_name,
                        'billing_details'=>[
                            'email'=>$data['email'],
                            'name'=>$data['name'],
                            //'phone'=>$data['phone'],
                            'address'=>[
                                'city'=>$data['address']['city'],
                                'country'=>isset($restaurantData->country_iso_code)?$restaurantData->country_iso_code:"US",
                                'line1'=>$data['address']['line1'],
                                'postal_code'=>$data['address']['postal_code'],
                                'state'=>$data['address']['state'],
                            ],
                        ],
                        'shipping'=>[
                            'name'=>$data['name'],
                            //'phone'=>$data['phone'],
                            'address'=>[
                                'city'=>$data['address']['city'],
                                'country'=>isset($restaurantData->country_iso_code)?$restaurantData->country_iso_code:"US",
                                'line1'=>$data['address']['line1'],
                                'postal_code'=>$data['address']['postal_code'],
                                'state'=>$data['address']['state'],
                            ],
                        ]
                    ];
                    //print_r($data); die;
                    $chargeResponse = $apiObj->createCharge($cdata);
		    if (isset($chargeResponse['errorMesg'])) {
		            $me = microtime(true) - $ms;
		            $errortype = 1;
		            //return response()->json(['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
		            $error = ['data' => null, 'error' => $chargeResponse['errorMesg'], 'xtime' => $me, "status_code" => Config('constants.status_code.BAD_REQUEST')];
		    } else {
		            $transaction_id = $chargeResponse->id;
		            $cardNum = $chargeResponse->source->last4;
		            $cardType = $chargeResponse->source->brand;
			    $this->storeCard($userId,$cardNum,$cardInfoData,$CardId,$restaurantData->id,$restaurantData->parent_restaurant_id);
		    } 
                }
            } else {
                if (isset($cardInfoData->card_id) == null) {
                    $cardNumber = str_replace(' ', '', $cardInfoData->card);
                    $cardData = [
                        'number' => $cardNumber,
                        'exp_month' => $cardInfoData->expiry_month,
                        'exp_year' => $cardInfoData->expiry_year,
                        'cvc' => $cardInfoData->cvv,
                        'name' => $cardInfoData->name,
                    ];
                    $apiObj = new StripeGateway($paymentGatewayConfig);
                    $result2 = $apiObj->createCustomerCard($userPayCustId, $cardData);
                    if (isset($result2['errorMesg'])) {
                        $me = microtime(true) - $ms;
                        $errortype = 1;
                        $error = ['data' => null, 'error' => $result2['errorMesg'], 'xtime' => $me, "status_code" => Config('constants.status_code.BAD_REQUEST')];
                    } else {
                        $CardId = $result2['id'];
                    }
		     
                    $this->storeCard($userId,$cardNumber,$cardInfoData,$CardId,$restaurantData->id,$restaurantData->parent_restaurant_id);
                } else {
                    $CardId = $cardInfoData->card_id;
                }
                // Charge the user
                $cdata = [
                    'customer' => $userPayCustId,
                    'card_id' => $CardId,
                    'amount' => $tot_amount,
		            "currency" => isset($restaurantData->currency_code)?$restaurantData->currency_code:"gbp",
                    'metadata' => 'ReceiptID:' . $txn_id,
                    'description' => 'MunchAdo-(' . $restInfo['restaurant_name'] . ')-' . $restaurantData->restaurant_name,
                    'billing_details'=>[
                        'email'=>$data['email'],
                        'name'=>$data['name'],
                        //'phone'=>$data['phone'],
                        'address'=>[
                            'city'=>$data['address']['city'],
                            'country'=>isset($restaurantData->country_iso_code)?$restaurantData->country_iso_code:"US",
                            'line1'=>$data['address']['line1'],
                            'postal_code'=>$data['address']['postal_code'],
                            'state'=>$data['address']['state'],
                        ],
                    ],
                    'shipping'=>[
                        'name'=>$data['name'],
                        //'phone'=>$data['phone'],
                        'address'=>[
                            'city'=>$data['address']['city'],
                            'country'=>isset($restaurantData->country_iso_code)?$restaurantData->country_iso_code:"US",
                            'line1'=>$data['address']['line1'],
                            'postal_code'=>$data['address']['postal_code'],
                            'state'=>$data['address']['state'],
                        ],
                    ]	
                ];
                $apiObj = new StripeGateway($paymentGatewayConfig);
                $chargeResponse = $apiObj->createCharge($cdata);
                $transaction_id = $chargeResponse->id;
                $cardNum = $chargeResponse->source->last4;
                $cardType = $chargeResponse->source->brand;
		 
            }
	 $paymentGatewayConfig["payment_gateway"]=$paymentGatewayConfig['method'];
        } else{
            	//echo "else---->";
		        //print_r($paymentGatewayConfig);
		        die;
        }
        return ["payment_gateway"=>$paymentGatewayConfig['method'],"payment_gateway_id"=>$payment_gateway_id,"response"=>$chargeResponse,"request"=>$data,"errortype"=>$errortype,"error"=>$error,"result2"=>$result2,"transaction_id"=>$transaction_id,"CardId"=>$CardId,"cardNum"=>$cardNum,"cardType"=>$cardType];
          }catch(\Execption $e) {
                    return response()->json(['data' => null, 'error' => 'Payment processing error. Please try again later! '.$e->getMessage()], 400);
          }
    }
    public function storeCard($userId,$cardNumber,$cardInfoData,$cardId=null,$restaurant_id,$parent_restaurant_id){
         //$userInfo = User::where('id', $userId)->first();
         $userCard = "";
          
	    $userCard = UserCards::where(['name' => $cardInfoData->name,'user_id' => $userId, 'last_four' => substr($cardNumber, -4), 'type' => $cardInfoData->type,'restaurant_id' =>$parent_restaurant_id])->first();
         if ($cardInfoData->isSave && empty($userCard)) {
            $data = [
                'user_id' => $userId,
                'name' => $cardInfoData->name,
		'parent_restaurant_id' => $parent_restaurant_id,
		'restaurant_id' => $parent_restaurant_id,	
                //'card'         => $cardNumber,
                'last_four' => substr($cardNumber, -4),
                'type' => $cardInfoData->type,
                //'expiry'         => $cardInfoData->expiry_month . '/' . $cardInfoData->expiry_year,
                'status' => 1,
                'stripe_card_id' => $cardId,
            ];
            UserCards::create($data);
           }
    }
    private function refundInitatedOnPaymentGateway($orderData,$amount_refunded,$status){
        $refundResponse = null;
        $paymentGatewayConfig = $this->getPaymentGetwayInfo($orderData->restaurant_id);
        //print_r($paymentGatewayConfig);die;
        if(isset($paymentGatewayConfig['GATEWAY']) && strtolower(trim($paymentGatewayConfig['GATEWAY']))=="stripe") {
            $apiObj = new StripeGateway($paymentGatewayConfig);
            $stripeRefundData = [
                'charge' => $orderData->stripe_charge_id,
                'amount' => ($amount_refunded * 100),
                'reason' => 'requested_by_customer',
            ];
            $refundResponse = $apiObj->createRefund($stripeRefundData);
            $orderLog = ['payment_gateway_order_id'=>$orderData->stripe_charge_id,
                'order_id'=>$orderData->id,
                'payment_gateway_id'=>$paymentGatewayConfig['ID'],
                'order_status'=>$status,
                'platform'=>'api',
                'cms_user_id'=>0,
                'request_data'=>isset($stripeRefundData)?json_encode($stripeRefundData):'',
                'response_data'=>isset($refundResponse)?json_encode($refundResponse):''
            ];
            PaymentGatewayLog::create($orderLog);//die;
        }else{
        }
        return $refundResponse;
    }
    public function getTimeZoneAndKpt($restaurant_id) {
        $kptAndTimeZone = [];
        if($restaurant_id) {
            $rest_data = Restaurant::where('restaurants.id',$restaurant_id)
                    ->select('currency_symbol','currency_code','kpt','kpt_calender','city_id','cities.time_zone')
                    ->leftJoin('cities','restaurants.city_id','=','cities.id')
                    ->get();
           
            if($rest_data && isset($rest_data[0])) { 
                $kptAndTimeZone['kpt'] = $rest_data[0]->kpt;
                $kptAndTimeZone['time_zone'] = $rest_data[0]->time_zone; 
                $kptAndTimeZone['kpt_calender'] = $rest_data[0]->kpt_calender;
            }
        }
        return $kptAndTimeZone;
    }
    
    
    public function opa_order_v2(Request $request) {
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version') && $request->has('type')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                if(!$request->has('search_keyword')) { 
                    $limit = $request->input('limit');
                    $page = $request->input('page');
                    // 12-Jul-19
                    // sorting by delivery_datetime ASC (discussed with Rajiv)
                    if($request->input('type') == 'archive') {
                        $order_type_key = 'archive_order';
                        $total_record_key = 'total_archive_records';
                        $orderState = 3;
                        $order_find =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $check_token['restaurant_id'])->whereIn('user_orders.status', $statusArray)->where('user_orders.product_type','=',"food_item")->orderBy('user_orders.id', 'DESC');
                        $total_counts = $order_find->count();
                        $order_ids = $order_find->paginate($limit);
                        $odr_ids = $order_ids->toArray();
                        $odr_ids = $odr_ids['data'];
                        if($odr_ids) {
                            $odr_ids = array_pluck($odr_ids, 'id');
                        }
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',                                
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.status', 
                                'user_orders.is_order_viewed',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_order_details.item', 
                                'user_order_details.item_size',
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'delivery_time','delivery_date',
                                'restaurants_comments',
                                'user_comments',
                                'is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update','menu_id',
                                'user_orders.updated_at',
                                'user_orders.order_state'
                                )
                            ->whereIn('user_orders.id', $odr_ids)
                            ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                            ->where('user_orders.order_state', $orderState)
                            ->orderBy('user_orders.updated_at', 'DESC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->where('user_orders.product_type', 'food_item')->get();
                    } else if($request->input('type') == 'schedule') {
                        //schedule Orders
                        
                        $order_type_key = 'schedule_order';
                        $total_record_key = 'total_schedule_records';
                        $orderState = 2;
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_orders.status', 
                                'user_order_details.item', 
                                'user_order_details.item_size', 
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'user_orders.is_order_viewed',
                                'delivery_time',
                                'delivery_date',
                                'restaurants_comments',
                                'user_comments','is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',                                
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update',
                                'item_price_desc',
                                'special_instruction',
                                'unit_price',
                                'menu_id',
                                'user_orders.updated_at',
                                'user_orders.order_state');
                        $orderData = $orderData
                            ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                            ->where('user_orders.order_state', $orderState)
                            ->orderBy('user_orders.delivery_date', 'ASC')
                            ->orderBy('user_orders.delivery_time', 'ASC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->where('user_orders.product_type', 'food_item')->get();                  
                    }else {
                        //Live Orders
                        $order_type_key = 'live_order';
                        $total_record_key = 'total_live_records';
                        $orderState = 1;
                        if($request->input('type') == 'all') {
                            $order_type_key = 'orders';
                            $total_record_key = 'total_records';
                            $statusArray = array('placed','ordered','confirmed','delivered','arrived','cancelled','rejected','archived','edited','sent','ready','refunded');
                        }
                        $orderData = UserOrder::select(
                                'restaurants.id',
                                'restaurants.restaurant_name',
                                'restaurants.delivery_provider_apikey',
                                'restaurants.kpt',
                                'restaurants.kpt_calender',
                                'cities.time_zone',
                                'user_orders.id',
                                'user_orders.user_id',
                                'user_orders.restaurant_id',
                                'user_orders.order_type',
                                'user_orders.total_amount',
                                'user_orders.is_order_viewed',
                                'user_orders.address',
                                'user_orders.address2',
                                'user_orders.apt_suite',
                                'user_orders.zipcode',
                                'user_orders.created_at',
                                'user_orders.delivery_time',
                                'user_orders.delivery_date',
                                'user_orders.status', 
                                'user_order_details.item', 
                                'user_order_details.item_size', 
                                'user_order_details.quantity', 
                                'user_order_details.user_order_id',
                                'delivery_time',
                                'delivery_date',
                                'restaurants_comments',
                                'user_comments',
                                'is_reviewed',
                                'review_id',
                                'payment_receipt',
                                'city',
                                'fname',
                                'lname',
                                'user_orders.email',
                                'user_orders.phone',
                                'menu_items.name', 
                                'menu_items.status as menu_status',
                                'manual_update',
                                'item_price_desc',
                                'special_instruction',
                                'unit_price','menu_id',
                                'user_orders.updated_at',
                                'user_orders.order_state');
                        if($request->input('type') == 'all') {
                            $orderData = $orderData
                            ->whereDate('user_orders.created_at', '>=', $request->input('fromDate'))
                            ->whereDate('user_orders.created_at', '<=',$request->input('toDate'))                            
                            ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                            ->whereIn('user_orders.status', $statusArray)
                            ->orderBy('user_orders.delivery_date', 'ASC')
                            ->orderBy('user_orders.delivery_time', 'ASC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->where('user_orders.product_type', 'food_item')->get();
                        }else{
                         $orderData = $orderData
                            ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                            ->whereIn('user_orders.order_state', $orderState)
                            ->orderBy('user_orders.delivery_date', 'ASC')
                            ->orderBy('user_orders.delivery_time', 'ASC')
                            ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                            ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                            ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                            ->leftJoin('cities','restaurants.city_id','=','cities.id')
                            ->where('user_orders.product_type', 'food_item')->get();   
                        }
                    }
                }else{
                    $order_type_key = 'searched_orders';
                    $total_record_key = 'total_searched_records';
                    $statusArray = array('placed','ordered','confirmed','delivered','arrived','cancelled','rejected','archived','edited','sent','ready','refunded');
                    $orderData = UserOrder::select(
                            'restaurants.id','restaurants.restaurant_name','restaurants.delivery_provider_apikey',
                            'user_orders.id','user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount',
                            'user_orders.created_at','user_orders.delivery_time','user_orders.delivery_date','restaurants.kpt',
                            'user_orders.status', 'user_order_details.item', 'user_order_details.item_size', 'cities.time_zone',
                            'user_order_details.quantity', 'user_order_details.user_order_id','user_orders.is_read',
                            'restaurants_comments','user_comments','is_reviewed','review_id','user_orders.apt_suite',
                            'payment_receipt','city','user_orders.fname','user_orders.lname','user_orders.email','user_orders.order_state',
                            'user_orders.phone','menu_items.name', 'menu_items.status as menu_status','manual_update',
                            'item_price_desc','special_instruction','unit_price','menu_id','users.fname as ufname',
                            'users.lname as ulname','users.email as uemail','users.mobile as umobile','restaurants.rest_code',
                            'restaurants.address as raddress','restaurants.zipcode as rzipcode','restaurants.street as rstreet', 
                            'user_orders.address', 'is_guest', 'user_orders.address2', 'user_orders.state', 'user_orders.updated_at',
                            'user_orders.zipcode', 'address_label','user_orders.user_id','is_order_printed','is_order_viewed',
                            DB::raw('CONCAT(user_orders.delivery_date, " ", user_orders.delivery_time) AS delivery_datetime'));
                       $orderData = $orderData
                                ->where('user_orders.restaurant_id', $check_token['restaurant_id'])
                                ->whereIn('user_orders.status', $statusArray)
                                ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                                ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                                ->leftJoin('users', 'user_orders.user_id', '=', 'users.id')
                                ->leftJoin('cities','restaurants.city_id','=','cities.id')
                                ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                                ->where('user_orders.product_type', 'food_item');
                        //sort Order
                        if($request->has('sortorder')){
                                $sortingfor = $request->input('sortorder');//dod or doo
                                if($sortingfor == 'dod') {
                                    $orderData = $orderData->orderBy('user_orders.delivery_datetime', 'ASC'); 
                                }else {
                                    $orderData = $orderData->orderBy('user_orders.created_at', 'ASC'); 
                                }
                        }else{
                                $orderData = $orderData->orderBy('user_orders.id', 'DESC');
                        }
                        if($request->input('search_keyword') !='') {
                            $keyword = $request->input('search_keyword');
                            $orderData = $orderData->where(function ($query) use ($keyword){
                                        $query->where('user_orders.email', 'like', '%' . $keyword. '%')
                                            ->orWhere('user_orders.phone', 'like', '%' . $keyword. '%')
                                            ->orWhere('user_orders.payment_receipt', 'like', '%' . $keyword. '%')
                                            ->orWhere('users.mobile', 'like', '%' . $keyword. '%')
                                            ->orWhere('users.email', 'like', '%' . $keyword. '%')
                                            ->orWhere(DB::raw('CONCAT(users.fname, " ", users.lname)'), 'like', '%' . $keyword. '%')
                                            ->orWhere(DB::raw('CONCAT(user_orders.fname, " ", user_orders.lname)'), 'like', '%' . $keyword. '%')
                                        ;
                                        });
                        }
                        $orderData = $orderData->get();
                }
                    if($orderData) {
                        $orderData = $orderData->toArray();
                        $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                        if(is_object($currentDateTimeObj)) {
                            $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                            $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i');
                        }else {
                            $restcurrentDateTime['current_date'] = date('Y-m-d');
                            $restcurrentDateTime['current_time'] = date('H:i');
                        }
                        $currentVersion = $request->input('current_version');
                        $hardVersion = Config('constants.opa.DASHBOARD_HARD_VERSION_ANDROID');
                        $softVersion = Config('constants.opa.DASHBOARD_SOFT_VERSION_ANDROID');
                        if($currentVersion < $hardVersion){
                            $updateType = "hard";
                        }elseif($currentVersion < $softVersion){
                            $updateType = "soft";
                        }else{
                            $updateType = "no";
                        }
                        $update_app = array(
                            "upgrade_type"=>$updateType,
                            "counter"=>  Config('constants.opa.COUNTER'),
                            "message"=>  Config('constants.opa.FOURCE_UPDATE_MESSAGE'),
                            "clear_data"=>  Config('constants.opa.CLEAR_DATA'),
                            "apk_link"=>  Config('constants.opa.APK_FILE_PATH')
                        );
                       
                        //live Orders keys
                        $kptCalendar = "0-0-0";
                        foreach ($orderData as $key => $order) {
                            if(array_key_exists($order['user_order_id'], $orders)) {
                                $item_count = count($orders[$order['user_order_id']]['item_list']);
                                $orders[$order['user_order_id']]['item_list'][$item_count] =  array(
                                    "order_item_id"=> (string)$order['menu_id'],
                                    "item_name"=> (string)$order['item'],
                                    "item_size"=>  $order['item_size'],
                                    "item_qty"=>  (string)$order['quantity']
                                );
                            }else {
                                $refunded_amount = DB::table('refund_histories')->where('order_id', $order['id'])->sum('amount_refunded');
                                $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['restaurant_id'], 'datetime' => $order['created_at'])));
                                $delivery_time_left_hour = explode('.',(strtotime($order['delivery_date'].' '.$order['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                                $orderType = ($order['order_type'] == 'carryout')? 'Takeout': 'Delivery';
                                $stageOfOrder = $this->orderStage($order['status'], $orderType);
                                $deliveryAddress = "";
                                if($orderType == "Delivery"){
                                    $address = [trim($order['address']),trim($order['address2']),trim($order['apt_suite']),trim($order['city']),trim($order['zipcode'])];
                                    $deliveryAddress  = array_filter($address);
                                    $deliveryAddress = implode(",", $deliveryAddress);                                    
                                }
                                
                                $customerTotalOrder = CommonFunctions::getCustomerOrders($order);
                                $timezone = $order['time_zone'];
                                $preparation_time = $order['kpt'];
                                $orders[$order['user_order_id']] = array(
                                    'id' => (string)$order['id'],
                                    'fname' =>  $order['fname'],
                                    'lname' =>  $order['lname'],
                                    'email' =>  $order['email'],
                                    'phone' =>  $order['phone'],
                                    'delivery_address'=>$deliveryAddress,
                                    'is_order_viewed'=>$order['is_order_viewed'],
                                    'delivery_time_left_hour' => (int)$delivery_time_left_hour,
                                    'delivery_time_left' => 0,
                                    'restaurant_id' => (string)$order['restaurant_id'],                                              
                                    'restaurant_name' => $order['restaurant_name'],
                                    'is_restaurant_exists' => 'Yes',
                                    'order_type1' => 'I',
                                    'order_type' => $order['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                                    'delivery_date' => $order['delivery_date'].' '.date('H:i:00', strtotime($order['delivery_time'])),
                                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                                    'menu_status' => $order['menu_status'],
                                    'is_reviewed' => $order['is_reviewed'] ? $order['is_reviewed'] : 0,
                                    'review_id' => $order['review_id'] ? $order['review_id'] : 0,
                                    'manual_update' => $order['manual_update'],
                                    'payment_receipt' =>  $order['payment_receipt'],
                                    'delivery_service' => $order['delivery_provider_apikey'] ? 1 : 0,
                                    'service_provider' => '',
                                    'status' => $order['status'],
                                    'order_state'=>$order['order_state'],
                                    'refunded_amount' => $refunded_amount ? (string) number_format($refunded_amount, 2) : 'NA',
                                    'total_amount' => $order['total_amount'],
                                    'user_total_order'=>$customerTotalOrder,
                                    'item_list' => array(
                                        array(
                                            "order_item_id"=> (string)$order['menu_id'],
                                            "item_name"=> (string)$order['item'],
                                            "item_size"=>  $order['item_size'],
                                            "item_qty"=> (string) $order['quantity']
                                        )
                                    ),
                                    'order_stage'=>$stageOfOrder,
                                    'time_zone'=>$order['time_zone'],
                                    'kpt'=>$order['kpt'],
                                    'kpt_cal'=>$order['kpt_calender'],
                                    'updated_at'=>$order['updated_at']
                                );
                                $kptCalendar = $order['kpt_calender'];
                            }
                            //}
                        }
                    }
                    if($orders) {
                        $orders = array_values($orders);
                    }
                    $get_restaurant_setting = CommonFunctions::getRestaurantSetting($check_token['restaurant_id']);
                    $timekpt = $this->getTimeZoneAndKpt($check_token['restaurant_id']);                   
                    $restInfo = CommonFunctions::getRestaurantDetails($check_token['restaurant_id'], false, array('restaurants.id', 'restaurants.restaurant_name','custom_from','support_from'));
                    $statusArray = array("placed");
                    $liveOrderCount = UserOrder::select('user_orders.id')
                        ->whereIn('user_orders.restaurant_id', $restInfo)
                        ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
                        ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',
                            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')
                        ->where('user_orders.product_type','=',"food_item")
                        ->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'>=', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )
                        ->whereIn('user_orders.status', $statusArray)->get()->count();
                    $scheduleOrderCount = UserOrder::select('user_orders.id')
                        ->whereIn('user_orders.restaurant_id', $restInfo)
                        ->where('user_orders.is_order_viewed', 0)
                        ->leftJoin('users', 'users.id', '=', 'user_orders.user_id')
                        ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                        ->leftJoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                        ->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')
                        ->groupBy('user_orders.id','user_orders.user_id','user_orders.order_type','user_orders.restaurant_id','user_orders.fname','user_orders.lname','user_orders.payment_receipt','user_orders.phone','user_orders.total_amount','user_orders.created_at',
                            'user_orders.delivery_time','user_orders.delivery_date','user_orders.manual_update','user_orders.status','user_orders.product_type')
                        ->where('user_orders.product_type','=',"food_item")->where(DB::raw('CONVERT_TZ(now(),"+00:00", cities.timezone_value)') ,'<', DB::raw('DATE_SUB(concat(user_orders.delivery_date," ",user_orders.delivery_time), INTERVAL kpt MINUTE)') )
                        ->whereIn('user_orders.status', $statusArray)->get()->count();
                    $response['data'] = array(
                        $order_type_key => $orders,
                        'restService' => $get_restaurant_setting,
                         $total_record_key => $total_counts ? $total_counts :  count($orders),
                        'fource_update' => $update_app,
                        'timezone'=>isset($timekpt['time_zone'])?$timekpt['time_zone']:"",
                        'preparation_time'=>$kptCalendar,//isset($timekpt['kpt_calender'])?$timekpt['kpt_calender']:"00-00-00",
                        'order_counts' => [
                            'live'      => $liveOrderCount,     // active confirmed orders count
                            'schedule'  => $scheduleOrderCount  // unread scheduled orders count
                        ]
                    );
                }else {
                    $response['message'] = $check_opa_version['message'];
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        return response()->json($response, $status);
    }
    
    public function opa_order_update_status_v2(Request $request, $order_id) {
        $requestData = json_decode($request->getContent(), true); 
        #$requestData = $request->input();
        \Log::info('opa_order_update_status');\Log::info($requestData);
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );
        $delivery_service_error_message = '';
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST'); 
        if($request->has('token')) { 
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
	    $check_pos_token = CommonFunctions::check_pos_token($request->input('token'));
	    if($check_pos_token['is_valid']==true){
		$requestData['status']=$request->input('status'); 
		$requestData['reason']=$request->input('reason'); 	
	    }
            if (($check_token['is_valid'] && $check_token['restaurant_id']) || ($check_pos_token['is_valid']==true)) {
		$check_token=$check_pos_token;
                $response['is_token_valid'] = true;
                $orderData = UserOrder::find($order_id);
		$check_token['restaurant_id'] = isset($orderData->restaurant_id)?$orderData->restaurant_id:null;
		//print_r($check_token);  print_r($orderData->toArray());//die;
                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                if(is_object($currentDateTimeObj)) {
                    $currentTimestamp =  strtotime($currentDateTimeObj->format('Y-m-d H:i:s'));
                    $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                    $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
                }else {
                    $currentTimestamp = strtotime(date('Y-m-d H:i:s'));
                    $restcurrentDateTime['current_date'] = date('Y-m-d');
                    $restcurrentDateTime['current_time'] = date('H:i:00');
                }
                $action = $requestData['status'];                
                
                $restaurantData = Restaurant::where(array('id'=> $check_token['restaurant_id']))->first();
                if($restaurantData) {
                    $curSymbol = $restaurantData->currency_symbol;
                    $curCode = $restaurantData->currency_code;
                } else {
                    $curSymbol = config('constants.currency');
                    $curCode = config('constants.currency_code');
                }
                $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
                $order_del_time = ($deliveryTimestamp - $restaurantData->kpt * 60);
                $orderState = 1;
                if($currentTimestamp < $order_del_time){
                    $is_order_scheduled = 1;
                    $orderState = 2;
                }else{
		    $is_order_scheduled = 0;
		}
		//print_r($check_pos_token);  
                //Update time slot:
                $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                    ->where('status', 1)->where('id', $restaurantData->parent_restaurant_id)->first();               
                
                if($orderData && isset($requestData[' movearc']) && $requestData[' movearc']===1 && $requestData['status']=="placed"){
                    $orderData->manual_update = 1;
                    $orderData->order_state = 3;                    
                    $orderData->save();
                    $response['data'] = array(
                        'message' => true,
                        'status' => $requestData['status'],
                        'orders' => 0,
                        'is_order_scheduled' => 0,
                        'delivery_service_error_message' => $delivery_service_error_message,
                        'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                        'delivery_time_left_hour' => 0
                    );
                    return response()->json($response, $status);
                }
                if(isset($requestData['status']) && $requestData['status'] == 'placed' && isset($requestData['delivery_date'])) {
                    #$orderData = UserOrder::where('id',$order_id)->select('id','user_id')->get();
                    if($orderData) {
                        $status = Config('constants.status_code.STATUS_SUCCESS');
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        list($date, $time) = explode(' ', $requestData['delivery_date']);
                        $orderData->delivery_date = $date;
                        $orderData->delivery_time = $time;
                        
                        $delivery_time_left_hour = (int) explode('.',(strtotime($requestData['delivery_date']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                        $deliveryTimestamp = strtotime($orderData->delivery_date . ' ' . $orderData->delivery_time . ':00');
                        $order_del_time = ($deliveryTimestamp - $restaurantData->kpt * 60);
                        $is_order_scheduled = 0;
                        $orderState = 1;
                        if($currentTimestamp < $order_del_time){
                            $is_order_scheduled = 1;
                            $orderState = 2;
                        }
                        $orderData->is_asap_order= $is_order_scheduled;
                        $orderData->order_state = $orderState;
                        $orderData->save();
                        $response['data'] = array(
                            'message' => true,
                            'status' => $requestData['status'],
                            'order_state'=>$orderState,
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => $delivery_time_left_hour
                        );
                    }
                    return response()->json($response, $status);
                }
		//print_r($requestData); 
		//die;
                #common data required:
                $langId = config('app.language')['id'];
                CommonFunctions::$langId = $langId;
                $orderItem = $label = "";
                $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00'); 
                $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
                //$cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
		if($orderData->card_type=='Cash' || $orderData->card_type=='No Payment Required'){
                            $cardInfo = "<span style='color: #000;'>".$orderData->card_type."</span>";
                            $cardInfoTxt = '';
                }else{
                            $cardInfo = "<span style='color: #000;'>".$orderData->card_type."</span>".' ('.$orderData->card_number.')';
                            $cardInfoTxt = '<b>Credit Card:</b>';
                }
                $orderDetail = new UserOrderDetail();
                $orderDetailData = $orderDetail->where('user_order_id', $order_id)->get();;
                $orderItem = '';
                $total_refunded_amount = DB::table('refund_histories')->where('order_id', $order_id)->sum('amount_refunded');
                ##-----------Cancel Order --- ###
                if(isset($requestData['status']) && $requestData['status'] == 'cancelled') {
                    $orderData->status = $requestData['status'];
                    $orderData->restaurants_comments = $requestData['reason'];
                    $orderData->order_state = 3;
                    $orderData->save();
                    //twinjet cancel
                    $deliveryServicesData1 = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                    if(isset($deliveryServicesData1) && $deliveryServicesData1->provider_name=="twinjet"){
                        $orderDataArray = $orderData->toArray();
                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                        $restaurantData1= $restaurantData->toArray();
                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                    }// send email
                    $orderItem = '';
                    foreach($orderDetailData as $data){
                        $label = "";
                        $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                        $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                        $addons_data = [];
                        $bypData = json_decode($data->menu_json, true );
                        //print_r($bypData); die;
                        if (!is_null($bypData)) {
                            $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                        }
                        $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                    <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <b>'.$data->quantity.' '.$data->item.'</b>';
                        $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                        //$label = strtolower($data->size) != 'full' ? $data->size .' ' : '';
                       // $label = (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? $data->item_size .' ' : '';
                        if($data->is_byp){
                            $label='';
                            $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                        }else{
                            $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                            $label .= $addon_modifier_html;
                        }
                            $instruct = '';
                            if(!empty($data->special_instruction)){
                                $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                            }
                            $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                    }
                    $mailKeywords = array(
                        'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                        'RESTAURANT_ADD' => $restaurantData->address,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                        'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                        'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                        'CARD_NUMBER' => $cardInfo,
                        'REASON' => $requestData['reason'],
                        'ITEM_DETAIL' => $orderItem,
                        'SITE_URL' => $restaurantData->source_url,
			'cardInfoTxt' =>$cardInfoTxt,
                        'title' => "Cancel Order"
                    );
                    /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);
                    /**************************************************************************************************/
                    $tipAmount = $orderData->tip_amount;
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                    if($orderData->order_type=="delivery") {
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery-cancel';
                    } else {
                        $mailTemplate = 'carryout_cancel';
                        $mailKeywords['USER_ADDRESS'] = '';
                    }
                    $mailKeywords = array(
                        'header' => array(),
                        'footer' => array(),
                        'data' => $mailKeywords
                    );
                    $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                    $mailData['subject']        = "Your order has been cancelled";
                    $mailData['body']           = $mail_html;
                    $mailData['receiver_email'] =  $orderData->email;
                    $mailData['receiver_name']  = ucfirst($orderData->fname);
                    $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                    $mailData['MAIL_FROM'] = $parentData->custom_from;
                    CommonFunctions::sendMail($mailData);
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data'] = array(
                        'message' => true,
                        'status' => 'cancelled',
                        'order_id' => $order_id,
                        'orders' => 0,
                        'is_order_scheduled' => $is_order_scheduled,
                    );
                    /******** SMS Send **** @18-10-2018 by RG *****/
                    $sms_keywords = array(
                        'order_id' => $orderData->id,
                        'sms_module' => 'order',
                        'action' => 'rejected',
                        'sms_to' => 'customer'
                    );
                    CommonFunctions::getAndSendSms($sms_keywords);
                    /************ END SMS *************/
                    $status  = Config('constants.status_code.STATUS_SUCCESS');
                }
                ## END CANCEL ORDER Template
                #status = archived, sent, ready, confirmed
                ############ Confirm Order / Ready Order ##########
                if(isset($requestData['status']) && in_array($requestData['status'], array('confirmed',' sent','ready', 'archived'))) {
                    $action = $requestData['status'];
                    $orderState = ($requestData['status']=="confirmed" || $requestData['status']== 'ready')?1:3;
                      
                    ############ Relay Delivery service ##########
                    if($orderData->order_type == "delivery" && $restaurantData->delivery_provider_id > 0 && !$requestData['manual_update'] && $requestData['status']==="confirmed"){
                        $deliveryServiceObj = new DeliveryServiceProvider();
                        $deliveryServicesData = DeliveryServices::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                        $deliveryRelayDetails = array(
                            'id'=>$restaurantData->delivery_provider_id,
                            'producer_key'=>$restaurantData->producer_key,
                            'order_creation_url'=>$deliveryServicesData->order_creation_url,
                            'api_key'=>$restaurantData->delivery_provider_apikey
                        );
                        $deliveryServiceObj->deliveryRelayDetails = $deliveryRelayDetails;
                        if(!$deliveryServiceObj->orderCreation($orderData)){
                            if(isset($deliveryServiceObj->orderCreationSuccess['message'])) {
                                #$response['message'] = $deliveryServiceObj->orderCreationSuccess['message'];
                                $delivery_service_error_message = $response['message'] = "There was an error connecting to your delivery service. Please call them to schedule delivery for this order.";
                            } else {
                                #$response['message'] = "Something went wrong while pushing an order to Relay Service provider.";
                                $delivery_service_error_message = $response['message'] = "There was an error connecting to your delivery service. Please call them to schedule delivery for this order.";
                            }
                            $me = microtime(true) - $ms;
                            $response['xtime'] = $me;
                            #return response()->json($response, $status);
                        }
                        //twinjet confirm order
                        if($deliveryServicesData->provider_name=="twinjet" && $orderData->order_type=="delivery"){ //trigger twinjet
                            $orderDataArray = $orderData->toArray();
                            $restaurantDataArray = $restaurantData->toArray();
                            $orderDetailsData =  UserOrderDetail::find($orderData->id);
                            //$orderDetailsData = $orderDetail->getOrderDetail($orderData->id);
                            $orderDetailsData1 = $orderDetailsData->toArray();
                            $orderDataArray['item_list'][] = $orderDetailsData1;
                            $responseData = $this->submitOrderToVendor($orderDataArray,$restaurantDataArray,1,$deliveryServicesData->id);
                        }
                    }
                    ####################### end ###############
                    $orderData->order_state = $orderState;
                    $orderData->status = $action;
                    $orderData->save();
                   
                    $orderItem = '';
                    foreach($orderDetailData as $data){
                        $label = "";
                        $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                        $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                        $addons_data = [];
                        $bypData = json_decode($data->menu_json, true );
                        //print_r($bypData); die;
                        if (!is_null($bypData)) {
                            $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                        }
                        $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody>	
                    <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                    <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <b>'.$data->quantity.' '.$data->item.'</b>';
                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                  //  $label = (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? $data->item_size .' ' : '';
                    if($data->is_byp){
                        $label='';
                        $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                    }else{
                        $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                        $label .= $addon_modifier_html;
                    }
                    $instruct = '';
                    if(!empty($data->special_instruction)){
                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                    }
                        $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                    }
                    $mailKeywords = array(
                        'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                        'ORDER_NUMBER' => $orderData->payment_receipt,
                        'USER_NAME' => ucfirst($orderData->fname),
                        'ORDER_TYPE' => ucfirst($orderData->order_type),
                        'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                        'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                        'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                        'DELIVERY_ADD' => $orderData->address.' '.$orderData->city,
                        'PHONE' => $orderData->phone,
                        'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                        'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.$orderData->total_amount,
                        'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                        'ITEM_DETAIL' => $orderItem,
                        'CARD_NUMBER' => $cardInfo,
                        'URL' => $restaurantData->source_url,
                        'SITE_URL' => $restaurantData->source_url,
                        'title' => "Order Confirmation"
                    );
                    /*************************************************************************************************/
                    $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);                    
                    $tipAmount = $orderData->tip_amount;
                    if($orderData->user_comments!=""){
                        $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                    }
                    else {
                        $mailKeywords['SPECIAL_INFO'] = '';
                    }
                  
                    $mailKeywords['DISPLAY_REFUND'] = 'none';
                    if($total_refunded_amount > 0 ) {
                        $mailKeywords['DISPLAY_REFUND'] = 'table';
                    }
                    if($orderData->order_type=="delivery") {
                        $address = $orderData->address;
                        if ($orderData->address2) {
                            $address = $address . ' ' . $orderData->address2;
                        }
                        $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                        //echo $address;
                        $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                        $mailKeywords['USER_ADDRESS'] = $address;
                        $mailTemplate = 'delivery_confirm_user';
                    } else {
                        $mailTemplate = 'carryout_confirm_user';
                        $mailKeywords['USER_ADDRESS'] = '';
                        $img = "";
                        if($action == "ready"){
                            $heading = "We have finished preparing your takeout order and can't wait for you to enjoy it.";
                            $img = '<a href="#"><img src="https://dashboard.munchado.biz/mailer-images/keki/get-direction.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                            $mailKeywords['DISPLAY_IMG'] = 'none';
                            /******** SMS Send **** @18-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $orderData->id,
                                'sms_module' => 'order',
                                'action' => 'ready',
                                'sms_to' => 'customer'
                            );
                            CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }else{
                            $heading = "You've put together an amazing order.";
                            $img = '<a href="#"><img src="https://dashboard.munchado.biz/mailer-images/keki/manage-order.jpg" alt="" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></a>';
                            $mailKeywords['DISPLAY_IMG'] = 'table';
                            /******** SMS Send **** @18-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $orderData->id,
                                'sms_module' => 'order',
                                'action' => 'confirmed',
                                'sms_to' => 'customer'
                            );
                            CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }
                        $mailKeywords['HEAD'] = $heading;
                        $mailKeywords['IMG'] = $img;
                        $mailKeywords['ACTION'] = ucfirst($action);
                    }
                    $config['to'] = $orderData->email;
                    $config['subject'] = "Your order has been ".$action;
                    //$config['from'] = 'KEKI'."<MunchAdo2015@gmail.com>";
                    $status  = Config('constants.status_code.STATUS_SUCCESS');
                    $delivery_time_left_hour = explode('.',(strtotime($orderData->delivery_date.' '.$orderData->delivery_time) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                    if(($action == "archived") || ($action=="sent")) {
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        $response['data'] = array(
                            'message' => true,
                            'status' => $action,
                            'order_state'=>$orderState,
                            'order_id' => $order_id,
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            #'delivery_service' => false,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => (int) $delivery_time_left_hour
                        );
                    }
                    else{
                        $mailKeywords = array(
                            'header' => array(),
                            'footer' => array(),
                            'data' => $mailKeywords
                        );
                        $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                        $mailData['subject']        = "Your order has been ".$action;
                        $mailData['body']           = $mail_html;
                        $mailData['receiver_email'] =  $orderData->email;
                        $mailData['receiver_name']  = ucfirst($orderData->fname);
                        $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                        $mailData['MAIL_FROM'] = $parentData->custom_from;
                        CommonFunctions::sendMail($mailData);
                        $response['result'] = true;
                        $response['message'] = 'Success';
                        if($action == "ready") {
                            $action = "arrived";
                        }
                        $response['data'] = array(
                            'message' => true,
                            'status' => $action,
                            'order_id' => $order_id,
                            'orders' => 0,
                            'is_order_scheduled' => $is_order_scheduled,
                            #'delivery_service' => false,
                            'delivery_service_error_message' => $delivery_service_error_message,
                            'delivery_service' => $restaurantData->delivery_provider_apikey ? true : false,
                            'delivery_time_left_hour' => (int) $delivery_time_left_hour
                        );
                    }
                    # END Confirm Order ####3
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
    public function order_refund_v2(Request $request) {
        $requestData = json_decode($request->getContent(), true);
        #$requestData = $request->input();
        \Log::info('opa_order_order_refund');\Log::info($requestData);
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => true,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('order_id')  && $request->has('refund_type') && in_array($request->input('refund_type'), array('F','P'))) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                #$orderData = UserOrder::find($request->input('order_id'));
                $orderData = UserOrder::where('id', $request->input('order_id'))->whereNotIn('status', ['refunded','pending'])->first();
                if($orderData) {
                    $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
                    if(is_object($currentDateTimeObj)) {
                        $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                        $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
                    }else {
                        $restcurrentDateTime['current_date'] = date('Y-m-d');
                        $restcurrentDateTime['current_time'] = date('H:i:00');
                    }
                    $action = $orderData->status;
                    $globalSetting = DB::table('global_settings')->whereNotNull('bcc_email')->whereNotNull('bcc_name')->first();
                    if($request->input('refund_mode') && $request->input('refund_mode') == 'fully') {
                        $action = 'refunded';
                        $orderState = '3';
                        // if($request->input('refund_type') == 'F') {
                        //     $refund_value = $orderData->total_amount;
                        //     $amount_refunded = $refund_value = $orderData->total_amount;
                        // }else {
                        //     $amount_refunded  = $orderData->total_amount;
                        //     $refund_value = 100; //100% is being send
                        // }
                        $refund_value = $orderData->total_amount;
                        $amount_refunded = $refund_value = $orderData->total_amount;
                    }else {
                        if($request->input('refund_type') == 'F') {
                            $refund_value = $request->input('refund_value');
                            $amount_refunded = $request->input('refund_value');
                        }else {
                            $amount_refunded  =  ($request->input('refund_value') * $orderData->total_amount) / 100;
                            $refund_value = $request->input('refund_value'); //100% is being send
                        }
                    }
                    if($amount_refunded <= $orderData->total_amount) {
                        $restaurantData = Restaurant::where(array('id'=> $check_token['restaurant_id']))->first();
                        // Restaurant currency symbol
                        if($restaurantData) {
                            $curSymbol = $restaurantData->currency_symbol;
                            $curCode = $restaurantData->currency_code;
                        } else {
                            $curSymbol = config('constants.currency');
                            $curCode = config('constants.currency_code');
                        }
                        //Update time slot:
                        $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                            ->where('status', 1)->where('id', $restaurantData->parent_restaurant_id)->first();
                        $refundResponse = $this->refundInitatedOnPaymentGateway($orderData,$amount_refunded,$action);
                        if(!isset($refundResponse['errorMessage'])) {
                            #Refund Data Saved
                            $refund_data = array(
                                'order_id' => $request->input('order_id'),
                                'refund_type' => $request->input('refund_type'),
                                'refund_value' => $refund_value,
                                'amount_refunded' => $amount_refunded,
                                'charge_id' => $refundResponse->charge,
                                'refund_id' =>  $refundResponse->id,
                                'reason' =>  $request->input('reason'),
                                'host_name' =>  $request->input('host_name'),
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            );
                            $oldOrderStatus = $orderData->status;
                            #RefundHistory::create($refund_data);
                            DB::table('refund_histories')->insert($refund_data);
                            $orderData->status = $action;
                            $orderData->total_amount = $orderData->total_amount - $amount_refunded;
                            if($orderData->total_amount <= 0) {
                                $orderData->status = 'refunded';
                                $orderData->order_state = 3;
                            }
                            $orderData->save();
                            $deliveryServicesData1 = DeliveryService::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                            if(isset($deliveryServicesData1) && $deliveryServicesData1->provider_name=="twinjet"){
                                if($request->input('refund_mode') != 'partially') {
                                    if($orderData->order_type=="delivery" && ($oldOrderStatus!="delivered" && $oldOrderStatus!="archived" )){
                                        $orderDataArray = $orderData->toArray();
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $restaurantData1 = $restaurantData->toArray();
                                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                                        $orderState =1;
                                    }
                                }else{
                                    if($orderData->total_amount == 0  && $orderData->order_type=="delivery" && ($oldOrderStatus!="delivered" && $oldOrderStatus!="archived" )){
                                        $orderState =1;
                                        $orderDataArray = $orderData->toArray();
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $restaurantData1 = $restaurantData->toArray();
                                        $this->submitOrderToVendor($orderDataArray,$restaurantData1,2,$restaurantData->delivery_provider_id);
                                    }else{
                                        $orderState =3;
                                        //$restaurantData = Restaurant::where(array('id'=>$orderData->restaurant_id))->first();
                                        $deliveryServicesData = DeliveryServices::where(array('id'=>$restaurantData->delivery_provider_id))->first();
                                        if($deliveryServicesData && $deliveryServicesData->provider_name=="twinjet"){ //trigger twinjet
                                            $orderDataArray = $orderData->toArray();
                                            $restaurantDataArray = $restaurantData->toArray();
                                            $orderDetailsData1 = [];
                                            $orderDataArray['item_list'][] = $orderDetailsData1;
                                            $responseData = $this->submitOrderToVendor($orderDataArray,$restaurantDataArray,0,$deliveryServicesData->id);
                                        }
                                    }
                                }
                            }
                            #common data required:
                            $langId = config('app.language')['id'];
                            CommonFunctions::$langId = $langId;
                            $orderItem = $label = "";
                            $timestamp = strtotime($orderData->delivery_date. ' '.$orderData->delivery_time.':00');
                            $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderData->restaurant_id, 'datetime' => $orderData->created_at)));
                            $cardInfo = $orderData->card_type.' ('.$orderData->card_number.')';
                            $orderDetail = new UserOrderDetail();
                            $orderDetailData = $orderDetail->where('user_order_id',  $request->input('order_id'))->get();
                            $orderItem = '';
                            #if($orderData->status == 'refunded') {
                            if(1) {
                            ##-----------Refund Order Email and sms--- ###
                                $total_refunded_amount = DB::table('refund_histories')->where('order_id', $request->input('order_id'))->sum('amount_refunded');
                                    // send email
                                    $orderItem = '';
                                foreach($orderDetailData as $data){
                                    $label = "";
                                    $addon_modifier=CommonFunctions::getAddonsAndModifiers($data);
                                    $addon_modifier_html=CommonFunctions::getAddonsAndModifiersForMailer($addon_modifier);
                                    $addons_data = [];
                                    $bypData = json_decode($data->menu_json, true );
                                    //print_r($bypData); die;
                                    if (!is_null($bypData)) {
                                        $addons_data[$data->id] = CommonFunctions::changeMyBagJsonData($data->is_byp, $bypData);
                                    }
                                    $orderItem .= '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody> 
                            <tr style="padding:0;text-align:left;vertical-align:top"><th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                            <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                            <b>'.$data->quantity.' '.$data->item.'</b>';
                                    $result = isset($addons_data[$data->id]) ? $addons_data[$data->id] : null;
                                    //$label = strtolower($data->size) != 'full' ? $data->size .' ' : '';
                                  //  $label = (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? $data->item_size .' ' : '';
                                    // if($data->is_byp==1 && $result){
                                    //     foreach($result as $key=>$data1){
                                    //         $label .= $key;
                                    //         $label .= '['.$data1.'] ';
                                    //     }
                                    // }
                                    // if($data->is_byp==0 && $result) {
                                    //     foreach ($result as $data1) {
                                    //         $label .= $data1['addons_name'];
                                    //         $label .= '[' . $data1['addons_option'] . '] ';
                                    //     }
                                    // }
                                    if($data->is_byp){
                                        $label='';
                                        $label =  CommonFunctions::getOldAddons($label, $data->is_byp, $result);
                                    }else{
                                        $label .= (strtolower($data->item_size) != 'null' && $data->item_size != "NA" && !empty($data->item_size) && !is_null($data->item_size)) ? '<tr style="font-size:12px;line-height:18px;"><td style="padding-left:10px" ><b>Size: </b>'.$data->item_size.'</td></tr>' .' ' : '';
                                        $label = $addon_modifier_html;
                                    }
                                    $instruct = '';
                                    if(!empty($data->special_instruction)){
                                        $instruct='<b style="Margin:0;Margin-bottom:10px;/* color:#8a8a8a; */font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left;">Special Instructions -</b><p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$data->special_instruction.'</p>';
                                    }
                                    $orderItem .='<p class="light-text item-desc" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:15px 0 0;margin-bottom:10px;padding:0;text-align:left">'.$label.'</p>'.$instruct.'</p></th></tr></table></th><th class="price-clmn small-6 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left">
                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$data->total_item_amt.'</p></th></tr></table></th></tr></tbody></table>
                                    <hr class="light-hr" style="border-bottom:1px solid rgba(119,119,119,.15);border-left:1px solid #ebebeb;border-right:1px solid #ebebeb;border-top:1px solid #ebebeb;display:block;height:1px;margin:25px 0;padding:0">';
                                }
                                $mailKeywords = array(
                                    'RESTAURANT_NAME' => $restaurantData->restaurant_name,
                                    'RESTAURANT_ADD' => $restaurantData->address,
                                    'ORDER_NUMBER' => $orderData->payment_receipt,
                                    'USER_NAME' => ucfirst($orderData->fname),
                                    'ORDER_TYPE' => ucfirst($orderData->order_type),
                                    'ORDER_TIME' => date('l d M, h:i A', $timestamp2),
                                    'ORDER_DELIVERY_TIME' => date('l d M, h:i A', $timestamp),
                                    'TIME_TAKOUT' => date('l d M, h:i A', $timestamp),
                                    'SUB_TOTAL' => '<span>'.$curSymbol.'</span>'.$orderData->order_amount,
                                    'TOT_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($orderData->total_amount,2),
                                    'REFUND_AMOUNT' => '<span>'.$curSymbol.'</span>'.number_format($total_refunded_amount,2),
                                    'CARD_NUMBER' => $cardInfo,
                                    'REASON' => $request->input('reason'),
                                    'ITEM_DETAIL' => $orderItem,
                                    'SITE_URL' => $restaurantData->source_url,
                                    'title' => "Refund Order",
                                    'URL' => $restaurantData->source_url,
                                );
                                /*************************************************************************************************/
                                $mailKeywords=CommonFunctions::getAllChargesForMailer($mailKeywords,$orderData,$curSymbol);
                                /**************************************************************************************************/
                                if($orderData->tax==0.00){
                                    $mailKeywords['TAX'] = '';
                                    $mailKeywords['TAX_TITLE'] = '';
                                    $mailKeywords['DISPLAY'] = 'none';
                                } else{
                                    $mailKeywords['TAX'] = '<span>'.$curSymbol.'</span>'.$orderData->tax;
                                    $mailKeywords['TAX_TITLE'] = "Taxes";
                                    $mailKeywords['DISPLAY'] = 'table';
                                }
                                $tipAmount = $orderData->tip_amount;
                                if($orderData->tip_amount==0.00){
                                    $mailKeywords['TIP_AMOUNT'] = '';
                                    $mailKeywords['TIP_TITLE'] = '';
                                    $mailKeywords['DISPLAY_TIP'] = 'none';
                                } else{
                                    $mailKeywords['TIP_TITLE'] = 'Tip Amount:';
                                    $mailKeywords['TIP_AMOUNT'] ='<table class="row tip-price" style="border:2px solid #cacaca;border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                                                <th class="small-6 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left"><p style="Margin:0;Margin-bottom:10px;color:#cacaca;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:left">'.$orderData->tip_percent.'<span>%</span></p>
                                                </th></tr></table></th>
                                                <th class="small-6 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:3px 0 3px 5px!important;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:50%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:3px 0 3px 5px!important;padding-bottom:0;text-align:left">
                                                    <p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"><span>'.$curSymbol.'</span>'.$tipAmount.'</p></th></tr></table></th>
                                            </tr></tbody></table>';
                                    $mailKeywords['DISPLAY_TIP'] = 'table';
                                }
                                $mailKeywords['DISPLAY_REFUND'] = 'table';
                                if($orderData->user_comments!=""){
                                    $mailKeywords['SPECIAL_INFO'] = '<b>Special Instructions:</b> <br><i>'.$orderData->user_comments.'</i>';
                                }
                                else {
                                    $mailKeywords['SPECIAL_INFO'] = '';
                                }
                                if($orderData->order_type=="delivery") {
                                    $address = $orderData->address;
                                    if ($orderData->address2) {
                                        $address = $address . ' ' . $orderData->address2;
                                    }
                                    $address = $address . ' ' . $orderData->city . ' ' . $orderData->state_code . ' ' . $orderData->zipcode;
                                    //echo $address;
                                    $mailKeywords['DELIVERY_CHARGE'] = '<span>'.$curSymbol.'</span>'.$orderData->delivery_charge;
                                    $mailKeywords['USER_ADDRESS'] = $address;
                                    $mailTemplate = 'delivery_order_refund';
                                } else {
                                    $mailKeywords['USER_ADDRESS'] = '';
                                    $mailTemplate = 'carryout_order_refund';
                                }
                                $order_refund_date = date('M d, Y h:i A', strtotime($orderData->delivery_date.' '.$orderData->delivery_time.':00'));
                                if($orderData->status == 'refunded'){
                                    $mailKeywords['HEAD'] = "We've initiated a refund for your experience with us on $order_refund_date";
                                }else {
                                    $mailKeywords['HEAD'] = "We've initiated a partial refund for your experience with us on $order_refund_date";
                                }
                                $mailKeywords = array(
                                    'header' => array(),
                                    'footer' => array(),
                                    'data' => $mailKeywords
                                );
                                $mail_html = CommonFunctions::restMailTemplate($mailTemplate, $restaurantData->parent_restaurant_id, $mailKeywords);
                                $mailData['subject']  = "Your Refund Is Being Processed";
                                $mailData['body']           = $mail_html;
                                $mailData['receiver_email'] =  $orderData->email;
                                $mailData['receiver_name']  = ucfirst($orderData->fname);
                                $mailData['MAIL_FROM_NAME'] =  $parentData->restaurant_name;
                                $mailData['MAIL_FROM'] = $parentData->custom_from;
                                if($globalSetting) {
                                    $mailData['bcc_email'] = $globalSetting->bcc_email;
                                    $mailData['bcc_name'] = $globalSetting->bcc_name;
                                }
                                CommonFunctions::sendMail($mailData);
                                /******** SMS Send **** @18-10-2018 by RG *****/
                                $sms_keywords = array(
                                    'order_id' => $orderData->id,
                                    'sms_module' => 'order',
                                    'action' => 'refund',
                                    'sms_to' => 'customer',
                                    'amount_refunded' => $total_refunded_amount
                                );
                                CommonFunctions::getAndSendSms($sms_keywords);
                                /************ END SMS *************/
                            }
                            $status  = Config('constants.status_code.STATUS_SUCCESS');
                            #$response['message'] = 'Refund initiated successfully!';
                            $response['result'] = true;
                            $response['message'] = 'Success';
                            $response['data'] = array(
                                'message' => true,
                                'status' => $action,
                                'order_id' => $request->input('order_id'),
                                'orders' => 0
                            );
                        }else {
                            $response['message'] = $refundResponse['errorMessage'];
                        }
                        ## END CANCEL ORDER Template
                    }else {
                        $response['message'] = 'Invalid refund amount!';
                    }
                }else {
                    $response['message'] = 'Invalid refund Order Request!';
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    }
}
