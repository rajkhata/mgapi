<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Helpers\Netcore;
use DB;
class SubscriptionsController extends Controller {
    public function showSubscrib(Request $request)
    {        
        $email = $request->input('emailid');
        $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
         if(!empty($email) ){
            $email = Subscription::select('id')->where(['email' => $email, 'parent_restaurant_id' => $restaurantId])->get()->toArray();
            if(empty($email)){
                $itemData = ['email' => $request->input('emailid'), 'parent_restaurant_id' => $restaurantId, 'restaurant_id' => $locationId
                ];             
                $menuItemObj = Subscription::create($itemData); 
                $result = 'inserted record';
            }else{
                Subscription::where(['email'=>$request->input('emailid'), 'parent_restaurant_id' => $restaurantId, 'restaurant_id' => $locationId])
                                ->update(['updated_at'=>date('Y-m-d H:i:s')]);    
                $result = 'updated record';
            } 
            $netcoreData = DB::table('netcoreconfig')->select('id', 'apikey', 'listid')->where('list_type', 'S')->where('status', 1)->where('restaurant_id', $restaurantId)->first();          
            if($netcoreData) { 
                /*$config =  array(
                    'apiurl' => 'http://api.netcoresmartech.com/',
                    'apikey' => $netcoreData->apikey,
                    'lisetid' => $netcoreData->listid
                );
                $netcore = new Netcore($config);
                $result = $netcore->upload(array('EMAIL'=>$request->input('emailid')));*/

                $config =  array(
                    'apiurl' => 'http://api.exacttouch.com/API/mailing',
                    'apikey' => $netcoreData->apikey,
                    'lisetid' => $netcoreData->listid
                );
                $netcore = new Netcore($config);
                $result = $netcore->uploadDoublOptin(array('EMAIL'=>$request->input('emailid')));//for double optin
            }
            return response()->json(['data' => $result]);
            
         }
        return response()->json(['data' => 'required field missing']);
    }    
}
