<?php

namespace App\Http\Controllers;

use App\Models\UserOrder;
use Illuminate\Http\Request;

//use DB;
class SentFaxController extends Controller
{

    public function sentFax($orderid)
    {
        $orderData = UserOrder::select('user_orders.id','user_orders.restaurant_id','user_orders.created_at','restaurants.phone','restaurants.call_interval')
        ->where('user_orders.status', 'placed')->where('restaurants.sent_fax',1)->where('user_orders.is_fax_sent',0)
        ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->get()->toArray();                     
        if(isset($orderData[0]) && !empty($orderData[0])){
            
            foreach($orderData as $key => $value){
                $currentDate = \App\Helpers\CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $value['restaurant_id']));
                
                $currentTimestamp =  strtotime($currentDate->format('Y-m-d H:i:s'));
                $orderTimestamp = strtotime($value['created_at']);
                $interval = $value['call_interval']*60;
               
                $sentCallTime = $orderTimestamp+$interval;
               
                if($sentCallTime <= $currentTimestamp){  
                    $data['phone_number'] = $value['phone'];
                    $response = $this->sendCall($data); 
                    $search = "SUCCESS";
                    if(preg_match("/{$search}/i", $response)) {
                       $this->updateCallSent($value['id']);
                       echo $response;
                    }else{
                        echo "Call not sent : Please contact the administrator";
                    }
                    
                    die;
                }else{
                    echo "Wait for time";
                }
            }
        }
            
        die;

    }
    
    
    public function faxContents($orderData){
        $template = "";
        $template .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $template .='<html xmlns="http://www.w3.org/1999/xhtml"><head><style type="text/css">body{margin:0;}</style></head>';
        $template .='<body><center><table width="600"  bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="margin-left:auto; margin-right:auto; margin-top:0;margin-bottom:0;">';
        $template .='<tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px; color: #666666; text-align:left">';
                            
        ########################
        
        $template .='<tr><td><br><p style="font-size: 16px;">';
        $template .='Hey <strong>'.$this->restaurantName.',</strong>';
        $template .='<p>It&#8217;s time to get cooking! <strong>'.$this->name.'</strong> has placed a <?php echo strtolower('.$this->type.') order. See the details below.';
        $template .='<br> <br></p>.</p>';

        $template .='<table width="640" height="161" border="0" cellpadding="0" cellspacing="0" bgcolor="#ececec" style="margin-top: 10px;">';
        $template .='<tr><td width="25">&nbsp;</td><td width="309">&nbsp;</td><td width="81">&nbsp;</td><td width="217">&nbsp;</td><td width="25">&nbsp;</td></tr>';
        $template .='<tr><td>&nbsp;</td><td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px;">';
        
        $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Order Type: <strong>'.$this->orderType.'</strong></td></tr>';
        $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Receipt number: <strong>'.$this->receiptNo.'</strong></td></tr>';
        $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Time of Order: <strong>'.$this->timeOfOrder.'</strong></td></tr>';
        $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Time of Delivery: <strong>'.$this->timeOfDelivery.'</strong></td></tr>';
        
        if(isset($this->address) && !empty($this->address)){
            $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Delivery Address: <strong>'.$this->address.'</strong></td></tr>';
        }
                    
        if(isset($this->phone) && !empty($this->phone)){ 
            $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Phone Number: <strong>+1&nbsp;'.$this->phone.'</strong></td></tr>';
        }
                    
        if (!empty($this->specialInstructions)) { 
            $template .='<tr><td height="25" align="left" valign="top" style="color: #666666;">Special Instructions: <br /> <i>'.$this->specialInstructions.'</i></td></tr>';
        }
        $template .='</table>';

        $template .='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px;">';
        $template .='<tr><td colspan="3" align="left" style="color: #666666;"><img src="'.TEMPLATE_IMG_PATH . 'border.png" width="309" height="1" /></td> </tr>';
        $template .= $this->orderData;
        $template .= '</table>';

        $template .= '</td><td>&nbsp;</td><td valign="top">';

        $template .= '<table width="217" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">';
        $template .= '<tr><td>&nbsp;</td><td colspan="3" align="right" valign="top" style="font-size: 12px; color: #666666;">&nbsp;</td><td width="6%" rowspan="6">&nbsp;</td></tr>';
        $template .= '<tr><td width="7%">&nbsp;</td>
                        <td width="46%" align="right" valign="top"
                            style="font-size: 12px; color: #8f8f8f;">Sub total:</td>
                        <td width="2%" align="left" valign="top"
                            style="font-size: 12px; color: #666666;">&nbsp;</td>
                        <td width="35%" align="left" valign="top"
                            style="font-size: 12px; color: #666666;"><strong> $'. $this->subtotal.'</strong></td></tr>';
        if ($this->dealDiscount != 0) {

            $template .='<tr>
            <td width="7%">&nbsp;</td>
            <td width="46%" align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Deal Discount:</td>
            <td width="2%" align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td width="35%" align="left" valign="top"
                style="font-size: 12px; color: #666666;"><strong>$' . $this->dealDiscount.'</strong></td></tr>';
        }

        if ($this->promocodeDiscount != 0) {
        $template .='<tr>
           <td width="7%">&nbsp;</td>
           <td width="46%" align="right" valign="top"
               style="font-size: 12px; color: #8f8f8f;">Discount:</td>
           <td width="2%" align="left" valign="top"
               style="font-size: 12px; color: #666666;">&nbsp;</td>
           <td width="35%" align="left" valign="top"
               style="font-size: 12px; color: #666666;"><strong>$' . $this->promocodeDiscount.'</strong></td></tr>';
        }

        if ($this->deliveryCharge != 0) { 
        $template .='<tr>
            <td width="7%">&nbsp;</td>
            <td align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Delivery:</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td width="35%" align="left" valign="top"
                style="font-size: 12px; color: #666666;"><strong>$'.$this->deliveryCharge.'</strong></td></tr>';
        }
        if(isset($this->servicetax) && $this->servicetax > 0){
        $template .='<tr>
            <td>&nbsp;</td>
            <td align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Service Charge:</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;"><strong> $' . $this->servicetax.'</strong></td></tr>';
        }
                        
        $template .='<tr>
            <td>&nbsp;</td>
            <td align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Tax:</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;"><strong>$' . $this->tax.'</strong></td></tr>';
        $template .='<tr>
            <td>&nbsp;</td>
            <td align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Tip Amount:</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;"><strong>$' . $this->tipAmount.'</strong></td></tr>';

        $template .='<tr>
            <td>&nbsp;</td>
            <td align="right" valign="top"
                style="font-size: 12px; color: #8f8f8f;">Total:</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #666666;">&nbsp;</td>
            <td align="left" valign="top"
                style="font-size: 12px; color: #ff8208;"><strong>$' . $this->total.'</strong></td></tr>';

        $template .='<tr><td height="15" colspan="5"></td></tr>';

        if ($this->order_pass_through == 0) {
            $template .='<tr><td colspan="5"><img src="'.TEMPLATE_IMG_PATH . 'whitebdr.gif" /></td></tr>';
            $template .='<tr><td colspan="5">&nbsp;</td></tr>';
            $template .='<tr><td>&nbsp;</td><td colspan="3" align="left" valign="top" style="font-size: 12px; color: #666666;">
                <strong>Payment:</strong><br>Credit Card<br>'.$this->cardType.' (XXXX-XXXX-XXXX-'.$this->cardNo.' /<br> Exp: '.$this->expiredOn.')</td><td>&nbsp;</td></tr>';
        }
                    
        $template .='<tr><td colspan="5">&nbsp;</td></tr></table>';

        $template .='</td><td>&nbsp;</td></tr><tr><td height="20"></td><td></td><td></td><td></td><td></td></tr></table>';

        $template .='</td></tr>';

        $template .='<tr><td><p><br />Best,<br />The Munch Ado Crew<br /><br /></p></td></tr>';
        
        #######################      
        
        //echo $this->content;                          
                            
        $template .='</table></td></tr></table></center></body></html>';
    }
    
    
     public function orderDetail( $order_id) {
        
        $orders = array();
        $total_counts = 0;
        $statusArray = array("placed");
        
        $orderData = UserOrder::select('users.fname as ufname','users.lname as ulname','users.email as uemail','users.mobile as umobile','users.display_pic_url','restaurants.id','user_orders.user_id','restaurants.restaurant_name','restaurants.delivery_provider_apikey','restaurants.rest_code','restaurants.address as raddress','restaurants.zipcode as rzipcode','restaurants.street as rstreet' ,'restaurant_logo_name','user_orders.id','user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount','user_orders.created_at','name_on_card','card_type','expired_on','billing_zip','expired_on',
                   'user_orders.delivery_time','user_orders.delivery_date','user_orders.status', 'user_order_details.item','user_order_details.id as user_order_detail_id', 'user_order_details.quantity', 'user_order_details.item_size', 'user_order_details.user_order_id','delivery_time','delivery_date','restaurants_comments','user_comments','is_reviewed','review_id','payment_receipt','city','user_orders.fname','user_orders.lname','user_orders.email','user_orders.phone','menu_items.name', 'menu_items.status as menu_status','tip_percent', 'user_orders.cod', 'host_name', 'user_orders.order_pass_through', 'user_orders.city_id', 'manual_update', 'ds_order_id', 'latitude', 'longitude','restaurants.phone as rphone', 'user_orders.tax', 'user_orders.zipcode', 'header_color_code', 'special_checks', 'restaurant_image_name', 'apt_suite', 'tip_amount', 'user_orders.delivery_charge', 'deal_discount', 'promocode_discount', 'redeem_point', 'pay_via_point', 'pay_via_card','facebook_url', 'twitter_url','card_number','order_amount','user_orders.address','user_orders.address2','address_label','user_orders.state','user_order_details.menu_id','user_order_details.menu_json','unit_price',
                   'special_instruction','menu_items.status as mstatus', 'item_price_desc', 'user_orders.is_byp', 'user_orders.is_guest','user_order_details.is_byp as byp_new','user_orders.is_read')

                    ->where('user_orders.id', $order_id)                    
                    ->leftJoin('user_order_details', 'user_order_details.user_order_id', '=', 'user_orders.id')
                    ->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')
                    ->leftJoin('users', 'user_orders.user_id', '=', 'users.id')
                    ->leftJoin('menu_items', 'user_order_details.menu_id', '=', 'menu_items.id')
                    ->get();

                   

        if($orderData) {
            $orderDataObj=$orderData;
            $orderData = $orderData->toArray();

//            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $check_token['restaurant_id']));
//            if(is_object($currentDateTimeObj)) {
//                $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
//                $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
//            }else {
//                $restcurrentDateTime['current_date'] = date('Y-m-d');
//                $restcurrentDateTime['current_time'] = date('H:i:00');
//            }

            
            


        foreach ($orderData as $key => $order) {                            

            $curorderobj=$orderDataObj[$key];
            $curorderobj->id=$curorderobj->user_order_detail_id;
            $add_data = \App\Helpers\CommonFunctions::getAddonsAndModifiers($curorderobj);
            $add_data_returned = empty($add_data) ? (object)[] : $add_data;
            $addon_modifier=!empty($curorderobj->user_order_detail_id)? $add_data_returned : (object)[];

            $addon_list = array();
            if(array_key_exists($order['user_order_id'], $orders)) {
                $item_count = count($orders[$order['user_order_id']]['item_list']);
                
                $menuJsonDecoded = json_decode($order['menu_json'], true) ?? null;
                if($order['is_byp'] == 0 && $menuJsonDecoded) {
                    foreach ($menuJsonDecoded as $premenu) {
                        if(isset($premenu['items']) && $premenu['items']) {
                            $addon_label = $premenu['label'];
                            foreach ($premenu['items'] as $cust_menu) {
                                if($cust_menu['price'] && $cust_menu['default_selected']) {
                                    $addon_list[] = array(
                                        "addons_id"=> $cust_menu['id'],
                                        "addon_label"=> $addon_label,
                                        "addon_name"=> $cust_menu['label'],
                                        "addon_price"=>  $cust_menu['price'],
                                        "addons_total_price"=> (string) ($order['quantity'] * $cust_menu['price']),
                                        "addon_quantity"=>  (string) $order['quantity']
                                    );
                                }
                            }
                        }
                    }
                }
                              
                $is_byp = 0;
                if($order['byp_new'] == 1 ) {
                    $addon_modifier = json_decode($order['menu_json'], true) ?? null;
                    $is_byp = 1;
                }
                               
                $orders[$order['user_order_id']]['item_list'][$item_count] =  array(
                    "order_item_id"=> $order['menu_id'],
                    "item_name"=> $order['item'],
                    "item_qty"=>  $order['quantity'],
                    "item_size"=>  $order['item_size'],
                    "unit_price"=>  $order['unit_price'],
                    "item_special_instruction"=>  $order['special_instruction'],
                    "item_price_desc"=>  $order['item_price_desc'],
                    "item_status"=>  $order['mstatus'],
                    "addons_list"=> $addon_list,
                    "addon_modifier"=> $addon_modifier,
                    "is_byp" => $is_byp,
                );


            }else {
                $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['restaurant_id'], 'datetime' => $order['created_at'])));

                $delivery_time_left_hour = explode('.',(strtotime($order['delivery_date'].' '.$order['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
                $orderArchived = false;
                if(in_array($order['status'], array("archived", "cancelled", "arrived", "sent", "rejected", "refunded"))) {
                    $orderArchived = true;
                }
                if($order['user_id']) {
                    $total_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $check_token['restaurant_id'])->where('user_orders.user_id', $order['user_id'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
                }else {
                    $total_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $check_token['restaurant_id'])->where('user_orders.email', $order['email'])->orderBy('user_orders.id', 'DESC')->whereNotIn('status', ['pending'])->count();
                }
                //@10-09-2018 Addon by RG
                $menuJsonDecoded = json_decode($order['menu_json'], true) ?? null;
                if($order['is_byp'] == 0 && $menuJsonDecoded) {
                    foreach ($menuJsonDecoded as $premenu) {
                        if(isset($premenu['items']) && $premenu['items']) {
                            $addon_label = $premenu['label'];
                            foreach ($premenu['items'] as $cust_menu) {
                                if($cust_menu['price'] && $cust_menu['default_selected']) {
                                    $addon_list[] = array(
                                        "addons_id"=> $cust_menu['id'],
                                        "addon_label"=> $addon_label,
                                        "addon_name"=> $cust_menu['label'],
                                        "addon_price"=>  $cust_menu['price'],
                                        "addons_total_price"=> (string) ($order['quantity'] * $cust_menu['price']),
                                        "addon_quantity"=>  (string) $order['quantity']
                                    );
                                }
                            }
                        }
                    }
                }
            #Addons End#
            $cust_fname = $order['fname'];
            $cust_lname = $order['lname'];
            $cust_email = $order['email'];
            $phone_no = $order['phone'];
            if($order['is_guest'] == 0) {
                $cust_fname = $order['ufname'];
                $cust_lname = $order['ulname'];
                $cust_email = $order['uemail'];
                $phone_no = $order['umobile'] ? $order['umobile'] : $order['phone'];
            }else {
                $order['fname']  = '';
                $order['lname']  = '';
                $order['email']  = '';
                $order['phone'] = '';
            }
            if($order['order_type'] == 'delivery') {
                $del_address = $order['address'].' '.$order['address2'].' '.$order['city'].' '.$order['state'].' '.$order['zipcode'];
                if($order['address_label']) {
                    $del_address = $order['address_label'].' '.$del_address;
                }
            }else {
                $del_address = $order['restaurant_name'].' '.$order['raddress'].' '.$order['rstreet'].' '.$order['rzipcode'];
            }
            /*added by jawed*/
            $is_byp = 0;
            if($order['byp_new'] == 1) {
                $addon_modifier = json_decode($order['menu_json'], true) ?? null;
                $is_byp = 1;
            }
                                /*added by jawed*/
                                $orders[$order['user_order_id']] = array(
                                    'id' => (string) $order['id'],
                                    'user_id' => $order['user_id'],
                                    'customer_first_name' => $cust_fname,
                                    'customer_last_name' => $cust_lname,
                                    'email' => $cust_email,
                                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                                    'status' => $order['status'],
                                    'order_type1' => 'I',
                                    'order_type2' => 'p',
                                    'is_byp' => $order['is_byp'],
                                    'is_read' => $order['is_read'],
                                    'delivery_date' => $order['delivery_date'].' '.date('H:i:s', strtotime($order['delivery_time'])),
                                    'order_type' => $order['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                                    'special_instruction' => $order['user_comments'],
                                    'user_comments' => $order['user_comments'],
                                    'restaurant_id' => $order['restaurant_id'],
                                    'tip_percent' => $order['tip_percent'],
                                    'redeem_point' => $order['redeem_point'],
                                    'pay_via_point' => $order['pay_via_point'],
                                    'pay_via_card' => $order['pay_via_card'],
                                    'total_amount' => $order['total_amount'],
                                    'payment_receipt' => $order['payment_receipt'],
                                    'is_reviewed' => $order['is_reviewed'],
                                    'review_id' => (int)$order['review_id'] ? $order['review_id'] : 0,
                                    'cod' => $order['is_reviewed'],
                                    'created_at' =>  date('Y-m-d H:i:s', $local_timestamp),
                                    'host_name' => $order['host_name'],
                                    'order_pass_through' => $order['order_pass_through'],
                                    'city_id' => $order['city_id'],
                                    'restaurants_comments' => $order['restaurants_comments'] ? $order['restaurants_comments'] : '',
                                    'manual_update' => $order['manual_update'],
                                    'ds_order_id' => $order['ds_order_id'],
                                    'latitude' => $order['latitude'],
                                    'longitude' => $order['longitude'],
                                    'pay_via_cash' => '0',
                                    'orderArchived' => $orderArchived,
                                    'delivery_service' => $order['delivery_provider_apikey'] ? 1 : 0,
                                    'service_provider' => '',
                                    "pos" => "1",
                                    "phone_no" => $phone_no,
                                    "orderStatus" => $order['status'],
                                    "sales_tax" => $order['tax'],
                                    "minimum_delivery" => "0.00",
                                    "current_date" => $restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'],
                                    'restaurant_name' => $order['restaurant_name'],
                                    "restaurant_address" => $order['raddress'],
                                    "restaurant_city" => $order['city'],
                                    "restaurant_state_code" =>  "NY",
                                    "restaurant_zipcode" => $order['rzipcode'],
                                    "rest_code" => $order['rest_code'],
                                    "restaurantLogo" => $order['restaurant_logo_name'],
                                    "social_link" => array(
                                        'i_fb.png#Facebook' =>  (string) $order['facebook_url'],
                                        'i_twtr.png#Twitter' => $order['twitter_url'],
                                    ),
                                    "header_color_code" => $order['header_color_code'],
                                    "time_of_delivery" => date('M d, Y', strtotime($order['delivery_date'])).' @ '.date('h:i A', strtotime($order['delivery_time'])),
                                    "delivery_hour" => '00',
                                    "delivery_minutes" => '00',
                                    'time_of_order' => date('M d, Y @ h:i A', $local_timestamp),
                                    "delivery_time_left" => 0,
                                    'delivery_time_left_hour' => $delivery_time_left_hour,
                                    'created_time' => date('M d, Y', $local_timestamp),
                                    "special_checks" => $order['user_comments'],
                                    "restaurant_image_name" => $order['restaurant_image_name'],
                                    "my_payment_details" => array(
                                        "card_name" => $order['name_on_card'],
                                        "card_number" => $order['card_number'],
                                        "card_type" => $order['card_type'],
                                        "expiry_year" => '2022',
                                        "expiry_month" => '10',
                                        "billing_zip" => $order['zipcode'],
                                    ),
                                    "my_delivery_detail" => array(
                                        "first_name" => $order['fname'],
                                        "last_Name"  => $order['lname'],
                                        "email"      => $order['email'],
                                        "address_label" => $order['address_label'],
                                        "city"       => $order['city'],
                                        "apt_suite"  => $order['apt_suite'],
                                        "state"      => $order['state'],
                                        "phone"      => $order['phone'],
                                        "delivery_address" => $del_address,
                                        "address" => $del_address,
                                        "zipcode" => $order['zipcode'],
                                    ),
                                    "order_amount_calculation" => array(
                                        "subtotal"=>  $order['order_amount'],
                                        "tax_amount"=> $order['tax'],
                                        "tip_amount"=>  $order['tip_amount'],
                                        "delivery_charge"=> $order['delivery_charge'],
                                        "discount"=> $order['deal_discount'],
                                        "promocode_discount"=> $order['promocode_discount'],
                                        "redeem_point"=> $order['redeem_point'],
                                        "pay_via_point"=> $order['pay_via_point'],
                                        "pay_via_card"=> $order['pay_via_card'],
                                        "pay_via_cash"=> '0',
                                        "total_order_price"=> $order['total_amount'],
                                        'refunded_amount' => number_format($refunded_amount, 2),
                                        'refund_history' => $refund_reason
                                    ),
                                    'item_list' => array(
                                        array(
                                            "order_item_id"=> $order['menu_id'],
                                            "item_name"=> $order['item'],
                                            "item_qty"=>  $order['quantity'],
                                            "item_size"=>  $order['item_size'],
                                            "unit_price"=>  $order['unit_price'],
                                            "item_special_instruction"=>  $order['special_instruction'],
                                            "item_price_desc"=>  $order['item_price_desc'],
                                            "item_status"=>  $order['mstatus'],
                                            "addons_list"=> $addon_list,
                                            "addon_modifier"=> $addon_modifier,
                                            "is_byp" => $is_byp,
                                        )
                                    ),
                                    'user_image' => $order['display_pic_url'],
                                    'user_default_image' => '',
                                    'user_activity' => array(
                                        "total_user_order" => $total_counts,
                                        "total_user_review" => "0",
                                        "total_user_checkin" => "0",
                                        "total_user_reservation" => "0"
                                    ),
                                    'fource_update' => $update_app

                                );
                            }
                        }
                    }
                    if($orders) {
                        $orders = array_values($orders);
                        $orders['0']['item_list'] = array_values($orders['0']['item_list']);
                        $orders = $orders['0'];
                    }
                    $response['data'] = $orders;
                }
}