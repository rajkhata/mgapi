<?php

namespace App\Http\Controllers;

use App\Models\UserAuth;
use App\Models\UserOrder;
use App\Models\UserOrderDetail;
use App\Models\Restaurant;
use App\Models\DeliveryServicesLogs;
use Illuminate\Http\Request;

class CreatePosOrderController extends Controller {

    private $orderId;
    private $posDetails = [];
    private $restaurantId;
    private $requestedData = [];
    private $reciept;
    private $posResponseId=0;
    private $posResponseData;
    private $orderCreationSuccess;
    private $product_type;

    public function create(Request $request) {
        $ms = microtime(true);
        $error = "No Response";
        $accessToken = $request->bearerToken() ?? '';
        if ($accessToken) {
            $userAuth = UserAuth::where(['access_token' => $accessToken])->first();
        } else {
            $guestHeader = $request->header('Authorization');
            $guestToken = "";
            if (!empty($guestHeader)) {
                $guestToken = explode(' ', $guestHeader)[1];
            }

            $userAuth = UserAuth::where(['guest_token' => $guestToken])->first();
        }

        $this->orderId = $request->input('order_id');
        
        if ($userAuth) {
            if (!$this->orderId) {
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => "Order not found", 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }

            $userOrder = UserOrder::where(['id' => $this->orderId, 'status' => 'placed', 'pos_status' => 0])->first();
            if ($userOrder) {
                $this->restaurantId = $userOrder->restaurant_id;
                $this->reciept = $userOrder->payment_receipt;
                $this->getPostDetails();
                $this->product_type = $userOrder->product_type;
                if (empty($this->posDetails)) {
                    $me = microtime(true)-$ms;
                    return response()->json(['data' => [], 'error' => "Order not found for pos system", 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }


                $customer = [
                    "id" => (string) $userOrder->user_id,
                    "name" => (string)$userOrder->fname . " " . $userOrder->lname,
                    "phone" => (string)$userOrder->phone,
                    "address1" => !empty($userOrder->delivery_address) ? (string)$userOrder->delivery_address : "",
                    "address2" => !empty($userOrder->apt_suite) ? (string)$userOrder->apt_suite : "",
                    "city" => !empty($userOrder->city) ? (string)$userOrder->city : "",
                    "state" => !empty($userOrder->state) ? (string)$userOrder->state : "",
                    "zip" => !empty($userOrder->zipcode) ? (string)$userOrder->zipcode : "",
                    "cross-street" => "",
                    "special-instructions" => !empty($userOrder->user_comments) ? (string)$userOrder->user_comments : "",
                    "longitude" => !empty($userOrder->longitude) ? (string) $userOrder->longitude : "",
                    "latitude" => !empty($userOrder->latitude) ? (string) $userOrder->latitude : ""
                ];

                if ($userOrder->deal_discount) {
                    $discount = $userOrder->deal_discount;
                } elseif ($userOrder->promocode_discount) {
                    $discount = $userOrder->promocode_discount;
                } else {
                    $discount = 0;
                }

                $info = [
                    "id" => (string) $userOrder->id,
                    "scheduled-dt" => (string) strtotime($userOrder->delivery_date . " " . $userOrder->delivery_time),
                    "estimated-dt" => (string) strtotime($userOrder->delivery_date . " " . $userOrder->delivery_time),
                    "confirmation-number" => (string)$userOrder->payment_receipt,
                    "service-type" => ($userOrder->order_type == "carryout") ? "Pick-Up" : "Delivery",
                    "payment-is-cash" => (string) false,
                    "tip-payment-is-cash" => (string) false,
                    "payment-type" => "card",
                    "tip-payment-type" => "card",
                    "subtotal" => (string)$userOrder->order_amount,
                    "delivery-charge" => (string)$userOrder->delivery_charge,
                    "sales-tax" => (string)$userOrder->tax,
                    "tip" => (string)$userOrder->tip_amount,
                    "total" => (string)$userOrder->total_amount,
                    "coupon-description" => "",
                    "coupon-amount" => (string) $discount
                ];

                $restaurant = [
                    "id" => (string) $userOrder->restaurant_id,
                    "name" => (string)$userOrder->restaurant_name,
                    "billing-comment" => ""
                ];
                 $items = [];
                if($this->product_type=="food_item"){
                $userOrderDetails = UserOrderDetail::where(['user_order_id' => $userOrder->id])->get()->toArray();
               
                // print_R($userOrderDetails);
                $modkey = 0;
                
                
                foreach ($userOrderDetails as $key => $menu) {
                    //print_r($mvalue['menu_json']);
                    $items[$key]['mods'] = [];
                    $items[$key]['price'] = (string)$menu['unit_price'];
                    if(!in_array($menu['item_size'], array("NA","FULL"))){                        
                        $items[$key]['price'] = "0.00";  
                        $items[$key]['mods'][$modkey]['id'] = (string) $menu['menu_id']."-".substr($menu['item_size'],0,1);
                        $items[$key]['mods'][$modkey]['name'] = (string)$menu['item_size'];
                        $items[$key]['mods'][$modkey]['portion'] = "";
                        $items[$key]['mods'][$modkey]['action'] = "";
                        $items[$key]['mods'][$modkey]['group-name'] = (string)$menu['item'];
                        $items[$key]['mods'][$modkey]['group-id'] = (string) $menu['menu_id'];
                        $items[$key]['mods'][$modkey]['price'] = (string)$menu['unit_price'];
                        $items[$key]['mods'][$modkey]['quantity'] = (string) $menu['quantity'];
                        $modkey++;
                    }
                    
                    $items[$key]['id'] = (string) $menu['menu_id'];
                    $items[$key]['name'] = (string)$menu['item'];
                    $items[$key]['group-name'] = "";
                    $items[$key]['group-id'] = "";
                    $items[$key]['quantity'] = (string) $menu['quantity'];
                    
                    $items[$key]['notes'] = (string)$menu['special_instruction'];
                    
                    if ($menu['menu_json'] != "null" && !empty($menu['menu_json'])) {
                        $addonsDetails = json_decode($menu['menu_json'], 1);
                        foreach ($addonsDetails as $addonkey => $addon) {                            
                            $modkey = ($modkey > 0)?$modkey+1:0;                             
                            foreach ($addon['items'] as $addonitemkey => $addonvalue) {                               
                                if($addonvalue['default_selected']==1){                                                                                                            
                                    $items[$key]['mods'][$modkey]['id'] = (string) $addonvalue['id'];
                                    $items[$key]['mods'][$modkey]['name'] = (string)$addonvalue['label'];
                                    $items[$key]['mods'][$modkey]['portion'] = ($addonvalue['sides'] != null && !empty($addonvalue['sides'])) ? (string)$addonvalue['sides'] : "";
                                    $items[$key]['mods'][$modkey]['action'] = "";
                                    $items[$key]['mods'][$modkey]['group-name'] = (string)$menu['item'];
                                    $items[$key]['mods'][$modkey]['group-id'] = (string) $menu['menu_id'];
                                    $items[$key]['mods'][$modkey]['price'] = (string)$addonvalue['price'];
                                    $items[$key]['mods'][$modkey]['quantity'] = (string) $addonvalue['quantity'];
                                   
                                }
                                $modkey++;
                            }
                            
                        }
                    } 
                }
                }
                $this->prepareItemes($items);
                $this->requestedData['customer'] = $customer;
                $this->requestedData['info'] = $info;
                $this->requestedData['restaurant'] = $restaurant;
                $this->requestedData['items'] = $items;
                if (!$this->createPosOrder()) {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => $this->requestedData, 'error' => $this->orderCreationSuccess, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }
                $me = microtime(true) - $ms;
                return response()->json(['data' => $this->requestedData, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
                $me = microtime(true) - $ms;
                return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            }
        } else {
            $me = microtime(true) - $ms;
            $error = 'Access token not valid';
            return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => [], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    private function updatePosStatus() {
        UserOrder::where(['id' => $this->orderId])->update(['pos_status' => 1]);
        return true;
    }

    private function getPostDetails() {
        $this->posDetails = Restaurant::join('delivery_services', 'restaurants.delivery_provider_id', '=', 'delivery_services.id')->
                        where('restaurants.id', '=', $this->restaurantId)->select('restaurants.delivery_provider_id', 'restaurants.delivery_provider_apikey', 'delivery_services.order_creation_url')->get()->toArray();
        return true;
    }

    private function createPosOrder() {
        $posData = json_encode($this->requestedData); 
        $maCurl = curl_init();
        curl_setopt($maCurl, CURLOPT_URL, $this->posDetails[0]['order_creation_url']);
        curl_setopt($maCurl, CURLOPT_POST, 1);
        curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json', "api-key:" . $this->posDetails[0]['delivery_provider_apikey']));
        //curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json', "api-key:bb80ffa2029e6424"));
        curl_setopt($maCurl, CURLOPT_POSTFIELDS, $posData);
        curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
        $this->posResponseData = curl_exec($maCurl);
        $response = json_decode($this->posResponseData, true);

        if (isset($response['id']) && !empty($response['id'])) {
            $this->posResponseId = $response['id'];
            $this->updatePosStatus();
            $this->createPosLog();
            return true;
        } else {
            if(isset($response['errors'])){            
                $this->orderCreationSuccess = $response['errors'];
            }elseif(isset($response['msg'])){
                $this->orderCreationSuccess = $response['msg'];
            }
            $this->createPosLog();
            return false;
        }
    }

    private function createPosLog() {

        $logData = [
            'order_receipt' => $this->reciept,
            'order_source_id' => $this->posResponseId,
            'order_key' => "",
            'provider_id' => $this->posDetails[0]['delivery_provider_id'],
            'order_id' => $this->orderId,
            'restaurant_id' => $this->restaurantId,
            'request_data' => json_encode($this->requestedData),
            'response_data' => $this->posResponseData,
            'requested_by' => "createOrder"
        ];
        DeliveryServicesLogs::create($logData);

        return true;
    }
    
    private function prepareItemes(&$items){
        foreach($items as $key =>$value){            
            if(!empty($value['mods'])){
                $items[$key]['mods'] = array();
                foreach($value['mods'] as $mkey =>$mval){
                    $items[$key]['mods'][] = $mval;
                }
            }            
        }   
        return true;
    }

}
