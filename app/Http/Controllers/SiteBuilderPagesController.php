<?php

    namespace App\Http\Controllers;


    use Illuminate\Http\Request;
    use App\Models\SiteBuilderPages;
    use App\Helpers\CommonFunctions;

    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Collection;

    use Carbon\Carbon;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Input;


    class SiteBuilderPagesController extends Controller
    {

/*
        public function savePage(Request $request) {

           try {

               DB::beginTransaction();

               $locId = $request->header('X-location');
               $parent_id = $request->header('X-restaurant');

                $this->validate($request, [
                'title' => 'required|unique:sb_pages',
                ]);

                $data = $request->all();

                $dataToSave=[
                    'title' =>$data['title'],
                    'slug' =>CommonFunctions::slugify($data['title']),
                    'meta_description' =>isset($data['meta_description'])?$data['meta_description']:NULL,
                    'meta_keyword' => isset($data['meta_keyword'])?$data['meta_keyword']:NULL,
                    'description' => isset($data['description'])?$data['description']:NULL,
                    'status' => isset($data['status'])?$data['status']:1,
                    'restaurant_id' => $parent_id,
                ];

                SiteBuilderPages::create($dataToSave);

                DB::commit();

                return response()->json(['success' => true, 'message' =>'Page has been created successfully.'],  Config('constants.status_code.STATUS_SUCCESS'));

            } catch (\Exception $e) {

                DB::rollback();
                return response()->json(['success' => false, 'message' => $e->getMessage()], Config('constants.status_code.BAD_REQUEST'));

            }
        }*/

        public function getPages(Request $request) {

            $locId = $request->header('X-location');
            $parent_id = $request->header('X-restaurant');

            $pages = SiteBuilderPages::where(['restaurant_id' => $locId,'status' => 1])->orderBy('sort_order', 'ASC')->get(['title','slug','status','restaurant_id','user_group_promotion','include_in_navigation','sort_order']);

            if(isset($pages) and count($pages)){

                return response()->json(['data' => $pages,'success_msg' =>'All pages'], Config('constants.status_code.STATUS_SUCCESS'));

            } else {
                 return response()->json(['data'=>'','error' => 'No page found'], Config('constants.status_code.STATUS_SUCCESS'));
            }
        }

        public function getPage(Request $request,$slug) {

            $parent_id = $request->header('X-restaurant');
            $locId = $request->header('X-location');

            $page = SiteBuilderPages::where(['restaurant_id' => $locId,'status' => 1,'slug'=>$slug])->first();

            if (  $bearerToken = $request->bearerToken()) {
                $user_group_id=$user_group_id??1;
            } else {
                $user_group_id=$user_group_id??3;
            }

            if(isset($page) and !empty($page)){
		if(!empty($page->meta_tags)){
                    $page->meta_tags=json_decode($page->meta_tags,1);
		}else{
			$page->meta_tags=[];
		}
                if(!empty($page->user_group_promotion)){
                    $user_group_id=$page->user_group_promotion;
                }
                $newbanners=[];
                if(!empty($page->promo_banners)){
                    $banners=json_decode($page->promo_banners,1);
                    $l=0;
                    foreach ($banners as $i=>$banner){
			if(isset($banner['UserGroup']) && ($banner['UserGroup']==$user_group_id)){
			    foreach($banner as $key=>$b){
				$l = ((int)$banner['SortOrder'] - 1);
				$l = ($l<0)?0:$l;
				$k = $banner['Position'];
				$j = $banner['Display'];
				if($key=="SortOrder" || $key=="Position" || $key=="Display" || $key=="AltText" || $key=="Type"  || $key=='Link'){
					$newbanners[$user_group_id][$k][$l][$key]=$b;
				}if($key=="Value"){
							$j = "Web";
							$key = "Value";
							$newbanners[$user_group_id][$k][$l][$key][$j]="/".$b;
							//$k++;
				}if($key=="Value_Mobile"){
							$j = "Mobile";
							$key = "Value";
							$newbanners[$user_group_id][$k][$l][$key][$j]="/".$b;
							//$k++;
				}
				
			    }
                        }
			
                    }$newbanners1=[]; 
		    $i=0; 
		    foreach($newbanners as $b){
			foreach($b as $key=>$b1){
			   foreach($b1 as $key=>$b2){
			   	$newbanners1[]=$b2;$i++;
			   }
			    
			}
			
		    }	
                    //$page->promo_banners=json_encode($newbanners);
		    $page->promo_banners=$newbanners1;
		    /// all banners
		    $l=0;
		    foreach ($banners as $i=>$banner){
			 foreach($banner as $key=>$b){
			    	$l = ((int)$banner['SortOrder'] - 1);
			   	$l = ($l<0)?0:$l;
				$k = $banner['Position'];
				//$j = $banner['Display'];
				$user_group_id = $banner['UserGroup'];
				if($key=="SortOrder" || $key=="Position" || $key=="AltText" || $key=="Type"  || $key=='Link' || $key=='UserGroup'){
							$newbanners[$user_group_id][$k][$l][$key]=$b;
				}if($key=="Value"){
							$j = "Web";$key = "Value";
							$newbanners[$user_group_id][$k][$l][$key][$j]="/".$b;
							//$k++;
				}if($key=="Value_Mobile"){
							$j = "Mobile";$key = "Value";
							$newbanners[$user_group_id][$k][$l][$key][$j]="/".$b;
							//$k++;
				}
			 }
		     }
		     $newbanners1=[]; 
		    // print_r($newbanners);
		     $i=0;
		     foreach($newbanners as $groupId=>$b){ 
			$j=0;
			
			foreach($b as $position=>$b1){
				
				 foreach($b1 as $key=>$b2){
					//$b3['id']=$k;
					//$b3['banner']=$b2;
					$newbanners1[$i]['id']=$groupId;
					$newbanners1[$i]['banner'][]=$b2;
				} 
			$j++;		    
			}$i++;
		    }
		    $page = $page->toArray();
		    $page['allbanners']	=$newbanners1;	
		    //$allBanner = isset($newbanners1)?$newbanners1:null;		
                }

                return response()->json(['data' => $page,'success_msg' =>'Page found'], Config('constants.status_code.STATUS_SUCCESS'));

            } else {
                return response()->json(['data'=>'','error' => 'No page found'], Config('constants.status_code.STATUS_SUCCESS'));
            }
        }


    }
