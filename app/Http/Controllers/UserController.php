<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Models\UserAuth;
use Illuminate\Support\Facades\Hash;
use App\Models\UserSocial;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['checkUser']]);
    }

    public function showAllUsers()
    {
        $ms = microtime(true);
        $userData = User::all();
        $me = microtime(true) - $ms;

        return response()->json(['data' => $userData, 'error' => null, 'xtime' => $me], 200);
    }

    public function showOneUser(Request $request, $id)
    {
        $restaurantId = $request->header('X-restaurant');
        $ms = microtime(true);
        $userData = User::find($id);
        $error = '';
        if(!$userData) {
            $error = 'Not a valid user.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => [$userData], 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        //@17-08-2018 by RG check social login       
        $checkUserSocial = UserSocial::where(['user_id' => $id, 'parent_restaurant_id' => $restaurantId])->first();
        if ($checkUserSocial) {
            $userData->show_change_password_option = false;
        }else {
            $userData->show_change_password_option = true;
        }       
        $me = microtime(true) - $ms;
        return response()->json(['data' => [$userData], 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    public function create(Request $request)
    {
        $ms = microtime(true);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);

        $user = User::create($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data' => $user, 'error' => null, 'xtime' => $me], 201);
    }

    public function delete($id)
    {
        $ms = microtime(true);
        User::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data' => 'Deleted successfully', 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

    /**
     * Update user profile data, after sign up
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $ms = microtime(true);
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $userAuth = '';
        $userID = '';
        $authToken = $request->bearerToken();
        if (!empty($authToken)) {
            $userAuth = UserAuth::where(['access_token' => $authToken, 'parent_restaurant_id' => $restaurantId])->first();
            if($userAuth) {        
                $userMobile = User::where(['mobile' => trim($request->input('mobile')), 'restaurant_id' => $restaurantId])->first();
        	    if(!$userMobile){
                    $userID = $userAuth->user_id;
        		    $me = microtime(true) - $ms;

        		    $validation = Validator::make($request->all(), [
        		        'fname' => 'required|string|max:255',
        		        'mobile' => 'required|numeric',
        		        'email' => 'required|string|email|max:255',
        		    ]);
        		    if (!$validation->fails()) { 

        		        /*$dobInput = $request->input('dob');
        		        $dob      = null;
        		        if (!empty($dobInput)) {
        		            $dob = Carbon::parse($request->input('dob'))->format('Y-m-d');
        		        } */

        		        $data = [
				    'surname' => $request->input('surname'),
        		            'fname' => $request->input('fname'),
        		            'lname' => $request->input('lname'),
        		            'mobile' => $request->input('mobile'),
        		            'email' => $request->input('email'),
        		        ];
                        if($request->has('dob') && $request->input('dob')) {
                            $data['dob'] = date('Y-m-d', strtotime($request->input('dob')));   
                        }                        
        		        $user = User::findOrFail($userID);                        
        		        $user->update($data);
                        //@17-08-2018 by RG check social login       
                        $checkUserSocial = UserSocial::where(['user_id' => $userID, 'parent_restaurant_id' => $restaurantId])->first();
                        if ($checkUserSocial) {
                            $user->show_change_password_option = false;
                        }else {
                            $user->show_change_password_option = true;
                        }   
        		        $me = microtime(true) - $ms;

        		        return response()->json(['data' => $user, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        		    } else {
        		        $me = microtime(true) - $ms;
        		        return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        		    }
        	    }else {
        		        $me = microtime(true) - $ms;
        		        return response()->json(['data' => null, 'error' => 'Mobile number is already assoicated with another account.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        	    }
            }else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
            }
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => 'Access token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /*User update profile function */
    public function updateProfile(Request $request)
    {
        $ms = microtime(true);
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $userAuth = '';
        $userID = '';
        $authToken = $request->bearerToken();
        if (!empty($authToken)) {
            $userAuth = UserAuth::where(['access_token' => $authToken, 'parent_restaurant_id' => $restaurantId])->first();
            if($userAuth) {
                $userID = $userAuth->user_id;
                $me = microtime(true) - $ms;

                $validation = Validator::make($request->all(), [
                    'fname' => 'required|string|max:255',
                    'mobile' => 'required|max:15',
                    'email' => 'required|string|email|max:255',
                ]);
              
                if (!$validation->fails()) {
                    $userMobile = array();
                    if($request->input('mobile')) { 
                        $userMobile = User::where(['mobile' => trim($request->input('mobile')), 'restaurant_id' => $restaurantId])->where('id', '<>', $userID)->first();
                    }
                    if($userMobile) {
                        $validation->errors()->add('mobile', 'This mobile is associated with other account. Please add another mobile!');
                        $errors = $validation->errors()->toArray();
                        $me = microtime(true) - $ms;
                        return response()->json(['data' => null, 'error' => $errors, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    }
                    $data = [
			'surname' => $request->input('surname'),
                        'fname' => $request->input('fname'),
                        'lname' => $request->input('lname'),
                        'mobile' => $request->input('mobile'),
                        'email' => $request->input('email'),
                    ];

                    if($request->has('dob') && $request->input('dob')) {
                        $data['dob'] = date('Y-m-d', strtotime($request->input('dob')));   
                    }  

                    $user = User::findOrFail($userID);
                    $user->update($data); 
                    $me = microtime(true) - $ms;
                    $user['show_change_password_option'] = true;
                    return response()->json(['data' => $user, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                } else { 
                    $errors = $validation->errors()->toArray();
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => $errors, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
            }else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));  
            }

        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => 'Access token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    /* End function */

    public function resetPassword(Request $request)
    {
        $ms = microtime(true);
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $userAuth = '';
        $userID = '';
        $passMatch = '';
        $authToken = $request->bearerToken();
        if (!empty($authToken)) {
            
            $userAuth = UserAuth::where(['access_token' => $authToken, 'parent_restaurant_id' => $restaurantId])->first();
            if($userAuth) {
                $userID = $userAuth->user_id;
                $validation = Validator::make($request->all(), [
                    'password' => 'required|min:8|max:64',
                    'oldpassword' => 'required|min:8|max:64',
                ]);

                if (!$validation->fails()) {
                    $user = User::findOrFail($userID);
                    if($user) {
                        //$OldPassword = Hash::make($request->input('oldpassword'));
                        $OldPassword = $request->input('oldpassword');
                        //$passMatch = User::where('password', '=', $OldPassword)->first();
                        if (Hash::check($OldPassword, $user->password)) {
                            $user->fill([
                                'password' => Hash::make($request->input('password'))
                            ])->save();

                            $me = microtime(true) - $ms;
                            return response()->json(['data' => $user, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                        } else {
                            $me = microtime(true) - $ms;
                            return response()->json(['data' => null, 'error' => 'Old password does not match.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                        }
                    } else{
                        $me = microtime(true) - $ms;
                        return response()->json(['data' => null, 'error' => 'User not found.', 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                    }
                } else {
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => null, 'error' => $validation->errors(), 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
                }
            } else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
            }
        }
        else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => null, 'error' => 'Access token is not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }


    }
    public function getUserSocial(Request $request)
    {
        $ms = microtime(true);
        return response()->json($request->all());


        $me = microtime(true) - $ms;

        return response()->json(['data' => null, 'error' => null, 'xtime' => $me], 400);
    }

    public function checkUser(Request $request, $email){
        
        $ms = microtime(true);
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');

        $bearerToken = $request->bearerToken();
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $tokenField = 'Guest';
        }
        if($userAuth) {

                $user = User::where(['email' => trim($email), 'restaurant_id' => $restaurantId, 'status' => 1])->first();
                if(isset($user->id) and $user->id !=''){
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => ['userFound' => true, 'user' => $user], 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }else{
                    $me = microtime(true) - $ms;
                    return response()->json(['data' => ['userFound' => false, 'user' => ''], 'error' => 'User not found', 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
                }

            }else {
                $me = microtime(true) - $ms;
                return response()->json(['data' => null, 'error' => 'Invalid access token OR it has been expired!', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
            }
        } 
     public function addPromoCodeToUser(Request $request)
    {
        $restaurantId = $request->header('X-restaurant');
        $ms = microtime(true);
	$bearerToken = $request->bearerToken();
       
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
            $tokenField = 'Access';
	    $id = $userAuth->user_id;	
        }else{
	   $id = $request->input('id');
	}  
	
        $userData = User::find($id);
        $error = '';
        if(!$userData) {
            $error = 'Not a valid user.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => [$userData], 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
	
         
        $checkUserSocial = UserSocial::where(['user_id' => $id, 'parent_restaurant_id' => $restaurantId])->first();
        if ($checkUserSocial) {
	    $code = $request->input('promocode');
		if(isset($code) && $code!=""){
			$resultsReferral = DB::select('select * from restaurant_referral_promotions where referral_code = :code', ['code' => $code]);
			if(isset($resultsReferral[0])){
			   $ref_restaurantId = $resultsReferral[0]->restaurant_id;
			   DB::table('users')->where('id',$id)->update(['referral_code'=>$code,'referral_restaurant_id'=>$resultsReferral[0]->restaurant_id]);	
			} 
	        }	
            $userData->show_change_password_option = false;
        }else {
            $userData->show_change_password_option = true;
        }   
	$userData = User::find($id);    
        $me = microtime(true) - $ms;
        return response()->json(['data' =>$userData, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    public function deliveryCoverageNotFound(Request $request)
    {
        $restaurantId = $request->header('X-restaurant');
        $ms = microtime(true);
	$id = $request->input('id');
        $userData = User::find($id);
        $error = '';
        if(!$userData) {
            $error = 'Not a valid user.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => [$userData], 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
	
         
        $checkUserSocial = UserSocial::where(['user_id' => $id, 'parent_restaurant_id' => $restaurantId])->first();
        if ($checkUserSocial) {
	    $code = $request->input('promocode');
		if(isset($code) && $code!=""){
			$resultsReferral = DB::select('select * from restaurant_referral_promotions where referral_code = :code', ['code' => $code]);
			if(isset($resultsReferral[0])){
			   $ref_restaurantId = $resultsReferral[0]->restaurant_id;
			   DB::table('users')->where('id',$id)->update(['referral_code'=>$code,'restaurant_id'=>$resultsReferral[0]->restaurant_id,'referral_restaurant_id'=>$resultsReferral[0]->restaurant_id]);	
			} 
	        }	
            $userData->show_change_password_option = false;
        }else {
            $userData->show_change_password_option = true;
        }   
	$userData = User::find($id);    
        $me = microtime(true) - $ms;
        return response()->json(['data' =>$userData, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
}
