<?php
namespace App\Http\Controllers;

use App\Models\MenuCategories;
use App\Models\MenuSubCategories;
use App\Models\NewMenuItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\CommonFunctions;


class OpaMenuController extends Controller {
    private $notMenuId = 0;
    public function getItem(Request $request,$location_id){
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version')) {            
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $type = $request->input('type');
                    $type = (isset($type) && !empty($type))?$type:'food_item';
                    $locationId = $location_id;
                    $menuCats = MenuCategories::where(['restaurant_id' => $locationId, 'product_type' => $type])
                    ->select('id as category_id',
                            'restaurant_id', 
                            'name as category_name', 
                            'status as item_status', 
                            'priority',
                            'language_id',
                            'product_type',
                            'is_popular',
                            'is_favourite',
                            'priority',
                            'slug',
                            'status as item_status',
                            'image_png as item_image_url',
                            'thumb_image_med_png',
                            'priority as item_rank',
                            'parent',
                            'is_delivery',
                            'is_carryout',
                            'description as category_desc',
                            'sub_description')
                    ->get();
                                                                          
                    if ($menuCats) {

                        foreach($menuCats as &$menuCat){
                            $category=$menuCat->slug;                 

                            $subcats = $this->getSubcategories($menuCat->category_id, $locationId, $category, $request);
                           
                            $parent_cat_items = $this->getDirectParentCatOnly($menuCat->category_id, $locationId, $category, $request);
                            
                            $menuCat->sub_categories=$subcats;
                            $menuCat->category_items=$parent_cat_items;

                        }

                        $me = microtime(true) - $ms;
                        $response['data'] = $menuCats;                        
                        return response()->json($response, $status);
                    }
                }
            }
        }
        $me = microtime(true) - $ms;
        $response['error']="Invalid access token OR it has been expired!";
        return response()->json($response, $status);
    }
    
    public function getSubcategories($cat_id, $locId, $category, $request, $parent = 0) {

        $categoryData = MenuSubCategories::where('menu_category_id', '=', $cat_id)              
            ->where('parent', '=', $parent)              
            ->select(
                    'id as category_id',
                    'name as category_name', 
                    'status as item_status',
                    'description as category_desc',  
                    'parent'
                    );
       
        $categoryData = $categoryData->orderBy('name', 'DESC')->get();
        if (count($categoryData)) {
            foreach ($categoryData as &$cat) {
                $cat->subcats = $this->getSubcategories($cat_id, $locId, $category, $request, $cat->id);
                $menuItems = $this->getItemList($cat_id, $cat->category_id, $locId, $category, $request);
                $cat->menu_items_list = $menuItems;
            }
        }
        
        return $categoryData;
    }
    
    public function getItemList($cat_id, $sub_cat_id = null, $locId, $category, $request) {

//        $localization_id = $request->header('X-localization');
//
//        $language_id = CommonFunctions::getLanguageInfo($localization_id);
//        $language_id = $language_id->id;

        $request_params = [
            'new_menu_items.restaurant_id' => $locId,
            'new_menu_items.menu_category_id' => $cat_id,
            'new_menu_items.menu_sub_category_id' => $sub_cat_id,            
        ];

        $menuItems = array();

        $menuItems = NewMenuItems::leftJoin('menu_sub_categories', 'new_menu_items.menu_sub_category_id', '=', 'menu_sub_categories.id')
            ->where($request_params)
            ->select(
                    'new_menu_items.id as item_id', 
                    'new_menu_items.name as item_name',
                    'new_menu_items.description as item_desc',
                    'product_type',
                    'product_subtype',
                    'food_type',
                    'new_menu_items.menu_category_id as category_id',
                    'menu_sub_category_id',
                    'display_order',
                    'new_menu_items.is_popular',
                    'new_menu_items.is_favourite',
                    'is_customizable',
                    'customizable_data',
                    'delivery',
                    'takeout',
                    'new_menu_items.sub_description',
                    'image',
                    'image_caption',
                    'size_price as prices',
                    'size_weight',
                    'price_flag',
                    'new_menu_items.language_id',
                    'images as image_name',
                    'new_menu_items.status as item_status',
                    'inventory',
                    'priority',                    
                    'menu_sub_categories.name as sub_category_name')
            ->orderBy('new_menu_items.display_order', 'ASC')->orderBy('new_menu_items.images', 'DESC')->get()->toArray();           

        $temp = [];
        $defaultImage = null;
        if (strtolower($category) == 'pizza') {
            $defaultImage = '/images/menu/dummy_pizza_image.png';
        }
        if ($request->has('time')) {
            $current_time = $request->query('time') . ':00';
        } else {
            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $locId));
            if (is_object($currentDateTimeObj)) {
                $current_time = $currentDateTimeObj->format('H:i:00');
            } else {
                $current_time = date('H:i:00');
            }
        }
        $deleteFlagNIndex = 0;
        for ($i = 0; $i < count($menuItems); $i++) {
            
            if ($i % 2 != 0 && is_null($menuItems[$i]['image']) && $deleteFlagNIndex == 0 && strtolower($category) != 'pizza') {
                $menuItems[$i]['to_delete'] = $i;
                $deleteFlagNIndex = $i - 1;
            }
           
            $size_prices = json_decode($menuItems[$i]['prices'], true);
           
            $this->getSizePrice($size_prices, $locId, $current_time);

            $menuItems[$i]['prices'] = $size_prices;
            $menuItems[$i]['customizable_data'] = json_decode($menuItems[$i]['customizable_data'], true);
            
            if($menuItems && isset($menuItems[$i]['images'])) {
                $menuItems[$i]['images']  = json_decode($menuItems[$i]['images']);
            }
            $menuItems[$i]['priority'] = $i + 1;
            unset($menuItems[$i]['customizable_data_old']);
            unset($menuItems[$i]['thumb_image_med']);
            unset($menuItems[$i]['banner_image']);
            unset($menuItems[$i]['thumb_banner_image_med']);



            // set language description to item fields
//            $newMenuItemsLanguage = NewMenuItemsLanguage::where([
//                'new_menu_items_id' => $menuItems[$i]['item_id'],
//                'language_id' => $language_id,
//            ])->first();
//
//            if($newMenuItemsLanguage !== null) {
//                $menuItems[$i]['item_other_info'] = $newMenuItemsLanguage->item_other_info;
//                $menuItems[$i]['gift_message'] = $newMenuItemsLanguage->gift_message;
//                $menuItems[$i]['name'] = $newMenuItemsLanguage->name;
//                $menuItems[$i]['description'] = $newMenuItemsLanguage->description;
//                $menuItems[$i]['sub_description'] = $newMenuItemsLanguage->sub_description;
//            }
        }
        
        return $menuItems;
    }
    
    public function getSizePrice(&$size_price_list, $location_id, $current_time, $extra = array()) { 
        
        $current_menu_meal = DB::table('menu_meal_types')
                ->whereTime('to_time', ' >= ', $current_time)
                ->whereTime('from_time', '<= ', $current_time)
                ->where('restaurant_id', $location_id)                
                ->first();
        if ($size_price_list) {
            foreach ($size_price_list as $key => $size) {
                $has_price = false;
                if ((count($size['price']) > 1) || !isset($size['price']['0'])) {

                    if ($current_menu_meal) {
                        if (isset($size['price'][$current_menu_meal->id])) {

                            $size_price_list[$key]['price'] = $size['price'][$current_menu_meal->id];
                            if (isset($size['available'])) {
                                $size_price_list[$key]['is_delivery'] = $size['available'][$current_menu_meal->id]['is_delivery'];
                                $size_price_list[$key]['is_carryout'] = $size['available'][$current_menu_meal->id]['is_carryout'];
                            } else {                                
                                $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                            }
                            $has_price = true;
                        }
                    } else {
                        
                        unset($size_price_list[$key]);
                    }
                } elseif (count($size['price']) == 1) {                    
                    $size_price_list[$key]['price'] = $size['price']['0'];
                    if (isset($size['available'])) {
                        $size_price_list[$key]['is_delivery'] = $size['available']['is_delivery'];
                        $size_price_list[$key]['is_carryout'] = $size['available']['is_carryout'];
                    } else {
                       $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                    }
                    $has_price = true;
                }
                
                if (!$has_price) {
                    unset($size_price_list[$key]);
                }
                unset($size_price_list[$key]['available']);
            }
        }
        
        if(isset($extra['slot_code']) && $extra['slot_code']) {
            $check_availablity = $this->check_menu_availablity($extra);
           
            foreach ($size_price_list as &$list) {
                if($list['is_delivery'] == 1) {                    
                    $list['is_delivery'] = $check_availablity['is_delivery'];
                }
                if($list['is_carryout'] == 1) {                    
                    $list['is_carryout'] = $check_availablity['is_carryout'];
                }
            }
        }            
    }
    public function getDirectParentCatOnly($cat_id, $locId, $category, $request) {
        return $this->getItemList($cat_id, null, $locId, $category, $request);
    }
    
    
   public function updateItem(Request $request,$id) { 
        $requestData = json_decode($request->getContent(), true);  
//{"category_id":[{"5":0}],"all":1,"token":"1057315080f7bc8485bf4f280f927637bed86fb7ce","current_version":0}
//$data = array("category_id"=>array("7837483"=>0,"3784738"=>1),"menu_id"=>array("73874"=>0,"26323"=>1));
//print_r(json_encode($data));
//die;
//print_R($requestData);
//       $parent = $this->getParent($requestData);
//       print_r($parent);
//       die;
        
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => ['success'=>false,'message'=>'']
        );
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version')) {    
           
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
             
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
               
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;
                    $response['message'] = 'Success';
                    $response['data']=['success'=>true,'message'=>'']; 
                   
                    if(isset($requestData['menu_id']) && is_array($requestData['menu_id'])){                       
                        $menuupdate = $this->updateMenu($requestData);                        
                        $this->notMenuId = 1;
                    }
                                        
                    if(isset($requestData['category_id']) && is_array($requestData['category_id'])){
                        $categoryupdate = $this->updateCategory($requestData);
                    }
                    
                    
                    return response()->json($response);
                    
                }else{
                    $response['message']="Not a valid version";
                    return response()->json($response);
                }
            }else{
                $response['message']="Invalid access token OR it has been expired!";
                return response()->json($response);
            }
        }else{            
            return response()->json($response);
        }        
    }
    
    private function updateMenu($data){  
        
        foreach ($data['menu_id'] as $dkey => $details) {            
            foreach ($details as $key => $status)
                NewMenuItems::where(['id'=>$key])->update(['status'=>$status]); 
                $itemId = $key;
                $status = $status;
        } 
        
        if(isset($data['all']) && $data['all']==1){
              $categoryData = NewMenuItems::where('id', '=', $itemId)->select('menu_category_id','menu_sub_category_id')->get(); 
              if ($categoryData) {
                foreach ($categoryData as $cat) {                    
                    if($cat->menu_sub_category_id > 0){
                        MenuCategories::where(['id'=>$cat->menu_sub_category_id])->update(['status'=>$status]);
                    }
                    
                    if($cat->menu_category_id > 0){
                        $has_chield = $this->categoryChild($cat->menu_category_id);
                        if(empty($has_chield)){
                            MenuCategories::where(['id'=>$cat->menu_category_id])->update(['status'=>$status]);
                        }
                         
                    }
                }
              }
        }
        return true;
    }
    
    private function updateCategory($data){
         $category = $data['category_id'];
         foreach($category as $dkey =>$details){
            foreach($details as $key => $status){              
               //update category status
               MenuCategories::where(['id' => $key])->update(['status'=>$status]);
               if(isset($data['all']) && $data['all']==1){
                   NewMenuItems::where(['menu_category_id'=>$key])->update(['status'=>$status]);
                   MenuCategories::where(['parent'=>$key])->update(['status'=>$status]);
               }

            }
         }
        return true;
    }
    
       
    private function getParent($data){
        $subcategory = $data['category_id'];
        $subcats = [];
        $has_parent = [];
        foreach($subcategory as $dkey => $details){  
            foreach($details as $key =>$status){   
               $parent = $this->categoryParent($key);
            }
        }
       return $parent;//Array([16] => Array ([17] => Array()))       
    }
    
     private function categoryParent($id) {
       
        $categoryData = MenuCategories::where('id', '=', $id)->select('id','parent')->get();
        
        $children = [];

        if ($categoryData) {
            foreach ($categoryData as $cat) {
                # Add the child to the list of children, and get its subchildren
                if($cat->parent!=0){
                 $children[$cat->parent] = $this->categoryParent($cat->parent);
                }
            }
        }
       
    return $children;
   }   
    
//    private function getChildren($data){
//        $subcategory = $data['category_id'];
//        $subcats = [];
//        $has_parent = [];
//        foreach($subcategory as $dkey => $details){  
//            foreach($details as $key =>$status){   
//               $children = $this->categoryChild($key);
//            }
//        }
//       return $children;//Array([16] => Array ([17] => Array()))       
//    }
    
    
    
  
    
    
    
    
    private function categoryChild($id) {
       
        $categoryData = MenuCategories::where('parent', '=', $id)->select('id','parent')->get();
        
        $children = [];

        if ($categoryData) {
            foreach ($categoryData as $cat) {
                # Add the child to the list of children, and get its subchildren
                $children[$cat->id] = $this->categoryChild($cat->id);
            }
        }
       
    return $children;
   }   
}


