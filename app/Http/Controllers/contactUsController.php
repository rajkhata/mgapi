<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\contactUs;
use App\Helpers\CommonFunctions;
use App\Models\UserAuth;
use App\Models\User; 
use App\Models\Restaurant;
use Illuminate\Support\Facades\DB;
use App\Models\StaticBlockResponse;
use Illuminate\Http\File;


class contactUsController extends Controller {

    public function contactus(Request $request) {
        $ms = microtime(true);

        $userAuth = false;
        $authHeader = $request->header('Authorization');
        if (!empty($authHeader)) {
            $guestToken = explode(' ', $authHeader)[1];
            $userAuth = UserAuth::where(['guest_token' => $guestToken])->first();
        }

        if (!$userAuth) {
            $me = microtime(true) - $ms;
            return response()->json(['data' => $result, 'error' => "Access token not valid", 'xtime' => $me], 201);
        }


        $langId = config('app.language')['id'];
        $restaurantId = $request->header('X-restaurant');
        $userAgent = config('app.userAgent');


        // input data
        $deviceIpAdd = $request->getClientIp() ?? $request->ip();        

        $validation = Validator::make($request->all(), [            
            'first_name' => 'required|string|max:255',
            'last_name' => 'string|max:255',
            'email' => 'required|string|email|max:255',
        ]);               
        
        if (!$validation->fails()) {

            $user = contactUs::create([
                'restaurant_id' => $restaurantId,
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email'),
                'status' => 1,
                'language_id' => $langId,
                'inquiry_type' => $request->input('inquiry_type'),
                'customer_service' => $request->input('customer_service'),
                'address' => $request->input('address'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),
                'zip' => $request->input('zip'),
                'phone_no' => $request->input('phone_no'),
                'best_buy_date' => $request->input('best_buy_date'),
                'comment' => $request->input('comment'),
                'upc_code' => $request->input('upc_code'),
                'device_ip' => $deviceIpAdd,
                'platform' => $userAgent
            ]);

            $result = [
                'name' => ucwords($user->first_name . ' ' . $user->last_name),
                'email' => $user->email                
            ];
            //send email
            CommonFunctions::$langId = $langId;
            $mailKeywords = array(
                'SITE_URL' => config('constants.site_url'),
                'SITE_NAME' => config('constants.site_name'),
                'FIRST_NAME' => $request->input('first_name'),
                'LAST_NAME' => $request->input('last_name'),
                'MESSAGE' => "Thanks you for contacting us! We will revert back to you as soon as possible.",
                'INQUIRY_TYPE' => $request->input('inquiry_type'),
                'CUSTOMER_SERVICE' => $request->input('customer_service'),
                'ADDRESS' => $request->input('address'),
                'CITY' => $request->input('city'),
                'STATE' => $request->input('state'),
                'ZIP' => $request->input('zip'),
                'PHONE_NO' => $request->input('phone_no'),
                'BEST_BUY_DATE' => $request->input('best_buy_date'),
                'COMMENT' => $request->input('comment'),
            );

            $mailTemplate = CommonFunctions::mailTemplate('contactus', $mailKeywords);

            $mailData['subject'] = trans('Contact us');
            $mailData['body'] = $mailTemplate;
            $mailData['receiver_email'] = $request->input('email');
            $mailData['receiver_name'] = $request->input('first_name');
            $mailData['cc_email'] = "msaxena@munchado.in";
            $mailData['cc_name'] = "sudhanshu";
            CommonFunctions::sendMail($mailData);

            $me = microtime(true) - $ms;

            return response()->json(['data' => $result, 'error' => null, 'xtime' => $me], 201);
        } else {
            $me = microtime(true) - $ms;
            return response()->json(['data' => [], 'error' => $validation->errors(), 'xtime' => $me], 400);
        }
    }

    /**
    * @param Static Block ID and response based on form Field
    * @author Rahul Gupta
    * @date 02-08-2018
    */
    public function v1_sendQuery(Request $request) { 
        $error = ('Unauthorized Request. Please try again!');
        $status = Config('constants.status_code.BAD_REQUEST');

        $userAuth = config('app.userData');
        $ms = microtime(true);
        $location_id = 0;
        $parentRestaurantId = $request->header('X-restaurant');
        $restaurantId = $request->header('X-restaurant');
	$restID =$request->header('X-location');
        \Log::info('all request log');
        \Log::info($request->all());
        \Log::info($request->headers->all());
        if($request->hasHeader('X-location')) {
            // If it has from outlet then send email to locations based manager
            $restaurantId = $location_id = $request->header('X-location');
        }
        $parentrestaurantInfo = Restaurant::where('id', $parentRestaurantId)->select('id', 'email', 'restaurant_name','custom_from','support_from','source_url', 
							'enquiry_prefix', 'mobile','email_data')->first();
        //$restaurantInfo = Restaurant::where('id', $restID)->select('id', 'email', 'restaurant_name','custom_from','support_from','source_url', 'enquiry_prefix','email_data')->first();
	$restaurantInfo = Restaurant::where('id', $parentRestaurantId)->select('id', 'email', 'restaurant_name','custom_from','support_from','source_url', 'enquiry_prefix','email_data')->first();
        $enquiry_type = $request->input('enquiry_type');

        $branchEmailData = $brandEmailData = [];
        $emailData = [];
        if($restaurantInfo && isset($restaurantInfo->email_data)) {
            $branchEmailData = json_decode($restaurantInfo->email_data, true);
            if(isset($branchEmailData[$enquiry_type])) {
                $emailData = $branchEmailData[$enquiry_type];
            }
        }
        
        if($restaurantInfo) {
            \Log::info('restaurant exist');  
            $enquiry_type = $request->input('enquiry_type');
            $uploaded_file_path='';
            if( $enquiry_type=='career' && $request->hasFile('resume') && $request->file('resume')->isValid()){
                $me = microtime(true) - $ms;

                $validation = Validator::make($request->all(), [  
                    'resume' => 'required|mimes:doc,pdf,docx,ppt,jpg,jpeg,png,ppt',
                    ]);               

                    if ($validation->fails()) {
                        return response()->json(['error' =>'Invalid file uploaded', 'xtime' => $me], $status);
                        die;
                    }  
                $filname = $request->file('resume')->getClientOriginalName();
                $imagePath = config('constants.base_public_image_path');
                $curr_restaurant_name=strtolower(str_replace(' ','-',$restaurantInfo->restaurant_name));
                $restpath=$imagePath.$curr_restaurant_name;
                $filname = uniqid() . '_' . $filname;

                 $uploaded_file_path='images'.DIRECTORY_SEPARATOR.$curr_restaurant_name.DIRECTORY_SEPARATOR.$filname; 

                $request->file('resume')->move($restpath, $filname);
               // $uploaded_file_path=$restpath. DIRECTORY_SEPARATOR.$filname; 
            }
              
            $json_array=$request->all();
            if(!empty($uploaded_file_path)){
                $json_array['resume']=$uploaded_file_path;
            }
	    if( $enquiry_type=='subscriptions' || $enquiry_type=='deliverynotfound'){
		$nm = explode("@",$json_array['email']);
		$data['first_name'] = $json_array['first_name']=isset($nm[0])?$nm[0]:"--";
		$data['last_name'] = $json_array['last_name']="";
		$data['phone'] = $json_array['phone']="-"; 
	    }	

            $data = array(
                'static_block_id' => 1,
                'parent_restaurant_id' => $request->header('X-restaurant'),
                'restaurant_id' => $location_id,
                'response' => json_encode($json_array),
                'enquiry_type' =>  $enquiry_type,
                'status' => 1,
                'updated_at' => date('y-m-d H:i:s')
            );

            #16-11-2018 by RG  as per Discussion in Meerting of 15-11 with Prakash / Deepak and Rahul P
            if($request->has('email')) {
                $data['email'] = $request->input('email');
            }
            //PE-2325 @RG 30-10-2018 

            $rest_prefix = $restaurantInfo->enquiry_prefix;
            $check_old_sequence = StaticBlockResponse::where(['restaurant_id' => $restaurantId, 
					'enquiry_type' => $enquiry_type])->select('id', 'sequence_no', 'with_prefix')->orderBy('created_at', 'DESC');
            if($rest_prefix) {
                $check_old_sequence =  $check_old_sequence->where('with_prefix', 1)->first();
                $data['with_prefix'] = 1;
                if($check_old_sequence) {
                    $data['sequence_no'] = $check_old_sequence->sequence_no + 1;
                }else {
                    $data['sequence_no'] = 1;
                }
                $data['receipt_id'] = $rest_prefix.'-'.$data['sequence_no'];
                
            }else {
                $check_old_sequence = $check_old_sequence->first();
                $data['with_prefix'] = 0;
                if($check_old_sequence) {
                    $data['sequence_no'] = $check_old_sequence->sequence_no + 1;
                }else {
                    $data['sequence_no'] = 1;
                }
                $data['receipt_id'] = $data['sequence_no'];
            }
            $all_input = $request->all();
            /* PE-2680 @22-11-2018 BY RG */
            $userId = NULL;
            $user = array();
            if($request->has('email') && $request->has('phone')) {
                $email_id = $request->input('email');
                $mobile_no = $request->input('phone');
                $user = User::where('restaurant_id', $request->header('X-restaurant'))
                    ->where(function ($query) use ($email_id,$mobile_no){
                        $query->where('email', $email_id)
                        ->orWhere('mobile', $mobile_no);
                    })->first();
            }else if($request->has('email') && $request->input('email') && !$request->has('phone')) {

                $email_id = $request->input('email');
                $user = User::where('restaurant_id', $request->header('X-restaurant'))
                    ->where(function ($query) use ($email_id){
                        $query->where('email', $email_id);
                    })->first();

            }else if(!$request->has('email')  && $request->has('phone') && $request->input('phone') ) {
                $mobile_no = $request->input('phone');
                $user = User::where('restaurant_id', $request->header('X-restaurant'))
                    ->where(function ($query) use ($mobile_no){
                        $query->where('mobile', $mobile_no);
                    })->first();
            } 

            if($user){
                $userId = $user->id;
                $data['user_id'] =  $userId;
            } 
            
            $insert_data = StaticBlockResponse::create($data);
            if($insert_data && $insert_data->id) {
                // send email to manager

                if($parentrestaurantInfo->email) {
                    //$html_data = '<ul style="padding-left:0!important;list-style-type:none;">';
                    $html_data = '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">';
                    $i = 1;
                    $j = 1;
                    $email = $name = $date = $full_name = '';
                     
                    $messageHtml = '';
                    $first = '';
                    $last = '';
                    $check_count = count($request->all());
                    $counter = 0;
                    foreach ($request->all() as $key => $value) {                        
                        $counter++;
                        if($key == 'enquiry_type') {                                
                            continue;
                        }
                        if($key =='email') {
                            $email = $value;   
                        }
                        if($key =='first_name') {
                            $name = $name. ' '.$value;   
                        }
            			if($key =='last_name') {
                            $name = $name. ' '.$value;   
                        }
            			 if($key =='order_id') {
                                        $order_id =  $value;   
                        }
            			if($key =='phone') {
                            $phone = $value;  ;   
                        }
                        if($key =='contact_name') {
                            $contact_name = $value;  ;   
                        } 
                        if($key =='website') {
                            $website = $value;  ;   
                        } 
                        if($key =='message') {
                            $message = $value;  ;   
                        }   
                        if($key =='address') {
                            $address = $value;  ;   
                        }  
                        $original_key = $key;
                        $key = ucwords(str_replace('_', ' ', $key));                         
                      
                        
                        #Message Keywords
                        if(in_array($original_key, array('comments', 'message', 'additional_info', 'text') ) ) {
                            if($counter != $check_count) {
                                $messageHtml .= '<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                            <tbody>
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                                </tr>
                                            </tbody>
                                        </table>';
                            }

                            $messageHtml .= '<b>'.$key.':</b><br>'. $value .'<br/><br/>';
                            continue;
                        }

                        if($j%2 != 0) {
                            #first Index
                            $first .= '<b>'.$key.'</b><br>'.$value;

                            if($counter != $check_count) {
                                $first .= '<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                        <tbody>
                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                <td height="30px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;hyphens:auto;line-height:30px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>';
                            }
                            
                        }

                        if($j%2 == 0) {
                            #last Index
                            $last .= '<b>'.$key.'</b><br> '.$value;

                            if($counter != $check_count) {
                                $last .=  '<table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                    <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td height="30px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;hyphens:auto;line-height:30px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>';
                            }
                        } 

                        $j++;
                    } 
                    if( $enquiry_type=='career'){
                        $subject = "New Career Inquiry on Your Website";
                    }elseif($enquiry_type == 'support') {
                        $subject = "You Received a New Message on Your Website";
                    }elseif($enquiry_type == 'contact') {
                        $subject = "You Received a New Message on Your Website";
                    }elseif($enquiry_type == 'deliverynotfound' || $enquiry_type == 'subscriptions') {
                        $subject = "You Received a New Message on Your Website";
                    }elseif($enquiry_type == 'reservation') {
                        #$subject = "New Reservation Request Received!";
                        $subject = "New Table Request from Munch Ado!";

                        if(isset($mobile_no) && $mobile_no) {
                            /******** SMS Send **** @20-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $insert_data->id,
                                'sms_module' => 'reservation_enquiry',
                                'action' => 'placed',
                                'sms_to' => 'customer',
                                'mobile_no' => array($mobile_no)
                            );
                            // As discussed with Prakash & Deepak
                            //CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }
                    }elseif($enquiry_type == 'private_event') {
                        $subject = "New Event Request Received!";

                        if(isset($mobile_no) && $mobile_no) {
                            /******** SMS Send **** @20-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $insert_data->id,
                                'sms_module' => 'event_enquiry',
                                'action' => 'placed',
                                'sms_to' => 'customer',
                                'mobile_no' => array($mobile_no)
                            );
                            // As discussed with Prakash & Deepak
                            //CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }

                    }
                    elseif($enquiry_type == 'franchise') {
                        $subject = "You Received a New Message About Franchises on Your Website";
                    }else {
                        //$subject = "You Received a New Catering Order with Munch Ado!";
                        if(isset($mobile_no) && $mobile_no) {
                            /******** SMS Send **** @20-10-2018 by RG *****/
                            $sms_keywords = array(
                                'order_id' => $insert_data->id,
                                'sms_module' => 'catering',
                                'action' => 'placed',
                                'sms_to' => 'customer',
                                'mobile_no' => array($mobile_no)
                            );
                            // As discussed with Prakash & Deepak
                            //CommonFunctions::getAndSendSms($sms_keywords);
                            /************ END SMS *************/
                        }
                    }   
                   $templateName ='send_query';

                    $mailKeywords = array(
                        'SITE_URL' => $parentrestaurantInfo->source_url,
                        'SITE_NAME' => config('constants.site_name'),
                        'REST_NAME' =>  $parentrestaurantInfo->restaurant_name,  
                        'HTML_DATA' => $html_data, 
                        'MESSAGE' => $messageHtml, 
                        'FIRST' => $first,
                        'LAST' => $last,
                    );  
                    if( $enquiry_type=='career'){
                        $mailKeywords['HEAD'] = "New Career Inquiry";
                        $mailKeywords['SUB_HEAD'] = "Someone used the Careers section of your Munch Ado powered website to send you a message.";
                        $mailKeywords['RECEIPT'] = '';
                    }elseif($enquiry_type == 'contact' ) {
                    $templateName ='send_query_contact';     
                        #$mailKeywords['HEAD'] = "You Received a New Message on Your Website";
                        $mailKeywords['HEAD'] = "Make a New Contact";
                        $mailKeywords['SUB_HEAD'] = "Someone used the Contact Us section of your ".$parentrestaurantInfo->restaurant_name." powered website to send you a message.";
                        $mailKeywords['RECEIPT'] = '';
                    }elseif( $enquiry_type == 'support') {
			     //$mailKeywords['PHONE'] =  $phone; $mailKeywords['EMAIL'] =  $email; $mailKeywords['SUBJECT'] =  $order_id;
			    $templateName ='send_query_support';
                        #$mailKeywords['HEAD'] = "You Received a New Message on Your Website";
                        $mailKeywords['HEAD'] = "You have received a new support request";
                        $mailKeywords['SUB_HEAD'] = "Someone used the support section of your ".$parentrestaurantInfo->restaurant_name." powered website to send you a message.";
                        $mailKeywords['RECEIPT'] = '';
                    }elseif($enquiry_type == 'deliverynotfound' || $enquiry_type == 'subscriptions') {
                        #$mailKeywords['HEAD'] = "You Received a New Message on Your Website";
                        $mailKeywords['HEAD'] = "Make a New Contact";
                        $mailKeywords['SUB_HEAD'] = "Someone used the Contact Us section of your ".$parentrestaurantInfo->restaurant_name." powered website to send you a message.";
                        $mailKeywords['RECEIPT'] = '';
                    }elseif($enquiry_type == 'reservation') {
                        $mailKeywords['HEAD'] = "You Received a New Reservation!";
                        #$mailKeywords['SUB_HEAD'] = "There has been a new reservation request through your Munch Ado website. Exciting!";
                        $dated = date('M d, Y', strtotime($all_input['date_of_event']));
                        $times =  date('h:i A', strtotime($all_input['time_of_event']));
                        $mailKeywords['SUB_HEAD'] = "Its looks like {$all_input['first_name']} has requested a table for {$all_input['no_of_people']} on $dated at $times.";
                        $mailKeywords['RECEIPT'] = 'Receipt No: '.$receipt_no;
                    }elseif($enquiry_type == 'private_event') {
                        $mailKeywords['HEAD'] = "You Received a New Event request!";
                        $mailKeywords['SUB_HEAD'] = "There has been a new request for a private event through your ".$parentrestaurantInfo->restaurant_name." website. Exciting!";
                        $mailKeywords['RECEIPT'] = '';
                    }
                    elseif($enquiry_type == 'franchise') {
                        $mailKeywords['HEAD'] = "Make a New Franchise";
                        $mailKeywords['SUB_HEAD'] = "Someone used the Franchise section of your ".$parentrestaurantInfo->restaurant_name." powered website to send you a message.";
                        $mailKeywords['RECEIPT'] = '';

                    }else{
                        $mailKeywords['HEAD'] = "It's Time to Cater";
                        $mailKeywords['SUB_HEAD'] = "There has been a new request for a catering order through your ".$parentrestaurantInfo->restaurant_name." website. Exciting!";
                        $mailKeywords['RECEIPT'] = '';
                    }
		    
                    $header = "header_layout_restaurant";
                    $footer = "footer_layout_restaurant";
                    $mailTemplate = CommonFunctions::mailTemplate($templateName, $mailKeywords, $header, $footer, $parentRestaurantId);
                    $mailData['subject'] = $subject;
                    $mailData['body'] = $mailTemplate;
                    $mailData['receiver_email'] = isset($emailData['to_email']) ? $emailData['to_email'] : $parentrestaurantInfo->email;
                    $mailData['receiver_name'] = isset($emailData['to_name']) ? $emailData['to_name'] : $parentrestaurantInfo->restaurant_name;
                    $mailData['MAIL_FROM_NAME'] = isset($emailData['from_name']) ? $emailData['from_name'] : 'Believers';
                    $mailData['MAIL_FROM'] = isset($emailData['from_email']) ? $emailData['from_email'] : $parentrestaurantInfo['support_from'];

                   
                    if(!empty($uploaded_file_path)){
                        $mailData['attachment']=$request->file('resume')->getPathName();
                        $mailData['attachment_name']=$request->file('resume')->getClientOriginalName();
                     }        
                    if($enquiry_type!='subscriptions' && $enquiry_type!='deliverynotfound') {CommonFunctions::sendMail($mailData);}
		 
                        $temp_name = "";
                         
                            $temp_name = "customer_email";
                            $subject = "Thank You for Contacting ".$parentrestaurantInfo->restaurant_name;
                            $mailKeywords['HEAD'] = "We've Received Your Request";
                            $mailKeywords['REST_NAME'] = $parentrestaurantInfo->restaurant_name;
                            $mailKeywords['CUST_NAME'] = $name;
                            $mailKeywords['SUB_HEAD'] = "";
                         
                        $mailKeywords['RECEIPT'] = '';
                        $mailTemplate = CommonFunctions::mailTemplate($temp_name, $mailKeywords, 'header_layout', 'footer_layout', $parentRestaurantId);
                        $mailData['subject'] = $subject;
                        $mailData['body'] = $mailTemplate;
                        $mailData['receiver_email'] = $email;
                        $mailData['receiver_name'] = $name;
                        $mailData['MAIL_FROM_NAME'] = isset($emailData['from_name']) ? $emailData['from_name'] : $parentrestaurantInfo->restaurant_name;
                        $mailData['MAIL_FROM'] = isset($emailData['from_email']) ? $emailData['from_email'] : $parentrestaurantInfo['custom_from'];
                        CommonFunctions::sendMail($mailData);
                     

                    $me = microtime(true) - $ms;
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    return response()->json(['success_msg' => 'Thank You! We will connect with you soon!!' ,'data' => [], 'xtime' => $me], $status);
                }else {
                    $me = microtime(true) - $ms;
                    return response()->json(['error' => 'Recipient Email is not configure. Please contact support@munchado.biz', 'xtime' => $me], $status);
                }
                
            }else {
                $error = ('Unable to send Query. Please try again!');
                $status = Config('constants.status_code.INTERNAL_ERROR');
            }
        }

        $me = microtime(true) - $ms;
        // Failure response
        return response()->json(['error' => $error, 'xtime' => $me], $status);

    }

    function generateReservationReceipt() {
        $timestamp = date('mdhis');
        $keys = rand(0, 9);
        $randString = 'M' . $timestamp . $keys;
        return $randString;
    }

} 
