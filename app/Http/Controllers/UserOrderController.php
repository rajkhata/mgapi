<?php

namespace App\Http\Controllers;

use App\Models\UserOrder;
use Illuminate\Http\Request;
use App\Models\UserOrderDetail;
use App\Models\UserAuth;
use App\Models\User;
use App\Helpers\CommonFunctions;
use App\Models\MenuItems;
use App\Models\NewMenuItems;
use App\Models\ItemAddons;
use DB;

class UserOrderController extends Controller {

    public function getItemType($id){
       return MenuItems::find($id, ['product_type']);
    }
    /**
     * Function to show all User's address,
     * Registered to a restaurant
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request) {
        $ms = microtime(true);

        $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        $accessToken = $request->bearerToken() ?? '';
        $userInfo = User::select('users.id')->leftJoin('user_auth', 'user_auth.user_id', '=', 'users.id')
                            ->where('users.restaurant_id', $restaurantId)
                            ->where('user_auth.access_token', $accessToken)->first();
        $userOrder = [];
        if ($userInfo) {
            $userId = $userInfo->id;
            $userOrder = UserOrder::where(['user_id' => $userId,  'is_guest'=>0])->orderby('id','DESC')->get();
            if ($userOrder) {
                $itemStr = "";
                foreach($userOrder as $key => $value){
                    #Get Refund of this Order if any
                    $refunded_amount = 0;
                    $refund_reason = array();
                    $refunded_amount_data = DB::table('refund_histories')->where('order_id', $value->id)->select('amount_refunded', 'reason')->get(); 

                    if( $refunded_amount_data) {
                        foreach ($refunded_amount_data as $key1 => $amount_data) {
                            $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                            $refund_reason[$key1] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
                        }
                    }   
 
                    $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$value->id])->get()->toArray();
                    foreach($userOrderDetails as $mkey =>$mvalue){
                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                            $userOrderDetails[$mkey]['menu_json'] = json_decode($mvalue['menu_json']);
                        }else{
                            $userOrderDetails[$mkey]['menu_json'] = [];
                        }
                        
                        if($value->product_type=='gift_card'){
                          
                            $item_otherinfo = !empty($mvalue['item_other_info']) ? json_decode($mvalue['item_other_info']) : '';
    
                            $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                            $userOrderDetails[$mkey]['promotion_amount']=$promotion_amount;
    
                            }



                        if($mvalue['image']!="null" && !empty($mvalue['image'])){
                            $userOrderDetails[$mkey]['image'] = json_decode($mvalue['image'], true);
                        }
                        /*
                        $product_type='food_item'; //default type for product;
                        if($mvalue['menu_id']!="null" && !empty($mvalue['menu_id'])){
                              $menu_type=$this->getItemType($mvalue['menu_id']);
                              if(!empty($menu_type)){
                                $product_type=$menu_type->product_type;                                   
                              }
                        }
                        */
                        $product_type=$value->product_type;//Order type from order table
                        $userOrderDetails[$mkey]['product_type']=$product_type; 
                        $userOrderDetails[$mkey]['size'] = $mvalue['item_size'];


                    }

                    $userOrder[$key]['refunded_amount'] = $refunded_amount;

                    $userOrder[$key]['refund_reason'] = $refund_reason;
                   
                    /*
                    if(!empty($userOrderDetails) && count($userOrderDetails)){
                        //set product type to order based on first item of order
                        $userOrder[$key]['product_type'] = $userOrderDetails[0]['product_type'];
                    }
                    */
                    $userOrder[$key]['items'] = $userOrderDetails;
                    $userOrder[$key]['create_date'] = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $value->restaurant_id, 'datetime' => $value->created_at));

                   /* $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$value->id])->get()->toArray();
                    foreach($userOrderDetails as $mkey =>$mvalue){
                       $itemStr .= $mvalue['item']." ".$mvalue['quantity'].", ";
                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                           $items = json_decode($mvalue['menu_json']);
                           $userOrder[$key]['items_details'] = $items;
                        }else{
                          $userOrder[$key]['items_details'] = ""; 
                        }
                    }
                    $userOrder[$key]['item'] = substr($itemStr, 0, -2);
                    $itemStr= "";   */
                }
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $error = 'User not found.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    
    public function get(Request $request,$id){
        $ms = microtime(true);
        $accessToken = $request->bearerToken() ?? '';
        if($accessToken){
            $userAuth    = UserAuth::where(['access_token' => $accessToken])->first();
        }else{
            $guestHeader = $request->header('Authorization');
            $guestToken = "";
            if (!empty($guestHeader)) {
                $guestToken = explode(' ', $guestHeader)[1];
            }
            $userAuth    = UserAuth::where(['guest_token' => $guestToken])->first();
        }
        
//        $userId = 5;
//        $orderId = 10;
        $userOrder = [];
          if ($userAuth) {
            if(!$id){
               $me = microtime(true) - $ms;
               return response()->json(['data' => [], 'error' => "Order not found", 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            //$userOrder = UserOrder::leftJoin('user_order_details','user_orders.id','=','user_order_details.user_order_id')->where(['user_orders.user_id' => $userId,'user_orders.id'=>$id])->get();
            $userOrder = UserOrder::where(['id' => $id])->first();
            if ($userOrder) {
                $orders = [];

                    $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$userOrder->id])->get()->toArray(); 
                    
                    foreach($userOrderDetails as $mkey =>$mvalue){
                       
                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                           $userOrderDetails[$mkey]['menu_json'] = json_decode($mvalue['menu_json']);
                        }else{
                           $userOrderDetails[$mkey]['menu_json'] = []; 
                        }

                        if($userOrder->product_type=='gift_card'){
                          
                            $item_otherinfo = !empty($mvalue['item_other_info']) ? json_decode($mvalue['item_other_info']) : '';
    
                            $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                            $userOrderDetails[$mkey]['promotion_amount']=$promotion_amount;
    
                            }


                        if($mvalue['image']!="null" && !empty($mvalue['image'])){
                            $userOrderDetails[$mkey]['image'] = json_decode($mvalue['image'], true);
                        }
                        $userOrderDetails[$mkey]['product_type'] = $userOrder['product_type'];
                        $userOrderDetails[$mkey]['size'] = $mvalue['item_size'];

                    }
                    $userOrder['items'] = $userOrderDetails; 
                    $timestamp = strtotime($userOrder['delivery_date']. ' '.$userOrder['delivery_time'].':00');                   
                    $userOrder['formatted_date'] = date('l d M, h:i A', $timestamp);
                    #txn ID instead of stripe charge ID Jira Issue no. PE-306 
                    $userOrder['stripe_charge_id'] = $userOrder['payment_receipt'];                   
                
                
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
            }
        } else {
            $me = microtime(true) - $ms;
            $error = 'Access token not valid';
            return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }


    public function getOrderList(Request $request) {
        $ms = microtime(true);

        $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        $accessToken = $request->bearerToken() ?? '';
        $userInfo = User::select('users.id')->leftJoin('user_auth', 'user_auth.user_id', '=', 'users.id')
                            ->where('users.restaurant_id', $restaurantId)
                            ->where('user_auth.access_token', $accessToken)->first();
        $userOrder = [];
        if ($userInfo) {
            $userId = $userInfo->id;
            $userOrder = UserOrder::where(['user_id' => $userId,  'is_guest'=>0])->orderby('id','DESC')->get();
            if ($userOrder) {
                $itemStr = "";
                foreach($userOrder as $key => $value){
                    /*Added by Jawed for bug PE-4182*/
                    #Get Refund of this Order if any
                    $refunded_amount = 0;
                    $refund_reason = array();
                    $refunded_amount_data = DB::table('refund_histories')->where('order_id', $value->id)->select('amount_refunded', 'reason')->get();

                    if( $refunded_amount_data) {
                        foreach ($refunded_amount_data as $key1 => $amount_data) {
                            $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                            $refund_reason[$key1] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
                        }
                    }
                    /* bug PE-4182 changes ends here*/

                    $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$value->id])->with('addonoptions')->get()->toArray();
                    foreach($userOrderDetails as $mkey =>&$mvalue){

                        if(isset($mvalue['addonoptions']) && count($mvalue['addonoptions'])){
                            foreach($mvalue['addonoptions'] as &$addonoption){
                                
                                $addonInfo      = ItemAddons::where('id',$addonoption['option_id'])->with('addongroup')->first();
                                if($addonInfo){
                                    $addonoption['is_pot']=$value->is_pot??0;   
                                    $mvalue['is_pot']  =$value->is_pot??0;                             
                                    $addonoption['addongroup']=$addonInfo->addongroup;
    
                                } 
    
                            }
                            
                        }


                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                            $userOrderDetails[$mkey]['menu_json'] = json_decode($mvalue['menu_json']);
                            $mvalue['is_modifier']  =1;                             

                        }else{
                            $userOrderDetails[$mkey]['menu_json'] = [];
                            $mvalue['is_modifier']  =0;   
                        }
                        $menuItem_detail = NewMenuItems::where('id', $mvalue['menu_id'])->first(['id','prompt']);
                        if(isset($menuItem_detail) && isset($menuItem_detail->prompt)){
                            $mvalue['prompt']  =$menuItem_detail->prompt; 
                        }else{
                            $mvalue['prompt']  =''; 
                        }
                        
                        if($value->product_type=='gift_card'){
                          
                            $item_otherinfo = !empty($mvalue['item_other_info']) ? json_decode($mvalue['item_other_info']) : '';
    
                            $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                            $userOrderDetails[$mkey]['promotion_amount']=$promotion_amount;
    
                            }



                        if($mvalue['image']!="null" && !empty($mvalue['image'])){
                            $userOrderDetails[$mkey]['image'] = json_decode($mvalue['image'], true);
                        }
                        
                        $product_type=$value->product_type;//Order type from order table
                        $userOrderDetails[$mkey]['product_type']=$product_type; 
                        $userOrderDetails[$mkey]['size'] = $mvalue['item_size'];


                    }
                    /*Added by Jawed for bug PE-4182*/
                    $userOrder[$key]['refunded_amount'] = $refunded_amount;
                    $userOrder[$key]['refund_reason'] = $refund_reason;
                    /* bug PE-4182 changes ends here*/
			
                    $userOrder[$key]['items'] = $userOrderDetails;
		    $userOrder[$key]['status_history'] =  DB::table('order_statuses')->where('order_id', $value->id)->groupBy('status')->select('status', 'created_at','comments')->orderBy('id','asc')->get();
		    $userOrder[$key]['eta'] = (time() + 60*60*24);
                    $userOrder[$key]['create_date'] = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $value->restaurant_id, 'datetime' => $value->created_at));

                 
                }
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $error = 'User not found.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    
    public function getOrderDetail(Request $request,$id){
        $ms = microtime(true);
        $accessToken = $request->bearerToken() ?? '';
        if($accessToken){
            $userAuth    = UserAuth::where(['access_token' => $accessToken])->first();
        }else{
            $guestHeader = $request->header('Authorization');
            $guestToken = "";
            if (!empty($guestHeader)) {
                $guestToken = explode(' ', $guestHeader)[1];
            }
            $userAuth    = UserAuth::where(['guest_token' => $guestToken])->first();
        }
        
//        $userId = 5;
//        $orderId = 10;
        $userOrder = [];
          if ($userAuth) {
            if(!$id){
               $me = microtime(true) - $ms;
               return response()->json(['data' => [], 'error' => "Order not found", 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
            //$userOrder = UserOrder::leftJoin('user_order_details','user_orders.id','=','user_order_details.user_order_id')->where(['user_orders.user_id' => $userId,'user_orders.id'=>$id])->get();
            $userOrder = UserOrder::where(['id' => $id])->first();
            if ($userOrder) {
                $orders = [];

                    $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$userOrder->id])->with('addonoptions')->get()->toArray(); 
                    
                    foreach($userOrderDetails as $mkey =>&$mvalue){
                        
                        if(isset($mvalue['addonoptions']) && count($mvalue['addonoptions'])){
                            foreach($mvalue['addonoptions'] as &$addonoption){
                                
                                $addonInfo      = ItemAddons::where('id',$addonoption['option_id'])->with('addongroup')->first();
                                if($addonInfo){
                                    $addonoption['is_pot']=$userOrder->is_pot??0;   
                                    $mvalue['is_pot']  =$userOrder->is_pot??0;                             
                                    $addonoption['addongroup']=$addonInfo->addongroup;
    
                                } 
    
                            }
                            
                        }
                        $menuItem_detail = NewMenuItems::where('id', $mvalue['menu_id'])->first(['id','prompt','images']);
                        if(isset($menuItem_detail) && isset($menuItem_detail->prompt)){
                            $mvalue['prompt']  =$menuItem_detail->prompt; 
                        }else{
                            $mvalue['prompt']  =''; 
                        }
                        
                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                           $userOrderDetails[$mkey]['menu_json'] = json_decode($mvalue['menu_json']);
                           $mvalue['is_modifier']  =1; 
                        }else{
                           $userOrderDetails[$mkey]['menu_json'] = []; 
                           $mvalue['is_modifier']  =0; 
                        }

                        if($userOrder->product_type=='gift_card'){
                          
                            $item_otherinfo = !empty($mvalue['item_other_info']) ? json_decode($mvalue['item_other_info']) : '';
    
                            $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                            $userOrderDetails[$mkey]['promotion_amount']=$promotion_amount;
    
                            }


                        if($mvalue['image']!="null" && !empty($mvalue['image'])){
                            $userOrderDetails[$mkey]['image'] = json_decode($mvalue['image'], true);
                        }
			$defaultImage = '/images/Product-Image.png';
			$userOrderDetails[$mkey]['images']  = ($menuItem_detail->images!="")?json_decode($menuItem_detail->images,true):null;
		        $imageAr1=["mobile_app_images"=>["image" => $defaultImage, "thumb_med" => $defaultImage, "thumb_sml" => $defaultImage, "image_caption" =>  ''],
				"desktop_web_images"=>["web_image" => $defaultImage, "web_thumb_med" => $defaultImage, "web_thumb_sml" => $defaultImage, "web_image_caption" => ''],
				"mobile_web_images"=>["mobile_web_image" =>$defaultImage, "mobile_web_thumb_med" => $defaultImage, "mobile_web_thumb_sml" => $defaultImage, "mobile_web_image_caption" =>  ''],
				"banner_image" => '',
				"thumb_banner_image" => '',
		    	];
		    if($userOrderDetails[$mkey]['images']!=null){
			foreach($userOrderDetails[$mkey]['images'] as $key=>$imageAr){
				$userOrderDetails[$mkey]['images'][$key] = isset($imageAr[0])?$imageAr:$imageAr1;  
			}
		    }
                        $userOrderDetails[$mkey]['product_type'] = $userOrder['product_type'];
                        $userOrderDetails[$mkey]['size'] = $mvalue['item_size'];

                    }
                    $userOrder['items'] = $userOrderDetails; 
                    $timestamp = strtotime($userOrder['delivery_date']. ' '.$userOrder['delivery_time'].':00');                   
                    $userOrder['formatted_date'] = date('l d M, h:i A', $timestamp);
                    #txn ID instead of stripe charge ID Jira Issue no. PE-306 
                    $userOrder['stripe_charge_id'] = $userOrder['payment_receipt'];                   
                
                
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
            }
        } else {
            $me = microtime(true) - $ms;
            $error = 'Access token not valid';
            return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }
    
    public function searchOrder(Request $request,$query="") {
        $ms = microtime(true);

        $locationId     = $request->header('X-location');
        $restaurantId   = $request->header('X-restaurant');
        //$accessToken = $request->bearerToken() ?? '';
        ///$userInfo = User::select('users.id')->leftJoin('user_auth', 'user_auth.user_id', '=', 'users.id')->where('users.restaurant_id', $restaurantId)
		//->where('user_auth.access_token', $accessToken)->first();
	  $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }
	$userOrder = [];
        if ($userAuth) {
       
        //if ($userInfo) {
            //$userId = $userInfo->id;
            $userOrder = UserOrder::where(['payment_receipt' => "$query"])->orWhere(['email' => "$query"])->orderby('id','DESC')->get();
            if ($userOrder) {
                $itemStr = "";
                foreach($userOrder as $key => $value){
                    /*Added by Jawed for bug PE-4182*/
                    #Get Refund of this Order if any
                    $refunded_amount = 0;
                    $refund_reason = array();
                    $refunded_amount_data = DB::table('refund_histories')->where('order_id', $value->id)->select('amount_refunded', 'reason')->get();
		    
                    if( $refunded_amount_data) {
                        foreach ($refunded_amount_data as $key1 => $amount_data) {
                            $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                            $refund_reason[$key1] = array('amount' => $amount_data->amount_refunded, 'reason' => $amount_data->reason);
                        }
                    }
                    /* bug PE-4182 changes ends here*/

                    $userOrderDetails= UserOrderDetail::where(['user_order_id'=>$value->id])->with('addonoptions')->get()->toArray();
                    foreach($userOrderDetails as $mkey =>&$mvalue){

                        if(isset($mvalue['addonoptions']) && count($mvalue['addonoptions'])){
                            foreach($mvalue['addonoptions'] as &$addonoption){
                                
                                $addonInfo      = ItemAddons::where('id',$addonoption['option_id'])->with('addongroup')->first();
                                if($addonInfo){
                                    $addonoption['is_pot']=$value->is_pot??0;   
                                    $mvalue['is_pot']  =$value->is_pot??0;                             
                                    $addonoption['addongroup']=$addonInfo->addongroup;
    
                                } 
    
                            }
                            
                        }


                        if($mvalue['menu_json']!="null" && !empty($mvalue['menu_json'])){
                            $userOrderDetails[$mkey]['menu_json'] = json_decode($mvalue['menu_json']);
                            $mvalue['is_modifier']  =1;                             

                        }else{
                            $userOrderDetails[$mkey]['menu_json'] = [];
                            $mvalue['is_modifier']  =0;   
                        }
                        $menuItem_detail = NewMenuItems::where('id', $mvalue['menu_id'])->first(['id','prompt']);
                        if(isset($menuItem_detail) && isset($menuItem_detail->prompt)){
                            $mvalue['prompt']  =$menuItem_detail->prompt; 
                        }else{
                            $mvalue['prompt']  =''; 
                        }
                        
                        if($value->product_type=='gift_card'){
                          
                            $item_otherinfo = !empty($mvalue['item_other_info']) ? json_decode($mvalue['item_other_info']) : '';
    
                            $promotion_amount=(isset($item_otherinfo->promotion_amount) && !empty($item_otherinfo->promotion_amount))?$item_otherinfo->promotion_amount:00.00;
                            $userOrderDetails[$mkey]['promotion_amount']=$promotion_amount;
    
                            }



                        if($mvalue['image']!="null" && !empty($mvalue['image'])){
                            $userOrderDetails[$mkey]['image'] = json_decode($mvalue['image'], true);
                        }
                        
                        $product_type=$value->product_type;//Order type from order table
                        $userOrderDetails[$mkey]['product_type']=$product_type; 
                        $userOrderDetails[$mkey]['size'] = $mvalue['item_size'];
			$userOrderDetails[$mkey]['status_history'] =  DB::table('order_statuses')->where('order_id', $value->id)->select('status', 'created_at','comments')->get();

                    }
                    /*Added by Jawed for bug PE-4182*/
                    $userOrder[$key]['refunded_amount'] = $refunded_amount;
                    $userOrder[$key]['refund_reason'] = $refund_reason;
                    /* bug PE-4182 changes ends here*/
		    $userOrder[$key]['items'] = $userOrderDetails;
		    $userOrder[$key]['status_history'] =  DB::table('order_statuses')->where('order_id', $value->id)->groupBy('status')->select('status', 'created_at','comments')->orderBy('id','asc')->get();
		    $userOrder[$key]['eta'] = (60*60);
                   
                    $userOrder[$key]['create_date'] = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $value->restaurant_id, 'datetime' => $value->created_at));
		    
                 
                }
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => "", 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
            } else {
                $error = 'Order not found.';
                $me = microtime(true) - $ms;
                return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
            }
        } else {
            $error = 'User not found.';
            $me = microtime(true) - $ms;
            return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.BAD_REQUEST'));
        }
        $me = microtime(true) - $ms;
        return response()->json(['data' => $userOrder, 'error' => $error, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
    }

}
