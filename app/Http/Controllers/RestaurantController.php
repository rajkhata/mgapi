<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\UserAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Helpers\CommonFunctions;
use App\Models\State;
use App\Models\SiteBuilderPages;
use App\Models\PaymentGateway;
use Illuminate\Support\Facades\DB;
class RestaurantController extends Controller
{

    public function showAllRestaurants(Request $request)
    {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }
        if($userAuth) {
            $restaurantId = $request->header('X-restaurant');
            $restaurantData = Restaurant::active();
            $me = microtime(true) - $ms;

            return response()->json(['data' => $restaurantData, 'error' => null, 'xtime' => $me], 200);
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], 401);
        }
    }

    public function showAllRestaurantBranches(Request $request)
    {
        $ms = microtime(true);
        $bearerToken = $request->bearerToken();
        if($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
        }
        if($userAuth) {
            $restaurantId = $request->header('X-restaurant');
            $restaurantData = Restaurant::where('parent_restaurant_id', $restaurantId)->active();
            $me = microtime(true) - $ms;

            return response()->json(['data' => $restaurantData, 'error' => null, 'xtime' => $me], Config('constants.status_code.STATUS_SUCCESS'));
        } else {
            $me = microtime(true) - $ms;

            return response()->json(['data' => null, 'error' => 'Access token not valid.', 'xtime' => $me], Config('constants.status_code.UNAUTHORIZED_REQUEST'));
        }
    }

    public function showOneRestaurant($id, Request $request)
    {
        $langId = config('app.language')['id'];     
        $ms = microtime(true);
        //$restaurantData = Restaurant::find($id);
        $restaurantData = Restaurant::where(array('id'=>$id))->get();//,'language_id'=>$langId
        $error = !$restaurantData ? 'Not a valid restaurant.' : '';
        $me = microtime(true) - $ms;
        return response()->json(['data'=> $restaurantData, 'error' => $error, 'xtime' => $me]);
    }

    /**
     * New API for restaurant listing with new fields
     * ignoring the location
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showOneRestaurantNew(Request $request)
    {
        $ms = microtime(true);

        $restaurantId = $request->header('X-restaurant');
        $locationId   = $request->header('X-location');
        $selectFields = ['id','parent_restaurant_id','restaurant_name','google_verification_code','gtm_code','google_map_id',
            'google_client_id','facebook_client_id','netcore_api_key','title','meta_keyword','description',
            'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name','facebook_url','twitter_url','gmail_url',
            'pinterest_url','instagram_url','yelp_url','tripadvisor_url', 'foursquare_url','twitch_url','youtube_url',
            'header_script', 'footer_script','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message','common_message','common_message_status','common_message_url','source_url'];

        $restaurantData = Restaurant::select($selectFields)->where(array('id'=>$restaurantId))->first();
        $paymentData = PaymentGateway::select(['id','name','payment_title','payment_method'])->where(array('restaurant_id'=>$locationId))->get()->toArray();
        if(isset($paymentData[0]))$paymentData=$paymentData;
        else $paymentData=[];
        $data = [];
        if(!$restaurantData) {
            $me = microtime(true) - $ms;

            return response()->json(['data'=> [], 'error' => 'Not a valid restaurant.', 'xtime' => $me]);
        } else {
            if(isset($restaurantData['parent_restaurant_id']) && $restaurantData['parent_restaurant_id']>0) {
                $me = microtime(true) - $ms;

                return response()->json(['data'=> [], 'error' => 'Not a parent restaurant.', 'xtime' => $me]);
            }

            $branchSelectFields = ['restaurants.id','restaurant_name','lat','lng','address','landmark','street', 'zipcode','rest_code',
                'slug','delivery','takeout','dinning','reservation', 'delivery_area', 'minimum_delivery','delivery_charge',
                'restaurant_image_name', 'contact_address','title','meta_keyword','description','is_default_outlet',
                'facebook_url','twitter_url','gmail_url','pinterest_url','instagram_url','yelp_url','tripadvisor_url',
                'foursquare_url','twitch_url','youtube_url', 'restaurant_image_name', 'restaurant_logo_name', 'restaurant_video_name','common_message',
                'header_script','footer_script','phone','currency_symbol','currency_code','phone', 'meta_tags','delivery_custom_message','takeout_custom_message', 'cities.city_name','states.state', 'countries.country_name','countries.country_short_name','address2','common_message','common_message_status','common_message_url','source_url', 'facebook_client_id','google_client_id','gtm_code','google_verification_code'];


            $allBranchData = Restaurant::select($branchSelectFields)->where(['parent_restaurant_id' => $restaurantData['id'], 'restaurants.status' => 1])->leftJoin('cities', 'restaurants.city_id', '=', 'cities.id')->leftJoin('states', 'cities.state_id', '=', 'states.id')->leftJoin('countries', 'cities.country_id', '=', 'countries.id')->get();
            $branchData = app('App\Http\Controllers\LatlngDistanceCalculatorController')->getRestaurantDetails($allBranchData);
            $routes=[];
            $routes = SiteBuilderPages::select(['slug','type','title','target','external','include_in_navigation','sort_order'])->where(['restaurant_id' => $locationId,'status' => 1])->orderBy('sort_order', 'ASC')->get();
        $paymentDetails = DB::select('select id,payment_title,name,payment_method from payment_gateway where status="1" and restaurant_id = :id', ['id' => $restaurantData['id']]);     
            $data = [
                'base_url' => $_SERVER['SERVER_NAME'],
                'payment_gateway' =>$paymentDetails,
                'restaurant' => [
                    'google_verification_code'  => $restaurantData['google_verification_code'],
                    'gtm_code'                  => $restaurantData['gtm_code'],
                    'google_map_id'             => $restaurantData['google_map_id'],
                    'google_client_id'          => $restaurantData['google_client_id'],
                    'facebook_client_id'        => $restaurantData['facebook_client_id'],
                    'netcore_api_key'           => $restaurantData['netcore_api_key'],
                    'takeout_custom_message'           => $restaurantData['takeout_custom_message'],
                    'delivery_custom_message'           => $restaurantData['delivery_custom_message'],
                    'common_message' => $restaurantData['common_message'],
                    'common_message_status' => $restaurantData['common_message_status'],
                     'common_message_url' => $restaurantData['common_message_url'], 
                     'restaurant_url' => $restaurantData['source_url'],
                     'scripts'                   => [],
                    'title'                     => $restaurantData['title'],
                    'description'               => $restaurantData['description'],
                    'meta_keyword'              => !empty($restaurantData['meta_keyword']) ? json_decode($restaurantData['meta_keyword'], true) : '',
                    'currency_symbol'           => isset($restaurantData['currency_symbol']) ? $restaurantData['currency_symbol'] : config('constants.currency'),
                    'currency_code'             => isset($restaurantData['currency_code']) ? $restaurantData['currency_code'] : config('constants.currency_code'),
                    'meta_tags'                 => !empty($restaurantData['meta_tags']) ? json_decode($restaurantData['meta_tags'], true) : '',

                    'scripts' => [
                        'header' => $restaurantData['header_script'],
                        'footer' => $restaurantData['footer_script']
                    ],
                    'social_links' => [
                        ['label' => 'facebook', 'link' => $restaurantData['facebook_url']],
                        ['label' => 'twitter', 'link' => $restaurantData['twitter_url']],
                        ['label' => 'gmail', 'link' => $restaurantData['gmail_url']],
                        ['label' => 'pinterest', 'link' => $restaurantData['pinterest_url']],
                        ['label' => 'instagram', 'link' => $restaurantData['instagram_url']],
                        ['label' => 'yelp', 'link' => $restaurantData['yelp_url']],
                        ['label' => 'tripadvisor', 'link' => $restaurantData['tripadvisor_url']],
                        ['label' => 'foursquare', 'link' => $restaurantData['foursquare_url']],
                        ['label' => 'twitch', 'link' => $restaurantData['twitch_url']],
                        ['label' => 'youtube', 'link' => $restaurantData['youtube_url']],
                    ],
                    'media'                     => [
                        'restaurant_logo'   => $restaurantData['restaurant_logo_name'],
                        'restaurant_image'  => $restaurantData['restaurant_image_name'],
                        'restaurant_video'  => $restaurantData['restaurant_video_name'],
                    ],
                    'routes'=>$routes
                ],
                'locations'  => $branchData,
                // Static content as discussed with Ankur/Gopal 12-Mar-19
                'config' => [
                    "colors" => [
                        "screen_bg" => "#FFFFFF",
                        "text_color" => "#DCDCDC",
                        "loader_color" => "000000",
                        "button_border_color" => "#000000",
                        "theme" => "#F44542",
                        "theme_contrast" => "#b70101",
                        "button_cta_bg" => "#a10010",
                        "button_cta_title" => "#000000",
                        "button_cta_bg_contrast" => "#00FF00",
                        "button_cta_title_contrast" => "#00FF00",
                        "button_ghost_bg" => "#00FF00",
                        "button_ghost_title" => "#00FF00",
                        "button_link" => "#00FF00",
                        "button_link_contrast" => "#00FF00",
                        "separator_theme" => "#00FF00",
                        "separator_normal" => "#00FF00",
                        "separator_light" => "#00FF00",
                        "separator_dark" => "#00FF00",
                        "navigation_bar_title" => "#00FF00",
                        "navigation_bar_title_contrast" => "#00FF00",
                        "navigation_bar_tint" => "#00FF00",
                        "navigation_bar_tint_contrast" => "#00FF00",
                        "tab_item_title" => "#212121",
                        "tab_item_title_selected" => "#000000",
                        "tab_bar_tint" => "#212121",
                        "tab_bar_background" => "#f1f2f4",
                        "message_success" => "#00FF00",
                        "message_warning" => "#00FF00",
                        "message_error" => "#00FF00"
                    ],
                    "font" => "Arial",
                    "tab_bar_items" => [
                        [
                            "type"  => "home",
                            "title" => "Home",
                        ],
                        [
                            "type"  => "bag",
                            "title" => "Bag",
                        ],
                        [
                            "type"  => "account",
                            "title" => "Account",
                        ],
                        [
                            "type"  => "contact",
                            "title" => "Conatact",
                        ],
                    ],
                    "menu"          => [ "layout" => "grid" ],
                ]
            ];
        }


        $me = microtime(true) - $ms;

        return response()->json(['data'=> $data, 'error' => '', 'xtime' => $me]);
    }

    public function create(Request $request)
    {
        $ms = microtime(true);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users'
        ]);

        $restaurantData = Restaurant::create($request->all);
        $error = !$restaurantData ? 'Not a valid restaurant.' : '';
        $me = microtime(true) - $ms;

        return response()->json(['data'=> $restaurantData, 'error' => $error, 'xtime' => $me], 201);
    }

    public function delete($id)
    {
        $ms = microtime(true);
        Restaurant::findOrFail($id)->delete();
        $me = microtime(true) - $ms;

        return response()->json(['data'=>'Deleted successfully', 'error' => '', 'xtime' => $me], 200);
    }

    public function update($id, Request $request)
    {
        $ms = microtime(true);
        $restaurant = Restaurant::findOrFail($id);
        $restaurant->update($request->all());
        $me = microtime(true) - $ms;

        return response()->json(['data'=>$restaurant, 'error' => '', 'xtime' => $me], 200);
    }

    /**
    * Find the restaurants that are providing the delivery & Carryout
    *
    * @param $parent_rest_id 
    * @return data with all restaurants which are open / close
    */

    public function getDeliveryCarryoutStatus(Request $request, $parent_rest_id) {

        $userAuth = $restaurants = array();        
        $error = ('Unauthorized Request. Please try again!');
        $success_msg = ('Request processed successfully.');
        $ms = microtime(true);
        $status = Config('constants.status_code.BAD_REQUEST');
        $bearerToken = $request->bearerToken();
        if ($bearerToken) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();
        } else {
            if($request->header('Authorization')) {
                $tokenInfo    = explode(' ', $request->header('Authorization'));
                if($tokenInfo && isset($tokenInfo['1'])) {
                    $token = $tokenInfo['1'];
                    $userAuth = UserAuth::where(['guest_token' => $token])->first();
                }
            }          
        }
        if($userAuth) {
            if($parent_rest_id){
                $parent_restaurant = Restaurant::select('id','restaurant_name', 'description', 'delivery as is_currently_open_delivery', 'takeout as is_currently_open_carryout')->where('id', $parent_rest_id)->where('status', 1)->get()->toArray(); 
                if($parent_restaurant) {
                    $restaurants = Restaurant::select('id','restaurant_name', 'description', 'delivery as is_currently_open_delivery', 'takeout as is_currently_open_carryout')->where('parent_restaurant_id', $parent_rest_id)->where('status', 1)->get();
                    if($restaurants) {
                        $restaurants = $restaurants->toArray();
                        foreach ($restaurants as $key => &$rest) {
                            $get_restaurant_setting = CommonFunctions::getRestaurantSetting($rest['id']);  
                           $rest['restaurant_services'] =$get_restaurant_setting['restaurant_services'];
                        }
                    }                
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                }else {
                    $error = ('Invalid Restaurant Id. Please try again!');  
                }
            } 
        }else {
            $status = Config('constants.status_code.UNAUTHORIZED_REQUEST');
        }
        // success response
        $me = microtime(true) - $ms;
        if($status == 200) {
            return response()->json(['success_msg' => $success_msg ,'data' => $restaurants, 'xtime' => $me], $status);
        }
      
        $me = microtime(true) - $ms;
        // Failure response
        return response()->json(['error' => $error, 'xtime' => $me], $status);
    
    }

    /**
    * @param Restaurant ID 
    * @param Params : {"takeout":0} ,  {"delivery":0},  {"reservation":0}
    * @author : Rahul Gupta
    * @Date Created : 18-12-2018
    * @return Detail of Orders
    */

    public function opa_restaurant_setting_update(Request $request) {
        $requestData = json_decode($request->getContent(), true);
        #$requestData = $request->all();
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',            
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        ); 
        $orders = array();
        $total_counts = 0;
        // check Token 
        // check app version 
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token')) {
            // generate token for the user           
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $restaurant_check = Restaurant::select('id','restaurant_name', 'description', 'delivery as is_currently_open_delivery', 'takeout as is_currently_open_carryout','is_reservation_allowed', 'reservation', 'reservation_type','is_food_order_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','delivery','takeout')->where('id', $check_token['restaurant_id'])->where('status', 1)->first();
                if($restaurant_check) {
                    if(isset($requestData['takeout'])) {
                         $restaurant_check->takeout = $requestData['takeout'];
                    }
                    if(isset($requestData['delivery'])) {
                         $restaurant_check->delivery = $requestData['delivery'];
                    }
                    if(isset($requestData['reservation'])) {
                        $restaurant_check->reservation = $requestData['reservation'];
                        if($requestData['reservation']) {
                           $restaurant_check->reservation_type = 'full'; 
                        }
                    }
            if(isset($requestData['rest_kitchen_time'])) {
            $kpt = explode("-",$requestData['rest_kitchen_time']);
            $restaurant_check->kpt_calender = $requestData['rest_kitchen_time'];
                $restaurant_check->kpt = ($kpt[0]  * 24 * 60) + ($kpt[1]   * 60) + ($kpt[2]) ;
            }   
                    if(isset($requestData['reason']) && !empty($requestData['reason'])) {
                        #empty the messgae when every service is ON
                        #count how many services are allowed
                        $services = 0;
                        if($restaurant_check->is_food_order_allowed) {
                            if($restaurant_check->food_ordering_delivery_allowed) {
                               $services += 1; 
                            }
                            if($restaurant_check->food_ordering_takeout_allowed) {
                               $services += 1; 
                            }                            
                        } 
                        if($restaurant_check->is_reservation_allowed && $restaurant_check->reservation_type == 'full') {
                           $services += 1; 
                        }
                       
                        $current_services = $restaurant_check->reservation + $restaurant_check->delivery + $restaurant_check->takeout;
                       
                        if($services == $current_services && $services > 0) {
                            $restaurant_check->pause_service_message = '';
                        }else {
                            $restaurant_check->pause_service_message = $requestData['reason'];
                        }
                    }
                    $restaurant_check->save();
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;   
                    $response['message'] = 'Success'; 
                }else {
                   $response['message'] = 'Sorry, this restaurant is not active!';  
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    
    }

     /**
    * @param Token of restaurant
    * @param 
    * @author : Rahul Gupta
    * @Date Created : 18-13-2018
    * @return Detail of Restaurant
    */

    public function opa_get_restaurant_detail(Request $request) {

        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',            
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        ); 
        $orders = array();
        $total_counts = 0;
        // check Token 
        // check app version 
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token') && $request->has('current_version')) {
            // generate token for the user           
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;
                $check_opa_version = CommonFunctions::check_opa_version($check_token['restaurant_id'], $request->input('current_version'));
                if($check_opa_version['is_valid']) {
                    $restaurant_info = Restaurant::select('restaurant_name', 'rest_code','description', 'delivery as is_currently_open_delivery', 'takeout as is_currently_open_carryout', 'zipcode', 'email', 'phone', 'city_id', 'address', 'street','is_gift_card_allowed', 'is_food_order_allowed')->with(['city'])->where('id', $check_token['restaurant_id'])->first();
                    $state_name = '';
                    if($restaurant_info->city && $restaurant_info->city->state_id) {
                        $states_info = State::select('id', 'state')->where('id', $restaurant_info->city->state_id)->first();
                        if($states_info) {

                            $state_name = $states_info->state;
                        }
                    }
                    $address = $restaurant_info->address;
                    if($restaurant_info->street) {
                        $address = $address.' '.$restaurant_info->street; 
                    }
                    if( $restaurant_info->city->city_name) {
                        $address = $address.' '. $restaurant_info->city->city_name; 
                    }
                    if($state_name) {
                        $address =  $address. ' '. $state_name; 
                    }
                    if( $restaurant_info->zipcode) {
                        $address = $address. ' '. $restaurant_info->zipcode; 
                    }
                    $currentVersion = $request->input('current_version');
                    $hardVersion = Config('constants.opa.DASHBOARD_HARD_VERSION_ANDROID');

                    $softVersion = Config('constants.opa.DASHBOARD_SOFT_VERSION_ANDROID');
                    
                    if($currentVersion < $hardVersion){ 
                        $updateType = "hard";       
                    }elseif($currentVersion < $softVersion){
                        $updateType = "soft";
                    }else{
                        $updateType = "no";
                    }
                    $update_app = array(
                            "upgrade_type"=>$updateType,
                            "counter"=>  Config('constants.opa.COUNTER'),
                            "message"=>  Config('constants.opa.FOURCE_UPDATE_MESSAGE'),
                            "clear_data"=>  Config('constants.opa.CLEAR_DATA'),
                            "apk_link"=>  Config('constants.opa.APK_FILE_PATH')
                        );
                    $get_restaurant_setting = CommonFunctions::getRestaurantSetting($check_token['restaurant_id']);  
                    
                    $data = array(
                        'city_name' => $restaurant_info->city->city_name,
                        'restaurant_name' =>  $restaurant_info->restaurant_name,
                        'phone' =>  $restaurant_info->phone,
                        'zipcode' =>  $restaurant_info->zipcode,
                        'state' =>  $state_name,
                        'id' =>  $restaurant_info->id,
                        'address' =>  $address,
                        'email' =>  $restaurant_info->email,
                        'restService' => $get_restaurant_setting,
                        'fource_update' => $update_app
                    );
                    $status = Config('constants.status_code.STATUS_SUCCESS');
                    $response['result'] = true;   
                    $response['message'] = 'Success'; 
                    $response['data'] = $data; 

                }else {
                   $response['message'] = $check_opa_version['message'];  
                }
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);
    
    }


    /**
     * @param Params : token
     * @author : Rahul Gupta
     * @Date Created : 17-01-2019
     * @return Send timezone of the restaurant
     */

    public function opa_get_restaurant_timezone(Request $request) {
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token')) {
            // generate token for the user
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;

                $restaurant_data = Restaurant::where('id', $check_token['restaurant_id'])->select('id', 'email', 'restaurant_name', 'city_id')->with([
                    'city' => function($query) {
                        $query->select('id','time_zone');
                    }
                ])
                    ->first();

                $timezone = is_array($restaurant_data->city) ? $restaurant_data->city->time_zone : 'America/Chicago';
                $status = Config('constants.status_code.STATUS_SUCCESS');
                $response['result'] = true;
                $response['message'] = 'Success';
                $response['data'] = array(
                    'restaurant_id' => $check_token['restaurant_id'],
                    'timezone' =>  $timezone,
                    'token' => $request->input('token'),
                    'message' => true
                );
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);

    }

    /**
     * @param Params : token
     * @author : Rahul Gupta
     * @Date Created : 25-01-2019
     * @return Send logs
     */

    public function opa_get_pause_resume_services_logs(Request $request) {
        $ms = microtime(true);
        $response = array(
            'result' => false,
            'message' => 'Unauthorized Request. Please try again',
            'base_url' =>  env('APP_URL'),
            'deeplink_url' => env('APP_URL'),
            'is_token_valid' => false,
            'data' => array()
        );
        $orders = array();
        $total_counts = 0;
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
        if($request->has('token')) {
            // generate token for the user
            $requestData = $request->input();
            $check_token = CommonFunctions::check_opa_token($request->input('token'));
            if ($check_token['is_valid'] && $check_token['restaurant_id']) {
                $response['is_token_valid'] = true;

                
                $logs = DB::table('dashboard_services_logs')->where('restaurant_id', $check_token['restaurant_id']);
                if(isset($requestData['to']) && $requestData['from']) {
                    $logs = $logs->whereDate('effective_date', '>=', $request->input('from'))->whereDate('effective_date', '<=', $request->input('to'));
                }
                $log_data = $logs->select('effective_date', 'service_type', 'activity_type', 'reason', DB::raw('(CASE WHEN service_type = 1 THEN "Delivery" ELSE (CASE WHEN service_type = 2 THEN "Takeout" ELSE (CASE WHEN service_type = 3 THEN "Reservation" END) END ) END) AS service_type') )->orderBy('created_at', 'DESC')->get();
                $status = Config('constants.status_code.STATUS_SUCCESS');
                $response['result'] = true;
                $response['message'] = 'Success';
                $response['data'] = $log_data;
            }else {
                $status  = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                $response['message'] = 'Invalid access token OR it has been expired!';
            }
        }
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);

    }


     /**
     * @param Params : Parent Restaurant ID as optional params
     * @author : Rahul Gupta
     * @Date Created : 31-01-2019
     * @return Send Email and sms intimation
     */

    public function send_pause_resume_service_intimation($parent_restaurant_id=null) {
        \Log::info('send_pause_resume_service_intimation cron called');
        $ms = microtime(true);       
        // check Token
        // check app version
        $status  = Config('constants.status_code.BAD_REQUEST');
       
            // For all restaurant
        $restaurants = Restaurant::where('parent_restaurant_id', '<>',0)->where('status', 1)->select('restaurants.id', 'email', 'mobile', 'restaurant_name', 'current_version', 'delivery', 'takeout', 'reservation', 'reservation_type', 'delivery', 'takeout', 'reservation', 'reservation_type', 'is_merchandise_allowed', 'is_catering_allowed', 'is_contact_allowed', 'is_event_allowed', 'is_career_allowed','merchandise_takeout_allowed','merchandise_delivery_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','is_reservation_allowed','is_food_order_allowed','is_gift_card_allowed','parent_restaurant_id', 'support_from')
            ->with([
                    'dashboard_services_logs' => function($query) {
                        $query->select('service_type','restaurant_id', 'id','effective_date', 'activity_type', 'reason')
                        ->distinct('service_type')
                        ->whereIn('service_type',[1,2,3])
                        ->where('activity_type', 'Off')
                        ->orderBy('created_at', 'DESC')
                        ;
                    }
                ])
           
            ->get();
        
        if($restaurants) {
            foreach ($restaurants as $key => $restaurant) { 
                //send email
                $any_service_off = 0;
                $service_off_keys = '';
                $service_str = "";
                $service_str_array = array();
                if($restaurant->dashboard_services_logs) {
                    foreach ($restaurant->dashboard_services_logs as $key => $value) {
                        #1=>Delivery,2=>takeout,3=> Reservation
                        if(!array_key_exists('1', $service_str_array) && $value->service_type == 1) {
                            $service_str_array['1'] = date('M d, Y h:i A', strtotime($value->effective_date));
                        }
                        if(!array_key_exists('2', $service_str_array) && $value->service_type == 2) {
                            $service_str_array['2'] = date('M d, Y h:i A', strtotime($value->effective_date));
                        }
                        if(!array_key_exists('3', $service_str_array) && $value->service_type == 3) {
                            $service_str_array['3'] = date('M d, Y h:i A', strtotime($value->effective_date));
                        }
                    }
                }
                if($restaurant->is_food_order_allowed) {
                    if($restaurant->food_ordering_delivery_allowed) {
                        if($restaurant->delivery == 0) {
                            $any_service_off += 1;
                            $service_off_keys = 'delivery orders, ';
                            $service_str = "Delivery orders have been disabled";
                            $service_str.= isset($service_str_array['1']) ?  ' since '.  $service_str_array['1'] : '';
                            $service_str.='.<br/>';
                        }
                    }
                    if($restaurant->food_ordering_takeout_allowed) {
                        if($restaurant->takeout == 0) {
                            $any_service_off += 1;
                            $service_off_keys = $service_off_keys.' takeout orders, ';

                            $service_str .= "Takeout orders have been disabled";
                            $service_str.= isset($service_str_array['2']) ?  ' since '.  $service_str_array['2'] : '';
                            $service_str.='.<br/>';
                        }
                    }                            
                } 
                if($restaurant->is_reservation_allowed && $restaurant->reservation_type == 'full') {
                  
                   if($restaurant->reservation == 0) {
                        $any_service_off += 1;
                        $service_off_keys = $service_off_keys.' reservation';                        
                        $service_str .= "Reservations have been disabled";
                        $service_str.= isset($service_str_array['3']) ?  ' since '.  $service_str_array['3'] : '';
                        $service_str.='.<br/>';
                    }
                }

                $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurant->id));   
                if(is_object($currentDateTimeObj)) {                   
                    $current_datetime= $currentDateTimeObj->format('Y-m-d H:i:s');
                }else { 
                    $current_datetime = date('Y-m-d H:i:s');
                } 
              
                $send_email_via_cron = false;
                if( (isset($service_str_array['1']) && ((strtotime($current_datetime) - strtotime($service_str_array['1'])) / 60 ) >=  $globalSetting->intimation_pause_time) || (isset($service_str_array['2']) && ((strtotime($current_datetime) - strtotime($service_str_array['2'])) / 60 ) >=  $globalSetting->intimation_pause_time) || (isset($service_str_array['3']) && ((strtotime($current_datetime) - strtotime($service_str_array['3'])) / 60 ) >=  $globalSetting->intimation_pause_time))  {
                    $send_email_via_cron = true;
                }
                
                if($any_service_off &&  $send_email_via_cron) { 

                    if($any_service_off == 1) {
                       $service_off_keys = str_replace(',', '', $service_off_keys);
                    }

                    $restaurant_name = htmlentities($restaurant->restaurant_name, ENT_QUOTES, "UTF-8");
                   
                    if($restaurant->email && filter_var($restaurant->email, FILTER_VALIDATE_EMAIL) ) {                        
                        $subject = "REMINDER: Turn $service_off_keys Back on for $restaurant->restaurant_name";

                        $mailKeywords            = array(
                            'SITE_URL'     => '',
                            'LOGIN_URL'    => config('constants.login_url'),
                            'SITE_NAME'    => config('constants.site_name'),
                            'REST_NAME'    => 'dsad',
                            'USER_NAME'    => 'aa',
                            'MAIL_CONTENT' => "Thanks",
                            'REST_NAME' => $restaurant_name,
                            'HEAD' => 'Restore Services to Your Website',
                            'SERVICE'=>  "$service_str",
                            'SERVICE_OPTION' => $service_off_keys,
                            'CTA' =>env('APP_URL').'configure/manage_services'
                        );
                       
                        $mailTemplate = CommonFunctions::mailTemplate('reminder', $mailKeywords, 'header_layout_restaurant', 'footer_layout_restaurant', $restaurant->parent_restaurant_id);                       
                       

                        $mailData['subject']        = $subject;
                        $mailData['body']           = $mailTemplate;
                        $mailData['receiver_email'] = $restaurant->email;
                        $mailData['receiver_name'] =  $restaurant->restaurant_name;
                        $mailData['MAIL_FROM_NAME'] = 'Munch Ado';
                        $mailData['MAIL_FROM'] =  $restaurant->support_from;

                        CommonFunctions::sendMail($mailData);
                        
                    }
                    if($restaurant->mobile) {
                        /******** SMS Send **** @31-01-2019 by RG *****/
                        $sms_keywords = array(  
                            'services' => $service_off_keys,
                            'restaurant_id' =>  $restaurant->id,                         
                            'sms_module' => 'reminder',
                            'action' => 'send_reminder',
                            'sms_to' => 'manager',
                            'mobile_no' => array($restaurant->mobile)
                        );
                        CommonFunctions::getAndSendSms($sms_keywords);
                        /************ END SMS *************/
                    }
                }
                //send sms
            }
        }
        
        $status  = Config('constants.status_code.STATUS_SUCCESS');
        $me = microtime(true) - $ms;
        $response['xtime'] = $me;
        return response()->json($response, $status);

    }

}
