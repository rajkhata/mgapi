<?php

namespace App\Http\Controllers;

use App\Models\MenuCategories;
use App\Models\MenuSubCategories;
use App\Models\MenuItems;
use Illuminate\Http\Request;

class PosMenuController extends Controller {

    private $customizableData;
    private $mods;
    private $priceDetails;

    public function getPosMenus(Request $request,$id) {
        $ms = microtime(true);
        $error = null;
        
        $status = Config('constants.status_code.STATUS_SUCCESS');

        $locationId = $id;//$request->header('X-location');

        $menuCategories = MenuCategories::where(['restaurant_id' => $locationId, 'status' => 1])->orderBy('priority', 'asc')->get();
        $categoryCount = $menuCategories->count();

        if ($categoryCount > 0) {
            foreach ($menuCategories as $key => $category) {

                //$menuSubCategories = MenuSubCategories::where(['restaurant_id' => $locationId, 'menu_category_id' => $category['id'], 'status' => 1])->get();
                //$subCategoryCount = $menuSubCategories->count();
//                if ($subCategoryCount > 0) {
//
//                    foreach ($menuSubCategories as $skey => $subcategory) {
//                        $menuItems = MenuItems::where(['restaurant_id' => $locationId, 'menu_sub_category_id' => $subcategory['id'], 'status' => 1])->get();
//
//                        $menuCount = $menuItems->count();
//                        if ($menuCount > 0) {
//
//                            foreach ($menuItems as $mkey => $item) {
//                                $priceDetails = json_decode($item['size_price']);
//                                $price = (array) $priceDetails[0];
//                                                                
//                                if (!is_array($price['price'])) {
//                                    $price['price'] = (array) $price['price'];
//                                }
//
//                                $this->customizableData = $item['customizable_data'];
//                                $this->customizableData($item);
//                                $posData [] = array(
//                                    'group_id' => $category['id'],
//                                    'group_name' => $category['name'],
//                                    'id' => $item['id'],
//                                    'name' => $item['name'],
//                                    'notes' => $item['description'],
//                                    'price' => isset($price['price'][$key]) ? $price['price'][$key] : 0,
//                                    'mods' => $this->mods,
//                                    'coupons' => array()
//                                );
//                            }
//                        }
//                    }
//                } else {
                    $menuItems = MenuItems::where(['restaurant_id' => $locationId, 'menu_category_id' => $category['id'], 'status' => 1])->get();
                    $menuCount = $menuItems->count();
                    if ($menuCount > 0) {

                        foreach ($menuItems as $key => $item) {
                            $this->priceDetails = json_decode($item['size_price'], 1);                               
                            $this->customizableData($item);                            
                            $posData[] = array(
                                'group_id' => (string)$category['id'],
                                'group_name' => (string)$category['name'],
                                'id' => (string)$item['id'],
                                'name' => (string)$item['name'],
                                'price'=>(string)$this->getPrice($item),
                                'notes' => (string)$item['description'],                                
                                'mods' => $this->mods,
                                'coupons' => array()
                            );
                            
                    }
                }
            }
        }
        $me = microtime(true) - $ms;
        return response()->json(['items' => $posData, 'error' => $error, 'xtime' => $me], $status);
    }

    private function customizableData($item) {            
        
        if ($item['is_customizable'] == 1) {
            $customizableData = json_decode($item['customizable_data'], 1);
            
            $this->mods = [];
            $leveledMods = [];
            foreach ($customizableData as $keycd => $customData) {
                 foreach ($customData['items'] as $k => $cd) {
                    $leveledMods[] = [
                        'id'=>(string)$cd['id'],
                        'name'=>(string)$cd['label'],
                        'portions'=>[],
                        'group_name'=>(string)$item['name'],
                        'group_id'=>(string)$item['id'],
                        'price'=>(string)$cd['price']
                    ]; 
                }
            }
            $this->mods = $leveledMods;        
            
        }else{
            $this->mods = [];
        }
        
        return true;
    }
    
    private function getPrice($item){
        $price = 0;
        $modsPrice = [];
        $i=0;
        foreach ($this->priceDetails as $pkey => $pval) {
            if (!in_array($pval['size'],array("NA","FULL"))) {
                $modsPrice =[
                    'id'=>(string)$item['id']."-".substr($pval['size'],0,1),
                    'name'=>(string)$pval['size'],
                    'portions'=>[],
                    'group_name'=>(string)$item['name'],
                    'group_id'=>(string)$item['id'],
                    'price'=>(string)$pval['price'][0]
                ]; 
                $i++;
                $this->mods[] = $modsPrice;
            } else {
                $price = $pval['price'][0];
            }        
            
        }
        return $price;
    }

}
