<?php

namespace App\Http\Controllers;

use App\Helpers\CommonFunctions;

use App\Models\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// upcoming delivery first
// newest order first

class TestController extends Controller
{

    public function index(Request $request,$id)
    {
        
       $orderForList = $request->input('list');
        if($orderForList==1){
            $response['data'] = $this->getOrderDetailsForList($id);
            return response()->json($response);
        }
    }
    
    
    public function getOrderDetailsForList($orderid){
        $orderData = UserOrder::select(
                    'restaurants.id','restaurants.restaurant_name','restaurants.delivery_provider_apikey',
                    'restaurants.kpt','user_orders.id','user_orders.user_id',
                    'user_orders.restaurant_id','user_orders.order_type','user_orders.total_amount',
                    'user_orders.is_read','user_orders.address','user_orders.address2','user_orders.apt_suite',
                    'user_orders.zipcode','user_orders.created_at','user_orders.delivery_time',
                    'user_orders.delivery_date', 'user_orders.status', 'delivery_time','delivery_date',
                    'restaurants_comments','user_comments','is_reviewed','review_id','payment_receipt','city',
                    'fname','lname','user_orders.email','user_orders.phone',
                    'user_orders.updated_at'
                );
        $orderData = $orderData->where('user_orders.id', $orderid)->where('user_orders.product_type', 'food_item')->leftJoin('restaurants', 'user_orders.restaurant_id', '=', 'restaurants.id')->get();                        
                
        if($orderData) {
            $order = $orderData->toArray();
           
            $orderType = ($order[0]['order_type'] == 'carryout')? 'Takeout': 'Delivery';
            $stageOfOrder = $this->orderStage($order[0]['status'], $orderType);
            
            $deliveryAddress = "";
            if($order['0']['order_type'] == "delivery"){
                $address = [trim($order['0']['address']),trim($order['0']['address2']),trim($order['0']['apt_suite']),trim($order['0']['city']),trim($order['0']['zipcode'])];
                $deliveryAddress  = array_filter($address);
                $deliveryAddress = implode(",", $deliveryAddress);                                    
            }
            
            $customerTotalOrder = CommonFunctions::getCustomerOrders($order[0]);
            
            $currentDateTimeObj = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $order['0']['restaurant_id']));
            if(is_object($currentDateTimeObj)) {
                $restcurrentDateTime['current_date'] =  $currentDateTimeObj->format('Y-m-d');
                $restcurrentDateTime['current_time'] = $currentDateTimeObj->format('H:i:00');
            }else {
                $restcurrentDateTime['current_date'] = date('Y-m-d');
                $restcurrentDateTime['current_time'] = date('H:i:00');
            }
            
            $delivery_time_left_hour = explode('.',(strtotime($order['0']['delivery_date'].' '.$order['0']['delivery_time']) - strtotime($restcurrentDateTime['current_date'].' '.$restcurrentDateTime['current_time'])) / 3600)[0];
            $local_timestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $order['0']['restaurant_id'], 'datetime' => $order['0']['created_at'])));
            
            $refunded_amount = 0;
            $refund_reason = array();
            $refunded_amount_data = DB::table('refund_histories')->where('order_id', $orderid)->select('amount_refunded', 'reason')->get();
            if( $refunded_amount_data) {
                foreach ($refunded_amount_data as $key => $amount_data) {
                    $refunded_amount = $refunded_amount+$amount_data->amount_refunded;
                    $refund_reason[$key] = array('amount' => number_format($amount_data->amount_refunded, 2), 'reason' => $amount_data->reason);
                }
            }
            
            
            $dataList = array(
                    'id' => (string)$order['0']['id'],
                    'fname' =>  $order['0']['fname'],
                    'lname' =>  $order['0']['lname'],
                    'email' =>  $order['0']['email'],
                    'phone' =>  $order['0']['phone'],
                    'delivery_address'=>$deliveryAddress,
                    'is_read'=>$order['0']['is_read'],
                    'delivery_time_left_hour' => (int)$delivery_time_left_hour,
                    'delivery_time_left' => 0,
                    'restaurant_id' => (string)$order['0']['restaurant_id'],                                              
                    'restaurant_name' => $order['0']['restaurant_name'],
                    'is_restaurant_exists' => 'Yes',
                    'order_type1' => 'I',
                    'order_type' => $order['0']['order_type'] == 'carryout' ? 'Takeout': 'Delivery',
                    'delivery_date' => $order['0']['delivery_date'].' '.date('H:i:00', strtotime($order['0']['delivery_time'])),
                    'order_date' => date('Y-m-d H:i:s', $local_timestamp),
                    'menu_status' => "",
                    'is_reviewed' => $order['0']['is_reviewed'] ? $order['0']['is_reviewed'] : 0,
                    'review_id' => $order['0']['review_id'] ? $order['0']['review_id'] : 0,
                    'manual_update' => "",
                    'payment_receipt' =>  $order['0']['payment_receipt'],
                    'delivery_service' => $order['0']['delivery_provider_apikey'] ? 1 : 0,
                    'service_provider' => '',
                    'status' => $order['0']['status'],
                    'refunded_amount' => $refunded_amount ? (string) number_format($refunded_amount, 2) : 'NA',
                    'total_amount' => $order['0']['total_amount'],
                    'user_total_order'=>$customerTotalOrder,
                    'item_list' => array(),
                    'order_stage'=>$stageOfOrder,
                    'time_zone'=>"",
                    'kpt'=>$order['0']['kpt'],
                    'updated_at'=>$order['0']['updated_at']
                 );
            return $dataList;
            }else{
                return array();
            }
    }
    public function orderStage($status,$orderType){      
        if($status=="confirmed"){
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 1],["key"=>"Sent","value"=>0]):array(["key"=>"Confirmed", "value"=> 1],["key"=>"Ready for Pick Up","value"=>0],["key"=>"Picked Up","value"=>0]);            
        }elseif($status=="arrived" || $status=="delivered" || $status=="ready"){
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed", "value"=> 1],["key"=>"Sent", "value"=>1]):array(["key"=>"Confirmed","value" => 1],["key"=>"Ready for Pick Up","value"=>1],["key"=>"Picked Up","value"=>0]);
        }elseif($status=="archived"){
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 1],["key"=>"Sent","value"=>1]):array(["key"=>"Confirmed","value" => 1],["key"=>"Ready for Pick Up","value"=>1],["key"=>"Picked Up","value"=>1]);
        }else{
            return ($orderType  == "Delivery")?array(["key"=>"Confirmed","value" => 0],["key"=>"Sent","value"=>0]):array(["key"=>"Confirmed","value" => 0],["key"=>"Ready for Pick Up","value"=>0],["key"=>"Picked Up","value"=>0]);
        }
    }
    
   
}


