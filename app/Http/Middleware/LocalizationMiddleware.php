<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Language;

class LocalizationMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if (($request->hasHeader('X-localization'))) {
            $locale = Language::where(['code' => trim($request->header('X-localization')), 'status' => 1])->first()->toArray();            
            // set laravel localization            
            app('translator')->setLocale($request->header('X-localization'));
            
            //set language details
            app('config')->set('app.language',$locale);            
                       
            // continue request            
            return $next($request);
        } else {
            app('translator')->setLocale('en');
            
            //set language details
            $locale = ['id' => 1,'language_name' => 'English','code' => 'en','status' => 1];
            app('config')->set('app.language',$locale);            
                       
            // continue request            
            return $next($request);
        }
    }

}
