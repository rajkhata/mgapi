<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Language;

class UserAgentMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if (($request->hasHeader('User-Agent'))) {
             $userAgent = 'web';
            //set user agent details
            if (preg_match('/Android/i', $request->header('User-Agent'))) {
                $userAgent = "android";
            }elseif(preg_match('/iOS/i', $request->header('User-Agent'))) {
                $userAgent = "ios";
            }elseif(preg_match('/Mozilla/i', $request->header('User-Agent')) || preg_match('/Chrome/i', $request->header('User-Agent')) || preg_match('/Safari/i', $request->header('User-Agent'))){
                $userAgent = "web";
            }

            app('config')->set('app.userAgent',$userAgent);
                       
            // continue request            
            return $next($request);
        } else {
            
            app('config')->set('app.userAgent','web');            
                       
            // continue request            
            return $next($request);
        }
    }

}
