<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Language;
use App\Models\UserAuth;

class TokenValidateMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return Invalid Token else passed to the next
     */
    public function handle($request, Closure $next) {
        $ms = microtime(true);
        $error = ('Unauthorized Request. Please try again!');
        $status = Config('constants.status_code.BAD_REQUEST');

        if ($request->hasHeader('Authorization') && $request->hasHeader('X-restaurant')) {
            $token_data = explode(' ', $request->header('Authorization'));
            if(count($token_data) == 2) {
                $token_type = $token_data['0'];
                if(in_array( $token_type, array('Bearer', 'Guest'))) {
                    $parent_restaurant_id = $request->header('X-restaurant');
                    $token = $token_data['1'];
                    if($token_type == 'Bearer') {
                        // match Beaer Token
                        $key = 'access_token';
                        $is_guest = 0;
                    }else {
                        // Match Guest Token
                        $key = 'guest_token';
                        $is_guest = 1;
                    }
                    $userAuth = UserAuth::where([$key => $token, 'parent_restaurant_id' => $parent_restaurant_id])->first();
                    if($userAuth) {
                        # Pass Request and Continue to current Routes
                        $userAuth->is_guest = $is_guest;
                        config(['app.userData' => $userAuth]);
                        return $next($request);
                    }else {
                        $status = Config('constants.status_code.UNAUTHORIZED_REQUEST');
                        $error = ("Invalid $token_type Token OR It has been expired. Please try again with valid Token.");
                    }
                }else {
                    $error = ("Invalid Token Format. Please try again with valid Format.");
                }
               
            }
        }else {
            // Send Authorization Required message
            $error = ('Some Authorization Header\'s are missing.');
        }
        $me = microtime(true) - $ms;
        return response()->json(['error' => $error, 'xtime' => $me], $status);
    }

}
