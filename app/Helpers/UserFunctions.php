<?php
namespace App\Helpers;

use App\Helpers\ApiPayment;
use App\Helpers\CommonFunctions;
use App\Models\User;
use App\Models\UserAuth;
use App\Models\UserSocial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserFunctions
{
    public function userRegister(Request $request, $userData)
    {
        $userData = (array)$userData;
        $ms = microtime(true);
        $langId = config('app.language')['id'];
        // input data
        $deviceIpAdd = $request->getClientIp() ?? $request->ip();
        $platform = strtolower($request->input('platform')) ?? 'web';
        $deviceId = $request->input('device_id') ?? '';
        $deviceToken = $request->input('device_token') ?? '';

        $validation = Validator::make($userData, [
            'restaurant_id' => 'required|exists:restaurants,id',
            'fname'         => 'required|string|max:255',
            'lname'         => 'string|max:255',
            'email'         => 'required|string|email|max:255',
            //'password'      => 'required|alpha_num|min:8',
        ]);

        if (!$validation->fails()) {
            // check guest token
            $userAuth = '';
            $authHeader = $request->header('Authorization');
            if (!empty($authHeader)) {
                $guestToken = explode(' ', $authHeader)[1];
                $userAuth = UserAuth::where(['guest_token' => $guestToken])->first();
            }

            // registration data
            $user = User::create([
                'restaurant_id' => $userData['restaurant_id'],
                'fname' => $userData['fname'],
                'lname' => $userData['lname'],
                'email' => $userData['email'],
                'mobile' => $userData['phone'] ?? null,
                'password' => Hash::make(config('constants.user_default_password')),
                //'password' => Hash::make($request->input('password')),
                'status' => 1,
                'user_group_id' => 1,
                'language_id' => $langId
            ]);

            // STRIPE - create customer
            $data   = [
                'description' => ucwords($user->fname .' '. $user->lname),
                'email'       => $user->email,
                //'source'      => 'tok_visa',
            ];
            $apiObj = new ApiPayment();
            $result = $apiObj->createCustomer($data);
            if($result) {
                $stripeCustomerId = $result->id;
                $user = User::find($user->id);
                $user->stripe_customer_id = $stripeCustomerId;
                $user->save();
            }
            // generate token for the user

            if ($token = $this->guard()->login($user)) {
                $result         = $this->respondWithToken($token);
                $userInfo       = $this->guard()->user();
                $result['user'] = $user;

                unset($result['expires_in']);
				$authArr = [
					'access_token' => $result['access_token'],
					'user_id' => $userInfo->id,
					'ttl' => Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s')
				];
				UserAuth::create($authArr);
                /*$userAuth->access_token = $result['access_token'];
								
                $userAuth->user_id = $userInfo->id;
				
                $userAuth->ttl = Carbon::now()->addSeconds(config('constants.session_expiry'))->format('Y-m-d H:i:s');
                $userAuth->save();*/
            }
            //send email
           /* CommonFunctions::$langId = $langId;
            $mailKeywords = array(
                'SITE_URL' => config('constants.site_url'),
                'LOGIN_URL' => config('constants.login_url'),
                'SITE_NAME' => config('constants.site_name'),
                'USER_NAME' => $userData['fname'],
                'MAIL_CONTENT' => "Thanks"
            );

            $mailTemplate = CommonFunctions::mailTemplate('registration', $mailKeywords);

            $mailData['subject'] = trans('messages.greeting');
            $mailData['body'] = $mailTemplate;
            $mailData['receiver_email'] = $userData['email'];
            $mailData['receiver_name'] = $userData['fname'];

            CommonFunctions::sendMail($mailData);

            CommonFunctions::netcoreEvent(array(
                'email' => $userData['email'],
                'first_name' => $userData['fname'],
                'mobile' => '',
                'last_name' => $userData['lname'],
                'is_register' => 'yes',
                'host_url' => ''
            )); */

            return ['data' => $result, 'error' => null];
        } else {
            return ['data' => null, 'error' => $validation->errors()];
        }
    }
	
	protected function respondWithToken($token) {
        // response()->json()
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * config('constants.session_expiry'),
        ];
    }
	public function guard() {
        return Auth::guard();
    }
}
