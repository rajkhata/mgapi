<?php
namespace App\Helpers;

class MongoClient {
    private $manager;
    private $database;
    private $collection;
    public $writeException;
    public $type;
    public function __construct($host,$database,$collection) {
        $this->manager = new MongoDB\Driver\Manager($host);
        $this->database = $database;
        $this->collection = $collection;
    }
    
    public function insert($data){
        foreach($data as $key =>$value){
            
            $value['type'] = $this->type;
           // db.restaurant_accounts.find({'user_name':'ba59415'}).pretty()

            $filter = array('restaurant_id'=>$value['restaurant_id']);
            $record = $this->findOne($filter);
            
            if(!empty($record)){
                if($record[0]->user_password!=$value['user_password'] || $record[0]->email!=$value['email']){
                    $response = $this->update($value['restaurant_id'], array("user_password"=>$value['user_password'],"email"=>$value['email']));
                    if($response){
                    echo "Updated : ".$value['restaurant_id'];
                    die;
                    }
                }
            }else{
                $insRec  = new MongoDB\Driver\BulkWrite;    
                $insRec->insert($value);
                $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
                try{
                    $this->manager->executeBulkWrite($this->database.".".$this->collection, $insRec, $writeConcern);
                    echo "inserted ".$value['restaurant_id'];
                } catch(MongoDB\Driver\Exception\BulkWriteException $e){
                    $this->writeException = $e->getWriteResult()->getWriteErrors();
                    echo "Insert Fail".$value['restaurant_id']. "->". $this->writeException;
                }
            }
        }
    }

    public function findOne(array $filter = array(),array $options = array())
    {
       $query = new MongoDB\Driver\Query($filter, $options);
       $cursor = $this->manager->executeQuery($this->database.".".$this->collection, $query);
       return $cursor->toArray();
      
    } 
    
    public function update($restaurantId, $data){
        $insRec  = new MongoDB\Driver\BulkWrite; 
        $insRec->update(
         array('restaurant_id' => $restaurantId),
         array('$set' => $data),
         array('multi' => false, 'upsert' => false)
        );
        try{
            $this->manager->executeBulkWrite($this->database.".".$this->collection,$insRec);
            return true;
        } catch(MongoDB\Driver\Exception\BulkWriteException $e){
             echo "Update Fail ".$value['restaurant_id']. "->". $this->writeException;
        }
    }

}