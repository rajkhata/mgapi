<?php

    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Modules\Reservation\Entities\Reservation;
    use Modules\Reservation\Entities\ReservationStatus;
    use Modules\Reservation\Entities\YieldManagement;


    /**
     * Created By: Amit Malakar
     * Date: 23-Aug-18
     * Yield Management
     *
     * Optimize floor and table management
     * and remove dead space, creates usable
     *
     */
    class YieldReservation
    {
        protected $saveToDbFlag;
        protected $saveOptResvFlag;
        protected $sortTableIdsFlag;
        protected $forceNewSlotAvailability;
        protected $preserveIntervalArr;
        protected $slotInterval;

        public function __construct()
        {
            // save yield data to yield_management in DB
            $this->saveToDbFlag = 1;        // *CHANGE*
            // update user_restaurant_resv with optm floor, table in DB based on yield
            $this->saveOptResvFlag          = 0; // *CHANGE*
            $this->sortTableIdsFlag         = 0;
            $this->forceNewSlotAvailability = 1;
            $this->preserveIntervalArr      = [4];
            $this->slotInterval             = config('reservation.slot_interval_time');
        }

        /**
         * Yield Management - Reservation optimization and availability checker
         * @param array $newBookingDetail [start_time, end_time, party_size]
         * @return array
         */
        public function optimize($newBookingDetail = [], $restaurantId)
        {
            $ms = microtime(true);

            // get reservation details for a particular date, restaurant, floor
            //$restaurantId      = Auth::user()->restaurant_id;
            $resvAllowedStatus = [ReservationStatus::getReservedStatusId(), ReservationStatus::getRunningLateStatusId(), ReservationStatus::getConfirmedStatusId()];
            $reservations      = Reservation::where('restaurant_id', '=', $restaurantId)
                ->whereDate('start_time', '=', Carbon::now()->toDateString())//Carbon::now()->toDateString() *CHANGE* //Carbon::parse('2018-07-30')->toDateString()
                //->whereIn('floor_id', $floorId)
                ->select('id', 'restaurant_id', 'floor_id', 'table_id', 'start_time', 'end_time', 'reserved_seat', 'yield_override')
                ->where('source', '!=', config('reservation.source.walk_in'))
                ->whereIn('status_id', $resvAllowedStatus)// reserved-1, running late-4, confirmed-5
                ->with([
                    'table' => function ($query) {
                        $query->select('id', 'min', 'max'); // foreign key field is required for select
                    },
                ])->get();
            if (count($reservations)) {
                $reservations = $reservations->toArray();
                // YIELD FIXED SLOTS
                // booking ids that can't be moved (manager booked reservations)
                $fixedSlotsData = array_filter($reservations, function ($k) {
                    return $k['yield_override'] == 1;
                }, ARRAY_FILTER_USE_BOTH);
                $frozenSlotsArr = [];
                if (count($fixedSlotsData)) {
                    foreach ($fixedSlotsData as $slots) {
                        $frozenSlotsArr[] = [
                            'id'       => $slots['id'],
                            'table_id' => $slots['table_id'],
                        ];
                    }
                }

                $floorIds = implode(",", array_unique(array_column($reservations, 'floor_id')));
                // group them by floor
                $groupData  = $this->groupTimeSlotByTable($reservations);
                $minTime    = $groupData['min_time'];
                $maxTime    = $groupData['max_time'];
                $grpData    = $groupData['rsr_data'];
                $tabFlrData = $groupData['tab_flr_data'];   // floor table

                // get final slots array
                $finalSlotsData = $this->getFinalSlots($minTime, $maxTime, $grpData);
                $finalSlotsArr  = $finalSlotsData['final_slot_arr'];
                $slotArr        = $finalSlotsData['slot_arr'];

                // NEW REQUIRED SLOTS
                $newSlotBooking = [];
                if (count($newBookingDetail)) {
                    $partySize      = $newBookingDetail['party_size'];
                    $reqTableId     = isset($finalSlotsArr[$partySize]) ? key($finalSlotsArr[$partySize]) : $reservations[0]['table_id'];
                    $newSlotBooking = [
                        "id"            => "new",
                        "restaurant_id" => $restaurantId,
                        "floor_id"      => $tabFlrData[$reqTableId],
                        "table_id"      => $reqTableId,
                        "start_time"    => $newBookingDetail['start_time'],
                        "end_time"      => $newBookingDetail['end_time'],
                        "reserved_seat" => $partySize,
                        "table"         => [
                            "id"  => $reqTableId,
                            "min" => 1,
                            "max" => $partySize,
                        ],
                    ];
                    // ($newSlotBooking);
                }

                // create slot based on new slot requirement
                if ($this->forceNewSlotAvailability) {
                    //$reservations[] = $newSlotBooking;
                }

                // generate html of original slots data
                $originalHtml = '';
                if(count($finalSlotsArr)) {
                    $originalHtml = $this->generateHtml($finalSlotsArr, $tabFlrData, $frozenSlotsArr);
                }

                // OPTIMIZE SLOTS
                $optmFinalSlotsArr = [];
                $multipleTableInFloorFlag = false;
                foreach ($finalSlotsArr as $keyMax => $valTable) {
                    $maxSeatTableIds = array_keys($valTable);
                    //dd(array_keys($valTable), $reservations);
                    if (count($maxSeatTableIds) < 1) {
                        // no need for yield
                        $optmFinalSlotsArr[$keyMax] = $valTable;
                    } else {
                        // do yield
                        $maxSeatResv      = array_filter($reservations, function ($k) use ($maxSeatTableIds) {
                            return in_array($k['table_id'], $maxSeatTableIds);
                        }, ARRAY_FILTER_USE_BOTH);
                        $yieldData        = $this->yield($maxSeatResv, $frozenSlotsArr);
                        $yieldMissedSlots = $yieldData['missed_slots'];
                        $yieldArr         = $yieldData['optm_reservations'];
                        if ($yieldMissedSlots != 0) {
                            // missed slots, or unable to optimize, keep the slots as it is
                            $optmFinalSlotsArr[$keyMax] = $valTable;
                        } else {
                            // optimized slots
                            $optmFinalSlotsArr[$keyMax] = $yieldArr;
                            // RESERVATIONS UPDATE DB
                            if ($this->saveOptResvFlag && count($yieldArr)>1) {
                                $this->updateReservationsDb($yieldArr, $tabFlrData);
                                $multipleTableInFloorFlag = true;
                            }
                        }
                    }
                }

                $yieldSlotsData = $this->getFinalSlots($minTime, $maxTime, $optmFinalSlotsArr);
                $yieldSlotsArr  = $yieldSlotsData['final_slot_arr'];

                // generate html of optimized slots data
                $optimizedHtml = '';
                if(count($yieldSlotsArr)) {
                    $optimizedHtml = $this->generateHtml($yieldSlotsArr, $tabFlrData, $frozenSlotsArr);
                }

                // AVAILABLE SLOTS
                $availableSlots     = [];
                $origAvailableSlots = [];
                if (count($newSlotBooking)) {
                    //$origAvailableSlots = $this->checkAvailableSlots($finalSlotsArr, $newSlotBooking['start_time'], $newSlotBooking['end_time'], $this->slotInterval, $tabFlrData);
                    $availableSlots = $this->checkAvailableSlots($yieldSlotsArr, $newSlotBooking['start_time'], $newSlotBooking['end_time'], $this->slotInterval, $tabFlrData);
                }

                // NEAR-BY SLOTS
                /*if (count($availableSlots) == 0) {

                    $allSlots     = array_keys($slotArr);
                    $selStartTime = current($requiredSlots);
                    $selEndTime   = last($requiredSlots);
                    $selIndex     = array_search($selStartTime, $allSlots);
                    // check for ±2 slots intervals
                    $startIndex = $selIndex - 2;
                    $endIndex   = $selIndex + 2;
                    $startTime  = $allSlots[$startIndex];
                    $endTime    = $allSlots[$endIndex];
                    dump([$selStartTime, $selEndTime, $selIndex, $startIndex, $endIndex, $startTime, $endTime]);


                    dump($requiredSlots);
                    dump($allSlots);
                }*/

                // combine all htmls
                $html = '<h4>Original</h4>';
                $html .= $originalHtml;
                $html .= '<br /><br /><h4>Optimized</h4>';
                $html .= $optimizedHtml;
                /*if (count($newBookingReq)) {
                    $sTime = Carbon::parse($newBookingReq['start_time'])->format('H:i');
                    $eTime = Carbon::parse($newBookingReq['end_time'])->format('H:i');
                    $html  .= "<br /><br />Table <b>{$newBookingReq['table_id']}</b> booked between <b>{$sTime}</b> - <b>{$eTime}</b>";
                }*/

                // save to DB, if save flag is on, and
                // more than 1 table in any floor flag
                if ($this->saveToDbFlag && $multipleTableInFloorFlag) {
                    $data   = [
                        'restaurant_id' => $restaurantId,
                        'floor_id'      => $floorIds,
                        'data_before'   => json_encode($finalSlotsArr),
                        'html_before'   => $originalHtml,
                        'data_after'    => json_encode($yieldSlotsArr),
                        'html_after'    => $optimizedHtml,
                        'xtime'         => $me = microtime(true) - $ms,
                    ];
                    $result = $this->saveToDb($data);
                }

                //echo $html; dd([$origAvailableSlots, $availableSlots]); die();

                return [
                    'available_slots' => $availableSlots,
                    'yield_flag'      => 1,
                ];
            } else {
                return [
                    'available_slots' => null,
                    'yield_flag'      => 0,
                ];
            }

        }

        /**
         * Group time slots by table ids
         * @param $reservations
         * @return array
         */
        public function groupTimeSlotByTable($reservations)
        {
            $grpData    = [];
            $tabFlrData = [];
            $minTime    = '';
            $maxTime    = '';
            foreach ($reservations as $reservation) {
                $startTime = Carbon::parse($reservation['start_time'])->format('H:i');
                $endTime   = Carbon::parse($reservation['end_time'])->format('H:i');
                $max       = isset($reservation['table']['max']) ? $reservation['table']['max'] : 1;
                // floor table information
                /*if(!isset($tabFlrData[$reservation['floor_id']])) {
                    $tabFlrData[$reservation['floor_id']][] = $reservation['table_id'];
                } elseif(!in_array($reservation['table_id'], $tabFlrData[$reservation['floor_id']])) {
                    $tabFlrData[$reservation['floor_id']][] = $reservation['table_id'];
                }*/
                $tabFlrData[$reservation['table_id']] = $reservation['floor_id'];
                // MULTIPLE TABLES
                if (!strpos($reservation['table_id'], ',')) {
                    $grpData[$max][$reservation['table_id']]['slots'][] = [
                        'id'       => $reservation['id'],
                        'table_id' => $reservation['table_id'],
                        'beg'      => $startTime,
                        'end'      => $endTime,
                        'seat'     => $reservation['reserved_seat'],
                        'min'      => isset($reservation['table']['min']) ? $reservation['table']['min'] : 0,
                        'max'      => $max,
                    ];
                    $minTime                                            = ($minTime > $startTime || $minTime == '') ? $startTime : $minTime;
                    $maxTime                                            = ($maxTime < $endTime || $maxTime == '') ? $endTime : $maxTime;
                }
            }
            // padding slots
            $minTime = Carbon::parse($minTime)->subHour(1)->minute(0)->second(0);
            $maxTime = Carbon::parse($maxTime)->addHour(1)->minute(0)->second(0);

            return [
                'min_time'     => $minTime,
                'max_time'     => $maxTime,
                'rsr_data'     => $grpData,
                'tab_flr_data' => $tabFlrData,
            ];
        }

        /**
         * Method to get 0/1 slots array, group by  table based in 0/1
         * (0 - free, 1 - reserved)
         * @param $minTime
         * @param $maxTime
         * @param $interval
         * @param $rsrData
         * @return array
         */
        public function getFinalSlots($minTime, $maxTime, $grpData)
        {
            $interval     = $this->slotInterval;
            $finalSlotArr = [];
            $slots        = $minTime->diffInMinutes($maxTime) / $interval;
            if ($slots) {
                $slotsArr = $this->getSlotsInTime($minTime, $maxTime, $interval);
                $slotsArr = array_fill_keys(array_keys(array_flip($slotsArr)), 0);

                // GENERATE SLOTS DATA ARRAY
                foreach ($grpData as $keyMax => $valueTable) {
                    foreach ($valueTable as $key => $value) {
                        $finalSlotArr[$keyMax][$key] = $slotsArr;
                        if(isset($value['slots']) && !empty($value['slots'])){
                            foreach ($value['slots'] as $slot) {
                                $beg              = Carbon::parse($slot['beg']);
                                $end              = Carbon::parse($slot['end']);
                                $bookingSlotCount = $beg->diffInMinutes($end) / $interval;
                                for ($i = 0; $i < $bookingSlotCount; $i++) {
                                    if ($i == 0) {
                                        $tempTime = $beg->format('H:i');
                                    } else {
                                        $tempTime = $beg->addMinutes($interval)->format('H:i');
                                    }
                                    if (isset($finalSlotArr[$keyMax][$key][$tempTime])) {
                                        $finalSlotArr[$keyMax][$key][$tempTime] = $slot['id'];
                                    }
                                }
                            }
                        }
                    }
                    // for generating html sorting the table ids
                    if ($this->sortTableIdsFlag) {
                        ksort($finalSlotArr[$keyMax]);
                    }
                }
            }

            return [
                'final_slot_arr' => $finalSlotArr,
                'slot_arr'       => $slotsArr,
            ];
        }

        /**
         * Get slots between start and end time based on time interval
         * @param $startTime
         * @param $endTime
         * @param $interval
         * @return array
         */
        public function getSlotsInTime($startTime, $endTime, $interval)
        {
            $requiredSlots = [];
            $reqStartTime  = Carbon::parse($startTime);
            $reqEndTime    = Carbon::parse($endTime);
            $reqSlots      = $reqStartTime->diffInMinutes($reqEndTime) / $interval;
            for ($i = 0; $i < $reqSlots; $i++) {
                if ($i == 0)
                    $requiredSlots[$i] = $reqStartTime->format('H:i');
                else
                    $requiredSlots[$i] = $reqStartTime->addMinutes($interval)->format('H:i');
            }

            return $requiredSlots;
        }

        /**
         * Generate slot based HTML table
         * @param $minTime
         * @param $maxTime
         * @param $rsrData
         * @return string
         */
        public function generateHtml($finalSlotArr, $tabFlrData, $frozenSlotsArr)
        {
            $interval = $this->slotInterval;
            // if frozenSlotsArr
            $frozendIdArr = [];
            if (count($frozenSlotsArr)) {
                $frozendIdArr = array_column($frozenSlotsArr, 'id');
            }

            $html = '';
            $html .= '<table style="border: 1px solid black;"><thead>';

            // DRAW BODY
            $body = '';
            $i    = 0;
            foreach ($finalSlotArr as $maxKey => $valueTable) {
                foreach ($valueTable as $key => $value) {
                    $body .= "<tr>";
                    $body .= "<td >{$maxKey}_{$tabFlrData[$key]}_{$key}</td>";
                    if ($i == 0) {
                        $heading = '<th>Tables</th>';
                    }
                    $j = 0;
                    foreach ($value as $k => $v) {
                        if ($v == 0) {
                            $color = 'bgcolor="#FF0000"';
                        } else {
                            if (count($frozendIdArr) && in_array($v, $frozendIdArr)) {
                                $color = 'bgcolor=#33D1FF'; // FROZEN SLOTS
                            } else {
                                $colorCode = '"#FF' . substr(md5($maxKey), 0, 4) . '"'; //$v, bgcolor=
                                $color     = 'bgcolor=' . $colorCode;// ;
                            }
                        }
                        $body .= "<td {$color}>{$v}</td>";
                        if ($i == 0) {
                            $tempMinTime = Carbon::parse($k);
                            if ($j == 0) {
                                $tempTime = $tempMinTime->format('H:i');
                            } else {
                                $tempTime = $tempMinTime->addMinutes($interval)->format('H:i');
                            }
                            $heading .= "<th>{$tempTime} - " . $tempMinTime->copy()->addMinutes($interval)->format('H:i') . "</th>";
                        }
                    }
                    $body .= "</tr>";
                    $i++;
                }
            }

            // DRAW TABLE (HEADING + BODY)
            $html .= '<tr>' . $heading . '</tr>';
            $html .= '</thead><tbody>';
            $html .= $body;
            $html .= '</tbody></table>';

            $css = '<style>'
                   . 'table { border-collapse: collapse; }'
                   . 'table, th, td { border: 1px solid black; text-align: center; }'
                   . 'table { width: 100%; }'
                   . 'th { height: 50px; }'
                   . '</style>';

            return $html . $css;
        }

        /**
         * MAIN METHOD - Optimize time slots
         * @param $reservations
         * @param $frozenSlotsArr
         * @param $interval
         * @param $preserveIntervalArr
         * @return array
         */
        public function yield($reservations, $frozenSlotsArr)
        {
            $interval            = $this->slotInterval;
            $preserveIntervalArr = $this->preserveIntervalArr;
            // SORT RESERVATIONS BY END TIME ASC
            usort($reservations, [$this, 'dateTimeCompare']);

            // get list of tables
            $tableIdArr = array_unique(array_column($reservations, 'table_id'));
            // sort table ids ascending
            sort($tableIdArr);

            // allot reservations to tables
            // check for all the reservations, cluster them by end-start time of reservations
            $tempReservations = $reservations;
            $optmReservations = array_fill_keys(array_keys(array_flip($tableIdArr)), 0);

            // FROZEN SLOTS CHECK
            foreach ($frozenSlotsArr as $frSlot) {
                $resId = $frSlot['id'];
                $tabId = $frSlot['table_id'];
                // check for reservation id index
                $searchResIndex = array_search($resId, array_column($tempReservations, 'id'));
                if($searchResIndex > -1) {
                    // check for table id same in the same index
                    $searchResId   = $tempReservations[$searchResIndex]['id'];
                    $searchTableId = $tempReservations[$searchResIndex]['table_id'];
                    //dump([$searchResIndex, $resId, $searchResId, $tabId, $searchTableId]);
                    if ($resId == $searchResId && $tabId == $searchTableId) {
                        if (!isset($optmReservations[$tabId]['slots'])) {
                            $optmReservations[$tabId] = ['slots' => []];
                        }
                        $data = [
                            'id'       => $tempReservations[$searchResIndex]['id'],
                            'table_id' => $tabId,//$tempReservations[$r]['table_id'],
                            'beg'      => $tempReservations[$searchResIndex]['start_time'],
                            'end'      => $tempReservations[$searchResIndex]['end_time'],
                            'seat'     => $tempReservations[$searchResIndex]['reserved_seat'],
                            //'i_r'      => "{$i} - {$r}",
                        ];
                        //dump([$resId,$tabId,$searchResIndex]);
                        $optmReservations[$tabId]['slots'][] = $data;
                        unset($tempReservations[$searchResIndex]);
                        $tempReservations = array_values($tempReservations);
                    }
                }
            }
            //dd($optmReservations,$tempReservations);
            $tempReservationCount = count($tempReservations);

            // YIELD
            for ($i = 0; $i < count($tableIdArr); $i++) {
                for ($r = 0; $r < $tempReservationCount; $r++) {
                    $tableKeyDistanceArr = [];
                    if (isset($tempReservations[$r])) {
                        $curBegTime = $tempReservations[$r]['start_time'];
                        $curEndTime = $tempReservations[$r]['end_time'];

                        // OVERLAPPING CHECK
                        // loop through existing optmReservations array
                        foreach ($optmReservations as $key => $val) {
                            if (isset($tempReservations[$r])) {
                                // new slot
                                if (!isset($optmReservations[$key]['slots'])) {
                                    $optmReservations[$key]            = ['slots' => []];
                                    $data                              = [
                                        'id'       => $tempReservations[$r]['id'],
                                        'table_id' => $key,//$tempReservations[$r]['table_id'],
                                        'beg'      => $curBegTime,
                                        'end'      => $curEndTime,
                                        'seat'     => $tempReservations[$r]['reserved_seat'],
                                        //'i_r'      => "{$i} - {$r}",
                                    ];
                                    $optmReservations[$key]['slots'][] = $data;
                                    //if($tempReservations[$r]['id']=='new') { $newBookingReq = ['table_id'=> $key, 'start_time'=> $curBegTime, 'end_time'=> $curEndTime]; }
                                    unset($tempReservations[$r]);
                                } elseif (isset($optmReservations[$key]['slots'])) {
                                    // slot exists, check for overlapping/slot distance with current
                                    $j             = 0;
                                    $overlappingId = '';
                                    foreach ($optmReservations[$key]['slots'] as $slot) {
                                        // if overlapping reject
                                        if (($curBegTime >= $slot['beg'] && $curBegTime < $slot['end']) || ($curEndTime > $slot['beg'] && $curEndTime <= $slot['end'])) {
                                            // skip this
                                            //dump(["{$i} - {$r} - {$j}", $curBegTime, $curEndTime, $slot['beg'], $slot['end']]);
                                            $overlappingId = $key;
                                        } else {
                                            // else note distance with current, if 0 attach slot to current table
                                            $oeTime                    = Carbon::parse($slot['end']);
                                            $csTime                    = Carbon::parse($curBegTime);
                                            $tableKeyDistanceArr[$key] = $oeTime->diffInMinutes($csTime) / $interval; //[$oeTime,$csTime,$oeTime->diffInMinutes($csTime)/$interval];
                                        }
                                        // remove overlapping table slot
                                        if (is_numeric($overlappingId) && isset($tableKeyDistanceArr[$overlappingId])) {
                                            unset($tableKeyDistanceArr[$overlappingId]);
                                        }
                                        $j++;
                                    }
                                }
                            }
                        }
                        // SHORTEST DISTANCE/PREFERRED SLOT ALLOTMENT
                        if (count($tableKeyDistanceArr) && isset($tempReservations[$r])) {
                            asort($tableKeyDistanceArr);
                            if (current($tableKeyDistanceArr) == 0) {
                                $pushToTableId = key($tableKeyDistanceArr);
                            } else {
                                $prefferedSlot = array_intersect(array_values($tableKeyDistanceArr), $preserveIntervalArr);
                                if (count($prefferedSlot))
                                    $pushToTableId = array_search(current($prefferedSlot), $tableKeyDistanceArr);
                                else
                                    $pushToTableId = key($tableKeyDistanceArr);
                            }
                            //dump([$tableKeyDistanceArr,$pushToTableId,$tempReservations,$r,$optmReservations]);
                            $optmReservations[$pushToTableId]['slots'][] = [
                                'id'       => $tempReservations[$r]['id'],
                                'table_id' => $pushToTableId,//$tempReservations[$r]['table_id'],
                                'beg'      => $curBegTime,
                                'end'      => $curEndTime,
                                'seat'     => $tempReservations[$r]['reserved_seat'],
                                //'i_r'      => "{$i} - {$r} - {$j}",
                            ];
                            //if($tempReservations[$r]['id']=='new') { $newBookingReq = ['table_id'=> $pushToTableId, 'start_time'=> $curBegTime, 'end_time'=> $curEndTime]; }
                            unset($tempReservations[$r]);
                        }
                    }
                }
            }
            //dump($tempReservations);
            // IFF SLOTS ARE MISSED
            if (count($tempReservations)) {
                $tempReservations = array_values($tempReservations);
                for ($i = 0; $i < count($tempReservations); $i++) {
                    $curBegTime = $tempReservations[$i]['start_time'];
                    $curEndTime = $tempReservations[$i]['end_time'];
                    $minTime    = Carbon::parse($curBegTime);
                    $maxTime    = Carbon::parse($curEndTime);
                    $slots      = $minTime->diffInMinutes($maxTime) / $interval;
                    if ($slots) {
                        $slotsArr = $this->getSlotsInTime($curBegTime, $curEndTime, $interval);
                        $slotsArr = array_fill_keys(array_keys(array_flip($slotsArr)), 0);

                        // check for overlapping slots and same/partial intervals that are empty
                        $overlappingReservationArr = [];
                        foreach ($optmReservations as $key => $tables) {
                            foreach ($tables['slots'] as $slot) {
                                if ((($curBegTime >= $slot['beg'] && $curBegTime < $slot['end'])
                                     || ($curEndTime > $slot['beg'] && $curEndTime <= $slot['end']))
                                    && $slot['id'] != $tempReservations[$i]['id']) {    // && !in_array($slot['id'], array_column($frozenSlotsArr, 'id'))
                                    if (!isset($overlappingReservationArr[$key])) {
                                        $overlappingReservationArr[$key] = $slotsArr;
                                    }

                                    $tableId          = $slot['table_id'];
                                    $beg              = Carbon::parse($slot['beg']);
                                    $end              = Carbon::parse($slot['end']);
                                    $bookingSlotCount = $beg->diffInMinutes($end) / $interval;
                                    for ($k = 0; $k < $bookingSlotCount; $k++) {
                                        if ($k == 0) {
                                            $tempTime = $beg->format('H:i');
                                        } else {
                                            $tempTime = $beg->addMinutes($interval)->format('H:i');
                                        }
                                        if (isset($overlappingReservationArr[$tableId][$tempTime])) {
                                            $overlappingReservationArr[$tableId][$tempTime] = $slot['id'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //dump($overlappingReservationArr);
                    // each overlapping floor should have reservations that overshadows the missing slot else swap with it
                    $swappableTableIdArr = [];
                    if (count($overlappingReservationArr)) {
                        foreach ($overlappingReservationArr as $key => $val) {
                            $zeroFlag   = 0;
                            $swapResvId = '';
                            foreach ($val as $k => $s) {
                                if ($s == 0) {
                                    $zeroFlag = 1;
                                } else {
                                    if (!in_array($s, array_column($frozenSlotsArr, 'id'))) {
                                        $swapResvId = $s;
                                    }
                                }
                            }
                            if ($zeroFlag && !empty($swapResvId)) {
                                $swappableTableIdArr[$key] = $swapResvId;
                            }
                        }
                    }
                    //dump($swappableTableIdArr);
                    // try to swap, if it's movable
                    $movableSlots = $movableTable = '';
                    foreach ($swappableTableIdArr as $key => $value) {
                        $searchIndex = array_search($value, array_column($optmReservations[$key]['slots'], 'id'));
                        if (isset($optmReservations[$key]['slots'][$searchIndex])) {
                            $minTime         = Carbon::parse($optmReservations[$key]['slots'][$searchIndex]['beg']);
                            $maxTime         = Carbon::parse($optmReservations[$key]['slots'][$searchIndex]['end']);
                            $tableStatusData = $this->getFinalSlots($minTime, $maxTime, $optmReservations);
                            $tableStatus     = $tableStatusData['final_slot_arr'] ?? '';
                            foreach ($tableStatus as $tab => $res) {
                                if (!count(array_filter($res))) {
                                    $movableSlots = $res;
                                    $movableTable = $tab;
                                }
                            }
                        }
                        if ($movableTable && count($movableSlots) && isset($tempReservations[$i])) {
                            // search & replace
                            if (isset($optmReservations[$movableTable]['slots'])) {
                                $sIndex = array_search($value, array_column($optmReservations[$key]['slots'], 'id'));
                                if ($sIndex > -1) {
                                    $resvToMove                                 = $optmReservations[$key]['slots'][$sIndex];
                                    $optmReservations[$movableTable]['slots'][] = $resvToMove;
                                    $optmReservations[$key]['slots'][$sIndex]   = [
                                        "id"       => $tempReservations[$i]['id'],
                                        "table_id" => $tempReservations[$i]['table_id'],
                                        "beg"      => $tempReservations[$i]['start_time'],
                                        "end"      => $tempReservations[$i]['end_time'],
                                        "seat"     => $tempReservations[$i]['reserved_seat'],
                                    ];
                                    //if($tempReservations[$i]['id']=='new') { $newBookingReq = ['table_id'=> $tempReservations[$i]['table_id'], 'start_time'=> $tempReservations[$i]['start_time'], 'end_time'=> $tempReservations[$i]['end_time']]; }
                                    unset($tempReservations[$i]);
                                }
                            }
                        }
                    }
                }
            }

            $data = [
                'missed_slots'      => count($tempReservations),
                'optm_reservations' => $optmReservations,
            ];

            return $data;
        }

        /**
         * Update reservation i.e. floor and table ids interchange based on yield
         * @param $optReservations
         * @param $tabFlrData
         * @return mixed
         */
        public function updateReservationsDb($optReservations, $tabFlrData)
        {
            $floorQuery = '';
            $tableQuery = '';
            $resvIds    = '';
            foreach ($optReservations as $slots) {
                foreach ($slots['slots'] as $resv) {
                    $floorQuery .= ' WHEN ' . $resv['id'] . ' THEN ' . $tabFlrData[$resv['table_id']] . ' ';
                    $tableQuery .= ' WHEN ' . $resv['id'] . ' THEN ' . $resv['table_id'] . ' ';
                    $resvIds    .= $resv['id'] . ',';
                }
            }
            $resvIds = rtrim($resvIds, ',');

            $tableName   = with(new Reservation)->getTable();
            $updateQuery = "UPDATE `{$tableName}` SET `floor_id` = CASE `id` {$floorQuery} END, `table_id` = CASE `id` {$tableQuery} END WHERE `id` IN ({$resvIds})";
            $affected    = DB::update($updateQuery);

            \Log::info($affected);

            return $affected;
        }

        /**
         * Check if required slots are available
         * @param $yieldSlotsArr
         * @param $startTime
         * @param $endTime
         * @param $interval
         * @return array
         */
        public function checkAvailableSlots($yieldSlotsArr, $startTime, $endTime, $interval, $tabFlrData)
        {
            $availableSlots = [];
            $requiredSlots  = $this->getSlotsInTime($startTime, $endTime, $interval);
            // check for available slots
            /*foreach ($yieldSlotsArr as $keyMax => $valTable) {
                foreach ($valTable as $table => $slots) {
                    $slots = $this->searchInTimeSlots(0, $slots);
                    if ($slots && count(array_intersect($slots, $requiredSlots)) == count($requiredSlots)) {
                        // check for
                        $availableSlots[$table] = $requiredSlots;
                    }
                }
            }*/
            foreach ($yieldSlotsArr as $keyMax => $valTable) {
                foreach ($valTable as $table => $slots) {
                    $slots = $this->searchInTimeSlots(0, $slots);
                    if ($slots && count(array_intersect($slots, $requiredSlots)) == count($requiredSlots)) {
                        // check for
                        $availableSlots[$tabFlrData[$table]][] = $table;
                    }
                }
            }

            return $availableSlots;
        }

        /**
         * Search resrv id/ term in time slot array
         * return keys (time slots) in array values
         * @param $search
         * @param $slotArr
         * @return array
         */
        public function searchInTimeSlots($search, $slotArr)
        {
            return array_keys($slotArr, $search, true);
        }

        /**
         * Yield save to DB
         * @param $data
         * @return mixed
         */
        public function saveToDb($data)
        {
            $result = YieldManagement::create($data);

            return $result;
        }

        /**
         * Method to sort by date time
         * @param $a
         * @param $b
         * @return false|int
         */
        public function dateTimeCompare($a, $b)
        {
            $time1 = strtotime($a['end_time']);
            $time2 = strtotime($b['end_time']);

            return $time1 - $time2;
        }

        /**
         * Method to pluck yield floors/tables and sort by party_size
         *
         * @param $yieldFloorTable
         * @param $floorTableData
         * @param $party_size
         * @return array
         */
        public function getFloorTableData($yieldFloorTable, $floorTableData, $party_size)
        {
            // *CHANGE*
            /*$yieldFloorTable['available_slots'] = [
                41 => [41, 42, 46],
                45 => [50, 51],
                63 => [96, 97, 98],
            ];*/

            $finalFloorTableData    = [
                'floor_table_info' => [],
                'slot_range'       => $floorTableData['slot_range'],
                'tat'              => $floorTableData['tat'],
            ];
            $selFloorIdArr          = [];
            $yieldOptFloorTableTemp = [];
            foreach ($yieldFloorTable['available_slots'] as $floorId => $tableIds) {
                // look for floorId in floorTableData
                $floorIndex = array_search($floorId, array_column($floorTableData['floor_table_info'], 'id'));
                if ($floorIndex !== false) {
                    $selTableIdArr = [];
                    if(isset($floorTableData['floor_table_info'][$floorIndex]['tables'])) {
                        $floorTableData['floor_table_info'][$floorIndex]['tables'] = array_values($floorTableData['floor_table_info'][$floorIndex]['tables']);
                        foreach ($tableIds as $tabId) {
                            $tabIndex = array_search($tabId, array_column($floorTableData['floor_table_info'][$floorIndex]['tables'], 'id'));
                            if ($tabIndex !== false) {
                                // insert table data
                                $selTableIdArr[] = $floorTableData['floor_table_info'][$floorIndex]['tables'][$tabIndex];
                                if ($party_size <= $floorTableData['floor_table_info'][$floorIndex]['tables'][$tabIndex]['max']) {
                                    $yieldOptFloorTableTemp[] = [
                                        'floor_id' => $floorTableData['floor_table_info'][$floorIndex]['id'],
                                        'table_id' => $floorTableData['floor_table_info'][$floorIndex]['tables'][$tabIndex]['id'],
                                        'max'      => $floorTableData['floor_table_info'][$floorIndex]['tables'][$tabIndex]['max'],
                                    ];
                                }
                            }
                        }

                        // if table data present then add floor information
                        if (count($selTableIdArr)) {
                            $selFloorIdArr[] = [
                                'id'     => $floorTableData['floor_table_info'][$floorIndex]['id'],
                                'name'   => $floorTableData['floor_table_info'][$floorIndex]['name'],
                                'tables' => $selTableIdArr,
                            ];
                        }
                    }
                }
            }

            $yieldOptFloorTable = [];
            if(count($yieldOptFloorTableTemp)) {

                // sort by max
                usort($yieldOptFloorTableTemp, function ($a, $b) {
                    return $a['max'] <=> $b['max'];
                });

                // create array based on max covers
                foreach ($yieldOptFloorTableTemp as $value) {
                    $yieldOptFloorTable[$value['max']][] = $value;
                }

                $yieldOptFloorTable = [
                    'yield' => array_reverse($yieldOptFloorTable[key($yieldOptFloorTable)]),
                    'all'   => $yieldOptFloorTable
                ];
            }

            $finalFloorTableData['floor_table_info']  = $selFloorIdArr;
            $finalFloorTableData['yield_floor_table'] = $yieldOptFloorTable;

            return $finalFloorTableData;
        }

        /**
         * Method to pluck floors/tables and sort by party_size (non-yield)
         * @param $floorTableData
         * @param $party_size
         * @return array
         */
        public function getAllFloorTableData($floorTableData, $party_size)
        {
            $yieldOptFloorTableTemp = [];
            foreach ($floorTableData['floor_table_info'] as $keyFloor => $valFloor) {
                if(isset($valFloor['tables'])) {
                    foreach ($valFloor['tables'] as $keyTable => $valTable) {
                        // insert table data
                        if ($party_size <= $valTable['max']) {
                            $yieldOptFloorTableTemp[] = [
                                'floor_id' => $valFloor['id'],
                                'table_id' => $valTable['id'],
                                'max'      => $valTable['max'],
                            ];
                        }
                    }
                }
            }

            $yieldOptFloorTable = [];
            if(count($yieldOptFloorTableTemp)) {

                // sort by max
                usort($yieldOptFloorTableTemp, function ($a, $b) {
                    return $a['max'] <=> $b['max'];
                });
                // create array based on max covers
                foreach ($yieldOptFloorTableTemp as $value) {
                    $yieldOptFloorTable[$value['max']][] = $value;
                }
                $yieldOptFloorTable = [
                    'yield' => array_reverse($yieldOptFloorTable[key($yieldOptFloorTable)]),
                    'all'   => $yieldOptFloorTable
                ];
            }

            $floorTableData['yield_floor_table'] = $yieldOptFloorTable;

            return $floorTableData;
        }

    }

?>