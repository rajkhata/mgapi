<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * common function has been written for used in overall application where required to use
 * Function list
 * [sendMail,sendSMS, netcore]
 */
namespace App\Helpers;
use App\Models\Reservation;
use App\Models\ReservationStatus;
use App\Models\User;
use http\Client\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Models\MailTemplate;
use App\Models\SmsContent;
use App\Models\Restaurant;
use App\Models\CmsUser;
use App\Models\City;
use App\Models\MenuSettingItem;
use App\Models\MenuItems;
use App\Models\NewMenuItems;
use App\Models\UserMyBag;
use App\Models\ItemModifier;


use PubNub\PNConfiguration;
use PubNub\PubNub;
use App\Models\CmsAuth;
use Carbon\Carbon;
use App\Models\UserOrder;
use App\Models\StaticBlockResponse;
use App\Models\UserAuth;
use App\Models\OrderDetailOptions;
use App\Models\MybagOptions;
use App\Models\ItemAddons;
use App\Models\MenuCategories;
use App\Models\MenuSubCategories;
use DB;
use App\Models\Configuration;
class CommonFunctions {
    /*
     * sendMail is used to any mail accept bellow data
     * $amilData[receiver_email,cc_emial,bcc_emial,attachment,subject,body,AltBody]
     */
    public static $langId = 1;
    private static $_cache = array();
    public static $city_id;
    protected static $_date_time_zone;
    public static $lat1;
    public static $lon1;
    public static $lat2;
    public static $lon2;
    public static $unit;
    public static $timezoneError=[];
    public static function sendMail($mailData) {
        $mail = new PHPMailer(true);
	self::setEnvConfiguration(1);
        try {
            //Server settings
            $mail->CharSet = 'UTF-8';
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output2
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $_ENV['MAIL_HOST'];                     // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $_ENV['MAIL_USERNAME'];             // SMTP username
            $mail->Password = $_ENV['MAIL_PASSWORD'];             // SMTP password
            $mail->SMTPSecure = $_ENV['MAIL_ENCRYPTION'];         // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $_ENV['MAIL_PORT'];                     // TCP port to connect to
            //Recipients
            if(isset($mailData['MAIL_FROM_NAME']) && $mailData['MAIL_FROM_NAME'] && isset($mailData['MAIL_FROM']) && $mailData['MAIL_FROM']) {
                $mail->setFrom($mailData['MAIL_FROM'], $mailData['MAIL_FROM_NAME']);
            }elseif(isset($mailData['MAIL_FROM_NAME']) && $mailData['MAIL_FROM_NAME']) {
                $mail->setFrom($_ENV['MAIL_FROM'], $mailData['MAIL_FROM_NAME']);
            }else {
                $mail->setFrom($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
            }

            if(is_array($mailData['receiver_email']) && !empty($mailData['receiver_email'])){
                foreach($mailData['receiver_email'] as $key =>$sentTo){
                    if(is_array($mailData['receiver_name']) && !empty($mailData['receiver_name'])){
                        foreach($mailData['receiver_name'] as $rkey => $receivername){
                            if(!empty($receivername)){
                                $mail->addAddress($sentTo, $receivername);
                            }else{
                                $mail->addAddress($sentTo);
                            }
                            $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
                            if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                                $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                            }
                            if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                                $mail->addBCC($mailData['bcc_email']);
                            }
                            //Attachments
                            if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                                // $mail->addAttachment($mailData['attachment']);                            // Add attachments
                                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                                if(isset($mailData['attachment_name'])){
                                    $mail->addAttachment($mailData['attachment'],$mailData['attachment_name'],
                                        'base64',
                                        'mime/type');
                                }else{
                                    $mail->addAttachment($mailData['attachment']);
                                }
                            }
                            //Content
                            $mail->isHTML(true);                                                           // Set email format to HTML
                            $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                            $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                            $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                            $mail->send();
                            $mail->clearAddresses();
                        }
                    }else{
                        $mail->addAddress($sentTo);
                        $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
                        if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                            $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                        }
                        if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                            $mail->addBCC($mailData['bcc_email']);
                        }
                        //Attachments
                        if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                            // $mail->addAttachment($mailData['attachment']);                            // Add attachments
                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                            if(isset($mailData['attachment_name'])){
                                $mail->addAttachment($mailData['attachment'],$mailData['attachment_name'],
                                    'base64',
                                    'mime/type');
                            }else{
                                $mail->addAttachment($mailData['attachment']);
                            }
                        }
                        //Content
                        $mail->isHTML(true);                                                           // Set email format to HTML
                        $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                        $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                        $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                        $mail->send();
                        $mail->clearAddresses();
                    }

                }

            }else{

                $mail->addAddress($mailData['receiver_email'], $mailData['receiver_name']);     // Add a recipient
                //$mail->addAddress('sudhanshuk@bravvura.in');                                  // Name is optional
                $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
                if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                    $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                }
                if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                    $mail->addBCC($mailData['bcc_email']);
                }
                //Attachments
                if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                    // $mail->addAttachment($mailData['attachment']);                            // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                    if(isset($mailData['attachment_name'])){
                        $mail->addAttachment($mailData['attachment'],$mailData['attachment_name'],
                            'base64',
                            'mime/type');
                    }else{
                        $mail->addAttachment($mailData['attachment']);
                    }
                }
                //Content
                $mail->isHTML(true);                                                           // Set email format to HTML
                $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                $mail->send();
                $mail->clearAddresses();

            }
            return "Message has been sent";
        } catch (Exception $e) {
            return 'Message could not be sent. Mailer Error';
        }
    }
    /*
     * sendSms is used for sms accept array variable that containg below data
     * array[message,user_mob_no]
     */
    public static function sendSms($smsData) {
	self::setEnvConfiguration(1);
		
        $userMobNo = preg_replace('/\s+/', '', str_replace(" ","",$smsData['user_mob_no']));
        $SmsText = $smsData['message'];
        $clickatellConfig = [
            'cat_auth_url' => $_ENV['SMS_AUTH_URL'],
            'cat_ac_username' => $_ENV['SMS_AC_USERNAME'],
            'cat_ac_password' => $_ENV['SMS_AC_PASSWORD'],
            'cat_api_id' => $_ENV['SMS_API_ID'],
            'cat_sendmsg_url' => $_ENV['SMS_SEND_URL'],
            'cat_from' => $_ENV['SMS_FROM'],
        ];

        self::clickATellSms($clickatellConfig, $userMobNo, $SmsText);
        return "Message sent";
    }
    private static function clickATellSms($clickatellConfig, $userMobNo = false, $SmsText = false) {
        $smsresponse = false;
        if ($userMobNo && $SmsText) {
            $SmsText = urlencode($SmsText);
            //auth call
            $url_auth = $clickatellConfig['cat_auth_url'] . "?user=" . $clickatellConfig['cat_ac_username'] . "&password=" . $clickatellConfig['cat_ac_password'] . "&api_id=" . $clickatellConfig['cat_api_id'];
            $ret_auth = self::sendRequest($url_auth, 'get'); // do auth call
            $sess = explode(":", $ret_auth); // explode Auth response. 

            if ($sess[0] == 'OK') {
                $sess_id = trim($sess[1]); // remove any whitespace
                $url_send = $clickatellConfig['cat_sendmsg_url'] . "?session_id=" . $sess_id . "&to=" . $userMobNo . "&text=" . $SmsText . "&mo=1&from=" . $clickatellConfig['cat_from'];
                $ret_send = self::sendRequest($url_send, 'get'); // do sendmsg call               
                $send = explode(":", $ret_send);
                if ($send[0]) {
                    $smsresponse = true;
                }
            }

        }
        return $smsresponse;
    }
    public static function netcoreEvent($data) {
        $postDataArr = array(
            "EMAIL" => $data['email'],
            "FIRST_NAME" => isset($data['first_name']) ? $data['first_name'] : "",
            "MOBILE" => isset($data['mobile']) ? (string) $data['mobile'] : "",
            "IS_REGISTER" => isset($data['is_register']) ? (string) $data['is_register'] : "",
            "LAST_NAME" => isset($data['last_name']) ? (string) $data['last_name'] : "",
            "REGISTRATION_DATE" => isset($data['registration_date']) ? $data['registration_date'] : "",
            "HOST_URL" => isset($data['host_url']) ? $data['host_url'] : ""
        );
        $postData = json_encode($postDataArr);
        $url = $_ENV['NETCORE_URL'] . "/" . $_ENV['NETCORE_API_VERSION_APIV2'] . "?" . "type=contact&activity=add&listid=1&apikey=" . $_ENV['NETCORE_API_KEY'];
        $url .= "&data=" . urlencode($postData);
        return self::sendRequest($url, 'post');
    }
    private static function sendRequest($url, $method = false, $postData = array(), $htmlBuildQuery = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post") {
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($htmlBuildQuery && !empty($postData)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
            } elseif (!empty($postData)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            }
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return  $response;
    }
    public static function mailTemplate($mailFor, $mailKeywords = array(), $header="", $footer="", $restaurantId) {
        if ($mailFor) {$header =  "header_layout"; $footer =  "footer_layout";
            if($header=="")
                $header =  "header_layout";
            if($footer=="")
                $footer =  "footer_layout";
            if($restaurantId=="")
                $restaurantId = 20;
            $templateContent = MailTemplate::where(['template_name' => $mailFor, 'language_id' => self::$langId])->whereIn('restaurant_id', array($restaurantId))->orderBy('restaurant_id', 'DESC')->get()->toArray();
            $header = self::getHeaderLayout($header, $restaurantId)[0]['content'];
            $content = $templateContent[0]['content'];
            $footer = self::getFooterLayout($footer, $restaurantId)[0]['content'];
            $mailContent = $header . $content . $footer;
            $pattern = '[%s]';
            foreach ($mailKeywords as $key => $val) {
                $keywordMap[sprintf($pattern, $key)] = $val;
            }
//            print_R($keywordMap);
//            die;
            $mailTemplate = strtr($mailContent, $keywordMap);

            return $mailTemplate;
        }
        return false;
    }
    private static function getHeaderLayout($header, $restaurantId) {
        return MailTemplate::where(['template_name' => $header, 'language_id' => self::$langId])->whereIn('restaurant_id', array($restaurantId))->orderBy('restaurant_id', 'DESC')->limit(1)->get()->toArray();
    }
    private static function getFooterLayout($footer, $restaurantId) {
        return MailTemplate::where(['template_name' => $footer, 'language_id' => self::$langId])->whereIn('restaurant_id', array($restaurantId))->orderBy('restaurant_id', 'DESC')->limit(1)->get()->toArray();
    }
    public static function getSMSContent($smsFor) {
        return SmsContent::where(['sms_name' => $smsFor, 'language_id' => self::$langId])->get()->toArray();
    }
    /*
     * format the restarant polygon data into array
     */
    public static function formatDeliveryGeo($deliveryGeo = false) {
        $result = [];
        if (!empty($deliveryGeo)) {
            $polygonData = str_replace('POLYGON((', '', $deliveryGeo);
            $polygonData = str_replace('))', '', $polygonData);
            $polygonData = trim($polygonData);
            $polygonData = explode(',', $polygonData);
            foreach ($polygonData as $key => $polyvalue) {
                $polLatLng = explode(" ", trim($polyvalue));
                $result[] = $polLatLng[0] . " " . $polLatLng[1];
            }
        }
        return $result;
    }
    public static function getRelativeCityDateTime(array $options = array(), $dateTime = 'now', $format = '') {

        $cityTimeZone = static::getTimeZoneMapped($options);
        if(isset($cityTimeZone['error']) && $cityTimeZone['error']){
            self::$timezoneError = $cityTimeZone;
            return true;
        }

        if (strtolower($dateTime) == 'now' && $cityTimeZone) {
            $dateTime = new \DateTime();
            return $dateTime->setTimezone(new \DateTimeZone($cityTimeZone));
        }
        $dateTimeObject = \DateTime::createFromFormat($format, $dateTime, static::getDateTimeZone());
        return $dateTimeObject->setTimezone(new \DateTimeZone($cityTimeZone));
    }
    /**
     * Get time zone mapping according to restaurant_id, state_code, state_timezone
     *
     * @param array $options
     * @throws \Exception
     * @return string TimeZoneText
     */
    public static function getTimeZoneMapped(array $options = array()) {
        $city = new City();
        if (isset($options['state_timezone']) && $options['state_timezone']) {
            return $options['state_timezone'];
        } elseif (isset($options['state_code']) && $options['state_code']) {
            if (isset(static::$_cache['state_code_timezone']) && static::$_cache['state_code_timezone']) {
                return static::$_cache['state_code_timezone'];
            }
            // Get time zoen with state code
            $timeZoneResult = $city::Where(['state_code' => $options['state_code']])->get()->toArray();
            if ($timeZoneResult) {
                static::$_cache['state_code_timezone'] = $timeZoneResult['time_zone'];
                return static::$_cache['state_code_timezone'];
            }
        } elseif (isset($options['restaurant_id']) && $options['restaurant_id']) {
            if (isset(static::$_cache['restaurant_id_timezone']) && static::$_cache['restaurant_id_timezone']) {
                return static::$_cache['restaurant_id_timezone'];
            }
            $resturantModel = new Restaurant();
            $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')
                ->where(['restaurants.id' => $options['restaurant_id']])->get()->toArray();

            if (isset($timeZoneResult[0]['time_zone']) && !empty($timeZoneResult[0]['time_zone'])) {
                static::$city_id = $timeZoneResult[0]['city_id'];
                static::$_cache['restaurant_id_timezone'] = $timeZoneResult[0]['time_zone'];
                return static::$_cache['restaurant_id_timezone'];
            }else{
                return array('message'=>'Invalid Options for DateTime Please provide restaurant_id or state_code or state_timezone','error'=>true);
            }
        }
        return array('message'=>'Invalid Options for DateTime Please provide restaurant_id or state_code or state_timezone','error'=>true);
    }
    public static function getDateTimeZone() {
        if (!static::$_date_time_zone) {
            static::setDateTimeZone();
        }
        return static::$_date_time_zone;
    }
    public static function setDateTimeZone($dateTimeZoneText = null) {
        $defaultDateTimeZoneText = date_default_timezone_get();
        if (!$dateTimeZoneText || !is_string($dateTimeZoneText)) {
            $dateTimeZoneText = $defaultDateTimeZoneText;
        }
        try {
            $dateTimeZone = new \DateTimeZone($dateTimeZoneText);
        } catch (\Exception $ex) {
            $dateTimeZone = new \DateTimeZone($defaultDateTimeZoneText);
        }
        static::$_date_time_zone = $dateTimeZone;
    }
    public static function distanceBetweenTwoPointOnEarth() {
        $miles = "";
        $theta = self::$lon1 - self::$lon2;
        $dist = sin(deg2rad(self::$lat1)) * sin(deg2rad(self::$lat2)) + cos(deg2rad(self::$lat1)) * cos(deg2rad(self::$lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper(self::$unit);
        return number_format((float)$miles, 2, '.', '');


    }

    public static function getAddonsAndModifiersForMailer($addon_modifier){
        $data='';

        if(isset($addon_modifier['modifier_data']) && count($addon_modifier['modifier_data'])){

            $modifier_data_records=$addon_modifier['modifier_data'];
            // $data.='<div class="food_order_item padding-left-25"> Modifiers :-</div>';
            $addonDataArrary = array();
            $i = 0;
            foreach($modifier_data_records as $modifier_data){
                $groupname='';
                $price=0;
                $quantity=1;

                if(isset($modifier_data['quantity']) && !empty($modifier_data['quantity'])){
                    $quantity=$modifier_data['quantity'];
                }
                if(isset($modifier_data['price']) && !empty($modifier_data['price'])){
                    $price=$modifier_data['price'];
                }
                if(isset($modifier_data['group_prompt']) && !empty($modifier_data['group_prompt'])){
                    $groupname = $modifier_data['group_prompt'] ;
                }

                $addonDataArrary[$groupname][$i]['name'] = $modifier_data['modifier_name'];
                $addonDataArrary[$groupname][$i]['quantity'] = $quantity;
                $addonDataArrary[$groupname][$i]['price'] = $price;
                $i++;
                // $data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$modifier_data['modifier_name']." <b>".$groupname."</b> </div> <div class=\"col-md-2 col-xs-2 text-center\">".$quantity."</div> <div class=\"col-md-2 col-xs-2 text-right mb0\"> ".'$'. @number_format($price,2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\"> ".'$'. number_format($price * $quantity,2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\"></div> </div>";

                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$modifier_data['modifier_name']." ".$groupname." - <span style='float: right;'> ".$quantity." * ".number_format($price,2)."</span> </div> </div>";
                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$modifier_data['modifier_name']." ".$groupname."</div> </div>";

            }

            foreach($addonDataArrary as $key => $value){
                $abc = '';
                $pArr = array();
                $abc = '<b>'.$key.': </b>';
                foreach($value as $innerArr){
                    if($innerArr['quantity'] > 1){
                        $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                    }else{
                        $pArr[] = $innerArr['name'];
                    }
                }
                $data .= '<tr style="font-size:11px;line-height:18px;"><td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td></tr>';
            }

        }elseif(isset($addon_modifier['addons_data']) && count($addon_modifier['addons_data'])){

            $addons_data_records=$addon_modifier['addons_data'];
            //$data.='<div class="food_order_item padding-left-25"> Addons :-</div>';
            $addonDataArrary = array();
            $i = 0;
            foreach($addons_data_records as $addons_data){
                $groupname='';
                if(isset($addons_data['addongroup']) && !empty($addons_data['addongroup'])){
                    $groupname = $addons_data['addongroup']['prompt'] ;
                }

                $addonDataArrary[$groupname][$i]['name'] = $addons_data['option_name'];
                $addonDataArrary[$groupname][$i]['quantity'] = $addons_data['quantity'];
                $addonDataArrary[$groupname][$i]['price'] = $addons_data['price'];
                $i++;
                //  $data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$addons_data['option_name'] ." <b>".$groupname."</b> </div> <div class=\"col-md-2 col-xs-2 text-center\">".$addons_data['quantity']."</div> <div class=\"col-md-2 col-xs-2 text-right mb0\">".'$'. @number_format($addons_data['price'],2) ." </div> <div class=\"col-md-2 col-xs-2 text-right\">".'$'. number_format($addons_data['price'] * $addons_data['quantity'],2)." </div> <div class=\"col-md-2 col-xs-2 text-right\"></div> </div>";


                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$addons_data['option_name']." ".$groupname." - <span style='float: right;'>".$addons_data['quantity']." * ".number_format($addons_data['price'],2)."</span> </div> </div>";
                //$data.="<div class=\"row item_addons\"> <div class=\"col-md-4 col-xs-4 add__onItem padding-left-25\"> ".$addons_data['option_name']." ".$groupname."</div> </div>";

            }
            foreach($addonDataArrary as $key => $value){
                $abc = '';
                $pArr = array();
                $abc = '<b>'.$key.': </b>';
                foreach($value as $innerArr){
                    if($innerArr['quantity'] > 1){
                        $pArr[] = $innerArr['name'].' X'.$innerArr['quantity'];
                    }else{
                        $pArr[] = $innerArr['name'];
                    }
                }
                $data .= '<tr style="font-size:11px;line-height:18px;"><td style="padding-left:10px" >'.$abc.implode(', ', $pArr).'</td></tr>';
            }
        }

        return $data;
    }

    public static function getAddonsAndModifiers($order_data,$type='order'){

        $is_byp=$order_data->is_byp;
        $is_pot=$order_data->is_pot;

        $data=[];
        if(!empty($order_data->menu_json) ){
            if(!is_array($order_data->menu_json))
                $jsonData = json_decode($order_data->menu_json, true);
            else $jsonData = $order_data->menu_json;
            if(isset($jsonData['modifier_items'])){
                $data['modifier_data']= $jsonData['modifier_items'];
                //modifer category,modifer option group ,modifier option need to discuss
            }
        }
        if($type=='order'){
            $addon_options= OrderDetailOptions::where('user_order_detail_id',$order_data->id)->get();

        }else{
            $addon_options= MybagOptions::where('bag_id',$order_data->id)->get();

        }

        if( $addon_options){
            $option_data=[];
            foreach ($addon_options as $addon_menu) {
                $addongroup= (object)[];
                $addonInfo = ItemAddons::where('id', $addon_menu->option_id)->with('addongroup')->first();
                if ($addonInfo) {
                    $addongroup= $addonInfo->addongroup;
                }
                $option_data[]=[
                    'option_name'=>$addon_menu->option_name,
                    'option_id'=>$addon_menu->option_id,
                    'price'=>$addon_menu->price,
                    'quantity'=>$addon_menu->quantity,
                    'user_order_detail_id'=>$order_data->id,
                    'total_amount'=>$addon_menu->total_amount,
                    'addongroup'=>$addongroup,

                ];

                $data['addons_data']=$option_data;
            }
        }

        return  $data;
    }
    /* Function to Convert my bag json data in array format */
    public static function changeMyBagJsonData($isBYP, $jsonData){
        //echo $isBYP; die;
        $addons_data = [];
        if($isBYP==0) {
            if(!is_null($jsonData)) {
                $bagData = $jsonData;
                //print_r($bagData); die;
                foreach($bagData as $val){
                    if(isset($val['items'])){
                        $item_id = $val['id'];
                        $addons_label = $val['label'];
                        foreach($val['items'] as $data){
                            if($data['default_selected'] == 0) {
                                continue;
                            }
                            $label_addons = [];
                            $addons_option = '';
                            $menu_addons_option_id = $data['id'];
                            $MenuSettingItem = MenuSettingItem::select(['menu_setting_category_id', 'name', 'price'])->where('id', $menu_addons_option_id)->first();
                            //$menu_addons_id = $MenuSettingItem->menu_setting_category_id;
                            $menu_addons_id = $MenuSettingItem['menu_setting_category_id'];
                            $addons_name = $data['label'];
                            $price = $data['price'];
                            $addons_option = '';
                            if(!is_null($data['quantity'])) {
                                if (!empty($data['quantity'])) {
                                    //print_r([$menu_addons_option_id, $menu_addons_id]);
                                    foreach ($data['quantity'] as $qty_items) {
                                        if (isset($qty_items['is_checked'])) {
                                            $label_addons [] = $qty_items['label'];
                                        }
                                    }
                                }
                                if (!empty($data['sides'])) {
                                    foreach ($data['sides'] as $qty_sides) {
                                        if (isset($qty_sides['is_checked'])) {
                                            $label_addons [] = $qty_sides['label'];
                                        }
                                    }
                                }
                                $addons_option = implode(',', $label_addons);
                            }

                            $addons_data [] = [
                                'menu_addons_id' => $menu_addons_id,
                                'menu_addons_option_id' => $menu_addons_option_id,
                                'addons_label' => $addons_label,
                                'addons_name' => $addons_name,
                                'addons_option' => $addons_option,
                                'price' => $price,
                                'quantity' => 1,
                                'selection_type' => 0, //Not sure of the values yet,
                                'was_free' => 0,   //Not sure of the values yet,
                                'priority' => 1,   //Not sure of the values yet,
                            ];

                        }
                    }
                }
            }

        }
        else if($isBYP==1){
            //print_r($jsonData);
            $addoncData = [];
            $label = $addonVal = "";
            foreach($jsonData as $key=>$value){
                if($key=='label'){
                    continue;
                }
                $label = $key;
                if(is_array($value)){
                    $arrData = [];
                    $str_head='';
                    foreach($value as $data) {
                        $arr = [];
                        $addons_option = "";
                        if (is_array($data)) {
                            $mod_val='';
                            $headkey='';
                            foreach($data as $k=>$val){
                                if($k==$label) {
                                    $label = $label;
                                    $headkey=$val;
                                }
                                else{
                                    $arr[]= ucfirst($val);
                                }

                                $mod_val='';
                            }
                            $addons_option = implode(',', $arr);
                            $str_head.=ucwords($headkey).'- ['.ucwords($addons_option).']'."\n";
                            //$addonVal = $addons_option;
                        }
                        else{
                            $addonVal = $data;
                        }
                    }
                    if(!empty($str_head)){
                        $addonVal=nl2br($str_head);
                    }
                }
                else {
                    $addonVal =  $value;
                }

                $addons_data[ucwords($label)] = $addonVal;
            }
            //print_r($addons_data);
        }
        return $addons_data;
    }
    /*End function */
    /**
     * @param Menu_item_id
     * @author Rahul Gupta
     * @date 27-07-2018
     * Static is defined inorder to call from ClassName only
     */
    public static function get_menu_item_images($menu_item_id) {
        $images = array();
        $menuItem = MenuItems::where('id', $menu_item_id)->select('id', 'name','description','images')->first();
        if($menuItem && $menuItem->images) {
            $images = json_decode($menuItem->images, true);
        }
        // $images = array(
        //     'desktop_web_images' => array(
        //         '0' => array(
        //             'web_image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'web_thumb_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'web_thumb_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'web_image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'web_image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'web_thumb_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'web_thumb_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'web_image_caption' => 'Pasta'
        //         ),
        //     ),
        //     'mobile_web_images' => array(
        //         '0' => array(
        //             'mobile_web_image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'mobile_web_thumb_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'mobile_web_thumb_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'mobile_web_image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'mobile_web_image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'mobile_web_thumb_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'mobile_web_thumb_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'mobile_web_image_caption' => 'Pasta'
        //         ),
        //     ),
        //     'mobile_app_images' => array(
        //         '0' => array(
        //             'image' => '/images/menu/3_heart-pizza_18268214325b361de436063.png',
        //             'thumb_image_med' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'thumb_image_sml' => '/images/menu/thumb/3_heart-pizza_18268214325b361de436063_med.png',
        //             'image_caption' => 'Pizza'
        //         ),
        //         '1' => array(
        //             'image' => '/images/menu/4_VodkaRigatoni_680_3315085655afc4e6191059.png',
        //             'thumb_image_med' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.png',
        //             'thumb_image_sml' => '/images/menu/thumb/4_VodkaRigatoni_680_3315085655afc4e6191059_med.pngg',
        //             'image_caption' => 'Pasta'
        //         ),
        //     ),
        // );       
        return $images;
    }
    /**
     * @param Menu_item_id
     * @author Rahul Gupta
     * @date 31-07-2018
     * Static is defined inorder to call from ClassName only
     */
    public static function getLocalTimeBased($options = array()) {
        $return_datatime =  $options['datetime'];
        $resturantModel = new Restaurant();
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $options['restaurant_id']])->first();
        if($timeZoneResult && $timeZoneResult->time_zone && $options['datetime']) {
            $datetime = new \DateTime($options['datetime']);
            $la_time = new \DateTimeZone($timeZoneResult->time_zone);
            $datetime->setTimezone($la_time);
            $return_datatime =  $datetime->format('Y-m-d H:i:s');
        }
        return $return_datatime;
    }
    /**
     * @param Parent Rest ID/ Location ID, fetch_parent_rest, Fields
     * @author Rahul Gupta
     * @date 06-08-2018
     * It will send restaurant Details based on the fetch_parent_rest = true / false
     */
    public static function getRestaurantDetails($rest_id, $fetch_parent_rest = false, $fields = array()) {
        $restInfo = $restDetails = array();
        $resturantModel = new Restaurant();
        if($fetch_parent_rest) {
            // Get the details of parent Rest 
            $restDetails = Restaurant::Join('restaurants AS r2', 'restaurants.id', 'r2.parent_restaurant_id')->select($fields)->where(['restaurants.id' => $rest_id])->first();
        }else {
            // Get detail of child restaurants
            $restDetails = Restaurant::select($fields)->where(['restaurants.id' => $rest_id])->first();
        }
        if($restDetails) {
            $restInfo = $restDetails->toArray();
        }
        return $restInfo;
    }
    /**
     * @param $size_price_list = array
     * @param $location_id = Branch Id
     * @param $current_time = Restaurant Current Time
     */
    public static function getSizePrice(&$size_price_list, $location_id, $current_time, $extra = array()) {       
        $current_menu_meal = DB::table('menu_meal_types')
                ->whereTime('to_time', ' >= ', $current_time)
                ->whereTime('from_time', '<= ', $current_time)
                ->where('restaurant_id', $location_id)
                ->where('status', 1)
                ->first();
        if ($size_price_list) {
            foreach ($size_price_list as $key => $size) {
                $has_price = false;
                if ((count($size['price']) > 1) || !isset($size['price']['0'])) {

                    // It is price based on menu meal
                    if ($current_menu_meal) {
                        if (isset($size['price'][$current_menu_meal->id])) {

                            $size_price_list[$key]['price'] = $size['price'][$current_menu_meal->id];
                            if (isset($size['available'])) {
                                $size_price_list[$key]['is_delivery'] = $size['available'][$current_menu_meal->id]['is_delivery'];
                                $size_price_list[$key]['is_carryout'] = $size['available'][$current_menu_meal->id]['is_carryout'];
                            } else {
                                // for old data in DB if any 
                                $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                            }
                            $has_price = true;
                        }
                    } else {
                        // unset this price item as This is not available in meal type @26-07-2018
                        unset($size_price_list[$key]);
                    }
                } elseif (count($size['price']) == 1) {
                    // It is all price

                 //   $size_price_list[$key]['price'] = $size['price']['0'];
                    $size_price_list[$key]['price'] = $size['price']['0'];
		    $size_price_list[$key]['actual_price'] = $size['price']['0'];
                    if(isset($size['special_price']) && isset($size['start_date']) && isset($size['end_date'])){
                        $special_price=$size['special_price'];
                        $special_price_start_date=$size['start_date'];
                        $special_price_end_date=$size['end_date'];

                        $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                        $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                        $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                        if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
                            $size_price_list[$key]['price'] = $special_price;
                            $size_price_list[$key]['cutout_price'] = $size['price']['0'];

                            unset($size_price_list[$key]['special_price']);
                            unset($size_price_list[$key]['start_date']);
                            unset($size_price_list[$key]['end_date']);
                        }
                    }
		    if(isset($size['happy_hour_price']) && isset($size['from_time']) && isset($size['to_time'])){
                        $special_price=$size['happy_hour_price'];
                        $special_price_start_date=date("Y-m-d ").$size['from_time'];
                        $special_price_end_date=date("Y-m-d ").$size['to_time'];

                        $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $location_id, 'datetime' =>date('Y-m-d H:i:s')));
                        $start_date=date('Y-m-d H:i:s',strtotime($special_price_start_date));
                        $end_date=date('Y-m-d H:i:s',strtotime($special_price_end_date));

                        if(($start_date<=$cur_date)&& ($end_date>=$cur_date)){
			    
                            $size_price_list[$key]['price'] = $special_price;
                            $size_price_list[$key]['cutout_price'] = $size['price']['0'];

                            
                        }
			unset($size_price_list[$key]['happy_hour_price']);
                        unset($size_price_list[$key]['from_time']);
                        unset($size_price_list[$key]['to_time']);
                    }
                    if (isset($size['available'])) {
                        $size_price_list[$key]['is_delivery'] = $size['available']['is_delivery'];
                        $size_price_list[$key]['is_carryout'] = $size['available']['is_carryout'];
                    } else {
                        // for old data in DB if any 
                        $size_price_list[$key]['is_delivery'] = $size_price_list[$key]['is_carryout'] = 1;
                    }
                    $has_price = true;
                }
                //check if price does not exists then remove it from the array
                if (!$has_price) {
                    unset($size_price_list[$key]);
                }
                # 9-Aug-19 - As discussed with Deepak, Prakash, Nishant
                // check if is_delivery & is_carryout is 0, remove it from the array
                if(isset($size_price_list[$key]['is_delivery']) && isset($size_price_list[$key]['is_carryout']) && $size_price_list[$key]['is_delivery'] == 0 && $size_price_list[$key]['is_carryout'] == 0) {
                    unset($size_price_list[$key]);
                }
                unset($size_price_list[$key]['available']);
            }
            $size_price_list = array_values($size_price_list);
        }
        #check availabiltity of the menu Items @21-12-2018 RG
        /*if(isset($extra['slot_code']) && $extra['slot_code']) {
            $check_availablity = $this->check_menu_availablity($extra);
           
            foreach ($size_price_list as &$list) {
                if($list['is_delivery'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_delivery'] = $check_availablity['is_delivery'];
                }
                if($list['is_carryout'] == 1) {
                    //means delivery is allowed for that Item
                    //Now check if that item is available in that time
                    $list['is_carryout'] = $check_availablity['is_carryout'];
                }
            }
        }*/
        # End visibility Time        
    }	
    /**
     * @param get Restaurnat mail template
     * @author Rahul Gupta
     * @date 14-08-2018
     * It will filter data
     */
    public static function restMailTemplate($mailFor, $restaurantId, $mailKeywords = array()) {
        if ($mailFor) {
            $header = MailTemplate::where(['template_name' => 'header_layout', 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($header) {
                $header = $header->toArray();
                // format header with keys
                $header =  $header['content'];
            }
            $templateContent = MailTemplate::where(['template_name' => $mailFor, 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($templateContent) {
                $templateContent = $templateContent->toArray();
                // format header with keys
                $templateContent =  self::getFinalTemplate($mailKeywords['data'], $templateContent['content']);
            }
            $footer = MailTemplate::where(['template_name' => 'footer_layout', 'language_id' => self::$langId])->whereIn('restaurant_id', array(0, $restaurantId))->orderBy('restaurant_id', 'DESC')->first();
            if($footer) {
                $footer = $footer->toArray();
                // format header with keys
                $footer =  $footer['content'];
            }
            $mailContent = $header . $templateContent . $footer;
            return $mailContent;
        }
        return false;
    }
    /**
     * @param Data array and email tenplate content
     * @author Rahul Gupta
     * @date 14-08-2018
     * It will send replaced content with data
     */
    public static function getFinalTemplate(array $data,$template_content){
        if($template_content){

            $content = $template_content;
            preg_match_all("/\[(.*?)\]/",$content, $options_array);

            if(isset($options_array[1]) && count($options_array[1])) {
                foreach($options_array[1] as $key){
                    if (array_key_exists(strtolower($key),$data))
                    {
                        $content=preg_replace('/\['.strtolower($key).'\]/',$data[strtolower($key)],$content);
                    }elseif (array_key_exists(strtoupper($key),$data)){
                        $content=preg_replace('/\['.strtoupper($key).'\]/', $data[strtoupper($key)],$content);
                    }
                }
                return $content;
            }
        }
    }
    /**
     * @param Token ID
     * @author Rahul Gupta
     * @date 20-08-2018
     * It will validate the token and check expiry time
     */
    public static function check_opa_token($token) {
        $response = array('is_valid' => false);
        $opa_token = CmsAuth::select('id', 'token', 'expiry', 'restaurant_id')->where(['token' => $token])->where('expiry','>',Carbon::now())->first();
        if($opa_token) {
            $response = array('is_valid' => true, 'restaurant_id' => $opa_token->restaurant_id);
        }
        return $response;
    }
    /**
     * @param Token ID
     * @author Rahul Gupta
     * @date 20-08-2018
     * It will validate the current version of OPA app
     */
    public static function check_opa_version($restaurant_id, $request_version) {
        $response = array('is_valid' => true);
        $opa_version = Restaurant::select('id', 'current_version')->where(['id' => $restaurant_id])->first();
        if($opa_version->current_version > $request_version) {
            $response = array('is_valid' => false, 'message' => "Sorry, You are using older version($request_version) of app. Please update to latest version $opa_version->current_version");
        }
        return $response;
    }

    public static function pubnubPushNotification($data){
        $publishKey = $_ENV['DASHBOARD_PUBNUB_PUB'];
        $subscriberKey = $_ENV['DASHBOARD_PUBNUB_SUB'];
        $pnConfiguration = new PNConfiguration();
        $pnConfiguration->setSubscribeKey($subscriberKey);
        $pnConfiguration->setPublishKey($publishKey);
        $pnConfiguration->setSecure(false);

        $pubnub = new PubNub($pnConfiguration);
        $user_current_notification['aps'] = array('alert' => $data['msg'], 'badge' => 1);
        $user_current_notification['restaurant_id'] = $data['restaurant_id'];
        $user_current_notification['order_id']=$data['order_id'];
        $user_current_notification['type'] = $data['type'];
        try{
            ##### Push notification ####   
		$ms =  [
                    $user_current_notification,
                    "msg"=>$data['msg'],
                    "type"=>$data['type'],
                    "channel"=>$data['channel'],
                    "curDate"=>$data['curDate'],
                    'is_friend'=>$data['is_friend'],
                    'restaurant_id'=>$data['restaurant_id'],
                    'username'=>$data['username'],
                    'order_id'=>$data['order_id'],
                    'order_status'=>$data['order_status']
                ];
            $result = $pubnub->publish()->channel($data['channel'])->message($ms)->shouldStore(true)->usePost(true)->sync();
	    //print_r($ms); //print_r($result);	
            return [$result,$ms];
            ##### End of Push Notification #####

            ##### Start Log #####
             //$log = $pubnub->getLogger()->pushHandler(new ErrorLogHandler());
             //print_r($log);
             //print_r("### End of log ###");
            ##### End of log #####

            #### Start Subscription ####   
//            $subscribeCallback = new PubnubSubscribeCallback(); 
//            $pubnub->addListener($subscribeCallback);
//            $subscribe = $pubnub->subscribe()->channels($data['channel'])->execute();            
//            print_r($subscribe);
//            print_r("### End of Subscription ###");
            #### End of subscription ####

            // some time later
//            $pubnub->removeListener($subscribeCallback);

        }catch(PubNubException $error){
            return false;
        }
    }
    //** **/
    public static function sendManagerMail($mailData) {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output2
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.office365.com';                     // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'support@munchado.biz';             // SMTP username
            $mail->Password = '1234@qwertY';             // SMTP password
            $mail->SMTPSecure = $_ENV['MAIL_ENCRYPTION'];         // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $_ENV['MAIL_PORT'];                     // TCP port to connect to
            //Recipients
            if(isset($mailData['MAIL_FROM_NAME']) && $mailData['MAIL_FROM_NAME']) {
                $mail->setFrom('support@munchado.biz', $mailData['MAIL_FROM_NAME']);
            }else {
                $mail->setFrom('support@munchado.biz', $_ENV['MAIL_FROM_NAME']);
            }

            if(is_array($mailData['receiver_email']) && !empty($mailData['receiver_email'])){
                foreach($mailData['receiver_email'] as $key =>$sentTo){
                    if(is_array($mailData['receiver_name']) && !empty($mailData['receiver_name'])){
                        foreach($mailData['receiver_name'] as $rkey => $receivername){
                            if(!empty($receivername)){
                                $mail->addAddress($sentTo, $receivername);
                            }else{
                                $mail->addAddress($sentTo);
                            }
                            $mail->addReplyTo($_ENV['MAIL_FROM'], $_ENV['MAIL_FROM_NAME']);
                            if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                                $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                            }
                            if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                                $mail->addBCC($mailData['bcc_email']);
                            }
                            //Attachments
                            if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                                $mail->addAttachment($mailData['attachment']);                            // Add attachments
                                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                            }
                            //Content
                            $mail->isHTML(true);                                                           // Set email format to HTML
                            $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                            $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                            $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                            $mail->send();
                            $mail->clearAddresses();
                        }
                    }

                }

            }else{

                $mail->addAddress($mailData['receiver_email'], $mailData['receiver_name']);     // Add a recipient
                //$mail->addAddress('sudhanshuk@bravvura.in');                                  // Name is optional
                $mail->addReplyTo('support@munchado.biz', $_ENV['MAIL_FROM_NAME']);
                if (isset($mailData['cc_email']) && !empty($mailData['cc_email'])) {
                    $mail->addCC($mailData['cc_email'],$mailData['cc_name']);
                }
                if (isset($mailData['bcc_email']) && !empty($mailData['bcc_email'])) {
                    $mail->addBCC($mailData['bcc_email']);
                }
                //Attachments
                if (isset($mailData['attachment']) && !empty($mailData['attachment'])) {
                    $mail->addAttachment($mailData['attachment']);                            // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');                       // Optional name
                }
                //Content
                $mail->isHTML(true);                                                           // Set email format to HTML
                $mail->Subject = isset($mailData['subject']) ? $mailData['subject'] : "";
                $mail->Body = isset($mailData['body']) ? $mailData['body'] : "";
                $mail->AltBody = isset($mailData['AltBody']) ? $mailData['AltBody'] : "";
                $mail->send();
                $mail->clearAddresses();

            }
            return "Message has been sent";
        } catch (Exception $e) {
            return 'Message could not be sent. Mailer Error';
        }
    }
    /*
    * send SMS of based on 
    * RG 18-10-2018
    * sms_module = order/ reservation_enquiry, event_enquiry, contact, catering
    * sms_to = manager / customer
    * restaurant_id = optional 
    * order_id = optional 
    * action = accept / reject / placed / confirmed / ready 
    * mobile_no = array() : Optional if have then Get it from this key otherwise from Order's phone column
    * US No. 6465957373 Neela Mohan No.
    * $options = array(
        'order_id', 'restaurant_id', 'sms_to' ,'sms_module', 'mobile_no'
    );
    */

    public static function getAndSendSms($options = array()) {
        $sms_template = '';
        if(isset($options['mobile_no']) && $options['mobile_no']) {
            $mobile_no = $options['mobile_no'];
        }

        switch ($options['sms_module']) {
            case 'order':
                /************************* ORDER ************ Module */
                $orderInfo =  UserOrder::select('user_orders.id', 'order_type', 'user_orders.status', 'delivery_date', 'delivery_time', 'user_orders.created_at', 'restaurant_id', 'is_asap_order', 'is_guest', 'restaurants.restaurant_name', 'user_orders.phone', 'user_id', 'product_type', 'payment_receipt', 'user_orders.updated_at')
                    ->leftjoin('restaurants', 'restaurants.id', '=', 'user_orders.restaurant_id')
                    ->where('user_orders.id', $options['order_id'])
                    ->first();

                if($orderInfo) {
                    if($orderInfo->phone) {
                        $mobile_no = array($orderInfo->phone);
                    }
                    #$restaurant_name = $orderInfo->restaurant_name;
                    $restaurant_name = htmlentities($orderInfo->restaurant_name, ENT_QUOTES, "UTF-8");
                    // check the Order type : It is either  Food or Merchandise
                    #$restOrderLocalTimestamp = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderInfo->restaurant_id, 'datetime' => $orderInfo->created_at)));
                    if($orderInfo->delivery_date) {
                        $order_requested_date = date('l, F d, Y', strtotime($orderInfo->delivery_date));
                    }
                    if($orderInfo->delivery_time) {
                        $order_requested_time = date('h:i A', strtotime($orderInfo->delivery_time));
                    }
                    if($orderInfo->product_type == 'food_item') {
                        // check Order is delivery or Carryout
                        if(strtolower($orderInfo->order_type) == 'carryout') {
                            # check if Order is ASAP
                            if($orderInfo->is_asap_order) {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 7;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 9;
                                        break;
                                    case 'rejected':
                                        $sms_template = 12;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }

                            }else {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 8;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 10;
                                        break;
                                    case 'rejected':
                                        $sms_template = 13;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }
                            }
                            # Ready State
                            if($options['action'] == 'ready') {
                                $sms_template = 11;
                            }
                            
                        }elseif(strtolower($orderInfo->order_type) == 'delivery') {
                            # check if Order is ASAP
                            if($orderInfo->is_asap_order) {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 1;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 3;
                                        break;
                                    case 'rejected':
                                        $sms_template = 5;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }

                            }else {
                                switch ($options['action']) {
                                    case 'placed':
                                        $sms_template = 2;
                                        break;
                                    case 'confirmed':
                                        $sms_template = 4;
                                        break;
                                    case 'rejected':
                                        $sms_template = 6;
                                        break;

                                    default:
                                        # code...
                                        break;
                                }
                            }

                        }                        
                    }elseif($orderInfo->product_type == 'product') {
                        // merchandise Product
                        switch ($options['action']) {
                            case 'placed':
                                $sms_template = 16;
                                break;
                            case 'confirmed':
                                $sms_template = 17;
                                break;
                            case 'rejected':
                                # Restaurant manager Rejected
                                $sms_template =18;
                                break;
                            case 'Canceled':
                                #customer cancelled by their own
                                $sms_template =19;
                                break;

                            default:
                                # code...
                                break;
                        }
                    }elseif($orderInfo->product_type == 'gift_card') {
                        // Gift Type
                        // merchandise Product
                        $parent_restaurant_info = CommonFunctions::getRestaurantDetailsById($orderInfo->restaurant_id);
                        $parent_restaurant_name = isset($parent_restaurant_info['restaurantParentName']) ? htmlentities($parent_restaurant_info['restaurantParentName'], ENT_QUOTES, "UTF-8") : $restaurant_name;

                        switch ($options['action']) {
                            case 'placed':
                                $sms_template = 22;
                                break;
                            #Buyer will get confirmation Gift Card Sent to Recipient
                            case 'confirmed':
                                $sms_template = 23;
                                $recipient_name = 'Guest';
                                if(isset($options['recipient_name']) && $options['recipient_name']) {
                                    $recipient_name = $options['recipient_name'];
                                }
                                break;

                            default:
                                # code...
                                break;
                        }
                    }
                    # Refund Order
                    if($options['action'] == 'refund') {
                        $amount_refunded = number_format($options['amount_refunded'],2);
                        $sms_template = 25;
                        $receipt_id = $orderInfo->payment_receipt;

                        $timestamp2 = strtotime(CommonFunctions::getLocalTimeBased(array("restaurant_id" => $orderInfo->restaurant_id, 'datetime' => $orderInfo->updated_at)));
                        $order_refund_date = date('M d, Y h:i A', $timestamp2);
                        
                        if($orderInfo->product_type == 'gift_card') {
                            $parent_restaurant_info = CommonFunctions::getRestaurantDetailsById($orderInfo->restaurant_id);
                            $restaurant_name = isset($parent_restaurant_info['restaurantParentName']) ? htmlentities($parent_restaurant_info['restaurantParentName'], ENT_QUOTES, "UTF-8") : $restaurant_name;
                        }
                    }
                }
                break;
            case 'reservation_enquiry':
                /************************* Reservation Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    if(isset($response->no_of_guest)) {
                        $guest = $response->no_of_guest;
                    }elseif(isset($response->no_of_people)) {
                        $guest = $response->no_of_people;
                    }
                    $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    $time_of_event = date('h:i A', strtotime($response->time_of_event));

                    switch ($options['action']) {
                        case 'placed':
                            $sms_template = 14;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'event_enquiry':
                /************************* Event Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    if(isset($response->no_of_guest)) {
                        $guest = $response->no_of_guest;
                    }elseif(isset($response->no_of_people)) {
                        $guest = $response->no_of_people;
                    }
                    $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    $time_of_event = date('h:i A', strtotime($response->time_of_event));

                    switch ($options['action']) {
                        case 'placed':
                            $type_of_event = 'event';
                            $sms_template = 21;
                            if(isset($response->type_of_event)) {
                                $type_of_event = $response->type_of_event;
                            }
                            break;

                        default:
                            # code...
                            break;
                    }
                }
                break;
            case 'catering':
                /************************* Catering Enquiry ************ Module */
                $event = StaticBlockResponse::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                    $response = json_decode($event->response);
                    #$date_of_event = date('l, F d, Y', strtotime($response->date_of_occasion));
                    #$date_of_event = date('M d, Y', strtotime($response->date_of_occasion));
                    if(isset($response->date_of_occasion)) {
                        $date_of_event = date('M d, Y', strtotime($response->date_of_occasion));
                    }else if(isset($response->date_of_event)) {
                        $date_of_event = date('M d, Y', strtotime($response->date_of_event));
                    }
                    switch ($options['action']) {
                        case 'placed':
                            $room_requested = '';
                            if(isset($response->room)) {
                                $room_requested = $response->room;
                            }
                            $sms_template = 20;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            break;
            case 'forgot_password':
                /************************* Catering Enquiry ************ Module */
                $event = UserAuth::find($options['order_id']);
                if($event) {
                    $restInfo = CommonFunctions::getRestaurantDetails($event->parent_restaurant_id, false, array('restaurants.id', 'restaurants.restaurant_name'));
                    #$restaurant_name =  $restInfo['restaurant_name'];
                    $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");


                    switch ($options['action']) {
                        case 'forgot':
                            $sms_template = 24;
                            $otp = $event->otp;
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            break;
            case 'reminder':
                /************************* Catering Enquiry ************ Module */               
                $restInfo = CommonFunctions::getRestaurantDetails($options['restaurant_id'], false, array('restaurants.id', 'restaurants.restaurant_name'));
                #$restaurant_name =  $restInfo['restaurant_name'];
                $restaurant_name = htmlentities($restInfo['restaurant_name'], ENT_QUOTES, "UTF-8");
                $sms_template = 26;
            break;
            default:
                # code...
                break;
        }

        $smsText = '';
        switch ($sms_template) {
            case '1':
                #Order ASAP Delivery
                $smsText = "Martini Genie received your order.";
                break;
            case '2':
                # Future Delivery (same day or not same day)
                $smsText = "$restaurant_name received your order for $order_requested_date at $order_requested_time.";
                break;
            case '3':
                #ASAP Delivery Confirmed
                $smsText = "$restaurant_name is preparing your order.";
                break;
            case '4':
                #Future Delivery Confirmed (same day and not same day)
                $smsText ="$restaurant_name received your order for $order_requested_date at $order_requested_time.";
                break;
            case '5':
                #ASAP delivery rejected by restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your delivery order. Check your inbox for more details.";
                break;
            case '6':
                #Delivery Pre-order Rejected By Restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your delivery pre-order for $order_requested_date at $order_requested_time. Check your inbox for more details.";
                break;
            case '7':
                #Order ASAP Takeout
                $smsText = "Hey, it's $restaurant_name, we got your takeout order! We will update you once your order is confirmed.";
                break;
            case '8':
                #Ordered Future Takeout (same day or not same day)Martini Genie received your order for 
                $smsText = "$restaurant_name received your order for $order_requested_date at $order_requested_time.";
                break;
            case '9':
                #ASAP Takeout Confirmed
                $smsText = "$restaurant_name Staff: We've confirmed your order. We'll have your food ready as soon as possible.";
                break;
            case '10':
                #Future Takeout Confirmed (same day and not same day)
                $smsText = "$restaurant_name Staff: We've confirmed your takeout pre-order for $order_requested_date at $order_requested_time. See you then!";
                break;
            case '11':
                #Takeout Ready
                $smsText = "$restaurant_name Staff: Your takeout order is ready! Check your inbox for more details.";
                break;
            case '12':
                #ASAP takeout rejected by restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your takeout order. Check your inbox for more details.";
                break;
            case '13':
                #Takeout Pre-order Rejected By Restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your takeout pre-order for $order_requested_date at $order_requested_time. Check your inbox for more details.";

                break;
            case '14':
                #Placed a Reservations
                $smsText = "Hey, it's $restaurant_name! We received your reservation request for $guest people on $date_of_event at $time_of_event.";

                break;
            case '15':
                $smsText = "Don't want to receive SMS notifications? Reply “Unsubscribe” and we'll keep communications to email only.";
                break;
            case '16':
                #Placed a Merchandise Order
                $smsText = "Hey, it's $restaurant_name, we got your merchandise order! We will update you once your order is confirmed.";
                break;
            case '17':
                #Merchandise Order Confirmed
                $smsText = "$restaurant_name Staff: We've confirmed your merchandise order. We'll have your gift delivered as soon as possible.";
                break;
            case '18':
                #Merchandise Order Rejected by Restaurant
                $smsText = "$restaurant_name Staff: We're sorry, but we could not accept your merchandise order. Check your inbox for more details.";
                break;
            case '19':
                #Merchandise Order Canceled by User
                $smsText = "$restaurant_name Staff: We've successfully undone your order for you. Bummer.";
                break;
            case '20':
                #Placed a Catering Request
                #$smsText = "Hey, it's $restaurant_name, we got your catering request for $occassion on $date_of_event.";
               $smsText =  "Hey, it's $restaurant_name, we got your catering request for $date_of_event! We will let you know once your request is confirmed.";

                break;
            case '21':
                #Placed an Events Request
                #$smsText =  "Hey, it's $restaurant_name, we got your booking request for a private event on $date_of_event at $time_of_event for $guest people! We will book the same once your request is confirmed.";
                $smsText = "Hey, it's $restaurant_name, we got your event request for $date_of_event at $time_of_event for $guest people! We will let you know once your request is confirmed.";
                break;
            case '22':
                #Placed a Gift Card Order
                $smsText =  "Hey, it's $parent_restaurant_name, we got your gift card order! Check your inbox for details.";
                break;
            case '23':
                #Gift Card Sent to Recipient
                $smsText =  "$parent_restaurant_name Staff: We've sent your gift card to $recipient_name. You're a great gift giver, we're sure they're going to love it.";
                break;
             case '24':
                #Gift Card Sent to Recipient
                $smsText =  "$otp is your OTP for $restaurant_name. Use it to sign in and update your password.";
                break;
            case '25':
                #Ordered Future Takeout (same day or not same day)

                #$smsText = "Hey, it's $restaurant_name, we have refunded $".$amount_refunded." for your Order ID: $receipt_id on $order_refund_date!"; 
                $smsText = "$restaurant_name Staff: We've issued you a refund for your experience with us on $order_refund_date. Check your inbox for more details.";
            break;
            case '26':
                #Send Services off sms to Restaurant Manager

                $smsText =  "Munch Ado here, just reminding you to turn ".$options['services']." back on for $restaurant_name. Check your inbox or tablet for details. ";

            break;
            default:
                # code...
                break;
        }
        if($smsText && $mobile_no) {
            #$resp = NotifySms::dispatch($mobile_no['0'] ,$smsText);
            CommonFunctions::sendSms(array('message' => $smsText, 'user_mob_no' => $mobile_no['0']));
        }
    }
    /**
     * @param User_id
     * @author Rahul Gupta
     * @date 22-10-2018
     * It will update category of user, Like New Or Repeat Customer
     * Order (Except rejected and cancelled ) + Reservation > 1 Then Repeat else New Customer
     */
    public function updateCustomerCategory($options = array()) {
        $reservations = DB::table('user_restaurant_resv')
            ->select(DB::raw("count(id) as total_reservations"))
            ->where(['user_id' => $user_id, 'status' => 1])
            ->first();
        $orders = DB::table('user_orders')
            ->select(DB::raw("count(id) as total_orders"))
            ->where(['user_id' => $user_id, 'status' => 1])
            ->first();

        $guests =  User::where('users.id', $options['user_id'])
            ->select('users.id', 'users.fname', 'users.lname', 'users.email', 'users.mobile', 'category')
            ->with(['orders' => function($query) {
                $query->select(DB::raw("count(id) as total_orders"))
                    ->whereNotIn('status',['cancelled','rejected'])->orderBy('created_at', 'desc');
            }, 'reservations' => function($query)  {
                $query->select(DB::raw("count(id) as total_reservations"));
            }
            ]);
        if ($user_update) {
            $user_update->category = config('constants.guest_category.Repeat');
            $user_update->save();
        }
        return $return_datatime;
    }

/***************************SALTY GIFT CARD RELATED************************************* */
    
    public static function promotionProduct($restaurant_id,$type) {

        $promo_product= DB::table('menu_items')
                ->where(['restaurant_id' => $restaurant_id,'product_type' => $type])
                ->first(['id']);
        
        return $promo_product;
    }
    
    public static function productPromotions($restaurant_id) {
        $promos = DB::table('restaurant_promotions')
            ->where(['restaurant_id' => $restaurant_id])
            ->first();

        return $promos;
    }

    public static function productPromoCondition($unitprice, $conditionprice, $oprt) {
        switch($oprt) {
            case '=': return $unitprice = $conditionprice;
            case '>': return $unitprice > $conditionprice;
            case '<': return $unitprice < $conditionprice;
            case '<=': return $unitprice <= $conditionprice;
            case '>=': return $unitprice >= $conditionprice;
            default: return false;
        }
    }
    public static function validateGiftCard($request,$quantity,$price)
    {
        $ms     = microtime(true);
        $gifcard_conf =Config('constants.gift_card_config');
        $giftcard_quantity_limit=$gifcard_conf['quantity_limit'];
        $giftcard_min_price=$gifcard_conf['min_price'];
        $giftcard_max_price=$gifcard_conf['max_price'];
        $me           = microtime(true) - $ms;
        if(!$request->has('gift_type')){
            return response()->json(['data' => null, 'error' => 'Gift Type is required', 'xtime' => $me], 400);
            die;
        }else if(!$request->has('recipient')){
            return response()->json(['data' => null, 'error' => 'Recipient Type is required', 'xtime' => $me], 400);
            die;
        }else if(!$request->has('gift_wrapping_message')){
            return response()->json(['data' => null, 'error' => 'Gift Message is required', 'xtime' => $me], 400);
            die;
        }else if(!$request->has('giftcard_for')){
            return response()->json(['data' => null, 'error' => 'Bonus for is required', 'xtime' => $me], 400);
            die;
        }else if(!$request->has('menu_json') || ($request->has('menu_json') && empty($request->post('menu_json')))){
            return response()->json(['data' => null, 'error' => 'Invalid request', 'xtime' => $me], 400);
            die;
        }else if($quantity > $giftcard_quantity_limit){
            return response()->json(['data' => null, 'error' => 'Max quantity limit for this item is'.$giftcard_quantity_limit, 'xtime' => $me], 400);
            die;
        }else if($price < $giftcard_min_price){
            return response()->json(['data' => null, 'error' => 'Min Price limit for this item is'.$giftcard_min_price, 'xtime' => $me], 400);
            die;
        }else if($price > $giftcard_min_price){
            return response()->json(['data' => null, 'error' => 'Max Price limit for this item is'.$giftcard_min_price, 'xtime' => $me], 400);
            die;
        }else{
            $menuJson = !empty($request->post('menu_json')) ? json_decode($request->post('menu_json')) : new stdClass();
            if(!isset( $menuJson->recipient_details) || (isset($menuJson->recipient_details) && empty($menuJson->recipient_details))){
                return response()->json(['data' => null, 'error' => 'Invalid request', 'xtime' => $me], 400);
                die;
            }else{
                $recipient=$request->input('recipient');
                if($recipient=='single' && count($menuJson->recipient_details)<1){
                    return response()->json(['data' => null, 'error' => 'Invalid request', 'xtime' => $me], 400);
                    die;

                }else if($recipient=='multiple' && count($menuJson->recipient_details)!=$quantity){
                    return response()->json(['data' => null, 'error' => 'Invalid request', 'xtime' => $me], 400);
                    die;

                }else{
                    return true;
                }
            }

        }

    }
    public static function UniqueCoupancode(){
        $coupan = DB::table('gift_card_coupons')
            ->selectRaw(' RAND(),LPAD(FLOOR(RAND() * 999999999.99), 9, "0") AS random_9digit', [1.0825])
            ->whereRaw('"random_9digit" NOT IN (SELECT gift_unique_code FROM gift_card_coupons)')
            ->first();
        if( $coupan ){
            return $coupan->random_9digit;
        }else{
            $coupan = DB::select(' select RAND(),LPAD(FLOOR(RAND() * 999999999.99), 9, "0")  AS random_9digit limit 1');
            return $coupan[0]->random_9digit;
        }

    }
/***************************SALTY GIFT CARD RELATED END************************************* */

     # Common Email Templates for all


    public static function getProductList($order_id, $mail_type){
        $item_list_html = '';
        $orderInfo =  UserOrder::select('user_orders.id', 'order_type', 'user_orders.status', 'delivery_date', 'delivery_time', 'user_orders.created_at', 'restaurant_id', 'is_asap_order', 'is_guest', 'user_orders.phone', 'user_id', 'product_type','is_byp', 'order_amount', 'tax', 'tip_amount', 'tip_percent', 'user_comments', 'total_amount','delivery_charge')
            ->where('user_orders.id', $order_id)
            ->with([
                'details' =>function ($query) {
                    $query->with('giftdetails');
                }
            ])
            ->first();

        if($orderInfo) {
            $item_list_html = '<table class="callout" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="callout-inner summry secondary" style="Margin:0;background:#f3f3f3;border:0;color:#0a0a0a;font-family: Montserrat;font-size:16px;font-weight:400;line-height:30px;margin:25px 0;padding: 0 25px 25px;text-align:left;width:100%;">';
            $orderInfo = $orderInfo->toArray();
            // Restaurant currency symbol
            $restData = Restaurant::find($orderInfo['restaurant_id'])->select('currency_symbol','currency_code')->first();
            if($restData) {
                $curSymbol = $restData->currency_symbol;
                $curCode = $restData->currency_code;
            } else {
                $curSymbol = config('constants.currency');
                $curCode = config('constants.currency_code');
            }            // echo "<pre>";print_r($orderInfo); die;
            switch ($orderInfo['product_type']) {
                case 'gift_card':
                    #FOR GIFT CARD ITEM It has 2 varidant with Physical card and without physical cards
                    foreach ($orderInfo['details'] as  $items) {
                        $item_list_html .=
                            '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;border-bottom:1px #eaeaea solid">
                            <tbody>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" height="20" colspan="3"></td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width: 40%;"><b>' . $items['item'] . '</b></td>
                                    <td valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;text-align:center;width: 10%;"><b>' . $items['quantity'] . '</b></td>
                                    <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 25%;"><b><span>'.$curSymbol.'</span>' . number_format($items['unit_price'], 2) . '</b></td>
                                    <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height: 1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: right;width: 25%;"><b><span>'.$curSymbol.'</span>' . number_format($items['total_item_amt'], 2) . '</b></td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top;color: #757575;font-family: Helvetica,Arial,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.5;/* font-style: italic; */">
                                    <td colspan="4" class="small-9 large-9 columns first" height="30" style="
                                        /* width: 40%; */
                                        ">
                                        <table style="
                                            width: 100%;
                                            ">
                                            <tbody>';
                        #promotion Applied DIV
                        $giftotherInfo = json_decode($items['item_other_info'], true);
                        $qty = '';
                        if($items['quantity']>1){
                            $qty = $items['quantity'].'X';
                        }

                        if ($items['giftdetails'] && isset($items['giftdetails']['0']) && $items['giftdetails']['0']['amount']) {
                            foreach ($items['giftdetails'] as $gval){
                                $bonusAmt = "";
                                if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "recipient" && $gval['recipient'] == "single") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> Your Recipient Has Earned <span style="color: gray;">'.$qty.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card </span>';
                                }
                                else if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "recipient" && $gval['recipient'] == "multiple") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> Your Recipients Have Earned <span style="color: gray;">'.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card</span>';
                                }
                                else if ($gval['card_type'] == "bonus" && $gval['bonus_for'] == "myself") {
                                    $bonusAmt = '<b style="color: #000;font-family: Helvetica,Arial,sans-serif;">Offer:</b> You\'ve Earned <span style="color: gray;">'.$curSymbol.'</span><span style="color: gray;">' .$gval['amount'] . ' Bonus Card</span>';
                                }
                            }
                            if($bonusAmt!="") {
                                $item_list_html .= '<tr>
                                                        <td colspan="2" style="
                                                            font-size: 11px;
                                                            font-style: italic;
                                                            font-weight: 600;
                                                            font-family: Helvetica,Arial,sans-serif;
                                                            ">' . $bonusAmt . '</td>
                                                    </tr>';
                            }

                        }
                        $item_list_html .=  '<tr style="
                                                    height: 5px;
                                                    ">
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                
                                                    <td colspan="2" style="
                                                        word-break: break-all;
                                                        ">
                                                        <table style="width: 100%;">
                                                        <tbody>
                                                        <tr>
                                                        <td colspan="2">
                                                            <b style="font-weight:600;color:#000;font-family: Helvetica,Arial,sans-serif;">To&nbsp;</b></td>
                                                            </tr>';
                        if($giftotherInfo['gift_type'] == 'egiftcard') {
                            foreach ($items['giftdetails'] as $key => $details) {
                                if(($details) && ($details['card_type']=="normal")) {
                                    $item_list_html .= '<tr><td style="width: 18px;"></td>
<td><p  style="margin:0;font-family: Helvetica,Arial,sans-serif;">'.$details['name'].'<br> <a href="mailto:'.$details['email'].'" target="_blank" style="color: gray !important;">('.$details['email'].')</a></p></td>
             </tr>';
                                    if($details['recipient']=="single"){
                                        break;
                                    }


                                }
                            }
                        }else {
                            foreach ($items['giftdetails'] as $key => $details) {
                                if(($details) && ($details['card_type']=="normal")) {
                                    $item_list_html .= '<tr><td style="width: 18px;"></td>
<td><p style="margin: 0;font-family: Helvetica,Arial,sans-serif;">'.$details['name'].'('.$details['email'].')</div><div>'.$details['address'].'</p></td>
             </tr>';
                                }
                            }
                        }
                        $item_list_html .=  '</tbody></table></td>
                                                </tr>';
                        if($giftotherInfo['gift_wrapping_message']!='null') {
                            $item_list_html .= '<tr style="
                                                    height: 5px;
                                                    ">
                                                    <td colspan="2"></td>
                                                </tr><tr>
                                                    <td colspan="2">
                                                    <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                    <td colspan="2">
                                                        <b style="font-weight:600;color:#000">Message&nbsp;</b></td>
                                                        </tr>
                                                        <tr><td style="width: 18px;"></td><td>
                                                            <p style="margin: 0;"> ' . $giftotherInfo['gift_wrapping_message'] . '</p>
                                                            </td></tr></tbody></table>
                                                    </td>
                                                </tr>';
                        }
                        $item_list_html .='</tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="padding:0;text-align:left;vertical-align:top">
                                    <td class="small-9 large-9 columns first" height="30" colspan="5"></td>
                                </tr>
                            </tbody>
                        </table>';
                    }
                    break;

                case 'product':
                    break;
                case 'food_item':
                $item_list_html = '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;border-bottom:1px #eaeaea solid"><tbody>
                        
                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                <td class="small-9 large-9 columns first" height="20" colspan="3"></td>
                                                </tr>
                        
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width: 40%;"><b>Gift Card</b></td>
                            
                            <td valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;text-align:center;width: 15%;"><b>1</b></td>
                            
                            
<td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px !important;"><b>$20.00</b></td>
<td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: right;width: 15%;"><b>$20.00</b></td>
                            </tr>
        <tr style="padding:0;text-align:left;vertical-align:top">
        
        <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width: 40%;"><b>Sounds like katana CD</b></td>
                            
        <td valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;text-align:center;width: 15%;"><b>1</b></td>
                            
                            
        <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px !important;"><b>$20.00</b></td>

        <td valign="middle" class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: right;width: 15%;padding-right: 12px;"><b>$20.00</b></td>

        </tr>
                            
        <tr style="padding:0;text-align:left;vertical-align:top;">
                                                
       <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 13px;font-weight:400;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:15px!important;padding-right:0!important;text-align:left;width: 40%;">Gift Wrapping</td>

       <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: center;width: 15%;">1</td>
                                                
       <td class="small-9 large-9 columns first" style="color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px !important;">$40.00</td> 
                                                
        <td class="price-clmn small-3 large-3 columns last" style="color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px;"></td>
                                                </tr>


        <tr style="padding:0;text-align:left;vertical-align:top;">
                                                
       <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 13px;font-weight:400;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:15px!important;padding-right:0!important;text-align:left;width: 40%;">Gift Wrapping</td>

       <td class="small-9 large-9 columns first" style="Margin:0 auto;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align: center;width: 15%;">1</td>
                                                
       <td class="small-9 large-9 columns first" style="color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px !important;">$40.00</td> 
                                                
        <td class="price-clmn small-3 large-3 columns last" style="color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;text-align: right;width: 15%;padding-right: 12px;"></td>
                                                </tr>                                             
                                                
                                                
                                                
                                                
                                                
                                                <tr style="padding:0;text-align:left;vertical-align:top;color: #8a8a8a;font-family: Helvetica,Arial,sans-serif;font-size: 13px;font-weight: 400;line-height: 1.5;font-style: italic;">
                                                <td class="small-9 large-9 columns first" height="30" colspan="1">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
    </td>
                                                </tr>
<tr style="padding:0;text-align:left;vertical-align:top">
                                                <td class="small-9 large-9 columns first" height="30" colspan="5"></td>
                                                </tr>
                            
                            </tbody></table>';
                    #BYP
                    #BYP = 0 and with Customisation
                    #BYP = 0 without customisation 
                    break;
                default:
                    # code...
                    break;
            }

            if($orderInfo["order_amount"] > 0) {
                $item_list_html .= '<br>
                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th height="40" valign="middle" class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 14px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%;">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;">
                                            <b>Subtotal</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b><span>'.$curSymbol.'</span>'.number_format($orderInfo["order_amount"], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            if($orderInfo["tax"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;font-size: 15px;font-weight: 400;">
                                           
                                            <b>Tax</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b> <span>'.$curSymbol.'</span>'.number_format($orderInfo["tax"],2) .'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            if($orderInfo["tip_amount"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-5 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;">
                                           
                                            <b>Tip Amount</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-7 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:150px">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:2;margin:0;padding:0;padding-bottom:0;text-align:right;border:1px #eaeaea solid;padding-right:5px;">
                                            <b>'.$orderInfo['tip_percent'].'%'.number_format($orderInfo['tip_amount'], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>
                <br>';
            }
            if($orderInfo["delivery_charge"] > 0) {
                $item_list_html .=
                    '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left;font-size: 15px;font-weight: 400;">
                                           
                                            <b>Delivery Charge</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:2.5;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th height="40" valign="middle" style="Margin:0;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size: 15px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:right;">
                                            <b> <span>'.$curSymbol.'</span>'.number_format($orderInfo["delivery_charge"],2) .'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
                </table>';
            }
            $item_list_html .=  '<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                <tbody>
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th class="small-6 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%;vertical-align:middle">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size: 16px;font-weight:400;line-height:2;margin:0;padding:0;padding-bottom:0;text-align:left;padding-top:3px;">
                                            
                                            <b>Total</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                        <th class="price-clmn small-6 large-3 columns last" valign="bottom" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%">
                            <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <th style="Margin:0;color: #000;font-family:Helvetica,Arial,sans-serif;font-size: 16px;font-weight:700;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align: right;">
                                            
                                            <b><span>'.$curSymbol.'</span>'.number_format($orderInfo['total_amount'], 2).'</b>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </th>
                    </tr>
                </tbody>
            </table>
            </th></tr></tbody></table>';
        }
        return $item_list_html;
    }

    /* 28-11-2018  BY RG*/

    public static function getRestaurantDetailsById($id)
    {
        //$user = Auth::user();
        $restList = array();
        $groupRestData = [];
        $parentName = "";
        // Manager id for Single Outlet of the parent Restautant
        if(isset($id) && is_numeric($id)) {
            //$resturantModel = new Restaurant();
            $restList = Restaurant::from('restaurants AS r2')->select('id', 'restaurant_name', 'parent_restaurant_id', 'email', 'address','street','zipcode')->where('r2.id', '=', $id)->first();
            //print_r($restList); die;
            if ($restList) {
                $restList = $restList->toArray();
                if(isset($restList['parent_restaurant_id'])) {
                    $parentData = Restaurant::select('restaurant_name','custom_from', 'support_from')
                        ->where('status', 1)
                        ->where('id', $restList['parent_restaurant_id'])
                        ->first();
                    $parentName = $parentData->restaurant_name;
                    $custom_from = $parentData->custom_from;
                    $support_from = $parentData->support_from;
                }
                $restList['restaurantParentName'] = $parentName;
                $restList['custom_from'] = $custom_from;
                $restList['support_from'] = $support_from;
                //print_r($restList); die;
                return $restList;
                //$restList = array($restList['id'], $restList['parent_restaurant_id']);
            }

        }
    }
  
    public static function getLanguageInfo($code){
     return $reservations = DB::table('languages')
            ->select(DB::raw("*"))
            ->where(['code' => $code])
            ->first();
    //DB::raw("count(id) as total_reservations")
    }   

    /* End here */

    public static function getRestaurantCurrentLocalTime($restaurant_id, $format='now') {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        if($format=='now'){
            $localDateTime = Carbon::now($timezone);
        }else{
            $localDateTime = Carbon::now($timezone)->format($format);
        }
        return $localDateTime;
    }

    public static function convertRestaurantDateTimeInUTC($restaurant_id, $userSuppliedDateTime)
    {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        return Carbon::parse($userSuppliedDateTime, $timezone)->setTimezone('UTC');
    }

    public static function changeUserCategory($user_id, $parent_restaurant_id)
    {
        $user_category = User::where('restaurant_id', $parent_restaurant_id)->where('id', $user_id)->value('category');
        if ($user_category!=config('reservation.guest_category.VIP')) {
            $reservation_count = Reservation::where('user_id', $user_id)->whereNotIn('status_id', [ReservationStatus::getCancelledStatusId(), ReservationStatus::getNoShowStatusId()])->count();
            $order_count = UserOrder::where('user_id', $user_id)->whereNotIn('status', ['cancelled', 'rejected', 'pending'])->count();
            $total_transaction_count = $reservation_count+$order_count;
            if ($total_transaction_count > 1 && $user_category==config('reservation.guest_category.New')) {
                User::where('id', $user_id)->update(['category' => config('reservation.guest_category.Repeat')]);
            }elseif ($total_transaction_count <= 1 && $user_category==config('reservation.guest_category.Repeat')){
                User::where('id', $user_id)->update(['category' => config('reservation.guest_category.New')]);
            }
        }
    }

//do not remove
    public static function convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $userSuppliedDateTime)
    {
        $timeZoneResult = Restaurant::leftJoin('cities', 'restaurants.city_id', 'cities.id')->select('city_id', 'cities.time_zone')->where(['restaurants.id' => $restaurant_id])->first();
        if($timeZoneResult && $timeZoneResult->time_zone) {
            $timezone = $timeZoneResult->time_zone;
        }else{
            $timezone = 'UTC';
        }
        return Carbon::parse($userSuppliedDateTime, 'UTC')->setTimezone($timezone);
    }


     public static function get_menu_item_imagesV2($menu_item_id) {
        $images = array();
        $menuItem = NewMenuItems::where('id', $menu_item_id)->select('id', 'name','description','images')->first();
        if($menuItem && $menuItem->images) {
            $images = json_decode($menuItem->images, true);
        }
            
        return $images;
    }

    /* Restauarant setting* */

    public static function getRestaurantSetting($restaurant_id) {
        $setting = array('has_reservation' => '0', 'has_delivery' => '0', 'has_takeout' => '0');
        if($restaurant_id) {
            $restaurant_info = Restaurant::select('id', 'current_version', 'delivery', 'takeout', 'reservation', 'reservation_type', 'delivery', 'takeout', 'reservation', 'reservation_type', 'is_merchandise_allowed', 'is_catering_allowed', 'is_contact_allowed', 'is_event_allowed', 'is_career_allowed','merchandise_takeout_allowed','merchandise_delivery_allowed','food_ordering_delivery_allowed','food_ordering_takeout_allowed','pause_service_message','is_reservation_allowed','is_food_order_allowed','is_gift_card_allowed')->where(['id' => $restaurant_id])->first();
            if($restaurant_info) {
                $setting['has_delivery'] = (string)$restaurant_info->delivery;
                $setting['has_takeout'] = (string)$restaurant_info->takeout;
                if($restaurant_info->reservation) {
                    if($restaurant_info->reservation_type == 'full') {
                        $setting['has_reservation'] = '1';
                    }else {
                       $setting['has_reservation'] = '0'; 
                    }
                }else {
                    $setting['has_reservation'] = '0';
                }
                $setting['restaurant_services'] = array(
                    'food_ordering' => array(
                        'has_food_order' => $restaurant_info->is_food_order_allowed ? 1 : 0,
                        'has_delivery' => $restaurant_info->food_ordering_delivery_allowed,
                        'has_takeout' => $restaurant_info->food_ordering_takeout_allowed,
                        'current_delivery_status' => $restaurant_info->delivery,
                        'current_takeout_status' => $restaurant_info->takeout
                    ),
                    'has_gift_card_order' => $restaurant_info->is_gift_card_allowed ? 1 : 0,
                    'merchandise' => array(
                        'has_merchandise_order' => $restaurant_info->is_merchandise_allowed ? 1 : 0,
                        'has_delivery' => $restaurant_info->merchandise_delivery_allowed,
                        'has_takeout' => $restaurant_info->merchandise_takeout_allowed,
                        'current_delivery_status' => $restaurant_info->delivery,
                        'current_takeout_status' => $restaurant_info->takeout
                    ),
                    'has_reservation' => $restaurant_info->is_reservation_allowed,
                    'current_reservation_status' => $restaurant_info->reservation == 1 && $restaurant_info->reservation_type == 'full' ? 1 : 0 ,
                    'current_reservation_enquiry_status' => $restaurant_info->reservation == 1 && $restaurant_info->reservation_type == 'enquiry' ? 1 : 0 ,
                    'has_catering' => $restaurant_info->is_catering_allowed,
                    'has_contact' => $restaurant_info->is_contact_allowed,
                    'has_event' => $restaurant_info->is_event_allowed,
                    'has_career' => $restaurant_info->is_career_allowed,
                    'pause_service_message' => empty($restaurant_info->pause_service_message) ? '':$restaurant_info->pause_service_message
                );
            }
        }
        return $setting;
    }
    /*Rahul Gupta 08-01-2019 */
    public static function getOldAddons($label, $isbyp, $result) {
        if($isbyp ==1 && $result){
            foreach($result as $key=>$data){
                 $label .= '<div class="row item_addons">      
                    <div class="col-sm-2">
                    <span class="text-capitalize font-weight-700" style="font-size: 11px; line-height: 18px;"><b>'.
                        str_replace('-', ' ',
                    str_replace('_', ' ', $key)). ':</b></span>

                    <span style="font-size: 11px; line-height: 18px;">&nbsp;'.$data.'</span></div>
                    <div class="col-md-2 col-xs-2 text-center"></div>
                </div>';
            }
        }
        if($isbyp==0 && $result) {
            $is_label_present = array();
            $total_addons = count($result);         
            foreach ($result as $key => $data) {
                if(isset($data['addons_label']) && !in_array($data['addons_label'], $is_label_present)){      
                    $is_label_present[] = $data['addons_label'];
                    $label .= "</br><b style='color: #0a0a0a;'>".$data['addons_label'].'</b></br>';
                }
                if($data['addons_name']!=""){
                    $label .= $data['addons_name'];
                    if(($key + 1) != $total_addons) {
                        $label .= ', ';
                    }
                }                                            
            }
        }
        return $label;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
     public static function getCustomerOrders($order) { 
        
        if($order['user_id']) {
            $total_user_odr_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $order['restaurant_id'])->where('user_orders.user_id', $order['user_id'])->count();
        }else {
           $total_user_odr_counts =  UserOrder::select('user_orders.id')->where('user_orders.restaurant_id', $order['restaurant_id'])->where('user_orders.email', $order['email'])->count();
        }
        
        return $total_user_odr_counts;
    }
    public static function getTimeZoneAndKpt($restaurant_id) {
        $kptAndTimeZone = [];
        if($restaurant_id) {
            $rest_data = Restaurant::find($restaurant_id)->select('currency_symbol','currency_code','kpt','kpt_calender','city_id','cities.time_zone')->leftJoin('cities','restaurants.city_id','=','cities.id')->get();
           
            if($rest_data && isset($rest_data[0])) { 
                $kptAndTimeZone['kpt'] = $rest_data[0]->kpt;
                $kptAndTimeZone['time_zone'] = $rest_data[0]->time_zone; 
                $kptAndTimeZone['kpt_calender'] = $rest_data[0]->kpt_calender;
            }
        }
        return $kptAndTimeZone;
    }

    public static  function groupingPositionsData($positions){

        $positionsgroup=[];
        $groups=[];
        /*  if(!empty($positions)&& count($positions)){
             foreach ($positions as $pos){
                 $positionsgroup[$pos['type']]=$pos;
             }
          }*/


        if(!empty($positions)&& count($positions)){

            foreach ($positions as $pos){
                $groups[]=$pos['type'];
                //  $positionsgroup[$pos['type']][]=$pos;
                // $positionsgroup[$pos['type']]=$pos;
            }
            $groups=array_unique($groups);
            foreach ($positions as $key=> $pos){
                $groupkey=array_search($pos['type'],$groups);
                $positionsgroup[$groupkey]['type']=$pos['type'];
                //$positionsgroup[$groupkey]['is_selected']=(($key==0)?1:0);
                $positionsgroup[$groupkey]['tooltip_positions'][]=$pos;
            }
            $i=0;
            $positionsgroup1=[];
            foreach ($positionsgroup as &$pos){
                $is_selectedtype = array_column($pos['tooltip_positions'], 'is_selected_type');

                $a=array_filter($is_selectedtype);
                $pos['is_selected_type']=count($a)?1:0;

                $positionsgroup1[$i]=$pos;
                $i++;
            }
            $positionsgroup=$positionsgroup1;
        }

        return $positionsgroup;
    }
    public static function mybagTotalAmount($quantity,$unitPrice,$totalMenuAmount,$order){
        $addon_array=[];
        if($order->is_pot) {

            $addondata=OrderDetailOptions::where('user_order_detail_id',$order->id)->get();

            if($addondata ){
                $addon_price = 0;
                foreach ($addondata as $addon_menu) {
                        $addonInfo      = ItemAddons::where('id',$addon_menu->id)->first(); //
                        //add more validation to it
                        if( $addonInfo ){
                            $addon_price+= $addonInfo->addon_price * $addon_menu->quantity;
                            $addon_array[]=[
                                //  'bag_id'=>,
                                //addon_group_id
                                //menu_item_id
                                'option_name'=>$addonInfo->name,
                                'option_id'=>$addonInfo->id,
                                'price'=>$addonInfo->addon_price,
                                'quantity'=>$addon_menu->quantity,
                                'total_amount'=>$addonInfo->addon_price * $addon_menu->quantity,
                                'bag_id'=>(isset($menuData['bag_id']) && !empty($menuData['bag_id']))?$menuData['bag_id']:null
                            ];
                        }

                }
                $unitPrice=$addon_price;

                $totalMenuAmount  =($quantity* $addon_price);
            }



        }else if(!empty($order->menu_json)) {
            $menuJsonDecoded = json_decode($order->menu_json, true) ?? null;

            if($menuJsonDecoded ){

                $customization_price = 0;
                if(isset($menuJsonDecoded['modifier_items'])){

                    foreach ($menuJsonDecoded['modifier_items'] as $i=>$premenu) {
                        $modifier_item_id=$premenu['id'];
                        $modifierInfo      = ItemModifier::where('id',$premenu['id'])->first();

                        if( $modifierInfo ){
                            $customization_price+= ($modifierInfo->price*$quantity);
                            $menuJsonDecoded['modifier_items'][$i]['price'] = $modifierInfo->price;
                            //$modifier_pos_id	     =$modifierInfo->pos_id;
                        }

                        // if(isset($premenu['modifier_options']) && count($premenu['modifier_options'])) {
                        //     foreach ($premenu['modifier_options'] as $cust_menu) {
                        //         $customization_price+= $cust_menu['price'] * $quantity;

                        //     }
                        // }
                    }

                }

                $totalMenuAmount  = $totalMenuAmount  + $customization_price;

            }

        }


        return ['unitPrice'=>$unitPrice,'totalMenuAmount'=>$totalMenuAmount,'addon_array'=>$addon_array];
    }
    public static  function getMybagDataByOrder($order_id,$request)
    {   #Note :- Pot case is not handled

        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
       // $order_id=$request->input('order_id');

        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $field = 'access_token';
            $bearerToken = true;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $field = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth) {
            $userOrder = UserOrder::where(['id' => $order_id, 'user_id' => $userAuth->user_id,'restaurant_id'=>$locationId])->first();

            if($userOrder){

                $orderDetails=$userOrder->details;
                $invalid_products=[];
                $order_item_count=$order_item_available_count=0;
                foreach ($orderDetails as $order){
                  //  $menu_Ids[]=$order->menu_id;


                    if($order->menu_id==0 && $order->is_byp==1){  //In case of build your pizza   -discussion is pending
                        //$invalid_products[]=['item_name'=>$order->item,'error_message'=>'Cannot reorder customi'];
                        //As it is add to cart to BYP products without any validation
                        $orderType       = $request->post('order_type') ?? $userOrder->order_type;
                        $data = [

                            'parent_restaurant_id'       => $restaurantId ?? 0,
                            'restaurant_id'         => $locationId,
                            'user_id'             => $userAuth->user_id,
                            'user_auth_id'        => $userAuth->id, //userauthid
                            'menu_id'             => 0,
                            'is_byp'              => $order->is_byp,
                            'is_pot'              => $order->is_pot,
                            'menu_json'           => $order->menu_json,
                            'special_instruction' => $order->special_instruction ?? '',
                            'quantity'            => $order->quantity,
                            'unit_price'          => $order->unit_price,
                            'total_menu_amount'   => $order->total_item_amt,
                            'size'                => $order->item_size,
                            'order_type'          => $orderType,
                            'delivery_charge'     => $userOrder->delivery_charge,
                            'image'               => $order->image,
                            'status'              => 0,
                            'item_other_info'      => $order->item_other_info,
                            'pos_id' => $userOrder->pos_id,
                            //'discount'=>$userOrder->flat_discount,
                            // As discussed with Rajeev (acc. to CMS)
                           // 'additional_charge'=>$userOrder->additional_charge, //$userOrder->service_tax,
                            // 'additional_charge_type'=>$userOrder->additional_charge_type,
                           // 'additional_charge_name'=>$userOrder->additional_charge_name
                        ];

                        // NO BAG_ID, check for Normal/BYP

                        $order_item_available_count++;
                        $result = UserMyBag::create($data);

                    }else{ //normal item
                        $menuItem = NewMenuItems::where('id', $order->menu_id)->where('status', 1)->select('id', 'name','size_price','menu_category_id')->first();
                        $unit_price=0.00;
                        if($menuItem ){

                            $menuSizePrice = $menuItem->size_price ? json_decode($menuItem->size_price, true) : '';
                            if(!empty($menuSizePrice)){
                                $sizeArrTemp   = array_column($menuSizePrice,'size');
                                $sizeIndex     = array_search($order->item_size, $sizeArrTemp);
                                $valid_price=0;
                                $isDeliveryTakeout = array_column($menuSizePrice,'available');

                                if($sizeIndex!==false &&  $request->order_type=="delivery" && $isDeliveryTakeout[$sizeIndex]['is_delivery']==0){
                                    $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available for delivery! -ITEM NOT FOUND'];
                                    continue;
                                }
                                if($sizeIndex!==false && $request->order_type=="carryout" && $isDeliveryTakeout[$sizeIndex]['is_carryout']==0){
                                    $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available for takeout! -ITEM NOT FOUND'];
                                    continue;
                                }

                                if($sizeIndex!==false &&  (isset($menuSizePrice[$sizeIndex]['price']) && $menuSizePrice[$sizeIndex]['price']) ) {

                                    $unit_price = current($menuSizePrice[$sizeIndex]['price']);

                                   /* if ($unit_price != $order->unit_price) {
                                        $invalid_products[] = ['item_name' => $order->item, 'error_message' => 'Item price changed!'];
                                        //error message here -price changed
                                    }else{*/
                                    $valid_price = 1;

                                    $cat = MenuCategories::where('id', $menuItem->menu_category_id)->where('status', 1)->select('id', 'name')->first();
                                    if (!$cat) {
                                        //$invalid_products[] = ['item_name' => $order->item, 'error_message' => 'Item not available! --CATEGORY NOT AVAILABLE'];
                                        $invalid_products[] = ['item_name' => $order->item, 'error_message' => 'Item not available!'];

                                    } else {
                                       /* $subcat = MenuSubCategories::where('id', $menuItem->menu_sub_category_id)->where('status', 1)->select('id', 'name')->first();

                                        if ($menuItem->menu_sub_category_id && !$subcat) {
                                            $invalid_products[] = ['item_name' => $order->item, 'error_message' => 'Item not available!'];

                                        } else {*/

                                        $quantity=$order->quantity;
                                        $totalMenuAmount = $quantity * ($unit_price);
                                        $amounts=self::mybagTotalAmount($quantity,$unit_price,$totalMenuAmount,$order);

                                        $unit_price=$amounts['unitPrice'];
                                        $totalMenuAmount=$amounts['totalMenuAmount'];

                                        $orderType       = $request->post('order_type') ?? $userOrder->order_type;
                                        $data = [

                                                'parent_restaurant_id' => $restaurantId ?? 0,
                                                'restaurant_id' => $locationId,
                                                'user_id' => $userAuth->user_id,
                                                'user_auth_id' => $userAuth->id,
                                                'menu_id' => $menuItem->id ?? 0,
                                                'is_byp' => $order->is_byp,
                                                'is_pot' => $order->is_pot,
                                                'menu_json' => $order->menu_json,
                                                'special_instruction' => $order->special_instruction ?? '',
                                                'quantity' => $order->quantity,
                                                'unit_price' => $unit_price, //change this
                                                'total_menu_amount' => $totalMenuAmount, //change this
                                                'size' => $order->item_size,
                                                'order_type' => $orderType,
                                                'delivery_charge' => $userOrder->delivery_charge,
                                                'image' => $order->image,
                                                'status' => 0,
                                                'item_other_info' => $order->item_other_info,
                                                'pos_id' => $userOrder->pos_id,
                                                //'discount'=>$userOrder->flat_discount,
                                                // As discussed with Rajeev (acc. to CMS)
                                                //'additional_charge' => $userOrder->additional_charge, //$userOrder->service_tax,
                                                // 'additional_charge_type'=>$userOrder->additional_charge_type,
                                               // 'additional_charge_name' => $userOrder->additional_charge_name
                                            ];
                                            $order_item_available_count++;
                                            $result = UserMyBag::create($data);
                                        /*    $addonoptions = $order->addonoptions;
                                            if ($order->is_byp == 0 && count($addonoptions) && $result) {
                                                foreach ($addonoptions as $addon) {
                                                    $addon_array = [
                                                        'modifier_json' => $addon->modifier_json,
                                                        'option_name' => $addon->option_name,
                                                        'option_id' => $addon->option_id,
                                                        'price' => $addon->price,
                                                        'quantity' => $addon->quantity,
                                                        'total_amount' => $addon->total_amount,
                                                        'bag_id' => $result->id
                                                    ];
                                                    MybagOptions::create($addon_array);

                                                }
                                            }*/

                                        //}

                                    }
                               // }
                                }else{

                                    // return response()->json(['data' => null, 'error' => 'Item size not available!'], 400)->send();die();
                                   // $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item size not available!--PRICE NOT FOUND'];
                                    $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item size not available!'];

                                }



                            }else{

                                //$invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available! --SIZE NOT FOUND'];
                                $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available!'];

                            }

                        }else{
                           // $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available! -ITEM NOT FOUND'];
                            $invalid_products[]=['item_name'=>$order->item,'error_message'=>'Item not available!'];

                            //return response()->json(['data' => null, 'error' => 'Item not available!'], 400)->send();die();
                        }


                    }


                }

                if($order_item_available_count==0){
                    return response()->json(['data' => null, 'error' => "We're sorry, none of your previously ordered menu items are available for reordering at this time."], 400,['Access-Control-Allow-Origin' => '*'])->send();die();

                }else{
                    return $invalid_products;
                }
            }else{
                return response()->json(['data' => null, 'error' => 'Order not found!'], 400,['Access-Control-Allow-Origin' => '*'])->send();die();

            }

        }else{
            return response()->json(['data' => null, 'error' => 'Invalid Request'], 400,['Access-Control-Allow-Origin' => '*'])->send();die();
        }


    }
    public static function  getAllChargesForMailer($mailKeywords,$userOrder,$curSymbol){

        $data=self::getOrderCharges($userOrder,$curSymbol);
        $mailhtml='';
        if(count($data)){
            foreach ($data as $val){
                $mailhtml.='<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%;display:'.$val['display'].';"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-9 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:75%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><p class="light-text bold" style="Margin:0;Margin-bottom:10px;color:#8a8a8a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left">'.$val['name'].'</p></th></tr></table></th><th class="price-clmn small-3 large-3 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:0;padding-left:0!important;padding-right:0!important;text-align:left;width:25%"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;padding-bottom:0;text-align:left"><p class="bold text-right" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;padding-right:10px;text-align:right"> '.$val['value'].'</p></th></tr></table></th></tr></tbody></table>';
            }
        }

        $mailKeywords['ALL_CHARGES']=$mailhtml;
        return $mailKeywords;

    }
    public static function  getOrderCharges($userOrder,$curSymbol){
        $data=[];
            if($userOrder->flat_discount==0.00){

            } else{
                 $data[]=['name'=>'Discount','value'=>'-<span>'.$curSymbol.'</span>'.number_format($userOrder->flat_discount, 2),'display'=>'table'];
            }

            if($userOrder->promocode_discount==0.00){

            } else{
                $data[]=['name'=>'Voucher Discount','value'=>'-<span>'.$curSymbol.'</span>'.number_format($userOrder->promocode_discount, 2),'display'=>'table'];
            }

            if($userOrder->additional_charge==0.00){

            } else{
                $data[]=['name'=>$userOrder->additional_charge_name,'value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->additional_charge, 2),'display'=>'table'];
            }

            if($userOrder->order_type=="delivery") {
                if ($userOrder->delivery_charge == 0.00) {
                    $data[]=['name'=>'Delivery Charge','value'=>'Free','display'=>'table'];

                } else {
                    $data[]=['name'=>'Delivery Charge','value'=>'<span>' . $curSymbol . '</span>' . number_format($userOrder->delivery_charge, 2),'display'=>'table'];
                }
            }

            if($userOrder->service_tax==0.00){

            } else{
              $data[]=['name'=>'Service Tax','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->service_tax, 2),'display'=>'table'];

            }
            if($userOrder->tax==0.00){

            } else{
                $data[]=['name'=>'Tax','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->tax, 2),'display'=>'table'];

            }

            if($userOrder->tip_amount==0.00){

            } else{
                $data[]=['name'=>'Tip Amount','value'=>'<span>'.$curSymbol.'</span>'.number_format($userOrder->tip_amount, 2),'display'=>'table'];

            }


            return $data;
   }

   public static function setEnvConfiguration($locationId=1){
	$configuration = Configuration::select(["config_key","config_value"])->where("restaurant_id",$locationId)->get()->toArray();
	if(is_array($configuration)){
		foreach($configuration as $row){
			$key = $row['config_key'];
			$_ENV[$key]=$row['config_value'];
		}
	}
   }
   public static function check_pos_token($token) {
        $response = array('is_valid' => false);
        if($token == md5("postoken@123")) {
            $response = array('is_valid' => true);
        }
        return $response;
    }	
 }






