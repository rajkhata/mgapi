<?php

namespace App\Helpers;
use DateTime;
use DateTimeZone;

class Twinjet {
    public $isConfig = false;
    public $dataArray =  null;
    public $twinjetConfig = array(
                'apiurl' => null,
                'apikey' => null 
            ); 	
    public function __construct($config = null) {
        
        if (!empty($config)) {
            $this->isConfig = true;;
            $this->twinjetConfig = array(
                'apiurl' => 'https://www.twinjet.co',
                'apikey' => $config['apikey'] 
            ); //test server
        }  
    }
    public function getOrderStatus($requestId)	{
	    $statusOrder = json_encode(array("api_token"=>$this->twinjetConfig['apikey'],"request_id"=>$requestId));
	    $maCurl = curl_init(); 
	    
	    curl_setopt($maCurl, CURLOPT_URL, $this->twinjetConfig['apiurl']. "/api/v1/status/" );            
	         curl_setopt($maCurl, CURLOPT_POST, 1); 
	    curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json'));
	    curl_setopt($maCurl, CURLOPT_POSTFIELDS, $statusOrder);             
	    curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	    $maData = curl_exec($maCurl);  
	    $response = json_decode($maData,true); 
	    $r = ['request_data'=>$requestId,'response_data'=> $response]; 
	    return $r;
    }	
    public function canceledOrderTwinjet($requestId){
     
	    $cancelOrder = json_encode(array("api_token"=>$this->twinjetConfig['apikey'],"request_id"=>$requestId));
	    $maCurl = curl_init(); 
	    
	    curl_setopt($maCurl, CURLOPT_URL, $this->twinjetConfig['apiurl']. "/api/v1/jobs/" );            
	    curl_setopt($maCurl, CURLOPT_CUSTOMREQUEST, "DELETE");      
	    curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json'));
	    curl_setopt($maCurl, CURLOPT_POSTFIELDS, $cancelOrder);             
	    curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	    $maData = curl_exec($maCurl);  
	     $response = json_decode($maData,true); 
	     $r = ['request_data'=>$requestId,'response_data'=> $response]; 
	     return $r;
	     
	     
    }
    public function editOrderTwinjet($orderData,$restaurantData,$requestId){
	 	 
		 
	  //////////////////////////////////////////
		$deliveryFromTime = date("Y-m-d H:i:s",strtotime($orderData['delivery_date']." ".$orderData['delivery_time'])- (30*60));
	        $datetime = new DateTime($orderData['delivery_date']." ".$orderData['delivery_time'],new DateTimeZone($restaurantData['time_zone']));
		//$dateObj = new \DateTime($deliveryFromTime,new \DateTimeZone($cityDetails[0]['time_zone']));
		$deliveryToTime =  $datetime->format(DateTime::ATOM);
		$datetime = new DateTime($deliveryFromTime,new DateTimeZone($restaurantData['time_zone']));
		$deliveryFromTimeObj =  $datetime->format(DateTime::ATOM);
		$customerName = $orderData["fname"]." ".$orderData["lname"];
		$order = [
		"live" => true,
		"request_id"=>$requestId, 
		"api_token" => $this->twinjetConfig['apikey'],
		"order_contact_name" => $customerName,
		"order_contact_phone" => $orderData["phone"],
		    "pick_address" => [
		        "address_name" => $restaurantData["restaurant_name"],
		        "street_address" => $restaurantData["address"],
		        "floor" => "",
		        "city" => $restaurantData["city"],
		        "state" => $restaurantData["state"],
		        "zip_code" => $restaurantData["zipcode"],
		        "contact" => "",
		        "special_instructions" => ""
		    ],
		    "deliver_address" => [
		        "address_name" => $customerName,
		        "street_address" => $orderData["address"],
		        "floor" => $orderData["address2"],
		        "city" => $orderData["city"],
		        "state" => $orderData["state_code"],
		        "zip_code" => $orderData["zipcode"],
		        "contact" => '',
		        "special_instructions" => $orderData["user_comments"],
		        "phone_number"=>$orderData["phone"]
		    ],
		"ready_time" => "",
		"deliver_from_time" => $deliveryFromTimeObj,
		"deliver_to_time" => $deliveryToTime,
		"service_id" => ($orderData["order_amount"]>=100)?1197:0,
		"order_total" => $orderData["order_amount"],
		"special_instructions"=> $orderData['user_comments'],
		"delivery_fee"=> $orderData['delivery_charge'],
		"tip" => $orderData["tip_amount"],
		"webhook_url" => "",
		];
	    $items = [];
	    foreach($orderData['item_list'] as $key => $value){ 
		if($key!="menu_json"){
		$items[] = ["quantity" => $value['quantity'], "description" => $value['item']];}
	    } 

	    $order['job_items'] = $items;
		 
	    $TwinjetData = json_encode($order);
	    $maCurl = curl_init(); 

	    curl_setopt($maCurl, CURLOPT_URL, $this->twinjetConfig['apiurl']. "/api/v1/jobs/" );            
	    curl_setopt($maCurl, CURLOPT_POST, 1);  
	    curl_setopt($maCurl, CURLOPT_CUSTOMREQUEST, "PATCH");        
	    curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json'));
	    curl_setopt($maCurl, CURLOPT_POSTFIELDS, $TwinjetData);             
	    curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	    $maData = curl_exec($maCurl); 

	    $response = json_decode($maData,true);   //Array ( [request_id] => 78BDGTXRX1 ) 
            return ["response_data"=>$response,"request_data"=>$TwinjetData];     
    }	
    public function createOrderTwinjet($orderData,$restaurantData){
	 	 
		 
	  //////////////////////////////////////////
		$deliveryFromTime = date("Y-m-d H:i:s",strtotime($orderData['delivery_date']." ".$orderData['delivery_time'])- (30*60));
	        $datetime = new DateTime($orderData['delivery_date']." ".$orderData['delivery_time'],new DateTimeZone($restaurantData['time_zone']));
		//$dateObj = new \DateTime($deliveryFromTime,new \DateTimeZone($cityDetails[0]['time_zone']));
		$deliveryToTime =  $datetime->format(DateTime::ATOM);
		$datetime = new DateTime($deliveryFromTime,new DateTimeZone($restaurantData['time_zone']));
		$deliveryFromTimeObj =  $datetime->format(DateTime::ATOM);
		$customerName = $orderData["fname"]." ".$orderData["lname"];
		$order = [
		"live" => true,
		"api_token" => $this->twinjetConfig['apikey'],
		"order_contact_name" => $customerName,
		"order_contact_phone" => $orderData["phone"],
		    "pick_address" => [
		        "address_name" => $restaurantData["restaurant_name"],
		        "street_address" => $restaurantData["address"],
		        "floor" => "",
		        "city" => $restaurantData["city"],
		        "state" => $restaurantData["state"],
		        "zip_code" => $restaurantData["zipcode"],
		        "contact" => "",
		        "special_instructions" => ""
		    ],
		    "deliver_address" => [
		        "address_name" => $customerName,
		        "street_address" => $orderData["address"],
		        "floor" => $orderData["address2"],
		        "city" => $orderData["city"],
		        "state" => $orderData["state_code"],
		        "zip_code" => $orderData["zipcode"],
		        "contact" => '',
		        "special_instructions" => $orderData["user_comments"],
		        "phone_number"=>$orderData["phone"]
		    ],
		"ready_time" => "",
		"deliver_from_time" => $deliveryFromTimeObj,
		"deliver_to_time" => $deliveryToTime,
		"service_id" => ($orderData["order_amount"]>=100)?1197:0,
		"special_instructions"=> $orderData['user_comments'],
		"order_total" => $orderData["order_amount"],
		"delivery_fee"=> $orderData['delivery_charge'],
		"tip" => $orderData["tip_amount"],
		"webhook_url" => "",
		];
	    $items = [];
	    foreach($orderData['item_list'] as $key => $value){ 
		if($key!="menu_json"){
		$items[] = ["quantity" => $value['quantity'], "description" => $value['item']];}
	    } 

	    $order['job_items'] = $items;
	    $TwinjetData = json_encode($order);
	    $maCurl = curl_init(); 

	    curl_setopt($maCurl, CURLOPT_URL, $this->twinjetConfig['apiurl']. "/api/v1/jobs/" );            
	    curl_setopt($maCurl, CURLOPT_POST, 1);      
	    curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	    curl_setopt($maCurl, CURLOPT_HTTPHEADER, array('content-type:application/json'));
	    curl_setopt($maCurl, CURLOPT_POSTFIELDS, $TwinjetData);             
	    curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	    $maData = curl_exec($maCurl); 

	    $response = json_decode($maData,true);   //Array ( [request_id] => 78BDGTXRX1 ) 
            return ["response_data"=>$response,"request_data"=>$TwinjetData];     
    }	
    public function prepareDeliveryAddressData($data,$pickupData){
	 $this->dataArray = [
                    "api_token" => $this->twinjetConfig['apikey'],
                    "pick_address" => [
			"address_name" => "",
			"floor"=> "",
                        "street_address" => $pickupData['street_address'],
                        "city" => $pickupData['city'],
                        "state" =>  $pickupData['state'],
                        "zip_code" =>  $pickupData['zip_code']
                       
                    ],
                    "deliver_address" => [
			"address_name" => "",
			"floor"=> "",
                        "street_address" => $data['street_address'],
                        "city" => $data['city'],
                        "state" =>  $data['state'],//statecode
                        "zip_code" =>  $data['zip_code']
                    ]
                ]; 
	 
    }		
    public function validateDeliveryAddress($userData,$pickupData){
	$this->prepareDeliveryAddressData($userData,$pickupData);
	$uri ='api/v1/validate/';
	return $response = $this->callPostCurl($uri); 
	//print_r($response);die;
    }
    public function callPostCurl($uri){
	  $url = $this->twinjetConfig['apiurl']. "/" . $uri;
	 
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->dataArray));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        //$result = curl_exec($ch);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return  json_decode($response);
        }  
    }				

}
