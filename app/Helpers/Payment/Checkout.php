<?php
/**
     * Created by PhpStorm.
     * User: Chitrasen Prabvakar
     * Date: 18/6/19
     * Time: 4:03 PM
     */

//namespace App\Helpers\Payment;

/*
Security scheme type: 	API Key
Header parameter name:	Authorization
*/ 

class CheckoutGateway
{
    protected $apiPay;
    protected $config;
    protected $endPointUrl;
    protected $endUrl;		
    public function __construct($config)
    {
        
        //$cecretKey = $config['SECRET_KEY'];
        $this->config    = $config;
	if(isset($config['Environment']) && $config['Environment']=="sandbox"){
		$this->endUrl    = "https://api.sandbox.checkout.com";	
	}else{
	 	$this->endUrl    = "https://api.checkout.com";
	}
    }
     

      

    /**
     * Create customer [Add a reusable payment source that can be used later to make one or more payments. Payment sources are linked to a specific customer and cannot be shared between customers.]
     * @param $data
     * @return Customer
     */
    public function createCustomer($data)
    {
	$this->endPointUrl = "/sources";
        $data = [
 	    "type"=> "sepa",
	    "reference"=> "X-080957-N34",
	    "billing_address"=> [

		    "address_line1"=> "Checkout.com",
		    "address_line2"=> "90 Tottenham Court Road",
		    "city"=> "London",
		    "state"=> "London",
		    "zip"=> "W1T 4TJ",
		    "country"=> "GB"

		],
	"phone"=>[
	    "country_code"=> "+1",
	    "number"=> "415 555 2671"

	],
	"customer"=>[
	   "id"=> "cus_y3oqhf46pyzuxjbcn2giaqnb44",
	    "email"=> "jokershere@gmail.com",
	    "name"=> "Jack Napier"

	],
	"source_data"=>[ 
  		"first_name"=> "Marcus",
		"last_name"=> "Barrilius Maximus",
		"account_iban"=> "DE25100100101234567893",
		"bic"=> "PBNKDEFFXXX",
		"billing_descriptor"=> "ExampleCompany.com",
		"mandate_type"=> "recurring"
	   ]
 	];
	$data = json_encode($data);
        $maCurl = curl_init(); 
	 curl_setopt($maCurl, CURLOPT_URL, $this->endUrl. $this->endPointUrl );            
	 curl_setopt($maCurl, CURLOPT_POST, 1); 
	 curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	 curl_setopt($maCurl, CURLOPT_HTTPHEADER, ['content-type:application/json',"Authorization"=>$this->config['SECRET_KEY']]);
	 curl_setopt($maCurl, CURLOPT_POSTFIELDS, $data);             
	 //curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	 $maData = curl_exec($maCurl);  
	 $response = json_decode($maData,true); 
	 $r = ['request_data'=>$data,'response_data'=> $response]; 

        return $r;
    }

     


    /**========================================
     * TOKEN
     ========================================*/
    /**
     * Create token for card [Request a token]
     * @param $data
     * @return Token
     */
    public function createToken($data, $type='card')
    {
        $this->endPointUrl = "/tokens";
        $data = [
 	        "type"=> "card",
		"number"=> "4543474002249996",
		"expiry_month"=> 6,
		"expiry_year"=> 2025,
		"name"=> "Bruce Wayne",
		"cvv"=> "956",
		"billing_address"=> 
		[

		    "address_line1"=> "Checkout.com",
		    "address_line2"=> "90 Tottenham Court Road",
		    "city"=> "London",
		    "state"=> "London",
		    "zip"=> "W1T 4TJ",
		    "country"=> "GB"

		],
		"phone"=>
		[

		    "country_code"=> "+1",
		    "number"=> "415 555 2671"

		]
 	];
	$data = json_encode($data);
        $maCurl = curl_init(); 
	 curl_setopt($maCurl, CURLOPT_URL, $this->endUrl. $this->endPointUrl );            
	 curl_setopt($maCurl, CURLOPT_POST, 1); 
	 curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	 curl_setopt($maCurl, CURLOPT_HTTPHEADER, ['content-type:application/json',"Authorization"=>$this->config['SECRET_KEY']]);
	 curl_setopt($maCurl, CURLOPT_POSTFIELDS, $data);             
	 //curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	 $maData = curl_exec($maCurl);  
	 $response = json_decode($maData,true); 
	 $r = ['request_data'=>$requestId,'response_data'=> $response]; 

        return $r;
    }

     /**========================================
     * PAYMENTS
     ========================================*/
    /**
     * Accept payments from cards, digital wallets and many alternative payment methods, and pay out to a variety of destinations. Just specify the source or destination of the payment using the source.type or destination.type field, along with the source- or destination-specific data.
     * @param $data
     * @return Token
     */
    public function createPayments($data, $type='card')
    {
        $this->endPointUrl = "/payments";
        $data = [  "source"=>["type"=> "card" ],
		"destination"=>[ "type"=> "card"],
		"amount"=> 6540,
		"currency"=> "USD",
		"payment_type"=> "Recurring",
		"reference"=> "ORD-5023-4E89",
		"description"=>"Set of 3 masks",
		"capture"=> true,
		"capture_on"=> "2019-07-15T14:24:39Z",
		"customer"=>[
 		    "id"=> "cus_y3oqhf46pyzuxjbcn2giaqnb44",
		    "email"=>"jokershere@gmail.com",
		    "name"=> "Jack Napier"
 		],
		"billing_descriptor"=> [
 			"name"=>"SUPERHEROES.COM",
		       "city"=>"GOTHAM"
		],
		"shipping"=> [ "address"=> [

				    "address_line1"=> "Checkout.com",
				    "address_line2"=> "90 Tottenham Court Road",
				    "city"=> "London",
				    "state"=> "London",
				    "zip"=> "W1T 4TJ",
				    "country"=>"GB"

				],
				"phone"=>[
					"country_code"=> "+1",
					"number"=> "415 555 2671"
		    		]

		],
		"3ds"=>[

		    "enabled"=> true,
		    "attempt_n3d"=> true,
		    "eci"=>"05",
		    "cryptogram"=> "AgAAAAAAAIR8CQrXcIhbQAAAAAA=",
		    "xid"=> "MDAwMDAwMDAwMDAwMDAwMzIyNzY="

		],
		"previous_payment_id"=> "pay_fun26akvvjjerahhctaq2uzhu4",
		"risk"=>[ "enabled"=> false],
		"success_url"=> "http://example.com/payments/success",
		"failure_url"=> "http://example.com/payments/fail",
		"payment_ip"=> "90.197.169.245",
		"recipient"=>[

		    "dob"=> "1985-05-15",
		    "account_number"=> "5555554444",
		    "zip"=> "W1T",
		    "last_name"=> "Jones"

		],
		"processing"=>[

		    "mid"=> "1234567"

		],
		"metadata"=>  [
			"coupon_code"=> "NY2018",
			"partner_id"=> 123989
		    ]

		];
	 $data = json_encode($data);
         $maCurl = curl_init(); 
	 curl_setopt($maCurl, CURLOPT_URL, $this->endUrl. $this->endPointUrl );            
	 curl_setopt($maCurl, CURLOPT_POST, 1); 
	 curl_setopt($maCurl, CURLOPT_RETURNTRANSFER, 1); 
	 curl_setopt($maCurl, CURLOPT_HTTPHEADER, ['content-type:application/json',"Authorization"=>$this->config['SECRET_KEY']]);
	 curl_setopt($maCurl, CURLOPT_POSTFIELDS, $data);             
	 //curl_setopt($maCurl, CURLOPT_SSL_VERIFYPEER, true);
	 $maData = curl_exec($maCurl);  
	 $response = json_decode($maData,true); 
	 $r = ['request_data'=>$data,'response_data'=> $response]; 

        return $r;
    }
   
}
$p = new CheckoutGateway(['Environment'=>'sandbox','SECRET_KEY'=>"sk_test_6699eaf3-2357-4d2e-a377-380499564ba4"]);
$r = $p->createCustomer(["a"=>'s']);
print_r($r);die;
?>
