<?php
 /* 
     * This is the PHP class file which contains methods specific to card
     * processing, to construct an empty variable and to process a payment
     * to the payment gateway, using either curl or SOAP.  These functions
     * are called from the submit_txn.php file.
     * 
     * For questions regarding this sample code or for other code samples, 
     * contact developersupport@tgatepayments.com.
    *
     * Copyright 2010, TGATE Payments.
     */  
namespace App\Helpers\Payment;
 

Class BridgePayGateway{
    public $postdata = '';
    public $userid = '';
    public $password = '';	
	
    //You can create a transaction using an $amount and the $nonceFromTheClient you received in the previous step:
    public function __construct($config) {
        $this->userid = $config['UserID'];
    	$this->password = $config['Password'];	
    }
    
    function curlProcessCreditCardPayment($data) {
		     $this->base = 'https://gatewaystage.itstgate.com/SmartPayments/transact.asmx/ProcessCreditCard';
		     $data = '';
 		     $this->postdata = array('UserName'=>$this->userid,
		      	  	'Password'=>$this->password,
		      	  	'TransType'=>$data['txn_type'],
		      	  	'CardNum'=>$data['card_number'],
		      	        'ExpDate'=>$data['expy_dt'],
			      	'MagData'=>$data['mag_data'],
			       	'NameOnCard'=>$data['name'],
			       	'Amount'=>$data['amt'],
			       	'InvNum'=>$data['invoice'],
			       	'PNRef'=>$data['pnref'],
			     	'Zip'=>$data['zip'],
			      	'Street'=>$data['street'],
			     	'CVNum'=>$data['cvnum'],
			    	'ExtData'=>$data['ext_data']);

			// concatenate this->postdata and put into variable
			while(list($key, $value) = each($this->postdata)) 
	    		{  
				$data .= $key . '=' . urlencode($value) . '&';
			}  	//end the while loop

			// Remove the last "&" from the string
			$data = substr($data, 0, -1);

			// make appropriate copy of data for error-reporting purposes
			$copy_post_data = $this->postdata;
			
			// mask the CVNum as of card industry best practices
			$copy_post_data['CVNum'] = '****';
			$copy_post_data['UserName'] = '*******';
			$copy_post_data['Password'] = '*******';
			$copy_post_data['CardNum'] = '*******' . substr($copy_post_data['CardNum'], -4);
			$copy_post_data['MagData'] = '*******';

		    	 
	    

		     	$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL,$this->base);
			curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			/* curl_setopt($ch, CURLOPT_WRITEHEADER, $fp); */
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt ($ch, CURLOPT_TIMEOUT, 300);
			curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE,FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
			/* curl_setopt($ch, CURLOPT_STDERR,$fp);  */

		  	$result = curl_exec($ch);

		  	$commError = curl_error($ch);
		  	$commInfo = @curl_getinfo($ch);
			curl_close($ch);

		    	/* fclose($fp) or die($php_errormsg);  */
			print_r($result);die;
		    return $result;

	  }

	  function SOAPTxn($txn_type,$card_number,$expy_dt,$mag_data,$name,$amt,$invoice,$pnref,$zip,$street,$cvnum,$ext_data) {

	   $this->base = 'https://gatewaystage.itstgate.com/smartPayments/transact.asmx?WSDL';
	   $this->userid = 'USERID';
	   $this->password = 'PASSWORD';

	   $this->postdata = array('UserName'=>$this->userid,
		      	  	'Password'=>$this->password,
		      	  	'TransType'=>$txn_type,
		      	  	'CardNum'=>$card_number,
		      	        'ExpDate'=>$expy_dt,
			      	'MagData'=>$mag_data,
			 	'NameOnCard'=>$name,
				'Amount'=>$amt,
				'InvNum'=>$invoice,
				'PNRef'=>$pnref,
				'Zip'=>$zip,
				'Street'=>$street,
				'CVNum'=>$cvnum,
				'ExtData'=>$ext_data);

	  /*  set up parameters to send txn to cynergydata */

	  $client = new SOAPClient($this->base, array('connection_timeout' =>60));
	 
	  /*  now POST the txn  */

	  $result = $client->ProcessCreditCard($this->postdata);
	  return $result;

	  }  /*  end of function  */	

        
	   
}
