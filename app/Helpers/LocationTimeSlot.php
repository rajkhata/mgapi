<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * Location time slot define the delivery and carryout time
 * and open the template in the editor.
 */

namespace App\Helpers;

use App\Models\RestaurantCarryoutCustomeTime;
use App\Models\RestaurantTakeoutWeekCalendar;
use App\Models\RestaurantDeliveryCustomTime;
use App\Models\RestaurantDeliveryWeekCalendar;

class LocationTimeSlot {
    /*
     * Location timeslot is used to any define stots
     * 
     */
    public static $timeSlots = array(
        '00:00',
        '00:30',
        '01:00',
        '01:30',
        '02:00',
        '02:30',
        '03:00',
        '03:30',
        '04:00',
        '04:30',
        '05:00',
        '05:30',
        '06:00',
        '06:30',
        '07:00',
        '07:30',
        '08:00',
        '08:30',
        '09:00',
        '09:30',
        '10:00',
        '10:30',
        '11:00',
        '11:30',
        '12:00',
        '12:30',
        '13:00',
        '13:30',
        '14:00',
        '14:30',
        '15:00',
        '15:30',
        '16:00',
        '16:30',
        '17:00',
        '17:30',
        '18:00',
        '18:30',
        '19:00',
        '19:30',
        '20:00',
        '20:30',
        '21:00',
        '21:30',
        '22:00',
        '22:30',
        '23:00',
        '23:30'
    );
    public static $customeDeliverySlotDetail = [];
    public static $customeCarryoutSlotDetail = [];
    public static $weekCarryoutSlotDetail = [];
    public static $weekDeliverySlotDetail = [];
    public static $isCurrentlyOpem = false;
    
    public static function getCarryoutWeekTimeSlots($restaurantId, $currentDate) {
        $finalSlot = [];
        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
        }
        $currentDay = strtolower(date('D', strtotime($currentDate)));
        $restaurantWeekDay = new RestaurantTakeoutWeekCalendar();
        $weekSlot = $restaurantWeekDay::where(['restaurant_id' => $restaurantId, 'is_dayoff' => 0, 'calendar_day' => $currentDay])->get();
        if ($weekSlot) {
            $weekSlot = $weekSlot->toArray();            
            if(isset($weekSlot[0]['slot_detail'])){
                if(self::$isCurrentlyOpem){
                    self::$weekCarryoutSlotDetail = $weekSlot[0]['slot_detail'];
                    return true;
                }
                $finalSlot = self::getDateWiseSlot($weekSlot[0]['slot_detail'], 'delivery', $currentDate);
            }
        }
        return $finalSlot;
    }

    /* This function is in use Verified by RG on 14-09-2018 */

    public static function getDeliveryWeekTimeSlots($restaurantId, $currentDate) {
        $finalSlot = false;
        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
        }
        $currentDay = strtolower(date('D', strtotime($currentDate)));

        $restaurantWeekDay = new RestaurantDeliveryWeekCalendar();
        $weekSlot = $restaurantWeekDay::where(['restaurant_id' => $restaurantId, 'is_dayoff' => 0, 'calendar_day' => $currentDay])->get();

        if ($weekSlot) {
            $weekSlot = $weekSlot->toArray();
            if(isset($weekSlot[0]['slot_detail'])){
                if(self::$isCurrentlyOpem){
                    self::$weekDeliverySlotDetail = $weekSlot[0]['slot_detail'];
                    return true;
                }
            }
        }
        return $finalSlot;
    }

    public static function getCarryoutCustomeTimeSlot($restaurantId, $currentDate) {
        $finalSlot = [];
        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
        }
        $currentDay = strtolower(date('D', strtotime($currentDate)));

        $finalSlot = self::getCarryoutCustomeSlot($restaurantId, $currentDate, $currentDay);
        return $finalSlot;
    }

    public static function getCarryoutCustomeSlot($restaurantId, $currentDate = false) {

        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
        }
        $slots = [];
        $restaurantCustomeDay = new RestaurantCarryoutCustomeTime();
        $restaurantSlots = $restaurantCustomeDay::where(['restaurant_id' => $restaurantId, 'is_dayoff' => 0])->get()->toArray();

        if ($restaurantSlots) {
            if ($restaurantSlots[0]['calendar_date']) {
                $calendarDate = json_decode($restaurantSlots[0]['calendar_date']);
                if(isset($restaurantSlots[0]['slot_detail'])){
                    if (strtotime($calendarDate->start_date) <= strtotime($currentDate) && strtotime($calendarDate->end_date) >= strtotime($currentDate)) {
                        if(self::$isCurrentlyOpem){
                            self::$customeCarryoutSlotDetail = $restaurantSlots[0]['slot_detail'];
                            return true;
                        }
                        $slots = self::getDateWiseSlot($restaurantSlots[0]['slot_detail'], 'delivery', $currentDate);
                    }
                }
            }
        }
        return $slots;
    }

    public static function getDeliveryCustomeTimeSlot($restaurantId, $currentDate) {
        $finalSlot = [];
        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
        }
        $currentDay = strtolower(date('D', strtotime($currentDate)));

        $finalSlot = self::getDeliveryCustomeSlot($restaurantId, $currentDate, $currentDay);
        return $finalSlot;
    }

    /** This function in Use verified by RG on 14-09-2018 */

    public static function getDeliveryCustomeSlot($restaurantId, $currentDate = false) {

        if (!$currentDate) {
            $currentDate = self::getRelativeCityDateTime(array("restaurant_id" => $restaurantId))->format('Y-m-d h:i');
            if(is_object($currentDate)) {
                $currentDate =  $currentDate->format('Y-m-d');
                $currentTime = $currentDate->format('H:i:00');
            }else {
                $currentDate = date('Y-m-d');
                $currentTime =  date('H:i:00');
            }
        }else {
            list($currentDate, $currentTime) = explode(" ", $currentDate);
        } 
        $slots = false;
        $restaurantWeekDay = new RestaurantDeliveryCustomTime();
      
        $restaurantSlots = $restaurantWeekDay::where('calendar_date->start_date', '=', $currentDate)->where('restaurant_id', $restaurantId)->orderBy('id', 'DESC')->get()->first();
        if($restaurantSlots) {
            $restaurantSlots = $restaurantSlots->toArray();
        }
        
        if ($restaurantSlots) {
              if ($restaurantSlots['calendar_date']) { 
                $calendarDate = json_decode($restaurantSlots['calendar_date']);
                if(isset($restaurantSlots['slot_detail']) && $restaurantSlots['slot_detail']){
                    if (strtotime($calendarDate->start_date) <= strtotime($currentDate) && strtotime($calendarDate->end_date) >= strtotime($currentDate)) {
                        if(self::$isCurrentlyOpem){ 
                            self::$customeDeliverySlotDetail = $restaurantSlots['slot_detail'];
                            return true;
                        }
                    }
                }
            }
        }
        return $slots;
    }

    public static function getDateWiseSlot($slotsDetails, $type, $currentDate) {
        $finalSlots = [];
        if ($slotsDetails) {
            $slotDetail = json_decode($slotsDetails);

            foreach ($slotDetail as $key => $openCloseTime) {

                if ($type == 'delivery') {
                    $increasdOpenTime = date("H:i", strtotime('+45 minutes', strtotime($openCloseTime->open_time)));
                    $decreseCloseTime = date("H:i", strtotime('-45 minutes', strtotime($openCloseTime->close_time)));
                } else {
                    $increasdOpenTime = date("H:i", strtotime('+30 minutes', strtotime($openCloseTime->open_time)));
                    $decreseCloseTime = date("H:i", strtotime('-30 minutes', strtotime($openCloseTime->close_time)));
                }
                $i = 0;
                foreach (self::$timeSlots as $kt => $manulatime) {
                    if (strtotime($manulatime) >= strtotime($increasdOpenTime) && strtotime($decreseCloseTime) >= strtotime($manulatime)) {
                        $finalSlots[$i]['key'] = $manulatime . " - " . date("H:i", strtotime('+30 minutes', strtotime($manulatime)));
                        $finalSlots[$i]['value'] = date("h:i A", strtotime($manulatime)) . " - " . date("h:i A", strtotime('+30 minutes', strtotime($manulatime)));
                     $i++;
                        
                    }
                   
                }
            }
        }

        return $finalSlots;
    }
    
    public static function getDeliveryTimeSlot($restaurantId,$date){
       $customeTimeslot = self::getDeliveryCustomeTimeSlot($restaurantId,$date);
       if($customeTimeslot){
           return $customeTimeslot;
       }
       $weekTimeSlot = self::getDeliveryWeekTimeSlots($restaurantId,$date);
       return $weekTimeSlot;
       
    }
    
    public static function getCarryoutTimeSlot($restaurantId,$date){
       $customeTimeslot = self::getCarryoutCustomeTimeSlot($restaurantId,$date);
       if($customeTimeslot){
           return $customeTimeslot;
       }
       $weekTimeSlot = self::getCarryoutWeekTimeSlots($restaurantId,$date);
       return $weekTimeSlot; 
        
    }
    /* This function is in use Verified by RG on 14-09-2018 */

    public static function isRestaurantCurrentlyOpenForDelivery($id,$currentDate){
        self::$isCurrentlyOpem = true;        
        self::getDeliveryCustomeSlot($id,$currentDate);  
       
        $customeForDeliveryCurrentlyOpen = self::currentlyOpen(self::$customeDeliverySlotDetail,$currentDate);  
      
        if($customeForDeliveryCurrentlyOpen){
            return true;
        }
        
        self::getDeliveryWeekTimeSlots($id,$currentDate);       
        $weekForDeliveryCurrentlyOpen = self::currentlyOpen(self::$weekDeliverySlotDetail,$currentDate);   
        if($weekForDeliveryCurrentlyOpen){
            return true;
        }
        
        return false;
       
    }
    
    public static function isRestaurantCurrentlyOpenForCarryout($id,$currentDate){
        self::getCarryoutCustomeSlot($id,$currentDate);       
        $customeForCarryoutCurrentlyOpen = self::currentlyOpen(self::$customeCarryoutSlotDetail,$currentDate); 
        if($customeForCarryoutCurrentlyOpen){
            return true;
        }
        
               
        self::getCarryoutWeekTimeSlots($id,$currentDate);       
        $weekForCarryoutCurrentlyOpen = self::currentlyOpen(self::$weekCarryoutSlotDetail,$currentDate);
        if($weekForCarryoutCurrentlyOpen){
            return true;
        }
        
        return false;
    }
    
    /* This function is in use Verified by RG on 14-09-2018 */

    public static function currentlyOpen($slotDetails,$currentDateTime){
        
        $currentDate = date("Y-m-d",strtotime($currentDateTime));
        $iscurrentOpen = false;
        if($slotDetails){
            
            $slots = json_decode($slotDetails,true); 
            foreach ($slots as $key => $slot) {
                if((isset($slot['open_time']) && isset($slot['close_time'])) && (!empty($slot['open_time']) && !empty($slot['close_time']))){

                    if(strtotime($currentDate." ".$slot['open_time'])>= strtotime($currentDateTime) && strtotime($currentDateTime) <= strtotime($currentDate." ".$slot['close_time'])){  // echo $currentDate." ".$slot['open_time'].'--'.$currentDateTime;echo "<pre>";print_r($slots);die;
                        $iscurrentOpen = true; 
                        break;
                        
                    }else{
                        $iscurrentOpen = false;
                    }
                }
            }
           return $iscurrentOpen;
            
        }
    }

}
