<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * Location working day define the delivery and carryout day
 * and open the template in the editor.
 */

namespace App\Helpers;

use App\Models\Restaurant;
use App\Models\RestaurantDeliveryWeekCalendar;
use App\Models\RestaurantTakeoutWeekCalendar;
use App\Models\RestaurantDeliveryCustomTime;
use App\Models\RestaurantCarryoutCustomeTime;
use App\Models\CmsRestaurantOperationWeekTime;
use App\Helpers\CommonFunctions;

class LocationWorkingDay {
    /*
     * Location timeslot is used to any define stots
     * 
     */

    public static $weekDay;
    public static $deliveryWeekDay;
    public static $takeoutWeekDay;
    public static $timezoneError = [];
    
     public static function getTakeoutWeekDay($restaurantId) {
        $restaurantWeekDay = new RestaurantTakeoutWeekCalendar();
        $weekDay = $restaurantWeekDay::where(['restaurant_id' => $restaurantId])->get()->toArray();
        self::weekSlotDay($weekDay);
        return $weekDay;
    }

    public static function weekSlotDay(&$weekDay) {
        foreach ($weekDay as $key => $val) {
            foreach ($val as $k => $slot) {
                if ($k == "slot_detail") {
                    if ($slot) {
                        $weekDay[$key][$k] = json_decode($slot, true);
                    } else {
                        $weekDay[$key][$k] = array();
                    }
                }
            }
        }
    }
   
    public static function getRestaurantWeekDay($restaurantId) {
        self::$deliveryWeekDay = CommonFunctions::getDeliveryWeekDay($restaurantId);
        self::$takeoutWeekDay = CommonFunctions::getTakeoutWeekDay($restaurantId);
        return true;
    }

    public static function getDeliveryCustomeDay($restaurantId, $currentDate = false) {
        $orderDay = [];
        if (!$currentDate) {
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurantId));
            if(CommonFunctions::$timezoneError){
               self::$timezoneError = CommonFunctions::$timezoneError;    
               return $orderDay;
            }
            
            $currentDate = $currentDate->format('Y-m-d h:i');
        }
        
        $restaurantWeekDay = new RestaurantDeliveryCustomTime();
        $slots = $restaurantWeekDay::where(['restaurant_id' => $restaurantId, 'is_dayoff' => 0])->get()->toArray();

        if ($slots) {
            if ($slots[0]['calendar_date']) {
                $calendarDate = json_decode($slots[0]['calendar_date']);
                if (strtotime($calendarDate->start_date) <= strtotime($currentDate) && strtotime($calendarDate->end_date) >= strtotime($currentDate)) {
                    $orderDay = self::getNextSevenDayDate($currentDate, $calendarDate->end_date);
                }
            }
        }
        return $orderDay;
    }

    public static function getNextSevenDayDate($currentDate, $calendarEndDate) {

        $days = 6;
        $sevenDateFromCurrent = [];
        for ($i = 0; $i <= $days; $i++) {
            $xmasDay = new \DateTime($currentDate . '+ ' . $i . ' day');
            if (strtotime($xmasDay->format('Y-m-d')) <= strtotime($calendarEndDate)) {
                if ($i == 0) {
                    $sevenDateFromCurrent[$i]['day'] = "Today";
                } elseif ($i == 1) {
                    $sevenDateFromCurrent[$i]['day'] = "Tomorrow";
                } else {
                    $sevenDateFromCurrent[$i]['day'] = $xmasDay->format("l");
                }

                $sevenDateFromCurrent[$i]['date'] = $xmasDay->format('Y-m-d'); // 2010-12-25
                $sevenDateFromCurrent[$i]['status'] = "open";
            }
        }
        return $sevenDateFromCurrent;
    }

    public static function getDeliveryWeekDay($restaurantId, $currentDate = false) {
        $sevenDateFromCurrent = [];
        if (!$currentDate) {
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurantId));
            if(CommonFunctions::$timezoneError){
                self::$timezoneError = CommonFunctions::$timezoneError;  
                return $sevenDateFromCurrent;
            }
            $currentDate = $currentDate->format('Y-m-d h:i');
        }
        
        $restaurantWeekDay = new RestaurantDeliveryWeekCalendar();
        $weekDay = $restaurantWeekDay::where(['restaurant_id' => $restaurantId])->get()->toArray();
        $restaurantDaysData = restaurant::where([  'id' => $restaurantId])->get()->toArray();
        if ($weekDay) {
            $xmasDay = new \DateTime($currentDate);
            $currentday = $xmasDay->format("D");


            //$days = 6;
	    if($restaurantDaysData[0]['is_preorder_accept']==1)
            $days = isset($restaurantDaysData[0]['preorder_delivery_accept_in_days'])?$restaurantDaysData[0]['preorder_delivery_accept_in_days']:6;
	    else $days = 0;	
            for ($i = 0; $i <= $days; $i++) {
                $xmasDay = new \DateTime($currentDate . '+ ' . $i . ' day');
                if ($i == 0) {
                    $sevenDateFromCurrent[$i]['day'] = "Today";
                } elseif ($i == 1) {
                    $sevenDateFromCurrent[$i]['day'] = "Tomorrow";
                } else {
                    $sevenDateFromCurrent[$i]['day'] = $xmasDay->format("l");
                }
		$sevenDateFromCurrent[$i]['day_alias'] = $xmasDay->format("l");
		$sevenDateFromCurrent[$i]['day_alias_short'] = $xmasDay->format("D");
                $sevenDateFromCurrent[$i]['date'] = $xmasDay->format('Y-m-d');
                /*foreach ($weekDay as $key => $wd) {
                    if ($wd['is_dayoff'] == 0) {
                        $sevenDateFromCurrent[$i]['status'] = "open";
                    } else {
                        $sevenDateFromCurrent[$i]['status'] = "close";
                    }
                }*///
		$sevenDateFromCurrent[$i]['status'] = "close";
		foreach ($weekDay as $key => $wd) {
			if (strtolower($wd['calendar_day'])==strtolower($sevenDateFromCurrent[$i]['day_alias_short']) && $wd['is_dayoff'] == 0) {
		                $sevenDateFromCurrent[$i]['status'] = "open";
		        } 
		}
            }
        }
        return $sevenDateFromCurrent;
    }

    public static function getDeliveryWorkingDay($restaurantId) {
        $customeDay = [];
        if(self::$timezoneError){
            return $customeDay;
        }
        $customeDay = self::getDeliveryCustomeDay($restaurantId);
        $totalCustomeDay = count($customeDay);
        $weekDay = self::getDeliveryWeekDay($restaurantId);
        $totalWeekDay = count($weekDay);
        if ($totalCustomeDay == 7) {
            return $customeDay;
        } elseif ($totalCustomeDay == 0) {
            return $weekDay;
        } elseif ($totalCustomeDay > 0 && $totalCustomeDay < 7) {
            foreach ($customeDay as $key => $val) {
                foreach ($weekDay as $keyw => $valw) {
                    if ($keyw != $key) {
                        $customeDay[$keyw] = $valw;
                    }
                }
            }
            return $customeDay;
        }
    }

    public static function getCarryoutCustomeDay($restaurantId, $currentDate = false) {
        $orderDay = [];
        if (!$currentDate) {
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurantId));
            if(CommonFunctions::$timezoneError){
               self::$timezoneError = CommonFunctions::$timezoneError;  
               return $orderDay;
            }
            $currentDate = $currentDate->format('Y-m-d h:i');
        }
        
        $restaurantWeekDay = new RestaurantCarryoutCustomeTime();
        $slots = $restaurantWeekDay::where(['restaurant_id' => $restaurantId, 'is_dayoff' => 0])->get()->toArray();

        if ($slots) {
            if ($slots[0]['calendar_date']) {
                $calendarDate = json_decode($slots[0]['calendar_date']);
                if (strtotime($calendarDate->start_date) <= strtotime($currentDate) && strtotime($calendarDate->end_date) >= strtotime($currentDate)) {
                    $orderDay = self::getNextSevenDayDate($currentDate, $calendarDate->end_date);
                }
            }
        }
        return $orderDay;
    }

    public static function getCarryoutWeekDay($restaurantId, $currentDate = false) {
        $sevenDateFromCurrent = [];
        if (!$currentDate) {
            $currentDate = CommonFunctions::getRelativeCityDateTime(array("restaurant_id" => $restaurantId));
            if(CommonFunctions::$timezoneError){
              self::$timezoneError = CommonFunctions::$timezoneError;   
              return $sevenDateFromCurrent;
            }
            $currentDate = $currentDate->format('Y-m-d h:i');
        }
        
        $restaurantWeekDay = new RestaurantTakeoutWeekCalendar();
        $weekDay = $restaurantWeekDay::where(['restaurant_id' => $restaurantId])->get()->toArray();
        $restaurantDaysData = restaurant::where([  'id' => $restaurantId])->get()->toArray();
        if ($weekDay) {
            $xmasDay = new \DateTime($currentDate);
            $currentday = $xmasDay->format("D");


            //$days = 6;
	    if($restaurantDaysData[0]['is_preorder_accept']==1)	
            $days = isset($restaurantDaysData[0]['preorder_takeout_accept_in_days'])?$restaurantDaysData[0]['preorder_takeout_accept_in_days']:6;
	    else $days = 0;
            for ($i = 0; $i <= $days; $i++) {
                $xmasDay = new \DateTime($currentDate . '+ ' . $i . ' day');
                if ($i == 0) {
                    $sevenDateFromCurrent[$i]['day'] = "Today";
                } elseif ($i == 1) {
                    $sevenDateFromCurrent[$i]['day'] = "Tomorrow";
                } else {
                    $sevenDateFromCurrent[$i]['day'] = $xmasDay->format("l");
                }
		$sevenDateFromCurrent[$i]['day_alias'] = $xmasDay->format("l");
		$sevenDateFromCurrent[$i]['day_alias_short'] = $xmasDay->format("D");
                $sevenDateFromCurrent[$i]['date'] = $xmasDay->format('Y-m-d'); // 2010-12-25 
                /*foreach ($weekDay as $key => $wd) {
                    if ($wd['is_dayoff'] == 0) {
                        $sevenDateFromCurrent[$i]['status'] = "open";
                    } else {
                        $sevenDateFromCurrent[$i]['status'] = "close";
                    }
                }*/
		$sevenDateFromCurrent[$i]['status'] = "close";
		foreach ($weekDay as $key => $wd) {
			if (strtolower($wd['calendar_day'])==strtolower($sevenDateFromCurrent[$i]['day_alias_short']) && $wd['is_dayoff'] == 0) {
		                $sevenDateFromCurrent[$i]['status'] = "open";
		        } 
		}
            }
        }
        return $sevenDateFromCurrent;
    }

    public static function getTakeoutWorkingDay($restaurantId) {
        $customeDay = [];
        if(self::$timezoneError){
            return $customeDay;
        }
        $customeDay = self::getCarryoutCustomeDay($restaurantId);
        $totalCustomeDay = count($customeDay);
        $weekDay = self::getCarryoutWeekDay($restaurantId);
        $totalWeekDay = count($weekDay);
        if ($totalCustomeDay == 7) {
            return $customeDay;
        } elseif ($totalCustomeDay == 0) {
            return $weekDay;
        } elseif ($totalCustomeDay > 0 && $totalCustomeDay < 7) {

            foreach ($customeDay as $key => $val) {
                foreach ($weekDay as $keyw => $valw) {
                    if ($keyw != $key) {
                        $customeDay[$keyw] = $valw;
                    }
                }
            }

            return $customeDay;
        }
    }

    /* RG 04-09-2018 Get operational hours * */
    public static function getOperationalDay($rest_id) {
        $ops_hours = array();
        $operational_hours = CmsRestaurantOperationWeekTime::where(['restaurant_id' => $rest_id])->get();
        if($operational_hours) {
            $operational_hours = $operational_hours->toArray();
            foreach ($operational_hours as $key => $value) {
                $times = json_decode($value['slot_detail'], true);
                switch ($value['calendar_day']) {
                    case 'sun':
                        $day = 'Sunday';
                    break;
                    case 'mon':
                        $day = 'Monday';
                    break;   
                    case 'tue':
                        $day = 'Tuesday';
                    break;   
                    case 'wed':
                        $day = 'Wednesday';
                    break;   
                    case 'thu':
                        $day = 'Thursday';
                    break;   
                    case 'fri':
                        $day = 'Friday';
                    break;
                       case 'sat':
                        $day = 'Saturday';
                    break;
                   
                }
                $ops_hours[$key] = array('day' => $day, 'is_closed' =>  $value['is_dayoff'], 'open_time' => $times['0']['open_time'], 'close_time' => $times['0']['close_time'],'slot_detail'=>$times);
				
            }
        }
        return $ops_hours;
    }

}
