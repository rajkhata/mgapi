<?php

namespace App\Helpers;
use Config;


class Netcore {
    private $version = array("v1", "apiv2");
    public $netcore;
    public function __construct($config = null) {
        
        if (!empty($config)) {
            $this->netcore = $config;
        } else {
            $netcoreConfig = Config('constants.netcore');
            $this->netcore = array(
                'apiurl' => 'http://api.netcoresmartech.com/',
                'apikey' => $netcoreConfig['apikey'],
                'lisetid' => $netcoreConfig['lisetid']
            ); //test server
        }
    }


    public function upload($postDataArr,$type='POST') {
        $apikey = $this->netcore['apikey'];
        $listId = $this->netcore['lisetid'];
        $postData = json_encode($postDataArr);
        $url = $this->netcore['apiurl'] . "/" . $this->version[1] . "?" . "type=contact&activity=add&listid=".$listId.'&apikey='.$apikey;
        $readyData = array("data" => $postData);        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($readyData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //$result = curl_exec($ch);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return  $response;
        }        
    }

    public function uploadDoublOptin($postDataArr,$type='POST') {
        $apikey = $this->netcore['apikey'];
        $listId = $this->netcore['lisetid'];
         $url =$this->netcore['apiurl'].'/?type=contact&activity=Add&data=<DATASET><CONSTANT><ApiKey>'.$apikey.'</ApiKey><RefIp></RefIp><RefWeb></RefWeb></CONSTANT><INPUT><Unique_id>email</Unique_id><AddEmail>'.$postDataArr['EMAIL'].'</AddEmail><ListMember>'.$listId.'</ListMember><DoubleOptin>1</DoubleOptin><TriggerEmail></TriggerEmail></INPUT></DATASET>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($readyData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //$result = curl_exec($ch);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return  $response;
        }
    }
    public function register($postDataArr,$type='POST') {
        $apikey = $this->netcore['apikey'];
        $listId = $this->netcore['lisetid'];
        ///$url =$this->netcore['apiurl'].'/?type=contact&activity=Add&data=<DATASET><CONSTANT><ApiKey>'.$apikey.'</ApiKey><RefIp></RefIp><RefWeb></RefWeb></CONSTANT><INPUT><Unique_id>email</Unique_id><AddEmail>'.$postDataArr['EMAIL'].'</AddEmail><ListMember>'.$listId.'</ListMember><DoubleOptin>1</DoubleOptin><TriggerEmail></TriggerEmail></INPUT></DATASET>';
        ///https://api.netcoresmartech.com/apiv2?type=contact&activity=add&listid=50&apikey=58b5dc3ec938fb539bb8d6967bacbc28
	 $url =$this->netcore['apiurl'].'?type=contact&activity=add&listid='.$listId.'&apikey='.$apikey;
	
	$postData = json_encode($postDataArr);
 	$url .= "&data=" . urlencode($postData);
  	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return  ["response"=>$response];
    }

}
