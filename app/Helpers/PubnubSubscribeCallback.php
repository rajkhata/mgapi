<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Helpers;

use PubNub\Callbacks\SubscribeCallback;

class PubnubSubscribeCallback extends SubscribeCallback {
    function status($pubnub, $status) {
        if ($this->checkUnsubscribeCondition()) {
            throw (new PubNubUnsubscribeException())->setChannels("dashboard_15");
        }
        
        // The status object returned is always related to subscribe but could contain
        // information about subscribe, heartbeat, or errors
        // use the operationType to switch on different options
        if ($status->getCategory() === PNStatusCategory::PNUnexpectedDisconnectCategory) {
            // This event happens when radio / connectivity is lost
        } else if ($status->getCategory() === PNStatusCategory::PNConnectedCategory) {
            // Connect event. You can do stuff like publish, and know you'll get it
            // Or just use the connected event to confirm you are subscribed for
            // UI / internal notifications, etc
        } else if ($status->getCategory() === PNStatusCategory::PNDecryptionErrorCategory) {
            // Handle message decryption error. Probably client configured to
            // encrypt messages and on live data feed it received plain text.
        } else if ($status->getCategory() === PNStatusCategory::PNAccessDeniedCategory) {
            // This means that PAM does allow this client to subscribe to this
            // channel and channel group configuration. This is another explicit error
        }
        
        
        
    }
 
    function message($pubnub, $message) {
    }
 
    function presence($pubnub, $presence) {
    }
 
    function checkUnsubscribeCondition() {
        // return true or false
    }
}