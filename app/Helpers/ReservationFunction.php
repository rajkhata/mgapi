<?php
namespace App\Helpers;
use App\Models\BlockTable;
use App\Models\CmsUser;
use App\Models\MailTemplate;
use App\Models\Restaurant;
use App\Models\SmsContent;
use Carbon\Carbon;


class ReservationFunction
{
    public static function getDayOfWeekByDate($date)
    {
        return strtolower(date("D", strtotime($date)));
    }

    public static function getSlotsTimeArrWithIn12HourFormat($reservation_date, $restaurant_id, $time_arr, $format_am_pm=false)
    {
        $array_of_time = array();
        $localCurrentTime = CommonFunctions::getRestaurantCurrentLocalTime($restaurant_id, 'now');
        $i=0;
        foreach ($time_arr as $key=>$time){
            $start_date_time = Carbon::parse($reservation_date . ' ' . $time)->format('Y-m-d H:i:s');
            if ($start_date_time > $localCurrentTime) {
                if($format_am_pm){
                    $array_of_time[$i]['key'] = $time;
                    $array_of_time[$i]['value'] = Carbon::parse($time)->format('h:i A');
                }
                else{
                    $array_of_time[] = $time;
                }
                $i++;
            }
        }
        return $array_of_time;
    }

    public static function rangeMonth ($datestr) {
        date_default_timezone_set (date_default_timezone_get());
        $dt = strtotime ($datestr);
        return array (
            date ('Y-m-d', strtotime ('first day of this month', $dt)),
            date ('Y-m-d', strtotime ('last day of this month', $dt))
        );
    }

    public static function rangeWeek ($datestr){
        date_default_timezone_set(date_default_timezone_get());
        $dt = strtotime($datestr);
        return array(
            date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt)),
            date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt))
        );
    }

    public static function dateDiff($start_date, $end_date){
        $start_date = new DateTime($start_date);
        $end_date = new DateTime($end_date);
        $days = $end_date->diff($start_date)->format('%a');
        return $days+1;
    }

    /*
     * $time format is H:i:s
     */
    public static function addMinutesInTime($time, $time_duration)
    {
        return date('H:i', strtotime("$time_duration minutes", strtotime($time)));
    }

    public static function getDateTime($date, $time)
    {
        return date('Y-m-d H:i:s', strtotime("$date $time"));
    }

    public static function getSlotsFromTimeRange($start_time, $end_time, $slot_interval, $format_am_pm=false)
    {
        $array_of_time = array();
        $start_time = strtotime($start_time); //change to strtotime
        $end_time = strtotime($end_time); //change to strtotime
        $add_mins = $slot_interval * 60;
        while ($start_time < $end_time) // loop between time
        {
            if($format_am_pm){
                $array_of_time[] = date("h:i A", $start_time);
            }else{
                $array_of_time[] = date("H:i", $start_time);
            }
            $start_time += $add_mins; // to check endtie=me
        }
        return $array_of_time;
    }

    public static function getSlotsRangeFromTimeRange($start_time, $end_time, $slot_interval)
    {
        $array_of_time = array();
        $start_time = strtotime($start_time); //change to strtotime
        $end_time = strtotime($end_time); //change to strtotime
        $add_mins = $slot_interval * 60;
        while ($start_time < $end_time) // loop between time
        {
            $start_time_range = date("H:i", $start_time);
            $start_time += $add_mins; // to check endtie=me
            $array_of_time[] = $start_time_range.'-'.date("H:i", $start_time);
        }
        return $array_of_time;
    }

    public static function getSlotWithIntevals($start_time, $end_time, $slot_interval)
    {
        $array_of_time = array();
        $start_time = strtotime($start_time); //change to strtotime
        $end_time = strtotime($end_time); //change to strtotime
        $add_mins = $slot_interval * 60;

        $array_of_time[]=date("H:i", $start_time);
        while ($start_time < $end_time) // loop between time
        {
            $start_time_range = date("H:i", $start_time);
            $start_time += $add_mins; // to check endtie=me
            $array_of_time[] = date("H:i", $start_time);
        }
        array_pop($array_of_time);
        return $array_of_time;
    }

    public static function timeBefore($time, $time_blocks) {
        $time = strtotime($time);
        $closest_before = NULL;
        foreach ($time_blocks as $t=>$time_var) {
            $time_var['time'] = strtotime($time_var['time']);
            if ($time_var['time'] <= $time) {
                if (!$closest_before) {
                    $closest_before = $time_var['time'];
                } else {
                    if ($time_var['time'] > $closest_before) {
                        $closest_before = $time_var['time'];
                    }
                }
            }
        }
        $closest_before = ($closest_before) ? date('H:i', $closest_before): '';
        return $closest_before;
    }

    public static function timeAfter($time, $time_blocks) {
        $time = strtotime($time);
        $closest_after = NULL;
        foreach ($time_blocks as $t=>$time_var) {
            $time_var['time'] = strtotime($time_var['time']);
            if ($time_var['time'] > $time) {
                if (!$closest_after) {
                    $closest_after = $time_var['time'];
                } else {
                    if ($time_var['time'] < $closest_after) {
                        $closest_after = $time_var['time'];
                    }
                }
            }
        }
        $closest_after = ($closest_after) ? date('H:i', $closest_after): '';
        return $closest_after;
    }

    public static function getTableIDAndCountArr($table_availability)
    {
        $tableCountArr = array();
        $table_availability = json_decode($table_availability, true);
        foreach ($table_availability as $table){
            if($table['table_count']>0){
                $tableCountArr[$table['table_type_id']] = $table['table_count'];
            }
        }
        return $tableCountArr;
    }

    public static function isInputTimeExistInRange($start_end_time_range, $input_time)
    {
        $start_end_time_range = explode('-', $start_end_time_range);
        $start_time = strtotime($start_end_time_range[0]);
        $end_time = strtotime($start_end_time_range[1]);
        $input_time = strtotime($input_time);
        if ($start_time - $input_time <= 0 && $input_time - $end_time < 0)
        {
            return true;
        }
        return false;
    }

    /*
     * this function check time fall in time range
     * $start_end_time_range = 08:00-10:00
     * $input_time = 09:00
     * return true
     */
    public static function isInputTimeExistInTimeRange($start_end_time_range, $input_time)
    {
        $start_end_time_range = explode('-', $start_end_time_range);
        $start_time = strtotime($start_end_time_range[0]);
        $end_time = strtotime($start_end_time_range[1]);
        $input_time = strtotime($input_time);
        if ($start_time - $input_time <= 0 && $input_time - $end_time <= 0)
        {
            return true;
        }
        return false;
    }

    public static function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while( $current <= $last ) {

            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public static function isStartTimeEndTimeExistInTimeRangeArr($start_end_time_range_arr, $start_time, $end_time)
    {
        $start_end_time_range_arr = array_unique($start_end_time_range_arr);
        foreach ($start_end_time_range_arr as $start_end_time_range_key=>$start_end_time_range){
            if(self::isInputTimeExistInTimeRange($start_end_time_range, $start_time) && self::isInputTimeExistInTimeRange($start_end_time_range, $end_time)){
                return true;
            }
        }
        return false;
    }



    public static function generateReservationReceipt() {
        $timestamp = date('mdhis');
        $keys = rand(0, 9);
        $randString = 'M' . $timestamp . $keys;
        return $randString;
    }


    public static function checkEndTimeExceedFromRange($start_end_time_range_arr, $start_time, $end_time)
    {
        $end_time_exceed_flag = true;
        $start_time_exist = false;
        $start_end_time_range_arr = array_unique($start_end_time_range_arr);
        foreach ($start_end_time_range_arr as $start_end_time_range_key => $start_end_time_range) {
            if (self::isInputTimeExistInTimeRange($start_end_time_range, $start_time)) {
                $start_time_exist = true;
                if (self::isInputTimeExistInTimeRange($start_end_time_range, $end_time)) {
                    $end_time_exceed_flag = false;
                }
            }
        }
        if($start_time_exist){
            return $end_time_exceed_flag;
        }
        return false;
    }

    public static function getSpecialOccasionKeyPair($all_special_occasion)
    {
        $special_occasion = array();
        $i=0;
        foreach ($all_special_occasion as $key=>$value){
            $special_occasion[$i]['key'] = $key;
            $special_occasion[$i]['value'] = $value;
            $i++;
        }
        return $special_occasion;
    }

    public static function sendReservationNotification($parent_restaurant_id, $reservation, $by)
    {
        $params = [];
        $restaurant_id=$reservation->restaurant_id;
        $restaurant_manager=CmsUser::where('restaurant_id',$restaurant_id)->first();
        $notification_setting = ReservationFunction::getNotificationSetting($restaurant_id);
        \Log::info($notification_setting);
        \Log::info('Restaurant manager email=>'.$restaurant_manager->email);
        if($by=='Customer'){
            \Log::info("New Reservation by".$reservation->email);
            $start_date_time=CommonFunctions::convertUTCDateTimeInRestaurantLocalDateTime($restaurant_id, $reservation->start_time);
            $data=[
                'reservation_id'=>$reservation->receipt_no,
                'name'=>$reservation->fname.' '.$reservation->lname,
                'start_date_time'=>$start_date_time->format('D, M d, Y'),
                'fname'=>$reservation->fname,
                'lname'=>$reservation->lname,
                'email'=>$reservation->email,
                'phone'=>$reservation->mobile,
                'party_size'=>$reservation->reserved_seat,
                'time'=>$start_date_time->format('h:i A'),
                'date'=>$start_date_time->format('d-m-Y'),
                'occasion'=>$reservation->special_occasion,
                'special_instruction'=>$reservation->user_instruction,
                'restaurant_name'=>$reservation->restaurant_name
            ];
            if($notification_setting['cust_resv_sms_flag']){
                $params['customer']['sms'] = [
                    'template'=>'reservation_customer',
                    'data'=>$data,
                    'mobile'=>$reservation->mobile
                ];
            }
            if($notification_setting['cust_resv_email_flag']){
                $params['customer']['mail'] = [
                    'template'=>'reservation_customer',
                    'email'=>$reservation->email,
                    'data'=>$data
                ];
            }
            //\Log::info($notification_setting);
            if($notification_setting['manager_resv_incoming_email_flag']){
                $emails = !empty($notification_setting['manager_resv_emails'])?str_replace(array('[', ']', '"'), '', ($notification_setting['manager_resv_emails'])):'';
                $emails = !empty($emails)?$emails:$restaurant_manager->email;
                $params['manager']['mail'] = [
                    'template'=>'reservation_customer_notify_manager',
                    'email'=>$emails,
                    'data'=>$data
                ];
            }
            if(!empty($params) && count($params)>0){
                self::notifiy($params,$restaurant_id, $parent_restaurant_id);
            }
        }
    }

    public static function getNotificationSetting($restaurant_id)
    {
        $notification_setting['cust_resv_sms_flag'] = true;
        $notification_setting['cust_resv_email_flag'] = true;
        $notification_setting['manager_resv_incoming_email_flag'] = true;
        $notification_setting['manager_resv_modified_email_flag'] = true;
        $notification_setting['manager_resv_cancelled_email_flag'] = true;
        $notification_setting['manager_resv_emails'] = null;
        $reservation_settings = Restaurant::where('id', $restaurant_id)->value('reservation_settings');
        $reservation_settings = json_decode($reservation_settings, true);
        if (!empty($reservation_settings) && !is_array($reservation_settings)) {
            $reservation_settings = json_decode($reservation_settings, true);
        }
        if (empty($reservation_settings) || empty($reservation_settings['notification'])) {
            return $notification_setting;
        }
        $notification_setting['cust_resv_sms_flag'] = $reservation_settings['notification']['guest_sms_on_reservation']==0?false:true;

        $notification_setting['cust_resv_email_flag'] = $reservation_settings['notification']['guest_email_on_reservation']==0?false:true;

        $notification_setting['manager_resv_incoming_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_incoming']==0?false:true;

        $notification_setting['manager_resv_modified_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_modified']==0?false:true;

        $notification_setting['manager_resv_cancelled_email_flag'] = $reservation_settings['notification']['restaurant_receive_notification_cancelled']==0?false:true;

        $notification_setting['manager_resv_emails'] = !empty($reservation_settings['notification']['restaurant_receive_notification_emails'])?$reservation_settings['notification']['restaurant_receive_notification_emails']:null;

        return $notification_setting;
    }

    public static function notifiy(array $params,$restaurant_id, $parent_restaurant_id){
        foreach($params as $user_type=>$user){
            foreach($user as $key=> $data){
                \Log::info($key);
                if($key=='sms'){
                    \Log::info('hello');
                    \Log::info($data);
                    if(isset($data['template']) && !empty($data['template']) && !empty($data['mobile']) && !empty($data['data'])){
                        \Log::info('hello1');
                        self::sendSms($data['template'],$restaurant_id,['to'=>$data['mobile']],$data['data'], null, $parent_restaurant_id, $user_type);
                    }
                    if(isset($data['template_data']) && !empty($data['template_data']) && !empty($data['phone']) && !empty($data['data'])){
                        \Log::info('hello2');
                        self::sendSms(null,$restaurant_id,['to'=>$data['phone']],$data['data'],$data['template_data'], $parent_restaurant_id, $user_type);
                    }

                }elseif($key=='mail'){
                    //\Log::info($data);
                    if(!empty($data['template']) && !empty($data['email']) && !empty($data['data'])){

                        self::sendMail($data['template'],$restaurant_id,['to'=>$data['email']],$data['data'], $parent_restaurant_id, $user_type);
                    }
                }
            }
        }

    }
    public static function sendSms($template_name,$restaurant_id,array $config,array $data,$template_data=null, $parent_restaurant_id,$user_type){

        if(count($config) && isset($config['to']) && !empty($config['to'])  &&  count($data) && !empty($restaurant_id)){

            if(!empty($template_name)){
                $finaltemplate=self::getTemplate('sms',$template_name,$restaurant_id,$data,$parent_restaurant_id,$user_type);
                if($finaltemplate!=false){
                    if(isset($finaltemplate['content']) && !empty($finaltemplate['content'])){
                        $text = json_decode($finaltemplate['content'], true);
                        if(isset($text['content']) && !empty($text['content'])){
                            CommonFunctions::sendSms(array('message' => $text['content'], 'user_mob_no' => $config['to']));
                        }
                    }
                }

            }	else if(!empty($template_data)){
                \Log::info($template_data);
                CommonFunctions::sendSms(array('message' => $template_data, 'user_mob_no' => $config['to']));

            }


        }

    }
    public static function sendMail($template_name,$restaurant_id,array $config,array $data, $parent_restaurant_id, $user_type){
        \Log::info('Senmailcalled');
        \Log::info($data);
        \Log::info($config);

        if(count($config) && isset($config['to']) && !empty($config['to'])  &&  count($data) && !empty($template_name) && !empty($restaurant_id)){
            \Log::info(' inner sendMail');

            $finaltemplate=self::getTemplate('email',$template_name,$restaurant_id,$data, $parent_restaurant_id,$user_type);
            //\Log::info($template_name);
            if($finaltemplate!=false){

                /*$mail=Mail::to($config['to']);

                if(isset($config['cc']) && !empty($config['cc'])){
                    $mail=$mail->cc($config['cc']);
                }
                if(isset($config['bcc']) && !empty($config['bcc'])){
                    $mail=$mail->bcc($config['bcc']);
                }*/
                //\Log::info($config);
                //\Log::info($finaltemplate);
                if (!empty($config['to']) && strpos($config['to'], ',') !== false) {
                  $config['to'] = explode(',', $config['to']);
                }
                //\Log::info($config);
                $mailData=[
                    'body'=>$finaltemplate['content'],
                    'subject'=>$finaltemplate['subject'],
                    'receiver_email'=> $config['to'],
                    'receiver_name' => $data['fname']
                ];
                CommonFunctions::sendMail($mailData);
            }

        }else{



        }

    }


    public static function getEmailByTemplateId($template_name, $parent_restaurant_id){

        return MailTemplate::select(['id','subject','content'])->where('restaurant_id', $parent_restaurant_id)->where('template_name',$template_name)->where('status',1)->first();
    }

    public static function getSmsByTemplateId($sms_name, $parent_restaurant_id){

        return SmsContent::select(['id','content'])->where('sms_name',$sms_name)->where('restaurant_id', $parent_restaurant_id)->where('status',1)->first();
    }



    public static function getFinalTemplate(array $data,$template_content){

        if($template_content){

            $content=$template_content;
            preg_match_all("/\[(.*?)\]/",$content, $options_array);

            if(isset($options_array[1]) && count($options_array[1])) {
                foreach($options_array[1] as $key){

                    if (array_key_exists(strtolower($key),$data))
                    {

                        $content=preg_replace('/\['.strtolower($key).'\]/',$data[strtolower($key)],$content);

                    }elseif (array_key_exists(strtoupper($key),$data)){

                        $content=preg_replace('/\['.strtoupper($key).'\]/', $data[strtoupper($key)],$content);
                    }
                }

                return $content;


            }

        }
    }

    public static function getTemplate($type = 'email', $template_name, $restaurant_id, array $data, $parent_restaurant_id, $user_type)
    {
        $resp = [];
        if (!empty($restaurant_id)) {

            $restaurant = Restaurant::select(['restaurant_name', 'address', 'street', 'phone', 'lat', 'lng'])->where('id', $restaurant_id)->first();

            $data['restaurant_name'] = $restaurant->restaurant_name;
            $data['restaurant_address'] = $restaurant->address;
            $data['restaurant_address_street'] = $restaurant->street;
            $data['restaurant_phone'] = $restaurant->phone;
            $data['restaurant_latitude'] = $restaurant->lat;
            $data['restaurant_longitude'] = $restaurant->lng;

        }

        if ($type == 'email' && !empty($restaurant_id)) {
            if ($user_type == 'customer') {
                //header
                $header_template = self::getEmailByTemplateId('header_layout', $parent_restaurant_id);
                //footer
                $footer_template = self::getEmailByTemplateId('footer_layout', $parent_restaurant_id);
            } else {
                //header
                $header_template = self::getEmailByTemplateId('header_layout_restaurant', $parent_restaurant_id);
                //footer
                $footer_template = self::getEmailByTemplateId('footer_layout_restaurant', $parent_restaurant_id);
            }

            $template = self::getEmailByTemplateId($template_name, $parent_restaurant_id);
            $resp['subject'] = $template->subject;
            $final_template = $header_template->content . $template->content . $footer_template->content;
        } else if ($type == 'sms') {

            $final_template = $template=self::getSmsByTemplateId($template_name, $parent_restaurant_id);

        }

        if ($template && !empty($restaurant_id) && count($data)) {

            $content_mail = self::getFinalTemplate($data, $final_template);

            $resp['content'] = $content_mail;
            return $resp;
        }

        return false;

    }

    public static function getBlockedTableArr($restaurant_id, $floor_id=null, $table_id=null, $start_date_time, $end_date_time)
    {
        $blocked_table = [];
        $block_table = BlockTable::where('restaurant_id', $restaurant_id);
        if($floor_id){
            $block_table = $block_table->where('floor_id', $floor_id);
        }
        if($table_id){
            $block_table = $block_table->whereRaw("FIND_IN_SET('".$table_id."', table_id)");
        }
        $block_table = $block_table->where('start_time', '<', $end_date_time)
            ->where('end_time', '>', $start_date_time)->get()->toArray();
        foreach ($block_table as $key=>$table){
            if (strpos($table['table_id'], ',')) {
                $blocked_table = array_merge($blocked_table, explode(',', $table['table_id']));
            }else{
                array_push($blocked_table, $table['table_id']);
            }
        }
        $blocked_table = count($blocked_table)==0?$blocked_table:array_unique($blocked_table);
        return $blocked_table;
    }

}