<?php

namespace App\Helpers;
use App\Models\NewMenuItems;
use App\Models\MenuCategories;

use App\Models\Restaurant;
use App\Models\DeliveryConfig;
use App\Models\DeliveryRates;
use App\Models\PromotionCoupon;
use App\Models\UserAuth;
use App\Models\UserMyBag;
use Config;
use App\Helpers\CommonFunctions;
use DB;
//use App\Helpers\ApiPayment;
use App\Models\UserOrder;
class Delivery
{
    private $delivery_service_id = 0; //custom delivery provider
    public  static  $static_pizza_coupon='freepizza15';
    public  static  $static_pizza_coupon_free='nkdfree';

    public  static  $static_pizza_coupon2='freepizza';
    public  static  $static_pizza_coupon_min_spent=15;
    public  static  $static_pizza_coupon_delivery=6;
    public  static  $medium_two_pizza=1;
    public  static  $large_two_pizza=1;

    public  static  $medium_two_pizza_quant=2;
    public  static  $large_two_pizza_quant=2;
    //public  static  $medium_two_pizza_discount=16.50;
    //public  static  $large_two_pizza_discount=20.00;
    /******************Deals discount updated on 6th November 2019 *******************************/
    //public  static  $medium_two_pizza_discount=20.00;
    //public  static  $large_two_pizza_discount=25.00;
    public  static  $medium_two_pizza_discount=19.00; //7th nov 2019
    public  static  $large_two_pizza_discount=22.00;  //7th nov 2019
    public  static  $special_deal_delivery_charge=0;

    public  static  $glutenfree_two_pizza=0;
    public  static  $glutenfree_two_pizza_quant=2;
    public  static  $glutenfree_two_pizza_discount=22.00;  //7th nov 2019
    
    #####-Deal: 50% off on all Signature Pizzas.-#####
    /*
     * 1. Applicable for Takeout only.
       2. Applicable on all Pizza sizes under Signature Pizza
       3. No other deal is applicable if this deal is applied.
       4. Cannot be used in conjunction with NKDFREE or Freepizza15 coupon code.
       5. Applicable for registered as well as unregistered users
       6. No minimum amount is required to avail this deal.
     */
    public static $deal_on_takeout_signature_pizzas = 50; 
    public static $deal_on_takeout_only_fifty_percent = 1; 
    #####-End Deal : 50% off-#####

    /**********************************************************************************************/

    //public  static  $medium_two_pizza_discount=0;
    //public  static  $large_two_pizza_discount=0;

    //ask for promotion limit

    public  static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function pizzaTwoDiscount($pizza,$pizzanewamount){
        $different_price_pizza=[];
        $pizza_discounted=0;
        foreach ($pizza as $p){
            $price=$p['price'];
            $quantity=$p['quantity'];
            /* $odd_check=($quantity%2);

           if($odd_check!=0){
                $different_price_pizza[]=$price;
            }
           $pizza_discounted+=((floor(($quantity/2)) * (2*$price))-(floor(($quantity/2)) * $pizzanewamount));*/
            for($i=0;$i<$quantity;$i++){

                $different_price_pizza[]=$price;

            }

        }

        sort($different_price_pizza);

        $total=0;

        if(count($different_price_pizza)%2!=0){
            array_pop($different_price_pizza);

        }
       // return response()->json($different_price_pizza)->send();

        $i=0;
       foreach($different_price_pizza  as $pizza){
           if(isset($different_price_pizza[$i]) && isset($different_price_pizza[$i+1])){
               $total+=(($different_price_pizza[$i]+$different_price_pizza[$i+1])-$pizzanewamount);
               $i=$i+2;
           }

       }

       return  $pizza_discounted=$pizza_discounted+$total;
    }



    public static  function getPizzaItemCheckPromotions($locationId,$userAuthId){ //static promotion logic for pizzas


        $userBagItems = UserMyBag::where('menu_id', '!=' , 0)
            ->where(function($q) {
                $q->where('size', 'like', 'Medium%')
                ->orWhere('size', 'like', 'Large%');
                //->orWhere('size', 'like', '%Gluten-Free%');
        })->where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->orderBy('unit_price', 'ASC')->get(['id','unit_price','menu_id','quantity','size']);

        $medium_pizza_quant=$large_pizza_quant=$gluttenfree_pizza_quant=0;
        $medium_pizza_discounted=$large_pizza_discounted=$glutenfree_pizza_discounted=0;
        if ($userBagItems) {
            $medium_pizza=$large_pizza=$glutenfree_pizza=[];
            foreach ($userBagItems as $userMyBag) {

                $menuItemDetail = NewMenuItems::select('id', 'menu_category_id')->find($userMyBag->menu_id);

                if($menuItemDetail){

                    $menu_category_id=$menuItemDetail->menu_category_id;
                    $menuCat = MenuCategories::where(['id' =>$menu_category_id,'status' => 1])->first(['id','status','slug']);
                    if($menuCat->slug=='pizza'){
                        if(self::startsWith($userMyBag->size,'Medium')){
                            $medium_pizza_quant+=$userMyBag->quantity;

                            $medium_pizza[]=['quantity'=>$userMyBag->quantity,'price'=>$userMyBag->unit_price];
                        }
                        if(self::startsWith($userMyBag->size,'Large')){
                            $large_pizza_quant+=$userMyBag->quantity;
                            $large_pizza[]=['quantity'=>$userMyBag->quantity,'price'=>$userMyBag->unit_price];

                        }
                       /* if(strpos($userMyBag->size, 'Gluten-Free') !== false){
                            $gluttenfree_pizza_quant+=$userMyBag->quantity;
                            $glutenfree_pizza[]=['quantity'=>$userMyBag->quantity,'price'=>$userMyBag->unit_price];

                        }*/
                    }
                }

            }
            if($medium_pizza_quant>=self::$medium_two_pizza_quant && self::$medium_two_pizza==1 ){
                $medium_pizza_discounted=self::pizzaTwoDiscount($medium_pizza,self::$medium_two_pizza_discount);
            }

            if($large_pizza_quant>=self::$large_two_pizza_quant && self::$large_two_pizza==1){
                $large_pizza_discounted=self::pizzaTwoDiscount($large_pizza,self::$large_two_pizza_discount);
            }
            /*if($gluttenfree_pizza_quant>=self::$glutenfree_two_pizza_quant && self::$glutenfree_two_pizza==1){
                $glutenfree_pizza_discounted=self::pizzaTwoDiscount($glutenfree_pizza,self::$glutenfree_two_pizza_discount);
            }*/
        }
        return ($medium_pizza_discounted+$large_pizza_discounted+$glutenfree_pizza_discounted);
        //return ['medium_pizza_discounted'=>$medium_pizza_discounted,'large_pizza_discounted'=>$large_pizza_discounted];
    }


    public static function getRestaurantPromotion($restaurant_id,$user_group_id=NULL,$request) {
        $orderType = $request->input('order_type') ?? 'delivery';
        $promos = DB::table('restaurant_promotions')
            ->where(['restaurant_id' => $restaurant_id,'status'=>1,'delivery_type'=>$orderType])
            ->whereRaw("find_in_set('".$user_group_id."',user_group_id)")
            ->first();
        if(!$promos){
            $promos = DB::table('restaurant_promotions')
                ->where(['restaurant_id' => $restaurant_id,'status'=>1,'delivery_type'=>$orderType])
                ->whereNull('user_group_id')
                ->first();
        }

        return $promos;
    }
    public static   function getDiscount($rest_provider_data,$totalMenuAmount,$request,$pizza_amount=0){
        $user_group_id = $request->header('User-group');
        $locationId = $request->header('X-location');

        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $field = 'access_token';
            $bearerToken = true;
            $user_group_id=$user_group_id??1;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $field = 'guest_token';
            $bearerToken = false;
            $user_group_id=$user_group_id??3;
        }
        $userAuth = UserAuth::where([$field => $token])->first();

        $userAuthId = $userAuth->id;
        $special_deal_delivery=0;
        $special_deal_carryout=0;
        $percent_discount = 0;
        $flat_discount = 0;
        
        if(self::getPizzaItemForDeal($locationId, $userAuthId) && $pizza_amount > 0 && self :: $deal_on_takeout_only_fifty_percent){
             $special_deal_carryout=1;
            
             $percent_discount = (float)number_format(($pizza_amount * (self::$deal_on_takeout_signature_pizzas / 100)),2);
           
        }elseif(self::$medium_two_pizza==1 || self::$large_two_pizza==1 || self::$glutenfree_two_pizza==1){
            $flat_discount=self::getPizzaItemCheckPromotions($locationId,$userAuthId);
            if(!empty($flat_discount)){
                $special_deal_delivery=1;
            }

        }else {

            $restaurantPromotion = self::getRestaurantPromotion($rest_provider_data->id, $user_group_id, $request);
            
            /*********************************FLAT DISCOUNT*********************************************/
            if ($restaurantPromotion && $rest_provider_data && !empty($restaurantPromotion->discount) && CommonFunctions::productPromoCondition($totalMenuAmount, $restaurantPromotion->condition_amount, $restaurantPromotion->condition)) {
                // $cur_date=date('Y-m-d H:i:s');
                $cur_date = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $rest_provider_data->id, 'datetime' => date('Y-m-d H:i:s')));
                $flat_discount_start_date = date('Y-m-d H:i:s', strtotime($restaurantPromotion->start_date));
                $flat_discount_end_date = date('Y-m-d H:i:s', strtotime($restaurantPromotion->end_date));

                if (($flat_discount_start_date <= $cur_date) && ($flat_discount_end_date >= $cur_date)) {

                    if ($restaurantPromotion->amount_or_percent == 1) {
                        $flat_discount = (($restaurantPromotion->discount / 100) * $totalMenuAmount);

                    } else {
                        $flat_discount = $restaurantPromotion->discount;
                    }
                }
            }
        }
        if($flat_discount>$totalMenuAmount){
            $flat_discount=$totalMenuAmount;
        }
        //return (float)number_format($flat_discount, 2);
        return [
            'special_deal_delivery'=>($special_deal_carryout)?$special_deal_carryout:$special_deal_delivery,
            'flat_discount'=>($percent_discount)?$percent_discount:(float)number_format($flat_discount, 2)                    
            ];

        /*********************************FLAT DISCOUNT*********************************************/
    }
    public static   function getDiscountOldVersion($rest_provider_data,$totalMenuAmount){

        $flat_discount=0;
        /*********************************FLAT DISCOUNT*********************************************/
        if ($rest_provider_data && !empty($rest_provider_data->flat_discount) && $rest_provider_data->flat_discount_min<=$totalMenuAmount) {
            // $cur_date=date('Y-m-d H:i:s');
            $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" => $rest_provider_data->id, 'datetime' =>date('Y-m-d H:i:s')));
             $flat_discount_start_date=date('Y-m-d H:i:s',strtotime($rest_provider_data->flat_discount_start_date));
             $flat_discount_end_date=date('Y-m-d H:i:s',strtotime($rest_provider_data->flat_discount_end_date));

            if(($flat_discount_start_date<=$cur_date)&& ($flat_discount_end_date>=$cur_date)){

                if($rest_provider_data->flat_discount_type==2){
                   $flat_discount=(($rest_provider_data->flat_discount/100)*$totalMenuAmount);

                }else{
                   $flat_discount=$rest_provider_data->flat_discount;
                }
            }
        }

       return (float)number_format($flat_discount, 2);
        //return (float)number_format($flat_discount,2);
        /*********************************FLAT DISCOUNT*********************************************/
    }


    public static function getDelivery($datafor = 'cart', $request, $restaurant_id, $order_type = 'delivery', $total_item = 0, $total_amount = 0, $total_weight = 0,$coupon_discount_amount=0,$pizzas_amount=0)
    {
        $orderType = $request->input('order_type') ?? 'delivery';
        $delivery_config = Restaurant::where(['id' => $restaurant_id, 'delivery_provider_id' =>0, 'status' => 1])->first();
        $is_tip_online=$delivery_config->is_tip_online;
        if ($delivery_config) {
            $tax = $total_amount * $delivery_config->tax / 100;  //need logic for this
            $service_tax = $tax * $delivery_config->service_tax / 100;  //need logic for this
            $delivery_charge = 0.00;
            $additional_charge = 0.00;
            $additional_charge_type = NULL;
            $is_greater_freedelivery=1;
          //  $tip_amount =0;
            $flat_discount=0.00;

           // $flat_discount=self::getDiscount($delivery_config,$total_amount,$request);
           
            $flat_discount_obj=self::getDiscount($delivery_config,$total_amount,$request,$pizzas_amount);
            
            $flat_discount=$flat_discount_obj['flat_discount'];
            $special_deal_delivery=$flat_discount_obj['special_deal_delivery'];

            $tip_amount = (float)$request->input('tip_amount') ?? 0.00;
           if ($order_type == 'carryout') { 
                
                if (!empty($delivery_config->additional_charge_takeout)) {
                    $additional_charge_type = $delivery_config->additional_charge_name;

                    if ($delivery_config->additional_charge_takeout_type == 1) {
                        if ($delivery_config->additional_charge_config == 2) {
                            $additional_charge = $delivery_config->additional_charge_takeout * $total_item;
                        } else {
                            $additional_charge = $delivery_config->additional_charge_takeout;
                        }

                    } else {

                        if ($delivery_config->additional_charge_config == 2) {
                            $additional_charge = ($total_amount * ($delivery_config->additional_charge_takeout / 100)) * $total_item;
                        } else {
                            $additional_charge = $total_amount * ($delivery_config->additional_charge_takeout / 100);
                        }
                    }
                }


            }else if ($order_type == 'delivery' ) {

               //&& $delivery_config->minimum_delivery <= $total_amount
                if (!empty($delivery_config->additional_charge_delivery)) {

                    $additional_charge_type = $delivery_config->additional_charge_name;
                    if ($delivery_config->additional_charge_delivery_type == 1) {
                        if ($delivery_config->additional_charge_config == 2) {
                            $additional_charge = $delivery_config->additional_charge_delivery * $total_item;
                        } else {
                            $additional_charge = $delivery_config->additional_charge_delivery;
                        }
                    } else {

                        if ($delivery_config->additional_charge_config == 2) {
                            $additional_charge = ($total_amount * ($delivery_config->additional_charge_delivery / 100)) * $total_item;
                        } else {
                            $additional_charge = $total_amount * ($delivery_config->additional_charge_delivery / 100);
                        }
                    }
                }

                 $delivery_check_amount=$total_amount-($flat_discount+$coupon_discount_amount); //new logic for delivery

              if ($delivery_config->free_delivery <= $delivery_check_amount) {  //here is the wrong logic there

                    $delivery_charge = 0;
                    return $data = [
                        'is_greater_freedelivery'=>$is_greater_freedelivery,
                        'special_deal_delivery'=>$special_deal_delivery,
                        'tip' => explode(",", $delivery_config->tip),
                        'checkout_allowed'=>$delivery_config->minimum_delivery <= $total_amount,
                        'tip_amount' =>(float)number_format($tip_amount, 2) ,
                        'tax' =>(float)number_format($tax, 2) ,
                        'service_tax' => (float)number_format($service_tax, 2),
                        'delivery_charge' =>(float)number_format($delivery_charge, 2) ,
                        'additional_charge' =>(float)number_format($additional_charge, 2) ,
                        'additional_charge_name' => $additional_charge_type,
                        'minimum_delivery' => $delivery_config->minimum_delivery != null ?(float)number_format($delivery_config->minimum_delivery, 2)  : 0,
                        'free_delivery' => $delivery_config->free_delivery,
                        'flat_discount'=>$flat_discount,
                        'is_tip_online'=>$is_tip_online
                       // 'discount_code'=>'',
                       // 'discount_value'=>'',
                    ];

                }
                else {
                    $delivery_rates = '';//'NEED LOGIC FOR THIS'

                    $is_greater_freedelivery=0;
                    if ($delivery_config->delivery_charge) { //Make it on live as well

                        if ($delivery_config->delivery_charge_type == 1) {

                        /*    if ($delivery_config->rate_type == 2) {
                                $delivery_charge = $delivery_getDeliveryconfig->delivery_charge * $total_item;
                            } else {*/
                                $delivery_charge = $delivery_config->delivery_charge;
                            //}
                        } else {
//                            if ($delivery_config->rate_type == 2) {
//                                $delivery_charge = ($total_amount * ($delivery_config->delivery_charge / 100)) * $total_item;
//                            } else {
                                $delivery_charge = ($total_amount * ($delivery_config->delivery_charge / 100));
                           // }
                        }

                        return $data = [
                            'is_greater_freedelivery'=>$is_greater_freedelivery,
                            'special_deal_delivery'=>$special_deal_delivery,
                            'tip' => explode(",", $delivery_config->tip),
                            'checkout_allowed'=>$delivery_config->minimum_delivery <= $total_amount,
                            'tip_amount' =>(float)number_format($tip_amount, 2) ,
                            'tax' =>(float)number_format($tax, 2) ,
                            'service_tax' => (float)number_format($service_tax, 2),
                            'delivery_charge' =>(float)number_format($delivery_charge, 2) ,
                            'additional_charge' =>(float)number_format($additional_charge, 2) ,
                            'additional_charge_name' => $additional_charge_type,
                            'minimum_delivery' => $delivery_config->minimum_delivery != null ?(float)number_format($delivery_config->minimum_delivery, 2)  : 0,
                            'free_delivery' => $delivery_config->free_delivery,
                            'flat_discount'=>$flat_discount,
                            'is_tip_online'=>$is_tip_online
                            // 'discount_code'=>'',
                            // 'discount_value'=>'',
                        ];

                    } else {
                        //return response()->json(['data' => null, 'error' => 'No Delivery rates defined'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();die();
                    }

                }

            }

            //if ($datafor == 'cart') {

            $checkout_allowed=false;
           if($order_type == 'carryout'){
               $checkout_allowed=true;

           }else{

               $checkout_allowed=$delivery_config->minimum_delivery <= $total_amount;
           }
            return $data = [
                'is_greater_freedelivery'=>$is_greater_freedelivery,
                'special_deal_delivery'=>$special_deal_delivery,
                'tip' => explode(",", $delivery_config->tip),
                'checkout_allowed'=>$checkout_allowed,
                'tip_amount' =>(float)number_format($tip_amount, 2) ,
                'tax' =>(float)number_format($tax, 2) ,
                'service_tax' => (float)number_format($service_tax, 2),
                'delivery_charge' =>(float)number_format($delivery_charge, 2) ,
                'additional_charge' =>(float)number_format($additional_charge, 2) ,
                'additional_charge_name' => $additional_charge_type,
                'minimum_delivery' => $delivery_config->minimum_delivery != null ?(float)number_format($delivery_config->minimum_delivery, 2)  : 0,
                'free_delivery' => $delivery_config->free_delivery,
                'flat_discount'=>$flat_discount,
                'is_tip_online'=>$is_tip_online
                // 'discount_code'=>'',
                // 'discount_value'=>'',
            ];

            //    }

        } else {
            return response()->json(['data' => null, 'error' => 'Delivery Service Not available'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();die();
        }

    }


    public  static  function  couponUsage($request,$locationId,$coupon_code){
      //in user order table we can use is_guest key to check guest order
      //by email id in order for both  logged in user and guest user
        $user_group_id = $request->header('User-group');
        if (  $bearerToken = $request->bearerToken()) {
            $userAuth = UserAuth::where(['access_token' => $bearerToken])->first();

            $user_group_id=$user_group_id??1;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $userAuth = UserAuth::where(['guest_token' => $token])->first();
            $user_group_id=$user_group_id??3;
        }

          $coupon_usage = UserOrder::where(['restaurant_id' => $locationId, 'promocode' =>$coupon_code])->count();
        if($user_group_id==3){
            $userAuthId       = $userAuth->id;

            $customer_coupon_count = UserOrder::where(['restaurant_id' => $locationId, 'promocode' =>$coupon_code,'user_auth_id'=>$userAuthId])->count();

        }else{
            $userId       = $userAuth->user_id;
            $customer_coupon_count = UserOrder::where(['restaurant_id' => $locationId, 'promocode' =>$coupon_code,'user_id'=>$userId])->count();


        }

          return [
              'coupon_usage'=>$coupon_usage,
              'customer_coupon_count'=>$customer_coupon_count
          ];
    }

    public  static function  validateCoupan($type='cart',$request,$coupon_code,$userAuthId,$total_menu_amount){

        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $discount=[];
        $discount['promocode_error']='';

        if ($userAuthId && !empty($userAuthId)) {
            $user_group_id = $request->header('User-group');
            if ($request->bearerToken()) {
                $field = 'access_token';
                $user_group_id = $user_group_id ?? 1;
            } else {
                $field = 'guest_token';
                $user_group_id = $user_group_id ?? 3;
            }
            $coupon = PromotionCoupon::where(['restaurant_id' => $locationId, 'coupon_code' => $coupon_code, 'status' => 1])
                ->whereRaw("find_in_set('".$user_group_id."',user_group_id)")
                ->whereNotNull('discount')->first();
            if($coupon){

                if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2)){   //static coupon code as per new requirement
                    $coupon_code=strtolower($coupon_code);
                    $pizza_coupon=self::getPizzaItemCheck($locationId,$userAuthId);
                }

                if($request->bearerToken() &&   (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['pizza_quantity']<1) {   //static coupon code as per new requirement

                    UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                    if ($type != 'cart') {
                        return response()->json(['data' => null, 'error' => 'Please add a signature pizza to your cart to redeem offer.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();

                    }
                    return;
                }

                if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['is_valid']==0){   //static coupon code as per new requirement
                    UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                    $discount['promocode_error'] = 'Your free pizza voucher code was replaced with a discount on your current selection(s) of medium or large pizzas. <br/>
To get your FREE pizza, please add another pizza to your bag and try the voucher code again.';
                    if ($type != 'cart') {
                        return response()->json(['data' => null, 'error' => 'Your free pizza voucher code was replaced with a discount on your current selection(s) of medium or large pizzas. <br/>
To get your FREE pizza, please add another pizza to your bag and try the voucher code again.'], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }

                     // return $discount;
                }else {

                    $coupon_usage_validate = self::couponUsage($request, $locationId, $coupon_code);

                    $coupon_usage = $coupon_usage_validate['coupon_usage'];
                    $customer_coupon_count = $coupon_usage_validate['customer_coupon_count'];

                    $uses_per_coupon = $coupon->uses_per_coupon;
                    $uses_per_customer = $coupon->uses_per_customer;
                    if ($field == 'guest_token' && $coupon_usage >= $uses_per_coupon) {

                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                        if ($type != 'cart') {
                            return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                            die();

                        }
                        return;

                    } else if ($field != 'guest_token' && ($coupon_usage >= $uses_per_coupon || $customer_coupon_count >= $uses_per_customer)) {
                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                        if ($type != 'cart') {
                            return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                            die();

                        }
                        return;
                    } else {

                        if ($coupon) {
                            //$cur_date=date('Y-m-d H:i:s');
                            $cur_date = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => date('Y-m-d H:i:s')));
                            $start_date = date('Y-m-d H:i:s', strtotime($coupon->start_date));
                            $end_date = date('Y-m-d H:i:s', strtotime($coupon->end_date));
                        }


                        if (!$coupon || ($coupon && !(($start_date <= $cur_date) && ($end_date >= $cur_date)))) {
                            UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                            if ($type != 'cart') {
                                return response()->json(['data' => null, 'error' => 'Voucher Code is invalid or expired'], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                                die();

                            }
                            return;

                        } else if (($coupon && (($start_date <= $cur_date) && ($end_date >= $cur_date)))) {
                            if ($coupon->discount_type == 2) {
                                $discount_amount = $total_menu_amount * ($coupon->discount / 100);
                            } else {
                                $discount_amount = $coupon->discount;
                            }
                            $minimum_amount = $coupon->minimum_amount;

                            if ($total_menu_amount > $minimum_amount) {
                                if ($request->bearerToken() && (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code) == self::$static_pizza_coupon || strtolower($coupon_code) == self::$static_pizza_coupon2)) {   //static coupon code as per new requirement

                                    if (!empty($pizza_coupon['lowPizzaAmount'])) {

                                        /*if(($total_menu_amount-$pizza_coupon['lowPizzaAmount'])<= $minimum_amount && strtolower($coupon_code)==self::$static_pizza_coupon ){
                                            UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));

                                            $restaurantId = $request->header('X-restaurant');

                                            $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
                                            if($restData) {
                                                $curSymbol = $restData->currency_symbol;
                                                $curCode = $restData->currency_code;
                                            } else {
                                                $curSymbol = config('constants.currency');
                                                $curCode = config('constants.currency_code');
                                            }
                                            if ($type != 'cart') 
                                                return response()->json(['data' => null, 'error' => 'Order amount must be greater than ' . $curSymbol . $minimum_amount], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                                                die();
                                            }
                                            return;
                                        }else {*/

                                        $discount_amount = $pizza_coupon['lowPizzaAmount'];
                                        $discount['promocode'] = $coupon_code;
                                        $discount['promocode_discount'] = (float)number_format($discount_amount, 2);
                                        // }
                                    }

                                } else {
                                    $discount['promocode'] = $coupon_code;
                                    $discount['promocode_discount'] = (float)number_format($discount_amount, 2);
                                }

                                $discount['minimum_delivery'] = $minimum_amount;


                            } else {
                                UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                                $restaurantId = $request->header('X-restaurant');

                                $restData = Restaurant::find($restaurantId)->select('currency_symbol', 'currency_code')->first();
                                if ($restData) {
                                    $curSymbol = $restData->currency_symbol;
                                    $curCode = $restData->currency_code;
                                } else {
                                    $curSymbol = config('constants.currency');
                                    $curCode = config('constants.currency_code');
                                }
                                if ($type != 'cart') {

                                    return response()->json(['data' => null, 'error' => 'Offer only valid on orders over '.$curSymbol. $minimum_amount.'. Add more items to your cart.'], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                                    die();

                                } else {
                                    $discount['promocode_error'] = 'Offer only valid on orders over '.$curSymbol. $minimum_amount.'. Add more items to your cart.';
                                }

                                $discount['minimum_delivery'] = $minimum_amount;

                            }
                        }
                    }
                }



        }else{

                UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
            }

            return $discount;

        }



        /*
        Select * from promotion_coupon where coupon_code= User Coupon;
        if(discount != NULL && (Current Datetime >= start_date ||  Current Datetime <= end_date))
        {
            if(discount_type==Percentage{
                Coupon Discount = Cart Total * discount/100;
            }
            else
            {
                Coupon Discount = discount;
            }
        }*/
    }

    public  static  function removeCoupon($request)
    {
        $coupon_code=$request->input('coupon');
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $status=0;
        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $field = 'access_token';
            $bearerToken = true;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $field = 'guest_token';
            $bearerToken = false;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth && !empty($coupon_code)) {

            $userAuthId = $userAuth->id;
            $userId = $userAuth->user_id ?? 0;
            $debug = $request->input('debug');            

            UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->whereNotNull('coupon_code')->update(array('coupon_code' => NULL));
            $status=1;            
        }

        //return  $status;
        $data_resp=new \stdClass();
        $data_resp->status=$status;

        return response()->json(['data' => $data_resp, 'error' => null, ], Config('constants.status_code.STATUS_SUCCESS'),['Access-Control-Allow-Origin' => '*'])->send();
    }

    public static  function getBagsTotalAmount($locationId,$userAuthId){
        $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->get(['id','total_menu_amount']);
        $total_menu_amount = 0;
        if ($userBagItems) {
            foreach ($userBagItems as $userMyBag) {
                $total_menu_amount += $userMyBag['total_menu_amount'];

            }
        }
        return $total_menu_amount;
    }

    public static  function getPizzaItemCheck($locationId,$userAuthId,$type='cart'){


        $userBagItems = UserMyBag::where('menu_id', '!=' , 0)->where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->orderBy('unit_price', 'ASC')->get(['id','size','unit_price','menu_id','quantity']);
        $lowestpizzaAmount = 0;
        $pizzaquant=0;

        $total_menu_amount=0;
        $medium_pizza_quant=$large_pizza_quant=0;
        $is_valid=0;

        if ($userBagItems) {
            foreach ($userBagItems as $userMyBag) {
                $total_menu_amount += $userMyBag['total_menu_amount'];


                $menuItemDetail = NewMenuItems::select('id', 'menu_category_id')->find($userMyBag->menu_id);

                if($menuItemDetail){

                    $menu_category_id=$menuItemDetail->menu_category_id;
                    $menuCat = MenuCategories::where(['id' =>$menu_category_id,'status' => 1])->first(['id','status','slug']);
                    if($menuCat->slug=='pizza'){

                        if(self::startsWith($userMyBag->size,'Medium')){
                            $medium_pizza_quant+=$userMyBag->quantity;

                        }
                        if(self::startsWith($userMyBag->size,'Large')){
                            $large_pizza_quant+=$userMyBag->quantity;

                        }


                       if($lowestpizzaAmount==0){
                           $lowestpizzaAmount=$userMyBag->unit_price;
                       }else if($userMyBag->unit_price<$lowestpizzaAmount){

                           $lowestpizzaAmount=$userMyBag->unit_price;

                       }

                        $pizzaquant+=$userMyBag->quantity;
                    }

                }

            }
            $mid_large_quant=$medium_pizza_quant+$large_pizza_quant;

            if($medium_pizza_quant && ($medium_pizza_quant==1 || ($medium_pizza_quant%2)!=0)){
                $is_valid=1;
            }else  if($mid_large_quant && ($mid_large_quant==1 || ($mid_large_quant%2)!=0)){
                $is_valid=1;
            }


        }
        return ['is_valid'=>$is_valid,'lowPizzaAmount'=>$lowestpizzaAmount,'pizza_quantity'=>$pizzaquant,'total_menu_amount'=>$total_menu_amount];
    }
    public static  function isRegistrationNeededForCoupon($group_id,$locationId,$coupon_code)
    {
         if($group_id!=1){
            $coupon = PromotionCoupon::where(['restaurant_id' => $locationId, 'coupon_code' => $coupon_code, 'status' => 1])
                ->whereNotNull('discount')->first(['user_group_id']);
            if ($coupon) {
                $groups = explode(",", $coupon->user_group_id);

                if (in_array(1, $groups)) {
                    if ((strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2)) {
                        $error_msg = 'Offer only valid for registered users. Sign up now.';

                    } else {
                        $error_msg = 'Offer only valid for registered users. Sign up now.';
                    }
                    return response()->json(['data' => null, 'error' => $error_msg], Config('constants.status_code.BAD_REQUEST'), ['Access-Control-Allow-Origin' => '*'])->send();
                    die();
                }
            }

        }
    }
    public  static  function applyCoupon($request)
    {
        $coupon_code=$request->input('coupon');
        $locationId = $request->header('X-location');
        $restaurantId = $request->header('X-restaurant');
        $status=0;
       
        $user_group_id = $request->header('User-group');
        if ($request->bearerToken()) {
            $token = $request->bearerToken();
            $field = 'access_token';
            $bearerToken = true;
            $user_group_id=$user_group_id??1;
        } else {
            $token = explode(' ', $request->header('Authorization'))[1];
            $field = 'guest_token';
            $bearerToken = false;
            $user_group_id=$user_group_id??3;
        }
        $userAuth = UserAuth::where([$field => $token])->first();
        if ($userAuth && !empty($coupon_code)) {

            $userAuthId = $userAuth->id;
            $userId = $userAuth->user_id ?? 0;  
            
            if(self::getPizzaItemForDeal($locationId, $userAuthId)){
                return response()->json(['data' => null, 'error' => 'A discount is already applied to your order based on your current selection(s) of pizzas.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                die();
            }
            
            
            $userBagItems = UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->whereNotNull('coupon_code')->first(['id']);
            
            if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2)){   //static coupon code as per new requirement
                $coupon_code=strtolower($coupon_code);
                $pizza_coupon=self::getPizzaItemCheck($locationId,$userAuthId);
            }
                     
            if($userBagItems) {
                $coupon = PromotionCoupon::where(['restaurant_id' => $locationId, 'coupon_code' =>$coupon_code, 'status' => 1])
                    ->whereRaw("find_in_set('".$user_group_id."',user_group_id)")
                    ->whereNotNull('discount')->first();               
               
                
                if($coupon) {



                    if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['pizza_quantity']<1){   //static coupon code as per new requirement
                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));

                        return response()->json(['data' => null, 'error' => 'Please add a signature pizza to your cart to redeem offer.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }

                  if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['is_valid']==0){   //static coupon code as per new requirement
                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));

                        return response()->json(['data' => null, 'error' => 'A discount is already applied to your order total based on your current selection(s) of medium or large pizzas.<br> 
To get your FREE pizza on top of it all, please add another pizza to your bag and try the voucher code again.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }

                $coupon_usage_validate = self::couponUsage($request, $locationId, $coupon_code);

                $coupon_usage = $coupon_usage_validate['coupon_usage'];
                $customer_coupon_count = $coupon_usage_validate['customer_coupon_count'];

                $uses_per_coupon = $coupon->uses_per_coupon;
                $uses_per_customer = $coupon->uses_per_customer;
                if ($field=='guest_token' && $coupon_usage >= $uses_per_coupon ) {
                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                        return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
               }
                else if ($field!='guest_token' && ($coupon_usage >= $uses_per_coupon || $customer_coupon_count >= $uses_per_customer)) {
                    UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));
                    return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                    die();

                }else {
                    if ($coupon) {
                        // $cur_date=date('Y-m-d H:i:s');
                        $cur_date = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => date('Y-m-d H:i:s')));

                        $start_date = date('Y-m-d H:i:s', strtotime($coupon->start_date));
                        $end_date = date('Y-m-d H:i:s', strtotime($coupon->end_date));
                    }

                    if (!$coupon || ($coupon && !(($start_date <= $cur_date) && ($end_date >= $cur_date)))) {

                        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => NULL));


                        return response()->json(['data' => null, 'error' => 'Voucher Code is invalid or expired'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();

                    } else {

                        return response()->json(['data' => null, 'error' => "You've already used this code."], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();

                    }
                }

                }else{
                    self::isRegistrationNeededForCoupon($user_group_id,$locationId,$coupon_code);
                    return response()->json(['data' => null, 'error' => 'Voucher Code is invalid or expired'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                    die();
                }
            }else {
                $coupon = PromotionCoupon::where(['restaurant_id' => $locationId, 'coupon_code' => $coupon_code, 'status' => 1])
                    ->whereRaw("find_in_set('".$user_group_id."',user_group_id)")
                    ->first();
                if($coupon) {
                    if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['pizza_quantity']<1){   //static coupon code as per new requirement

                        return response()->json(['data' => null, 'error' => 'Please add a signature pizza to your cart to redeem offer.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }
                    if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2) && $pizza_coupon['is_valid']==0){   //static coupon code as per new requirement

                        return response()->json(['data' => null, 'error' => 'A discount is already applied to your order total based on your current selection(s) of medium or large pizzas.<br> 
To get your FREE pizza on top of it all, please add another pizza to your bag and try the voucher code again.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }


                    $coupon_usage_validate = self::couponUsage($request, $locationId, $coupon_code);

                    $coupon_usage = $coupon_usage_validate['coupon_usage'];
                    $customer_coupon_count = $coupon_usage_validate['customer_coupon_count'];

                    $uses_per_coupon = $coupon->uses_per_coupon;
                    $uses_per_customer = $coupon->uses_per_customer;
                    if ($field=='guest_token' && $coupon_usage >= $uses_per_coupon ) {
                        return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();
                    }
                    else if ($field!='guest_token' && ($coupon_usage >= $uses_per_coupon || $customer_coupon_count >= $uses_per_customer)) {
                        return response()->json(['data' => null, 'error' => 'Voucher code usage limit exceeded!!'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                        die();

                    } else {
                        if ($coupon) {
                            // $cur_date=date('Y-m-d H:i:s');
                            $cur_date = CommonFunctions::getLocalTimeBased(array("restaurant_id" => $locationId, 'datetime' => date('Y-m-d H:i:s')));

                            $start_date = date('Y-m-d H:i:s', strtotime($coupon->start_date));
                            $end_date = date('Y-m-d H:i:s', strtotime($coupon->end_date));
                        }


                        if (($coupon && (($start_date <= $cur_date) && ($end_date >= $cur_date)))) {

                            $total_menu_amount = self::getBagsTotalAmount($locationId, $userAuthId);

                            if ($coupon->discount_type == 2) {
                                $discount_amount = $total_menu_amount * ($coupon->discount / 100);
                            } else {
                                $discount_amount = $coupon->discount;
                            }

                            $minimum_amount = $coupon->minimum_amount;
                            if ($total_menu_amount > $minimum_amount) {
                                if($request->bearerToken() &&  (strtolower($coupon_code)==self::$static_pizza_coupon_free || strtolower($coupon_code)==self::$static_pizza_coupon || strtolower($coupon_code)==self::$static_pizza_coupon2)) {   //static coupon code as per new requirement
                                    if(!empty($pizza_coupon['lowPizzaAmount'])){
                                        /*
                                        if((($total_menu_amount-$pizza_coupon['lowPizzaAmount'])<= $minimum_amount and strtolower($coupon_code)==self::$static_pizza_coupon)){
                                            $restaurantId = $request->header('X-restaurant');

                                            $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
                                            if($restData) {
                                                $curSymbol = $restData->currency_symbol;
                                                $curCode = $restData->currency_code;
                                            } else {
                                                $curSymbol = config('constants.currency');
                                                $curCode = config('constants.currency_code');
                                            }
                                            return response()->json(['data' => null, 'error' => 'Order amount must be greater than ' .$curSymbol.$minimum_amount], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                                            die();
                                        }else{*/

                                            UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' =>$coupon_code));
                                            $status = 1;
                                        //}

                                    }

                                }else{

                                    UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->update(array('coupon_code' => $coupon_code));
                                    $status = 1;
                                }

                            } else {
                                $restaurantId = $request->header('X-restaurant');

                                $restData = Restaurant::find($restaurantId)->select('currency_symbol','currency_code')->first();
                                if($restData) {
                                    $curSymbol = $restData->currency_symbol;
                                    $curCode = $restData->currency_code;
                                } else {
                                    $curSymbol = config('constants.currency');
                                    $curCode = config('constants.currency_code');
                                }

                                return response()->json(['data' => null, 'error' => 'Offer only valid on orders over '.$curSymbol. $minimum_amount.'. Add more items to your cart.'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                                die();

                            }

                        } else {
                            return response()->json(['data' => null, 'error' => 'Voucher Code is invalid or expired'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                            die();


                        }
                    }
                }else{
                    self::isRegistrationNeededForCoupon($user_group_id,$locationId,$coupon_code);
                    return response()->json(['data' => null, 'error' => 'Voucher Code is invalid or expired'], Config('constants.status_code.BAD_REQUEST'),['Access-Control-Allow-Origin' => '*'])->send();
                    die();
                }


            }
            }

      //  return $status;
        $data_resp=new \stdClass();
        $data_resp->status=$status;

        if($data_resp->status){
            $data_resp->message='Voucher Code applied successfully';

        }

        return response()->json(['data' => $data_resp, 'error' => null, ], Config('constants.status_code.STATUS_SUCCESS'),['Access-Control-Allow-Origin' => '*'])->send();
    }
    
    
    public static  function getPizzaItemForDeal($locationId,$userAuthId){
        if(self::$deal_on_takeout_only_fifty_percent==1){
            $userBagItems = UserMyBag::where('menu_id', '!=' , 0)
               ->where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->orderBy('unit_price', 'ASC')
               ->get(['id','menu_id','order_type']);

            if ($userBagItems) {

                foreach ($userBagItems as $userMyBag) {
                    if($userMyBag->order_type=="carryout"){
                        $menuItemDetail = NewMenuItems::select('id', 'menu_category_id')->find($userMyBag->menu_id);

                        if($menuItemDetail){

                            $menu_category_id=$menuItemDetail->menu_category_id;
                            $menuCat = MenuCategories::where(['id' =>$menu_category_id,'status' => 1])->first(['id','status','slug']);

                            if($menuCat->slug=='pizza'){        
                               return true;
                            }
                        }
                    }
               }        
            }
        }
        return false;
    }
    
    
    public  static  function removeCouponForPizzaDeal($locationId, $userAuthId)
    {
        UserMyBag::where(['restaurant_id' => $locationId, 'user_auth_id' => $userAuthId])->whereNotNull('coupon_code')->update(array('coupon_code' => NULL));
        return true;
    }
    
    public static  function getPizzaItemForDealV2($itemId){
        if(self::$deal_on_takeout_only_fifty_percent==1){
            $menuItemDetail = NewMenuItems::select('id', 'menu_category_id')->find($itemId);

            if($menuItemDetail){

                $menu_category_id=$menuItemDetail->menu_category_id;
                $menuCat = MenuCategories::where(['id' =>$menu_category_id,'status' => 1])->first(['id','status','slug']);

                if($menuCat->slug=='pizza'){        
                   return true;
                }
            }
        }
               
        return false;
    }
    
}
