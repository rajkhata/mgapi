<?php
    /**
     * Created by PhpStorm.
     * User: Amit Malakar
     * Date: 17/4/18
     * Time: 11:28 AM
     */

    /**
     * @SWG\Swagger(
     *     basePath="/",
     *     schemes={"http", "https"},
     *     @SWG\Info(
     *         version="1.0.0",
     *         title="Multipage & Pizza API",
     *         @SWG\Contact(
     *             email="amalakar@bravvura.in"
     *         ),
     *     )
     * )
     */

// ===== v2 AUTH,USER =========
    /**
     * @SWG\Post(
     *   path="/api/auth/guest",
     *   summary="v2 - Request Guest token.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="platform",
     *     in="formData",
     *     description="Platform.",
     *     required=true,
     *     enum={"web", "ios", "android"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_id",
     *     in="formData",
     *     description="Device id (only for mobile).",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_token",
     *     in="formData",
     *     description="Device token (only for mobile).",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/login",
     *   summary="v2 - Request login, get access_token.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="User's email address.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="User's password.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/auth/logout",
     *   summary="v2 - Request logout, clears MyBag. (## CHANGES ##)",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/forgot",
     *   summary="v2 - Forget password, generate OTP for email/mobile.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="User's email/mobile.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/verifyotp",
     *   summary="v2 - Verify OTP, in case of forgot password.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="otp",
     *     in="formData",
     *     description="Forgot password OTP.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/reset",
     *   summary="v2 - Reset password.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="New password.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/register",
     *   summary="v2 - User registration.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="fname",
     *     in="formData",
     *     description="First name.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="lname",
     *     in="formData",
     *     description="Last name.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email address.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/auth/social",
     *   summary="v2 - User's Social registration/login.",
     *   tags={"Auth"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="lname",
     *     in="formData",
     *     description="Last name.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="social",
     *     in="formData",
     *     description="Social.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="access_token",
     *     in="formData",
     *     description="Social access token.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_source",
     *     in="formData",
     *     description="User source.",
     *     required=true,
     *     enum={"fb", "gp"},
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

// ===== v2 USER CARD ==========
    /**
     * @SWG\Get(
     *   path="/api/card",
     *   summary="v2 - Get User card list.",
     *   tags={"User Card"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/user/card",
     *   summary="v2 - Add user card.",
     *   tags={"User Card"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Card Name.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="card",
     *     in="formData",
     *     description="Card numbers.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="type",
     *     in="formData",
     *     description="Card type.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="expiry_month",
     *     in="formData",
     *     description="Card expiry month.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="expiry_year",
     *     in="formData",
     *     description="Card expiry year.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

// ===== v2 USER ADDRESS =======
    /**
     * @SWG\Get(
     *   path="/api/address",
     *   summary="v2 - Get User address list.",
     *   tags={"User Address"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/user/address",
     *   summary="v2 - Add user address.",
     *   tags={"User Address"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-location",
     *     in="header",
     *     description="Location Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Name.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="label",
     *     in="formData",
     *     description="Label.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address_type",
     *     in="formData",
     *     description="Address type.",
     *     required=true,
     *     enum={"billing", "shipping"},
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address1",
     *     in="formData",
     *     description="Address line 1.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address2",
     *     in="formData",
     *     description="Address line 2.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="street",
     *     in="formData",
     *     description="Street.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     description="City.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state",
     *     in="formData",
     *     description="State.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     description="Zipcode.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Phone.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="latitude",
     *     in="formData",
     *     description="Latitude.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="longitude",
     *     in="formData",
     *     description="Longitude.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */


// ===== PIZZA =========
    /**
     * @SWG\Get(
     *   path="/api/pizzaexp/restaurant/{id}",
     *   summary="Restaurant specific all Pizza settings.",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/pizzaexp/restaurant/{id}/setting/{setting}",
     *   summary="Restaurant specific, particular Pizza setting.",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="setting",
     *     in="path",
     *     description="Page type.",
     *     required=true,
     *     enum={"crust", "size", "sauce", "cheese", "topping_veg", "topping_non_veg"},
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/pizzaexp/restaurant/{id}/topcount",
     *   summary="Pizza selected topping count.",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/order",
     *   summary="TEMP order POST JSON expected from front-end.",
     *   tags={"Pizza"},
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/user/{id}/address",
     *   summary="All user addresses.",
     *   tags={"Pizza", "User"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/user/{id}/card",
     *   summary="All user cards.",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{id}/menu",
     *   summary="TEMP menu data.",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{restaurant_id}/category",
     *   summary="Get all Restaurant categories",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{restaurant_id}/user/{user_id}/menu/suggestion",
     *   summary="Get all Restaurant categories",
     *   tags={"Pizza"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

// ===== ORDER,USER =========
    /**
     * @SWG\Get(
     *   path="/api/user/{id}/order",
     *   summary="User MyBags",
     *   tags={"Order", "User"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/order",
     *   summary="Add menu item to MyBag.",
     *   tags={"Order"},
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="pizza",
     *     in="path",
     *     description="Pizza or other category.",
     *     required=true,
     *     type="boolean"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_json",
     *     in="path",
     *     description="Build Your Pizza item JSON.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="menu_id",
     *     in="path",
     *     description="Menu id.",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="order_type",
     *     in="path",
     *     description="Order type - delivery/takeout.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="special_instruction",
     *     in="path",
     *     description="Special instructions.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="quantity",
     *     in="path",
     *     description="Quantity of items.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Post(
     *   path="/api/user/{user_id}/payment",
     *   summary="Payment URL for User",
     *   tags={"Order", "User"},
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="lat",
     *     in="path",
     *     description="Latitude.",
     *     required=false,
     *     type="boolean"
     *   ),
     *   @SWG\Parameter(
     *     name="lng",
     *     in="path",
     *     description="Longitude.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="path",
     *     description="Phone.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="order_type",
     *     in="path",
     *     description="Order type - delivery/takeout.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="order_amount",
     *     in="path",
     *     description="Order amount.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="pay_via_card",
     *     in="path",
     *     description="Payment via card.",
     *     required=true,
     *     type="boolean"
     *   ),
     *   @SWG\Parameter(
     *     name="tax",
     *     in="path",
     *     description="Tax amount.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="tip_percent",
     *     in="path",
     *     description="Tip percentage.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_charge",
     *     in="path",
     *     description="Delivery charge.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_user_address_id",
     *     in="path",
     *     description="Delivery address of user.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="delivery_time",
     *     in="path",
     *     description="Delivery time.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="instructions",
     *     in="path",
     *     description="Instructions.",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="card_id",
     *     in="path",
     *     description="User's card id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

// ===== USER =========
    /**
     * @SWG\Get(
     *   path="/api/user",
     *   summary="Get all user details.",
     *   tags={"User"},
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/user/{id}",
     *   summary="Get specific user detail.",
     *   tags={"User"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{id}",
     *   summary="Get specific restaurant detail.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/user/restaurant/{id}",
     *   summary="Restaurant registered users.",
     *   tags={"User","Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{id}/branch",
     *   summary="Brand Restaurant's branches list.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/geoloc/{geoloc}",
     *   summary="(NOT IN USE) Restaurant branches within User's proximity.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="geoloc",
     *     in="path",
     *     description="Latitude & Longitude respectively.",
     *     required=true,
     *     type="array",
     *     @SWG\Items(type="string"),
     *     collectionFormat="csv"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{id}/geoloc/{geoloc}",
     *   summary="User's distance with selected Restaurant branches.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="geoloc",
     *     in="path",
     *     description="Latitude & Longitude respectively.",
     *     required=true,
     *     type="array",
     *     @SWG\Items(type="string"),
     *     collectionFormat="csv"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

// ===== RESTAURANT =========
    /**
     * @SWG\Get(
     *   path="/api/restaurantbranches",
     *   summary="v2 - Restaurant branches list.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="X-restaurant",
     *     in="header",
     *     description="Restaurant Id.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

    /**
     * @SWG\Get(
     *   path="/api/static/restaurant/{id}",
     *   summary="Restaurant specific all static pages.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/static/restaurant/{id}/{page}",
     *   summary="Restaurant specific, particular static page.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="page",
     *     in="path",
     *     description="Page type.",
     *     required=true,
     *     enum={"home", "about", "story", "menu", "team", "branch"},
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{restaurant_id}/calculations",
     *   summary="Restaurant tax, delivery, tip percent.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{restaurant_id}/menu/category/{category}",
     *   summary="OLD - Restaurant specific, particular static page.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="category",
     *     in="path",
     *     description="Category.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */
    /**
     * @SWG\Get(
     *   path="/api/restaurant/{restaurant_id}/user/{user_id}/menu/category/{category}",
     *   summary="NEW - Restaurant specific, particular static page.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="user_id",
     *     in="path",
     *     description="User id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="category",
     *     in="path",
     *     description="Category.",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=406, description="Not acceptable"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */

     /**
     * @SWG\Get(
     *   path="/api/restaurant/getDeliveryCarryoutStatus/{restaurant_id}",
     *   summary="Get Restaurant List with Open and Close status.",
     *   tags={"Restaurant"},
     *   @SWG\Parameter(
     *     name="restaurant_id",
     *     in="path",
     *     description="Restaurant id.",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="X-localization",
     *     in="header",
     *     description="Language code - en, sp, ch etc.",
     *     required=true,
     *     type="string"
     *   ),    
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer / Guest <token>",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Bad request"),
     *   @SWG\Response(response=401, description="Unauthorized Request"),
     *   @SWG\Response(response=500, description="Internal server error"),
     *   @SWG\Response(response="default", description="An ""unexpected"" error")
     * )
     */