<?php

    /**
     * Created by PhpStorm.
     * User: Amit Malakar
     * Date: 17/1/19
     * Time: 5:10 AM
     * Old Date: 17/4/18
     * Old Time: 11:28 AM
     */

    #use OpenApi\Annotations as OA;

    /**
     * @OA\Info(
     *     version="1.0",
     *     title="Multipage API Documenation",
     *     @OA\Contact(
     *         email="amalakar@bravvura.in"
     *     ),
     * )
     */

    // ===== v2 AUTH,USER =========

    /**
     * @OA\Post(
     *     path="/api/auth/guest",
     *     summary="Request Guest token.",
     *     tags={"Auth"},
     *   @OA\Parameter(
     *     name="platform",
     *     in="path",
     *     description="Platform.",
     *     required=true,
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="device_id",
     *     in="path",
     *     description="Device id (only for mobile).",
     *     required=false,
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Parameter(
     *     name="device_token",
     *     in="path",
     *     description="Device token (only for mobile).",
     *     required=false,
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=200, description="Success"),
     *   @OA\Response(response=406, description="Not acceptable"),
     *   @OA\Response(response=500, description="Internal server error"),
     *   @OA\Response(response="default", description="An ""unexpected"" error")
     * )
     */

    // ===== v2 USER CARD ==========

    // ===== v2 USER ADDRESS =======

    // ===== PIZZA =========

    // ===== ORDER,USER =========

    // ===== USER =========
    /**
     * @OA\Get(
     *   path="/api/user",
     *   summary="Get all user details.",
     *   tags={"User"},
     *   @OA\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Bearer <token>.",
     *     required=true,
     *     @OA\Schema(type="string"),
     *   ),
     *   @OA\Response(response=200, description="Success"),
     *   @OA\Response(response=406, description="Not acceptable"),
     *   @OA\Response(response=500, description="Internal server error"),
     *   @OA\Response(response="default", description="An ""unexpected"" error")
     * )
     */

    

    // ===== RESTAURANT =========