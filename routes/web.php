<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

    use App\Models\Restaurant;
    use App\Models\ItemAddonGroup;
    use Illuminate\Support\Facades\DB;
    use App\Helpers\CommonFunctions;
    use Illuminate\Http\Request;

$router->get('/a', function () use ($router) {
    $res = config('constants.image.path.byp');

    return response()->json([$router->app->version(), $res]);
});

// 'middleware' => 'auth:api',
$router->group(['prefix' => 'api/auth','middleware' => ['localization','useragent']], function() use($router) {
    // DOCUMENTED
    $router->post('guest', 'AuthController@guest');
    $router->post('login', 'AuthController@login');
    //$router->get('logout', 'AuthController@logout');
    $router->post('logout', 'AuthController@logout');
    $router->post('forgot', 'AuthController@forgot');   // forgot and resend OTP
    $router->post('verifyotp', 'AuthController@verifyOtp');
    $router->post('reset', 'AuthController@resetpwd');
    $router->post('register', 'AuthController@register');

    $router->post('social', 'AuthController@social');
    $router->post('checkSocialUserExist', 'AuthController@checkSocial');

    $router->post('refresh', 'AuthController@refresh');
    $router->post('me', 'AuthController@me');
});


$router->group(['prefix' => 'api','middleware' => ['localization','useragent']], function () use ($router) {
    // localization
    $router->get('locale/{id}',     ['uses' => 'LocalizeController@getAllText']);
    $router->get('languages',     ['uses' => 'LocalizeController@getLanguage']);
    //mail
    $router->post('send', ['uses'=>'EmailController@sendMail']);
    //netcore subscription
    $router->get('subscribe', ['uses'=>'SubscriptionsController@showSubscrib']);
    //sms
    $router->post('send/sms', ['uses'=>'EmailController@sendSms']);
    //netcore
    ////sms
    $router->post('send/netcore', ['uses'=>'EmailController@pushNetcore']);

    // User
    $router->get('user',            ['uses' => 'UserController@showAllUsers']);
    $router->get('user/social',     ['uses' => 'UserController@getUserSocial']);
    $router->get('user/{id}',       ['uses' => 'UserController@showOneUser']);
    $router->post('user/updatesociallogincode',       ['uses' => 'UserController@addPromoCodeToUser']);		
    $router->post('user',           ['uses' => 'UserController@create']);
    $router->delete('user/{id}',    ['uses' => 'UserController@delete']);
    $router->put('user/{id}',       ['uses' => 'UserController@update']);   // user profile update, after signup
    $router->put('user',            ['uses' => 'UserController@update']);
    $router->put('resetpassword',   ['uses' => 'UserController@resetPassword']);
    $router->put('updateprofile',   ['uses' => 'UserController@updateProfile']);

    //Created by Jawed to check if user exits based on email address and restaurant
    $router->get('user/exits/{email}',            ['uses' => 'UserController@checkUser']);

    // User addresses
    //$router->get('user/{userId}/address',           ['uses' => 'UserAddressController@showAllUserAddresses']);
    $router->get('address',           ['uses' => 'UserAddressController@showAllUserAddresses']);
    $router->get('user/{userId}/address/{id}',      ['uses' => 'UserAddressController@showOneUserAddress']);
    $router->post('user/address',                   ['uses' => 'UserAddressController@create']);    // user address add, after signup
    //$router->delete('user/{userId}/address/{id}',   ['uses' => 'UserAddressController@delete']);
    //$router->put('user/{userId}/address/{id}',      ['uses' => 'UserAddressController@update']);
    $router->delete('user/address/{id}',   ['uses' => 'UserAddressController@delete']);
    $router->put('user/address/{id}',      ['uses' => 'UserAddressController@update']);
    $router->get('country/{countryId}/states',      ['uses' => 'UserAddressController@getStates']);
    $router->get('state/{stateId}/cities',          ['uses' => 'UserAddressController@getCities']);

    // User payment cards
    //$router->get('user/{userId}/card',          ['uses' => 'UserCardController@showAllUserCards']);
    $router->get('card',          ['uses' => 'UserCardController@showAllUserCards']);
    $router->get('user/{userId}/card/{id}',     ['uses' => 'UserCardController@showOneUserCard']);
    $router->post('user/card',                  ['uses' => 'UserCardController@create']);   // user card add, after signup
    //$router->delete('user/{userId}/card/{id}',  ['uses' => 'UserCardController@delete']);
    $router->delete('user/card/{id}',  ['uses' => 'UserCardController@delete']);
    $router->put('user/{userId}/card/{id}',     ['uses' => 'UserCardController@update']);


    // Restaurant
    $router->get('restaurant',          ['uses' => 'RestaurantController@showAllRestaurants']);     // get all restaurants
    $router->get('restaurantbranches',  ['uses' => 'RestaurantController@showAllRestaurantBranches']);     // get restaurant branches
    $router->get('restaurant/{id}',     ['uses' => 'RestaurantController@showOneRestaurant']);
    $router->post('restaurant',         ['uses' => 'RestaurantController@create']);
    $router->delete('restaurant/{id}',  ['uses' => 'RestaurantController@delete']);
    $router->put('restaurant/{id}',     ['uses' => 'RestaurantController@update']);
    $router->get('restaurant/new/list',  ['uses' => 'RestaurantController@showOneRestaurantNew']);

    // Static pages
    $router->get('static',          ['uses' => 'StaticController@showRestContent']);
    $router->get('static/{page}',   ['uses' => 'StaticController@showRestPageContent']);

    // PIZAA EXP
    $router->get('pizzaexp/restaurant/{id}',                    ['uses' => 'PizzaExpController@showAllPizzaSettings']);
    $router->get('pizzaexp/restaurant/{id}/setting/{setting}',  ['uses' => 'PizzaExpController@showPizzaSetting']);
    $router->get('pizzaexp/restaurant/{id}/topcount',           ['uses' => 'PizzaExpController@showToppingsCount']);

    // ORDER
    $router->get('mybag',               ['uses' => 'MybagController@showAllUserMyBagItems']);   // TEMP ROUTE - for PIZZA ORDER
    $router->get('mybag/{id}',          ['uses' => 'MybagController@showOneOrder']);
    $router->post('mybag',              ['uses' => 'MybagController@create']);          // add to my bag
    $router->delete('mybag/{id}',       ['uses' => 'MybagController@delete']);
    $router->put('mybag/{id}',          ['uses' => 'MybagController@update']);
    $router->get('mybag/test/{id}',     ['uses' => 'MybagController@testpayment']);
    //$router->get('user/mybag',          ['uses' => 'MybagController@userMyBagItems']);  // User MyBag items
    $router->get('deletebag',    ['uses' => 'MybagController@deleteCart']);   // TEMP ROUTE - for PIZZA ORDER

    // PAYMENT
    $router->get('user/{userId}/payment',          ['uses' => 'PaymentController@showAllPayments']);  //
    $router->get('user/{userId}/payment/{id}',     ['uses' => 'PaymentController@showOnePayment']);
    $router->post('user/{userId}/payment',         ['uses' => 'PaymentController@create']);           // Charge customer
    $router->delete('user/{userId}/payment/{id}',  ['uses' => 'PaymentController@delete']);
    $router->put('user/{userId}/payment/{id}',     ['uses' => 'PaymentController@update']);
    $router->post('user/placeorder',         ['uses' => 'PaymentController@placeOrder']);


    // NORMAL MENU
    $router->get('restaurant/{restId}/menu',                                      ['uses' => 'MenuController@showAllRestaurantMenus']);         // TO DELETE TEMP ROUTE - for PIZZA ORDER
    $router->get('restaurant/{restId}/menu/category/{category}',                  ['uses' => 'MenuController@showAllRestaurantCategoryMenus']); // TEMP ROUTE - for PIZZA ORDER
    //$router->get('restaurant/{restId}/menu/{id}',                                 ['uses' => 'MenuController@showOneRestaurantMenu']);
    $router->get('menu/{id}',                                 ['uses' => 'MenuController@showOneRestaurantMenu']);
    $router->post('restaurant/{restId}/menu',                                     ['uses' => 'MenuController@create']);
    $router->delete('restaurant/{restId}/menu/{id}',                              ['uses' => 'MenuController@delete']);
    $router->put('restaurant/{restId}/menu/{id}',                                 ['uses' => 'MenuController@update']);
    //$router->get('restaurant/{restId}/user/{userId}/menu/category/{category}',    ['uses' => 'MenuController@showAllRestaurantCategoryMenusTemp']); // TO USE

    $router->get('menu/category/{category}',            ['uses' => 'MenuController@showAllRestaurantCategoryMenusTemp']); // TO USE
    $router->get('category/all[/{type}]',                       ['uses' => 'MenuController@getAllMenuCategories']);

    $router->get('categoriesmenu/all[/{type}]',                       ['uses' => 'MenuController@getAllCategoriesMenu']);

    $router->get('menu/seocategory/micromenu',       ['uses' => 'MenuController@getMicroMenus']);
    $router->get('restaurant/candeliver/{restId}',      ['uses' => 'CanDeliverController@get']);
    $router->get('restaurant/timeslot/{restId}',        ['uses' => 'TimeslotController@get']);
    #Timeslots by Rahul Gupta @ 11-07-2018
    $router->get('restaurant/timeslots/{restId}',        ['uses' => 'TimeslotController@getRestTime']);
    #RestOpenClose api by RG @13-07-2018
    $router->get('restaurant/getDeliveryCarryoutStatus/{restId}', ['uses' => 'RestaurantController@getDeliveryCarryoutStatus']);

    // SUGGESTIONS Updated on 14-07-2018 By rahul
    $router->get('menu-suggestion', ['uses' => 'MenuController@showRestaurantSuggestionsToDelete']);      //showRestaurantSuggestions

    // MENU CATEGORIES
    $router->get('restaurant/distance/{restId}',['uses'=>'LatlngDistanceCalculatorController@get']);
     $router->get('restaurant/list/{restId}',['uses'=>'LatlngDistanceCalculatorController@get_list']);
    $router->get('user/order/list', ['uses' => 'UserOrderController@getList']);    
    $router->get('user/orderdetail/{orderId}',       ['uses' => 'UserOrderController@get']);
    $router->post('user/contactus',    ['uses' => 'contactUsController@contactus']);
    $router->post('order/notification',       ['uses' => 'NotificationController@sendNotification']);
    $router->get('restaurant/menu/posmenudetails/{restId}',       ['uses' => 'PosMenuController@getPosMenus']);
	
	$router->post('restaurant/order/pos', ['uses' => 'CreatePosOrderController@create']);
	
    // Restaurant registered Users
    $router->get('user/restaurant/{id}', function($id) {
        /*$restaurant = \App\Restaurant::find(1);
        return $restaurant->users()->get(); */
        $result = DB::table('restaurants')
            ->join('users', 'users.restaurant_id', '=', 'restaurants.id')
            ->select('users.*')
            ->where('restaurants.id', $id)->get();
        return $result;
    });

    // Brand Restaurant's branches
    $router->get('restaurant/{id}/branch', function($id) {
        return Restaurant::where('parent_restaurant_id', $id)->get();
    });

    // Restaurants (Brands/Self) within User's proximity
    // Brand with branch restaurants will not have lat/lng
    // TO BE MODIFIED/REMOVED
    $router->get('restaurant/geoloc/{geoloc}', function($geoloc) {
        // `id`,`restaurant_name`,`parent_restaurant_id`,`lat`,`lng`
        $distance = 10000;
        list($lat,$lng) = count(explode(',', $geoloc))>1 ? explode(',', $geoloc) : explode('%2C', $geoloc);
        $result = DB::select(
            'SELECT * FROM
              (SELECT *, (3959 * acos (
                  cos (radians('.$lat.'))
                  * cos(radians(`lat`))
                  * cos(radians(`lng`) - radians('.$lng.') )
                  + sin(radians('.$lat.'))
                  * sin(radians(`lat`)))) AS distance
              FROM `restaurants`) AS distance
              WHERE distance < '.$distance.' ORDER BY distance LIMIT 20;');

        return $result;
    });

    // User's distance with selected Restaurant branches
    // Brand won't be returned as Brand with branch restaurants will not have lat/lng
    // TO BE MODIFIED/REMOVED
    $router->get('restaurant/{id}/geoloc/{geoloc}', function($id, $geoloc) {
        $distance = 100;
        list($lat,$lng) = count(explode(',', $geoloc))>1 ? explode(',', $geoloc) : explode('%2C', $geoloc);
        $result = DB::select(
            'SELECT * FROM
              (SELECT *, (3959 * acos (
                  cos(radians('.$lat.'))
                  * cos(radians(`lat`))
                  * cos(radians(`lng`) - radians('.$lng.') )
                  + sin(radians('.$lat.'))
                  * sin(radians(`lat`)))) AS distance
              FROM `restaurants` WHERE `restaurants`.`parent_restaurant_id`='.$id.') AS distances
              WHERE distance < '.$distance.' ORDER BY distance LIMIT 20;');

        return $result;
    });


    //----------------
    // TO DELETE =====
    //----------------
    $router->get('restaurant/{restId}/calculations', function($restId) {
        return [
            'tax'             => 8,
            'delivery_charge' => 4.99,
            'tip_percent'     => [10, 15, 20, 25, 30],
        ];
    });
    $router->get('flush/mybag', function() {
        //\App\Models\UserMyBag::truncate();
        //die('MyBag flushed');
    });

});

/* @01-08-2018 By Rahul Gupta Versioning of api's*/

$router->group(['prefix' => 'api/v1','middleware' => ['localization', 'useragent', 'tokenvalidate']], function() use($router) {
    # Place New Order It was not completed
   # $router->post('payment/placeorder', ['uses' => 'PaymentController@v1_placeOrder']);

    #Get operational hours
    $router->post('contactUs/sendQuery', ['uses' => 'contactUsController@v1_sendQuery']);
    $router->get('restaurant/allmenu', ['uses' => 'MenuController@showAllMenuByRestaurant']); 

});

/* @20-08-2018 By Rahul Gupta OPA (Order Processing app) api's*/

$router->group(['prefix' => 'api/dashboard', 'middleware' => ['localization','useragent']], function() use($router) {
    $router->post('token', 'AuthController@opa_token');
    $router->get('updateapp', 'AuthController@opa_checkupdate');
    $router->post('login', 'AuthController@opa_login');
    $router->get('order', 'PaymentController@opa_order');
    $router->get('new/restaurant/list', 'PaymentController@opa_restaurant_list');	
    $router->get('order/{order_id}', 'PaymentController@opa_order_detail');
    $router->put('order/{order_id}', 'PaymentController@opa_order_update_status');
    $router->get('logout', 'AuthController@opa_logout');
    $router->post('order/refund', 'PaymentController@order_refund');
    $router->post('service', 'RestaurantController@opa_restaurant_setting_update');
    $router->get('restaurant', 'RestaurantController@opa_get_restaurant_detail');
    $router->get('menu/{location_id}', 'OpaMenuController@getItem');
    $router->put('menu/{id}', 'OpaMenuController@updateItem');

});


$router->group(['prefix' => 'api/v2/dashboard', 'middleware' => ['localization','useragent']], function() use($router) {    
    $router->get('order', 'PaymentController@opa_order_v2');   
    $router->put('order/{order_id}', 'PaymentController@opa_order_update_status_v2');    
    $router->post('order/refund', 'PaymentController@order_refund_v2');    
});

/* @29-11-2018 By Rahul khandelwal reservation api*/
$router->group(['prefix' => 'api/reservation','middleware' => ['localization', 'useragent', 'tokenvalidate']], function() use($router) {
    $router->get('working-hours', ['uses' => 'ReservationController@getReservationSlots']);
    $router->post('check-availability',['uses' => 'ReservationController@checkAvailabilityAndGetAvailableSlots']);
    $router->post('reserve-table',['uses' => 'ReservationController@reserveTable']);
    $router->get('listing',['uses' => 'ReservationController@getMyReservation']);
    $router->get('payment-link', ['uses' => 'ReservationController@checkPaymentLink']);
    $router->post('payment', ['uses' => 'ReservationController@reservationPayment']);
});

$router->get('coupanget',function(){

  // print_r(CommonFunctions::UniqueCoupancode()) ;
$addon_data=ItemAddonGroup::where('menu_item_id',2014)->where('id',1)->where('restaurant_id',40)->whereHas('addonitems', function($q){
    $q->where('id', '=',3);
})->with('addon_data')->get();
print_r($addon_data);
});

$router->group(['prefix' => 'api/v2/','middleware' => ['localization','useragent']], function () use ($router) {
	$router->get('menu/category/{category}',            ['uses' => 'MenuController@showAlaCartCategoryMenu']); // TO USE
	$router->get('restaurant/candeliver/{restId}',      ['uses' => 'CanDeliverController@get']);

    $router->post('mybag',              ['uses' => 'MybagController@cart']);          // add to my bag @cart
    $router->post('new_mybag',              ['uses' => 'MybagController@new_cart']);          // add to my bag
     // ORDER
    $router->get('mybag',               ['uses' => 'MybagController@showAllUserMyBagItemsV2']);   // TEMP ROUTE - for PIZZA ORDER
    $router->get('mybag/{id}',          ['uses' => 'MybagController@showOneOrderV2']);

    //$router->post('user/placeorder',         ['uses' => 'PaymentController@placeAOrder']);
    $router->post('user/placeorder',         ['uses' => 'PaymentController@placeOrderMultiPayment']);

    $router->get('user/order/list', ['uses' => 'UserOrderController@getOrderList']);
    $router->get('user/order/search/{query}', ['uses' => 'UserOrderController@searchOrder']);	    
    $router->get('user/orderdetail/{orderId}',       ['uses' => 'UserOrderController@getOrderDetail']);

    $router->get('menu/normal/category/{category}',            ['uses' => 'MenuController@allNormalMenusByParentCat']); // TO USE
    $router->get('static/{page}',   ['uses' => 'StaticController@showRestPageContentAndBlock']);

    $router->get('menuV1/withcats[/{type}]',                       ['uses' => 'MenuController@showAllMenuswithCats']);//old menus
    $router->get('menu/withcats[/{type}]',                       ['uses' => 'MenuController@showAllMenuswithCatsV2']);//new menus



});

#@ sudhanshu 05-Dec-2018 OPA
$router->get('api/restaccount/login/details',['uses' => 'LoginProcessController@create']);



#@ chitrasen 07-Jan-2019 Cut Cat Integration
$router->group(['prefix' => 'api/v3/','middleware' => ['localization','useragent']], function () use ($router) {
//$router->get('restaurant/candeliver/{restId}',      ['uses' => 'CanDeliverController@checkdeliveryavailable']);
$router->get('restaurant/candeliver/{restId}',      ['uses' => 'CanDeliverController@getNearestRestaurant']);
//$router->get('api/delivery/checkdeliveryavailable/',['uses' => 'DeliveryController@checkDeliveryAvailable']);
});

###############Added by sud########################
$router->group(['prefix' => 'api/'], function () use ($router) { 
    $router->get('test/order/details/{orderId}',['uses' => 'TestController@index']);//sud
});

$router->get('api/amit', function() {
    return 'hi';
});



$router->group(['prefix' => 'api/v1/','middleware' => ['localization','useragent']], function () use ($router) { 
    $router->post('mybag',              ['uses' => 'MybagController@v1_cart']);          // add to my bag
     // ORDER
    $router->get('mybag',               ['uses' => 'MybagController@v1_showAllUserMyBagItems']);   // 

});

#@ Rahul Gupta 31-Dec-2019 Pause and Resume Service Email and sms Notification
$router->get('services/intimate',['uses' => 'RestaurantController@send_pause_resume_service_intimation']);



   // $router->group(['prefix' => 'api/sb/','middleware' => ['localization','useragent', 'tokenvalidate']], function () use ($router) {
    //uncomment it to enable token validate



$router->group(['prefix' => 'api/sb/','middleware' => ['localization','useragent']], function () use ($router) {

    $router->get('pages',          ['uses' => 'SiteBuilderPagesController@getPages']);
    $router->get('page/{slug}',          ['uses' => 'SiteBuilderPagesController@getPage']);
    $router->post('page/save',         ['uses' => 'SiteBuilderPagesController@savePage']);
});






$router->group(['prefix' => 'api/v3/','middleware' => ['localization','useragent']], function () use ($router) {

    $router->post('mybag',              ['uses' => 'MybagController@cartV3']);          // add to my bag @cart

     // ORDER
    $router->get('mybag',               ['uses' => 'MybagController@showAllUserMyBagItemsV3']);   // TEMP ROUTE - for PIZZA ORDER
    $router->post('user/placeorder',         ['uses' => 'PaymentController@placeAOrder']);
    $router->get('user/order/list', ['uses' => 'UserOrderController@getOrderList']);
    $router->get('user/orderdetail/{orderId}',       ['uses' => 'UserOrderController@getOrderDetail']);





});




/****************************************************FOR BYW SITES DELIVERY ************************************************/
$router->group(['prefix' => 'api/v4/','middleware' => ['localization','useragent']], function () use ($router) {

    $router->post('mybag',              ['uses' => 'MybagController@cartByw']);
    $router->get('mybag',               ['uses' => 'MybagController@showAllUserMyBagItemsV2Byw']);
    $router->get('mybag/{id}',          ['uses' => 'MybagController@showOneOrderV2Byw']);

     $router->get('reorder/{id}',          ['uses' => 'MybagController@reorder']);


    $router->post('user/placeorder',         ['uses' => 'PaymentController@placeAOrderByw']);


    $router->post('coupon/apply', ['uses' => 'MybagController@applyCoupon']);

    $router->post('coupon/remove', ['uses' => 'MybagController@removeCoupon']);
     

});
$router->get('api/resttime',  function(){

    $cur_date=CommonFunctions::getLocalTimeBased(array("restaurant_id" =>2, 'datetime' =>date('Y-m-d H:i:s')));
    print_r($cur_date);
});




/****************************************************FOR BYW SITES DELIVERY ************************************************/


$router->group(['prefix' => 'api','middleware' => ['localization', 'useragent', 'tokenvalidate']], function() use($router) {

    $router->post('failure',function(Request $request) {
        $data =$request->all();
        DB::table('crash_logs')->insert($data);
    });

});

$router->group(['prefix' => 'api','middleware' => ['localization', 'useragent', 'tokenvalidate']], function() use($router) {
    $router->get('stripe/paymentintent', ['uses' => 'MybagController@getPaymentIntent']);

});

$router->post('stuart/api/update', function(Request $request) {
    $data =$request->all();
    if(isset($data)){	
	//$data  = json_decode($data[0],true);return $data;
	if(isset($data['id'])){
    	$statusData = DB::table('stuart_orders')->where('job_id', '=', $data['id'])->get();	//->select('name', 'email')
		 //DB::table('users')->insert( array('email' => 'john@example.com', 'votes' => 0) );	
		 //DB::table('users')->where('id', 1)->update(array('votes' => 1));
		return $statusData;
	}
    }
   return $data;
});
