<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [ 
 'site_name' => 'HRI',
 'welcome_mail_subject'=>'Welcome to HRI',
 
 'forgot_password_title'=>'Forgot Password',
 'forgot_password_subject'=>'Forgot Password',
 
 'error_social_credential'=>'Social credential is not valid.',
 'error_social_user'=>'User not found.',
 'error_guest_token'=>'Guest token is not valid.',
];