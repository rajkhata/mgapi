<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [ 
 'site_name' => 'HRI',
 'welcome_mail_subject'=>'欢迎来到HRI',
 
 'forgot_password_title'=>'忘记密码',
 'forgot_password_subject'=>'忘记密码',
 
 'error_social_credential'=>'社会凭证无效。',
 'error_social_user'=>'未找到用户。',
 'error_guest_token'=>'访客令牌无效。',
];