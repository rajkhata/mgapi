<?php

return [
    // set JWT time out
    'ttl' => env('JWT_TTL', 60*60*24*7),
];
