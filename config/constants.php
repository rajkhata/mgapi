<?php
    /**
     * Created by PhpStorm.
     * User: Amit Malakar
     * Date: 13/4/18
     * Time: 11:01 AM
     */

    // Server CMS & API path sync
    /*$apiDevPath  = 'dev-api-bravvurashowcase';
    $apiQcPath   = 'qc-api-bravvurashowcase';
    $apiBetaPath   = 'api-munchadobiz';
    $apiLivPath  = 'newapi';
    $apiLocPath1 = 'munch_new';
    $apiLocPath2 = 'munch_api';
    $cmsDevPath  = 'dev-bravvurashowcase';
    $cmsQcPath   = 'qc-bravvurashowcase';
    $cmsBetaPath   = 'cms-munchadobiz';
    $cmsLivPath  = 'newcms';
    $cmsLocPath  = 'munch_cms';
    if (strpos($_SERVER['DOCUMENT_ROOT'], $apiDevPath) !== false) {
        $cmsPath = $cmsDevPath;
    } elseif (strpos($_SERVER['DOCUMENT_ROOT'], $apiQcPath) !== false) {
        $cmsPath = $cmsQcPath;
    } elseif (strpos($_SERVER['DOCUMENT_ROOT'], $apiBetaPath) !== false) {
        $cmsPath = $cmsBetaPath;
    } elseif (strpos($_SERVER['DOCUMENT_ROOT'], $apiLocPath1) !== false || strpos($_SERVER['DOCUMENT_ROOT'], $apiLocPath2) !== false) {
        $cmsPath = $cmsLocPath;
    } else {
        $cmsPath = $cmsLivPath;
    }*/
    $cmsPath = env('CMS_FOLDER_NAME');

    return [
        'mail_image_url'=>env('APP_BASE_URL').'images',
        'base_public_image_path' => '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$cmsPath.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR,
        'socialimage' => '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$cmsPath.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'user',
        'site_url'=>"https://kekinyc.bravvurashowcase.com/",
        'login_url'=>"https://kekinyc.bravvurashowcase.com/login",
        'site_name'=>"Home Run Inn",
        'content_trim_length' => 700,
        // any change in static, requires db field enum change (re-migrate or add enum field in db)
        'static_page_type'    => ['home', 'about', 'story', 'menu', 'team', 'branch'],
        'topcount'            => [
            '3-6'    => ['min' => 3, 'max' => 5, 'light' => 3, 'regular' => 5, 'extra' => 9],
            '7-9'    => ['min' => 6, 'max' => 9, 'light' => 5, 'regular' => 9, 'extra' => 13],
            '10-13'  => ['min' => 10, 'max' => 13, 'light' => 9, 'regular' => 13, 'extra' => 18],
            '14-100' => ['min' => 14, 'max' => 100, 'light' => 13, 'regular' => 18, 'extra' => 25],
        ],
        'payment_gateway'     => [
            'stripe' => [
                #'key'    => 'pk_live_vAk3fvhMN0vWaoDz0Tp02YnL',
                #'secret' => 'sk_live_8Ui52w8VmHJSAgxCqndOR3n5',

                 #'key'    => 'pk_test_3ksHOaiyAisVKWE7xOa1Z99D',
                 #'secret' => 'sk_test_OxrK6FHy2yuax9TB60fspW3S',
                 'key'    => env('STRIPE_KEY'),
                 'secret' => env('STRIPE_SECRET'),
            ],
        ],
        'pizza_setting'       => ['crust', 'size', 'sauce', 'cheese', 'topping_veg', 'topping_non_veg', 'quantity', 'side', 'bake_level', 'cut', 'seasoning'],
        'sides'               => ['left', 'whole', 'right'],
        'quantity'            => ['light', 'regular', 'extra'],
        'session_expiry'      => 60*24*365, // min
        'delivery_interval_time' => 45,
        'carryout_interval_time' => 30,
        'restaurant_timing' => [
            'open_time' => '00:00',
            'close_time' => '23:00'
        ],
        'image'               => [
            'path'      => [
                'byp' => '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$cmsPath.DIRECTORY_SEPARATOR.'public'
                            .DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'byp',
            ],
            'rel_path'  => [
                'byp' => DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'byp' . DIRECTORY_SEPARATOR,
            ],
            'size'      => ['med' => 2, 'sml' => 3],
            'file_size' => 2048,
        ],
        'tax' => 9.5,
        'currency' => '$',
        'currency_code' => 'USD',
        'user_default_password' => 12345678,
	'user_code' => [
		'USER_EXIST_MOBILENO' =>"Mobile number is already assoicated with another account!!!"
	],
        'status_code' => [
            'STATUS_SUCCESS' => 200,
            'STATUS_CREATED' => 201,
            'UNAUTHORIZED_REQUEST' => 401,
            'BAD_REQUEST' => 400,
            'INTERNAL_ERROR' => 500,
        ],
        'opa' => [
            'DASHBOARD_HARD_VERSION_ANDROID' => 2,
            'DASHBOARD_SOFT_VERSION_ANDROID' => 1,
            'COUNTER' => 3,
            'FOURCE_UPDATE_MESSAGE' => 'Hey Muncher! There is a new version of our app available to make your digital dining experience even better.',
            'CLEAR_DATA' => false,
            'APK_FILE_PATH' => url()
        ],
        'guest_category' => [   // RESERVATION MODULE
            'New' => 'New',
            'Repeat' => 'Repeat',
            'VIP' => 'VIP'
        ],
        'netcore' => [   // RESERVATION MODULE
            'apikey_old' => '58b5dc3ec938fb539bb8d6967bacbc28',
            'lisetid_old' => '28',
	     'apikey' => '6f4b0f1a12f59b35080dd294d620cefe',
            'lisetid' => '153'
            ], 
            
        'gift_card_config'=>[

                "gift_type"=> [
                        [
                            "key"=> "egiftcard",
                            "value"=> "E-Gift Card"

                        ]
                           
                   ], 
                "giftcard_for"=> [
                    [ 
                        "key"=> "myself",
                        "value"=> "Myself"
                    ], 
                    [ 
                        "key"=> "recipient", 
                        "value"=> "Recipient"
                    ]
                    ],
                "quantity_limit"=> 20,
                "min_price"=> 15, 
                "max_price"=> 500, 
                "recipient"=> [
                         [
                              "key"=> "single",
                               "value"=> "Single Recipient" 
                         ], 
                         [ 
                             "key"=> "multiple",
                              "value"=> "Multiple Recipient"
                         ]
                        ],
                "other_denomination"=> 1        

        ]    
    ];
